﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;

using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Security.Cryptography;

public partial class PopUps_SafetyPQ_Compare : System.Web.UI.Page
{
    string lightGreen = "#80FF80";
    string lightRed = "#FF8080";
    string lightYellow = "#FFFFCC";

    int updated = 0;
    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Administrator || auth.RoleId == (int)RoleList.Reader)
            {
                if (Request.QueryString["q"] != null)
                {
                    ASPxPageControl1.TabPages[0].Enabled = false;
                    ASPxPageControl1.TabPages[1].Enabled = false;
                    ASPxPageControl1.TabPages[2].Enabled = false;
                    ASPxPageControl1.TabPages[3].Enabled = false;
                    compare(Convert.ToInt32(Request.QueryString["q"].ToString()), false); //http://www.mathertel.de/Diff/default.aspx
                }
                else
                {
                    errorOccured("Error: No Questionnaire Specified");
                }
            }
            else
            {
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Access Denied.")));
            }
        }
    }

    protected void errorOccured(string error)
    {
        //ASPxButton1.Visible = false;
        ASPxButton2.Visible = false;
        ASPxPageControl1.Visible = false;
        lblError.Text = error;
    }
    protected void compare(int qNo, bool copyApproval)
    {
        updated = 0;

        QuestionnaireService qService = new QuestionnaireService();
        CompaniesService cService = new CompaniesService();
        UsersService uService = new UsersService();

        Questionnaire qNew = qService.GetByQuestionnaireId(qNo);


        //find previous questionnaire to compare against
        int count = 0;
        QuestionnaireFilters qFilters = new QuestionnaireFilters();
        qFilters.Append(QuestionnaireColumn.CompanyId, qNew.CompanyId.ToString());
        qFilters.Append(QuestionnaireColumn.Status, ((int)QuestionnaireStatusList.AssessmentComplete).ToString());
        qFilters.AppendLessThan(QuestionnaireColumn.CreatedDate, qNew.CreatedDate.ToString("yyyy-MM-dd"));
        TList<Questionnaire> qList = DataRepository.QuestionnaireProvider.GetPaged(qFilters.ToString(), "CreatedDate DESC", 0, 100, out count);
        if (count > 0)
        {
            Questionnaire qOld = (Questionnaire)qList[0];

            if (!copyApproval)
            {
                if (qOld.Status == (int)QuestionnaireStatusList.AssessmentComplete && qNew.Status == (int)QuestionnaireStatusList.BeingAssessed)
                {
                    btnApprove.Enabled = true;
                }
            }

            /*
             * Questionnaire
             */
            HtmlTable table = new HtmlTable();
            table.ID = "tbQuestionnaire";
            table.Width = "100%";
            table.Rows.Add(rowTitle_Combined("Previous Questionnaire", "Current Questionnaire", true, Color.Red, "", ""));

            //-QuestionnaireId (Debug)
            //compare("QuestionnaireId", table, qOld.QuestionnaireId.ToString(), qNew.QuestionnaireId.ToString(), true);
            //-Company
            Companies c = cService.GetByCompanyId(qNew.CompanyId);
            CompareIsSame(0, "Company", table, c.CompanyName, c.CompanyName, true);
            lblCompanyName.Text = c.CompanyName;
            lblCompanyName.Visible = true;
            //-Created By
            Users uOld = uService.GetByUserId(qOld.CreatedByUserId);
            Users uNew = uService.GetByUserId(qNew.CreatedByUserId);
            Companies cOld = cService.GetByCompanyId(uOld.CompanyId);
            Companies cNew = cService.GetByCompanyId(uNew.CompanyId);
            CompareIsSame(0, "Created By", table, String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.CreatedDate),
                                         String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.CreatedDate), true);
            //-Modified By
            uOld = uService.GetByUserId(qOld.ModifiedByUserId);
            uNew = uService.GetByUserId(qNew.ModifiedByUserId);
            cOld = cService.GetByCompanyId(uOld.CompanyId);
            cNew = cService.GetByCompanyId(uNew.CompanyId);
            CompareIsSame(0, "Modified By", table, String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.ModifiedDate),
                                         String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.ModifiedDate), true);
            //-Assessed By
            string assessOld = "";
            string assessNew = "";
            if (qOld.AssessedByUserId != null)
            {
                uOld = uService.GetByUserId(Convert.ToInt32(qOld.AssessedByUserId));
                cOld = cService.GetByCompanyId(uOld.CompanyId);
                if (qOld.MainAssessmentDate != null) assessOld = String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.MainAssessmentDate);
            }
            if (qNew.AssessedByUserId != null)
            {
                uNew = uService.GetByUserId(Convert.ToInt32(qNew.AssessedByUserId));
                cNew = cService.GetByCompanyId(uNew.CompanyId);
                if (qNew.MainAssessmentDate != null) assessNew = String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.MainAssessmentDate);
            }

            CompareIsSame(0, "Assessed By", table, assessOld, assessNew, true);

            //-Approved By
            string approvedOld = "";
            string approvedNew = "";
            if (qOld.ApprovedByUserId != null)
            {
                uOld = uService.GetByUserId(Convert.ToInt32(qOld.ApprovedByUserId));
                cOld = cService.GetByCompanyId(uOld.CompanyId);
                if (qOld.MainAssessmentDate != null) approvedOld = String.Format("{0}, {1} ({2}) at {3}", uOld.LastName, uOld.FirstName, cOld.CompanyName, qOld.MainAssessmentDate);
            }
            if (qNew.ApprovedByUserId != null)
            {
                uNew = uService.GetByUserId(Convert.ToInt32(qNew.ApprovedByUserId));
                cNew = cService.GetByCompanyId(uNew.CompanyId);
                if (qNew.MainAssessmentDate != null) approvedNew = String.Format("{0}, {1} ({2}) at {3}", uNew.LastName, uNew.FirstName, cNew.CompanyName, qNew.MainAssessmentDate);
            }
            
            CompareIsSame(0, "Approved By", table, approvedOld, approvedNew, true);

            //-Questionnaire Status
            QuestionnaireStatusService qsService = new QuestionnaireStatusService();
            QuestionnaireStatus qsOld = qsService.GetByQuestionnaireStatusId(qOld.Status);
            QuestionnaireStatus qsNew = qsService.GetByQuestionnaireStatusId(qNew.Status);
            CompareIsSame(0, "Status", table, qsOld.QuestionnaireStatusDesc, qsNew.QuestionnaireStatusDesc, true);
            //Procurement Risk Rating
            CompareIsSame(0, "Procurement Risk Rating", table, qOld.InitialRiskAssessment, qNew.InitialRiskAssessment, true);
            //33.055.1 Risk Rating
            CompareIsSame(0, "33.055.1 Risk Rating", table, qOld.MainAssessmentRiskRating, qNew.MainAssessmentRiskRating, true);
            //Recommended
            string recommendedOld = "";
            string recommendedNew = "";
            if (qOld.Recommended == true) recommendedOld = "Yes";
            if (qOld.Recommended == false) recommendedOld = "No";
            if (qNew.Recommended == true) recommendedOld = "Yes";
            if (qNew.Recommended == false) recommendedOld = "No";
            CompareIsSame(0, "Recommended", table, recommendedOld, recommendedNew, true);
            //Recommendation Comments
            string recommendedCommentsOld = "";
            string recommendedCommentsNew = "";
            if (!String.IsNullOrEmpty(qOld.RecommendedComments)) recommendedCommentsOld = qOld.RecommendedComments;
            if (!String.IsNullOrEmpty(qNew.RecommendedComments)) recommendedCommentsNew = qNew.RecommendedComments;
            CompareIsSame(0, "Recommendation Comments", table, recommendedCommentsOld, recommendedCommentsNew, true);
            //-Questionnaire Type
            string typeOld = "Pre-Qualification";
            string typeNew = "Pre-Qualification";
            if (qOld.IsReQualification == true) typeOld = "Re-Qualification";
            if (qNew.IsReQualification == true) typeNew = "Re-Qualification";
            CompareIsSame(0, "Status", table, typeOld, typeNew, true);




            ASPxPageControl1.TabPages[0].Enabled = true;
            phQuestionnaire.Controls.Add(table);

            /*
             * Procurement
             */
            HtmlTable tableProcurement = new HtmlTable();
            tableProcurement.ID = "tbProcurement";
            tableProcurement.Width = "100%";
            tableProcurement.Rows.Add(rowTitle_Combined("Previous Questionnaire", "Current Questionnaire", true, Color.Red, "", ""));

            //-1
            CompareIsSame(1, "1 - First Name", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "1_1"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "1_1"), true);
            CompareIsSame(1, "1 - Last Name", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "1_2"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "1_2"), true);
            CompareIsSame(1, "1 - E-Mail Address", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "1_3"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "1_3"), true);
            CompareIsSame(1, "1 - Job Title", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "1_4"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "1_4"), true);
            CompareIsSame(1, "1 - Telephone", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "1_5"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "1_5"), true);
            //-2
            //compare(1, "2 - EHS Consultant", tableProcurement, EhsConsultantFullName_ByCompanyId(qOld.CompanyId),
            //                                                EhsConsultantFullName_ByCompanyId(qNew.CompanyId), true);
            CompareIsSame(1, "2 - Procurement Contact", tableProcurement, getProcurementValue_User(qOld.QuestionnaireId, "2"),
                                                            getProcurementValue_User(qNew.QuestionnaireId, "2"), true);
            CompareIsSame(1, "2 - Contract Manager", tableProcurement, getProcurementValue_User(qOld.QuestionnaireId, "2_1"),
                                                            getProcurementValue_User(qNew.QuestionnaireId, "2_1"), true);
            CompareIsSame(1, "2 - Original Requester Name", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "2_2"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "2_2"), true);
            CompareIsSame(1, "2 - Original Requester Telephone", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "2_3"),
                                                    Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "2_3"), true);
            //-3
            CompareIsSame(1, "3 - Brief Description of Work", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "3"),
                                                                        Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "3"), true);
            //-4
            string serviceOld = "";
            string serviceNew = "";
            QuestionnaireServicesCategoryService qscService = new QuestionnaireServicesCategoryService();
            QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();
            TList<QuestionnaireServicesSelected> qssListOld = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qOld.QuestionnaireId, (int)QuestionnaireTypeList.Initial_1);
            TList<QuestionnaireServicesSelected> qssListNew = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qNew.QuestionnaireId, (int)QuestionnaireTypeList.Initial_1);
            if (qssListOld.Count > 0)
            {
                QuestionnaireServicesCategory qscOld = qscService.GetByCategoryId(qssListOld[0].CategoryId);
                serviceOld = qscOld.CategoryText;
            }
            if (qssListNew.Count > 0)
            {
                QuestionnaireServicesCategory qscNew = qscService.GetByCategoryId(qssListNew[0].CategoryId);
                serviceNew = qscNew.CategoryText;
            }
            CompareIsSame(1, "4 - Primary service offered by Contractor", tableProcurement, serviceOld, serviceNew, true);
            //-4-Other
            CompareIsSame(1, "4 - Other", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "4_1"),
                                                                        Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "4_1"), true);
            //-5
            CompareIsSame(1, "5 - Number of people expected to work on site", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "5"),
                                                                        Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "5"), true);
            //-6 ********************SITES******
            //-7
            CompareIsSame(1, "7 - When is Service Required?", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "8"),
                                                                        Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "8"), true);

            //not used anymore...
            ////-8
            //compare(1, "8 - Direct Contract or Sub-contract?", tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "9"),
            //                                                            Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "9"), true);
            //string pcOld = "";
            //string pcNew = "";
            //if (Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "9_1") != "0") pcOld = Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "9_1");
            //if (Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "9_1") != "0") pcNew = Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "9_1");
            //try
            //{
            //    Companies cOld2 = cService.GetByCompanyId(Convert.ToInt32(pcOld));
            //    Companies cNew2 = cService.GetByCompanyId(Convert.ToInt32(pcNew));

            //    if (cOld != null) pcOld = cOld2.CompanyName;
            //    if (cNew != null) pcNew = cNew2.CompanyName;
            //}
            //catch (Exception)
            //{
            //    //ToDo: Handle Exception
            //}

            //compare(1, "8 - Primary Contractor", tableProcurement, pcOld, pcNew, true);
            //-9
            int _i = 1;
            while (_i <= 6)
            {
                CompareIsSame(1, "8 - #" + _i.ToString(), tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "11_" + _i.ToString()),
                                                                       Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "11_" + _i.ToString()), true);
                _i++;
            }
            //-10
            _i = 1;
            while (_i <= 11)
            {
                CompareIsSame(1, "9 - #" + _i.ToString(), tableProcurement, Helper.Questionnaire.Procurement.LoadAnswer(qOld.QuestionnaireId, "10_" + _i.ToString()),
                                                                       Helper.Questionnaire.Procurement.LoadAnswer(qNew.QuestionnaireId, "10_" + _i.ToString()), true);
                _i++;
            }

            ASPxPageControl1.TabPages[1].Enabled = true;
            phProcurement.Controls.Add(tableProcurement);

            /*
             * Supplier
             */
            if (qOld.IsMainRequired == true || qNew.IsMainRequired == true)
            {
                HtmlTable tableSupplier = new HtmlTable();
                tableSupplier.ID = "tbSupplier";
                tableSupplier.Width = "100%";
                tableSupplier.Rows.Add(rowTitle_Combined("Previous Questionnaire", "Current Questionnaire", true, Color.Red, "", ""));
                //Company Information

                bool same = true;

                if (!CompareIsSame(2, "1 - Date", tableSupplier,
                           DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId,
                                                  Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy"),
                           DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId,
                                                  Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy"), true))
                {
                    same = false;
                }
                if (!CompareIsSame(2, "2 - Legal Name of Company", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", "")), Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", "")),
                                                                     true)) same = false;
                cOld = cService.GetByCompanyId(Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""))));
                cNew = cService.GetByCompanyId(Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""))));
                if (!CompareIsSame(2, "3 - Company", tableSupplier, cOld.CompanyName, cNew.CompanyName, true)) same = false;
                if (!CompareIsSame(2, "4 - ABN", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", "")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "5 - Address - Street", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "5 - Address - Suburb", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "5 - Address - State", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "5 - Address - PostCode", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "5 - Address - Country", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "6 - Contact Name", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", "")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "7 - Contact Title", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", "")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "8 - Contact E-Mail", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", "")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "9 - Contact Phone", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", "")),
                                                                     true)) same = false;
                if (!CompareIsSame(2, "10 - Contact Fax", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", "")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", "")),
                                                                     true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "1");

                same = true;
                //11 - Services ************************
                string servicesSelectedOld = "";
                string servicesSelectedNew = "";

                qssListOld = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qOld.QuestionnaireId, (int)QuestionnaireTypeList.Main_1);
                if (qssListOld.Count > 0)
                {
                    int i = 1;
                    foreach (QuestionnaireServicesSelected _qs in qssListOld)
                    {
                        QuestionnaireServicesCategory qsc = qscService.GetByCategoryId(_qs.CategoryId);
                        if (qssListOld.Count != i)
                        {
                            servicesSelectedOld += qsc.CategoryText + ", ";
                            i++;
                        }
                        else
                        {
                            servicesSelectedOld += qsc.CategoryText;
                        }
                    }
                }
                qssListNew = qssService.GetByQuestionnaireIdQuestionnaireTypeId(qNew.QuestionnaireId, (int)QuestionnaireTypeList.Main_1);
                if (qssListNew.Count > 0)
                {
                    int i = 1;
                    foreach (QuestionnaireServicesSelected _qs in qssListNew)
                    {
                        QuestionnaireServicesCategory qsc = qscService.GetByCategoryId(_qs.CategoryId);
                        if (qssListNew.Count != i)
                        {
                            servicesSelectedNew += qsc.CategoryText + ", ";
                            i++;
                        }
                        else
                        {
                            servicesSelectedNew += qsc.CategoryText;
                        }
                    }
                }
                if (!CompareIsSame(2, "11 - Services", tableSupplier, servicesSelectedOld, servicesSelectedNew, true)) same = false;
                if (!CompareIsSame(2, "11 - Services: Other", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "B")),
                                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "B")),
                                                                     true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "11");

                //multiple choice time..
                //12
                same = true;
                if (!CompareIsSame(2, "12", tableSupplier, supplierselectedoption("ABC", qOld.QuestionnaireId, "12"), supplierselectedoption("ABC", qNew.QuestionnaireId, "12"), true)) same = false;
                if (!CompareIsSame(2, "12 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "12 - Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "AExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "AExample")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "12");

                //13
                same = true;
                if (!CompareIsSame(2, "13", tableSupplier, supplierselectedoption("ABCDE", qOld.QuestionnaireId, "13"), supplierselectedoption("ABCDE", qNew.QuestionnaireId, "13"), true)) same = false;
                if (!CompareIsSame(2, "13 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "13");

                //14
                same = true;
                if (!CompareIsSame(2, "14 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "14"), supplierselectedoption("A", qNew.QuestionnaireId, "14"), true)) same = false;
                if (!CompareIsSame(2, "14 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "14"), supplierselectedoption("B", qNew.QuestionnaireId, "14"), true)) same = false;
                if (!CompareIsSame(2, "14 - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "14"), supplierselectedoption("C", qNew.QuestionnaireId, "14"), true)) same = false;
                if (!CompareIsSame(2, "14 - (d)", tableSupplier, supplierselectedoption("D", qOld.QuestionnaireId, "14"), supplierselectedoption("D", qNew.QuestionnaireId, "14"), true)) same = false;
                if (!CompareIsSame(2, "14 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "14");

                //15
                same = true;
                if (!CompareIsSame(2, "15 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "15"), supplierselectedoption("A", qNew.QuestionnaireId, "15"), true)) same = false;
                if (!CompareIsSame(2, "15 - (a) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "15 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "15"), supplierselectedoption("B", qNew.QuestionnaireId, "15"), true)) same = false;
                if (!CompareIsSame(2, "15 - (b) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "15 - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "15"), supplierselectedoption("C", qNew.QuestionnaireId, "15"), true)) same = false;
                if (!CompareIsSame(2, "15 - (c) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "15");

                //16
                same = true;
                if (!CompareIsSame(2, "16 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "16"), supplierselectedoption("A", qNew.QuestionnaireId, "16"), true)) same = false;
                if (!CompareIsSame(2, "16 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "16"), supplierselectedoption("B", qNew.QuestionnaireId, "16"), true)) same = false;
                if (!CompareIsSame(2, "16 - (b) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "16 - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "16"), supplierselectedoption("C", qNew.QuestionnaireId, "16"), true)) same = false;
                if (!CompareIsSame(2, "16 - (c) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "16");

                //17
                same = true;
                if (!CompareIsSame(2, "17 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "17"), supplierselectedoption("A", qNew.QuestionnaireId, "17"), true)) same = false;
                if (!CompareIsSame(2, "17 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "17"), supplierselectedoption("B", qNew.QuestionnaireId, "17"), true)) same = false;
                if (!CompareIsSame(2, "17 - (b) Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "BExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "BExample")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "17");

                //18
                same = true;
                if (!CompareIsSame(2, "18", tableSupplier, supplierselectedoption("ABCD", qOld.QuestionnaireId, "18"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "18"), true)) same = false;
                if (!CompareIsSame(2, "18 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "18");

                //19
                same = true;
                if (!CompareIsSame(2, "19", tableSupplier, supplierselectedoption("ABCD", qOld.QuestionnaireId, "19"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "19"), true)) same = false;
                if (!CompareIsSame(2, "19 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "19");

                //20
                same = true;
                if (!CompareIsSame(2, "20 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "20"), supplierselectedoption("A", qNew.QuestionnaireId, "20"), true)) same = false;
                if (!CompareIsSame(2, "20 - (a) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "20 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "20"), supplierselectedoption("B", qNew.QuestionnaireId, "20"), true)) same = false;
                if (!CompareIsSame(2, "20 - (b)Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "20");

                //21
                same = true;
                if (!CompareIsSame(2, "21", tableSupplier, supplierselectedoption("ABCD", qOld.QuestionnaireId, "21"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "21"), true)) same = false;
                if (!CompareIsSame(2, "21 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments")),
                                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "21");

                //22
                same = true;
                if (!CompareIsSame(2, "22 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "22"), supplierselectedoption("A", qNew.QuestionnaireId, "22"), true)) same = false;
                if (!CompareIsSame(2, "22 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "22"), supplierselectedoption("B", qNew.QuestionnaireId, "22"), true)) same = false;
                if (!CompareIsSame(2, "22 - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "22"), supplierselectedoption("C", qNew.QuestionnaireId, "22"), true)) same = false;
                if (!CompareIsSame(2, "22 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "22");

                //23
                same = true;
                if (!CompareIsSame(2, "23", tableSupplier, supplierselectedoption("ABCD", qOld.QuestionnaireId, "23"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "23"), true)) same = false;
                if (!CompareIsSame(2, "23 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "23 - Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Example")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Example")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "23");

                //24
                same = true;
                if (!CompareIsSame(2, "24", tableSupplier, supplierselectedoption("ABCD", qOld.QuestionnaireId, "24"), supplierselectedoption("ABCD", qNew.QuestionnaireId, "24"), true)) same = false;
                if (!CompareIsSame(2, "24 - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "25");

                //25
                same = true;
                if (!CompareIsSame(2, "25 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "25"), supplierselectedoption("A", qNew.QuestionnaireId, "25"), true)) same = false;
                if (!CompareIsSame(2, "25 - (a) Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "AExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "AExample")),
                             true)) same = false;
                if (!CompareIsSame(2, "25 - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "25"), supplierselectedoption("B", qNew.QuestionnaireId, "25"), true)) same = false;
                if (!CompareIsSame(2, "25 - (b) Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "BExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "BExample")),
                             true)) same = false;
                if (!CompareIsSame(2, "25 - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "25"), supplierselectedoption("C", qNew.QuestionnaireId, "25"), true)) same = false;
                if (!CompareIsSame(2, "25 - (c) Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "CExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "CExample")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "25");

                //26
                same = true;
                if (!CompareIsSame(2, "26 - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "25"), supplierselectedoption("A", qNew.QuestionnaireId, "26"), true)) same = false;
                if (!CompareIsSame(2, "26 - (a) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "26 - (a) Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AExample")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "26");

                //27
                same = true;
                if (!CompareIsSame(2, "27 - (a)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3")), true)) same = false;
                if (!CompareIsSame(2, "27 - (b)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3")), true)) same = false;
                if (!CompareIsSame(2, "27 - (c)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3")), true)) same = false;
                if (!CompareIsSame(2, "27 - (d)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3")), true)) same = false;
                if (!CompareIsSame(2, "27 - (e)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3")), true)) same = false;
                if (!CompareIsSame(2, "28 - (a)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3")) + ", " +
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6")),
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2")) + ", " +
                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3")) + ", " +
                                                    Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6")), true)) same = false;
                if (!CompareIsSame(2, "28 - (b)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6")),
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6")), true)) same = false;
                if (!CompareIsSame(2, "28 - (c)", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6")),
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3")) + ", " +
                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6")), true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "28");

                //29i
                same = true;
                if (!CompareIsSame(2, "29i - (a)", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "29i"), supplierselectedoption("A", qNew.QuestionnaireId, "29i"), true)) same = false;
                if (!CompareIsSame(2, "29i - (a) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments")),
                                                                   Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "29i - (b)", tableSupplier, supplierselectedoption("B", qOld.QuestionnaireId, "29i"), supplierselectedoption("B", qNew.QuestionnaireId, "29i"), true)) same = false;
                if (!CompareIsSame(2, "29i - (b) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments")),
                                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "29i - (c)", tableSupplier, supplierselectedoption("C", qOld.QuestionnaireId, "29i"), supplierselectedoption("C", qNew.QuestionnaireId, "29i"), true)) same = false;
                if (!CompareIsSame(2, "29i - (c) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments")),
                                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "29i - (d)", tableSupplier, supplierselectedoption("D", qOld.QuestionnaireId, "29i"), supplierselectedoption("D", qNew.QuestionnaireId, "29i"), true)) same = false;
                if (!CompareIsSame(2, "29i - (d) Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments")),
                                                                 Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments")),
                                                                    true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "29i");

                //29ii
                same = true;
                if (!CompareIsSame(2, "29ii", tableSupplier, supplierselectedoption("A", qOld.QuestionnaireId, "29ii"), supplierselectedoption("A", qNew.QuestionnaireId, "29ii"), true)) same = false;
                if (!CompareIsSame(2, "29ii - Comments", tableSupplier, Helper.Questionnaire.Supplier.Main.LoadAnswer(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments")),
                                                                  Helper.Questionnaire.Supplier.Main.LoadAnswer(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments")),
                                                                    true)) same = false;
                if (!CompareIsSame(2, "29ii - Attachment (FileHash)", tableSupplier,
                            supplierattachmentinfo(qOld.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "AExample")),
                            supplierattachmentinfo(qNew.QuestionnaireId, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "AExample")),
                             true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "29ii");

                //30
                same = true;
                if (!CompareIsSame(2, "30", tableSupplier, supplierselectedoption("ABC", qOld.QuestionnaireId, "30"), supplierselectedoption("ABC", qNew.QuestionnaireId, "30"), true)) same = false;

                if(copyApproval && same)CopyAssessmentApproval_Supplier(qOld.QuestionnaireId, qNew.QuestionnaireId, "30");

                //

                ASPxPageControl1.TabPages[2].Enabled = true;
                phSupplier.Controls.Add(tableSupplier);
            }

            if (qOld.IsVerificationRequired == true || qNew.IsVerificationRequired == true)
            {
                HtmlTable tableVerification = new HtmlTable();
                tableVerification.ID = "tbVerification";
                tableVerification.Width = "100%";
                tableVerification.Rows.Add(rowTitle_Combined("Previous Questionnaire", "Current Questionnaire", true, Color.Red, "", ""));

                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 1, 4, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 4, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 2, 5, copyApproval);
                //write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 3, 1);
                //write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 3, 2);
                //write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 3, 3);
                //write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 3, 4);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 4, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 4, 5, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 4, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 5, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 5, 6, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 6, 4, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 7, 3, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 1, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 2, copyApproval);
                write_verif(tableVerification, qOld.QuestionnaireId, qNew.QuestionnaireId, 8, 4, copyApproval);



                ASPxPageControl1.TabPages[3].Enabled = true;
                phVerification.Controls.Add(tableVerification);
            }

        }
        else
        {
            errorOccured("No previous questionnaire of status 'Assessment Complete' exists to compare against.");
        }
    }
    protected string getProcurementValue_User(int questionnaireId, string questionId)
    {
        string value = " ";
        QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
        QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(questionnaireId, questionId);
        if (qir != null)
        {
            value = UserFullName_ByUserId(Convert.ToInt32(qir.AnswerText));
        }
        return value;
    }

    protected string UserFullName_ByUserId(int u)
    {
        string name = "";
        UsersService uService = new UsersService();
        Users _u = uService.GetByUserId(u);
        if (_u != null) name = String.Format("{0}, {1}", _u.LastName, _u.FirstName);
        return name;
    }
    protected string EhsConsultantFullName_ByCompanyId(int c)
    {
        string name = "";
        CompaniesService cService = new CompaniesService();
        Companies _c = cService.GetByCompanyId(c);

        EhsConsultantService eService = new EhsConsultantService();
        if (_c.EhsConsultantId != null)
        {
            EhsConsultant e = eService.GetByEhsConsultantId(Convert.ToInt32(_c.EhsConsultantId));

            name = UserFullName_ByUserId(e.UserId);
        }
        return name;
    }

    protected string supplierattachmentinfo(int q, int AnswerId)
    {
        string info = "";
        QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
        QuestionnaireMainAttachment qa = qas.GetByQuestionnaireIdAnswerId(q, AnswerId);
        if (qa != null)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qa.FileHash.Length; i++)
            {
                sb.Append(qa.FileHash[i].ToString("X2"));
            }

            info = qa.FileName + " (" + sb.ToString() + ")";
        }
        return info;
    }
    protected string supplierselectedoption(string options, int q, string QuestionId)
    {
        string _selected = "";
        bool sel = false;
        if (options.Length == 1) sel = true;
        if (options.Contains("A"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "A")) == "Yes") { _selected = "A"; };
        }
        if (options.Contains("B"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "B")) == "Yes") { _selected = "B"; };
        }
        if (options.Contains("C"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "C")) == "Yes") { _selected = "C"; };
        }
        if (options.Contains("D"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "D")) == "Yes") { _selected = "D"; };
        }
        if (options.Contains("E"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "E")) == "Yes") { _selected = "E"; };
        }
        if (options.Contains("F"))
        {
            if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, "F")) == "Yes") { _selected = "F"; };
        }

        if (sel)
        {
            if (!String.IsNullOrEmpty(_selected))
            {
                _selected = "Yes";
            }
            else
            {
                _selected = "No";
            }
        }
        return _selected;
    }
    protected string verificationvalue(int q, int sectionId, int questionId)
    {
        string a = "";
        if (!String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sectionId, questionId)))
        {
            a = Helper.Questionnaire.Supplier.Verification.LoadAnswerText(q, sectionId, questionId);
        }
        return a;
        //qvHelper.LoadAnswerByte(q, sNo, 1);
    }
    protected string verificationvalue_comments(int q, int sectionId, int questionId)
    {
        string comment = "";
        QuestionnaireVerificationResponseService qrs = new QuestionnaireVerificationResponseService();
        QuestionnaireVerificationResponse qr = new QuestionnaireVerificationResponse();
        qr = qrs.GetByQuestionnaireIdSectionIdQuestionId(q, sectionId, questionId);
        if (qr != null)
        {
            if (qr.AdditionalEvidence != null)
            {
                comment = Encoding.ASCII.GetString(qr.AdditionalEvidence);
            }
        }
        return comment;
    }
    protected string verificationvalue_attachment(int q, int sectionId, int questionId)
    {
        string info = "";
        QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
        QuestionnaireVerificationAttachment qa = qas.GetByQuestionnaireIdSectionIdQuestionId(q, sectionId, questionId);
        if (qa != null)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < qa.FileHash.Length; i++)
            {
                sb.Append(qa.FileHash[i].ToString("X2"));
            }

            info = qa.FileName + " (" + sb.ToString() + ")";
        }
        return info;
    }

    protected void write_verif(HtmlTable tableVerification, int qOld, int qNew, int sectionId, int questionId, bool copyApproval)
    {
        bool same = true;
        string sectionId2 = sectionId.ToString();
        if (sectionId >= 4) sectionId2 = (sectionId - 1).ToString();
        if (!CompareIsSame(3, "Section " + sectionId2 + " - " + questionId.ToString(), tableVerification, verificationvalue(qOld, sectionId, questionId),
                                                            verificationvalue(qNew, sectionId, questionId), true)) same = false;
        if (!CompareIsSame(3, "Section " + sectionId2 + " - " + questionId.ToString() + " - Comments", tableVerification, verificationvalue_comments(qOld, sectionId, questionId),
                                                    verificationvalue_comments(qNew, sectionId, questionId), true)) same = false;
        if (!CompareIsSame(3, "Section " + sectionId2 + " - " + questionId.ToString() + " - Attachment (FileHash)", tableVerification,
                    verificationvalue_attachment(qOld, sectionId, questionId),
                    verificationvalue_attachment(qNew, sectionId, questionId),
                     true)) same = false;

        if (copyApproval && same) CopyAssessmentApproval_Verification(qOld, qNew, sectionId, questionId);
    }

    protected HtmlTableRow rowTitle(string title, bool bold, Color color, string bgcolor)
    {
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        Label lbl = new Label();
        lbl.Text = title;
        lbl.Font.Bold = bold;
        lbl.ForeColor = color;
        cell.Style.Add("Padding-Top", "3px");
        cell.ColSpan = 2;
        cell.Controls.Add(lbl);
        cell.BgColor = bgcolor;
        row.Cells.Add(cell);
        return row;
    }
    public HtmlTableRow rowTitle_Combined(string title1, string title2, bool bold, Color color, string bgcolor1, string bgcolor2)
    {
        HtmlTableRow row = new HtmlTableRow();

        HtmlTableCell cell1 = new HtmlTableCell();
        Label lbl1 = new Label();
        lbl1.Text = title1;
        lbl1.Font.Bold = bold;
        lbl1.ForeColor = color;
        cell1.Style.Add("Padding-Top", "3px");
        cell1.Controls.Add(lbl1);
        cell1.BgColor = bgcolor1;
        cell1.Width = "50%";
        if (title1 == "Previous Questionnaire") cell1.Align = "Center";
        row.Cells.Add(cell1);

        HtmlTableCell cell2 = new HtmlTableCell();
        Label lbl2 = new Label();
        lbl2.Text = title2;
        lbl2.Font.Bold = bold;
        lbl2.ForeColor = color;
        cell2.Style.Add("Padding-Top", "3px");
        cell2.Controls.Add(lbl2);
        cell2.BgColor = bgcolor2;
        cell2.Width = "50%";
        if (title2 == "Current Questionnaire") cell2.Align = "Center";
        row.Cells.Add(cell2);

        return row;
    }

    protected HtmlTableRow rowCompare(string cellContents)
    {
        HtmlTableRow row = new HtmlTableRow();
        HtmlTableCell cell = new HtmlTableCell();
        cell.Controls.Add(new LiteralControl(cellContents));
        row.Cells.Add(cell);
        return row;
    }

    private static int[] DiffCharCodes(string aText, bool ignoreCase)
    {
        int[] Codes;

        if (ignoreCase)
            aText = aText.ToUpperInvariant();

        Codes = new int[aText.Length];

        for (int n = 0; n < aText.Length; n++)
            Codes[n] = (int)aText[n];

        return (Codes);
    } // DiffCharCodes
    protected bool CompareIsSame(int section, string Title, HtmlTable table, string one, string two, bool combined)
    {
        string color1 = "";
        string color2 = "";
        bool difference = false;
        if (String.Compare(one, two) == 0)
        {
            color1 = lightYellow;
            color2 = lightYellow;
        }
        else
        {
            color1 = lightRed;
            color2 = lightGreen;
            difference = true;
        }
        table.Rows.Add(rowTitle(Title, true, Color.Black, ""));
        if (difference) rowTitle_CheckDifference(table, section, Title, false, Color.Black, "");

        if (combined)
        {
            table.Rows.Add(rowTitle_Combined(one, two, false, Color.Black, color1, color2));
        }
        else
        {
            table.Rows.Add(rowTitle(one, false, Color.Black, color1));
            table.Rows.Add(rowTitle(two, false, Color.Black, color2));
        }

        bool same = true;
        if (difference) same = false;
        return same;
        //}
    }

    protected void CompareIsSame(int section, string Title, HtmlTable table, string one, string two, bool combined, out bool same)
    {
        bool _same = CompareIsSame(section, Title, table, one, two, combined);
        same = _same;
    }

    protected void rowTitle_CheckDifference(HtmlTable table, int section, string title, bool bold, Color color, string bgcolor)
    {
        string title2 = "";
        if (section == 1) // Procurement
        {
            switch (title)
            {
                case "8 - #1":
                    title2 = "Will all work completed by the contractor/subcontractor be completed off-site?";
                    break;
                case "8 - #2":
                    title2 = "Is the work completed within a defined and restricted area or travelling along a defined and restricted route?";
                    break;
                case "8 - #3":
                    title2 = "Does the work use, generate or expose employees to hazardous materials or chemicals?";
                    break;
                case "8 - #4":
                    title2 = "Does the work use tools and equipment which could create a significant hazard to themselves or others?";
                    break;
                case "8 - #5":
                    title2 = "Does the work require advanced layers of protection as safeguards from potential safety and health hazards?";
                    break;
                case "8 - #6":
                    title2 = "Works on-site within defined/restricted routes with no exposure to significant hazards and does not require advanced layers of protection?";
                    break;
                case "9 - #1":
                    title2 = "Is the contractor/supplier fully (100%) supervised or fully escorted when on site?";
                    break;
                case "9 - #2":
                    title2 = "Does the contractor/supplier have minimal exposure to production/maintenance areas?";
                    break;
                case "9 - #3":
                    title2 = "Does the contractor/supplier have minimal exposure to hazardous risks when working on site?";
                    break;
                case "9 - #4":
                    title2 = "Is the contractor on site more frequently and working with partial Alcoa supervision in production/maintenance areas?";
                    break;
                case "9 - #5":
                    title2 = "Is the contractor on site more frequently and working with partial Alcoa supervision and exposed to medium levels of risk?";
                    break;
                case "9 - #6":
                    title2 = "Is the contractor on site frequently and working under limited Alcoa supervision in production/maintenance areas?";
                    break;
                case "9 - #7":
                    title2 = "Is the contractor on site frequently and working under limited Alcoa supervision and exposed to high levels of risk?";
                    break;
                case "9 - #8":
                    title2 = "Is the contractor bidding for major maintenance or capital contract and mobilising a large (>100 people) workforce on site?";
                    break;
                case "9 - #9":
                    title2 = "Is the contractor seeking 'embedded' status and will set up operations on Alcoa property?";
                    break;
                case "9 - #10":
                    title2 = "Has the contractor had a serious or disabling injury, fatality to any employee or contractor within the last 5 years?";
                    break;
                case "9 - #11":
                    title2 = "Has the contractor received any citation notices from any government agency as a result of an injury or fatality within the last 3 years?";
                    break;
                default:
                    break;
            }
        }
        //if (section == 2) //Supplier
        //{
        //    switch (title)
        //    {
        //        case "12":
        //            title2 = "As part of your company’s safety process, are safety hazard assessments conducted, and written job specific safety plans prepared to eliminate safety hazards?<br />a) Yes. Please attach examples.<br />b) No";
        //            break;
        //        default:
        //            break;
        //    }
        //}
        //if (section == 3) //Verification
        //{

        //}
        if (!String.IsNullOrEmpty(title2))
        {
            table.Rows.Add(rowTitle(title2, false, Color.Maroon, ""));
        }
    }
    public HtmlTableRow rowTitle_Combined_CheckDifference(string title1, string title2, bool bold, Color color, string bgcolor1, string bgcolor2, bool difference)
    {
        HtmlTableRow row = new HtmlTableRow();

        HtmlTableCell cell1 = new HtmlTableCell();
        Label lbl1 = new Label();
        lbl1.Text = title1;
        lbl1.Font.Bold = bold;
        lbl1.ForeColor = color;
        cell1.Style.Add("Padding-Top", "3px");
        cell1.Controls.Add(lbl1);
        cell1.BgColor = bgcolor1;
        cell1.Width = "50%";
        if (title1 == "Previous Questionnaire") cell1.Align = "Center";
        row.Cells.Add(cell1);

        HtmlTableCell cell2 = new HtmlTableCell();
        Label lbl2 = new Label();
        lbl2.Text = title2;
        lbl2.Font.Bold = bold;
        lbl2.ForeColor = color;
        cell2.Style.Add("Padding-Top", "3px");
        cell2.Controls.Add(lbl2);
        cell2.BgColor = bgcolor2;
        cell2.Width = "50%";
        if (title2 == "Current Questionnaire") cell2.Align = "Center";
        row.Cells.Add(cell2);

        return row;
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        System.Threading.Thread.Sleep(500);
        compare(Convert.ToInt32(Request.QueryString["q"].ToString()), true);

        lblCompanyName.Text += String.Format(" - {0} Answers Updated", updated);
    }

    protected void CopyAssessmentApproval_Supplier(int qOldId, int qNewId, string QuestionNo)
    {
        //QuestionnaireService qService = new QuestionnaireService();
        //Questionnaire q = qService.GetByQuestionnaireId(qOldId);

        //if (q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //{
            QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
            QuestionnaireMainAssessment qmaOld = qmaService.GetByQuestionnaireIdQuestionNo(qOldId, QuestionNo);
            string oldComments = "";
            if (qmaOld != null) oldComments = qmaOld.AssessorComment;

            QuestionnaireMainAssessment qmaNew = qmaService.GetByQuestionnaireIdQuestionNo(qNewId, QuestionNo);

            if (qmaNew != null)
            {
                if (qmaNew.AssessorApproval == null)
                {
                    qmaNew.ModifiedDate = DateTime.Now;
                    qmaNew.ModifiedByUserId = auth.UserId;

                    qmaNew.AssessorApproval = true;
                    qmaNew.AssessorComment = "Automatically Approved due to no changes from last year.\n\n" + oldComments;
                    qmaService.Save(qmaNew);
                    updated++;
                }
            }
            else
            {
                qmaNew = new QuestionnaireMainAssessment();
                qmaNew.ModifiedDate = DateTime.Now;
                qmaNew.ModifiedByUserId = auth.UserId;

                qmaNew.QuestionnaireId = qNewId;
                qmaNew.QuestionNo = QuestionNo;
                qmaNew.AssessorApproval = true;
                qmaNew.AssessorComment = "Automatically Approved due to no changes from last year.\n\n" + oldComments;
                qmaService.Save(qmaNew);
                updated++;
            }
        //}
    }

    protected void CopyAssessmentApproval_Verification(int qOldId, int qNewId, int sectionId, int questionId)
    {
        //QuestionnaireService qService = new QuestionnaireService();
        //Questionnaire q = qService.GetByQuestionnaireId(qOldId);

        //if (q.Status == (int)QuestionnaireStatusList.AssessmentComplete)
        //{

        if (sectionId != 8 && questionId == 4) //do not copy
        {
            QuestionnaireVerificationAssessmentService qvaService = new QuestionnaireVerificationAssessmentService();
            QuestionnaireVerificationAssessment qvaOld = qvaService.GetByQuestionnaireIdSectionIdQuestionId(qOldId, sectionId, questionId);
            string oldComments = "";
            if (qvaOld != null) oldComments = qvaOld.AssessorComment;
            QuestionnaireVerificationAssessment qvaNew = qvaService.GetByQuestionnaireIdSectionIdQuestionId(qNewId, sectionId, questionId);

            if (qvaNew != null)
            {
                if (qvaNew.AssessorApproval == null)
                {
                    qvaNew.ModifiedDate = DateTime.Now;
                    qvaNew.ModifiedByUserId = auth.UserId;

                    qvaNew.AssessorApproval = true;
                    qvaNew.AssessorComment = "Automatically Approved due to no changes from last year.\n\n" + oldComments;
                    qvaService.Save(qvaNew);
                    updated++;
                }
            }
            else
            {
                qvaNew = new QuestionnaireVerificationAssessment();
                qvaNew.ModifiedDate = DateTime.Now;
                qvaNew.ModifiedByUserId = auth.UserId;

                qvaNew.QuestionnaireId = qNewId;
                qvaNew.SectionId = sectionId;
                qvaNew.QuestionId = questionId;
                qvaNew.AssessorApproval = true;
                qvaNew.AssessorComment = "Automatically Approved due to no changes from last year.\n\n" + oldComments;
                qvaService.Save(qvaNew);
                updated++;
            }
        }
        //}
    }

}

