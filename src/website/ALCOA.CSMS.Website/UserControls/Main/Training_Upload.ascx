﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Training_Upload" Codebehind="Training_Upload.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

    <script type="text/javascript">
    // <![CDATA[
        var textSeparator = ";";
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }

        function UnselectAll() {
            clbSites.UnselectIndices([0]);
            ccbSites.SetText('');
        }

        function UpdateSelectAllItemState() {
            IsAllSelected() ? clbSites.SelectIndices([0]) : clbSites.UnselectIndices([0]);
        }
        function IsAllSelected() {
            for (var i = 1; i < clbSites.GetItemCount(); i++)
                if (!clbSites.GetItem(i).selected)
                    return false;
            return true;
        }
        function UpdateText() {
            var selectedItems = clbSites.GetSelectedItems();
            ccbSites.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            clbSites.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            var values = GetValuesByTexts(texts);
            clbSites.SelectValues(values);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].text);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = clbSites.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
    // ]]>

</script>
<div style="text-align: left">
    <table border="0" cellpadding="2" cellspacing="0" width="900px">
        <tr>
            <td class="pageName" colspan="3" style="height: 43px">
                <span class="bodycopy"><span class="title">Training</span><br />
                    <span class="date">Upload or View Training Schedules</span><br />
                    <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
                </span>
                <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                    <tr>
                        <td style="width: 346px; height: 35px;">
                            <asp:DropDownList ID="ddlCompaniesMain" runat="server" Width="350px" Visible="False">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 278px; height: 35px; text-align: center;">
                            <dxe:ASPxButton ID="btn1" runat="server" AutoPostBack="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" PostBackUrl="javascript:void(0);" Text="Upload Training Schedule"
                                UseSubmitBehavior="False" Width="172px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dxe:ASPxButton>
                        </td>
                        <td style="width: 44px; height: 35px;">
                        </td>
                        <td style="width: 44px; height: 35px;">
                        </td>
                        <td style="width: 44px; height: 35px;">
                        </td>
                        <td style="width: 44px; height: 35px;">
                        </td>
                        <td align="right" class="pageName" style="width: 100px; height: 35px;">
                            <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: underline">
                                Customize</span></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="height: 197px">
                            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" DataSourceID="FileDbMedicalTrainingDataSource" AutoGenerateColumns="False"
                                KeyFieldName="FileId" OnCustomDataCallback="grid_CustomDataCallback" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize" OnHtmlRowCreated="grid_RowCreated"
                                Width="900px">
                                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    <ExpandedButton Height="12px" Width="11px" />
                                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                    </LoadingPanelOnStatusBar>
                                    <CollapsedButton Height="12px" Width="11px" />
                                    <DetailCollapsedButton Height="12px" Width="11px" />
                                    <DetailExpandedButton Height="12px" Width="11px" />
                                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                    </LoadingPanel>
                                </Images>
                                <Settings ShowGroupPanel="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="250" />
                                <ImagesFilterControl>
                                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                    </LoadingPanel>
                                </ImagesFilterControl>
                                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                    <LoadingPanel ImageSpacing="10px">
                                    </LoadingPanel>
                                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                    </Header>
                                    <AlternatingRow Enabled="True">
                                    </AlternatingRow>
                                </Styles>
                                <Columns>
                                    <dxwgv:GridViewCommandColumn Name="commandCol" Caption="Action" ShowInCustomizationForm="False" Visible="True" VisibleIndex="0" Width="60px">
                                        <HeaderStyle Font-Bold="True" />
                                        <ClearFilterButton Visible="False">
                                        </ClearFilterButton>
                                        <EditButton Visible="False">
                                        </EditButton>
                                        <NewButton Visible="False">
                                        </NewButton>
                                        <DeleteButton Visible="True">
                                        </DeleteButton>
                                    </dxwgv:GridViewCommandColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="1"
                                        Name="CompanyBox" ReadOnly="True" Width="230px">
                                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                                            ValueField="CompanyId" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="True" />
                                        <Settings SortMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId" VisibleIndex="2"
                                        Width="100px" Visible="false" ShowInCustomizationForm="false">
                                        <PropertiesComboBox DataSourceID="RegionsDataSource2" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                            TextField="RegionNameAbbrev" ValueField="RegionId" ValueType="System.Int32" NullDisplayText="-">
                                        </PropertiesComboBox>
                                        <Settings SortMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="3"
                                        Width="180px" Visible="true" ShowInCustomizationForm="false">
                                        <PropertiesComboBox DataSourceID="SitesDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                            TextField="SiteName" ValueField="SiteId" ValueType="System.Int32" NullDisplayText="-">
                                        </PropertiesComboBox>
                                        <Settings SortMode="DisplayText" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="File Name" FieldName="FileId" VisibleIndex="4"
                                        Width="200px">

                                        <DataItemTemplate>
                                            <asp:HyperLink ID="hlFile1" runat="server" Text="" Width="100%" ToolTip="Click to download file."></asp:HyperLink>
                                        </DataItemTemplate>
                                        <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                        </CellStyle>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Description" FieldName="FileId" VisibleIndex="5"
                                        Width="85px">
                                        <DataItemTemplate>
                                            <asp:HyperLink ID="hlFile2" runat="server" Text="" Width="100%" ToolTip="Click to download file."></asp:HyperLink>
                                        </DataItemTemplate>
                                        <CellStyle ForeColor="Orange" HorizontalAlign="Left">
                                        </CellStyle>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataHyperLinkColumn>
                                    <dxwgv:GridViewDataDateColumn Caption="Last Modified Date" FieldName="ModifiedDate"
                                        VisibleIndex="6" Width="110px" SortIndex="1" SortOrder="Descending">
                                        <EditFormSettings Visible="False" />
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yy HH:mm">
                                        </PropertiesDateEdit>
                                        <Settings SortMode="Value" AutoFilterCondition="Contains" />
                                    </dxwgv:GridViewDataDateColumn>
                                    <dxwgv:GridViewDataComboBoxColumn Caption="Last Modified By" FieldName="ModifiedByUserId"
                                        ReadOnly="True" UnboundType="String" Visible="false" Width="130px">
                                        <PropertiesComboBox DataSourceID="UsersFullNameDataSource" DropDownHeight="150px"
                                            TextField="UserFullName" ValueField="UserId" ValueType="System.String">
                                            <DropDownButton Enabled="False">
                                            </DropDownButton>
                                        </PropertiesComboBox>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataComboBoxColumn>
                                    <dxwgv:GridViewDataTextColumn Caption="Comments" FieldName="Comments" UnboundType="String"
                                        VisibleIndex="7" Visible="False" Width="110px">
                                        <EditFormSettings Visible="True" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="FileName" Visible="False" >
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataTextColumn FieldName="Description" Visible="False" >
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataTextColumn>
                                    <dxwgv:GridViewDataColumn Caption="Comments" VisibleIndex="8" Width="60px" ShowInCustomizationForm="False"
                                        Visible="false">
                                        <DataItemTemplate>
                                            <a href="#" onclick="OnMoreInfoClick(this, '<%# Container.KeyValue %>')">View</a>
                                        </DataItemTemplate>
                                        <CellStyle HorizontalAlign="Center">
                                        </CellStyle>
                                        <EditFormSettings Visible="False" />
                                    </dxwgv:GridViewDataColumn>
                                </Columns>
                                <SettingsCustomizationWindow Enabled="True" />
                                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn" />
                                <SettingsCookies CookiesID="kpiSP" Version="0.1" />
                                <SettingsPager>
                                    <AllButton Visible="True">
                                    </AllButton>
                                </SettingsPager>
                            </dxwgv:ASPxGridView>
                        </td>
                    </tr>
                    <%--<tr>
                <td colspan="8" style="height: 22px; text-align: right">
                    Number of Rows to show per page:
                    <asp:DropDownList ID="ddlPager" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPager_SelectedIndexChanged">
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>All</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>--%>
                    <tr>
                        <td colspan="7" style="text-align: center">
                            <dxpc:ASPxPopupControl ID="popup" ClientInstanceName="popup" runat="server" EnableClientSideAPI="True"
                                AllowDragging="True" PopupHorizontalAlign="OutsideRight" HeaderText="Comments"
                                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                EnableHotTrack="False" Width="200px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl runat="server">
                                        <dxe:ASPxMemo runat="server" Height="100px" Width="100%" ReadOnly="True" ClientInstanceName="edComments"
                                            EnableClientSideAPI="True" ID="edComments" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxMemo>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                                <HeaderStyle>
                                    <Paddings PaddingRight="6px"></Paddings>
                                </HeaderStyle>
                                <CloseButtonImage Height="12px" Width="13px">
                                </CloseButtonImage>
                                <SizeGripImage Height="16px" Width="16px">
                                </SizeGripImage>
                            </dxpc:ASPxPopupControl>
                            <dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Upload Training Schedule"
                                PopupElementID="btn1" Width="450px" AllowDragging="True" AllowResize="True" Modal="True"
                                PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel1">
                                            <ContentTemplate>
                                                &nbsp;<table style="font-size: 14pt" width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td style="width: 30%; text-align: right" colspan="2">
                                                                <strong>Company Name: </strong>
                                                            </td>
                                                            <td style="width: 70%; text-align: left">
                                                                <dxe:ASPxComboBox ID="ddlCompanies" Width="350px" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                    ClientInstanceName="ddlCompanies" EnableSynchronization="False" IncrementalFilteringMode="StartsWith"
                                                                    ValueType="System.Int32" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged">
                                                                    <ButtonStyle Width="13px">
                                                                    </ButtonStyle>
                                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                    </LoadingPanelImage>
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {
	    UnselectAll();
	    clbSites.PerformCallback(s.GetValue());
}"></ClientSideEvents>
                                                                    <ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="True"
                                                                        ValidationGroup="Upload">
                                                                        <RequiredField IsRequired="True"></RequiredField>
                                                                    </ValidationSettings>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 287px; height: 28px; text-align: right" colspan="2">
                                                                <strong>Site: </strong>
                                                            </td>
                                                            <td style="height: 28px; text-align: left">
                                                                <dxe:ASPxDropDownEdit ClientInstanceName="ccbSites" ID="ddeSites" SkinID="CheckComboBox"
                                                                    Width="350px" runat="server" EnableAnimation="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                    <DropDownWindowStyle BackColor="#EDEDED" />
                                                                    <DropDownWindowTemplate>
                                                                        <dxe:ASPxListBox Width="100%" ID="lbSites" ClientInstanceName="clbSites" SelectionMode="CheckColumn"
                                                                            runat="server" SkinID="CheckComboBoxListBox" OnCallback="clbSites_Callback">
                                                                            <Border BorderStyle="None" />
                                                                            <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                                                            <Items>
                                                                            </Items>
                                                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                                                        </dxe:ASPxListBox>
                                                                        <table style="width: 100%" cellspacing="0" cellpadding="4">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <dxe:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close"
                                                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                                                        <ClientSideEvents Click="function(s, e){ ccbSites.HideDropDown(); }" />
                                                                                    </dxe:ASPxButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </DropDownWindowTemplate>
                                                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                                                </dxe:ASPxDropDownEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 287px; height: 28px; text-align: right" colspan="2">
                                                                <strong>File Description: </strong>
                                                            </td>
                                                            <td style="height: 28px; text-align: left">
                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 150px">
                                                                                <dxe:ASPxComboBox ID="ddlQtr" Width="150px" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                    IncrementalFilteringMode="StartsWith" ValueType="System.String" SelectedIndex="0">
                                                                                    <Items>
                                                                                        <dxe:ListEditItem Text="Qtr 1 (Jan, Feb, Mar)" Value="Qtr 1" Selected="True"></dxe:ListEditItem>
                                                                                        <dxe:ListEditItem Text="Qtr 2 (Apr, May, Jun)" Value="Qtr 2"></dxe:ListEditItem>
                                                                                        <dxe:ListEditItem Text="Qtr 3 (Jul, Aug, Sep)" Value="Qtr 3"></dxe:ListEditItem>
                                                                                        <dxe:ListEditItem Text="Qtr 4 (Oct, Nov, Dec)" Value="Qtr 4"></dxe:ListEditItem>
                                                                                    </Items>
                                                                                    <ButtonStyle Width="13px">
                                                                                    </ButtonStyle>
                                                                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                    </LoadingPanelImage>
                                                                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="Upload">
                                                                                        <RequiredField IsRequired="True"></RequiredField>
                                                                                    </ValidationSettings>
                                                                                </dxe:ASPxComboBox>
                                                                            </td>
                                                                            <td style="width: 100px">
                                                                                <asp:DropDownList ID="ddlYear" Width="65px" runat="server" AutoPostBack="False">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 287px; text-align: right" colspan="2">
                                                                <strong>File: </strong>
                                                            </td>
                                                            <td style="text-align: left">
                                                                <dx:ASPxUploadControl ID="FileUpload1" runat="server">
                                                                    <ValidationSettings
                                                                        AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                                                                        GeneralErrorText="File Upload Failed." MaxFileSize="16252928" MaxFileSizeErrorText="File size exceeds the maximum allowed size (maybe consider ZIPing the file?)"
                                                                        NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
                                                                    </ValidationSettings>
                                                                </dx:ASPxUploadControl>
                                                                <dxe:ASPxLabel ID="lblAllowebMimeType" runat="server" Text="Allowed file types: pdf, doc, xls, ppt, txt, zip"
                                                                    Font-Size="8pt" Visible="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                    CssPostfix="Office2003Blue">
                                                                </dxe:ASPxLabel>
                                                                <br />
                                                                <dxe:ASPxLabel ID="lblMaxFileSize" runat="server" Text="Maximum file size: 15Mb"
                                                                    Font-Size="8pt" Visible="False" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                    CssPostfix="Office2003Blue">
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center" colspan="3">
                                                                <div align="center">
                                                                    <dxe:ASPxButton ID="btnUpload" OnClick="btnUpload_Click" Width="140px" runat="server"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Upload"
                                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        Height="31px" ValidationGroup="Upload">
                                                                    </dxe:ASPxButton>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center" colspan="3">
                                                                <asp:Label ID="Label1" runat="server" Font-Size="12pt" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnUpload"></asp:PostBackTrigger>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                                <HeaderStyle>
                                    <Paddings PaddingRight="6px" />
                                </HeaderStyle>
                                <CloseButtonImage Height="12px" Width="13px" />
                                <SizeGripImage Height="16px" Width="16px" />
                            </dxpc:ASPxPopupControl>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sqldsRegions_ByCompany" runat="server" SelectCommandType="StoredProcedure"
        SelectCommand="_Regions_GetWAOVIC_ByCompany" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="0" Name="CompanyId" Type="String" SessionField="spVar_CompanyId" />
        </SelectParameters>
    </asp:SqlDataSource>
    <%--     <asp:ObjectDataSource ID="odsCompaniesDS_Contractor" runat="server" SelectMethod="GetByCompanyId" TypeName="KaiZen.CSMS.Services.CompaniesService" OldValuesParameterFormatString="original_{0}">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="0" Name="companyId" SessionField="spVar_CompanyId" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>--%>
    <%--<asp:ObjectDataSource
            ID="odsCompaniesDS_Contractor"
            runat="server"
            DataObjectTypeName="KaiZen.CSMS.Entities.FileDbMedicalTraining"
            TypeName="KaiZen.CSMS.Services.FileDbMedicalTrainingService"
            SelectMethod="GetByCompanyIdType_Custom"
            DeleteMethod="Delete"
            InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
            UpdateMethod="Update">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="0" Name="CompanyID" SessionField="spVar_CompanyId"
                    Type="Int32" />
                <asp:Parameter DefaultValue="TS" Name="Type" DbType="String" />
            </SelectParameters>
            <DeleteParameters>
                <asp:Parameter Name="fileId" Type="Int32" />
            </DeleteParameters>
        </asp:ObjectDataSource>--%>
    <data:UsersDataSource ID="UsersDataSource" runat="server" EnableDeepLoad="False"
        EnableSorting="true" SelectMethod="GetAll">
    </data:UsersDataSource>
    <data:UsersFullNameDataSource runat="server" ID="UsersFullNameDataSource" SelectMethod="GetPaged">
    </data:UsersFullNameDataSource>
    <%--<data:FileDbMedicalTrainingDataSource
            ID="FileDbMedicalTrainingDataSource" runat="server"
            EnableDeepLoad="False" EnableSorting="true"
            SelectMethod="GetPaged">
                <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:FileDbMedicalTrainingFilter Column="Type" DefaultValue="TS" SessionField="TS" />
                         </Filters>
                      </data:SqlParameter>
                </Parameters>
        </data:FileDbMedicalTrainingDataSource>--%>
    <%--<data:EntityDataSource ID="FileDbMedicalTrainingDataSource" runat="server"
            ProviderName="FileDbMedicalTrainingProvider"
            EntityTypeName="KaiZen.CSMS.FileDbMedicalTraining, KaiZen.CSMS"
            EntityKeyTypeName="System.Int32"
            SelectMethod="GetAll"
            Filter="Type = 'TS'"
        />  --%>
    <asp:ObjectDataSource ID="FileDbMedicalTrainingDataSource" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.FileDbMedicalTraining"
        TypeName="KaiZen.CSMS.Services.FileDbMedicalTrainingService" SelectMethod="GetByType_Custom"
        DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
        UpdateMethod="Update">
        <SelectParameters>
            <asp:Parameter DefaultValue="TS" Name="Type" />
            <%--<asp:QueryStringParameter QueryStringField="VicOps" Name="VicOps" DefaultValue="False" Type="Boolean" />--%>
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="fileId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <data:EntityDataSource ID="RegionsDataSource2" runat="server" ProviderName="RegionsProvider"
        EntityTypeName="KaiZen.CSMS.Regions, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
        SelectMethod="GetAll" Sort="RegionName ASC" Filter="RegionInternalName = 'WAO'" />
    <data:EntityDataSource ID="SitesDataSource" runat="server" ProviderName="SitesProvider"
        EntityTypeName="KaiZen.CSMS.Sites, KaiZen.CSMS" EntityKeyTypeName="System.Int32"
        SelectMethod="GetAll" Sort="SiteAbbrev ASC" Filter="SiteName != 'All Sites' AND SiteName != 'All Mines' AND SiteName != 'Australia GPP'" />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="SELECT [StatusComments] FROM [FileDB]"></asp:SqlDataSource>
    <cc2:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="Safety Plans"
        GridViewID="grid">
    </cc2:ASPxGridViewExporter>
    <asp:SqlDataSource ID="sqldsAlcoaContact" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="_Companies_AlcoaContact" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="CompanyID" SessionField="CompanyId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</div>
