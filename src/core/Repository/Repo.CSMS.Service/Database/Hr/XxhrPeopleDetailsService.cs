﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.Hr
{
    #region Interface

    public interface IXxhrPeopleDetailsService
    {
        XXHR_PEOPLE_DETAILS Get(Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes);
        List<XXHR_PEOPLE_DETAILS> GetMany(int? top, Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, Func<IQueryable<XXHR_PEOPLE_DETAILS>, IOrderedQueryable<XXHR_PEOPLE_DETAILS>> orderBy, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes);
        List<XXHR_PEOPLE_DETAILS> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, Func<IQueryable<XXHR_PEOPLE_DETAILS>, IOrderedQueryable<XXHR_PEOPLE_DETAILS>> orderBy, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes);
        void Insert(XXHR_PEOPLE_DETAILS item, bool saveChanges = true);
        void Update(XXHR_PEOPLE_DETAILS item, bool saveChanges = true);
        void Delete(XXHR_PEOPLE_DETAILS item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class XxhrPeopleDetailsService : IXxhrPeopleDetailsService
    {
        private readonly IRepository<XXHR_PEOPLE_DETAILS> _repository;

        public XxhrPeopleDetailsService(IRepository<XXHR_PEOPLE_DETAILS> repository)
        {
            this._repository = repository;
        }

        public XXHR_PEOPLE_DETAILS Get(Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<XXHR_PEOPLE_DETAILS> GetMany(int? top, Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, Func<IQueryable<XXHR_PEOPLE_DETAILS>, IOrderedQueryable<XXHR_PEOPLE_DETAILS>> orderBy, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<XXHR_PEOPLE_DETAILS> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, Func<IQueryable<XXHR_PEOPLE_DETAILS>, IOrderedQueryable<XXHR_PEOPLE_DETAILS>> orderBy, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<XXHR_PEOPLE_DETAILS, bool>> where, List<Expression<Func<XXHR_PEOPLE_DETAILS, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(XXHR_PEOPLE_DETAILS item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_PEOPLE_DETAILS");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(XXHR_PEOPLE_DETAILS item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_PEOPLE_DETAILS");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(XXHR_PEOPLE_DETAILS item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_PEOPLE_DETAILS");

            _repository.Delete(item);
            //_userRepository.ExecuteCustomFunction("spDeactivateEntity", new ObjectParameter("TableName", typeof(XXHR_PEOPLE_DETAILS).Name), new ObjectParameter("Value", user.ID));

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
