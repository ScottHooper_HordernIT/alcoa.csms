﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesDataSourceDesigner))]
	public class QuestionnaireReportServicesDataSource : ReadOnlyDataSource<QuestionnaireReportServices>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesDataSource class.
		/// </summary>
		public QuestionnaireReportServicesDataSource() : base(new QuestionnaireReportServicesService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesDataSourceView used by the QuestionnaireReportServicesDataSource.
		/// </summary>
		protected QuestionnaireReportServicesDataSourceView QuestionnaireReportServicesView
		{
			get { return ( View as QuestionnaireReportServicesDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServices, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServices>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesDataSourceView(QuestionnaireReportServicesDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesDataSource QuestionnaireReportServicesOwner
		{
			get { return Owner as QuestionnaireReportServicesDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesService QuestionnaireReportServicesProvider
		{
			get { return Provider as QuestionnaireReportServicesService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServices>
	{
	}

	#endregion QuestionnaireReportServicesDataSourceDesigner

	#region QuestionnaireReportServicesFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServices"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesFilter : SqlFilter<QuestionnaireReportServicesColumn>
	{
	}

	#endregion QuestionnaireReportServicesFilter

	#region QuestionnaireReportServicesExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServices"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesExpressionBuilder		
}

