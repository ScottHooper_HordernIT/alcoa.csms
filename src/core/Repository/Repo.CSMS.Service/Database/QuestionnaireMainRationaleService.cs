﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireMainRationaleService
    {
        QuestionnaireMainRationale Get(Expression<Func<QuestionnaireMainRationale, bool>> where, List<Expression<Func<QuestionnaireMainRationale, object>>> includes);
        List<QuestionnaireMainRationale> GetMany(int? top, Expression<Func<QuestionnaireMainRationale, bool>> where, Func<IQueryable<QuestionnaireMainRationale>, IOrderedQueryable<QuestionnaireMainRationale>> orderBy, List<Expression<Func<QuestionnaireMainRationale, object>>> includes);
        List<QuestionnaireMainRationale> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainRationale, bool>> where, Func<IQueryable<QuestionnaireMainRationale>, IOrderedQueryable<QuestionnaireMainRationale>> orderBy, List<Expression<Func<QuestionnaireMainRationale, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireMainRationale, bool>> where, List<Expression<Func<QuestionnaireMainRationale, object>>> includes);
        void Insert(QuestionnaireMainRationale item, bool saveChanges = true);
        void Update(QuestionnaireMainRationale item, bool saveChanges = true);
        void Delete(QuestionnaireMainRationale item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireMainRationaleService : IQuestionnaireMainRationaleService
    {
        private readonly IRepository<QuestionnaireMainRationale> _repository;

        public QuestionnaireMainRationaleService(IRepository<QuestionnaireMainRationale> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireMainRationale Get(Expression<Func<QuestionnaireMainRationale, bool>> where, List<Expression<Func<QuestionnaireMainRationale, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireMainRationale> GetMany(int? top, Expression<Func<QuestionnaireMainRationale, bool>> where, Func<IQueryable<QuestionnaireMainRationale>, IOrderedQueryable<QuestionnaireMainRationale>> orderBy, List<Expression<Func<QuestionnaireMainRationale, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireMainRationale> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireMainRationale, bool>> where, Func<IQueryable<QuestionnaireMainRationale>, IOrderedQueryable<QuestionnaireMainRationale>> orderBy, List<Expression<Func<QuestionnaireMainRationale, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireMainRationale, bool>> where, List<Expression<Func<QuestionnaireMainRationale, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireMainRationale item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainRationale");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireMainRationale item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainRationale");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireMainRationale item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireMainRationale");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
