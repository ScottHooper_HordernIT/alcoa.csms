﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersEbiService
    {
        UsersEbi Get(Expression<Func<UsersEbi, bool>> where, List<Expression<Func<UsersEbi, object>>> includes);
        List<UsersEbi> GetMany(int? top, Expression<Func<UsersEbi, bool>> where, Func<IQueryable<UsersEbi>, IOrderedQueryable<UsersEbi>> orderBy, List<Expression<Func<UsersEbi, object>>> includes);
        List<UsersEbi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersEbi, bool>> where, Func<IQueryable<UsersEbi>, IOrderedQueryable<UsersEbi>> orderBy, List<Expression<Func<UsersEbi, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersEbi, bool>> where, List<Expression<Func<UsersEbi, object>>> includes);
        void Insert(UsersEbi item, bool saveChanges = true);
        void Update(UsersEbi item, bool saveChanges = true);
        void Delete(UsersEbi item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersEbiService : IUsersEbiService
    {
        private readonly IRepository<UsersEbi> _repository;

        public UsersEbiService(IRepository<UsersEbi> repository)
        {
            this._repository = repository;
        }

        public UsersEbi Get(Expression<Func<UsersEbi, bool>> where, List<Expression<Func<UsersEbi, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersEbi> GetMany(int? top, Expression<Func<UsersEbi, bool>> where, Func<IQueryable<UsersEbi>, IOrderedQueryable<UsersEbi>> orderBy, List<Expression<Func<UsersEbi, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersEbi> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersEbi, bool>> where, Func<IQueryable<UsersEbi>, IOrderedQueryable<UsersEbi>> orderBy, List<Expression<Func<UsersEbi, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersEbi, bool>> where, List<Expression<Func<UsersEbi, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersEbi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbi");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersEbi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbi");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersEbi item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersEbi");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
