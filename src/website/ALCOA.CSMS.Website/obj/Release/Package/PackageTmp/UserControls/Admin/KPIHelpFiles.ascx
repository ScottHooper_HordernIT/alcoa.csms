﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KPIHelpFiles.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.KPIHelpFiles" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>
<script language="javascript" type="text/javascript">
    function popUpClosed() {
        window.location.reload();
    }
</script>

    List of KPI Help Files
    <dx:ASPxGridView ID="grid1" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue"  OnHtmlRowCreated="grid1_RowCreated"
                DataSourceID="DocumentsDataSource" KeyFieldName="DocumentId" OnRowUpdating="grid1_RowUpdating">
                <Columns>
                <dx:GridViewCommandColumn Caption="Action" Visible="True" VisibleIndex="0" Width="50px">
                            <EditButton Visible="True">
                            </EditButton>
                            
                            <ClearFilterButton Visible="True">
                            </ClearFilterButton>
                            
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="DocumentId" ReadOnly="True" Visible="False"
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn ReadOnly="True" VisibleIndex="0" Width="8px">
                            <EditFormSettings Visible="False" />
                            <CellStyle>
                                <Paddings Padding="5px" />
                                <BackgroundImage ImageUrl="~/Images/ArticleIcon.gif" Repeat="NoRepeat" />
                            </CellStyle>
                        </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="DocumentName" Caption="Document Name" VisibleIndex="3" Width="300px" >
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataHyperLinkColumn FieldName="DocumentFileName" Caption="File Name" VisibleIndex="2" Width="100px" ToolTip="Click to download file.">
                         <DataItemTemplate>
                             <table cellpadding="0" cellspacing="0"><tr>
                             <td style="width:120px">
                                 <dx:ASPxHyperLink ID="hlFileDownload" runat="server" Text="">
                                 </dx:ASPxHyperLink>
                             </td>
                             </tr></table>
                        </DataItemTemplate>
                        </dx:GridViewDataHyperLinkColumn>
                    <dx:GridViewDataTextColumn FieldName="DocumentFileDescription" Visible="True" VisibleIndex="1" ReadOnly="true" SortIndex="0"  SortOrder="Ascending" Caption="KPI Help File Category" Width="400px">
                    <EditFormSettings Visible="false"></EditFormSettings>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn FieldName="DocumentType" Visible="False" VisibleIndex="3">
                        <EditFormSettings Visible="False" />
                    </dx:GridViewDataTextColumn>
                   <dx:GridViewDataComboBoxColumn FieldName="UseDefault" Visible="false" VisibleIndex="4" UnboundType="String">
                   <PropertiesComboBox ValueType="System.Int32">
                   <Items>
                   <dx:ListEditItem Text="No" Value="0" Selected="true"/>
                   <dx:ListEditItem Text="Yes" Value="1" />
                   </Items>
                   </PropertiesComboBox>
                   <EditFormSettings Visible="False" />
                   </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Using Default" FieldName="UsingDefault" ReadOnly="True"
                 VisibleIndex="4" Width="120px">
                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:120px"><asp:Label ID="lblRead" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
                 <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                         <EditFormSettings Visible="False" />
             </dx:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <CollapsedButton Height="12px"
                        Width="11px" />
                    <ExpandedButton Height="12px"
                        Width="11px" />
                    <DetailCollapsedButton Height="12px"
                        Width="11px" />
                    <DetailExpandedButton Height="12px"
                        Width="11px" />
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" />
            </dx:ASPxGridView>
    <br />
    Default Document Details
    <br />
<dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" DataSourceID="KpiHelpFilesDataSource"
    OnHtmlRowCreated="grid_HtmlRowCreated"
    OnRowInserting="grid_RowInserting" OnRowDeleting="grid_RowDeleting" OnRowUpdating="grid_RowUpdating">
    <Columns>
        <dx:GridViewDataTextColumn FieldName="KpiHelpFileId" 
            VisibleIndex="1" Caption="Action">
            <DataItemTemplate>
                <dx:ASPxHyperLink ID="hlEdit" runat="server" Text="Edit">
                </dx:ASPxHyperLink>
            </DataItemTemplate>
            <EditFormSettings Visible="False" />
            <Settings AllowHeaderFilter="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="KpiHelpCaption" VisibleIndex="2" Caption="Kpi Help File Category">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="FileName" VisibleIndex="3">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="Size" FieldName="ContentLength" 
            VisibleIndex="4">
            <DataItemTemplate>
                <dx:ASPxLabel ID="lblFileSize" runat="server" Text="-">
                </dx:ASPxLabel>
            </DataItemTemplate>
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        
    </Columns>
    
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dx:ASPxGridView>

    <br />

    <dx:ASPxHyperLink ID="hlAddNew" runat="server" Visible="false" Text="Add new default document">
                </dx:ASPxHyperLink>
    <br />
    <br />
    <strong><span style="text-decoration: underline">Note:</span></strong>

<strong>If no filename is entered for a KPI Help File Category, then the default file is automatically used.</strong>
<br />


<data:KpiHelpFilesDataSource ID="KpiHelpFilesDataSource" runat="server"></data:KpiHelpFilesDataSource>
<data:DocumentsDataSource
                    ID="DocumentsDataSource"
                    runat="server"
                    EnableDeepLoad="False"
                    EnableSorting="true"
                    SelectMethod="GetPaged">
                    <Parameters>
                      <data:SqlParameter Name="WhereClause" UseParameterizedFilters="false">
                         <Filters>
                            <data:DocumentsFilter Column="DocumentType" DefaultValue="KH"  />
                           
                         </Filters>
                      </data:SqlParameter>
                   </Parameters>
                </data:DocumentsDataSource>
