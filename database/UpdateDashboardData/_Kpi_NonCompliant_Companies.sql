USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_Kpi_NonCompliant_Companies]    Script Date: 30/07/2015 4:12:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_Kpi_NonCompliant_Companies]                                                  
(                                                  
 @SiteId int,                 
 @Month int,                                                 
 @Year int                                                  
)                                                  
AS                                        
DECLARE @Companies VARCHAR(MAX)              
DECLARE @ErrorMsg VARCHAR(1000)    
DECLARE @Operator VARCHAR(2), @PlanValue VARCHAR(50)    
----------------------        
SET @Companies = ''        
          
BEGIN TRY               
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'Lost Work Day   LWD'     
---------------------    
             
IF (@SiteId > 0)                                   
 BEGIN     
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) < @PlanValue ) X    
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) > @PlanValue ) X    
  END     
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) <> @PlanValue ) X    
  END              
 END                            
ELSE                            
 BEGIN     
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))    
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) < @PlanValue ) X    
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))    
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) > @PlanValue ) X    
  END     
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                    
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))    
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(A.ipLTI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(A.ipLTI,0)))                                
   END) AS int))),0) <> @PlanValue ) X    
  END              
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'LWD')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'LWD'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'LWD'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH               
                
-------------------------------               
SET @Companies = ''        
              
BEGIN TRY     
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'Medical Treatments   MT'     
---------------------              
                  
IF (@SiteId > 0)                                   
 BEGIN      
    
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
     SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
     (SELECT DISTINCT C.CompanyName                   
     FROM KpiCompanySiteCategory A               
     LEFT OUTER JOIN Companies C                      
     ON A.CompanyId = C.CompanyId                 
     WHERE A.SiteId = @SiteId                   
     AND YEAR(A.kpidatetime) = @Year                  
     GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
     HAVING ISNULL(((CAST((CASE                                
     WHEN (SUM(COALESCE(ipMTI,0)) = 0)                                
     THEN 0                                
     ELSE (SUM(COALESCE(ipMTI,0)))                                
     END) AS int))),0) < @PlanValue ) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
     SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
     (SELECT DISTINCT C.CompanyName                   
     FROM KpiCompanySiteCategory A               
     LEFT OUTER JOIN Companies C                      
     ON A.CompanyId = C.CompanyId                 
     WHERE A.SiteId = @SiteId                   
     AND YEAR(A.kpidatetime) = @Year                  
     GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
     HAVING ISNULL(((CAST((CASE                                
     WHEN (SUM(COALESCE(ipMTI,0)) = 0)                                
     THEN 0                                
     ELSE (SUM(COALESCE(ipMTI,0)))                                
     END) AS int))),0) > @PlanValue ) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
     SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
     (SELECT DISTINCT C.CompanyName                   
     FROM KpiCompanySiteCategory A               
     LEFT OUTER JOIN Companies C                      
     ON A.CompanyId = C.CompanyId                 
     WHERE A.SiteId = @SiteId                   
     AND YEAR(A.kpidatetime) = @Year                  
     GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
     HAVING ISNULL(((CAST((CASE                                
     WHEN (SUM(COALESCE(ipMTI,0)) = 0)                                
     THEN 0                                
     ELSE (SUM(COALESCE(ipMTI,0)))                                
     END) AS int))),0) <> @PlanValue ) X       
  END                 
 END                            
ELSE                            
 BEGIN                   
    
    
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipMTI,0)) = 0)                           
   THEN 0                                
   ELSE (SUM(COALESCE(ipMTI,0)))                                
   END) AS int))),0) < @PlanValue) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE      
   WHEN (SUM(COALESCE(ipMTI,0)) = 0)                           
   THEN 0                                
   ELSE (SUM(COALESCE(ipMTI,0)))                                
   END) AS int))),0) > @PlanValue) X        
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipMTI,0)) = 0)                           
   THEN 0                                
   ELSE (SUM(COALESCE(ipMTI,0)))                                
   END) AS int))),0) <> @PlanValue) X        
  END               
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'MT')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'MT'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'MT'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
              
-------------------------------               
SET @Companies = ''            
              
BEGIN TRY     
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'Restricted Duty   RST'     
---------------------                
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year        
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) < @PlanValue  ) X      
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) > @PlanValue) X     
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) <> @PlanValue ) X      
  END     
               
 END                            
ELSE                            
 BEGIN                   
    
    
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) < @PlanValue ) X      
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) > @PlanValue ) X     
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRDI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRDI,0)))                                
   END) AS int))),0) <> @PlanValue) X      
  END     
               
 END                     
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'RST')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'RST'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'RST'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
              
-------------------------------               
SET @Companies = ''           
              
BEGIN TRY      
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'First Aids   FA'     
---------------------              
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
   END) AS int))),0) < @PlanValue) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
   END) AS int))),0) > @PlanValue) X      
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
   END) AS int))),0) <> @PlanValue) X      
  END                
 END                            
ELSE                            
 BEGIN                   
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
   END) AS int))),0) < @PlanValue) X        
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
   END) AS int))),0) > @PlanValue) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING  ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipFATI,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipFATI,0)))                                
END) AS int))),0) <> @PlanValue) X     
  END    
               
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'FA')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'FA'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'FA'               
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
              
-------------------------------               
SET @Companies = ''              
              
BEGIN TRY       
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'Injury Free Events   IFE'     
---------------------               
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) < @PlanValue ) X          
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) > @PlanValue ) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) <> @PlanValue ) X      
  END    
                
 END             
ELSE                            
 BEGIN                   
      
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)         
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) < @PlanValue ) X          
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) > @PlanValue ) X        
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipIFE,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipIFE,0)))                                
   END) AS int))),0) <> @PlanValue ) X      
  END    
             
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'IFE')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'IFE'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'IFE'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH         
-------------------------------               
SET @Companies = ''        
              
BEGIN TRY       
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'Risk Notifications   RN'     
---------------------               
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                     
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                 
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) < @PlanValue ) X           
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                 
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) > @PlanValue ) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                 
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) <> @PlanValue ) X      
  END              
 END                            
ELSE                            
 BEGIN                   
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) < @PlanValue ) X            
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) > @PlanValue ) X        
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                   
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                
   HAVING ISNULL(((CAST((CASE                                
   WHEN (SUM(COALESCE(ipRN,0)) = 0)                                
   THEN 0                                
   ELSE (SUM(COALESCE(ipRN,0)))                                
   END) AS int))),0) <> @PlanValue ) X        
  END               
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'RN')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'RN'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
        ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'RN'                      
           ,@Companies)                  
                      
END TRY                
BEGIN CATCH                
  SET @ErrorMsg = ERROR_MESSAGE()                 
END CATCH                 
          
-------------------------------               
SET @Companies = ''        
           
BEGIN TRY      
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'IFE / Injury ratio'     
---------------------              
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) < @PlanValue ) X             
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) > @PlanValue ) X        
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) <> @PlanValue ) X      
  END                  
 END                            
ELSE                            
 BEGIN                   
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) < @PlanValue ) X            
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) > @PlanValue ) X       
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipIFE,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) <> @PlanValue ) X       
  END      
              
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'IFE Injury Ratio')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month                      
   AND [Year] = @Year                      
   AND [ItemName] = 'IFE Injury Ratio'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                      
           ,'IFE Injury Ratio'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH                 
-------------------------------               
SET @Companies = ''        
         
BEGIN TRY        
    
---------------------    
SELECT @Operator = C.Operator, @PlanValue = A.PlanValue     
FROM dbo.AdminPlan A    
INNER JOIN dbo.AdminDashboard B     
ON A.DashboardId = B.DashboardId     
INNER JOIN dbo.AdminPlanOperators C    
ON A.Operator = C.OperatorId    
WHERE A.[Month] = @Month    
AND A.[Year] = @Year     
AND B.FieldName = 'RN / Injury ratio'     
---------------------            
                  
IF (@SiteId > 0)                                   
 BEGIN                       
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) < @PlanValue ) X            
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) > @PlanValue ) X      
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                   
   (SELECT DISTINCT C.CompanyName                   
   FROM KpiCompanySiteCategory A               
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                 
   WHERE A.SiteId = @SiteId                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) <> @PlanValue ) X      
  END      
    
                 
 END                            
ELSE                            
 BEGIN                   
    
 IF ISNULL(@Operator,'') = '>' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) < @PlanValue ) X            
  END    
 ELSE IF ISNULL(@Operator,'') = '<' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) > @PlanValue ) X     
  END    
 ELSE IF ISNULL(@Operator,'') = '=' AND ISNULL(@PlanValue,'') <> ''    
  BEGIN                      
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM                       
   (SELECT DISTINCT C.CompanyName              
   FROM KpiCompanySiteCategory A              
   LEFT OUTER JOIN Companies C                      
   ON A.CompanyId = C.CompanyId                   
   WHERE A.SiteId IN (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))                   
   AND YEAR(A.kpidatetime) = @Year                  
   GROUP BY A.SiteId, A.CompanyId, C.CompanyName                  
   HAVING ISNULL(CAST((CASE                                  
   WHEN ((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0)) ) = 0.0)                                   
   THEN CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1))                                   
   ELSE (CAST(SUM(COALESCE(ipRN,0)) AS DECIMAL(18,1)) / CAST((SUM(COALESCE(ipLTI,0)) + SUM(COALESCE(ipRDI,0)) + SUM(COALESCE(ipMTI,0)) + SUM(COALESCE(ipFATI,0))) AS DECIMAL(18,1)))                  
   END) AS DECIMAL(18,1)),0) <> @PlanValue ) X       
  END              
 END                      
                
                
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year                       
 AND [Month] = @Month AND [ItemName] = 'RN Injury Ratio')                      
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]                      
 SET [NonCompliantCompanies] = @Companies                      
 WHERE [SiteId] = @SiteId                      
   AND [Month] = @Month               
   AND [Year] = @Year                      
   AND [ItemName] = 'RN Injury Ratio'                      
ELSE                      
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]                      
           ([SiteId]                      
           ,[Month]                      
           ,[Year]                      
           ,[ItemName]                      
           ,[NonCompliantCompanies])                      
     VALUES                      
           (@SiteId                      
           ,@Month                      
           ,@Year                               
           ,'RN Injury Ratio'                      
           ,@Companies)                  
                      
                       
END TRY                
BEGIN CATCH                
 SET @ErrorMsg = ERROR_MESSAGE()                
END CATCH           
----------------------------------------

GO

