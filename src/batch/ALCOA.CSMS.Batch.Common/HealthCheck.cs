﻿using System;
using System.Collections.Generic;
using System.Text;

using ALCOA.CSMS.Batch.Common.Log;
using ALCOA.CSMS.Batch.Common;
using Autofac;
using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;
using System.Web.Mvc;

namespace ALCOA.CSMS.Batch.Common.HealthCheck
{
    public class HealthCheck
    {
        //DT259 - new function to reest last run to now to allow email to be sent if a job didn't run on schedule
        public static void ResetJobSchedule(string jobName)
        {
            //Setup dependencies
            DependencyConfig.RegisterDependencies(new ContainerBuilder());

            var jobScheduleService = DependencyResolver.Current.GetService<repo.IJobScheduleService>();
            model.JobSchedule jobSchedule = jobScheduleService.Get(p => p.Name == jobName, null);

            if (jobSchedule != null)
            {
                Log4NetService.LogInfo(String.Format("JobWatch - JobName: {0}, previous last start date was: {1}", jobName, jobSchedule.LastStartedDateTime.ToString())); //Cindi Thornton 25/11/15 - added more logging

                jobSchedule.LastStartedDateTime = DateTime.Now;
                jobSchedule.EmailSent = false;

                jobScheduleService.Update(jobSchedule);
                Log4NetService.LogInfo(String.Format("JobWatch - JobName: {0}, new last start date is: {1}", jobName, jobSchedule.LastStartedDateTime.ToString())); //Cindi Thornton 25/11/15 - added more logging
            }
            else
            {
                Log4NetService.LogError(String.Format("Incorrect job name [{0}] passed to HealthCheck.ResetJobSchedule", jobName));
            }
        }
    }
}
