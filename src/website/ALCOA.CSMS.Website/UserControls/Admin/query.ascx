﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="query.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.query" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>

    <%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

    <%@ Register src="../Other/ExportButtons.ascx" tagname="ExportButtons" tagprefix="uc1" %>

    <strong><asp:Label ID="lblQuery" runat="server" Text="Query:"></asp:Label></strong><br />
<dx:ASPxMemo ID="mQuery" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" Height="150px" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="800px">
</dx:ASPxMemo>
<dx:ASPxComboBox ID="cbResultType" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" SelectedIndex="0" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <Items>
        <dx:ListEditItem Selected="True" Text="n/a" Value="n/a" />
        <dx:ListEditItem Text="grid" Value="grid" />
        <dx:ListEditItem Text="string" Value="string" />
        <dx:ListEditItem Text="int" Value="int" />
    </Items>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
</dx:ASPxComboBox>
<br />
<dx:ASPxButton ID="btnQuery" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
    Text="Query" onclick="btnQuery_Click">
</dx:ASPxButton>
<p>
    <strong>Results:</strong><br />
    <asp:Label ID="lblResults" runat="server" Text="-"></asp:Label>
</p>
<p>
    <dx:ASPxGridView ID="grid" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
            </LoadingPanelOnStatusBar>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
            </LoadingPanel>
        </Images>
        <ImagesFilterControl>
            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
            </LoadingPanel>
        </ImagesFilterControl>
        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
            CssPostfix="Office2003Blue">
            <Header ImageSpacing="5px" SortingImageSpacing="5px">
            </Header>
            <LoadingPanel ImageSpacing="10px">
            </LoadingPanel>
        </Styles>
    </dx:ASPxGridView>
</p>

    

    <asp:SqlDataSource ID="dsCsm" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"></asp:SqlDataSource>