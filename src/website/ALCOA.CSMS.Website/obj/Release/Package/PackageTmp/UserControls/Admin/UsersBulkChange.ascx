<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Admin_UsersBulkChange" Codebehind="UsersBulkChange.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
                    <strong>Companies - Change Safety Assessor<br />
                    </strong>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 143px">
                                    From</td>
                                <td style="width: 100px">
                                    To</td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 143px"><dx:ASPxComboBox runat="server" ValueType="System.Int32" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba1From" TextField="UserFullName" ValueField="EhsConsultantId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" ValidationGroup="a1" Display="Dynamic">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; padding-left: 10px;"><dx:ASPxComboBox runat="server" ValueType="System.Int32" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba1To" TextField="UserFullName" ValueField="EhsConsultantId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" ValidationGroup="a1" Display="Dynamic">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; text-align: center">
                                    <dx:ASPxButton runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Text="Change" ValidationGroup="a1" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="btna1" OnClick="btna1_Click">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <dx:ASPxCheckBox ID="cbSafetyAssessorChangeInProgress" runat="server" 
                                        Checked="True" CheckState="Checked" Text="Only Change In-Progress SQ's">
                                    </dx:ASPxCheckBox>
                                </td>
                                <td style="width: 100px; text-align: center">
                                    &nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <strong>Safety Management Plans - Change H&amp;S Contact</strong><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 143px">
                                    From</td>
                                <td style="width: 100px; text-align: left;">
                                    To</td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 143px"><dx:ASPxComboBox runat="server" ValueType="System.Int32" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba2From" TextField="UserFullName" ValueField="EhsConsultantId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" ValidationGroup="a2" Display="Dynamic">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; padding-left: 10px;">
                                    <dx:ASPxComboBox runat="server" ValueType="System.String" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba2To" TextField="UserFullName" ValueField="EhsConsultantId" IncrementalFilteringMode="StartsWith">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ValidationSettings CausesValidation="True" ValidationGroup="a2" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; text-align: center">
                                    <dx:ASPxButton runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Text="Change" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="btna2" OnClick="btna2_Click" ValidationGroup="a2">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <strong>Safety Questionnaires - Change Procurement Contact</strong><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 143px">
                                    From</td>
                                <td style="width: 100px">
                                    To</td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 143px; height: 40px;"><dx:ASPxComboBox runat="server" ValueType="System.Int32" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba3From" TextField="UserFullName" ValueField="UserId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" ValidationGroup="a3" Display="Dynamic">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; padding-left: 10px; height: 40px;">
                                    <dx:ASPxComboBox runat="server" ValueType="System.String" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba3To" TextField="UserFullName" ValueField="UserId" IncrementalFilteringMode="StartsWith">
                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                        </LoadingPanelImage>
                                        <ValidationSettings CausesValidation="True" ValidationGroup="a3" Display="Dynamic">
                                            <RequiredField IsRequired="True" />
                                        </ValidationSettings>
                                    </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; text-align: center; height: 40px;">
                                    <dx:ASPxButton runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Text="Change" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="btna3" OnClick="btna3_Click" ValidationGroup="a3">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <strong>Safety Questionnaires - Change Procurement&nbsp;Functional Supervisor</strong><br />
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 143px">
                                    From</td>
                                <td style="width: 100px">
                                    To</td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 143px"><dx:ASPxComboBox runat="server" ValueType="System.Int32" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba4From" TextField="UserFullName" ValueField="UserId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" ValidationGroup="a4" Display="Dynamic">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; padding-left: 10px;"><dx:ASPxComboBox runat="server" ValueType="System.String" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="cba4To" TextField="UserFullName" ValueField="UserId" IncrementalFilteringMode="StartsWith">
                                    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                    </LoadingPanelImage>
                                    <ValidationSettings CausesValidation="True" Display="Dynamic" ValidationGroup="a4">
                                        <RequiredField IsRequired="True" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                </td>
                                <td style="width: 100px; text-align: center">
                                    <dx:ASPxButton runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        Text="Change" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="btna4" OnClick="btna4_Click" ValidationGroup="a4">
                                    </dx:ASPxButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <dx:ASPxLabel ID="aResults" runat="server" ForeColor="Red">
                    </dx:ASPxLabel>
<%--<asp:SqlDataSource ID="dsEhsConsultants_ListAssignedToCompanies" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_EHSConsultant_ListAssignedToCompanies" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%><%--<asp:SqlDataSource ID="dsEhsConsultants_ListAssignedToSmps" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_EHSConsultant_ListAssignedToSmps" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%>
<%--<asp:SqlDataSource ID="dsQuestionnaire_ListAssignedProcurementFunctionalManagers" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_Questionnaire_ListAssignedProcurementFunctionalManagers" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%><%--<asp:SqlDataSource ID="dsQuestionnaire_ListAssignedProcurementContact" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_Questionnaire_ListAssignedProcurementContacts" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%>
<%--<asp:SqlDataSource ID="dsUsersEhsConsultants_ListOrdered" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_UsersEhsConsultants_ListOrdered" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%><%--<asp:SqlDataSource ID="dsUsersProcurementList_ListOrdered" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_UsersProcurementList_ListOrdered" SelectCommandType="StoredProcedure"></asp:SqlDataSource>--%>
