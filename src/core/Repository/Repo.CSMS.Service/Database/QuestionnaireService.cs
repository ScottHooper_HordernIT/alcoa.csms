﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireService
    {
        Questionnaire Get(Expression<Func<Questionnaire, bool>> where, List<Expression<Func<Questionnaire, object>>> includes);
        List<Questionnaire> GetMany(int? top, Expression<Func<Questionnaire, bool>> where, Func<IQueryable<Questionnaire>, IOrderedQueryable<Questionnaire>> orderBy, List<Expression<Func<Questionnaire, object>>> includes);
        List<Questionnaire> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Questionnaire, bool>> where, Func<IQueryable<Questionnaire>, IOrderedQueryable<Questionnaire>> orderBy, List<Expression<Func<Questionnaire, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Questionnaire, bool>> where, List<Expression<Func<Questionnaire, object>>> includes);
        void Insert(Questionnaire item, bool saveChanges = true);
        void Update(Questionnaire item, bool saveChanges = true);
        void Delete(Questionnaire item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireService : IQuestionnaireService
    {
        private readonly IRepository<Questionnaire> _repository;

        public QuestionnaireService(IRepository<Questionnaire> repository)
        {
            this._repository = repository;
        }

        public Questionnaire Get(Expression<Func<Questionnaire, bool>> where, List<Expression<Func<Questionnaire, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Questionnaire> GetMany(int? top, Expression<Func<Questionnaire, bool>> where, Func<IQueryable<Questionnaire>, IOrderedQueryable<Questionnaire>> orderBy, List<Expression<Func<Questionnaire, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Questionnaire> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Questionnaire, bool>> where, Func<IQueryable<Questionnaire>, IOrderedQueryable<Questionnaire>> orderBy, List<Expression<Func<Questionnaire, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Questionnaire, bool>> where, List<Expression<Func<Questionnaire, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Questionnaire item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Questionnaire");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Questionnaire item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Questionnaire");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Questionnaire item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Questionnaire");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
