﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiLastTimeOnSiteProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EbiLastTimeOnSiteProviderBaseCore : EntityViewProviderBase<EbiLastTimeOnSite>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSite&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EbiLastTimeOnSite&gt;"/></returns>
		protected static VList&lt;EbiLastTimeOnSite&gt; Fill(DataSet dataSet, VList<EbiLastTimeOnSite> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EbiLastTimeOnSite>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSite&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EbiLastTimeOnSite>"/></returns>
		protected static VList&lt;EbiLastTimeOnSite&gt; Fill(DataTable dataTable, VList<EbiLastTimeOnSite> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EbiLastTimeOnSite c = new EbiLastTimeOnSite();
					c.EbiId = (Convert.IsDBNull(row["EbiId"]))?(int)0:(System.Int32)row["EbiId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.ClockId = (Convert.IsDBNull(row["ClockId"]))?(int)0:(System.Int32?)row["ClockId"];
					c.FullName = (Convert.IsDBNull(row["FullName"]))?string.Empty:(System.String)row["FullName"];
					c.AccessCardNo = (Convert.IsDBNull(row["AccessCardNo"]))?(int)0:(System.Int32?)row["AccessCardNo"];
					c.SwipeSite = (Convert.IsDBNull(row["SwipeSite"]))?string.Empty:(System.String)row["SwipeSite"];
					c.DataChecked = (Convert.IsDBNull(row["DataChecked"]))?false:(System.Boolean?)row["DataChecked"];
					c.SwipeDateTime = (Convert.IsDBNull(row["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)row["SwipeDateTime"];
					c.SwipeYear = (Convert.IsDBNull(row["SwipeYear"]))?(int)0:(System.Int32)row["SwipeYear"];
					c.SwipeMonth = (Convert.IsDBNull(row["SwipeMonth"]))?(int)0:(System.Int32)row["SwipeMonth"];
					c.SwipeDay = (Convert.IsDBNull(row["SwipeDay"]))?(int)0:(System.Int32)row["SwipeDay"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EbiLastTimeOnSite&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EbiLastTimeOnSite&gt;"/></returns>
		protected VList<EbiLastTimeOnSite> Fill(IDataReader reader, VList<EbiLastTimeOnSite> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EbiLastTimeOnSite entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EbiLastTimeOnSite>("EbiLastTimeOnSite",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EbiLastTimeOnSite();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EbiId = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.EbiId)];
					//entity.EbiId = (Convert.IsDBNull(reader["EbiId"]))?(int)0:(System.Int32)reader["EbiId"];
					entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.ClockId = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.ClockId)))?null:(System.Int32?)reader[((int)EbiLastTimeOnSiteColumn.ClockId)];
					//entity.ClockId = (Convert.IsDBNull(reader["ClockId"]))?(int)0:(System.Int32?)reader["ClockId"];
					entity.FullName = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.FullName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteColumn.FullName)];
					//entity.FullName = (Convert.IsDBNull(reader["FullName"]))?string.Empty:(System.String)reader["FullName"];
					entity.AccessCardNo = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.AccessCardNo)))?null:(System.Int32?)reader[((int)EbiLastTimeOnSiteColumn.AccessCardNo)];
					//entity.AccessCardNo = (Convert.IsDBNull(reader["AccessCardNo"]))?(int)0:(System.Int32?)reader["AccessCardNo"];
					entity.SwipeSite = (System.String)reader[((int)EbiLastTimeOnSiteColumn.SwipeSite)];
					//entity.SwipeSite = (Convert.IsDBNull(reader["SwipeSite"]))?string.Empty:(System.String)reader["SwipeSite"];
					entity.DataChecked = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.DataChecked)))?null:(System.Boolean?)reader[((int)EbiLastTimeOnSiteColumn.DataChecked)];
					//entity.DataChecked = (Convert.IsDBNull(reader["DataChecked"]))?false:(System.Boolean?)reader["DataChecked"];
					entity.SwipeDateTime = (System.DateTime)reader[((int)EbiLastTimeOnSiteColumn.SwipeDateTime)];
					//entity.SwipeDateTime = (Convert.IsDBNull(reader["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)reader["SwipeDateTime"];
					entity.SwipeYear = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeYear)];
					//entity.SwipeYear = (Convert.IsDBNull(reader["SwipeYear"]))?(int)0:(System.Int32)reader["SwipeYear"];
					entity.SwipeMonth = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeMonth)];
					//entity.SwipeMonth = (Convert.IsDBNull(reader["SwipeMonth"]))?(int)0:(System.Int32)reader["SwipeMonth"];
					entity.SwipeDay = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeDay)];
					//entity.SwipeDay = (Convert.IsDBNull(reader["SwipeDay"]))?(int)0:(System.Int32)reader["SwipeDay"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSite"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSite"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EbiLastTimeOnSite entity)
		{
			reader.Read();
			entity.EbiId = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.EbiId)];
			//entity.EbiId = (Convert.IsDBNull(reader["EbiId"]))?(int)0:(System.Int32)reader["EbiId"];
			entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.ClockId = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.ClockId)))?null:(System.Int32?)reader[((int)EbiLastTimeOnSiteColumn.ClockId)];
			//entity.ClockId = (Convert.IsDBNull(reader["ClockId"]))?(int)0:(System.Int32?)reader["ClockId"];
			entity.FullName = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.FullName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteColumn.FullName)];
			//entity.FullName = (Convert.IsDBNull(reader["FullName"]))?string.Empty:(System.String)reader["FullName"];
			entity.AccessCardNo = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.AccessCardNo)))?null:(System.Int32?)reader[((int)EbiLastTimeOnSiteColumn.AccessCardNo)];
			//entity.AccessCardNo = (Convert.IsDBNull(reader["AccessCardNo"]))?(int)0:(System.Int32?)reader["AccessCardNo"];
			entity.SwipeSite = (System.String)reader[((int)EbiLastTimeOnSiteColumn.SwipeSite)];
			//entity.SwipeSite = (Convert.IsDBNull(reader["SwipeSite"]))?string.Empty:(System.String)reader["SwipeSite"];
			entity.DataChecked = (reader.IsDBNull(((int)EbiLastTimeOnSiteColumn.DataChecked)))?null:(System.Boolean?)reader[((int)EbiLastTimeOnSiteColumn.DataChecked)];
			//entity.DataChecked = (Convert.IsDBNull(reader["DataChecked"]))?false:(System.Boolean?)reader["DataChecked"];
			entity.SwipeDateTime = (System.DateTime)reader[((int)EbiLastTimeOnSiteColumn.SwipeDateTime)];
			//entity.SwipeDateTime = (Convert.IsDBNull(reader["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)reader["SwipeDateTime"];
			entity.SwipeYear = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeYear)];
			//entity.SwipeYear = (Convert.IsDBNull(reader["SwipeYear"]))?(int)0:(System.Int32)reader["SwipeYear"];
			entity.SwipeMonth = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeMonth)];
			//entity.SwipeMonth = (Convert.IsDBNull(reader["SwipeMonth"]))?(int)0:(System.Int32)reader["SwipeMonth"];
			entity.SwipeDay = (System.Int32)reader[((int)EbiLastTimeOnSiteColumn.SwipeDay)];
			//entity.SwipeDay = (Convert.IsDBNull(reader["SwipeDay"]))?(int)0:(System.Int32)reader["SwipeDay"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSite"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSite"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EbiLastTimeOnSite entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EbiId = (Convert.IsDBNull(dataRow["EbiId"]))?(int)0:(System.Int32)dataRow["EbiId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.ClockId = (Convert.IsDBNull(dataRow["ClockId"]))?(int)0:(System.Int32?)dataRow["ClockId"];
			entity.FullName = (Convert.IsDBNull(dataRow["FullName"]))?string.Empty:(System.String)dataRow["FullName"];
			entity.AccessCardNo = (Convert.IsDBNull(dataRow["AccessCardNo"]))?(int)0:(System.Int32?)dataRow["AccessCardNo"];
			entity.SwipeSite = (Convert.IsDBNull(dataRow["SwipeSite"]))?string.Empty:(System.String)dataRow["SwipeSite"];
			entity.DataChecked = (Convert.IsDBNull(dataRow["DataChecked"]))?false:(System.Boolean?)dataRow["DataChecked"];
			entity.SwipeDateTime = (Convert.IsDBNull(dataRow["SwipeDateTime"]))?DateTime.MinValue:(System.DateTime)dataRow["SwipeDateTime"];
			entity.SwipeYear = (Convert.IsDBNull(dataRow["SwipeYear"]))?(int)0:(System.Int32)dataRow["SwipeYear"];
			entity.SwipeMonth = (Convert.IsDBNull(dataRow["SwipeMonth"]))?(int)0:(System.Int32)dataRow["SwipeMonth"];
			entity.SwipeDay = (Convert.IsDBNull(dataRow["SwipeDay"]))?(int)0:(System.Int32)dataRow["SwipeDay"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EbiLastTimeOnSiteFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteFilterBuilder : SqlFilterBuilder<EbiLastTimeOnSiteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteFilterBuilder

	#region EbiLastTimeOnSiteParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteParameterBuilder : ParameterizedSqlFilterBuilder<EbiLastTimeOnSiteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteParameterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteParameterBuilder
	
	#region EbiLastTimeOnSiteSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSite"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiLastTimeOnSiteSortBuilder : SqlSortBuilder<EbiLastTimeOnSiteColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteSqlSortBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiLastTimeOnSiteSortBuilder

} // end namespace
