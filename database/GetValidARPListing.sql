

/****** Object:  StoredProcedure [dbo].[GetValidARPListing]    Script Date: 3/08/2015 5:00:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetValidARPListing]                                
(                                
               
 @SiteId int                
)                                
AS                 
              
IF (@SiteId > 0)                           
BEGIN                
              
              
select A.Person_id,A.Full_Name,DATEADD(yy,2, A.COURSE_END_DATE) As Valid_Until, A.op_centre               
,D.COMPANYID,E.SiteId,E.SiteName from HR..XXHR_ARP_CRP_ALL_V AS A                
LEFT OUTER JOIN HR..XXHR_PEOPLE_DETAILS_V AS B                  
ON A.PERSON_ID = B.PERSON_ID                
LEFT OUTER JOIN CompanyVendor AS C                
ON C.VENDOR_NUMBER = B.CWK_VENDOR_NO                 
LEFT OUTER JOIN Companies AS D                
ON D.CompanyAbn = C.TAX_REGISTRATION_NUMBER                
LEFT OUTER JOIN SITES AS E                
ON E.SITENAMEHR=B.LOCATION                 
WHERE A.PER_TYPE_ID = 'E'               
AND DATEADD(yy,2, A.COURSE_END_DATE)> getdate()                
              
AND E.SiteId = @SiteId               
            
--ORDER BY  E.SiteName DESC, A.op_centre DESC, DATEADD(yy,2, A.COURSE_END_DATE) DESC            
              
END                    
ELSE                    
BEGIN               
              
              
              
select A.Person_id,A.Full_Name,DATEADD(yy,2, A.COURSE_END_DATE) As Valid_Until, A.op_centre                
,D.COMPANYID,E.SiteId,E.SiteName from HR..XXHR_ARP_CRP_ALL_V AS A                
LEFT OUTER JOIN HR..XXHR_PEOPLE_DETAILS_V AS B                  
ON A.PERSON_ID = B.PERSON_ID                
LEFT OUTER JOIN CompanyVendor AS C                
ON C.VENDOR_NUMBER = B.CWK_VENDOR_NO                 
LEFT OUTER JOIN Companies AS D                
ON D.CompanyAbn = C.TAX_REGISTRATION_NUMBER                
LEFT OUTER JOIN SITES AS E                
ON E.SITENAMEHR=B.LOCATION                 
WHERE A.PER_TYPE_ID = 'E'                
AND DATEADD(yy,2, A.COURSE_END_DATE)> getdate()                
             
AND               
E.SiteId in (Select Siteid from RegionsSites where RegionId= (-1 * @SiteId))              
              
--ORDER BY  E.SiteName DESC, A.op_centre DESC, DATEADD(yy,2, A.COURSE_END_DATE) DESC            
             
END
----------------------------------------

GO


