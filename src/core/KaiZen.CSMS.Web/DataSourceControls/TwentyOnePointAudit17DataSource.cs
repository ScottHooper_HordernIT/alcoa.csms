﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit17Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit17DataSourceDesigner))]
	public class TwentyOnePointAudit17DataSource : ProviderDataSource<TwentyOnePointAudit17, TwentyOnePointAudit17Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17DataSource class.
		/// </summary>
		public TwentyOnePointAudit17DataSource() : base(new TwentyOnePointAudit17Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit17DataSourceView used by the TwentyOnePointAudit17DataSource.
		/// </summary>
		protected TwentyOnePointAudit17DataSourceView TwentyOnePointAudit17View
		{
			get { return ( View as TwentyOnePointAudit17DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit17DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit17SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit17SelectMethod selectMethod = TwentyOnePointAudit17SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit17SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit17DataSourceView class that is to be
		/// used by the TwentyOnePointAudit17DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit17DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit17, TwentyOnePointAudit17Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit17DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit17DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit17DataSourceView : ProviderDataSourceView<TwentyOnePointAudit17, TwentyOnePointAudit17Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit17DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit17DataSourceView(TwentyOnePointAudit17DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit17DataSource TwentyOnePointAudit17Owner
		{
			get { return Owner as TwentyOnePointAudit17DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit17SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit17Owner.SelectMethod; }
			set { TwentyOnePointAudit17Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit17Service TwentyOnePointAudit17Provider
		{
			get { return Provider as TwentyOnePointAudit17Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit17> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit17> results = null;
			TwentyOnePointAudit17 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit17SelectMethod.Get:
					TwentyOnePointAudit17Key entityKey  = new TwentyOnePointAudit17Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit17Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit17>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit17SelectMethod.GetAll:
                    results = TwentyOnePointAudit17Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit17SelectMethod.GetPaged:
					results = TwentyOnePointAudit17Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit17SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit17Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit17Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit17SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit17Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit17>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit17SelectMethod.Get || SelectMethod == TwentyOnePointAudit17SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit17 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit17Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit17> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit17Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit17DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit17DataSource class.
	/// </summary>
	public class TwentyOnePointAudit17DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit17, TwentyOnePointAudit17Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit17DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit17SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit17DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit17DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit17DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit17DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit17DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit17DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit17DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit17DataSourceActionList(TwentyOnePointAudit17DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit17SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit17DataSourceActionList
	
	#endregion TwentyOnePointAudit17DataSourceDesigner
	
	#region TwentyOnePointAudit17SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit17DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit17SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit17SelectMethod

	#region TwentyOnePointAudit17Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17Filter : SqlFilter<TwentyOnePointAudit17Column>
	{
	}
	
	#endregion TwentyOnePointAudit17Filter

	#region TwentyOnePointAudit17ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit17Column>
	{
	}
	
	#endregion TwentyOnePointAudit17ExpressionBuilder	

	#region TwentyOnePointAudit17Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit17ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit17"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit17Property : ChildEntityProperty<TwentyOnePointAudit17ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit17Property
}

