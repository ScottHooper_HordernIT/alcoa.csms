﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesActiveProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompaniesActiveDataSourceDesigner))]
	public class CompaniesActiveDataSource : ReadOnlyDataSource<CompaniesActive>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveDataSource class.
		/// </summary>
		public CompaniesActiveDataSource() : base(new CompaniesActiveService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesActiveDataSourceView used by the CompaniesActiveDataSource.
		/// </summary>
		protected CompaniesActiveDataSourceView CompaniesActiveView
		{
			get { return ( View as CompaniesActiveDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesActiveDataSourceView class that is to be
		/// used by the CompaniesActiveDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesActiveDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesActive, Object> GetNewDataSourceView()
		{
			return new CompaniesActiveDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesActiveDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesActiveDataSourceView : ReadOnlyDataSourceView<CompaniesActive>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesActiveDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesActiveDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesActiveDataSourceView(CompaniesActiveDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesActiveDataSource CompaniesActiveOwner
		{
			get { return Owner as CompaniesActiveDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesActiveService CompaniesActiveProvider
		{
			get { return Provider as CompaniesActiveService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompaniesActiveDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesActiveDataSource class.
	/// </summary>
	public class CompaniesActiveDataSourceDesigner : ReadOnlyDataSourceDesigner<CompaniesActive>
	{
	}

	#endregion CompaniesActiveDataSourceDesigner

	#region CompaniesActiveFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveFilter : SqlFilter<CompaniesActiveColumn>
	{
	}

	#endregion CompaniesActiveFilter

	#region CompaniesActiveExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesActive"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesActiveExpressionBuilder : SqlExpressionBuilder<CompaniesActiveColumn>
	{
	}
	
	#endregion CompaniesActiveExpressionBuilder		
}

