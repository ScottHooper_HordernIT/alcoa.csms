﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnairePresentlyWithMetricService
    {
        QuestionnairePresentlyWithMetric Get(Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes);
        List<QuestionnairePresentlyWithMetric> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, Func<IQueryable<QuestionnairePresentlyWithMetric>, IOrderedQueryable<QuestionnairePresentlyWithMetric>> orderBy, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes);
        List<QuestionnairePresentlyWithMetric> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, Func<IQueryable<QuestionnairePresentlyWithMetric>, IOrderedQueryable<QuestionnairePresentlyWithMetric>> orderBy, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes);
        void Insert(QuestionnairePresentlyWithMetric item, bool saveChanges = true);
        void Update(QuestionnairePresentlyWithMetric item, bool saveChanges = true);
        void Delete(QuestionnairePresentlyWithMetric item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnairePresentlyWithMetricService : IQuestionnairePresentlyWithMetricService
    {
        private readonly IRepository<QuestionnairePresentlyWithMetric> _repository;

        public QuestionnairePresentlyWithMetricService(IRepository<QuestionnairePresentlyWithMetric> repository)
        {
            this._repository = repository;
        }

        public QuestionnairePresentlyWithMetric Get(Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnairePresentlyWithMetric> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, Func<IQueryable<QuestionnairePresentlyWithMetric>, IOrderedQueryable<QuestionnairePresentlyWithMetric>> orderBy, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnairePresentlyWithMetric> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, Func<IQueryable<QuestionnairePresentlyWithMetric>, IOrderedQueryable<QuestionnairePresentlyWithMetric>> orderBy, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnairePresentlyWithMetric, bool>> where, List<Expression<Func<QuestionnairePresentlyWithMetric, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnairePresentlyWithMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithMetric");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnairePresentlyWithMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithMetric");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnairePresentlyWithMetric item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithMetric");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
