﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailTemplateListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(AdminTaskEmailTemplateListDataSourceDesigner))]
	public class AdminTaskEmailTemplateListDataSource : ReadOnlyDataSource<AdminTaskEmailTemplateList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListDataSource class.
		/// </summary>
		public AdminTaskEmailTemplateListDataSource() : base(new AdminTaskEmailTemplateListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailTemplateListDataSourceView used by the AdminTaskEmailTemplateListDataSource.
		/// </summary>
		protected AdminTaskEmailTemplateListDataSourceView AdminTaskEmailTemplateListView
		{
			get { return ( View as AdminTaskEmailTemplateListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailTemplateListDataSourceView class that is to be
		/// used by the AdminTaskEmailTemplateListDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailTemplateListDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailTemplateList, Object> GetNewDataSourceView()
		{
			return new AdminTaskEmailTemplateListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailTemplateListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailTemplateListDataSourceView : ReadOnlyDataSourceView<AdminTaskEmailTemplateList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailTemplateListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailTemplateListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailTemplateListDataSourceView(AdminTaskEmailTemplateListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailTemplateListDataSource AdminTaskEmailTemplateListOwner
		{
			get { return Owner as AdminTaskEmailTemplateListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailTemplateListService AdminTaskEmailTemplateListProvider
		{
			get { return Provider as AdminTaskEmailTemplateListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region AdminTaskEmailTemplateListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailTemplateListDataSource class.
	/// </summary>
	public class AdminTaskEmailTemplateListDataSourceDesigner : ReadOnlyDataSourceDesigner<AdminTaskEmailTemplateList>
	{
	}

	#endregion AdminTaskEmailTemplateListDataSourceDesigner

	#region AdminTaskEmailTemplateListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListFilter : SqlFilter<AdminTaskEmailTemplateListColumn>
	{
	}

	#endregion AdminTaskEmailTemplateListFilter

	#region AdminTaskEmailTemplateListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailTemplateList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailTemplateListExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailTemplateListColumn>
	{
	}
	
	#endregion AdminTaskEmailTemplateListExpressionBuilder		
}

