using DevExpress.Web.Data;
using KaiZen.CSMS.Entities;
using System;
using System.Web.UI;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class HsAssessorEscalation : System.Web.UI.UserControl
    {
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            UsersDataSource.SelectCommand = @"SELECT distinct u.UserId, u.LastName + ', ' + u.FirstName + ' (' + u.UserLogon + ') (' + c.CompanyName + ')' As UserDetails, u.LastName
                                              FROM dbo.Users as u INNER JOIN dbo.Companies as c ON u.CompanyId = c.CompanyID
                                              WHERE (u.UserId in (select UserId from dbo.EHSConsultant)) or (u.RoleId < 4)
                                              ORDER BY u.LastName";
            
            if (auth.RoleId != (int)RoleList.Administrator)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                if (!postBack)
                { //first time load
                }
            }
            Helper.ExportGrid.Settings(ucExportButtons, "H&S Assessors", "gridHsAssessorsNew");
        }

        protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
        {
            if(e.NewValues["Enabled"] == null)
            { 
                e.NewValues["Enabled"] = true;
            }
        }
    }
}
