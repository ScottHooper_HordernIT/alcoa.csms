﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEscalationChainProcurementService
    {
        EscalationChainProcurement Get(Expression<Func<EscalationChainProcurement, bool>> where, List<Expression<Func<EscalationChainProcurement, object>>> includes);
        List<EscalationChainProcurement> GetMany(int? top, Expression<Func<EscalationChainProcurement, bool>> where, Func<IQueryable<EscalationChainProcurement>, IOrderedQueryable<EscalationChainProcurement>> orderBy, List<Expression<Func<EscalationChainProcurement, object>>> includes);
        List<EscalationChainProcurement> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainProcurement, bool>> where, Func<IQueryable<EscalationChainProcurement>, IOrderedQueryable<EscalationChainProcurement>> orderBy, List<Expression<Func<EscalationChainProcurement, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EscalationChainProcurement, bool>> where, List<Expression<Func<EscalationChainProcurement, object>>> includes);
        void Insert(EscalationChainProcurement item, bool saveChanges = true);
        void Update(EscalationChainProcurement item, bool saveChanges = true);
        void Delete(EscalationChainProcurement item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EscalationChainProcurementService : IEscalationChainProcurementService
    {
        private readonly IRepository<EscalationChainProcurement> _repository;

        public EscalationChainProcurementService(IRepository<EscalationChainProcurement> repository)
        {
            this._repository = repository;
        }

        public EscalationChainProcurement Get(Expression<Func<EscalationChainProcurement, bool>> where, List<Expression<Func<EscalationChainProcurement, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EscalationChainProcurement> GetMany(int? top, Expression<Func<EscalationChainProcurement, bool>> where, Func<IQueryable<EscalationChainProcurement>, IOrderedQueryable<EscalationChainProcurement>> orderBy, List<Expression<Func<EscalationChainProcurement, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EscalationChainProcurement> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EscalationChainProcurement, bool>> where, Func<IQueryable<EscalationChainProcurement>, IOrderedQueryable<EscalationChainProcurement>> orderBy, List<Expression<Func<EscalationChainProcurement, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EscalationChainProcurement, bool>> where, List<Expression<Func<EscalationChainProcurement, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EscalationChainProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurement");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EscalationChainProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurement");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EscalationChainProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EscalationChainProcurement");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
