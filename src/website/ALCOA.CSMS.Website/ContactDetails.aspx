﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" CodeBehind="ContactDetails.aspx.cs" Inherits="ALCOA.CSMS.Website.ContactDetails" %>

<%@ Register src="UserControls/Main/ContactDetails.ascx" tagname="ContactDetails" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%: System.Web.Optimization.Styles.Render("~/bundles/Office2003BlueOld_styles") %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<uc1:ContactDetails ID="ContactDetails1" runat="server" />
</asp:Content>
