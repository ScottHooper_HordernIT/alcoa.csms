﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompanyStatus2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CompanyStatus2DataSourceDesigner))]
	public class CompanyStatus2DataSource : ProviderDataSource<CompanyStatus2, CompanyStatus2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2DataSource class.
		/// </summary>
		public CompanyStatus2DataSource() : base(new CompanyStatus2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompanyStatus2DataSourceView used by the CompanyStatus2DataSource.
		/// </summary>
		protected CompanyStatus2DataSourceView CompanyStatus2View
		{
			get { return ( View as CompanyStatus2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CompanyStatus2DataSource control invokes to retrieve data.
		/// </summary>
		public CompanyStatus2SelectMethod SelectMethod
		{
			get
			{
				CompanyStatus2SelectMethod selectMethod = CompanyStatus2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CompanyStatus2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompanyStatus2DataSourceView class that is to be
		/// used by the CompanyStatus2DataSource.
		/// </summary>
		/// <returns>An instance of the CompanyStatus2DataSourceView class.</returns>
		protected override BaseDataSourceView<CompanyStatus2, CompanyStatus2Key> GetNewDataSourceView()
		{
			return new CompanyStatus2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompanyStatus2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompanyStatus2DataSourceView : ProviderDataSourceView<CompanyStatus2, CompanyStatus2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompanyStatus2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompanyStatus2DataSourceView(CompanyStatus2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompanyStatus2DataSource CompanyStatus2Owner
		{
			get { return Owner as CompanyStatus2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CompanyStatus2SelectMethod SelectMethod
		{
			get { return CompanyStatus2Owner.SelectMethod; }
			set { CompanyStatus2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompanyStatus2Service CompanyStatus2Provider
		{
			get { return Provider as CompanyStatus2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CompanyStatus2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CompanyStatus2> results = null;
			CompanyStatus2 item;
			count = 0;
			
			System.Int32 _companyStatusId;
			System.String _companyStatusName;

			switch ( SelectMethod )
			{
				case CompanyStatus2SelectMethod.Get:
					CompanyStatus2Key entityKey  = new CompanyStatus2Key();
					entityKey.Load(values);
					item = CompanyStatus2Provider.Get(entityKey);
					results = new TList<CompanyStatus2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CompanyStatus2SelectMethod.GetAll:
                    results = CompanyStatus2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CompanyStatus2SelectMethod.GetPaged:
					results = CompanyStatus2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CompanyStatus2SelectMethod.Find:
					if ( FilterParameters != null )
						results = CompanyStatus2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CompanyStatus2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CompanyStatus2SelectMethod.GetByCompanyStatusId:
					_companyStatusId = ( values["CompanyStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyStatusId"], typeof(System.Int32)) : (int)0;
					item = CompanyStatus2Provider.GetByCompanyStatusId(_companyStatusId);
					results = new TList<CompanyStatus2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CompanyStatus2SelectMethod.GetByCompanyStatusName:
					_companyStatusName = ( values["CompanyStatusName"] != null ) ? (System.String) EntityUtil.ChangeType(values["CompanyStatusName"], typeof(System.String)) : string.Empty;
					item = CompanyStatus2Provider.GetByCompanyStatusName(_companyStatusName);
					results = new TList<CompanyStatus2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CompanyStatus2SelectMethod.Get || SelectMethod == CompanyStatus2SelectMethod.GetByCompanyStatusId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CompanyStatus2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CompanyStatus2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CompanyStatus2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CompanyStatus2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CompanyStatus2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompanyStatus2DataSource class.
	/// </summary>
	public class CompanyStatus2DataSourceDesigner : ProviderDataSourceDesigner<CompanyStatus2, CompanyStatus2Key>
	{
		/// <summary>
		/// Initializes a new instance of the CompanyStatus2DataSourceDesigner class.
		/// </summary>
		public CompanyStatus2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyStatus2SelectMethod SelectMethod
		{
			get { return ((CompanyStatus2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CompanyStatus2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CompanyStatus2DataSourceActionList

	/// <summary>
	/// Supports the CompanyStatus2DataSourceDesigner class.
	/// </summary>
	internal class CompanyStatus2DataSourceActionList : DesignerActionList
	{
		private CompanyStatus2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CompanyStatus2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CompanyStatus2DataSourceActionList(CompanyStatus2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CompanyStatus2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CompanyStatus2DataSourceActionList
	
	#endregion CompanyStatus2DataSourceDesigner
	
	#region CompanyStatus2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CompanyStatus2DataSource.SelectMethod property.
	/// </summary>
	public enum CompanyStatus2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCompanyStatusId method.
		/// </summary>
		GetByCompanyStatusId,
		/// <summary>
		/// Represents the GetByCompanyStatusName method.
		/// </summary>
		GetByCompanyStatusName
	}
	
	#endregion CompanyStatus2SelectMethod

	#region CompanyStatus2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2Filter : SqlFilter<CompanyStatus2Column>
	{
	}
	
	#endregion CompanyStatus2Filter

	#region CompanyStatus2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2ExpressionBuilder : SqlExpressionBuilder<CompanyStatus2Column>
	{
	}
	
	#endregion CompanyStatus2ExpressionBuilder	

	#region CompanyStatus2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CompanyStatus2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CompanyStatus2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompanyStatus2Property : ChildEntityProperty<CompanyStatus2ChildEntityTypes>
	{
	}
	
	#endregion CompanyStatus2Property
}

