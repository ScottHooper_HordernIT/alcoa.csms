﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.IO;
using System.Security;
using System.Security.Principal;

using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;

public partial class Common_GetFile : System.Web.UI.Page
{
    Auth auth = new Auth();
    bool forceDownload = true;
    int UserId;
    string ApssType;
    repo.IS812EvidenceFileService s812EvidenceFileService = ALCOA.CSMS.Website.Global.GetInstance<repo.IS812EvidenceFileService>();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            try
            {
                string aPath = "";
                string aFile = "";
                string type = "";
                string File = "";
                if (Request.QueryString["Type"] != null && Request.QueryString["File"] != null)
                {
                    Configuration configuration = new Configuration();
                    type = Request.QueryString["Type"];
                    File = Request.QueryString["File"];
                    string RawFileName = "";
                    string FilePath = "";

                    try
                    {
                        UserId = Convert.ToInt32(auth.UserId);
                    }
                    catch
                    {
                        throw new Exception("Access Denied. Has your login session timed out? Ensure you are logged in (Refresh Main Page).");
                    }

                    switch (type)
                    {
                        case "SQ":
                            if (Request.QueryString["SubType"] != null)
                            {
                                SendSafetyQuestionnaireFile(Convert.ToInt32(File), Request.QueryString["SubType"]);
                            }
                            break;
                        case "APSS":
                            FilePath = @configuration.GetValue(ConfigList.APSSDir);
                            if (Request.QueryString["Region"] != null)
                            {
                                if (Request.QueryString["Region"] == "VIC")
                                {
                                    FilePath = @configuration.GetValue(ConfigList.VicOpsDocsDir);
                                }
                            }
                            if (Request.QueryString["SubType"] != null)
                            {
                                ApssType = Request.QueryString["SubType"];
                            }
                            RawFileName = File;
                            break;
                        case "CSMS":
                            if (Request.QueryString["SubType"] != null)
                            {
                                switch (Request.QueryString["SubType"])
                                {
                                    case "TrainingQuestionnaire":
                                        SendQuestionnaireFile(Convert.ToInt32(File), "Questionnaire");
                                        break;
                                    case "TrainingQuestionnaireAnswers":
                                        SendQuestionnaireFile(Convert.ToInt32(File), "Answers");
                                        break;
                                    case "TrainingUploads":
                                        SendFileDbFile(Convert.ToInt32(File), "Training");
                                        break;
                                    case "MedicalUploads":
                                        SendFileDbFile(Convert.ToInt32(File), "Medical");
                                        break;
                                    case "SafetyPlans":
                                        SendFileDbFile(Convert.ToInt32(File), "SafetyPlans");
                                        break;
                                    default:
                                        break;
                                }
                            }
                            break;
                        case "CSMSFiles":
                            SendCsmsFile(Convert.ToInt32(File));
                            break;
                        case "Help":
                            //FilePath = @"\\aua.alcoa.com\dfs\Bgn\Teams\Contractor Management\17 CSMS Production Operational Files NEVER DELETE\CSMS Help Files\";
                            FilePath = configuration.GetValue(ConfigList.HelpFilesFilePath);
                            switch (File)
                            {
                                case "QuickReferenceKpi":
                                    //RawFileName = "WAOCSM_Help_QuickReferenceGuide.pdf";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileQuickReferenceKpi);
                                    break;
                                case "LoginAlcoaDirect":
                                    //RawFileName = "WAOCSM_Help_QuickReferenceGuide_AlcoaDirect_Login.pdf";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileLoginAlcoaDirect);
                                    break;
                                case "SubmitSmp":
                                    //RawFileName = "WAOCSM_Help_QuickReferenceGuide_SMP.pdf";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileSubmitSmp);
                                    break;
                                case "CompleteCsa":
                                    //RawFileName = "WAOCSM_Help_QuickReferenceGuide_CSA.pdf";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileCompleteCsa);
                                    break;
                                case "CompleteSq":
                                    //RawFileName = "WAOCSM_Help_QuickReferenceGuide_SRQ.pdf";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileCompleteSq);
                                    break;
                                case "KpiEmbedded":
                                    //RawFileName = "KPI for Embedded and Non Embedded 1 2 3.xls";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileKpiEmbedded);
                                    break;
                                case "Sa812Evidence":
                                    //RawFileName = "KPI for Embedded and Non Embedded 1 2 3.xls";
                                    RawFileName = configuration.GetValue(ConfigList.HelpFileS812Evidence);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "Templates":
                            //  FilePath = @configuration.GetValue(ConfigList.TemplatesDir);
                            switch (File)
                            {
                                case "cContactsInfo":
                                    RawFileName = configuration.GetValue(ConfigList.ContractorContactTemplateFilePath);  //Added by Jolly for Spec # 72
                                    break;
                                case "Medical":
                                    RawFileName = configuration.GetValue(ConfigList.MedicalTemplateFileName);
                                    break;
                                case "MedicalVicOps":
                                    RawFileName = configuration.GetValue(ConfigList.MedicalTemplateFileName_VICOPS);
                                    break;
                                case "Training":
                                    RawFileName = configuration.GetValue(ConfigList.TrainingTemplateFileName);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "S812CustomFile":
                            //Pass id in - here is server call
                            string id = Request.QueryString["Id"];
                            int intId = Int32.Parse(id);
                            model.S812EvidenceFile ef = s812EvidenceFileService.Get(i => i.S812EvidenceFileId == intId, null);
                            byte[] fileContent = ef.Content;
                            BuildFileResponse(ef.FileName, fileContent, "S812Custom");

                            break;
                        default:
                            break;
                    }
                    if (type == "APSS" || type == "Help" || type == "Templates")
                    {
                        if (!String.IsNullOrEmpty(RawFileName))
                        {
                            SendFileViaProxy(RawFileName, FilePath + RawFileName, type);
                        }
                        else
                        {
                            //throw new Exception("Invalid Request.");
                            Exception ex1 = new Exception("1.File: " + RawFileName + "1.FilePath: " + FilePath); //added by Debashis for error 'File Not Exist'
                            Elmah.ErrorSignal.FromContext(Context).Raise(ex1);
                        }
                    }

                    // TODO !####! this should not be here - this is NOT an error condition it is a successful download
                    Exception ex2 = new Exception("2.File: " + RawFileName + "2.FilePath: " + FilePath); //added by Debashis for error 'File Not Exist'
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex2);

                }
                else
                {
                    //throw new Exception("Invalid Request.");
                    Exception ex3 = new Exception("3.FileName: " + aFile + "3.FilePath: " + aPath + "3.File : " + File + "3.Type: " + type); //added by Debashis for error 'File Not Exist'
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex3);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                if (ex.Message == "Object reference not set to an instance of an object.")
                {
                    Response.Write("Invalid File");
                }
                else
                {
                    Response.Write(ex.Message);
                }
            }
            finally
            {
                //Response.End(); DT439 05/01/2015 Cindi Thornton - not recommended by MS, causes issue in IE.
            }
        }
        else
        {
            if (Request.UrlReferrer != null)
            {
                Response.Redirect(Request.UrlReferrer.AbsoluteUri + "?error=accessdenied");
            }
            else
            {
                Response.Redirect("../AccessDenied.aspx?error=" + Server.UrlEncode("Access Denied: You do not have permission to open/download this file."));
            }
        }
    }

    protected void BuildFileResponse(string filename, byte[] byteArray, string type)
    {
        //Response.ContentType = "application/download";
        Response.ContentType = "application/octet-stream";
        string ContentDisposition;
        String userAgent = Request.Headers.Get("User-Agent");
        if (userAgent.Contains("MSIE 7.0"))
        {
            ContentDisposition = String.Format(@"attachment; filename={0}", filename.Replace(" ", "%20"));
        }
        else
        {
            ContentDisposition = String.Format("attachment; filename=\"{0}\"", filename);
        }
        Response.AppendHeader("Content-Disposition", ContentDisposition);
        Response.AppendHeader("Content-Length", byteArray.Length.ToString());

        Stream byteStream = new MemoryStream(byteArray);
        byteStream.CopyTo(Response.OutputStream);
        byteStream.Close();
        LogTransaction(filename, type);
        Response.Flush();
        //Response.Close(); DT439 05/01/2015 Cindi Thornton - not recommended by MS, causes issue in IE.

    }

    protected void SendFileViaProxy(string filename, string filepath, string type)
    {
        const int BufferLen = 65536;       

        WindowsImpersonationContext impContext = null;
        try
        {
            impContext = Helper.ImpersonateWAOCSMUser();
        }
        catch (ApplicationException ex)
        {
            Trace.Write(ex.Message);
        }

        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    Stream iStream = null;

                    // Buffer to read in chunks:
                    byte[] buffer = new Byte[BufferLen];

                    // Length of the file:
                    int length;

                    // Total bytes to read:
                    long dataToRead;

                    Response.Clear();
                    try
                    {
                        // Open the file.
                        iStream = new FileStream(filepath, FileMode.Open,
                                                 FileAccess.Read, FileShare.Read);

                        // Total bytes to read:
                        dataToRead = iStream.Length;
                        if (forceDownload)
                        {
                            Response.ContentType = "application/download";
                        }
                        else
                        {
                            Response.ContentType = "application/octet-stream";
                        }
                        string FileName = filename;
                        string ContentDisposition;
                        String userAgent = Request.Headers.Get("User-Agent");
                        if (userAgent.Contains("MSIE 7.0"))
                        {
                            ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                        }
                        else
                        {
                            ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                        }
                        Response.AppendHeader("Content-Disposition", ContentDisposition);
                        Response.AppendHeader("Content-Length", dataToRead.ToString());

                        // Read the bytes.
                        while (dataToRead > 0)
                        {
                            // Verify that the client is connected.
                            if (Response.IsClientConnected)
                            {
                                // Read the data in buffer.
                                length = iStream.Read(buffer, 0, BufferLen);

                                // Write the data to the current output stream.
                                Response.OutputStream.Write(buffer, 0, length);

                                // Flush the data to the HTML output.
                                Response.Flush();

                                dataToRead = dataToRead - length;
                            }
                            else
                            {
                                //prevent infinite loop if user disconnects
                                dataToRead = -1;
                            }
                        }
                    }
                    finally
                    {
                        if (iStream != null)
                        {
                            //log the transaction
                            LogTransaction(filename, type);
                            //Close the file.
                            iStream.Close();
                            //Response.Close(); DT439 05/01/2015 Cindi Thornton - not recommended by MS, causes issue in IE.
                            Response.Flush();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Error: " + ex.Message); //TODO: Friendly Error Message Handling.
        }
    }

    protected void LogTransaction(string filename, string type)
    {
        if (type == "APSS")
        {
            try
            {
                DocumentsDownloadLogService documentsDownloadLogService = new DocumentsDownloadLogService();
                using (DocumentsDownloadLog documentsDownloadLog = new DocumentsDownloadLog())
                {
                    documentsDownloadLog.DocumentFileName = filename;
                    documentsDownloadLog.DownloadedByUserId = UserId;
                    documentsDownloadLog.DownloadedFrom = ApssType;
                    documentsDownloadLog.AuditedOn = DateTime.Now;
                    documentsDownloadLogService.Save(documentsDownloadLog);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                //Todo: Handle exception
            }
        }
    } //APSS
    protected void LogTransaction(int fileId, int companyId, string fileName) //Safety Plans
    {
        try
        {
            FileDbAuditService fileDbAuditService = new FileDbAuditService();
            using (FileDbAudit fileDbAudit = new FileDbAudit())
            {
                fileDbAudit.FileId = fileId;
                fileDbAudit.CompanyId = companyId;
                fileDbAudit.FileName = fileName;
                fileDbAudit.AuditedOn = DateTime.Now;
                fileDbAudit.AuditEventId = "V";
                fileDbAudit.ModifiedByUserId = UserId;
                fileDbAuditService.Save(fileDbAudit);
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            //Todo: Handle exception
        }
    }
    protected void SendQuestionnaireFile(int Id, string type)
    {
        DocumentsService dService = new DocumentsService();
        Documents d = dService.GetByDocumentId(Id);

        if (type == "Questionnaire")
        {
            Response.Clear();
            if (!String.IsNullOrEmpty(d.UploadedFileName))
            {
                Response.AppendHeader("Content-Type", "application/download");

                string FileName = d.UploadedFileName;
                string ContentDisposition;
                String userAgent = Request.Headers.Get("User-Agent");
                if (userAgent.Contains("MSIE 7.0"))
                {
                    ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                }
                else
                {
                    ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                }
                Response.AppendHeader("Content-Disposition", ContentDisposition);
                Response.AppendHeader("Content-Length", d.UploadedFile.Length.ToString());

                //Response.BinaryWrite(d.UploadedFile);
                Response.OutputStream.Write(d.UploadedFile, 0, d.UploadedFile.Length);
                //Response.End();
            }
            else
            {
                Response.Write("File Does not Exist");
            }
            Response.Flush();
        }
        else if (type == "Answers")
        {
            Response.Clear();
            if (!String.IsNullOrEmpty(d.UploadedFileName2))
            {
                Response.AppendHeader("Content-Type", "application/download");

                string FileName = d.UploadedFileName2;
                string ContentDisposition;
                String userAgent = Request.Headers.Get("User-Agent");
                if (userAgent.Contains("MSIE 7.0"))
                {
                    ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                }
                else
                {
                    ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                }
                Response.AppendHeader("Content-Disposition", ContentDisposition);
                Response.AppendHeader("Content-Length", d.UploadedFile2.Length.ToString());

                //Response.BinaryWrite(d.UploadedFile2);
                Response.OutputStream.Write(d.UploadedFile2, 0, d.UploadedFile2.Length);
                //Response.End();
            }
            else
            {
                Response.Write("File Does not Exist");
            }
            Response.Flush();
        }
    }
    protected void SendSafetyQuestionnaireFile(int Id, string type)
    {
        Response.Clear();
        try
        {
            string FileName = "";
            byte[] Content = null;
            int ContentLength = 0;
            if (type == "Supplier")
            {
                QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
                QuestionnaireMainAttachment qa = qas.GetByAttachmentId(Id);
                FileName = qa.FileName;
                Content = qa.Content;
                ContentLength = qa.ContentLength;
            }
            if (type == "Verification")
            {
                QuestionnaireVerificationAttachmentService qas = new QuestionnaireVerificationAttachmentService();
                QuestionnaireVerificationAttachment qa = qas.GetByAttachmentId(Id);
                FileName = qa.FileName;
                Content = qa.Content;
                ContentLength = qa.ContentLength;
            }
            if (ContentLength == 0 || Content == null || String.IsNullOrEmpty(FileName)) throw new Exception("Invalid File.");

            if (forceDownload)
            {
                Response.AppendHeader("Content-Type", "application/download");

                string ContentDisposition;
                String userAgent = Request.Headers.Get("User-Agent");
                if (userAgent.Contains("MSIE 7.0"))
                {
                    ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                }
                else
                {
                    ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                }

                Response.AppendHeader("Content-Disposition", ContentDisposition);
            }
            else
            {
                Response.AppendHeader("Content-Disposition", String.Format("filename={0}", FileName));
            }
            Response.AppendHeader("Content-Length", ContentLength.ToString());
            Response.OutputStream.Write(Content, 0, ContentLength);
            //Response.End();
            Response.Flush();
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            if (ex.Message == "Object reference not set to an instance of an object.")
            {
                Response.Write("Invalid File");
                return;
            }
            Response.Write(ex.Message);
        }
    }
    protected void SendFileDbFile(int fileID, string subtype)
    {
        if (subtype == "SafetyPlans")
        {
            FileDbService fileDbService = new FileDbService();
            FileDb fileDb = new FileDb();
            fileDb = fileDbService.GetByFileId(fileID);

            LogTransaction(fileDb.FileId, fileDb.CompanyId, fileDb.FileName);

            Response.AppendHeader("Content-Type", "application/download");

            string FileName = fileDb.FileName;
            string ContentDisposition;
            String userAgent = Request.Headers.Get("User-Agent");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
            }
            else
            {
                ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
            }
            Response.AppendHeader("Content-Disposition", ContentDisposition);
            Response.AppendHeader("Content-Length", fileDb.ContentLength.ToString());

            Response.OutputStream.Write(fileDb.Content, 0, fileDb.ContentLength);
            //Response.End();
            Response.Flush();
        }
        else
        {
            if (subtype == "Training" || subtype == "Medical")
            {
                FileDbMedicalTrainingService fileDbMedicalTrainingService = new FileDbMedicalTrainingService();
                FileDbMedicalTraining fileDbMedicalTraining = new FileDbMedicalTraining();
                fileDbMedicalTraining = fileDbMedicalTrainingService.GetByFileId(fileID);

                Response.AppendHeader("Content-Type", "application/download");

                string FileName = fileDbMedicalTraining.FileName;
                string ContentDisposition;
                String userAgent = Request.Headers.Get("User-Agent");
                if (userAgent.Contains("MSIE 7.0"))
                {
                    ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
                }
                else
                {
                    ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
                }
                Response.AppendHeader("Content-Disposition", ContentDisposition);
                Response.AppendHeader("Content-Length", fileDbMedicalTraining.ContentLength.ToString());

                Response.OutputStream.Write(fileDbMedicalTraining.Content, 0, fileDbMedicalTraining.ContentLength);
                //Response.End();
                Response.Flush();
            }
        }
    }


    protected void SendCsmsFile(int fileID)
    {
        CsmsFileService cfService = new CsmsFileService();
        CsmsFile cf = new CsmsFile();
        cf = cfService.GetByCsmsFileId(fileID);

        // TODO !####! why is this commented out
        //LogTransaction(fileDb.FileId, fileDb.CompanyId, fileDb.FileName);

        Response.AppendHeader("Content-Type", "application/download");

        string FileName = cf.FileName;
        string ContentDisposition;
        String userAgent = Request.Headers.Get("User-Agent");
        if (userAgent.Contains("MSIE 7.0"))
        {
            ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
        }
        else
        {
            ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
        }
        Response.AppendHeader("Content-Disposition", ContentDisposition);
        Response.AppendHeader("Content-Length", cf.ContentLength.ToString());

        Response.OutputStream.Write(cf.Content, 0, cf.ContentLength);
        //Response.End();
        Response.Flush();
    }
}
