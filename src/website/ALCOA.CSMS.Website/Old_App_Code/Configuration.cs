using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

/// <summary>
/// Summary description for Configuration
/// </summary>
public class Configuration
{
    public Configuration()
    {
    }

    public string GetValue(ConfigList Key)
    {
        ConfigService configService = new ConfigService();
        TList<Config> configList = configService.GetAll(); //All KPI
        TList<Config> configList2 = configList.FindAll( //Find KPI We Need
            delegate(Config config)
            {
                return
                    config.Key == Key.ToString();
            }
        );

        return configList2[0].Value.ToString();
    }
}
