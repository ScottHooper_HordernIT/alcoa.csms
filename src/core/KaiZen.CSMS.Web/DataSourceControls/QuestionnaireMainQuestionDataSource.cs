﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireMainQuestionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireMainQuestionDataSourceDesigner))]
	public class QuestionnaireMainQuestionDataSource : ProviderDataSource<QuestionnaireMainQuestion, QuestionnaireMainQuestionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionDataSource class.
		/// </summary>
		public QuestionnaireMainQuestionDataSource() : base(new QuestionnaireMainQuestionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireMainQuestionDataSourceView used by the QuestionnaireMainQuestionDataSource.
		/// </summary>
		protected QuestionnaireMainQuestionDataSourceView QuestionnaireMainQuestionView
		{
			get { return ( View as QuestionnaireMainQuestionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireMainQuestionDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireMainQuestionSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireMainQuestionSelectMethod selectMethod = QuestionnaireMainQuestionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireMainQuestionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireMainQuestionDataSourceView class that is to be
		/// used by the QuestionnaireMainQuestionDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireMainQuestionDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireMainQuestion, QuestionnaireMainQuestionKey> GetNewDataSourceView()
		{
			return new QuestionnaireMainQuestionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireMainQuestionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireMainQuestionDataSourceView : ProviderDataSourceView<QuestionnaireMainQuestion, QuestionnaireMainQuestionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireMainQuestionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireMainQuestionDataSourceView(QuestionnaireMainQuestionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireMainQuestionDataSource QuestionnaireMainQuestionOwner
		{
			get { return Owner as QuestionnaireMainQuestionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireMainQuestionSelectMethod SelectMethod
		{
			get { return QuestionnaireMainQuestionOwner.SelectMethod; }
			set { QuestionnaireMainQuestionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireMainQuestionService QuestionnaireMainQuestionProvider
		{
			get { return Provider as QuestionnaireMainQuestionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireMainQuestion> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireMainQuestion> results = null;
			QuestionnaireMainQuestion item;
			count = 0;
			
			System.String _questionNo;

			switch ( SelectMethod )
			{
				case QuestionnaireMainQuestionSelectMethod.Get:
					QuestionnaireMainQuestionKey entityKey  = new QuestionnaireMainQuestionKey();
					entityKey.Load(values);
					item = QuestionnaireMainQuestionProvider.Get(entityKey);
					results = new TList<QuestionnaireMainQuestion>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireMainQuestionSelectMethod.GetAll:
                    results = QuestionnaireMainQuestionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireMainQuestionSelectMethod.GetPaged:
					results = QuestionnaireMainQuestionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireMainQuestionSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireMainQuestionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireMainQuestionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireMainQuestionSelectMethod.GetByQuestionNo:
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					item = QuestionnaireMainQuestionProvider.GetByQuestionNo(_questionNo);
					results = new TList<QuestionnaireMainQuestion>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireMainQuestionSelectMethod.Get || SelectMethod == QuestionnaireMainQuestionSelectMethod.GetByQuestionNo )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireMainQuestion entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireMainQuestionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireMainQuestion> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireMainQuestionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireMainQuestionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireMainQuestionDataSource class.
	/// </summary>
	public class QuestionnaireMainQuestionDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireMainQuestion, QuestionnaireMainQuestionKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionDataSourceDesigner class.
		/// </summary>
		public QuestionnaireMainQuestionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainQuestionSelectMethod SelectMethod
		{
			get { return ((QuestionnaireMainQuestionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireMainQuestionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireMainQuestionDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireMainQuestionDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireMainQuestionDataSourceActionList : DesignerActionList
	{
		private QuestionnaireMainQuestionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainQuestionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireMainQuestionDataSourceActionList(QuestionnaireMainQuestionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireMainQuestionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireMainQuestionDataSourceActionList
	
	#endregion QuestionnaireMainQuestionDataSourceDesigner
	
	#region QuestionnaireMainQuestionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireMainQuestionDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireMainQuestionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionNo method.
		/// </summary>
		GetByQuestionNo
	}
	
	#endregion QuestionnaireMainQuestionSelectMethod

	#region QuestionnaireMainQuestionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionFilter : SqlFilter<QuestionnaireMainQuestionColumn>
	{
	}
	
	#endregion QuestionnaireMainQuestionFilter

	#region QuestionnaireMainQuestionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionExpressionBuilder : SqlExpressionBuilder<QuestionnaireMainQuestionColumn>
	{
	}
	
	#endregion QuestionnaireMainQuestionExpressionBuilder	

	#region QuestionnaireMainQuestionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireMainQuestionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainQuestionProperty : ChildEntityProperty<QuestionnaireMainQuestionChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireMainQuestionProperty
}

