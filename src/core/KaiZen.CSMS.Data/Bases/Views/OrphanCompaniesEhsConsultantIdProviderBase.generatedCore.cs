﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrphanCompaniesEhsConsultantIdProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class OrphanCompaniesEhsConsultantIdProviderBaseCore : EntityViewProviderBase<OrphanCompaniesEhsConsultantId>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;OrphanCompaniesEhsConsultantId&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;OrphanCompaniesEhsConsultantId&gt;"/></returns>
		protected static VList&lt;OrphanCompaniesEhsConsultantId&gt; Fill(DataSet dataSet, VList<OrphanCompaniesEhsConsultantId> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<OrphanCompaniesEhsConsultantId>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;OrphanCompaniesEhsConsultantId&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<OrphanCompaniesEhsConsultantId>"/></returns>
		protected static VList&lt;OrphanCompaniesEhsConsultantId&gt; Fill(DataTable dataTable, VList<OrphanCompaniesEhsConsultantId> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					OrphanCompaniesEhsConsultantId c = new OrphanCompaniesEhsConsultantId();
					c.EhsConsultantId = (Convert.IsDBNull(row["EHSConsultantId"]))?(int)0:(System.Int32?)row["EHSConsultantId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;OrphanCompaniesEhsConsultantId&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;OrphanCompaniesEhsConsultantId&gt;"/></returns>
		protected VList<OrphanCompaniesEhsConsultantId> Fill(IDataReader reader, VList<OrphanCompaniesEhsConsultantId> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					OrphanCompaniesEhsConsultantId entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<OrphanCompaniesEhsConsultantId>("OrphanCompaniesEhsConsultantId",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new OrphanCompaniesEhsConsultantId();
					}
					
					entity.SuppressEntityEvents = true;

					entity.EhsConsultantId = (reader.IsDBNull(((int)OrphanCompaniesEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)OrphanCompaniesEhsConsultantIdColumn.EhsConsultantId)];
					//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="OrphanCompaniesEhsConsultantId"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanCompaniesEhsConsultantId"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, OrphanCompaniesEhsConsultantId entity)
		{
			reader.Read();
			entity.EhsConsultantId = (reader.IsDBNull(((int)OrphanCompaniesEhsConsultantIdColumn.EhsConsultantId)))?null:(System.Int32?)reader[((int)OrphanCompaniesEhsConsultantIdColumn.EhsConsultantId)];
			//entity.EhsConsultantId = (Convert.IsDBNull(reader["EHSConsultantId"]))?(int)0:(System.Int32?)reader["EHSConsultantId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="OrphanCompaniesEhsConsultantId"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanCompaniesEhsConsultantId"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, OrphanCompaniesEhsConsultantId entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EhsConsultantId = (Convert.IsDBNull(dataRow["EHSConsultantId"]))?(int)0:(System.Int32?)dataRow["EHSConsultantId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region OrphanCompaniesEhsConsultantIdFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdFilterBuilder : SqlFilterBuilder<OrphanCompaniesEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanCompaniesEhsConsultantIdFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanCompaniesEhsConsultantIdFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanCompaniesEhsConsultantIdFilterBuilder

	#region OrphanCompaniesEhsConsultantIdParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanCompaniesEhsConsultantIdParameterBuilder : ParameterizedSqlFilterBuilder<OrphanCompaniesEhsConsultantIdColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanCompaniesEhsConsultantIdParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanCompaniesEhsConsultantIdParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanCompaniesEhsConsultantIdParameterBuilder
	
	#region OrphanCompaniesEhsConsultantIdSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanCompaniesEhsConsultantId"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrphanCompaniesEhsConsultantIdSortBuilder : SqlSortBuilder<OrphanCompaniesEhsConsultantIdColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanCompaniesEhsConsultantIdSqlSortBuilder class.
		/// </summary>
		public OrphanCompaniesEhsConsultantIdSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrphanCompaniesEhsConsultantIdSortBuilder

} // end namespace
