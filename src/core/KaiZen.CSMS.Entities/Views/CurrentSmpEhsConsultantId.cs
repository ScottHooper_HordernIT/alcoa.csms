﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the '_Current_Smp_EhsConsultantId' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CurrentSmpEhsConsultantId : CurrentSmpEhsConsultantIdBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CurrentSmpEhsConsultantId"/> instance.
		///</summary>
		public CurrentSmpEhsConsultantId():base(){}	
		
		#endregion
	}
}
