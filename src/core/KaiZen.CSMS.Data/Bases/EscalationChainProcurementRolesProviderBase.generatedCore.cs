﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EscalationChainProcurementRolesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EscalationChainProcurementRolesProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.EscalationChainProcurementRoles, KaiZen.CSMS.Entities.EscalationChainProcurementRolesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementRolesKey key)
		{
			return Delete(transactionManager, key.EscalationChainProcurementRoleId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_escalationChainProcurementRoleId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _escalationChainProcurementRoleId)
		{
			return Delete(null, _escalationChainProcurementRoleId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementRoleId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _escalationChainProcurementRoleId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.EscalationChainProcurementRoles Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementRolesKey key, int start, int pageLength)
		{
			return GetByEscalationChainProcurementRoleId(transactionManager, key.EscalationChainProcurementRoleId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(System.Int32 _escalationChainProcurementRoleId)
		{
			int count = -1;
			return GetByEscalationChainProcurementRoleId(null,_escalationChainProcurementRoleId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(System.Int32 _escalationChainProcurementRoleId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainProcurementRoleId(null, _escalationChainProcurementRoleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementRoleId)
		{
			int count = -1;
			return GetByEscalationChainProcurementRoleId(transactionManager, _escalationChainProcurementRoleId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementRoleId, int start, int pageLength)
		{
			int count = -1;
			return GetByEscalationChainProcurementRoleId(transactionManager, _escalationChainProcurementRoleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(System.Int32 _escalationChainProcurementRoleId, int start, int pageLength, out int count)
		{
			return GetByEscalationChainProcurementRoleId(null, _escalationChainProcurementRoleId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_escalationChainProcurementRoleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByEscalationChainProcurementRoleId(TransactionManager transactionManager, System.Int32 _escalationChainProcurementRoleId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_level"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(null,_level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetByLevel(null, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(TransactionManager transactionManager, System.Int32 _level)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength)
		{
			int count = -1;
			return GetByLevel(transactionManager, _level, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(System.Int32 _level, int start, int pageLength, out int count)
		{
			return GetByLevel(null, _level, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_EscalationChainProcurementRoles index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_level"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.EscalationChainProcurementRoles GetByLevel(TransactionManager transactionManager, System.Int32 _level, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;EscalationChainProcurementRoles&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;EscalationChainProcurementRoles&gt;"/></returns>
		public static TList<EscalationChainProcurementRoles> Fill(IDataReader reader, TList<EscalationChainProcurementRoles> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.EscalationChainProcurementRoles c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("EscalationChainProcurementRoles")
					.Append("|").Append((System.Int32)reader[((int)EscalationChainProcurementRolesColumn.EscalationChainProcurementRoleId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<EscalationChainProcurementRoles>(
					key.ToString(), // EntityTrackingKey
					"EscalationChainProcurementRoles",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.EscalationChainProcurementRoles();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EscalationChainProcurementRoleId = (System.Int32)reader[((int)EscalationChainProcurementRolesColumn.EscalationChainProcurementRoleId - 1)];
					c.Level = (System.Int32)reader[((int)EscalationChainProcurementRolesColumn.Level - 1)];
					c.RoleTitle = (System.String)reader[((int)EscalationChainProcurementRolesColumn.RoleTitle - 1)];
					c.Enabled = (System.Boolean)reader[((int)EscalationChainProcurementRolesColumn.Enabled - 1)];
					c.ContactAfterDaysWith = (reader.IsDBNull(((int)EscalationChainProcurementRolesColumn.ContactAfterDaysWith - 1)))?null:(System.Int32?)reader[((int)EscalationChainProcurementRolesColumn.ContactAfterDaysWith - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.EscalationChainProcurementRoles entity)
		{
			if (!reader.Read()) return;
			
			entity.EscalationChainProcurementRoleId = (System.Int32)reader[((int)EscalationChainProcurementRolesColumn.EscalationChainProcurementRoleId - 1)];
			entity.Level = (System.Int32)reader[((int)EscalationChainProcurementRolesColumn.Level - 1)];
			entity.RoleTitle = (System.String)reader[((int)EscalationChainProcurementRolesColumn.RoleTitle - 1)];
			entity.Enabled = (System.Boolean)reader[((int)EscalationChainProcurementRolesColumn.Enabled - 1)];
			entity.ContactAfterDaysWith = (reader.IsDBNull(((int)EscalationChainProcurementRolesColumn.ContactAfterDaysWith - 1)))?null:(System.Int32?)reader[((int)EscalationChainProcurementRolesColumn.ContactAfterDaysWith - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.EscalationChainProcurementRoles entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EscalationChainProcurementRoleId = (System.Int32)dataRow["EscalationChainProcurementRoleId"];
			entity.Level = (System.Int32)dataRow["Level"];
			entity.RoleTitle = (System.String)dataRow["RoleTitle"];
			entity.Enabled = (System.Boolean)dataRow["Enabled"];
			entity.ContactAfterDaysWith = Convert.IsDBNull(dataRow["ContactAfterDaysWith"]) ? null : (System.Int32?)dataRow["ContactAfterDaysWith"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.EscalationChainProcurementRoles"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainProcurementRoles Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementRoles entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByEscalationChainProcurementRoleId methods when available
			
			#region EscalationChainProcurementCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'EscalationChainProcurementCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.EscalationChainProcurementCollection = DataRepository.EscalationChainProcurementProvider.GetByLevel(transactionManager, entity.Level);

				if (deep && entity.EscalationChainProcurementCollection.Count > 0)
				{
					deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<EscalationChainProcurement>) DataRepository.EscalationChainProcurementProvider.DeepLoad,
						new object[] { transactionManager, entity.EscalationChainProcurementCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.EscalationChainProcurementRoles object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.EscalationChainProcurementRoles instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.EscalationChainProcurementRoles Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.EscalationChainProcurementRoles entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<EscalationChainProcurement>
				if (CanDeepSave(entity.EscalationChainProcurementCollection, "List<EscalationChainProcurement>|EscalationChainProcurementCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(EscalationChainProcurement child in entity.EscalationChainProcurementCollection)
					{
						if(child.LevelSource != null)
						{
							child.Level = child.LevelSource.Level;
						}
						else
						{
							child.Level = entity.Level;
						}

					}

					if (entity.EscalationChainProcurementCollection.Count > 0 || entity.EscalationChainProcurementCollection.DeletedItems.Count > 0)
					{
						//DataRepository.EscalationChainProcurementProvider.Save(transactionManager, entity.EscalationChainProcurementCollection);
						
						deepHandles.Add("EscalationChainProcurementCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< EscalationChainProcurement >) DataRepository.EscalationChainProcurementProvider.DeepSave,
							new object[] { transactionManager, entity.EscalationChainProcurementCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EscalationChainProcurementRolesChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.EscalationChainProcurementRoles</c>
	///</summary>
	public enum EscalationChainProcurementRolesChildEntityTypes
	{

		///<summary>
		/// Collection of <c>EscalationChainProcurementRoles</c> as OneToMany for EscalationChainProcurementCollection
		///</summary>
		[ChildEntityType(typeof(TList<EscalationChainProcurement>))]
		EscalationChainProcurementCollection,
	}
	
	#endregion EscalationChainProcurementRolesChildEntityTypes
	
	#region EscalationChainProcurementRolesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EscalationChainProcurementRolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesFilterBuilder : SqlFilterBuilder<EscalationChainProcurementRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilterBuilder class.
		/// </summary>
		public EscalationChainProcurementRolesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementRolesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementRolesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementRolesFilterBuilder
	
	#region EscalationChainProcurementRolesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EscalationChainProcurementRolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EscalationChainProcurementRolesParameterBuilder : ParameterizedSqlFilterBuilder<EscalationChainProcurementRolesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesParameterBuilder class.
		/// </summary>
		public EscalationChainProcurementRolesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EscalationChainProcurementRolesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EscalationChainProcurementRolesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EscalationChainProcurementRolesParameterBuilder
	
	#region EscalationChainProcurementRolesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EscalationChainProcurementRolesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EscalationChainProcurementRoles"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EscalationChainProcurementRolesSortBuilder : SqlSortBuilder<EscalationChainProcurementRolesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EscalationChainProcurementRolesSqlSortBuilder class.
		/// </summary>
		public EscalationChainProcurementRolesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EscalationChainProcurementRolesSortBuilder
	
} // end namespace
