﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportServicesOtherAllProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportServicesOtherAllDataSourceDesigner))]
	public class QuestionnaireReportServicesOtherAllDataSource : ReadOnlyDataSource<QuestionnaireReportServicesOtherAll>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllDataSource class.
		/// </summary>
		public QuestionnaireReportServicesOtherAllDataSource() : base(new QuestionnaireReportServicesOtherAllService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportServicesOtherAllDataSourceView used by the QuestionnaireReportServicesOtherAllDataSource.
		/// </summary>
		protected QuestionnaireReportServicesOtherAllDataSourceView QuestionnaireReportServicesOtherAllView
		{
			get { return ( View as QuestionnaireReportServicesOtherAllDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportServicesOtherAllDataSourceView class that is to be
		/// used by the QuestionnaireReportServicesOtherAllDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportServicesOtherAllDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportServicesOtherAll, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportServicesOtherAllDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportServicesOtherAllDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportServicesOtherAllDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportServicesOtherAll>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportServicesOtherAllDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportServicesOtherAllDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportServicesOtherAllDataSourceView(QuestionnaireReportServicesOtherAllDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportServicesOtherAllDataSource QuestionnaireReportServicesOtherAllOwner
		{
			get { return Owner as QuestionnaireReportServicesOtherAllDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportServicesOtherAllService QuestionnaireReportServicesOtherAllProvider
		{
			get { return Provider as QuestionnaireReportServicesOtherAllService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportServicesOtherAllDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportServicesOtherAllDataSource class.
	/// </summary>
	public class QuestionnaireReportServicesOtherAllDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportServicesOtherAll>
	{
	}

	#endregion QuestionnaireReportServicesOtherAllDataSourceDesigner

	#region QuestionnaireReportServicesOtherAllFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOtherAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherAllFilter : SqlFilter<QuestionnaireReportServicesOtherAllColumn>
	{
	}

	#endregion QuestionnaireReportServicesOtherAllFilter

	#region QuestionnaireReportServicesOtherAllExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportServicesOtherAll"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportServicesOtherAllExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportServicesOtherAllColumn>
	{
	}
	
	#endregion QuestionnaireReportServicesOtherAllExpressionBuilder		
}

