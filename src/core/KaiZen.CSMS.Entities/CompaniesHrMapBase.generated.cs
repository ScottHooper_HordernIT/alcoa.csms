﻿
/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file CompaniesHrMap.cs instead.
*/

#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using KaiZen.CSMS.Entities.Validation;
#endregion

namespace KaiZen.CSMS.Entities
{
	///<summary>
	/// An object representation of the 'CompaniesHrMap' table. [No description found the database]	
	///</summary>
	[Serializable]
	[DataObject, CLSCompliant(true)]
	public abstract partial class CompaniesHrMapBase : EntityBase, ICompaniesHrMap, IEntityId<CompaniesHrMapKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private CompaniesHrMapEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private CompaniesHrMapEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private CompaniesHrMapEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<CompaniesHrMap> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event CompaniesHrMapEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event CompaniesHrMapEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="CompaniesHrMapBase"/> instance.
		///</summary>
		public CompaniesHrMapBase()
		{
			this.entityData = new CompaniesHrMapEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="CompaniesHrMapBase"/> instance.
		///</summary>
		///<param name="_companyId"></param>
		///<param name="_companyNameHr"></param>
		public CompaniesHrMapBase(System.Int32 _companyId, System.Int32 _companyNameHr)
		{
			this.entityData = new CompaniesHrMapEntityData();
			this.backupData = null;

			this.CompanyId = _companyId;
			this.CompanyNameHr = _companyNameHr;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="CompaniesHrMap"/> instance.
		///</summary>
		///<param name="_companyId"></param>
		///<param name="_companyNameHr"></param>
		public static CompaniesHrMap CreateCompaniesHrMap(System.Int32 _companyId, System.Int32 _companyNameHr)
		{
			CompaniesHrMap newCompaniesHrMap = new CompaniesHrMap();
			newCompaniesHrMap.CompanyId = _companyId;
			newCompaniesHrMap.CompanyNameHr = _companyNameHr;
			return newCompaniesHrMap;
		}
				
		#endregion Constructors
			
		#region Properties	
		
		#region Data Properties		
		/// <summary>
		/// 	Gets or sets the CompaniesHrMapId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[ReadOnlyAttribute(false)/*, XmlIgnoreAttribute()*/, DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(true, true, false)]
		public virtual System.Int32 CompaniesHrMapId
		{
			get
			{
				return this.entityData.CompaniesHrMapId; 
			}
			
			set
			{
				if (this.entityData.CompaniesHrMapId == value)
					return;
				
                OnPropertyChanging("CompaniesHrMapId");                    
				OnColumnChanging(CompaniesHrMapColumn.CompaniesHrMapId, this.entityData.CompaniesHrMapId);
				this.entityData.CompaniesHrMapId = value;
				this.EntityId.CompaniesHrMapId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(CompaniesHrMapColumn.CompaniesHrMapId, this.entityData.CompaniesHrMapId);
				OnPropertyChanged("CompaniesHrMapId");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the CompanyId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 CompanyId
		{
			get
			{
				return this.entityData.CompanyId; 
			}
			
			set
			{
				if (this.entityData.CompanyId == value)
					return;
				
                OnPropertyChanging("CompanyId");                    
				OnColumnChanging(CompaniesHrMapColumn.CompanyId, this.entityData.CompanyId);
				this.entityData.CompanyId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(CompaniesHrMapColumn.CompanyId, this.entityData.CompanyId);
				OnPropertyChanged("CompanyId");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the CompanyNameHr property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 CompanyNameHr
		{
			get
			{
				return this.entityData.CompanyNameHr; 
			}
			
			set
			{
				if (this.entityData.CompanyNameHr == value)
					return;
				
                OnPropertyChanging("CompanyNameHr");                    
				OnColumnChanging(CompaniesHrMapColumn.CompanyNameHr, this.entityData.CompanyNameHr);
				this.entityData.CompanyNameHr = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(CompaniesHrMapColumn.CompanyNameHr, this.entityData.CompanyNameHr);
				OnPropertyChanged("CompanyNameHr");
			}
		}
		
		#endregion Data Properties		

		#region Source Foreign Key Property
				
		#endregion
		
		#region Children Collections
	
		/// <summary>
		///	Holds a collection of CompaniesHrCrpData objects
		///	which are related to this object through the relation FK_CompaniesHrCrpData_CompaniesHrMap
		/// </summary>	
		[System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual TList<CompaniesHrCrpData> CompaniesHrCrpDataCollection
		{
			get { return entityData.CompaniesHrCrpDataCollection; }
			set { entityData.CompaniesHrCrpDataCollection = value; }	
		}
		#endregion Children Collections
		
		#endregion
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
		}
   		#endregion
		
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "CompaniesHrMap"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"CompaniesHrMapId", "CompanyId", "CompanyNameHr"};
			}
		}
		#endregion 
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as CompaniesHrMapEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (CompaniesHrMap) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<CompaniesHrMap>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as CompaniesHrMap);
	        }
	    }


		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed CompaniesHrMap Entity 
		///</summary>
		protected virtual CompaniesHrMap Copy(IDictionary existingCopies)
		{
			if (existingCopies == null)
			{
				// This is the root of the tree to be copied!
				existingCopies = new Hashtable();
			}

			//shallow copy entity
			CompaniesHrMap copy = new CompaniesHrMap();
			existingCopies.Add(this, copy);
			copy.SuppressEntityEvents = true;
				copy.CompaniesHrMapId = this.CompaniesHrMapId;
				copy.CompanyId = this.CompanyId;
				copy.CompanyNameHr = this.CompanyNameHr;
			
		
			//deep copy nested objects
			copy.CompaniesHrCrpDataCollection = (TList<CompaniesHrCrpData>) MakeCopyOf(this.CompaniesHrCrpDataCollection, existingCopies); 
			copy.EntityState = this.EntityState;
			copy.SuppressEntityEvents = false;
			return copy;
		}		
		
		
		
		///<summary>
		///  Returns a Typed CompaniesHrMap Entity 
		///</summary>
		public virtual CompaniesHrMap Copy()
		{
			return this.Copy(null);	
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy(null);
		}
		
		///<summary>
		/// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone(IDictionary existingCopies)
		{
			return this.Copy(existingCopies);
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x == null)
				return null;
				
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x, IDictionary existingCopies)
		{
			if (x == null)
				return null;
			
			if (x is ICloneableEx)
			{
				// Return a deep copy of the object
				return ((ICloneableEx)x).Clone(existingCopies);
			}
			else if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
		}
		
		
		///<summary>
		///  Returns a Typed CompaniesHrMap Entity which is a deep copy of the current entity.
		///</summary>
		public virtual CompaniesHrMap DeepCopy()
		{
			return EntityHelper.Clone<CompaniesHrMap>(this as CompaniesHrMap);	
		}
		#endregion
		
		#region Methods	
			
		///<summary>
		/// Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			if (this._originalData != null)
			{
				this.entityData = this._originalData.Clone() as CompaniesHrMapEntityData;
			}
			else
			{
				//Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
				this.entityData = new CompaniesHrMapEntityData();
			}
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as CompaniesHrMapEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(CompaniesHrMapColumn column)
		{
			switch(column)
			{
					case CompaniesHrMapColumn.CompaniesHrMapId:
					return entityData.CompaniesHrMapId != _originalData.CompaniesHrMapId;
					case CompaniesHrMapColumn.CompanyId:
					return entityData.CompanyId != _originalData.CompanyId;
					case CompaniesHrMapColumn.CompanyNameHr:
					return entityData.CompanyNameHr != _originalData.CompanyNameHr;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="columnName">The column name.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyChanged(string columnName)
		{
			return 	IsPropertyChanged(EntityHelper.GetEnumValue< CompaniesHrMapColumn >(columnName));
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.CompaniesHrMapId != _originalData.CompaniesHrMapId;
			result = result || entityData.CompanyId != _originalData.CompanyId;
			result = result || entityData.CompanyNameHr != _originalData.CompanyNameHr;
			return result;
		}	
		
		///<summary>
		///  Returns a CompaniesHrMap Entity with the original data.
		///</summary>
		public CompaniesHrMap GetOriginalEntity()
		{
			if (_originalData != null)
				return CreateCompaniesHrMap(
				_originalData.CompanyId,
				_originalData.CompanyNameHr
				);
				
			return (CompaniesHrMap)this.Clone();
		}
		#endregion
	
	#region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="CompaniesHrMapBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
			// Cast exception if Object1 is null or DbNull
			if (Object1 != null && Object1 != DBNull.Value && Object1 is CompaniesHrMapBase)
				return ValueEquals(this, (CompaniesHrMapBase)Object1);
			else
				return false;
        }

        /// <summary>
		/// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="CompaniesHrMapBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
			return this.CompaniesHrMapId.GetHashCode() ^ 
					this.CompanyId.GetHashCode() ^ 
					this.CompanyNameHr.GetHashCode();
        }
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object using value semantics.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="CompaniesHrMapBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(CompaniesHrMapBase toObject)
		{
			if (toObject == null)
				return false;
			return ValueEquals(this, toObject);
		}
		#endregion
		
		///<summary>
		/// Determines whether the specified <see cref="CompaniesHrMapBase"/> instances are considered equal using value semantics.
		///</summary>
		///<param name="Object1">The first <see cref="CompaniesHrMapBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="CompaniesHrMapBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool ValueEquals(CompaniesHrMapBase Object1, CompaniesHrMapBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.CompaniesHrMapId != Object2.CompaniesHrMapId)
				equal = false;
			if (Object1.CompanyId != Object2.CompanyId)
				equal = false;
			if (Object1.CompanyNameHr != Object2.CompanyNameHr)
				equal = false;
					
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((CompaniesHrMapBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
		}
		
		/*
		// static method to get a Comparer object
        public static CompaniesHrMapComparer GetComparer()
        {
            return new CompaniesHrMapComparer();
        }
        */

        // Comparer delegates back to CompaniesHrMap
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(CompaniesHrMap rhs, CompaniesHrMapColumn which)
        {
            switch (which)
            {
            	
            	
            	case CompaniesHrMapColumn.CompaniesHrMapId:
            		return this.CompaniesHrMapId.CompareTo(rhs.CompaniesHrMapId);
            		
            		                 
            	
            	
            	case CompaniesHrMapColumn.CompanyId:
            		return this.CompanyId.CompareTo(rhs.CompanyId);
            		
            		                 
            	
            	
            	case CompaniesHrMapColumn.CompanyNameHr:
            		return this.CompanyNameHr.CompareTo(rhs.CompanyNameHr);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public virtual void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<CompaniesHrMapKey> Members
		
		// member variable for the EntityId property
		private CompaniesHrMapKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public virtual CompaniesHrMapKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new CompaniesHrMapKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityState
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false) , XmlIgnoreAttribute()]
		public override EntityState EntityState 
		{ 
			get{ return entityData.EntityState;	 } 
			set{ entityData.EntityState = value; } 
		}
		#endregion 
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = new System.Text.StringBuilder("CompaniesHrMap")
					.Append("|").Append( this.CompaniesHrMapId.ToString()).ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{4}{3}- CompaniesHrMapId: {0}{3}- CompanyId: {1}{3}- CompanyNameHr: {2}{3}{5}", 
				this.CompaniesHrMapId,
				this.CompanyId,
				this.CompanyNameHr,
				System.Environment.NewLine, 
				this.GetType(),
				this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n",this.Error));
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'CompaniesHrMap' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class CompaniesHrMapEntityData : ICloneable, ICloneableEx
	{
		#region Variable Declarations
		private EntityState currentEntityState = EntityState.Added;
		
		#region Primary key(s)
		/// <summary>			
		/// CompaniesHrMapId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "CompaniesHrMap"</remarks>
		public System.Int32 CompaniesHrMapId;
			
		#endregion
		
		#region Non Primary key(s)
		
		/// <summary>
		/// CompanyId : 
		/// </summary>
		public System.Int32 CompanyId = (int)0;
		
		/// <summary>
		/// CompanyNameHr : 
		/// </summary>
		public System.Int32 CompanyNameHr = (int)0;
		#endregion
			
		#region Source Foreign Key Property
				
		#endregion
        
		#endregion Variable Declarations

		#region Data Properties

		#region CompaniesHrCrpDataCollection
		
		private TList<CompaniesHrCrpData> _companiesHrCrpDataCompanyId;
		
		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _companiesHrCrpDataCompanyId
		/// </summary>
		
		public TList<CompaniesHrCrpData> CompaniesHrCrpDataCollection
		{
			get
			{
				if (_companiesHrCrpDataCompanyId == null)
				{
				_companiesHrCrpDataCompanyId = new TList<CompaniesHrCrpData>();
				}
	
				return _companiesHrCrpDataCompanyId;
			}
			set { _companiesHrCrpDataCompanyId = value; }
		}
		
		#endregion

		#endregion Data Properties
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			CompaniesHrMapEntityData _tmp = new CompaniesHrMapEntityData();
						
			_tmp.CompaniesHrMapId = this.CompaniesHrMapId;
			
			_tmp.CompanyId = this.CompanyId;
			_tmp.CompanyNameHr = this.CompanyNameHr;
			
			#region Source Parent Composite Entities
			#endregion
		
			#region Child Collections
			//deep copy nested objects
			if (this._companiesHrCrpDataCompanyId != null)
				_tmp.CompaniesHrCrpDataCollection = (TList<CompaniesHrCrpData>) MakeCopyOf(this.CompaniesHrCrpDataCollection); 
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone(IDictionary existingCopies)
		{
			if (existingCopies == null)
				existingCopies = new Hashtable();
				
			CompaniesHrMapEntityData _tmp = new CompaniesHrMapEntityData();
						
			_tmp.CompaniesHrMapId = this.CompaniesHrMapId;
			
			_tmp.CompanyId = this.CompanyId;
			_tmp.CompanyNameHr = this.CompanyNameHr;
			
			#region Source Parent Composite Entities
			#endregion
		
			#region Child Collections
			//deep copy nested objects
			_tmp.CompaniesHrCrpDataCollection = (TList<CompaniesHrCrpData>) MakeCopyOf(this.CompaniesHrCrpDataCollection, existingCopies); 
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public EntityState	EntityState
		{
			get { return currentEntityState;  }
			set { currentEntityState = value; }
		}
	
	}//End struct

		#endregion
		
				
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="CompaniesHrMapColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanging(CompaniesHrMapColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="CompaniesHrMapColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanged(CompaniesHrMapColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="CompaniesHrMapColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanging(CompaniesHrMapColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);
                
			if (!SuppressEntityEvents)
			{
				CompaniesHrMapEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new CompaniesHrMapEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="CompaniesHrMapColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanged(CompaniesHrMapColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				CompaniesHrMapEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new CompaniesHrMapEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
			
	} // End Class
	
	
	#region CompaniesHrMapEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="CompaniesHrMap"/> object.
	/// </remarks>
	public class CompaniesHrMapEventArgs : System.EventArgs
	{
		private CompaniesHrMapColumn column;
		private object value;
		
		///<summary>
		/// Initalizes a new Instance of the CompaniesHrMapEventArgs class.
		///</summary>
		public CompaniesHrMapEventArgs(CompaniesHrMapColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the CompaniesHrMapEventArgs class.
		///</summary>
		public CompaniesHrMapEventArgs(CompaniesHrMapColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		///<summary>
		/// The CompaniesHrMapColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="CompaniesHrMapColumn" />
		public CompaniesHrMapColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	///<summary>
	/// Define a delegate for all CompaniesHrMap related events.
	///</summary>
	public delegate void CompaniesHrMapEventHandler(object sender, CompaniesHrMapEventArgs e);
	
	#region CompaniesHrMapComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class CompaniesHrMapComparer : System.Collections.Generic.IComparer<CompaniesHrMap>
	{
		CompaniesHrMapColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:CompaniesHrMapComparer"/> class.
        /// </summary>
		public CompaniesHrMapComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:CompaniesHrMapComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public CompaniesHrMapComparer(CompaniesHrMapColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <see cref="CompaniesHrMap"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="CompaniesHrMap"/> to compare.</param>
        /// <param name="b">The second <c>CompaniesHrMap</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(CompaniesHrMap a, CompaniesHrMap b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(CompaniesHrMap entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(CompaniesHrMap a, CompaniesHrMap b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public CompaniesHrMapColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region CompaniesHrMapKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class CompaniesHrMapKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapKey class.
		/// </summary>
		public CompaniesHrMapKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapKey class.
		/// </summary>
		public CompaniesHrMapKey(CompaniesHrMapBase entity)
		{
			this.Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.CompaniesHrMapId = entity.CompaniesHrMapId;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapKey class.
		/// </summary>
		public CompaniesHrMapKey(System.Int32 _companiesHrMapId)
		{
			#region Init Properties

			this.CompaniesHrMapId = _companiesHrMapId;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private CompaniesHrMapBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public CompaniesHrMapBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the CompaniesHrMapId property
		private System.Int32 _companiesHrMapId;
		
		/// <summary>
		/// Gets or sets the CompaniesHrMapId property.
		/// </summary>
		public System.Int32 CompaniesHrMapId
		{
			get { return _companiesHrMapId; }
			set
			{
				if ( this.Entity != null )
					this.Entity.CompaniesHrMapId = value;
				
				_companiesHrMapId = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				CompaniesHrMapId = ( values["CompaniesHrMapId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompaniesHrMapId"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("CompaniesHrMapId", CompaniesHrMapId);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("CompaniesHrMapId: {0}{1}",
								CompaniesHrMapId,
								System.Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region CompaniesHrMapColumn Enum
	
	/// <summary>
	/// Enumerate the CompaniesHrMap columns.
	/// </summary>
	[Serializable]
	public enum CompaniesHrMapColumn : int
	{
		/// <summary>
		/// CompaniesHrMapId : 
		/// </summary>
		[EnumTextValue("CompaniesHrMapId")]
		[ColumnEnum("CompaniesHrMapId", typeof(System.Int32), System.Data.DbType.Int32, true, true, false)]
		CompaniesHrMapId = 1,
		/// <summary>
		/// CompanyId : 
		/// </summary>
		[EnumTextValue("CompanyId")]
		[ColumnEnum("CompanyId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		CompanyId = 2,
		/// <summary>
		/// CompanyNameHr : 
		/// </summary>
		[EnumTextValue("CompanyNameHr")]
		[ColumnEnum("CompanyNameHr", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		CompanyNameHr = 3
	}//End enum

	#endregion CompaniesHrMapColumn Enum

} // end namespace
