﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_NonComplianceKPISQ" Codebehind="NonComplianceKPISQ.ascx.cs" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 16px; width: 1170px;">
            <span class="bodycopy"><span class="title">Compliance Report</span><br />
                <span class="date">KPI</span><br />
                <img src="images/grfc_dottedline.gif" width="24" height="1"></span>
        </td>
    </tr>
    <tr>
        <td style="height: 28px; width: 1170px;" colspan="3">
            <table style="width: 522px">
                <tr>
                    <td style="width: 76px; text-align: right">
                        
                    </td>
                    <td style="width: 201px; text-align: left">
                        
                    </td>
                    <td align="right" style="width: 85px; height: 28px">
                        <button name="btnPrint" onclick="javascript:print();" style="font-weight: bold; width: 80px;
                            height: 25px" type="button">
                            Print</button>
                    </td>
                </tr>
            </table>
            
            
            </td>
            </tr>
            <tr>
        <td style="width: 1170px; padding-top: 2px; text-align: center" class="pageName" align="right" colspan="3">
            <div align="right">
                <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                    Customize</a>
            </div>
        </td>
    </tr>
            
            <tr><td style="height: 28px; width: 1170px;" colspan="3">
            
            
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                 Width="900px" OnHtmlRowCreated="grid_RowCreated"
                KeyFieldName="CompanyId" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
                <SettingsCustomizationWindow Enabled="True"></SettingsCustomizationWindow>
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
                        SortIndex="2" SortOrder="Ascending" VisibleIndex="0" Visible="false" ShowInCustomizationForm="false">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company Name" FieldName="CompanyName2" SortIndex="0" SortOrder="Ascending" VisibleIndex="0"
                        Visible="True">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="SiteId" ShowInCustomizationForm="false"
                        Visible="False">
                        <PropertiesComboBox ValueType="System.String">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Site Name" FieldName="SiteName2"
                        VisibleIndex="1" SortIndex="1" SortOrder="Ascending">
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Last Time On Site (EBI)" 
                        FieldName="LastEbiSwipe" VisibleIndex="3">
                        <PropertiesDateEdit DisplayFormatString="yyyy/MM/dd">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Last KPI Entered" 
                        FieldName="LastKpiEntered" VisibleIndex="3">
                        <PropertiesDateEdit DisplayFormatString="yyyy/MM">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Type" VisibleIndex="4" />
                    <dxwgv:GridViewDataTextColumn Caption="Valid For Site" FieldName="ValidForSite" VisibleIndex="5">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Approved For Site" FieldName="ApprovedForSite" Width="55px" VisibleIndex="6">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="SQ Validity" FieldName="SqValidity" VisibleIndex="7">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="SQ Valid To" 
                        FieldName="SqValidTo" VisibleIndex="9">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company SQ Status" FieldName="CompanySqStatus"
                        VisibleIndex="9">
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Procurement Contact" FieldName="ProcurementContact"
                        Visible="false" Width="150px" ShowInCustomizationForm="true">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="true">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="H&S Assessor" FieldName="HSAssessor"
                        Visible="false" Width="150px" ShowInCustomizationForm="true">
                        <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="true">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                <SettingsPager PageSize="20" AlwaysShowPager="True">
                    <AllButton Visible="True">
                    </AllButton>
                    <NextPageButton>
                    </NextPageButton>
                    <PrevPageButton>
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn" />
                <SettingsCookies CookiesID="NonComplianceKPI" Version="0.2" />
                <SettingsText CommandClearFilter="Clear" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
            <tr>
                <td>
                    <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: underline">
                    </span></a>
                </td>
            </tr>
            <tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
</tr>
</table>
<%--<asp:SqlDataSource ID="sqldsComplianceKPI" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_GetReport_Compliance_SQ" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>--%>
<asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>     
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True"
    Sort="Ordinal ASC">
</data:RegionsDataSource>
