﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdminTaskEmailRecipientProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdminTaskEmailRecipientProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdminTaskEmailRecipient, KaiZen.CSMS.Entities.AdminTaskEmailRecipientKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailRecipientKey key)
		{
			return Delete(transactionManager, key.AdminTaskEmailRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adminTaskEmailRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adminTaskEmailRecipientId)
		{
			return Delete(null, _adminTaskEmailRecipientId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdminTaskEmailRecipient Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailRecipientKey key, int start, int pageLength)
		{
			return GetByAdminTaskEmailRecipientId(transactionManager, key.AdminTaskEmailRecipientId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(null,_adminTaskEmailRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(null, _adminTaskEmailRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(transactionManager, _adminTaskEmailRecipientId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdminTaskEmailRecipientId(transactionManager, _adminTaskEmailRecipientId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(System.Int32 _adminTaskEmailRecipientId, int start, int pageLength, out int count)
		{
			return GetByAdminTaskEmailRecipientId(null, _adminTaskEmailRecipientId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdminTaskEmailRecipient index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adminTaskEmailRecipientId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByAdminTaskEmailRecipientId(TransactionManager transactionManager, System.Int32 _adminTaskEmailRecipientId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="_recipientName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(System.String _recipientName)
		{
			int count = -1;
			return GetByRecipientName(null,_recipientName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="_recipientName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(System.String _recipientName, int start, int pageLength)
		{
			int count = -1;
			return GetByRecipientName(null, _recipientName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_recipientName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(TransactionManager transactionManager, System.String _recipientName)
		{
			int count = -1;
			return GetByRecipientName(transactionManager, _recipientName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_recipientName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(TransactionManager transactionManager, System.String _recipientName, int start, int pageLength)
		{
			int count = -1;
			return GetByRecipientName(transactionManager, _recipientName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="_recipientName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(System.String _recipientName, int start, int pageLength, out int count)
		{
			return GetByRecipientName(null, _recipientName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_AdminTaskEmailRecipientName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_recipientName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdminTaskEmailRecipient GetByRecipientName(TransactionManager transactionManager, System.String _recipientName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdminTaskEmailRecipient&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdminTaskEmailRecipient&gt;"/></returns>
		public static TList<AdminTaskEmailRecipient> Fill(IDataReader reader, TList<AdminTaskEmailRecipient> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdminTaskEmailRecipient c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdminTaskEmailRecipient")
					.Append("|").Append((System.Int32)reader[((int)AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdminTaskEmailRecipient>(
					key.ToString(), // EntityTrackingKey
					"AdminTaskEmailRecipient",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdminTaskEmailRecipient();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdminTaskEmailRecipientId = (System.Int32)reader[((int)AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId - 1)];
					c.RecipientName = (System.String)reader[((int)AdminTaskEmailRecipientColumn.RecipientName - 1)];
					c.RecipientDesc = (System.String)reader[((int)AdminTaskEmailRecipientColumn.RecipientDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdminTaskEmailRecipient entity)
		{
			if (!reader.Read()) return;
			
			entity.AdminTaskEmailRecipientId = (System.Int32)reader[((int)AdminTaskEmailRecipientColumn.AdminTaskEmailRecipientId - 1)];
			entity.RecipientName = (System.String)reader[((int)AdminTaskEmailRecipientColumn.RecipientName - 1)];
			entity.RecipientDesc = (System.String)reader[((int)AdminTaskEmailRecipientColumn.RecipientDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdminTaskEmailRecipient entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdminTaskEmailRecipientId = (System.Int32)dataRow["AdminTaskEmailRecipientId"];
			entity.RecipientName = (System.String)dataRow["RecipientName"];
			entity.RecipientDesc = (System.String)dataRow["RecipientDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdminTaskEmailRecipient"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailRecipient Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailRecipient entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdminTaskEmailRecipientId methods when available
			
			#region AdminTaskEmailTemplateRecipientCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdminTaskEmailTemplateRecipientCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdminTaskEmailTemplateRecipientCollection = DataRepository.AdminTaskEmailTemplateRecipientProvider.GetByAdminTaskEmailRecipientId(transactionManager, entity.AdminTaskEmailRecipientId);

				if (deep && entity.AdminTaskEmailTemplateRecipientCollection.Count > 0)
				{
					deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdminTaskEmailTemplateRecipient>) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepLoad,
						new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdminTaskEmailRecipient object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdminTaskEmailRecipient instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdminTaskEmailRecipient Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdminTaskEmailRecipient entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdminTaskEmailTemplateRecipient>
				if (CanDeepSave(entity.AdminTaskEmailTemplateRecipientCollection, "List<AdminTaskEmailTemplateRecipient>|AdminTaskEmailTemplateRecipientCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdminTaskEmailTemplateRecipient child in entity.AdminTaskEmailTemplateRecipientCollection)
					{
						if(child.AdminTaskEmailRecipientIdSource != null)
						{
							child.AdminTaskEmailRecipientId = child.AdminTaskEmailRecipientIdSource.AdminTaskEmailRecipientId;
						}
						else
						{
							child.AdminTaskEmailRecipientId = entity.AdminTaskEmailRecipientId;
						}

					}

					if (entity.AdminTaskEmailTemplateRecipientCollection.Count > 0 || entity.AdminTaskEmailTemplateRecipientCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdminTaskEmailTemplateRecipientProvider.Save(transactionManager, entity.AdminTaskEmailTemplateRecipientCollection);
						
						deepHandles.Add("AdminTaskEmailTemplateRecipientCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdminTaskEmailTemplateRecipient >) DataRepository.AdminTaskEmailTemplateRecipientProvider.DeepSave,
							new object[] { transactionManager, entity.AdminTaskEmailTemplateRecipientCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdminTaskEmailRecipientChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdminTaskEmailRecipient</c>
	///</summary>
	public enum AdminTaskEmailRecipientChildEntityTypes
	{

		///<summary>
		/// Collection of <c>AdminTaskEmailRecipient</c> as OneToMany for AdminTaskEmailTemplateRecipientCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdminTaskEmailTemplateRecipient>))]
		AdminTaskEmailTemplateRecipientCollection,
	}
	
	#endregion AdminTaskEmailRecipientChildEntityTypes
	
	#region AdminTaskEmailRecipientFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdminTaskEmailRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientFilterBuilder : SqlFilterBuilder<AdminTaskEmailRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilterBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientFilterBuilder
	
	#region AdminTaskEmailRecipientParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdminTaskEmailRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientParameterBuilder : ParameterizedSqlFilterBuilder<AdminTaskEmailRecipientColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientParameterBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdminTaskEmailRecipientParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdminTaskEmailRecipientParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdminTaskEmailRecipientParameterBuilder
	
	#region AdminTaskEmailRecipientSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdminTaskEmailRecipientColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdminTaskEmailRecipientSortBuilder : SqlSortBuilder<AdminTaskEmailRecipientColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientSqlSortBuilder class.
		/// </summary>
		public AdminTaskEmailRecipientSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdminTaskEmailRecipientSortBuilder
	
} // end namespace
