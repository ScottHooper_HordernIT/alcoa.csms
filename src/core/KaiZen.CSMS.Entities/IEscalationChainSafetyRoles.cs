﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'EscalationChainSafetyRoles' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IEscalationChainSafetyRoles 
	{
		/// <summary>			
		/// EscalationChainSafetyRoleId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "EscalationChainSafetyRoles"</remarks>
		System.Int32 EscalationChainSafetyRoleId { get; set; }
				
		
		
		/// <summary>
		/// Level : 
		/// </summary>
		System.Int32  Level  { get; set; }
		
		/// <summary>
		/// RoleTitle : 
		/// </summary>
		System.String  RoleTitle  { get; set; }
		
		/// <summary>
		/// Enabled : 
		/// </summary>
		System.Boolean  Enabled  { get; set; }
		
		/// <summary>
		/// ContactAfterDaysWith : 
		/// </summary>
		System.Int32?  ContactAfterDaysWith  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _escalationChainSafetyLevel
		/// </summary>	
		TList<EscalationChainSafety> EscalationChainSafetyCollection {  get;  set;}	

		#endregion Data Properties

	}
}


