﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiProjectService
    {
        KpiProject Get(Expression<Func<KpiProject, bool>> where, List<Expression<Func<KpiProject, object>>> includes);
        List<KpiProject> GetMany(int? top, Expression<Func<KpiProject, bool>> where, Func<IQueryable<KpiProject>, IOrderedQueryable<KpiProject>> orderBy, List<Expression<Func<KpiProject, object>>> includes);
        List<KpiProject> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiProject, bool>> where, Func<IQueryable<KpiProject>, IOrderedQueryable<KpiProject>> orderBy, List<Expression<Func<KpiProject, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiProject, bool>> where, List<Expression<Func<KpiProject, object>>> includes);
        void Insert(KpiProject item, bool saveChanges = true);
        void Update(KpiProject item, bool saveChanges = true);
        void Delete(KpiProject item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiProjectService : IKpiProjectService
    {
        private readonly IRepository<KpiProject> _repository;

        public KpiProjectService(IRepository<KpiProject> repository)
        {
            this._repository = repository;
        }

        public KpiProject Get(Expression<Func<KpiProject, bool>> where, List<Expression<Func<KpiProject, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiProject> GetMany(int? top, Expression<Func<KpiProject, bool>> where, Func<IQueryable<KpiProject>, IOrderedQueryable<KpiProject>> orderBy, List<Expression<Func<KpiProject, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiProject> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiProject, bool>> where, Func<IQueryable<KpiProject>, IOrderedQueryable<KpiProject>> orderBy, List<Expression<Func<KpiProject, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiProject, bool>> where, List<Expression<Func<KpiProject, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiProject item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProject");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiProject item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProject");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiProject item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiProject");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
