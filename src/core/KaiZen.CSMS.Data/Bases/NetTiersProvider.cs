﻿
#region Using directives

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Configuration.Provider;

using KaiZen.CSMS.Entities;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// The base class to implements to create a .NetTiers provider.
	///</summary>
	public abstract class NetTiersProvider : NetTiersProviderBase
	{
        ///<summary>
        /// Current SafetyPlansSEResponsesAttachmentProviderBase instance.
        /// Bishwajit Sahoo(TCS)
        ///</summary>
        public virtual SafetyPlansSEResponsesAttachmentProviderBase SafetyPlansSEResponsesAttachmentProvider { get { throw new NotImplementedException(); } }
		
		///<summary>
		/// Current QuestionnaireMainAnswerProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainAnswerProviderBase QuestionnaireMainAnswerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireServicesCategoryProviderBase instance.
		///</summary>
		public virtual QuestionnaireServicesCategoryProviderBase QuestionnaireServicesCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationAssessmentAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationAssessmentAuditProviderBase QuestionnaireVerificationAssessmentAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireTypeProviderBase instance.
		///</summary>
		public virtual QuestionnaireTypeProviderBase QuestionnaireTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RoleProviderBase instance.
		///</summary>
		public virtual RoleProviderBase RoleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireStatusProviderBase instance.
		///</summary>
		public virtual QuestionnaireStatusProviderBase QuestionnaireStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanyStatusProviderBase instance.
		///</summary>
		public virtual CompanyStatusProviderBase CompanyStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireServicesSelectedAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireServicesSelectedAuditProviderBase QuestionnaireServicesSelectedAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanyStatus2ProviderBase instance.
		///</summary>
		public virtual CompanyStatus2ProviderBase CompanyStatus2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationAttachmentAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationAttachmentAuditProviderBase QuestionnaireVerificationAttachmentAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainQuestionProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainQuestionProviderBase QuestionnaireMainQuestionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationRationaleProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationRationaleProviderBase QuestionnaireVerificationRationaleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireProviderBase instance.
		///</summary>
		public virtual QuestionnaireProviderBase QuestionnaireProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireServicesSelectedProviderBase instance.
		///</summary>
		public virtual QuestionnaireServicesSelectedProviderBase QuestionnaireServicesSelectedProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ResidentialProviderBase instance.
		///</summary>
		public virtual ResidentialProviderBase ResidentialProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AccessLogsProviderBase instance.
		///</summary>
		public virtual AccessLogsProviderBase AccessLogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnairePresentlyWithUsersProviderBase instance.
		///</summary>
		public virtual QuestionnairePresentlyWithUsersProviderBase QuestionnairePresentlyWithUsersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RegionsProviderBase instance.
		///</summary>
		public virtual RegionsProviderBase RegionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesProviderBase instance.
		///</summary>
		public virtual CompaniesProviderBase CompaniesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProviderBase instance.
		///</summary>
		public virtual UsersProviderBase UsersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationAssessmentProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationAssessmentProviderBase QuestionnaireVerificationAssessmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationResponseProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationResponseProviderBase QuestionnaireVerificationResponseProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationResponseAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationResponseAuditProviderBase QuestionnaireVerificationResponseAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationAttachmentProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationAttachmentProviderBase QuestionnaireVerificationAttachmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current RegionsSitesProviderBase instance.
		///</summary>
		public virtual RegionsSitesProviderBase RegionsSitesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireVerificationSectionProviderBase instance.
		///</summary>
		public virtual QuestionnaireVerificationSectionProviderBase QuestionnaireVerificationSectionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnairePresentlyWithMetricProviderBase instance.
		///</summary>
		public virtual QuestionnairePresentlyWithMetricProviderBase QuestionnairePresentlyWithMetricProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialContactStatusProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialContactStatusProviderBase QuestionnaireInitialContactStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialLocationProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialLocationProviderBase QuestionnaireInitialLocationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialLocationAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialLocationAuditProviderBase QuestionnaireInitialLocationAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialContactProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialContactProviderBase QuestionnaireInitialContactProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialContactEmailTypeProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialContactEmailTypeProviderBase QuestionnaireInitialContactEmailTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireActionProviderBase instance.
		///</summary>
		public virtual QuestionnaireActionProviderBase QuestionnaireActionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialContactEmailProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialContactEmailProviderBase QuestionnaireInitialContactEmailProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialResponseProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialResponseProviderBase QuestionnaireInitialResponseProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialContactAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialContactAuditProviderBase QuestionnaireInitialContactAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainResponseProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainResponseProviderBase QuestionnaireMainResponseProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireInitialResponseAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireInitialResponseAuditProviderBase QuestionnaireInitialResponseAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainResponseAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainResponseAuditProviderBase QuestionnaireMainResponseAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainRationaleProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainRationaleProviderBase QuestionnaireMainRationaleProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnairePresentlyWithActionProviderBase instance.
		///</summary>
		public virtual QuestionnairePresentlyWithActionProviderBase QuestionnairePresentlyWithActionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainAssessmentProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainAssessmentProviderBase QuestionnaireMainAssessmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainAttachmentAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainAttachmentAuditProviderBase QuestionnaireMainAttachmentAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainAssessmentAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainAssessmentAuditProviderBase QuestionnaireMainAssessmentAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireContractorRsProviderBase instance.
		///</summary>
		public virtual QuestionnaireContractorRsProviderBase QuestionnaireContractorRsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainAttachmentProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainAttachmentProviderBase QuestionnaireMainAttachmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit18ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit18ProviderBase TwentyOnePointAudit18Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SafetyPlansSeAnswersProviderBase instance.
		///</summary>
		public virtual SafetyPlansSeAnswersProviderBase SafetyPlansSeAnswersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit19ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit19ProviderBase TwentyOnePointAudit19Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit17ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit17ProviderBase TwentyOnePointAudit17Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit20ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit20ProviderBase TwentyOnePointAudit20Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit13ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit13ProviderBase TwentyOnePointAudit13Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit16ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit16ProviderBase TwentyOnePointAudit16Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit14ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit14ProviderBase TwentyOnePointAudit14Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit21ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit21ProviderBase TwentyOnePointAudit21Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit15ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit15ProviderBase TwentyOnePointAudit15Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAuditQtrProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAuditQtrProviderBase TwentyOnePointAuditQtrProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersPrivileged3ProviderBase instance.
		///</summary>
		public virtual UsersPrivileged3ProviderBase UsersPrivileged3Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UserPrivilegeProviderBase instance.
		///</summary>
		public virtual UserPrivilegeProviderBase UserPrivilegeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProcurementProviderBase instance.
		///</summary>
		public virtual UsersProcurementProviderBase UsersProcurementProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersPrivileged2ProviderBase instance.
		///</summary>
		public virtual UsersPrivileged2ProviderBase UsersPrivileged2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProcurementAuditProviderBase instance.
		///</summary>
		public virtual UsersProcurementAuditProviderBase UsersProcurementAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersAuditProviderBase instance.
		///</summary>
		public virtual UsersAuditProviderBase UsersAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersPrivilegedProviderBase instance.
		///</summary>
		public virtual UsersPrivilegedProviderBase UsersPrivilegedProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersEbiProviderBase instance.
		///</summary>
		public virtual UsersEbiProviderBase UsersEbiProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit12ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit12ProviderBase TwentyOnePointAudit12Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersEbiAuditProviderBase instance.
		///</summary>
		public virtual UsersEbiAuditProviderBase UsersEbiAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit11ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit11ProviderBase TwentyOnePointAudit11Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SitesProviderBase instance.
		///</summary>
		public virtual SitesProviderBase SitesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SafetyPlansSeQuestionsProviderBase instance.
		///</summary>
		public virtual SafetyPlansSeQuestionsProviderBase SafetyPlansSeQuestionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TemplateProviderBase instance.
		///</summary>
		public virtual TemplateProviderBase TemplateProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SystemLogProviderBase instance.
		///</summary>
		public virtual SystemLogProviderBase SystemLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TemplateTypeProviderBase instance.
		///</summary>
		public virtual TemplateTypeProviderBase TemplateTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SqExemptionAuditProviderBase instance.
		///</summary>
		public virtual SqExemptionAuditProviderBase SqExemptionAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SafetyPlansSeResponsesProviderBase instance.
		///</summary>
		public virtual SafetyPlansSeResponsesProviderBase SafetyPlansSeResponsesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SqExemptionProviderBase instance.
		///</summary>
		public virtual SqExemptionProviderBase SqExemptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit10ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit10ProviderBase TwentyOnePointAudit10Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SafetyPlanStatusProviderBase instance.
		///</summary>
		public virtual SafetyPlanStatusProviderBase SafetyPlanStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit07ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit07ProviderBase TwentyOnePointAudit07Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireCommentsAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireCommentsAuditProviderBase QuestionnaireCommentsAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit08ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit08ProviderBase TwentyOnePointAudit08Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit01ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit01ProviderBase TwentyOnePointAudit01Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit09ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit09ProviderBase TwentyOnePointAudit09Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit02ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit02ProviderBase TwentyOnePointAudit02Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit06ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit06ProviderBase TwentyOnePointAudit06Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit05ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit05ProviderBase TwentyOnePointAudit05Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit04ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit04ProviderBase TwentyOnePointAudit04Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAudit03ProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAudit03ProviderBase TwentyOnePointAudit03Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current TwentyOnePointAuditProviderBase instance.
		///</summary>
		public virtual TwentyOnePointAuditProviderBase TwentyOnePointAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigNavigationProviderBase instance.
		///</summary>
		public virtual ConfigNavigationProviderBase ConfigNavigationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireCommentsProviderBase instance.
		///</summary>
		public virtual QuestionnaireCommentsProviderBase QuestionnaireCommentsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigSa812ProviderBase instance.
		///</summary>
		public virtual ConfigSa812ProviderBase ConfigSa812Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigProviderBase instance.
		///</summary>
		public virtual ConfigProviderBase ConfigProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigTextTypeProviderBase instance.
		///</summary>
		public virtual ConfigTextTypeProviderBase ConfigTextTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanyStatusChangeApprovalProviderBase instance.
		///</summary>
		public virtual CompanyStatusChangeApprovalProviderBase CompanyStatusChangeApprovalProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryExceptionProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryExceptionProviderBase CompanySiteCategoryExceptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryStandardAuditProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryStandardAuditProviderBase CompanySiteCategoryStandardAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryException2ProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryException2ProviderBase CompanySiteCategoryException2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigTextProviderBase instance.
		///</summary>
		public virtual ConfigTextProviderBase ConfigTextProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryStandardProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryStandardProviderBase CompanySiteCategoryStandardProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsEmailLogProviderBase instance.
		///</summary>
		public virtual CsmsEmailLogProviderBase CsmsEmailLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigText2ProviderBase instance.
		///</summary>
		public virtual ConfigText2ProviderBase ConfigText2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsaProviderBase instance.
		///</summary>
		public virtual CsaProviderBase CsaProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsFileTypeProviderBase instance.
		///</summary>
		public virtual CsmsFileTypeProviderBase CsmsFileTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsEmailLogRecipientProviderBase instance.
		///</summary>
		public virtual CsmsEmailLogRecipientProviderBase CsmsEmailLogRecipientProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsAccessProviderBase instance.
		///</summary>
		public virtual CsmsAccessProviderBase CsmsAccessProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ContactsAlcoaProviderBase instance.
		///</summary>
		public virtual ContactsAlcoaProviderBase ContactsAlcoaProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsaAnswersProviderBase instance.
		///</summary>
		public virtual CsaAnswersProviderBase CsaAnswersProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryProviderBase CompanySiteCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ContactsContractorsProviderBase instance.
		///</summary>
		public virtual ContactsContractorsProviderBase ContactsContractorsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailTemplateProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailTemplateProviderBase AdminTaskEmailTemplateProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesRelationshipProviderBase instance.
		///</summary>
		public virtual CompaniesRelationshipProviderBase CompaniesRelationshipProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailRecipientProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailRecipientProviderBase AdminTaskEmailRecipientProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskSourceProviderBase instance.
		///</summary>
		public virtual AdminTaskSourceProviderBase AdminTaskSourceProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdHocRadarProviderBase instance.
		///</summary>
		public virtual AdHocRadarProviderBase AdHocRadarProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdHocRadarItems2ProviderBase instance.
		///</summary>
		public virtual AdHocRadarItems2ProviderBase AdHocRadarItems2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailTemplateRecipientProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailTemplateRecipientProviderBase AdminTaskEmailTemplateRecipientProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskStatusProviderBase instance.
		///</summary>
		public virtual AdminTaskStatusProviderBase AdminTaskStatusProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdHocRadarItemsProviderBase instance.
		///</summary>
		public virtual AdHocRadarItemsProviderBase AdHocRadarItemsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskTypeProviderBase instance.
		///</summary>
		public virtual AdminTaskTypeProviderBase AdminTaskTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AnnualTargetsProviderBase instance.
		///</summary>
		public virtual AnnualTargetsProviderBase AnnualTargetsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesHrMapProviderBase instance.
		///</summary>
		public virtual CompaniesHrMapProviderBase CompaniesHrMapProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesEhsimsMapProviderBase instance.
		///</summary>
		public virtual CompaniesEhsimsMapProviderBase CompaniesEhsimsMapProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskProviderBase instance.
		///</summary>
		public virtual AdminTaskProviderBase AdminTaskProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesHrCrpDataProviderBase instance.
		///</summary>
		public virtual CompaniesHrCrpDataProviderBase CompaniesHrCrpDataProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailLogProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailLogProviderBase AdminTaskEmailLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ApssLogsProviderBase instance.
		///</summary>
		public virtual ApssLogsProviderBase ApssLogsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesAuditProviderBase instance.
		///</summary>
		public virtual CompaniesAuditProviderBase CompaniesAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AuditFileVaultProviderBase instance.
		///</summary>
		public virtual AuditFileVaultProviderBase AuditFileVaultProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsEmailRecipientTypeProviderBase instance.
		///</summary>
		public virtual CsmsEmailRecipientTypeProviderBase CsmsEmailRecipientTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AuditFileVaultTableProviderBase instance.
		///</summary>
		public virtual AuditFileVaultTableProviderBase AuditFileVaultTableProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiProviderBase instance.
		///</summary>
		public virtual KpiProviderBase KpiProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsFileProviderBase instance.
		///</summary>
		public virtual CsmsFileProviderBase CsmsFileProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultSubCategoryProviderBase instance.
		///</summary>
		public virtual FileVaultSubCategoryProviderBase FileVaultSubCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiAuditProviderBase instance.
		///</summary>
		public virtual KpiAuditProviderBase KpiAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultCategoryProviderBase instance.
		///</summary>
		public virtual FileVaultCategoryProviderBase FileVaultCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileDbMedicalTrainingProviderBase instance.
		///</summary>
		public virtual FileDbMedicalTrainingProviderBase FileDbMedicalTrainingProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultAuditProviderBase instance.
		///</summary>
		public virtual FileVaultAuditProviderBase FileVaultAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultProviderBase instance.
		///</summary>
		public virtual FileVaultProviderBase FileVaultProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiProjectListProviderBase instance.
		///</summary>
		public virtual KpiProjectListProviderBase KpiProjectListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultTableProviderBase instance.
		///</summary>
		public virtual FileVaultTableProviderBase FileVaultTableProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiProjectsProviderBase instance.
		///</summary>
		public virtual KpiProjectsProviderBase KpiProjectsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PrivilegeProviderBase instance.
		///</summary>
		public virtual PrivilegeProviderBase PrivilegeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiPurchaseOrderListProviderBase instance.
		///</summary>
		public virtual KpiPurchaseOrderListProviderBase KpiPurchaseOrderListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireActionLogProviderBase instance.
		///</summary>
		public virtual QuestionnaireActionLogProviderBase QuestionnaireActionLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current PermissionProviderBase instance.
		///</summary>
		public virtual PermissionProviderBase PermissionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireAuditProviderBase instance.
		///</summary>
		public virtual QuestionnaireAuditProviderBase QuestionnaireAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailTemplateAttachmentProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailTemplateAttachmentProviderBase AdminTaskEmailTemplateAttachmentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current News2ProviderBase instance.
		///</summary>
		public virtual News2ProviderBase News2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current MonthsProviderBase instance.
		///</summary>
		public virtual MonthsProviderBase MonthsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileDbAuditProviderBase instance.
		///</summary>
		public virtual FileDbAuditProviderBase FileDbAuditProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current NewsProviderBase instance.
		///</summary>
		public virtual NewsProviderBase NewsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileDbAdminProviderBase instance.
		///</summary>
		public virtual FileDbAdminProviderBase FileDbAdminProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EhsConsultantProviderBase instance.
		///</summary>
		public virtual EhsConsultantProviderBase EhsConsultantProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiIssueProviderBase instance.
		///</summary>
		public virtual EbiIssueProviderBase EbiIssueProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiDailyReportProviderBase instance.
		///</summary>
		public virtual EbiDailyReportProviderBase EbiDailyReportProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiMetricProviderBase instance.
		///</summary>
		public virtual EbiMetricProviderBase EbiMetricProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CustomReportingLayoutsProviderBase instance.
		///</summary>
		public virtual CustomReportingLayoutsProviderBase CustomReportingLayoutsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiProviderBase instance.
		///</summary>
		public virtual EbiProviderBase EbiProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DocumentsProviderBase instance.
		///</summary>
		public virtual DocumentsProviderBase DocumentsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EhsimsExceptionsProviderBase instance.
		///</summary>
		public virtual EhsimsExceptionsProviderBase EhsimsExceptionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current DocumentsDownloadLogProviderBase instance.
		///</summary>
		public virtual DocumentsDownloadLogProviderBase DocumentsDownloadLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ElmahErrorProviderBase instance.
		///</summary>
		public virtual ElmahErrorProviderBase ElmahErrorProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EscalationChainSafetyRolesProviderBase instance.
		///</summary>
		public virtual EscalationChainSafetyRolesProviderBase EscalationChainSafetyRolesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailLogProviderBase instance.
		///</summary>
		public virtual EmailLogProviderBase EmailLogProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileDbProviderBase instance.
		///</summary>
		public virtual FileDbProviderBase FileDbProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EscalationChainSafetyProviderBase instance.
		///</summary>
		public virtual EscalationChainSafetyProviderBase EscalationChainSafetyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EscalationChainProcurementRolesProviderBase instance.
		///</summary>
		public virtual EscalationChainProcurementRolesProviderBase EscalationChainProcurementRolesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailLogTypeProviderBase instance.
		///</summary>
		public virtual EmailLogTypeProviderBase EmailLogTypeProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EscalationChainProcurementProviderBase instance.
		///</summary>
		public virtual EscalationChainProcurementProviderBase EscalationChainProcurementProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EnumApprovalProviderBase instance.
		///</summary>
		public virtual EnumApprovalProviderBase EnumApprovalProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current YearlyTargetsProviderBase instance.
		///</summary>
		public virtual YearlyTargetsProviderBase YearlyTargetsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EnumQuestionnaireRiskRatingProviderBase instance.
		///</summary>
		public virtual EnumQuestionnaireRiskRatingProviderBase EnumQuestionnaireRiskRatingProvider{get {throw new NotImplementedException();}}
		
		
		///<summary>
		/// Current CurrentCompaniesEhsConsultantIdProviderBase instance.
		///</summary>
		public virtual CurrentCompaniesEhsConsultantIdProviderBase CurrentCompaniesEhsConsultantIdProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrentQuestionnaireProcurementContactProviderBase instance.
		///</summary>
		public virtual CurrentQuestionnaireProcurementContactProviderBase CurrentQuestionnaireProcurementContactProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrentQuestionnaireProcurementFunctionalManagerProviderBase instance.
		///</summary>
		public virtual CurrentQuestionnaireProcurementFunctionalManagerProviderBase CurrentQuestionnaireProcurementFunctionalManagerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CurrentSmpEhsConsultantIdProviderBase instance.
		///</summary>
		public virtual CurrentSmpEhsConsultantIdProviderBase CurrentSmpEhsConsultantIdProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrphanCompaniesEhsConsultantIdProviderBase instance.
		///</summary>
		public virtual OrphanCompaniesEhsConsultantIdProviderBase OrphanCompaniesEhsConsultantIdProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrphanQuestionnaireProcurementContactProviderBase instance.
		///</summary>
		public virtual OrphanQuestionnaireProcurementContactProviderBase OrphanQuestionnaireProcurementContactProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrphanQuestionnaireProcurementFunctionalManagerProviderBase instance.
		///</summary>
		public virtual OrphanQuestionnaireProcurementFunctionalManagerProviderBase OrphanQuestionnaireProcurementFunctionalManagerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current OrphanSmpEhsConsultantIdProviderBase instance.
		///</summary>
		public virtual OrphanSmpEhsConsultantIdProviderBase OrphanSmpEhsConsultantIdProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailRecipientsListProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailRecipientsListProviderBase AdminTaskEmailRecipientsListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailTemplateListProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailTemplateListProviderBase AdminTaskEmailTemplateListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskEmailTemplateListWithAttachmentsProviderBase instance.
		///</summary>
		public virtual AdminTaskEmailTemplateListWithAttachmentsProviderBase AdminTaskEmailTemplateListWithAttachmentsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current AdminTaskHistoryProviderBase instance.
		///</summary>
		public virtual AdminTaskHistoryProviderBase AdminTaskHistoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesActiveProviderBase instance.
		///</summary>
		public virtual CompaniesActiveProviderBase CompaniesActiveProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesEhsConsultantsProviderBase instance.
		///</summary>
		public virtual CompaniesEhsConsultantsProviderBase CompaniesEhsConsultantsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesEhsimsMapSitesEhsimsIhsListProviderBase instance.
		///</summary>
		public virtual CompaniesEhsimsMapSitesEhsimsIhsListProviderBase CompaniesEhsimsMapSitesEhsimsIhsListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompaniesHrCrpDataListAllProviderBase instance.
		///</summary>
		public virtual CompaniesHrCrpDataListAllProviderBase CompaniesHrCrpDataListAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryStandardDetailsProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryStandardDetailsProviderBase CompanySiteCategoryStandardDetailsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryStandardNamesProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryStandardNamesProviderBase CompanySiteCategoryStandardNamesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CompanySiteCategoryStandardRegionProviderBase instance.
		///</summary>
		public virtual CompanySiteCategoryStandardRegionProviderBase CompanySiteCategoryStandardRegionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current ConfigSa812SitesProviderBase instance.
		///</summary>
		public virtual ConfigSa812SitesProviderBase ConfigSa812SitesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsaCompanySiteCategoryProviderBase instance.
		///</summary>
		public virtual CsaCompanySiteCategoryProviderBase CsaCompanySiteCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current CsmsEmailLogListProviderBase instance.
		///</summary>
		public virtual CsmsEmailLogListProviderBase CsmsEmailLogListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiLastTimeOnSiteProviderBase instance.
		///</summary>
		public virtual EbiLastTimeOnSiteProviderBase EbiLastTimeOnSiteProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiLastTimeOnSiteDistinctProviderBase instance.
		///</summary>
		public virtual EbiLastTimeOnSiteDistinctProviderBase EbiLastTimeOnSiteDistinctProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EbiLastTimeOnSiteDistinctAnySiteProviderBase instance.
		///</summary>
		public virtual EbiLastTimeOnSiteDistinctAnySiteProviderBase EbiLastTimeOnSiteDistinctAnySiteProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EhsimsExceptionsCompaniesSitesProviderBase instance.
		///</summary>
		public virtual EhsimsExceptionsCompaniesSitesProviderBase EhsimsExceptionsCompaniesSitesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailLogAutoProviderBase instance.
		///</summary>
		public virtual EmailLogAutoProviderBase EmailLogAutoProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EmailLogIhsProviderBase instance.
		///</summary>
		public virtual EmailLogIhsProviderBase EmailLogIhsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EnumQuestionnaireMainAnswerProviderBase instance.
		///</summary>
		public virtual EnumQuestionnaireMainAnswerProviderBase EnumQuestionnaireMainAnswerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EnumQuestionnaireMainQuestionProviderBase instance.
		///</summary>
		public virtual EnumQuestionnaireMainQuestionProviderBase EnumQuestionnaireMainQuestionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current EnumQuestionnaireMainQuestionAnswerProviderBase instance.
		///</summary>
		public virtual EnumQuestionnaireMainQuestionAnswerProviderBase EnumQuestionnaireMainQuestionAnswerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileDbUsersCompaniesSitesProviderBase instance.
		///</summary>
		public virtual FileDbUsersCompaniesSitesProviderBase FileDbUsersCompaniesSitesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultListProviderBase instance.
		///</summary>
		public virtual FileVaultListProviderBase FileVaultListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultListHelpFilesProviderBase instance.
		///</summary>
		public virtual FileVaultListHelpFilesProviderBase FileVaultListHelpFilesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultListSqExemptionProviderBase instance.
		///</summary>
		public virtual FileVaultListSqExemptionProviderBase FileVaultListSqExemptionProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultTableListProviderBase instance.
		///</summary>
		public virtual FileVaultTableListProviderBase FileVaultTableListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current FileVaultTableListHelpFilesProviderBase instance.
		///</summary>
		public virtual FileVaultTableListHelpFilesProviderBase FileVaultTableListHelpFilesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiCompanySiteCategoryProviderBase instance.
		///</summary>
		public virtual KpiCompanySiteCategoryProviderBase KpiCompanySiteCategoryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiEngineeringProjectHoursProviderBase instance.
		///</summary>
		public virtual KpiEngineeringProjectHoursProviderBase KpiEngineeringProjectHoursProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiListProviderBase instance.
		///</summary>
		public virtual KpiListProviderBase KpiListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiProjectListOpenProviderBase instance.
		///</summary>
		public virtual KpiProjectListOpenProviderBase KpiProjectListOpenProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiPurchaseOrderListOpenProjectsAscProviderBase instance.
		///</summary>
		public virtual KpiPurchaseOrderListOpenProjectsAscProviderBase KpiPurchaseOrderListOpenProjectsAscProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current KpiPurchaseOrderListOpenProjectsDistinctAscProviderBase instance.
		///</summary>
		public virtual KpiPurchaseOrderListOpenProjectsDistinctAscProviderBase KpiPurchaseOrderListOpenProjectsDistinctAscProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireActionLogFriendlyProviderBase instance.
		///</summary>
		public virtual QuestionnaireActionLogFriendlyProviderBase QuestionnaireActionLogFriendlyProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireContactProcurementProviderBase instance.
		///</summary>
		public virtual QuestionnaireContactProcurementProviderBase QuestionnaireContactProcurementProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireEscalationHsAssessorListProviderBase instance.
		///</summary>
		public virtual QuestionnaireEscalationHsAssessorListProviderBase QuestionnaireEscalationHsAssessorListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireEscalationProcurementListProviderBase instance.
		///</summary>
		public virtual QuestionnaireEscalationProcurementListProviderBase QuestionnaireEscalationProcurementListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireMainQuestionAnswerProviderBase instance.
		///</summary>
		public virtual QuestionnaireMainQuestionAnswerProviderBase QuestionnaireMainQuestionAnswerProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnairePresentlyWithUsersActionsProviderBase instance.
		///</summary>
		public virtual QuestionnairePresentlyWithUsersActionsProviderBase QuestionnairePresentlyWithUsersActionsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportDaysSinceActivityProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportDaysSinceActivityProviderBase QuestionnaireReportDaysSinceActivityProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportExpiryProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportExpiryProviderBase QuestionnaireReportExpiryProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportOverviewProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportOverviewProviderBase QuestionnaireReportOverviewProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportOverviewSubQuery1ProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportOverviewSubQuery1ProviderBase QuestionnaireReportOverviewSubQuery1Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportOverviewSubQuery2ProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportOverviewSubQuery2ProviderBase QuestionnaireReportOverviewSubQuery2Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportOverviewSubQuery3ProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportOverviewSubQuery3ProviderBase QuestionnaireReportOverviewSubQuery3Provider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportOverviewSubContractorsProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportOverviewSubContractorsProviderBase QuestionnaireReportOverviewSubContractorsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportProgressProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportProgressProviderBase QuestionnaireReportProgressProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesProviderBase QuestionnaireReportServicesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesAllProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesAllProviderBase QuestionnaireReportServicesAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesListProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesListProviderBase QuestionnaireReportServicesListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesListAllProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesListAllProviderBase QuestionnaireReportServicesListAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesOtherProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesOtherProviderBase QuestionnaireReportServicesOtherProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportServicesOtherAllProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportServicesOtherAllProviderBase QuestionnaireReportServicesOtherAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportTimeDelayAssessmentCompleteProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportTimeDelayAssessmentCompleteProviderBase QuestionnaireReportTimeDelayAssessmentCompleteProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportTimeDelayBeingAssessedProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportTimeDelayBeingAssessedProviderBase QuestionnaireReportTimeDelayBeingAssessedProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireReportTimeDelayWithSupplierProviderBase instance.
		///</summary>
		public virtual QuestionnaireReportTimeDelayWithSupplierProviderBase QuestionnaireReportTimeDelayWithSupplierProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireWithLocationApprovalViewProviderBase instance.
		///</summary>
		public virtual QuestionnaireWithLocationApprovalViewProviderBase QuestionnaireWithLocationApprovalViewProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current QuestionnaireWithLocationApprovalViewLatestProviderBase instance.
		///</summary>
		public virtual QuestionnaireWithLocationApprovalViewLatestProviderBase QuestionnaireWithLocationApprovalViewLatestProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SitesEhsimsIhsListProviderBase instance.
		///</summary>
		public virtual SitesEhsimsIhsListProviderBase SitesEhsimsIhsListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SqExemptionFriendlyAllProviderBase instance.
		///</summary>
		public virtual SqExemptionFriendlyAllProviderBase SqExemptionFriendlyAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SqExemptionFriendlyCurrentProviderBase instance.
		///</summary>
		public virtual SqExemptionFriendlyCurrentProviderBase SqExemptionFriendlyCurrentProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current SqExemptionFriendlyExpiredProviderBase instance.
		///</summary>
		public virtual SqExemptionFriendlyExpiredProviderBase SqExemptionFriendlyExpiredProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersAlcoanProviderBase instance.
		///</summary>
		public virtual UsersAlcoanProviderBase UsersAlcoanProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersCompaniesProviderBase instance.
		///</summary>
		public virtual UsersCompaniesProviderBase UsersCompaniesProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersCompaniesActiveAllProviderBase instance.
		///</summary>
		public virtual UsersCompaniesActiveAllProviderBase UsersCompaniesActiveAllProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersCompaniesActiveContractorsProviderBase instance.
		///</summary>
		public virtual UsersCompaniesActiveContractorsProviderBase UsersCompaniesActiveContractorsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersEhsConsultantsProviderBase instance.
		///</summary>
		public virtual UsersEhsConsultantsProviderBase UsersEhsConsultantsProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersFullNameProviderBase instance.
		///</summary>
		public virtual UsersFullNameProviderBase UsersFullNameProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersHsAssessorListWithLocationProviderBase instance.
		///</summary>
		public virtual UsersHsAssessorListWithLocationProviderBase UsersHsAssessorListWithLocationProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProcurementEscalationListProviderBase instance.
		///</summary>
		public virtual UsersProcurementEscalationListProviderBase UsersProcurementEscalationListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProcurementListProviderBase instance.
		///</summary>
		public virtual UsersProcurementListProviderBase UsersProcurementListProvider{get {throw new NotImplementedException();}}
		
		///<summary>
		/// Current UsersProcurementListWithLocationProviderBase instance.
		///</summary>
		public virtual UsersProcurementListWithLocationProviderBase UsersProcurementListWithLocationProvider{get {throw new NotImplementedException();}}



        ///<summary>
        ///By Vikas Naidu(TCS)///
        
        /// Current TwentyOnePointAudit19ProviderBase instance.
        ///</summary>
        public virtual KpiHelpFilesProviderBase KpiHelpFilesProvider { get { throw new NotImplementedException(); } }


        ///<summary>
        /// Current ContractReviewsRegisterProviderBase instance.
        /// By Bishwajit Sahoo(TCS)
        ///</summary>
        public virtual ContractReviewsRegisterProviderBase ContractReviewsRegisterProvider { get { throw new NotImplementedException(); } }

        ///<summary>
        /// Current UserCompanyProviderBase instance.
        /// Bishwajit Sahoo(TCS)
        ///</summary>
        public virtual UserCompanyProviderBase UserCompanyProvider { get { throw new NotImplementedException(); } }


        ///<summary>
        /// Current UsersCompaniesMapProviderBase instance.
        /// Bishwajit Sahoo(TCS)
        ///</summary>
        public virtual UsersCompaniesMapProviderBase UsersCompaniesMapProvider { get { throw new NotImplementedException(); } }

        ///<summary>
        /// Current CompanyNoteProviderBase instance.
        ///</summary>
        public virtual CompanyNoteProviderBase CompanyNoteProvider { get { throw new NotImplementedException(); } }
    
    }
}
