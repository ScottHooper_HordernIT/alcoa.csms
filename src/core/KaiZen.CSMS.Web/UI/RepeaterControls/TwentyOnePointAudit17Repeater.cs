﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit17Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit17RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit17RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit17RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit17Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit17Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit17Repeater z = (TwentyOnePointAudit17Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit17Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit17RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit17Repeater runat=\"server\"></{0}:TwentyOnePointAudit17Repeater>")]
	public class TwentyOnePointAudit17Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit17Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit17Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit17Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit17Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit17Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit17Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit17Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit17;
						TwentyOnePointAudit17Item container = new TwentyOnePointAudit17Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit17Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit17 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit17Item"/> class.
        /// </summary>
		public TwentyOnePointAudit17Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit17Item"/> class.
        /// </summary>
		public TwentyOnePointAudit17Item(KaiZen.CSMS.Entities.TwentyOnePointAudit17 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved17a
        /// </summary>
        /// <value>The Achieved17a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17a
		{
			get { return _entity.Achieved17a; }
		}
        /// <summary>
        /// Gets the Achieved17b
        /// </summary>
        /// <value>The Achieved17b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17b
		{
			get { return _entity.Achieved17b; }
		}
        /// <summary>
        /// Gets the Achieved17c
        /// </summary>
        /// <value>The Achieved17c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17c
		{
			get { return _entity.Achieved17c; }
		}
        /// <summary>
        /// Gets the Achieved17d
        /// </summary>
        /// <value>The Achieved17d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17d
		{
			get { return _entity.Achieved17d; }
		}
        /// <summary>
        /// Gets the Achieved17e
        /// </summary>
        /// <value>The Achieved17e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17e
		{
			get { return _entity.Achieved17e; }
		}
        /// <summary>
        /// Gets the Achieved17f
        /// </summary>
        /// <value>The Achieved17f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17f
		{
			get { return _entity.Achieved17f; }
		}
        /// <summary>
        /// Gets the Achieved17g
        /// </summary>
        /// <value>The Achieved17g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17g
		{
			get { return _entity.Achieved17g; }
		}
        /// <summary>
        /// Gets the Achieved17h
        /// </summary>
        /// <value>The Achieved17h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17h
		{
			get { return _entity.Achieved17h; }
		}
        /// <summary>
        /// Gets the Achieved17i
        /// </summary>
        /// <value>The Achieved17i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17i
		{
			get { return _entity.Achieved17i; }
		}
        /// <summary>
        /// Gets the Achieved17j
        /// </summary>
        /// <value>The Achieved17j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17j
		{
			get { return _entity.Achieved17j; }
		}
        /// <summary>
        /// Gets the Achieved17k
        /// </summary>
        /// <value>The Achieved17k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17k
		{
			get { return _entity.Achieved17k; }
		}
        /// <summary>
        /// Gets the Achieved17l
        /// </summary>
        /// <value>The Achieved17l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17l
		{
			get { return _entity.Achieved17l; }
		}
        /// <summary>
        /// Gets the Achieved17m
        /// </summary>
        /// <value>The Achieved17m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved17m
		{
			get { return _entity.Achieved17m; }
		}
        /// <summary>
        /// Gets the Observation17a
        /// </summary>
        /// <value>The Observation17a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17a
		{
			get { return _entity.Observation17a; }
		}
        /// <summary>
        /// Gets the Observation17b
        /// </summary>
        /// <value>The Observation17b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17b
		{
			get { return _entity.Observation17b; }
		}
        /// <summary>
        /// Gets the Observation17c
        /// </summary>
        /// <value>The Observation17c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17c
		{
			get { return _entity.Observation17c; }
		}
        /// <summary>
        /// Gets the Observation17d
        /// </summary>
        /// <value>The Observation17d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17d
		{
			get { return _entity.Observation17d; }
		}
        /// <summary>
        /// Gets the Observation17e
        /// </summary>
        /// <value>The Observation17e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17e
		{
			get { return _entity.Observation17e; }
		}
        /// <summary>
        /// Gets the Observation17f
        /// </summary>
        /// <value>The Observation17f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17f
		{
			get { return _entity.Observation17f; }
		}
        /// <summary>
        /// Gets the Observation17g
        /// </summary>
        /// <value>The Observation17g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17g
		{
			get { return _entity.Observation17g; }
		}
        /// <summary>
        /// Gets the Observation17h
        /// </summary>
        /// <value>The Observation17h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17h
		{
			get { return _entity.Observation17h; }
		}
        /// <summary>
        /// Gets the Observation17i
        /// </summary>
        /// <value>The Observation17i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17i
		{
			get { return _entity.Observation17i; }
		}
        /// <summary>
        /// Gets the Observation17j
        /// </summary>
        /// <value>The Observation17j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17j
		{
			get { return _entity.Observation17j; }
		}
        /// <summary>
        /// Gets the Observation17k
        /// </summary>
        /// <value>The Observation17k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17k
		{
			get { return _entity.Observation17k; }
		}
        /// <summary>
        /// Gets the Observation17l
        /// </summary>
        /// <value>The Observation17l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17l
		{
			get { return _entity.Observation17l; }
		}
        /// <summary>
        /// Gets the Observation17m
        /// </summary>
        /// <value>The Observation17m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation17m
		{
			get { return _entity.Observation17m; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit17"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit17 Entity
        {
            get { return _entity; }
        }
	}
}
