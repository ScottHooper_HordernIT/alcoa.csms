using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
//Added By Bishwajit for Item#31
using KaiZen.CSMS.Web.Data;
//End Added By Bishwajit for Item#31

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_MR : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    Hashtable htReadFiles = new Hashtable();
    bool unread = false;
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

        if (!postBack)
        {
            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
				//Added By Bishwajit for Item#31
                Session["DocumentsList"] = null;
				//End Added By Bishwajit for Item#31
                grid.SettingsBehavior.AllowSort = false;
                grid.SettingsBehavior.AllowGroup = false;

                SessionHandler.ViewMode = "View";
                Session["MR"] = "MR";

                DataView dvReadCD = (DataView)sqldsReadCD.Select(DataSourceSelectArguments.Empty);
                foreach (DataRow dr in dvReadCD.Table.Rows)
                {
                    htReadFiles.Add(dr["MustReadDownloaded"].ToString(), "1");
                }
				//Added By Bishwajit for Item#31
                Session["HashTableReadDownLoaded"] = htReadFiles;
				//End Added By Bishwajit for Item#31
                string Region = "";
                int _SiteId = Convert.ToInt32(SessionHandler.spVar_SiteId);

                RegionsService rService = new RegionsService();
                Regions rList_WAO = rService.GetByRegionInternalName("WAO");
                Regions rList_VICOPS = rService.GetByRegionInternalName("VICOPS");
                Regions rList_MINES = rService.GetByRegionInternalName("MINES");
                int? _RegionId_WAO = rList_WAO.RegionId;
                int? _RegionId_VICOPS = rList_VICOPS.RegionId;
                int? _RegionId_MINES = rList_MINES.RegionId;

                if (_SiteId > 0)
                {
                    int vCount = 0;
                    int wCount = 0;

                    RegionsSitesService rsService = new RegionsSitesService();
                    TList<RegionsSites> rsList = rsService.GetBySiteId(_SiteId);
                    foreach (RegionsSites rs in rsList)
                    {
                        if (rs.RegionId == _RegionId_VICOPS) vCount++;
                        if (rs.RegionId == _RegionId_WAO) wCount++;
                    }

                    if (wCount != 0 || vCount != 0)
                    {
                        if (wCount == 0) Region = "VIC";
                        if (vCount == 0) Region = "WAO";
                    }
                }
                else
                {
                    if (_SiteId == -1 * _RegionId_MINES) Region = "WAO";
                    if (_SiteId == -1 * _RegionId_WAO) Region = "WAO";
                    if (_SiteId == -1 * _RegionId_VICOPS) Region = "VIC";
                }

                if (!String.IsNullOrEmpty(Region))
                {
                    if (Region == "VIC") Session["Documents_Region"] = _RegionId_VICOPS;
                    if (Region == "WAO") Session["Documents_Region"] = _RegionId_WAO;
                    DocumentsDataSource.DataBind();
					//Added By Bishwajit for Item#31
                    TList<Documents> DocumentsList = (TList<Documents>)DocumentsDataSource.Select(DataSourceSelectArguments.Empty);
                    Session["DocumentsList"] = DocumentsList;
                    //End Added By Bishwajit for Item#31
					grid.DataBind();
                }
				//Added By Bishwajit for Item#31
                else
                {
                    //DocumentsFilter df1 = new DocumentsFilter();
                   // df1.Column[];
                    TList<Documents> DocumentsList = (TList<Documents>)DocumentsDataSource.Select(DataSourceSelectArguments.Empty);
                    Session["DocumentsList"] = DocumentsList;
                    
                }
				//End Added By Bishwajit for Item#31

            }
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    // do something
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }
        }
    }


    protected void grid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }
    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e) //Default Values 
    {
        e.NewValues["ApssDocumentType"] = "MR";
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;
        Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblRead") as Label;
        string fileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");

        if (label != null)
        {
            if (htReadFiles.ContainsKey(fileName))
            {
                label.Text = "Read";
                label.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                label.Text = "UnRead";
                label.ForeColor = System.Drawing.Color.Red;
                unread = true;
            }
        }
    }

    public int getExpected()
    {
        if (unread == false) { return 1; } else { return 0; };
    }
}
