﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="printSQ" Codebehind="printSQ.aspx.cs" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <table width="100%">
        <tr width="100%">
            <td align="center" style="text-align: center">
                <strong><span style="font-size: 16pt; color: maroon">Complete Safety Qualification Questionnaire
                    - Printable Version</span></strong>
            </td>
        </tr>
        <tr width="100%">
            <td align="center" style="text-align: center">
                <dx:ASPxHyperLink ID="hlGoBack" runat="server" Text="(Click here to go back to the main questionnaire page view).">
                </dx:ASPxHyperLink>
            </td>
        </tr>
        <tr>
            <br />
            <td align="center" style="text-align: center">
                <input id="btnPrint" onclick="window.print();" type="button" value="Print" style="width: 50px;
                    height: 29px;" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    <asp:PlaceHolder ID="phQuestionnaire" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phProcurement" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phSupplier" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification1" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification2" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification4" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification5" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification6" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification7" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phVerification8" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bodybottom" runat="Server">
    <%: System.Web.Optimization.Scripts.Render("~/bundles/countdown_timer_scripts") %>
</asp:Content>
