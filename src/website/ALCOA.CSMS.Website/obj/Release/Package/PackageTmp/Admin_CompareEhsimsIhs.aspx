﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" Inherits="Admin_CompareEhsimsIhs" Codebehind="Admin_CompareEhsimsIhs.aspx.cs" %>

<%@ Register Src="UserControls/Admin/EhsimsCompare.ascx" TagName="EhsimsCompare"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="900" bgcolor="#E9EFFF">
        <tr>
            <td rowspan="6" style="width: 900; height: 20px;" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
                    <tr>
                        <td style="width: 900px; height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 900px; height: 10px">
                            <span class="bodycopy"><span class="title">Administration Control Panel</span><br />
                                <asp:SiteMapPath ID="SiteMapPath1" runat="server" Font-Bold="True" RenderCurrentNodeAsLink="True">
                                    <CurrentNodeStyle Font-Bold="True" Font-Names="Verdana" />
                                </asp:SiteMapPath>
                                <br />
                                <img height="1" src="images/grfc_dottedline.gif" width="24" />
                                &nbsp;<br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 900px; height: 10px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 900px; height: 100%;">
                            <uc1:EhsimsCompare ID="EhsimsCompare1" runat="server" />
                            <br />
                            <asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
