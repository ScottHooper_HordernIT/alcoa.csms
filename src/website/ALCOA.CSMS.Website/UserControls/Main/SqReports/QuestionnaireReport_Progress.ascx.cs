using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Drawing;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;

public partial class UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Progress : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        SessionHandler.spVar_Questionnaire_FrontPageLink = "QuestionnaireReport_Progress";

        if (auth.RoleId == (int)RoleList.Contractor)
        {
            QuestionnaireReportProgressDataSource.Filter = "CompanyId = " + auth.CompanyId.ToString();
            grid.Columns["Company"].Visible = false;
            QuestionnaireReportProgressDataSource.DataBind();
        }

        if (!postBack)
        {
            cbFilterCompany.DataSourceID = "QuestionnaireReportProgressDataSource_FilterCompanies";
            cbFilterCompany.TextField = "CompanyName";
            cbFilterCompany.ValueField = "CompanyId";
            cbFilterCompany.DataBind();
            cbFilterCompany.Items.Add("-----------------------------", 0);
            cbFilterCompany.Items.Add("All", -1);
            cbFilterCompany.SelectedIndex = -1;
            cbFilterCompany.Text = "All";

            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
            {
                if (auth.RoleId == (int)RoleList.Reader)
                {
                    if (!Helper.General.isEhsConsultant(auth.UserId) && !Helper.General.isPrivilegedUser(auth.UserId) && !Helper.General.isProcurementUser(auth.UserId))
                    {
                        if (Helper.General.isSQReadAccessUser(auth.UserId)) grid.Columns["QuestionnaireId"].Visible = false;
                    }

                    //[CSMS-140] Modifications to Status Report - Remove 'Review' button for Procurement Users
                    //if (Helper.General.isProcurementUser(auth.UserId))
                    //{
                    //    grid.Columns["QuestionnaireId"].Visible = false;
                    //}
                }

                // Export
                String exportFileName = @"ALCOA CSMS - Safety Qualification Report - Status"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);

                

                //cbFilterType.DataSourceID = "QuestionnaireReportProgressDataSource_FilterType";
                //cbFilterType.TextField = "Type";
                //cbFilterType.ValueField = "Type";
                //cbFilterType.DataBind();
                //cbFilterType.Items.Add("-----------------------------", 0);
                //cbFilterType.Items.Add("All", -1);
                //cbFilterType.Value = "Contractor";
                //cbFilterType.Text = "Contractor";

                btnFilter.Enabled = true;

                try
                {
                    if (Request.QueryString["ID"] != null)
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                        {
                            LoadLayout(Convert.ToInt32(Request.QueryString["ID"]));
                        }
                        else
                        {
                            cbFilterCompany.Text = "All";
                        }
                    }
                    else
                    {
                        cbFilterCompany.Text = "All";
                    }
                }
                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }
        }
    }

    protected void grid_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            if (e.DataColumn == null) return;
            if (e.DataColumn.FieldName != "Recommended") return;

            if (e.DataColumn.FieldName == "Recommended")
            {
                if(e.GetValue("Recommended") != null)
                {
                    bool value = (bool)e.GetValue("Recommended");
                    if (value == true)
                    {
                        e.Cell.ForeColor = Color.Green;
                    }
                    else
                    {
                        e.Cell.ForeColor = Color.Red;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

            HyperLink hlAssess = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlAssess") as HyperLink;
            if (hlAssess != null)
            {
                int QuestionnaireId = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");
                hlAssess.NavigateUrl = String.Format("../../../SafetyPQ_QuestionnaireModify.aspx{0}",
                                        QueryStringModule.Encrypt("q=" + QuestionnaireId.ToString()));

            }

            HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "RiskLevel") as HyperLink;
            int? score = null;
            try
            {
                if (grid.GetRowValues(e.VisibleIndex, "MainScorePOverall") != null)
                    score = (int)grid.GetRowValues(e.VisibleIndex, "MainScorePOverall");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            }
            int id = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");

            if (score != null)
            {
                hl.Text = (string)grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");
                hl.NavigateUrl = String.Format("javascript:popUp('./PopUps/SafetyPQ_QuestionnaireReport.aspx?id={0}');", id);
            }
            else
            {
                hl.Text = "";
            }

            //string mainAssessmentStatus = "";

            //if (grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating") != DBNull.Value)
            //{
            //    mainAssessmentStatus = (string)grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");
            //}

            //if (!String.IsNullOrEmpty(mainAssessmentStatus))
            //{
            //    Label lblFinalRiskRating = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "FinalRiskLevel") as Label;
            //    lblFinalRiskRating.Text = "Incomplete"; //default value.

            //    int status = (int)grid.GetRowValues(e.VisibleIndex, "Status");
            //    if (status == (int)QuestionnaireStatusList.AssessmentComplete)
            //    {
            //        string ProcurementRiskRating = (string)grid.GetRowValues(e.VisibleIndex, "InitialRiskAssessment");
            //        string SupplierRiskRating = mainAssessmentStatus;
            //        if (!String.IsNullOrEmpty(ProcurementRiskRating))
            //        {
            //            lblFinalRiskRating.Text = ProcurementRiskRating;
            //        }
            //        if (!String.IsNullOrEmpty(SupplierRiskRating))
            //        {
            //            int _ProcurementRiskRating = Helper.Questionnaire.getRiskLevel(ProcurementRiskRating);
            //            int _SupplierRiskRating = Helper.Questionnaire.getRiskLevel(SupplierRiskRating);

            //            if (_SupplierRiskRating > _ProcurementRiskRating)
            //            {
            //                lblFinalRiskRating.Text = SupplierRiskRating;
            //            }
            //        }
            //    }
            //}
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
    }

    protected int convertObjToInt32(object o)
    {
        int i = 0;
        try
        {
            i = Convert.ToInt32(o);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            i = 0;
        }
        return i;
    }


    protected void btnFilter_Click(object sender, EventArgs e)
    {
        //int count = 0;
        //QuestionnaireReport
        //QuestionnaireReportOverviewService qroService = new QuestionnaireReportOverviewService();
        //QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();

        int CompanyId = convertObjToInt32(cbFilterCompany.SelectedItem.Value);
        //string Type = convertObjToInt32(cbFilterType.SelectedItem.Value);
        if (CompanyId > 0)
        {
            grid.FilterExpression = "CompanyId = " + CompanyId;
            grid.DataBind();
        }
        else
        {
            grid.FilterExpression = "";
            grid.DataBind();
        }


        ////Sites Filter - HardCoded...
        //if (convertObjToInt32(cbFilterSite.SelectedItem.Value) > 0)
        //{
        //    qroFilters.Clear();
        //    qroFilters.Junction = string.Empty; // This prevents the ParameterBuilder from throwing an "AND" before next line's output

        //    //Sites
        //    int SiteId = convertObjToInt32(cbFilterSite.SelectedItem.Value);
        //    SitesService sService = new SitesService();
        //    Sites s = sService.GetBySiteId(SiteId);
        //    qroFilters.BeginGroup();
        //    switch (s.SiteAbbrev)
        //    {
        //        case "KWI":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Kwi, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Kwi);
        //            Session["QuestionnaireReportOverview_ColSite"] = "KWI";
        //            break;
        //        case "PIN":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pin, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pin);
        //            Session["QuestionnaireReportOverview_ColSite"] = "PIN";
        //            break;
        //        case "WGP":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
        //            Session["QuestionnaireReportOverview_ColSite"] = "WGP";
        //            break;
        //        case "HUN":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Hun, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Hun);
        //            Session["QuestionnaireReportOverview_ColSite"] = "HUN";
        //            break;
        //        case "WDL":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wdl, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wdl);
        //            Session["QuestionnaireReportOverview_ColSite"] = "WDL";
        //            break;
        //        case "FML":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Fml, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Fml);
        //            Session["QuestionnaireReportOverview_ColSite"] = "FML";
        //            break;
        //        case "BUN":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bun, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bun);
        //            Session["QuestionnaireReportOverview_ColSite"] = "BUN";
        //            break;
        //        case "BGN":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bgn, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bgn);
        //            Session["QuestionnaireReportOverview_ColSite"] = "BGN";
        //            break;
        //        case "ANG":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ang, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ang);
        //            Session["QuestionnaireReportOverview_ColSite"] = "ANG";
        //            break;
        //        case "PTH":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pth, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pth);
        //            Session["QuestionnaireReportOverview_ColSite"] = "PTH";
        //            break;
        //        case "PTD": //Portland
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ptl, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ptl);
        //            Session["QuestionnaireReportOverview_ColSite"] = "PTL";
        //            break;
        //        case "CE": //Central Engineering
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ce, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ce);
        //            Session["QuestionnaireReportOverview_ColSite"] = "CE";
        //            break;
        //        case "PNJ": //Peel
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pel, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pel);
        //            Session["QuestionnaireReportOverview_ColSite"] = "PEEL";
        //            break;
        //        case "YEN":
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Yen, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Yen);
        //            Session["QuestionnaireReportOverview_ColSite"] = "Yen - ARP";
        //            break;
        //        case "KPH": //ARP Point Henry
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Arp, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Arp);
        //            Session["QuestionnaireReportOverview_ColSite"] = "PTH - ARP";
        //            break;
        //    }

        //    qroFilters.Junction = "AND";
        //    qroFilters.EndGroup();
        //}
        //else if (convertObjToInt32(cbFilterSite.SelectedItem.Value) < 0)
        //{
        //    //Regions
        //    int RegionId = -1 * convertObjToInt32(cbFilterSite.SelectedItem.Value);

        //    int _s = convertObjToInt32(cbFilterSite.SelectedItem.Value);
        //    bool VICOPS = false;
        //    bool WAO = false;
        //    if (_s == -2)
        //    {
        //        VICOPS = true;
        //        WAO = true;
        //        Session["QuestionnaireReportOverview_Region"] = "AU";
        //    }
        //    if (_s == -1)
        //    {
        //        WAO = true;
        //        Session["QuestionnaireReportOverview_Region"] = "WAO";
        //    }
        //    if (_s == -4)
        //    {
        //        VICOPS = true;
        //        Session["QuestionnaireReportOverview_Region"] = "VICOPS";
        //    }

        //    if (WAO && VICOPS)
        //    {
        //        //do nothing yeh;
        //        int sa = 1;
        //    }
        //    else
        //    {
        //        qroFilters.Clear();
        //        qroFilters.Junction = string.Empty; // This prevents the ParameterBuilder from throwing an "AND" before next line's output
        //        qroFilters.BeginGroup();
        //        if (WAO)
        //        {
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.BeginGroup();
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Kwi, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Kwi);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pin, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pin);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Hun, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Hun);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wdl, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wdl);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Fml, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Fml);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bun, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bun);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bgn, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bgn);
        //            qroFilters.EndGroup();
        //        }
        //        if (VICOPS)
        //        {
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.BeginGroup();
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ang, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ang);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pth, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pth);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ptl, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ptl);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ce, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ce);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pel, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pel);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Yen, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Yen);
        //            qroFilters.EndGroup();

        //            //qroFilters.Junction = "AND";
        //            qroFilters.BeginGroup();
        //            qroFilters.Junction = string.Empty;
        //            qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Arp, "-");
        //            qroFilters.Junction = "OR";
        //            qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Arp);
        //            qroFilters.EndGroup();
        //        }

        //        qroFilters.Junction = "AND";
        //        qroFilters.EndGroup();
        //    }
        //}

        //if (cbFilterType.SelectedItem.Value != null)
        //{
        //    if (cbFilterType.SelectedItem.Value.ToString() == "Sub Contractor" || cbFilterType.SelectedItem.Value.ToString() == "Contractor")
        //    {
        //        qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.Type, cbFilterType.SelectedItem.Value.ToString());
        //    }
        //}
        //if (cbFilterService.SelectedItem.Value != null)
        //{
        //    if (convertObjToInt32(cbFilterService.SelectedItem.Value) > 0)
        //    {
        //        qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.TypeOfService, cbFilterService.Value.ToString());
        //    }
        //}
        //else
        //{
        //    qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.TypeOfService);
        //}

        //if (convertObjToInt32(cbFilterCompany.SelectedItem.Value) > 0)
        //{
        //    qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.CompanyId, cbFilterCompany.Value.ToString());
        //}

        //VList<QuestionnaireReportOverview> qroVlist = DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 99999999, out count);
        //Session["vlQuestionnaireReportOverview"] = qroVlist;
        //DataSet ds = qroVlist.ToDataSet(false);
        //grid.DataSource = ds.Tables[0];
        //grid.DataBind();
        //ShowHideCols();
    }

    protected void grid2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["Area"] = "SQStatus";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(cbFilterCompany.SelectedItem.Value.ToString()))
        {
            e.NewValues["ReportDescription"] = cbFilterCompany.SelectedItem.Value.ToString();
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["Area"] = "SQStatus";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(cbFilterCompany.SelectedItem.Value.ToString()))
        {
            e.NewValues["ReportDescription"] = cbFilterCompany.SelectedItem.Value.ToString();
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["Area"] = "SQStatus";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        if (!String.IsNullOrEmpty(cbFilterCompany.SelectedItem.Value.ToString()))
        {
            e.NewValues["ReportDescription"] = cbFilterCompany.SelectedItem.Value.ToString();
        }
        else
        {
            e.NewValues["ReportDescription"] = "";
        }
    }
    protected void grid2_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        //REQUIRED FIELDS (EVERYTHING ELSE IS DEFAULT TO 0)
        if (e.NewValues["ReportName"] == null) { e.Errors[ASPxGridView1.Columns["ReportName"]] = "Required Field."; }

        //int temp = 0;

        if (e.Errors.Count > 0) e.RowError = "Report Name can not be empty.";
    }
    protected void grid2_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        if (!ASPxGridView1.IsNewRowEditing)
        {
            ASPxGridView1.DoRowValidation();
        }
    }
    protected void grid2_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        int ReportId = (int)ASPxGridView1.GetRowValues(e.VisibleIndex, "ReportId");
        ASPxHyperLink hl = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex, null, "hlLoad") as ASPxHyperLink;
        if (hl != null)
        {
            hl.NavigateUrl = "../../../Reports_SafetyPQStatus.aspx" + QueryStringModule.Encrypt("ID=" + ReportId.ToString());
        }
    }

    protected void LoadLayout(int ReportId)
    {
        CustomReportingLayoutsService crlService = new CustomReportingLayoutsService();
        TList<CustomReportingLayouts> crlList = crlService.GetAll();
        TList<CustomReportingLayouts> crlList2 = crlList.FindAll(
            delegate(CustomReportingLayouts c2)
            {
                return
                    c2.ReportId == ReportId;
            }
        );
        string ReportName = "";
        if (crlList2.Count != 0)
        {
            foreach (CustomReportingLayouts crl in crlList2)
            {
                grid.LoadClientLayout(crl.ReportLayout);
                try
                {
                    if (!String.IsNullOrEmpty(crl.ReportDescription.ToString()))
                    {
                        ReportName = crl.ReportName;
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(Convert.ToInt32(crl.ReportDescription.ToString()));

                        if (c != null)
                        {
                            cbFilterCompany.Text = c.CompanyName;
                        }
                        else
                        {
                            cbFilterCompany.Text = "All";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                }


                lblLayout.Text = ReportName;
            }
        }
        else
        {
            lblLayout.Text = "(Default) | Selected Layout could not be loaded.";
        }
    }
}
