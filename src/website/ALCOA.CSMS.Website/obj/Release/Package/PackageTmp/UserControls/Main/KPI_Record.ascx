﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_KPI_Record"
    CodeBehind="KPI_Record.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<script language="javascript" type="text/javascript">
    function stopRKey(evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
    }

    document.onkeypress = stopRKey;


    if (needToConfirm) {
        var needToConfirm = false;

        var confirmMsg = "Are you sure want to navigate away from this page?\n\nYou have attempted to leave this page.  If you have made any changes to the fields without clicking the Save or Submit button, your changes will be lost.  Are you sure you want to exit this page?\n\nPress OK to continue, or Cancel to stay on the current page.";
        //window.onbeforeload = setNeedToConfirmTrue;

        function setNeedToConfirmTrue() {
            if (needToConfirm == false) {
                needToConfirm = true;
            }
        }
        window.onbeforeunload = confirmExit;
        function confirmExit() {
            if (needToConfirm)
                return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save or Submit button, your changes will be lost.  Are you sure you want to exit this page?";
        }
        function confirmSave() {
            needtoConfirm = false;
        }
    }
    function openPopup(popupURL) {
        needToConfirm = false;
        popUp(popupURL);
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server" RenderMode="Inline"
    Visible="True">
    <ContentTemplate>
        <table cellspacing="0" cellpadding="2" width="100%" border="0">
            <tbody>
                <tr>
                    <td style="height: 43px" class="pageName" colspan="3">
                        <span class="bodycopy"><span class="title">Monthly KPI Recording</span><br />
                            <span class="date">Employees On-Site Hours Reporting, Key Performance Indicators (KPI's)
                                &amp; Training Matrix</span><br />
                            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px; text-align: left" class="bodyText" valign="top" colspan="4"
                        rowspan="1">
                        <span class="rcchead"></span><span style="font-size: 10pt"></span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: left" class="bodyText" valign="top">
                        <span style="font-size: 11pt; color: navy"><strong>1. Select Site/Month</strong></span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: left" class="bodyText" valign="top">
                        <dxcp:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px">
                            <PanelCollection>
                                <dx:PanelContent runat="server">
                                    <table border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 110px; height: 28px; text-align: right">
                                                    <strong>Company Name:</strong>
                                                </td>
                                                <td style="height: 28px; text-align: left" colspan="2">
                                                    <dx:ASPxComboBox runat="server" EnableSynchronization="False" IncrementalFilteringMode="StartsWith"
                                                        ValueType="System.Int32" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ClientInstanceName="ddlCompanies" Width="350px" ID="ddlCompanies" OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" AutoPostBack="true">
                                                        <%-- <ClientSideEvents SelectedIndexChanged="function(s, e) {
	ddlSites.PerformCallback(s.GetValue());
}"></ClientSideEvents>--%>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="width: 135px; text-align: right" colspan="2" rowspan="3">
                                                    <dx:ASPxButton runat="server" Text="View/Enter/Update" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="140px" Height="63px"
                                                        Font-Bold="True" ID="btnSearchGo" OnClick="btnSearchGo_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                        <ClientSideEvents Click="function(s, e) {
	needToConfirm = true;
}" />
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; height: 15px; text-align: right">
                                                    <strong>Site:</strong>
                                                </td>
                                                <td style="font-size: 10pt; height: 15px; text-align: left" colspan="2">
                                                    <dx:ASPxComboBox runat="server" EnableSynchronization="False" IncrementalFilteringMode="StartsWith"
                                                        ValueType="System.Int32" TextField="SiteName" ValueField="SiteId" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ClientInstanceName="ddlSites"
                                                        ID="ddlSites" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" OnSelectedIndexChanged="ddlSites_SelectedIndexChanged" AutoPostBack="true">

                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                            </tr>
                                            <tr id="trCategory" runat="server" visible="false">
                                                <td style="width: 110px; height: 15px; text-align: right"></td>
                                                <td style="font-size: 10pt; height: 15px; text-align: left; color: Red" colspan="2">
                                                    <b>Residential Category: </b>
                                                    <dx:ASPxLabel ID="lblCategory" runat="server" Text="" ForeColor="Red"></dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 110px; height: 10px; text-align: right">
                                                    <strong>Month:</strong>
                                                </td>
                                                <td style="width: 90px; height: 10px; text-align: left">
                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                                                        ValueType="System.Int32" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Width="90px" ID="ddlMonth" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <Items>
                                                            <dx:ListEditItem Text="January" Value="1"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="February" Value="2"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="March" Value="3"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="April" Value="4"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="May" Value="5"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="June" Value="6"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="July" Value="7"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="August" Value="8"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="September" Value="9"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="October" Value="10"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="November" Value="11"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="December" Value="12"></dx:ListEditItem>
                                                        </Items>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td style="width: 260px; height: 10px; text-align: left">
                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Width="60px" ID="ddlYear" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </dx:PanelContent>
                            </PanelCollection>
                        </dxcp:ASPxCallbackPanel>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: left" class="bodyText" valign="top"></td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: left" class="bodyText" valign="top">
                        <span style="font-size: 11pt; color: navy"><strong>2. Enter/Update KPI Statistics </strong>
                            <span style="font-size: 10pt; color: black">(<span style="color: red">* Denotes mandatory
                                fields, Fill in "0" for no value</span>)</span></span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: center" class="bodyText" valign="top">
                        <asp:Label ID="Label1" Visible="False" runat="server" Text="No KPI statistics for this month have been submitted. Please enter and save."
                            ForeColor="Red" Font-Size="12pt" Font-Names="verdana,arial,sans-serif"></asp:Label><asp:Label
                                ID="lblKpiID" Visible="False" runat="server" ForeColor="Blue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="height: 17px; text-align: center" class="bodyText" valign="top">
                        <asp:Label ID="lblRestriction" Visible="False" runat="server" Text="" ForeColor="Red"
                            Font-Size="9pt" Font-Names="verdana,arial,sans-serif"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="border-right: #000000 1px solid; padding-right: 0px; border-top: #000000 1px solid; padding-left: 0px; background: #eeeeee; padding-bottom: 0px; border-left: #000000 1px solid; padding-top: 0px; border-bottom: #000000 1px solid">
                            <div style="visibility: visible" id="sResults">
                                <asp:Panel ID="panelKPI_Record" runat="server" Width="100%" Height="100%">
                                    <asp:Panel ID="panelKPIinfo" runat="server" Width="100%" Height="100%">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="width: 298px; height: 15px; text-align: right" colspan="1">
                                                        <strong>Company Name:</strong>
                                                    </td>
                                                    <td style="width: 410px; height: 15px; text-align: left" colspan="2">
                                                        <strong>
                                                            <asp:Label ID="lbldCompanyname" runat="server" Text="Label" ForeColor="Navy" Font-Bold="False"></asp:Label></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 298px; height: 1px; text-align: right" colspan="1">
                                                        <strong>Site:</strong>
                                                    </td>
                                                    <td style="width: 410px; height: 1px; text-align: left" colspan="2">
                                                        <strong>
                                                            <asp:Label ID="lbldSite" runat="server" Text="Label" ForeColor="Navy" Font-Bold="False"></asp:Label></strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 298px; height: 1px; text-align: right" colspan="1">
                                                        <strong>Month:</strong>
                                                    </td>
                                                    <td style="width: 410px; height: 1px; text-align: left" colspan="2">
                                                        <strong>
                                                            <asp:Label ID="lbldTime" runat="server" Text="Label" ForeColor="Navy" Font-Bold="False"></asp:Label></strong>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table style="width: 100%">
                                            <tbody>
                                                <tr style="font-weight: bold; color: #333333">
                                                    <td style="text-align: left" colspan="2">
                                                        <table style="border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; font: 11px Tahoma; overflow: hidden; border-left: #4f93e3 1px solid; color: black; border-bottom: #4f93e3 1px solid; background-color: transparent">
                                                            <tbody>
                                                                <tr height="16" style="padding-right: 4px; padding-left: 4px; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid">
                                                                    <td colspan="3" style="height: 16px; text-align: center">
                                                                        <span style="color: red"><strong>Please round up Employees On-Site Hours to the nearest
                                                                            hour</strong></span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="width: 397px; height: 16px;">
                                                                        <strong>Labour</strong>
                                                                    </td>
                                                                    <td style="width: 120px; height: 16px;">
                                                                        <strong></strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 16px;"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px" align="left">&nbsp;General<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px">
                                                                        <asp:TextBox ID="tbGeneral" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" SetFocusOnError="True"
                                                                            ErrorMessage="General Field is a mandatory field" ControlToValidate="tbGeneral"
                                                                            EnableClientScript="false">
								*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="text-align: left" colspan="3">
                                                                        <strong>Projects (Capital)</strong>&nbsp; <a href="javascript:popUp('PopUps/KpiProjectsList.aspx');">(Click Here for the Complete Projects List and Purchase Order Numbers List)</a><br />
                                                                        <%-- Jolly - Spec- 50 --%>
                                                                        <div style="float: left;">Note:</div>
                                                                        <div style="float: left;">
                                                                            <ul style="margin-left: 0px;">
                                                                                <li>Please enter either the Project Number or Purchase Order Number, if entering in project hours.</li>
                                                                                <li>If Purchase Order Number is closed please use Project Number. If unknown, please contact your Procurement Personnel.</li>
                                                                            </ul>
                                                                        </div>
                                                                        <%-- Jolly - Spec- 50 --%>
                                                                    </td>
                                                                    <%--<td style="width: 518px; text-align: left" colspan="1">
                                                                    </td>--%>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">
                                                                        <dx:ASPxPageControl ID="pcProject1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            Style="text-align: center">
                                                                            <TabPages>
                                                                                <dx:TabPage Text="Alcoa Project Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>1 - Project Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbProjectNumber1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="170px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                                <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="text-align: right">1 - Purchase Order Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbPoNumber1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: right">1 - Line Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbLineNumber1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                            </TabPages>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <LoadingPanelStyle ImageSpacing="6px">
                                                                            </LoadingPanelStyle>
                                                                            <ContentStyle>
                                                                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </ContentStyle>
                                                                        </dx:ASPxPageControl>
                                                                        <span class="red">Selected Project #1: </span>
                                                                        <dx:ASPxLabel ID="lblSelectedProject1" runat="server" Text="(n/a)" CssClass="red">
                                                                        </dx:ASPxLabel>
                                                                        <br />
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            ID="tbProject1" MaxLength="10">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                ValidationGroup="save">
                                                                                <RegularExpression ErrorText="Project #1 - Entered Hours must be a number greater than 0."
                                                                                    ValidationExpression="^\d+$" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">
                                                                        <dx:ASPxPageControl ID="pcProject2" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            Style="text-align: center">
                                                                            <TabPages>
                                                                                <dx:TabPage Text="Alcoa Project Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>2 - Project Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbProjectNumber2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="170px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                                <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="text-align: right">2 - Purchase Order Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbPoNumber2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: right">2 - Line Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbLineNumber2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                            </TabPages>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <LoadingPanelStyle ImageSpacing="6px">
                                                                            </LoadingPanelStyle>
                                                                            <ContentStyle>
                                                                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </ContentStyle>
                                                                        </dx:ASPxPageControl>
                                                                        <span class="red">Selected Project #2: </span>
                                                                        <dx:ASPxLabel ID="lblSelectedProject2" runat="server" Text="(n/a)" CssClass="red">
                                                                        </dx:ASPxLabel>
                                                                        <br />
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            ID="tbProject2" MaxLength="10">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                ValidationGroup="save">
                                                                                <RegularExpression ErrorText="Project #2 - Entered Hours must be a number greater than 0."
                                                                                    ValidationExpression="^\d+$" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">
                                                                        <dx:ASPxPageControl ID="pcProject3" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            Style="text-align: center">
                                                                            <TabPages>
                                                                                <dx:TabPage Text="Alcoa Project Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl3" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>3 - Project Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbProjectNumber3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="170px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                                <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl4" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="text-align: right">3 - Purchase Order Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbPoNumber3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: right">3 - Line Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbLineNumber3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                            </TabPages>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <LoadingPanelStyle ImageSpacing="6px">
                                                                            </LoadingPanelStyle>
                                                                            <ContentStyle>
                                                                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </ContentStyle>
                                                                        </dx:ASPxPageControl>
                                                                        <span class="red">Selected Project #3: </span>
                                                                        <dx:ASPxLabel ID="lblSelectedProject3" runat="server" Text="(n/a)" CssClass="red">
                                                                        </dx:ASPxLabel>
                                                                        <br />
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            ID="tbProject3" MaxLength="10">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                ValidationGroup="save">
                                                                                <RegularExpression ErrorText="Project #3 - Entered Hours must be a number greater than 0."
                                                                                    ValidationExpression="^\d+$" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">
                                                                        <dx:ASPxPageControl ID="pcProject4" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            Style="text-align: center">
                                                                            <TabPages>
                                                                                <dx:TabPage Text="Alcoa Project Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl5" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>4 - Project Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbProjectNumber4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="170px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                                <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl6" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="text-align: right">4 - Purchase Order Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbPoNumber4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: right">4 - Line Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbLineNumber4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                            </TabPages>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <LoadingPanelStyle ImageSpacing="6px">
                                                                            </LoadingPanelStyle>
                                                                            <ContentStyle>
                                                                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </ContentStyle>
                                                                        </dx:ASPxPageControl>
                                                                        <span class="red">Selected Project #4: </span>
                                                                        <dx:ASPxLabel ID="lblSelectedProject4" runat="server" Text="(n/a)" CssClass="red">
                                                                        </dx:ASPxLabel>
                                                                        <br />
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            ID="tbProject4" MaxLength="10">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                ValidationGroup="save">
                                                                                <RegularExpression ErrorText="Project #4 - Entered Hours must be a number greater than 0."
                                                                                    ValidationExpression="^\d+$" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">
                                                                        <dx:ASPxPageControl ID="pcProject5" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            Style="text-align: center">
                                                                            <TabPages>
                                                                                <dx:TabPage Text="Alcoa Project Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl7" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td>5 - Project Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbProjectNumber5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="170px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                                <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                    <ContentCollection>
                                                                                        <dx:ContentControl ID="ContentControl8" runat="server" SupportsDisabledAttribute="True">
                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td style="text-align: right">5 - Purchase Order Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbPoNumber5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td style="text-align: right">5 - Line Number:&nbsp;
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <dx:ASPxTextBox ID="tbLineNumber5" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                            Width="130px">
                                                                                                            <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                ValidationGroup="save">
                                                                                                            </ValidationSettings>
                                                                                                        </dx:ASPxTextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </dx:ContentControl>
                                                                                    </ContentCollection>
                                                                                </dx:TabPage>
                                                                            </TabPages>
                                                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                            </LoadingPanelImage>
                                                                            <LoadingPanelStyle ImageSpacing="6px">
                                                                            </LoadingPanelStyle>
                                                                            <ContentStyle>
                                                                                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                            </ContentStyle>
                                                                        </dx:ASPxPageControl>
                                                                        <span class="red">Selected Project #5: </span>
                                                                        <dx:ASPxLabel ID="lblSelectedProject5" runat="server" Text="(n/a)" CssClass="red">
                                                                        </dx:ASPxLabel>
                                                                        <br />
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                            ID="tbProject5" MaxLength="10">
                                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                ValidationGroup="save">
                                                                                <RegularExpression ErrorText="Project #5 - Entered Hours must be a number greater than 0."
                                                                                    ValidationExpression="^\d+$" />
                                                                            </ValidationSettings>
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td colspan="3" width="100%">
                                                                        <asp:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                                                            TargetControlID="ContentPanel" ExpandControlID="TitlePanel" Collapsed="true"
                                                                            TextLabelID="TextToControl" ExpandedText="" CollapsedText="" ImageControlID="ImageToControl"
                                                                            ExpandedImage="~/Images/gvExpandedButton.png" CollapsedImage="~/Images/gvCollapsedButton.png"
                                                                            SuppressPostBack="False" CollapseControlID="TitlePanel">
                                                                        </asp:CollapsiblePanelExtender>
                                                                        <!-- Collapsible Panel Title Bar-->
                                                                        <div id="Div1" class="collapsePanel">
                                                                            <asp:Panel ID="TitlePanel" runat="server" Height="23px" Width="100%">
                                                                                <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                                                                    <div style="float: left; vertical-align: middle;">
                                                                                        <asp:Image ID="ImageToControl" runat="server" ImageUrl="~/Images/gvExpandedButton.png" />
                                                                                    </div>
                                                                                    <div style="float: left; font-size: 10pt">
                                                                                        <font color="red">Need to add more projects? Click here for space to add 10 more additional
                                                                                            projects...</font>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                        <!-- Collapsible Panel Target -->
                                                                        <div id="Div2" class="collapseTargetPanel">
                                                                            <asp:Panel ID="ContentPanel" runat="server" Height="50px" Width="100%">
                                                                                <table width="100%">
                                                                                    <tr style="height: 13px; background-color: #edf5ff">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject6" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl9" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>6 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl10" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">6 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">6 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #6: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject6" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject6" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #6 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px; height: 13px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: white">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject7" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl11" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>7 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl12" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">7 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">7 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #7: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject7" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject7" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #7 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: #edf5ff">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject8" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl13" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>8 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl14" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">8 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">8 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #8: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject8" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject8" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #8 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: white">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject9" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl15" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>9 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl16" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">9 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">9 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #9: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject9" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject9" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #9 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: #edf5ff">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject10" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl17" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>10 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl18" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">10 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">10 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #10: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject10" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject10" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #10 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: white">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject11" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl19" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>11 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber11" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl20" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">11 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber11" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">11 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber11" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #11: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject11" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject11" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #11 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: #edf5ff">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject12" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl21" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>12 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber12" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl22" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">12 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber12" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">12 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber12" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #12: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject12" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject12" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #12 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: white">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject13" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl23" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>13 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber13" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl24" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">13 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber13" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">13 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber13" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #13: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject13" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject13" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #13 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: #edf5ff">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject14" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl25" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>14 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber14" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl26" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">14 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber14" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">14 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber14" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #14: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject14" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject14" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #14 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr style="height: 13px; background-color: white">
                                                                                        <td style="width: 397px; height: 13px; text-align: left">
                                                                                            <dx:ASPxPageControl ID="pcProject15" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                Style="text-align: center">
                                                                                                <TabPages>
                                                                                                    <dx:TabPage Text="Alcoa Project Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl27" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td>15 - Project Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbProjectNumber15" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="170px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                    <dx:TabPage Text="Alcoa Purchase Order Number">
                                                                                                        <ContentCollection>
                                                                                                            <dx:ContentControl ID="ContentControl28" runat="server" SupportsDisabledAttribute="True">
                                                                                                                <table cellpadding="0" cellspacing="0">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">15 - Purchase Order Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbPoNumber15" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: right">15 - Line Number:&nbsp;
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <dx:ASPxTextBox ID="tbLineNumber15" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                                                Width="130px">
                                                                                                                                <ValidationSettings EnableCustomValidation="True" ErrorDisplayMode="ImageWithTooltip"
                                                                                                                                    ValidationGroup="save">
                                                                                                                                </ValidationSettings>
                                                                                                                            </dx:ASPxTextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </dx:ContentControl>
                                                                                                        </ContentCollection>
                                                                                                    </dx:TabPage>
                                                                                                </TabPages>
                                                                                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                                                </LoadingPanelImage>
                                                                                                <LoadingPanelStyle ImageSpacing="6px">
                                                                                                </LoadingPanelStyle>
                                                                                                <ContentStyle>
                                                                                                    <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                                                                                                </ContentStyle>
                                                                                            </dx:ASPxPageControl>
                                                                                            <span class="red">Selected Project #15: </span>
                                                                                            <dx:ASPxLabel ID="lblSelectedProject15" runat="server" Text="(n/a)" CssClass="red">
                                                                                            </dx:ASPxLabel>
                                                                                            <br />
                                                                                        </td>
                                                                                        <td style="width: 397px; height: 13px">
                                                                                            <dx:ASPxTextBox runat="server" Width="70px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                                                CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                                                ID="tbProject15" MaxLength="10">
                                                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="True"
                                                                                                    ValidationGroup="save">
                                                                                                    <RegularExpression ErrorText="Project #15 - Entered Hours must be a number greater than 0."
                                                                                                        ValidationExpression="^\d+$" />
                                                                                                </ValidationSettings>
                                                                                            </dx:ASPxTextBox>
                                                                                        </td>
                                                                                        <td style="width: 518px">
                                                                                            <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                                                Hours for Month</span></em>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="text-align: left" colspan="2">
                                                                        <strong>Western Australian Operations (WAO)</strong>
                                                                    </td>
                                                                    <td style="width: 518px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px">&nbsp;Engineering project work : Refinery<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px">
                                                                        <asp:TextBox ID="tbRefineryWork" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox><strong>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Refinery Work Field is a mandatory field"
                                                                                    ControlToValidate="tbRefineryWork" EnableClientScript="false">
						*</asp:RequiredFieldValidator></strong>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px">&nbsp;Engineering project work : Residue<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px">
                                                                        <asp:TextBox ID="tbResidueWork" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Residue Work Field is a mandatory field"
                                                                            ControlToValidate="tbResidueWork" EnableClientScript="false">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="text-align: left" colspan="2">
                                                                        <strong>Victorian Operations (VICOPS)</strong>
                                                                    </td>
                                                                    <td style="width: 518px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px">&nbsp;Engineering project work : Smelting<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px">
                                                                        <asp:TextBox ID="tbSmeltingWork" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator17"
                                                                                runat="server" ErrorMessage="Smelting Work Field is a mandatory field" ControlToValidate="tbSmeltingWork"
                                                                                EnableClientScript="false">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px">&nbsp;Engineering project work : Power<br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Generation<span
                                                                            style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px">
                                                                        <asp:TextBox ID="tbPowerGenerationWork" runat="server" Width="70px" Height="14px"
                                                                            CausesValidation="True" MaxLength="9"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                                                                ID="RequiredFieldValidator18" runat="server" ErrorMessage="Power Generation Work Field is a mandatory field"
                                                                                ControlToValidate="tbPowerGenerationWork" EnableClientScript="false">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="text-align: left" colspan="2">
                                                                        <strong>WAO - Other Hours</strong>
                                                                    </td>
                                                                    <td style="width: 518px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Calciner - Expense<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <asp:TextBox ID="tbCalcinerExpense" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Calciner - Expense Field  is a mandatory field"
                                                                            ControlToValidate="tbCalcinerExpense" EnableClientScript="false">
						*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; text-align: left">&nbsp;Calciner - Capital<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; color: #000000">
                                                                        <asp:TextBox ID="tbCalcinerCapital" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Calciner - Capital Field  is a mandatory field"
                                                                            ControlToValidate="tbCalcinerCapital" EnableClientScript="false">
						*</asp:RequiredFieldValidator><span style="color: #ff0000"></span>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; text-align: left">&nbsp;Residue Hours (Other)<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="font-size: 8pt; width: 397px; color: #000000; background-color: white">
                                                                        <asp:TextBox ID="tbResidue" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Residue Hours (Other)  is a mandatory field"
                                                                            ControlToValidate="tbResidue" EnableClientScript="false">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Employees On-Site
                                                                            Hours for Month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: white">
                                                                    <td style="font-size: 10pt; color: #008000; font-style: italic; font-family: Arial; height: 13px; background-color: palegoldenrod; text-align: right"
                                                                        colspan="3">
                                                                        <strong><span style="color: black">Total Employees On-Site Hours:
                                                                            <asp:Label ID="lblTotalManHours" Visible="true" runat="server" Font-Size="Larger"
                                                                                Font-Bold="True"></asp:Label></span></strong>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Employees</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Peak No. of Employees on
                                                                        <asp:Label ID="lblAlcoaPeakSite" runat="server" Text="site"></asp:Label><span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; color: #000000; height: 13px">
                                                                        <asp:TextBox ID="tbPeakNoEmp" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox><em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Peak No. of Employees employed on site for the month  Field  is a mandatory field"
                                                                                    ControlToValidate="tbPeakNoEmp" EnableClientScript="false">
						*</asp:RequiredFieldValidator></span></em>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Maximum number of employees on site for the month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Average No. of Employees on
                                                                        <asp:Label ID="lblAlcoaAdvancedSite" runat="server" Text="site"></asp:Label><span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; color: #000000; height: 13px">
                                                                        <asp:TextBox ID="tbAvgNoEmp" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Average No. of Employees employed on site for the month  Field  is a mandatory field"
                                                                            ControlToValidate="tbAvgNoEmp" EnableClientScript="false">
						*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Average number
                                                                            of employees for the month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="font-size: 10pt; font-family: Arial; height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 12px; text-align: left">&nbsp;No. of Employees working in excess of the 16/64 guidelines for the month<span
                                                                        style="color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; color: #000000; height: 12px">
                                                                        <asp:TextBox ID="tbEmployeesExcess" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="No. of Employees working in excess of the 16/64 policy for the month  Field  is a mandatory field"
                                                                            ControlToValidate="tbEmployeesExcess" EnableClientScript="false">
						*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="font-weight: bold; width: 518px; color: #000000; height: 12px"></td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Incident Performance (EHSIMS)</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;First Aid Treatment Injuries
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <asp:Label ID="lblipFATI" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Medical Treatment Injuries
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px">
                                                                        <asp:Label ID="lblipMTI" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Restricted Duty Injuries
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:Label ID="lblipRDI" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Lost Work Day Injuries
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <strong>
                                                                            <asp:Label ID="lblipLTI" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label></strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Injury Free Events
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:Label ID="lblipIFE" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <%--//Added By Sayani for task# 13--%>

                                                                <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Risk Notifications
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:Label ID="lblipRN" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>

                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px" colspan="2">
                                                                        <strong>EHS Performance</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;No. of Lost Work Days<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbehsNoLWD" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="No. of Lost Work Days  Field  is a mandatory field"
                                                                            ControlToValidate="tbehsNoLWD" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px"></td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;No. of Restricted Days<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbehspNoRD" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="No. of Restricted Days is a mandatory field"
                                                                            ControlToValidate="tbehspNoRD" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px"></td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Audits</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white" id="trJSAAudit" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;JSA Field Audit Verifications<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="JSALink" href="javascript:needToConfirm=false; openPopup('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>

                                                                    </td>
                                                                    <td style="width: 397px; color: #000000; height: 13px">
                                                                        <asp:TextBox ID="tbJSA" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvJSA" runat="server" ErrorMessage="JSA Field is a mandatory field"
                                                                            ControlToValidate="tbJSA" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Corrective Actions Outstanding<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 397px; color: #000000; height: 13px">
                                                                        <asp:TextBox ID="tbCorrective" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvCorrective" runat="server" ErrorMessage="Corrective Actions Outstanding is a mandatory field"
                                                                            ControlToValidate="tbCorrective" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; height: 13px"></td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16" id="trInspections" runat="server" visible="true">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Inspections</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: white" id="trWSC" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Workplace Safety &amp; Compliance<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="WSCLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="font-size: 8pt; width: 397px; color: #000000; font-family: Verdana; height: 13px; background-color: #edf5ff">
                                                                        <asp:TextBox ID="tbiWSC" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Workplace Safety & Compliance  Field  is a mandatory field"
                                                                            ControlToValidate="tbiWSC" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; font-family: Verdana; height: 13px; background-color: #edf5ff">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: arial">Number of Weekly
                                                                            conducted this month</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: #000000; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16" id="trObservations" runat="server" visible="true">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Observations</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white" id="trMHS" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 12px; text-align: left">&nbsp;Management Health Safety work contacts<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="MHLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="font-size: 8pt; width: 397px; color: #000000; font-family: Verdana; height: 12px">
                                                                        <asp:TextBox ID="tboNoHSWC" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox><strong>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="Health & Safety work standards  Field  is a mandatory field"
                                                                                    ControlToValidate="tboNoHSWC" EnableClientScript="false">
							*</asp:RequiredFieldValidator></strong>
                                                                    </td>
                                                                    <td style="width: 518px; color: #000000; font-family: Verdana; height: 12px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: arial">Number of Health
                                                                            and Safety Contacts made by the Management team</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="color: #000000; height: 13px; background-color: #edf5ff" id="trBO" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Behavioural Observations<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="BOLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tboNoBSP" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="Behavioural Observations  Field  is a mandatory field"
                                                                            ControlToValidate="tboNoBSP" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of Behavioural
                                                                            Observations</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px" colspan="2">
                                                                        <strong>Quality</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; background-color: palegoldenrod; text-align: left">&nbsp;Alcoa - Contractor Services Audit Score
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px; background-color: palegoldenrod; text-align: left">&nbsp;<asp:Label ID="lblqQAS" Visible="true" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 397px; height: 13px; background-color: palegoldenrod; text-align: left">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Conducted annually
                                                                            by Alcoa</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;No. of Non conformance issues<span style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbqNoNCI" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="No of Non conformance issues  Field  is a mandatory field"
                                                                            ControlToValidate="tbqNoNCI" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px"></td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px" colspan="2">
                                                                        <strong>Meetings</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Tool box meetings per month<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="TBMLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbmTbmpm" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Tool box meetings per month  Field  is a mandatory field"
                                                                            ControlToValidate="tbmTbmpm" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of Tool box meetings</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff" id="trAlcoaWCM" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Alcoa weekly contractors meeting<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="AWCLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbmAwcm" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Alcoa weekly contractors meeting  Field  is a mandatory field"
                                                                            ControlToValidate="tbmAwcm" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Attendance at
                                                                            the Alcoa weekly contractor meeting</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white" id="trAlcoaMCM" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Alcoa monthly contractors meeting<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="AMCLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbmAmcm" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Alcoa monthly contractors meeting  Field  is a mandatory field"
                                                                            ControlToValidate="tbmAmcm" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Attendance at
                                                                            the Alcoa monthly contractor meeting</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff" id="trFatality" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Fatality Prevention discussion and presentation<span style="font-size: 10pt; color: #ff0000">*</span>&nbsp;<a visible="true" runat="server" id="FPLink" href="javascript:popUp('PopUps/KpiHelpFilePopup.aspx?Help=Default');"><img src="Images/help.gif" alt="Help" /></a>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbFatality" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvFatality" runat="server" ErrorMessage="Fatality Prevention Field is a mandatory field"
                                                                            ControlToValidate="tbFatality" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Number of</span></em>
                                                                    </td>
                                                                </tr>

                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Mandated Training</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Percentage of your workforce who worked on the Alcoa site this month up to date with the mandated training<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbTraining" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="Training is a mandatory field"
                                                                            ControlToValidate="tbTraining" EnableClientScript="false">
											*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">Percentage of
                                                                            workforce up to date with mandated training</span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Fitness For Work</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Blood Alcohol Tests (Breathlyser)<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbBloodAlcoholTests" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"  ControlToValidate="tbBloodAlcoholTests" EnableClientScript="false"
                                                                            ErrorMessage="Blood Alcohol Tests (Breathlyser) is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            No. of breathlyser tests conducted for the month
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Positive BAC Results<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbPositiveBACResults" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"  ControlToValidate="tbPositiveBACResults" EnableClientScript="false"
                                                                            ErrorMessage="Positive BAC Results is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            No. of positive breathlyser tests in the month
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Drug Tests<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbDrugTests" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"  ControlToValidate="tbDrugTests" EnableClientScript="false"
                                                                            ErrorMessage="Drug Tests is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            No. of drug tests conducted for the month
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Non-Negative Results - Total<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbNonNegativeResults" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"  ControlToValidate="tbNonNegativeResults" EnableClientScript="false"
                                                                            ErrorMessage="Non-Negative Results - Total is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            No. of tests that returned a Non-Negative Result
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Positive Results - Medication Declared<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbPositiveResultsMedDeclared" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server"  ControlToValidate="tbPositiveResultsMedDeclared" EnableClientScript="false"
                                                                            ErrorMessage="Positive Results - Medication Declared is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            Positive test confirmed by Lab - Medication Declared
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Positive Results - Medication not Declared<span
                                                                        style="font-size: 10pt; color: #ff0000">*</span>
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:TextBox ID="tbPositiveResultsMedNotDeclared" runat="server" Width="70px" Height="14px" CausesValidation="True"
                                                                            MaxLength="9"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server"  ControlToValidate="tbPositiveResultsMedNotDeclared" EnableClientScript="false"
                                                                            ErrorMessage="Positive Results - Medication not Declared is a mandatory field">*</asp:RequiredFieldValidator>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                             Positive test confirmed by Lab - Medication Not Declared
                                                                        </span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="padding-right: 4px; padding-left: 4px; background: url(./App_Themes/Office2003BlueOld/GridView/gvGradient.gif) #94b6e8 repeat-x center top; padding-bottom: 8px; color: black; padding-top: 8px; border-bottom: #4f93e3 1px solid"
                                                                    height="16">
                                                                    <td style="height: 13px; text-align: left" colspan="2">
                                                                        <strong>Your Feedback</strong>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px; text-align: left" colspan="1"></td>
                                                                </tr>
                                                                <tr style="height: 100px; background-color: white">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Comments (Max. 1000 Characters)
                                                                    </td>
                                                                    <td style="height: 13px; text-align: center" colspan="2">
                                                                        <asp:TextBox ID="tbmtOthers" runat="server" Width="400px" Height="90px" MaxLength="1000"
                                                                            TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: #edf5ff" id="trSMPSubmitted" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Safety Plan Submitted for year
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:Label ID="lblSafetyPlanSubmitted" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial">
                                                                            <asp:Label ID="lblSafetyPlanSubmittedMsg" Visible="False" runat="server" Font-Size="10pt"
                                                                                Font-Bold="True">Your company has 
										not submitted an EHS plan to Alcoa for 
										the current year. Please go to 
										Safety/Safety Plans Submissions/Status 
										to do this.</asp:Label></span></em>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 13px; background-color: white" id="trSMPApproved" runat="server" visible="true">
                                                                    <td style="width: 397px; height: 13px; text-align: left">&nbsp;Safety Plan Approved for year
                                                                    </td>
                                                                    <td style="width: 120px; height: 13px">
                                                                        <asp:Label ID="lblSafetyPlanApproved" runat="server" Font-Size="Larger" Font-Bold="True"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 518px; height: 13px">
                                                                        <em><span style="font-size: 10pt; color: #008000; font-family: Arial"></span></em>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr style="font-weight: bold; color: #333333">
                                                    <td style="width: 241px; height: 18px">
                                                        <asp:Label ID="lblCode" Visible="False" runat="server" ForeColor="Red" Font-Size="16pt"></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblErrors" Visible="False" runat="server" Text="Some errors were encountered and the KPI entered was not saved. Please correct the errors below and try again."
                                                            ForeColor="Red"></asp:Label>
                                                    </td>
                                                    <td style="height: 18px; text-align: right" width="30%">
                                                        <dx:ASPxButton ID="btnSave" OnClick="btnSave_Click" runat="server" Width="167px"
                                                            Text="Save KPI Record" Height="77px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                            CssPostfix="Office2003Blue" Font-Bold="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                            <ClientSideEvents Click="function(s, e) {    
	e.processOnServer = confirm('Are you sure you wish to save your changes?');
}"></ClientSideEvents>
                                                        </dx:ASPxButton>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server"></asp:ValidationSummary>
                                        <dx:ASPxValidationSummary ID="ASPxValidationSummary2" runat="server" RenderMode="BulletedList"
                                            ShowErrorsInEditors="True">
                                        </dx:ASPxValidationSummary>
                                        <br />
                                    </asp:Panel>
                                    <br />
                                    <table style="width: 100%">
                                        <tbody>
                                            <tr>
                                                <td style="height: 18px; text-align: left" colspan="2">
                                                    <strong>Created by:</strong>
                                                    <asp:Label ID="lbldCreatedBy" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 1px; text-align: left" colspan="2">
                                                    <strong>Last Modified by:</strong>
                                                    <asp:Label ID="lbldLastModifiedBy" runat="server" ForeColor="Blue"></asp:Label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
            TargetControlID="tbGeneral" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
            TargetControlID="tbCalcinerExpense" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
            TargetControlID="tbCalcinerCapital" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
            TargetControlID="tbResidue" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
            TargetControlID="tbRefineryWork" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
            TargetControlID="tbResidueWork" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
            TargetControlID="tbSmeltingWork" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server"
            TargetControlID="tbPowerGenerationWork" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server"
            TargetControlID="tbPeakNoEmp" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7a" runat="server"
            TargetControlID="tbAvgNoEmp" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server"
            TargetControlID="tbEmployeesExcess" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server"
            TargetControlID="tbehsNoLWD" ValidChars="123456789." FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server"
            TargetControlID="tbehspNoRD" ValidChars="123456789." FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server"
            TargetControlID="tbiWSC" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server"
            TargetControlID="tboNoHSWC" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server"
            TargetControlID="tboNoBSP" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server"
            TargetControlID="tbqNoNCI" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
            TargetControlID="tbmTbmpm" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server"
            TargetControlID="tbmAwcm" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server"
            TargetControlID="tbmAmcm" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server"
            TargetControlID="tbFatality" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server"
            TargetControlID="tbTraining" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server"
            TargetControlID="tbBloodAlcoholTests" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server"
            TargetControlID="tbPositiveBACResults" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
            TargetControlID="tbDrugTests" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server"
            TargetControlID="tbNonNegativeResults" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server"
            TargetControlID="tbPositiveResultsMedDeclared" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server"
            TargetControlID="tbPositiveResultsMedNotDeclared" ValidChars="123456789" FilterType="Custom, Numbers">
        </asp:FilteredTextBoxExtender>
        <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
            SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </ContentTemplate>
</asp:UpdatePanel>
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dx:ASPxLabel runat="server" Text="You can not enter monthly KPI information for a future month."
                Height="30px" Font-Size="14px" ID="ASPxLabel1">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dx:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>
<dxcb:ASPxCallback ID="ASPxCallback1" runat="server">
</dxcb:ASPxCallback>
<%--<data:KpiProjectListOpenDataSource ID="dsKpiProjectListOpen" runat="server" SelectMethod="GetPaged" EnablePaging="true" EnableCaching="true" CacheDuration="30" >
    </data:KpiProjectListOpenDataSource>--%>
<asp:SqlDataSource ID="dsKpiProjectListOpen" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_KpiPurchaseOrderList_GetDistinct_ProjectNumberDesc_Open" SelectCommandType="StoredProcedure"
    CacheDuration="30" EnableCaching="true"></asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsProjectOpen" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_KpiPurchaseOrderList_GetDistinct_PurchaseOrderNumber_Open_List"
    SelectCommandType="StoredProcedure" CacheDuration="30" EnableCaching="true"></asp:SqlDataSource>
