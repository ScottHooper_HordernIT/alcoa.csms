﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiLastTimeOnSiteDistinctAnySiteProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EbiLastTimeOnSiteDistinctAnySiteProviderBaseCore : EntityViewProviderBase<EbiLastTimeOnSiteDistinctAnySite>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt;"/></returns>
		protected static VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt; Fill(DataSet dataSet, VList<EbiLastTimeOnSiteDistinctAnySite> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EbiLastTimeOnSiteDistinctAnySite>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EbiLastTimeOnSiteDistinctAnySite>"/></returns>
		protected static VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt; Fill(DataTable dataTable, VList<EbiLastTimeOnSiteDistinctAnySite> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EbiLastTimeOnSiteDistinctAnySite c = new EbiLastTimeOnSiteDistinctAnySite();
					c.LastSwipeDateTime = (Convert.IsDBNull(row["LastSwipeDateTime"]))?DateTime.MinValue:(System.DateTime?)row["LastSwipeDateTime"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EbiLastTimeOnSiteDistinctAnySite&gt;"/></returns>
		protected VList<EbiLastTimeOnSiteDistinctAnySite> Fill(IDataReader reader, VList<EbiLastTimeOnSiteDistinctAnySite> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EbiLastTimeOnSiteDistinctAnySite entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EbiLastTimeOnSiteDistinctAnySite>("EbiLastTimeOnSiteDistinctAnySite",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EbiLastTimeOnSiteDistinctAnySite();
					}
					
					entity.SuppressEntityEvents = true;

					entity.LastSwipeDateTime = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctAnySiteColumn.LastSwipeDateTime)))?null:(System.DateTime?)reader[((int)EbiLastTimeOnSiteDistinctAnySiteColumn.LastSwipeDateTime)];
					//entity.LastSwipeDateTime = (Convert.IsDBNull(reader["LastSwipeDateTime"]))?DateTime.MinValue:(System.DateTime?)reader["LastSwipeDateTime"];
					entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctAnySiteColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteDistinctAnySiteColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EbiLastTimeOnSiteDistinctAnySite entity)
		{
			reader.Read();
			entity.LastSwipeDateTime = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctAnySiteColumn.LastSwipeDateTime)))?null:(System.DateTime?)reader[((int)EbiLastTimeOnSiteDistinctAnySiteColumn.LastSwipeDateTime)];
			//entity.LastSwipeDateTime = (Convert.IsDBNull(reader["LastSwipeDateTime"]))?DateTime.MinValue:(System.DateTime?)reader["LastSwipeDateTime"];
			entity.CompanyName = (reader.IsDBNull(((int)EbiLastTimeOnSiteDistinctAnySiteColumn.CompanyName)))?null:(System.String)reader[((int)EbiLastTimeOnSiteDistinctAnySiteColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EbiLastTimeOnSiteDistinctAnySite entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LastSwipeDateTime = (Convert.IsDBNull(dataRow["LastSwipeDateTime"]))?DateTime.MinValue:(System.DateTime?)dataRow["LastSwipeDateTime"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EbiLastTimeOnSiteDistinctAnySiteFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteFilterBuilder : SqlFilterBuilder<EbiLastTimeOnSiteDistinctAnySiteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctAnySiteFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctAnySiteFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteFilterBuilder

	#region EbiLastTimeOnSiteDistinctAnySiteParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiLastTimeOnSiteDistinctAnySiteParameterBuilder : ParameterizedSqlFilterBuilder<EbiLastTimeOnSiteDistinctAnySiteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteParameterBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiLastTimeOnSiteDistinctAnySiteParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiLastTimeOnSiteDistinctAnySiteParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiLastTimeOnSiteDistinctAnySiteParameterBuilder
	
	#region EbiLastTimeOnSiteDistinctAnySiteSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiLastTimeOnSiteDistinctAnySite"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiLastTimeOnSiteDistinctAnySiteSortBuilder : SqlSortBuilder<EbiLastTimeOnSiteDistinctAnySiteColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiLastTimeOnSiteDistinctAnySiteSqlSortBuilder class.
		/// </summary>
		public EbiLastTimeOnSiteDistinctAnySiteSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiLastTimeOnSiteDistinctAnySiteSortBuilder

} // end namespace
