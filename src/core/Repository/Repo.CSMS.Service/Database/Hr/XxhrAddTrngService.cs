﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.Hr
{
    #region Interface

    public interface IXxhrAddTrngService
    {
        XXHR_ADD_TRNG Get(Expression<Func<XXHR_ADD_TRNG, bool>> where, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes);
        List<XXHR_ADD_TRNG> GetMany(int? top, Expression<Func<XXHR_ADD_TRNG, bool>> where, Func<IQueryable<XXHR_ADD_TRNG>, IOrderedQueryable<XXHR_ADD_TRNG>> orderBy, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes);
        List<XXHR_ADD_TRNG> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_ADD_TRNG, bool>> where, Func<IQueryable<XXHR_ADD_TRNG>, IOrderedQueryable<XXHR_ADD_TRNG>> orderBy, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<XXHR_ADD_TRNG, bool>> where, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes);
        void Insert(XXHR_ADD_TRNG item, bool saveChanges = true);
        void Update(XXHR_ADD_TRNG item, bool saveChanges = true);
        void Delete(XXHR_ADD_TRNG item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class XxhrAddTrngService : IXxhrAddTrngService
    {
        private readonly IRepository<XXHR_ADD_TRNG> _repository;

        public XxhrAddTrngService(IRepository<XXHR_ADD_TRNG> repository)
        {
            this._repository = repository;
        }

        public XXHR_ADD_TRNG Get(Expression<Func<XXHR_ADD_TRNG, bool>> where, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<XXHR_ADD_TRNG> GetMany(int? top, Expression<Func<XXHR_ADD_TRNG, bool>> where, Func<IQueryable<XXHR_ADD_TRNG>, IOrderedQueryable<XXHR_ADD_TRNG>> orderBy, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<XXHR_ADD_TRNG> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_ADD_TRNG, bool>> where, Func<IQueryable<XXHR_ADD_TRNG>, IOrderedQueryable<XXHR_ADD_TRNG>> orderBy, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<XXHR_ADD_TRNG, bool>> where, List<Expression<Func<XXHR_ADD_TRNG, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(XXHR_ADD_TRNG item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_ADD_TRNG");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(XXHR_ADD_TRNG item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_ADD_TRNG");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(XXHR_ADD_TRNG item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_ADD_TRNG");

            _repository.Delete(item);
            //_userRepository.ExecuteCustomFunction("spDeactivateEntity", new ObjectParameter("TableName", typeof(XXHR_ADD_TRNG).Name), new ObjectParameter("Value", user.ID));

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
