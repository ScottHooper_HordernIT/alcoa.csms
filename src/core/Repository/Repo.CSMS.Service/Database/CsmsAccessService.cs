﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICsmsAccessService
    {
        CsmsAccess Get(Expression<Func<CsmsAccess, bool>> where, List<Expression<Func<CsmsAccess, object>>> includes);
        List<CsmsAccess> GetMany(int? top, Expression<Func<CsmsAccess, bool>> where, Func<IQueryable<CsmsAccess>, IOrderedQueryable<CsmsAccess>> orderBy, List<Expression<Func<CsmsAccess, object>>> includes);
        List<CsmsAccess> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsAccess, bool>> where, Func<IQueryable<CsmsAccess>, IOrderedQueryable<CsmsAccess>> orderBy, List<Expression<Func<CsmsAccess, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CsmsAccess, bool>> where, List<Expression<Func<CsmsAccess, object>>> includes);
        void Insert(CsmsAccess item, bool saveChanges = true);
        void Update(CsmsAccess item, bool saveChanges = true);
        void Delete(CsmsAccess item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CsmsAccessService : ICsmsAccessService
    {
        private readonly IRepository<CsmsAccess> _repository;

        public CsmsAccessService(IRepository<CsmsAccess> repository)
        {
            this._repository = repository;
        }

        public CsmsAccess Get(Expression<Func<CsmsAccess, bool>> where, List<Expression<Func<CsmsAccess, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CsmsAccess> GetMany(int? top, Expression<Func<CsmsAccess, bool>> where, Func<IQueryable<CsmsAccess>, IOrderedQueryable<CsmsAccess>> orderBy, List<Expression<Func<CsmsAccess, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CsmsAccess> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CsmsAccess, bool>> where, Func<IQueryable<CsmsAccess>, IOrderedQueryable<CsmsAccess>> orderBy, List<Expression<Func<CsmsAccess, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CsmsAccess, bool>> where, List<Expression<Func<CsmsAccess, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CsmsAccess item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsAccess");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CsmsAccess item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsAccess");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CsmsAccess item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CsmsAccess");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
