<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Services_Main" Codebehind="QuestionnaireReport_Services_Main.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 875px">
    <tr>
        <td style="width: 874px; padding-bottom: 2px; text-align: center" class="pageName" align="right">
            <div align="right">
                <a style="color: #0000ff; text-align: right; text-decoration: none" href="javascript:ShowHideCustomizationWindow()">
                    Customize</a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                OnHtmlDataCellPrepared="grid_HtmlDataCellPrepared"
                OnHeaderFilterFillItems="grid_HeaderFilterFillItems"
                Width="100%">
                <Columns>
                    <dxwgv:GridViewDataTextColumn Caption="Services Category" FieldName="CategoryText"
                        VisibleIndex="0">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company" FieldName="CompanyName" SortIndex="0"
                        SortOrder="Ascending" VisibleIndex="1" Width="220px" Settings-SortMode="DisplayText">
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="ActiveSites" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <%--<dxwgv:GridViewDataComboBoxColumn FieldName="ActiveSites" Visible="true" VisibleIndex="2">
                        <PropertiesComboBox DataSourceID="SitesDataSource1" TextField="SiteName" IncrementalFilteringMode="Contains">
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" AllowHeaderFilter="false"></Settings>
                    </dxwgv:GridViewDataComboBoxColumn>--%>
                    <dxwgv:GridViewDataTextColumn FieldName="LevelOfSupervision" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Status" Visible="False" VisibleIndex="2">
                        <PropertiesComboBox DataSourceID="QuestionnaireStatusDataSource" TextField="QuestionnaireStatusDesc"
                            ValueField="QuestionnaireStatusId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="Recommended" Visible="False" VisibleIndex="3"
                        Width="85px">
                        <PropertiesComboBox ValueType="System.Int32">
                            <Items>
                                <dxe:ListEditItem Text="Yes" Value="1">
                                </dxe:ListEditItem>
                                <dxe:ListEditItem Text="No" Value="0">
                                </dxe:ListEditItem>
                            </Items>
                        </PropertiesComboBox>
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle HorizontalAlign="Center" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Created By User" FieldName="CreatedByUserId"
                        Visible="False" VisibleIndex="4">
                        <PropertiesComboBox DataSourceID="UsersFullNameDataSource" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="CreatedDate" Visible="False" VisibleIndex="5">
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Modified By User" FieldName="ModifiedByUserId"
                        Visible="False" VisibleIndex="6">
                        <PropertiesComboBox DataSourceID="UsersFullNameDataSource" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="ModifiedDate" Visible="False" VisibleIndex="7">
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Approved By User" FieldName="ApprovedByUserId"
                        Visible="False" VisibleIndex="8">
                        <PropertiesComboBox DataSourceID="UsersFullNameDataSource" TextField="UserFullName"
                            ValueField="UserId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataDateColumn FieldName="ApprovedDate" Visible="False" VisibleIndex="9">
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Process Status" FieldName="CompanyStatusDesc"
                        Visible="False" Width="120px">
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Type" Width="120px" Caption="Type">
                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior SortMode="DisplayText" ColumnResizeMode="NextColumn" />
                <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" ShowFilterRow="false"/>
                <SettingsCustomizationWindow Enabled="True" />
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003BlueOld/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                   <%-- <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">--%>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr align="right">
        <td align="right" class="pageName" colspan="7" style="height: 30px; text-align: right;
            text-align: -moz-right; width: 900px;">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
</table>
<%--<data:QuestionnaireReportServicesDataSource ID="QuestionnaireReportServicesDataSource"
    runat="server">
</data:QuestionnaireReportServicesDataSource>--%>
<data:UsersFullNameDataSource ID="UsersFullNameDataSource" runat="server">
</data:UsersFullNameDataSource>
<data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:QuestionnaireStatusDataSource>
<asp:SqlDataSource ID="SitesDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select SiteNameEbi from Sites where IsVisible=1 and SitenameEbi is not null order by SitenameEbi">
</asp:SqlDataSource>
