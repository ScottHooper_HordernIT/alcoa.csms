﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireActionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireActionProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireAction, KaiZen.CSMS.Entities.QuestionnaireActionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionKey key)
		{
			return Delete(transactionManager, key.QuestionnaireActionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireActionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireActionId)
		{
			return Delete(null, _questionnaireActionId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireActionId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireAction Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireActionKey key, int start, int pageLength)
		{
			return GetByQuestionnaireActionId(transactionManager, key.QuestionnaireActionId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="_questionnaireActionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(System.Int32 _questionnaireActionId)
		{
			int count = -1;
			return GetByQuestionnaireActionId(null,_questionnaireActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(System.Int32 _questionnaireActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireActionId(null, _questionnaireActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId)
		{
			int count = -1;
			return GetByQuestionnaireActionId(transactionManager, _questionnaireActionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireActionId(transactionManager, _questionnaireActionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(System.Int32 _questionnaireActionId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireActionId(null, _questionnaireActionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireActionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireAction GetByQuestionnaireActionId(TransactionManager transactionManager, System.Int32 _questionnaireActionId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(System.String _actionName)
		{
			int count = -1;
			return GetByActionName(null,_actionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(System.String _actionName, int start, int pageLength)
		{
			int count = -1;
			return GetByActionName(null, _actionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(TransactionManager transactionManager, System.String _actionName)
		{
			int count = -1;
			return GetByActionName(transactionManager, _actionName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(TransactionManager transactionManager, System.String _actionName, int start, int pageLength)
		{
			int count = -1;
			return GetByActionName(transactionManager, _actionName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(System.String _actionName, int start, int pageLength, out int count)
		{
			return GetByActionName(null, _actionName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAction index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_actionName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireAction GetByActionName(TransactionManager transactionManager, System.String _actionName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireAction&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireAction&gt;"/></returns>
		public static TList<QuestionnaireAction> Fill(IDataReader reader, TList<QuestionnaireAction> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireAction c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireAction")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireActionColumn.QuestionnaireActionId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireAction>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireAction",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireAction();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireActionId = (System.Int32)reader[((int)QuestionnaireActionColumn.QuestionnaireActionId - 1)];
					c.OriginalQuestionnaireActionId = c.QuestionnaireActionId;
					c.ActionName = (System.String)reader[((int)QuestionnaireActionColumn.ActionName - 1)];
					c.ActionDescription = (System.String)reader[((int)QuestionnaireActionColumn.ActionDescription - 1)];
					c.HistoryLogFormat = (System.String)reader[((int)QuestionnaireActionColumn.HistoryLogFormat - 1)];
					c.LogAction = (System.Boolean)reader[((int)QuestionnaireActionColumn.LogAction - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireAction entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireActionId = (System.Int32)reader[((int)QuestionnaireActionColumn.QuestionnaireActionId - 1)];
			entity.OriginalQuestionnaireActionId = (System.Int32)reader["QuestionnaireActionId"];
			entity.ActionName = (System.String)reader[((int)QuestionnaireActionColumn.ActionName - 1)];
			entity.ActionDescription = (System.String)reader[((int)QuestionnaireActionColumn.ActionDescription - 1)];
			entity.HistoryLogFormat = (System.String)reader[((int)QuestionnaireActionColumn.HistoryLogFormat - 1)];
			entity.LogAction = (System.Boolean)reader[((int)QuestionnaireActionColumn.LogAction - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireAction entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireActionId = (System.Int32)dataRow["QuestionnaireActionId"];
			entity.OriginalQuestionnaireActionId = (System.Int32)dataRow["QuestionnaireActionId"];
			entity.ActionName = (System.String)dataRow["ActionName"];
			entity.ActionDescription = (System.String)dataRow["ActionDescription"];
			entity.HistoryLogFormat = (System.String)dataRow["HistoryLogFormat"];
			entity.LogAction = (System.Boolean)dataRow["LogAction"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAction"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireAction Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAction entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireActionId methods when available
			
			#region QuestionnaireActionLogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireActionLogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireActionLogCollection = DataRepository.QuestionnaireActionLogProvider.GetByQuestionnaireActionId(transactionManager, entity.QuestionnaireActionId);

				if (deep && entity.QuestionnaireActionLogCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireActionLog>) DataRepository.QuestionnaireActionLogProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireAction object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireAction instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireAction Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAction entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireActionLog>
				if (CanDeepSave(entity.QuestionnaireActionLogCollection, "List<QuestionnaireActionLog>|QuestionnaireActionLogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireActionLog child in entity.QuestionnaireActionLogCollection)
					{
						if(child.QuestionnaireActionIdSource != null)
						{
							child.QuestionnaireActionId = child.QuestionnaireActionIdSource.QuestionnaireActionId;
						}
						else
						{
							child.QuestionnaireActionId = entity.QuestionnaireActionId;
						}

					}

					if (entity.QuestionnaireActionLogCollection.Count > 0 || entity.QuestionnaireActionLogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireActionLogProvider.Save(transactionManager, entity.QuestionnaireActionLogCollection);
						
						deepHandles.Add("QuestionnaireActionLogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireActionLog >) DataRepository.QuestionnaireActionLogProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireActionLogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireActionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireAction</c>
	///</summary>
	public enum QuestionnaireActionChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireAction</c> as OneToMany for QuestionnaireActionLogCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireActionLog>))]
		QuestionnaireActionLogCollection,
	}
	
	#endregion QuestionnaireActionChildEntityTypes
	
	#region QuestionnaireActionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionFilterBuilder : SqlFilterBuilder<QuestionnaireActionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilterBuilder class.
		/// </summary>
		public QuestionnaireActionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionFilterBuilder
	
	#region QuestionnaireActionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAction"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireActionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionParameterBuilder class.
		/// </summary>
		public QuestionnaireActionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireActionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireActionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireActionParameterBuilder
	
	#region QuestionnaireActionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireActionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAction"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireActionSortBuilder : SqlSortBuilder<QuestionnaireActionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionSqlSortBuilder class.
		/// </summary>
		public QuestionnaireActionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireActionSortBuilder
	
} // end namespace
