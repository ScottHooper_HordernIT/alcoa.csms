﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialContactStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialContactStatusProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatusKey key)
		{
			return Delete(transactionManager, key.QuestionnaireInitialContactStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireInitialContactStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireInitialContactStatusId)
		{
			return Delete(null, _questionnaireInitialContactStatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactStatusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactStatusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatusKey key, int start, int pageLength)
		{
			return GetByQuestionnaireInitialContactStatusId(transactionManager, key.QuestionnaireInitialContactStatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(System.Int32 _questionnaireInitialContactStatusId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactStatusId(null,_questionnaireInitialContactStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(System.Int32 _questionnaireInitialContactStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactStatusId(null, _questionnaireInitialContactStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactStatusId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactStatusId(transactionManager, _questionnaireInitialContactStatusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactStatusId(transactionManager, _questionnaireInitialContactStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(System.Int32 _questionnaireInitialContactStatusId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireInitialContactStatusId(null, _questionnaireInitialContactStatusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByQuestionnaireInitialContactStatusId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactStatusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(null,_statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(System.String _statusName, int start, int pageLength, out int count)
		{
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContactStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialContactStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialContactStatus&gt;"/></returns>
		public static TList<QuestionnaireInitialContactStatus> Fill(IDataReader reader, TList<QuestionnaireInitialContactStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialContactStatus")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialContactStatusColumn.QuestionnaireInitialContactStatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialContactStatus>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialContactStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireInitialContactStatusId = (System.Int32)reader[((int)QuestionnaireInitialContactStatusColumn.QuestionnaireInitialContactStatusId - 1)];
					c.StatusName = (System.String)reader[((int)QuestionnaireInitialContactStatusColumn.StatusName - 1)];
					c.StatusDesc = (System.String)reader[((int)QuestionnaireInitialContactStatusColumn.StatusDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireInitialContactStatusId = (System.Int32)reader[((int)QuestionnaireInitialContactStatusColumn.QuestionnaireInitialContactStatusId - 1)];
			entity.StatusName = (System.String)reader[((int)QuestionnaireInitialContactStatusColumn.StatusName - 1)];
			entity.StatusDesc = (System.String)reader[((int)QuestionnaireInitialContactStatusColumn.StatusDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireInitialContactStatusId = (System.Int32)dataRow["QuestionnaireInitialContactStatusId"];
			entity.StatusName = (System.String)dataRow["StatusName"];
			entity.StatusDesc = (System.String)dataRow["StatusDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireInitialContactStatusId methods when available
			
			#region QuestionnaireInitialContactCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactCollection = DataRepository.QuestionnaireInitialContactProvider.GetByContactStatusId(transactionManager, entity.QuestionnaireInitialContactStatusId);

				if (deep && entity.QuestionnaireInitialContactCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContact>) DataRepository.QuestionnaireInitialContactProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireInitialContact>
				if (CanDeepSave(entity.QuestionnaireInitialContactCollection, "List<QuestionnaireInitialContact>|QuestionnaireInitialContactCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContact child in entity.QuestionnaireInitialContactCollection)
					{
						if(child.ContactStatusIdSource != null)
						{
							child.ContactStatusId = child.ContactStatusIdSource.QuestionnaireInitialContactStatusId;
						}
						else
						{
							child.ContactStatusId = entity.QuestionnaireInitialContactStatusId;
						}

					}

					if (entity.QuestionnaireInitialContactCollection.Count > 0 || entity.QuestionnaireInitialContactCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactProvider.Save(transactionManager, entity.QuestionnaireInitialContactCollection);
						
						deepHandles.Add("QuestionnaireInitialContactCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContact >) DataRepository.QuestionnaireInitialContactProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialContactStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialContactStatus</c>
	///</summary>
	public enum QuestionnaireInitialContactStatusChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireInitialContactStatus</c> as OneToMany for QuestionnaireInitialContactCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContact>))]
		QuestionnaireInitialContactCollection,
	}
	
	#endregion QuestionnaireInitialContactStatusChildEntityTypes
	
	#region QuestionnaireInitialContactStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialContactStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusFilterBuilder : SqlFilterBuilder<QuestionnaireInitialContactStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactStatusFilterBuilder
	
	#region QuestionnaireInitialContactStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialContactStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactStatusParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialContactStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactStatusParameterBuilder
	
	#region QuestionnaireInitialContactStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialContactStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContactStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialContactStatusSortBuilder : SqlSortBuilder<QuestionnaireInitialContactStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactStatusSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialContactStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialContactStatusSortBuilder
	
} // end namespace
