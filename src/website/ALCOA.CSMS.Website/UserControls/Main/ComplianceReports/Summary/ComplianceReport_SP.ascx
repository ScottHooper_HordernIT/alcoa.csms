﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_SP" Codebehind="ComplianceReport_SP.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
     <span style="color: #000099">
     <strong>
            <br />
            <span style="font-size: 10pt" runat="server" id="Caption">
            Safety Management Plans</span></strong><br />
            </span>
    <dxwgv:ASPxGridView ID="grid"
                ClientInstanceName="grid"
                runat="server"
                AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue"
                Width="900px"
                OnHtmlRowCreated="grid_RowCreated" KeyFieldName="CompanyID"
                OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
             >
                <Columns>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyID" Name="CompanyBox"
                        VisibleIndex="0">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                   <%-- <dxwgv:GridViewDataTextColumn Caption="Safety Compliance Category" SortIndex="0" SortOrder="Ascending"
                                                    FieldName="CategoryDesc" VisibleIndex="1" >
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataTextColumn>--%>

                     <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" SortIndex="0" SortOrder="Ascending"
                     Settings-SortMode="DisplayText" Name="CategoryBox"
                        FieldName="CategoryId" VisibleIndex="1">
                        <PropertiesComboBox DataSourceID="SqldsCategoryList" DropDownHeight="150px" TextField="CategoryDesc"
                            ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>
                                                    
                    <dxwgv:GridViewDataTextColumn Name="Submitted" FieldName="Submitted" ReadOnly="True"
                 VisibleIndex="2" Width="100px">
                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td><asp:Image ID="changeImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     <td style="width:30px"><asp:Label ID="noSPSubmitted" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
                 <HeaderStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <Settings SortMode="DisplayText" />
             </dxwgv:GridViewDataTextColumn>

                    <dxwgv:GridViewDataComboBoxColumn Caption="Latest Plan Current Status" FieldName="StatusName" Name="StatusBox"
                        VisibleIndex="3" Width="150px">
                        <PropertiesComboBox DataSourceID="SafetyPlanStatusDataSource" DropDownHeight="150px" TextField="StatusName"
                            ValueField="StatusName" ValueType="System.String" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="False" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>


                            
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupedColumns="True" ShowFilterRow="True" />
                <SettingsPager Visible="False" PageSize="100" Mode="ShowAllRecords">
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True"/>
                <SettingsCookies CookiesID="NonComplianceSP" Version="0.1" />
        <SettingsText EmptyDataRow="No Safety Plan(s) submitted" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>

    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dxwgv:ASPxGridViewExporter>
            
                <asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
                    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
                </asp:SqlDataSource>
                
                <data:SafetyPlanStatusDataSource ID="SafetyPlanStatusDataSource" runat="server"
                    SelectMethod="GetPaged" EnableSorting="true" EnableCaching="true" EnableDeepLoad="False" Sort="StatusName ASC">
                </data:SafetyPlanStatusDataSource>
                <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<%--//Added for Task DT2416--%>
<asp:SqlDataSource ID="SqldsCategoryList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanySiteCategory_SiteCategory" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:Label ID="lblGood" runat="server" Text="0" Visible="false"></asp:Label>


