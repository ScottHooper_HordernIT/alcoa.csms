<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_ContractorServices" Codebehind="ContractorServices.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
    
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<span style="color: maroon"><strong>The "Big 8" Risk Categories are:<br />
    Falls, Mobile Equipment, Tagout/Lockout, Confined Space Entry, Electrical, Combustion
    Safety, Machine Guarding, Contractor Management.</strong></span><br />
<br />
<dxwgv:ASPxGridView ID="grid" runat="server" ClientInstanceName="grid"
    AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    DataSourceID="QuestionnaireServicesCategoryDataSource1" 
    KeyFieldName="CategoryId" Width="900px">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <AlternatingRow Enabled="True">
        </AlternatingRow>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <FilterRowButton Height="13px" Width="13px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <SettingsEditing EditFormColumnCount="1" Mode="Inline" />
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="70px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="CategoryId" ReadOnly="True" Visible="False"
            VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Service Name" FieldName="CategoryText" SortIndex="0"
            SortOrder="Ascending" VisibleIndex="1" Width="690px">
            <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Caption="Service Description" FieldName="CategoryDesc"
            Visible="False" VisibleIndex="2" Width="265px">
            <Settings AutoFilterCondition="Contains" FilterMode="DisplayText" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataCheckColumn FieldName="Visible" VisibleIndex="2" Width="65px">
        </dxwgv:GridViewDataCheckColumn>
        <dxwgv:GridViewDataCheckColumn FieldName="HighRisk" VisibleIndex="3" Width="75px">
        </dxwgv:GridViewDataCheckColumn>
    </Columns>
    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowFilterBar="Visible"/>
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>

<div align="right" style="padding-top: 2px;">
    <uc1:ExportButtons ID="ucExportButtons" runat="server" />
</div>

<data:QuestionnaireServicesCategoryDataSource ID="QuestionnaireServicesCategoryDataSource1"
    runat="server">
</data:QuestionnaireServicesCategoryDataSource>
