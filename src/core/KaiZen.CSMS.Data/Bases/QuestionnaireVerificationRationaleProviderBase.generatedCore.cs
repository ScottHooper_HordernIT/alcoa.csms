﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireVerificationRationaleProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireVerificationRationaleProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireVerificationRationale, KaiZen.CSMS.Entities.QuestionnaireVerificationRationaleKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationRationaleKey key)
		{
			return Delete(transactionManager, key.QuestionnaireVerificationRationale);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireVerificationRationale">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireVerificationRationale)
		{
			return Delete(null, _questionnaireVerificationRationale);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireVerificationRationale">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireVerificationRationale);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireVerificationRationale Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationRationaleKey key, int start, int pageLength)
		{
			return GetByQuestionnaireVerificationRationale(transactionManager, key.QuestionnaireVerificationRationale, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(System.Int32 _questionnaireVerificationRationale)
		{
			int count = -1;
			return GetByQuestionnaireVerificationRationale(null,_questionnaireVerificationRationale, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(System.Int32 _questionnaireVerificationRationale, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireVerificationRationale(null, _questionnaireVerificationRationale, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(TransactionManager transactionManager, System.Int32 _questionnaireVerificationRationale)
		{
			int count = -1;
			return GetByQuestionnaireVerificationRationale(transactionManager, _questionnaireVerificationRationale, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(TransactionManager transactionManager, System.Int32 _questionnaireVerificationRationale, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireVerificationRationale(transactionManager, _questionnaireVerificationRationale, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(System.Int32 _questionnaireVerificationRationale, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireVerificationRationale(null, _questionnaireVerificationRationale, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireVerificationRationale"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetByQuestionnaireVerificationRationale(TransactionManager transactionManager, System.Int32 _questionnaireVerificationRationale, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(System.Int32 _sectionId, System.String _questionId)
		{
			int count = -1;
			return GetBySectionIdQuestionId(null,_sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(System.Int32 _sectionId, System.String _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySectionIdQuestionId(null, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(TransactionManager transactionManager, System.Int32 _sectionId, System.String _questionId)
		{
			int count = -1;
			return GetBySectionIdQuestionId(transactionManager, _sectionId, _questionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(TransactionManager transactionManager, System.Int32 _sectionId, System.String _questionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySectionIdQuestionId(transactionManager, _sectionId, _questionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(System.Int32 _sectionId, System.String _questionId, int start, int pageLength, out int count)
		{
			return GetBySectionIdQuestionId(null, _sectionId, _questionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireVerificationRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sectionId"></param>
		/// <param name="_questionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireVerificationRationale GetBySectionIdQuestionId(TransactionManager transactionManager, System.Int32 _sectionId, System.String _questionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireVerificationRationale&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireVerificationRationale&gt;"/></returns>
		public static TList<QuestionnaireVerificationRationale> Fill(IDataReader reader, TList<QuestionnaireVerificationRationale> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireVerificationRationale c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireVerificationRationale")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireVerificationRationaleColumn.QuestionnaireVerificationRationale - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireVerificationRationale>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireVerificationRationale",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireVerificationRationale();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireVerificationRationale = (System.Int32)reader[((int)QuestionnaireVerificationRationaleColumn.QuestionnaireVerificationRationale - 1)];
					c.SectionId = (System.Int32)reader[((int)QuestionnaireVerificationRationaleColumn.SectionId - 1)];
					c.QuestionId = (System.String)reader[((int)QuestionnaireVerificationRationaleColumn.QuestionId - 1)];
					c.Rationale = (reader.IsDBNull(((int)QuestionnaireVerificationRationaleColumn.Rationale - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationRationaleColumn.Rationale - 1)];
					c.Assistance = (reader.IsDBNull(((int)QuestionnaireVerificationRationaleColumn.Assistance - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationRationaleColumn.Assistance - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireVerificationRationale entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireVerificationRationale = (System.Int32)reader[((int)QuestionnaireVerificationRationaleColumn.QuestionnaireVerificationRationale - 1)];
			entity.SectionId = (System.Int32)reader[((int)QuestionnaireVerificationRationaleColumn.SectionId - 1)];
			entity.QuestionId = (System.String)reader[((int)QuestionnaireVerificationRationaleColumn.QuestionId - 1)];
			entity.Rationale = (reader.IsDBNull(((int)QuestionnaireVerificationRationaleColumn.Rationale - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationRationaleColumn.Rationale - 1)];
			entity.Assistance = (reader.IsDBNull(((int)QuestionnaireVerificationRationaleColumn.Assistance - 1)))?null:(System.Byte[])reader[((int)QuestionnaireVerificationRationaleColumn.Assistance - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireVerificationRationale entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireVerificationRationale = (System.Int32)dataRow["QuestionnaireVerificationRationale"];
			entity.SectionId = (System.Int32)dataRow["SectionId"];
			entity.QuestionId = (System.String)dataRow["QuestionId"];
			entity.Rationale = Convert.IsDBNull(dataRow["Rationale"]) ? null : (System.Byte[])dataRow["Rationale"];
			entity.Assistance = Convert.IsDBNull(dataRow["Assistance"]) ? null : (System.Byte[])dataRow["Assistance"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireVerificationRationale"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationRationale Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationRationale entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireVerificationRationale object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireVerificationRationale instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireVerificationRationale Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireVerificationRationale entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireVerificationRationaleChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireVerificationRationale</c>
	///</summary>
	public enum QuestionnaireVerificationRationaleChildEntityTypes
	{
	}
	
	#endregion QuestionnaireVerificationRationaleChildEntityTypes
	
	#region QuestionnaireVerificationRationaleFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireVerificationRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleFilterBuilder : SqlFilterBuilder<QuestionnaireVerificationRationaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilterBuilder class.
		/// </summary>
		public QuestionnaireVerificationRationaleFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationRationaleFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationRationaleFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationRationaleFilterBuilder
	
	#region QuestionnaireVerificationRationaleParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireVerificationRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireVerificationRationaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleParameterBuilder class.
		/// </summary>
		public QuestionnaireVerificationRationaleParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireVerificationRationaleParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireVerificationRationaleParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireVerificationRationaleParameterBuilder
	
	#region QuestionnaireVerificationRationaleSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireVerificationRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireVerificationRationaleSortBuilder : SqlSortBuilder<QuestionnaireVerificationRationaleColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleSqlSortBuilder class.
		/// </summary>
		public QuestionnaireVerificationRationaleSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireVerificationRationaleSortBuilder
	
} // end namespace
