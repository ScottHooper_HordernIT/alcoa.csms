﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web.Mvc;
using WebMatrix.WebData;
using Repo.CSMS.Service.Database;

namespace Repo.CSMS.Service.Security
{
    public class CSMSMembershipProvider : ExtendedMembershipProvider
    {
        private string applicationname = "CSMSMembershipProvider";

        public override string ApplicationName
        {
            get
            {
                return this.applicationname;
            }
            set
            {
                this.applicationname = value;
            }
        }

        public override bool ConfirmAccount(string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmAccount(string userName, string accountConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateAccount(string userName, string password, bool requireConfirmationToken)
        {
            throw new NotImplementedException();
        }

        public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteAccount(string userName)
        {
            throw new NotImplementedException();
        }

        public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow)
        {
            throw new NotImplementedException();

            //string token = "";

            //var userService = DependencyResolver.Current.GetService<IUserService>();

            //if (userService != null)
            //{
            //    var user = userService.GetUser(e => e.EmailAddress == userName, null);

            //    if (user == null)
            //    {
            //        throw new ApplicationException("Unable to find user");
            //    }
            //    else
            //    {
            //        user.PasswordResetToken = Guid.NewGuid();
            //        user.PasswordResetRequested = DateTime.Now;
            //        userService.Update(user);

            //        token = user.PasswordResetToken.ToString();
            //    }
            //}

            //return token;
        }

        public override ICollection<OAuthAccountData> GetAccountsForUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetCreateDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetLastPasswordFailureDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetPasswordChangedDate(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetPasswordFailuresSinceLastSuccess(string userName)
        {
            throw new NotImplementedException();
        }

        public override int GetUserIdFromPasswordResetToken(string token)
        {
            throw new NotImplementedException();

            //int userId = -1;

            //if (string.IsNullOrEmpty(token)) throw new ApplicationException("Invalid password reset token");

            //var userService = DependencyResolver.Current.GetService<IUserService>();
            //if (userService != null)
            //{
            //    var gToken = new Guid(token);

            //    var user = userService.GetUser(e => e.PasswordResetToken == gToken, null);
            //    if (user != null)
            //    {
            //        // make sure token not older than 24 hours
            //        if (user.PasswordResetRequested != null && user.PasswordResetRequested > DateTime.Now.AddDays(-1))
            //        {
            //            userId = user.ID;
            //        }
            //        else
            //        {
            //            throw new ApplicationException("Password reset request is older than 24 hours");
            //        }
            //    }
            //    else
            //    {
            //        throw new ApplicationException("Unable to locate password reset request");
            //    }
            //}
            //else
            //{
            //    throw new ApplicationException("Unable to initialise user service");
            //}

            //return userId;
        }

        public override bool IsConfirmed(string userName)
        {
            throw new NotImplementedException();
        }

        public override bool ResetPasswordWithToken(string token, string newPassword)
        {
            throw new NotImplementedException();

            //bool success = false;

            //if (string.IsNullOrEmpty(token)) throw new ApplicationException("Invalid password reset token");

            //var userService = DependencyResolver.Current.GetService<IUserService>();
            //if (userService != null)
            //{
            //    var gToken = new Guid(token);

            //    var user = userService.GetUser(e => e.PasswordResetToken == gToken, null);
            //    if (user != null)
            //    {
            //        user.NewPassword = newPassword;
            //        user.PasswordResetRequested = null;
            //        user.PasswordResetToken = null;

            //        userService.Update(user);
            //    }
            //    else
            //    {
            //        throw new ApplicationException("Unable to locate password reset request");
            //    }
            //}
            //else
            //{
            //    throw new ApplicationException("Unable to initialise user service");
            //}
            
            //return success;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            MembershipUser membershipUser = null;

            var userService = DependencyResolver.Current.GetService<IUserService>();

            if (userService != null)
            {
                var user = userService.Get(e => e.UserLogon == username, null);

                if (user != null)
                {
                    membershipUser = new MembershipUser(this.ApplicationName, user.FirstName, user.UserId, user.Email, "", "", true, false, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                }
            }

            return membershipUser;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            bool isValid = false;

            var userService = DependencyResolver.Current.GetService<IUserService>();

            if (userService != null)
            {
                string passwordHash = CSMS.Common.Helpers.EncryptionHelper.GetMD5Hash(password, username.ToLower());

                var user = userService.Get(e => e.UserLogon == username, null);

                if (user != null)
                {
                    isValid = true;
                }
            }

            return isValid;
        }
    }
}
