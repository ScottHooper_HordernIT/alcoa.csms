﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportTimeDelayBeingAssessedProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportTimeDelayBeingAssessedProviderBaseCore : EntityViewProviderBase<QuestionnaireReportTimeDelayBeingAssessed>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt; Fill(DataSet dataSet, VList<QuestionnaireReportTimeDelayBeingAssessed> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportTimeDelayBeingAssessed>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportTimeDelayBeingAssessed>"/></returns>
		protected static VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt; Fill(DataTable dataTable, VList<QuestionnaireReportTimeDelayBeingAssessed> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportTimeDelayBeingAssessed c = new QuestionnaireReportTimeDelayBeingAssessed();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.CompanyName = (Convert.IsDBNull(row["CompanyName"]))?string.Empty:(System.String)row["CompanyName"];
					c.ProcurementContact = (Convert.IsDBNull(row["ProcurementContact"]))?string.Empty:(System.String)row["ProcurementContact"];
					c.ContractManager = (Convert.IsDBNull(row["ContractManager"]))?string.Empty:(System.String)row["ContractManager"];
					c.ProcurementContactUserId = (Convert.IsDBNull(row["ProcurementContactUserId"]))?string.Empty:(System.String)row["ProcurementContactUserId"];
					c.ContractManagerUserId = (Convert.IsDBNull(row["ContractManagerUserId"]))?string.Empty:(System.String)row["ContractManagerUserId"];
					c.DaysSinceSubmission = (Convert.IsDBNull(row["DaysSinceSubmission"]))?(int)0:(System.Int32?)row["DaysSinceSubmission"];
					c.Recommended = (Convert.IsDBNull(row["Recommended"]))?false:(System.Boolean?)row["Recommended"];
					c.ApprovedByUserId = (Convert.IsDBNull(row["ApprovedByUserId"]))?(int)0:(System.Int32?)row["ApprovedByUserId"];
					c.ApprovedBy = (Convert.IsDBNull(row["ApprovedBy"]))?string.Empty:(System.String)row["ApprovedBy"];
					c.SafetyAssessor = (Convert.IsDBNull(row["SafetyAssessor"]))?string.Empty:(System.String)row["SafetyAssessor"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportTimeDelayBeingAssessed&gt;"/></returns>
		protected VList<QuestionnaireReportTimeDelayBeingAssessed> Fill(IDataReader reader, VList<QuestionnaireReportTimeDelayBeingAssessed> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportTimeDelayBeingAssessed entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportTimeDelayBeingAssessed>("QuestionnaireReportTimeDelayBeingAssessed",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportTimeDelayBeingAssessed();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.CompanyName)];
					//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
					entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContact)];
					//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
					entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManager)];
					//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
					entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContactUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContactUserId)];
					//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?string.Empty:(System.String)reader["ProcurementContactUserId"];
					entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManagerUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManagerUserId)];
					//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?string.Empty:(System.String)reader["ContractManagerUserId"];
					entity.DaysSinceSubmission = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.DaysSinceSubmission)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.DaysSinceSubmission)];
					//entity.DaysSinceSubmission = (Convert.IsDBNull(reader["DaysSinceSubmission"]))?(int)0:(System.Int32?)reader["DaysSinceSubmission"];
					entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.Recommended)];
					//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
					entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedByUserId)];
					//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
					entity.ApprovedBy = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedBy)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedBy)];
					//entity.ApprovedBy = (Convert.IsDBNull(reader["ApprovedBy"]))?string.Empty:(System.String)reader["ApprovedBy"];
					entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.SafetyAssessor)];
					//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportTimeDelayBeingAssessed entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.CompanyName = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.CompanyName)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.CompanyName)];
			//entity.CompanyName = (Convert.IsDBNull(reader["CompanyName"]))?string.Empty:(System.String)reader["CompanyName"];
			entity.ProcurementContact = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContact)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContact)];
			//entity.ProcurementContact = (Convert.IsDBNull(reader["ProcurementContact"]))?string.Empty:(System.String)reader["ProcurementContact"];
			entity.ContractManager = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManager)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManager)];
			//entity.ContractManager = (Convert.IsDBNull(reader["ContractManager"]))?string.Empty:(System.String)reader["ContractManager"];
			entity.ProcurementContactUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContactUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ProcurementContactUserId)];
			//entity.ProcurementContactUserId = (Convert.IsDBNull(reader["ProcurementContactUserId"]))?string.Empty:(System.String)reader["ProcurementContactUserId"];
			entity.ContractManagerUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManagerUserId)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ContractManagerUserId)];
			//entity.ContractManagerUserId = (Convert.IsDBNull(reader["ContractManagerUserId"]))?string.Empty:(System.String)reader["ContractManagerUserId"];
			entity.DaysSinceSubmission = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.DaysSinceSubmission)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.DaysSinceSubmission)];
			//entity.DaysSinceSubmission = (Convert.IsDBNull(reader["DaysSinceSubmission"]))?(int)0:(System.Int32?)reader["DaysSinceSubmission"];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.Recommended)))?null:(System.Boolean?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.Recommended)];
			//entity.Recommended = (Convert.IsDBNull(reader["Recommended"]))?false:(System.Boolean?)reader["Recommended"];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedByUserId)))?null:(System.Int32?)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedByUserId)];
			//entity.ApprovedByUserId = (Convert.IsDBNull(reader["ApprovedByUserId"]))?(int)0:(System.Int32?)reader["ApprovedByUserId"];
			entity.ApprovedBy = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedBy)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.ApprovedBy)];
			//entity.ApprovedBy = (Convert.IsDBNull(reader["ApprovedBy"]))?string.Empty:(System.String)reader["ApprovedBy"];
			entity.SafetyAssessor = (reader.IsDBNull(((int)QuestionnaireReportTimeDelayBeingAssessedColumn.SafetyAssessor)))?null:(System.String)reader[((int)QuestionnaireReportTimeDelayBeingAssessedColumn.SafetyAssessor)];
			//entity.SafetyAssessor = (Convert.IsDBNull(reader["SafetyAssessor"]))?string.Empty:(System.String)reader["SafetyAssessor"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportTimeDelayBeingAssessed entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.CompanyName = (Convert.IsDBNull(dataRow["CompanyName"]))?string.Empty:(System.String)dataRow["CompanyName"];
			entity.ProcurementContact = (Convert.IsDBNull(dataRow["ProcurementContact"]))?string.Empty:(System.String)dataRow["ProcurementContact"];
			entity.ContractManager = (Convert.IsDBNull(dataRow["ContractManager"]))?string.Empty:(System.String)dataRow["ContractManager"];
			entity.ProcurementContactUserId = (Convert.IsDBNull(dataRow["ProcurementContactUserId"]))?string.Empty:(System.String)dataRow["ProcurementContactUserId"];
			entity.ContractManagerUserId = (Convert.IsDBNull(dataRow["ContractManagerUserId"]))?string.Empty:(System.String)dataRow["ContractManagerUserId"];
			entity.DaysSinceSubmission = (Convert.IsDBNull(dataRow["DaysSinceSubmission"]))?(int)0:(System.Int32?)dataRow["DaysSinceSubmission"];
			entity.Recommended = (Convert.IsDBNull(dataRow["Recommended"]))?false:(System.Boolean?)dataRow["Recommended"];
			entity.ApprovedByUserId = (Convert.IsDBNull(dataRow["ApprovedByUserId"]))?(int)0:(System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedBy = (Convert.IsDBNull(dataRow["ApprovedBy"]))?string.Empty:(System.String)dataRow["ApprovedBy"];
			entity.SafetyAssessor = (Convert.IsDBNull(dataRow["SafetyAssessor"]))?string.Empty:(System.String)dataRow["SafetyAssessor"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportTimeDelayBeingAssessedFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedFilterBuilder : SqlFilterBuilder<QuestionnaireReportTimeDelayBeingAssessedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayBeingAssessedFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayBeingAssessedFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedFilterBuilder

	#region QuestionnaireReportTimeDelayBeingAssessedParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportTimeDelayBeingAssessedParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportTimeDelayBeingAssessedColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedParameterBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportTimeDelayBeingAssessedParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportTimeDelayBeingAssessedParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportTimeDelayBeingAssessedParameterBuilder
	
	#region QuestionnaireReportTimeDelayBeingAssessedSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportTimeDelayBeingAssessed"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportTimeDelayBeingAssessedSortBuilder : SqlSortBuilder<QuestionnaireReportTimeDelayBeingAssessedColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportTimeDelayBeingAssessedSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportTimeDelayBeingAssessedSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportTimeDelayBeingAssessedSortBuilder

} // end namespace
