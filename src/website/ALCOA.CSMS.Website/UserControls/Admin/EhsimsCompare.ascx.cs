using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraPrinting;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Security;
using System.Security.Principal;
using System.Collections.Generic;
using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;

public partial class UserControls_Admin_EhsimsCompare : System.Web.UI.UserControl
{
    int? currentMonth, currentYear;
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }


    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.RoleId == (int)RoleList.Reader || auth.RoleId == (int)RoleList.Administrator)
            {
                SessionHandler.spVar_CompanyId = "-1";
            }
            else
            {
                SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                //grid.ExpandAll();
            }
            //Calc();
            //DT 3384 : commented part start
            //currentMonth = DateTime.Today.Month - 1;
            //currentYear = DateTime.Today.Year;

            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3)));
            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2)));
            //ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1)));
            //ddlYear.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            //ddlYear.Value = DateTime.Now.Year;
            //ddlMonth.Value = currentMonth;
            //DT 3384 : commented part End

            // DT 3384 : Start 

            ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3)));
            ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2)));
            ddlYear.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1)));
            ddlYear.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlYearTo.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 3).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 3)));
            ddlYearTo.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 2).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 2)));
            ddlYearTo.Items.Add(new ListEditItem((Convert.ToInt32(DateTime.Now.Year) - 1).ToString(), (Convert.ToInt32(DateTime.Now.Year) - 1)));
            ddlYearTo.Items.Add(new ListEditItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));

            if (DateTime.Today.Month == 1)
            {
                currentMonth = DateTime.Today.Month + 11;
                currentYear = DateTime.Now.Year - 1;
            }
            else
            {
                currentMonth = DateTime.Today.Month - 1;
                currentYear = DateTime.Today.Year;
            }
            ddlMonth.Value = currentMonth;
            ddlYear.Value = currentYear;
            ddlMonthTo.Value = currentMonth;
            ddlYearTo.Value = currentYear;

            //DT 3384 : End 


            Compare(currentMonth.Value, currentYear.Value, currentMonth.Value, currentYear.Value);

            // Export
            String exportFileName = @"ALCOA CSMS - Admin - Compare EHSIMS to CSMS"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtComparison"];
            grid.DataBind();
        }
    }

    private void Compare(int MonthFrom, int YearFrom, int MonthTo, int YearTo)
    {
        try
        {
            int monthCounter = 1;
            List<DateTime> monthList = GenerateMonths(MonthFrom,YearFrom, MonthTo,YearTo);
            DataTable dtCompareIhsToCsms = null;
            foreach (DateTime selectedMonth in monthList)
            {
                int month = selectedMonth.Month;
                int Year = selectedMonth.Year;

                DataTable dtCsmsKpi = readKpiData(month, Year);     //1. Read KPI data from database into DataTable

                DataTable dtEhsims = readEhsimsData(month, Year);   //2. Read EHSIMS data from CSV flat file into DataTable (Pre-2011)

                DataTable dtIhs = readIhsData(month, Year);         //3. Read IHS data from CSV flat file into DataTable (2011-Onwards)

                DataTable dtCompareCsmsToEhsimsIhs = compareCsmsToEhsimsIhs(dtCsmsKpi, dtEhsims, dtIhs);
                DataTable dtCompareEhsimsToCsms = compareEhsimsToCsms(dtCompareCsmsToEhsimsIhs, dtCsmsKpi, dtEhsims);
                if (monthCounter == 1)
                    dtCompareIhsToCsms = compareIhsToCsms(dtCompareCsmsToEhsimsIhs, dtCsmsKpi, dtIhs);
                else
                    dtCompareIhsToCsms.Merge(compareIhsToCsms(dtCompareCsmsToEhsimsIhs, dtCsmsKpi, dtIhs));
                monthCounter++;
            }
            Session["dtComparison"] = dtCompareIhsToCsms;
            grid.DataSource = (DataTable)Session["dtComparison"];
            grid.DataBind();
        }
        catch (Exception ex)
        {
            //write error msg.
        }
        //try
        //{
        //    List<int> monthList = GenerateMonths(Month);

        //    DataTable dtEhsims = null;
        //    DataTable dtCsmsKpi = readKpiData(Month, Year); //1. Read KPI data from database into DataTable

        //    dtEhsims = readEhsimsData(Month, Year); //2. Read EHSIMS data from CSV flat file into DataTable (Pre-2011)

        //    DataTable dtIhs = readIhsData(Month, Year); //3. Read IHS data from CSV flat file into DataTable (2011-Onwards)

        //    DataTable dtCompareCsmsToEhsimsIhs = compareCsmsToEhsimsIhs(dtCsmsKpi, dtEhsims, dtIhs);
        //    DataTable dtCompareEhsimsToCsms = compareEhsimsToCsms(dtCompareCsmsToEhsimsIhs, dtCsmsKpi, dtEhsims);
        //    DataTable dtCompareIhsToCsms = compareIhsToCsms(dtCompareCsmsToEhsimsIhs, dtCsmsKpi, dtIhs);


        //    Session["dtComparison"] = dtCompareIhsToCsms;
        //    grid.DataSource = (DataTable)Session["dtComparison"];
        //    grid.DataBind();
        //}
        //catch (Exception ex)
        //{
        //    //write error msg.
        //}
    }
    private List<DateTime> GenerateMonths(int MonthFrom,int YearFrom, int MonthTo, int YearTo)
    {
        List<DateTime> monthList = new List<DateTime>();
        //Modified by Ashley Goldstraw to allow for date range across years.  DT421 15/12/2015
        /*if (Month == 0)
        {
            monthList.Add(1);
            monthList.Add(2);
            monthList.Add(3);
            monthList.Add(4);
            monthList.Add(5);
            monthList.Add(6);
            monthList.Add(7);
            monthList.Add(8);
            monthList.Add(9);
            monthList.Add(10);
            monthList.Add(11);
            monthList.Add(12);

        }
        else
            monthList.Add(Month.Value);*/

        DateTime fromDate = new DateTime(YearFrom, MonthFrom, 1);
        DateTime toDate = new DateTime(YearTo, MonthTo, 1);
        do
        {
            monthList.Add(fromDate);
            fromDate = fromDate.AddMonths(1);
        } while (fromDate <= toDate);
        return monthList;
    }

    private string getCompareString(int Month, int Year)
    {
        string strCompare = "01/";
        switch (Month)
        {
            case 1: strCompare += "JAN";
                break;
            case 2: strCompare += "FEB";
                break;
            case 3: strCompare += "MAR";
                break;
            case 4: strCompare += "APR";
                break;
            case 5: strCompare += "MAY";
                break;
            case 6: strCompare += "JUN";
                break;
            case 7: strCompare += "JUL";
                break;
            case 8: strCompare += "AUG";
                break;
            case 9: strCompare += "SEP";
                break;
            case 10: strCompare += "OCT";
                break;
            case 11: strCompare += "NOV";
                break;
            case 12: strCompare += "DEC";
                break;
        }

        strCompare += "/"+Year.ToString().Substring(2);
        return strCompare;
    }

    private DataTable readKpiData(int? searchMonth, int? searchYear)
    {
        KpiService kpiService = new KpiService();
        DataSet dsCsms = kpiService.GetComparisonReportDataGroupedByMonthCompanySite();
        if(searchMonth != null && searchYear != null)
        {
            dsCsms = null;
            dsCsms = kpiService.GetComparisonReportDataGroupedByMonthCompanySiteFilterMonthYear(searchMonth, searchYear);
        }
        DataTable dtCsms = dsCsms.Tables[0];
        /* -----------------------------
         *                  Column Names
         * -----------------------------
         * CompanyId
         * SiteId
         * MonthYear
         * Ehsims_LocCode
         * Ihs_SiteNameIhs
         * EhsimsIhs_ContCompanyCode
         * HoursWorked
         * LostWorkDayCount
         * TotalRecCount
         * AllInjuryCount
         * InjuryFreeEvents
         * IFE
         * FA
         * RW
         * MT
         * LWD
         * -----------------------------
         */
        return dtCsms;
    }
    private DataTable readEhsimsData(int? searchMonth, int? searchYear)
    {
        Configuration configuration = new Configuration();
        string csvFilePath = @configuration.GetValue(ConfigList.ContractorHoursCountsFilePath);
        //string csvFilePath = @"C:\Files\NewFiles\ContractorHoursCounts.csv";
        DataTable dtEhsims = new DataTable();
        dtEhsims.Columns.Add("CompanyCode", typeof(string)); //Cont Company Code
        dtEhsims.Columns.Add("CompanyName", typeof(string)); //Cont Company Name
        dtEhsims.Columns.Add("LocCode", typeof(string)); //Location
        dtEhsims.Columns.Add("Month", typeof(int));
        dtEhsims.Columns.Add("Year", typeof(int));
        dtEhsims.Columns.Add("Hours", typeof(string)); //Hours Worked
        dtEhsims.Columns.Add("IFE", typeof(string));
        dtEhsims.Columns.Add("FA", typeof(string));
        dtEhsims.Columns.Add("RW", typeof(string));
        dtEhsims.Columns.Add("MT", typeof(string));
        dtEhsims.Columns.Add("LWD", typeof(string));
        string startHeader = "Location";
        string myFile = "";
        bool start = false;

        WindowsImpersonationContext impContext = null;
        try { impContext = Helper.ImpersonateWAOCSMUser(); }
        catch (ApplicationException ex) { Trace.Write(ex.Message); }
        try
        {
            if (null != impContext)
            {
                using (impContext)
                {
                    try
                    {
                        FileStream logFileStream = new FileStream(csvFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                        StreamReader logFileReader = new StreamReader(logFileStream);

                        while (!logFileReader.EndOfStream)
                        {
                            string line = logFileReader.ReadLine();
                            if (line.Contains(startHeader)) { start = true; };
                            if (start) myFile += line + "\n";
                        }
                        logFileReader.Close();
                        logFileStream.Close();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                        TextBox1.Text += "Error Exception Type-1: " + ex.Message.ToString() + "\n";
                    }
                    finally
                    {
                        // Finally we have to revert the impersonation.
                        impContext.Undo();
                    }

                    if (!String.IsNullOrEmpty(myFile))
                    {
                        try
                        {
                            using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                            {
                                int fieldCount = csv.FieldCount;
                                string[] headers = csv.GetFieldHeaders();

                                while (csv.ReadNextRecord())
                                {
                                   // DateTime date = DateTime.ParseExact(csv["Report Month"], "dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    DateTime date = Convert.ToDateTime(csv["Report Month"]);
                                    //FA

                                    string contCompanyCode = "";
                                    string contCompanyName = "";
                                    string location = "";
                                    string hoursWorked = "";
                                    string ife = "";
                                    string fa = "";
                                    string rw = "";
                                    string mt = "";
                                    string lwd = "";

                                    try { contCompanyCode = csv["Cont Company Code"].ToString(); }
                                    catch { }
                                    try { contCompanyName = csv["Cont Company Name"].ToString(); }
                                    catch { }
                                    try { location = csv["Location"].ToString(); }
                                    catch { }
                                    try { hoursWorked = csv["Hours Worked"].ToString(); }
                                    catch { }
                                    try { ife = csv["IFE"].ToString(); }
                                    catch { }
                                    try { fa = csv["FA"].ToString(); }
                                    catch { }
                                    try { rw = csv["RW"].ToString(); }
                                    catch { }
                                    try { mt = csv["MT"].ToString(); }
                                    catch { }
                                    try { lwd = csv["LWD"].ToString(); }
                                    catch { }

                                    // TODO !####! in short as per the comments below we never add any rows to dtEhsims
                                    if (date.Year < 2011)
                                    {
                                        // TODO !####! - I cant see how this can ever be executed as there is no way to get here with null year/month
                                        if (searchMonth == null && searchYear == null)
                                        {
                                            dtEhsims.Rows.Add(new object[] {contCompanyCode, contCompanyName, location, date.Month, date.Year,
                                                hoursWorked, ife, fa, rw, mt, lwd });
                                        }
                                        // TODO !####! - I cant see how this can ever be executed as no way to select a search year < 2011
                                        else
                                        {
                                            if ((date.Month == Convert.ToInt32(searchMonth)) && (date.Year == Convert.ToInt32(searchYear)))
                                            {
                                                dtEhsims.Rows.Add(new object[] {contCompanyCode, contCompanyName, location, date.Month, date.Year,
                                                hoursWorked, ife, fa, rw, mt, lwd });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                            TextBox1.Text += "Error Exception Type-2: " + ex.Message.ToString() + "\n";
                        }
                    }
                    else
                    {
                        //throw new Exception("EHSIMS file is empty");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Server Configuration Account Error. Contact Administrator");
        }
        finally
        {
            //if (impContext != null) impContext.Undo();
        }

        return dtEhsims;
    }
    private DataTable readIhsData(int? searchMonth, int? searchYear)
    {
        Configuration configuration = new Configuration();
        string csvFilePathAI = @configuration.GetValue(ConfigList.AI_ContractorHoursCountsFilePath);

        //string csvFilePathAI = @"C:\temp\AI-ContractorHoursCounts-NEW.csv";
        //2.1 Read IHS data from csv flat file into datatable
        DataTable dtIhs = new DataTable();
        dtIhs.Columns.Add("CompanyCode", typeof(string)); //Cont Company Code
        dtIhs.Columns.Add("CompanyName", typeof(string)); //Cont Company Name
        dtIhs.Columns.Add("LocCode", typeof(string)); //Location
        dtIhs.Columns.Add("Month", typeof(int));
        dtIhs.Columns.Add("Year", typeof(int));
        dtIhs.Columns.Add("Hours", typeof(string)); //Hours Worked
        dtIhs.Columns.Add("IFE", typeof(string));
        dtIhs.Columns.Add("FA", typeof(string));
        dtIhs.Columns.Add("RW", typeof(string));
        dtIhs.Columns.Add("MT", typeof(string));
        dtIhs.Columns.Add("LWD", typeof(string));

        //Add by Sayani For Item# 1
        //dtIhs.Columns.Add("RN", typeof(string));
        //End of addition

        String startHeader = @"""Location"",""Report Month"",""Contractor Company Code"",""Cont Company Name"",""Contracting Company"",""Hours Worked"",""IFE"",""FA"",""RWD"",""MT"",""LWD""";
        String myFile = "";
        bool start = false;

        //DT654: Remove impersonation of service account as site is already running under this context
        //WindowsImpersonationContext impContext = null;
        //try { impContext = Helper.ImpersonateWAOCSMUser(); }
        //catch (ApplicationException ex) { Trace.Write(ex.Message); }
        try
        {

            try
            {
                FileStream logFileStream = new FileStream(csvFilePathAI, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                StreamReader logFileReader = new StreamReader(logFileStream);
                int i = 0;
                string dateCheck = getCompareString(Convert.ToInt32(searchMonth), Convert.ToInt32(searchYear));
                //DT 3305 Changes
                string dateCheckAlternate = dateCheck.Replace("/", "-");
                while (!logFileReader.EndOfStream)
                {
                    string line = logFileReader.ReadLine();
                    if (line.Contains(startHeader)) { start = true; };
                    //start = true;

                    if (start)
                    {
                        if (i == 0)
                        {
                            myFile += line + "\n";
                            i++;
                        }
                        else
                        {
                            //DT 3305 Changes
                            //dateCheck = "01/JUL/14"; dateCheckAlternate = "01-JUL-14"
                            if (line.Contains(dateCheck) || line.Contains(dateCheckAlternate))
                                myFile += line + "\n";
                        }
                    }
                }
                logFileReader.Close();
                logFileStream.Close();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                TextBox1.Text += "Error Exception Type-1: " + ex.Message.ToString() + "\n";
            }

            if (!String.IsNullOrEmpty(myFile))
            {
                try
                {
                    using (CsvReader csv = new CsvReader(new StringReader(myFile), true))
                    {
                        int fieldCount = csv.FieldCount;
                        string[] headers = csv.GetFieldHeaders();

                        while (csv.ReadNextRecord())
                        {
                            //DateTime date = DateTime.ParseExact(csv["Report Month"], "dd/MMM/yy", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime date = Convert.ToDateTime(csv["Report Month"]);
                            //FA

                            string contCompanyCode = "";
                            string contCompanyName = "";
                            string location = "";
                            string hoursWorked = "";
                            string ife = "";
                            string fa = "";
                            string rw = "";
                            string mt = "";
                            string lwd = "";
                            //Add by Sayani For Item# 1
                            //string rn = "";
                            //End of addition

                            try { contCompanyCode = csv["Contractor Company Code"].ToString(); }
                            catch { }
                            try { contCompanyName = csv["Cont Company Name"].ToString(); }
                            catch { }
                            try { location = (csv["Location"].ToString()).Replace("C} ", ""); }
                            catch { }
                            try { hoursWorked = csv["Hours Worked"].ToString(); }
                            catch { }
                            try { ife = csv["IFE"].ToString(); }
                            catch { }
                            try { fa = csv["FA"].ToString(); }
                            catch { }
                            try { rw = csv["RWD"].ToString(); }
                            catch { }
                            try { mt = csv["MT"].ToString(); }
                            catch { }
                            try { lwd = csv["LWD"].ToString(); }
                            catch { }

                            //Add by Sayani For Item# 1
                            //try { rn = csv["RN"].ToString(); }
                            //catch { }
                            //End of addition

                            if (date.Year >= 2011)
                            {
                                if (searchMonth == null && searchYear == null)
                                {
                                    dtIhs.Rows.Add(new object[] {contCompanyCode, contCompanyName, location, date.Month, date.Year,
                                                hoursWorked, ife, fa, rw, mt, lwd});
                                }
                                else
                                {
                                    if ((date.Month == Convert.ToInt32(searchMonth)) && (date.Year == Convert.ToInt32(searchYear)))
                                    {
                                        dtIhs.Rows.Add(new object[] {contCompanyCode, contCompanyName, location, date.Month, date.Year,
                                                hoursWorked, ife, fa, rw, mt, lwd });
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                    TextBox1.Text += "Error Exception Type-2: " + ex.Message.ToString() + "\n";
                }
            }
            else
            {
                //throw new Exception("EHSIMS file is empty");
            }

        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Server Configuration Account Error. Contact Administrator");
        }
        finally
        {
            // if (impContext != null) impContext.Undo();
        }
        return dtIhs;
    }

    private DataTable compareCsmsToEhsimsIhs(DataTable dtCsmsKpi, DataTable dtEhsims, DataTable dtIhs)
    {
        DataTable dtComparison = new DataTable();
        dtComparison.Columns.Add("Year-Month", typeof(string));
        dtComparison.Columns.Add("Company", typeof(string));
        dtComparison.Columns.Add("RegionName", typeof(string));
        dtComparison.Columns.Add("Site", typeof(string));
        dtComparison.Columns.Add("Status", typeof(bool));
        dtComparison.Columns.Add("Status-Hours", typeof(bool));
        dtComparison.Columns.Add("Status-Counts", typeof(bool));
        dtComparison.Columns.Add("Csms-Hours", typeof(string));
        dtComparison.Columns.Add("Csms-IFE", typeof(string));
        dtComparison.Columns.Add("Csms-FA", typeof(string));
        dtComparison.Columns.Add("Csms-RW", typeof(string));
        dtComparison.Columns.Add("Csms-MT", typeof(string));
        dtComparison.Columns.Add("Csms-LWD", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-Hours", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-IFE", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-FA", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-RW", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-MT", typeof(string));
        dtComparison.Columns.Add("EhsimsIhs-LWD", typeof(string));
        dtComparison.Columns.Add("StatusDesc", typeof(string));
        //Modified by Ashley Goldstraw to allow for date range across years.  This section improves performance by only going to the DB once.  DT421 15/12/2015
        repo.ISiteService siteServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.ISiteService>();
        List<model.Site> siteFullList = siteServiceRepo.GetMany(null, null, null, null);
        repo.IRegionsSiteService rsServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.IRegionsSiteService>();
        List<model.RegionsSite> rsFullList = rsServiceRepo.GetMany(null, null, null, null);
        repo.ICompanyService companyServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
        List<model.Company> companyFullList = companyServiceRepo.GetMany(null, null, null, null);

        

        foreach (DataRow dr in dtCsmsKpi.Rows)
        {
            try
            {
                DateTime dateKpiMonthYear = (DateTime)dr["MonthYear"];
                string _Month2 = dateKpiMonthYear.Month.ToString();
                if (dateKpiMonthYear.Month < 10) _Month2 = "0" + _Month2;

                bool Status = false;
                bool Status_Hours = false;
                bool Status_Counts = false;
                string StatusDesc = "-";

                //CompaniesService companiesService = new CompaniesService();
                //Companies company = companiesService.GetByCompanyId((int)dr["CompanyId"]);
                //Modified by Ashley Goldstraw DT421
                int companyId = (int)dr["CompanyId"];
                model.Company company = companyFullList.Find(c=>c.CompanyId == companyId);
                //SitesService sitesService = new SitesService();
                //Sites site = sitesService.GetBySiteId((int)dr["SiteId"]);
                //Modified by AG DT421.  Removed db calls from inside the loop and did only one call at the top.
                int siteId = (int)dr["SiteId"];
                model.Site site = siteFullList.Find(s => s.SiteId == siteId);
                
                string SiteNameLocCodeSiteNameIhs = site.SiteName + " - " + site.LocCode + " - " + site.SiteNameIhs;

                string RegionName = "?";
                bool wao = false;
                bool vic = false;
                //Modified by AG DT 421
                //RegionsSitesService rsService = new RegionsSitesService();
                //TList<RegionsSites> rsTlist = rsService.GetBySiteId(site.SiteId);
                List<model.RegionsSite> rsList = rsFullList.FindAll(rs => rs.SiteId == site.SiteId);
                foreach(model.RegionsSite rs in rsList)
                {
                    if (rs.RegionId == 1) wao = true;
                    if (rs.RegionId == 4) vic = true;
                }
                if (wao & !vic) RegionName = "WAO";
                if (vic & !wao) RegionName = "VIC OPS";


                string EhsimsIhs = "EHSIMS";
                if (dateKpiMonthYear.Year >= 2011) EhsimsIhs = "IHS";

                //1. Find in EHSIMS/IHS Map
                //DataSet dsCEM = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByCompanyIdSiteId((int)dr["CompanyId"], (int)dr["SiteId"]);
                /* -----------------------------
                 *                  Column Names
                 * -----------------------------
                 * [0]  EhsimsMapId
                 * [1]  CompanyId ***
                 * [2]  ContCompanyCode ***
                 * [3]  SiteId
                 * [4]  SiteNameIhs
                 * [5]  LocCode
                 * [6]  LocCodeSiteNameIhs
                 * [7]  SiteNameLocCodeSiteNameIhs ***
                 * [8]  SiteName
                 * [9]  SiteAbbrev
                 * [10] CompanyName
                 * -----------------------------
                 */

                
                string ABN=string.Empty;
                if (company != null)
                    ABN = company.CompanyAbn;

               
                string locCode=string.Empty;
                

                if (company != null && site != null)
                {
                    
                    DataRow[] foundRows;
                    //string LocCode = "";
                    if (dateKpiMonthYear.Year <= 2010) //EHSIMS
                    {
                        if (site != null)
                            locCode = site.LocCode;
                        //LocCode = dsCEM.Tables[0].Rows[0].ItemArray[5].ToString(); //LocCode
                        foundRows = dtEhsims.Select("CompanyCode = '" + ABN +
                                                    "' AND LocCode = '" + locCode +
                                                    "' AND Year = " + dateKpiMonthYear.Year.ToString() + " AND Month = " + dateKpiMonthYear.Month.ToString());
                    }
                    else //IHS
                    {
                        if (site != null)
                            locCode = site.SiteNameIhs;
                        //LocCode = dsCEM.Tables[0].Rows[0].ItemArray[4].ToString(); //SiteNameIhs
                        foundRows = dtIhs.Select("CompanyCode = '" + ABN +
                                                    "' AND LocCode = '" + locCode +
                                                    "' AND Year = " + dateKpiMonthYear.Year.ToString() + " AND Month = " + dateKpiMonthYear.Month.ToString());
                    }

                    
                    int foundRows_Count = 0;
                    foreach (DataRow foundRows_dr in foundRows) foundRows_Count++;

                    if (foundRows_Count <= 1)
                    {
                        string Hours = "";
                        string IFE = "";
                        string FA = "";
                        string RW = "";
                        string MT = "";
                        string LWD = "";

                        if (foundRows_Count == 1)
                        {
                            Status = true;
                            Status_Hours = true;
                            Status_Counts = true;
                            StatusDesc = "Match.";

                            Hours = foundRows[0]["Hours"].ToString();
                            IFE = foundRows[0]["IFE"].ToString();
                            FA = foundRows[0]["FA"].ToString();
                            RW = foundRows[0]["RW"].ToString();
                            MT = foundRows[0]["MT"].ToString();
                            LWD = foundRows[0]["LWD"].ToString();

                            if (String.IsNullOrEmpty(IFE)) IFE = "0";
                            if (String.IsNullOrEmpty(FA)) FA = "0";
                            if (String.IsNullOrEmpty(RW)) RW = "0";
                            if (String.IsNullOrEmpty(MT)) MT = "0";
                            if (String.IsNullOrEmpty(LWD)) LWD = "0";

                            if (Hours != dr["HoursWorked"].ToString()) Status = false;

                            if (Status == false) Status_Hours = false;

                            if (IFE != dr["IFE"].ToString())
                            {
                                Status = false;
                                Status_Counts = false;
                            }
                            if (FA != dr["FA"].ToString())
                            {
                                Status = false;
                                Status_Counts = false;
                            }
                            if (RW != dr["RW"].ToString())
                            {
                                Status = false;
                                Status_Counts = false;
                            }
                            if (MT != dr["MT"].ToString())
                            {
                                Status = false;
                                Status_Counts = false;
                            }
                            if (LWD != dr["LWD"].ToString())
                            {
                                Status = false;
                                Status_Counts = false;
                            }

                            if (!Status) 
                                StatusDesc = "Data does not match.";
                            dtComparison.Rows.Add(new object[] {dateKpiMonthYear.Year.ToString() + "-" +  _Month2, company.CompanyName, RegionName, SiteNameLocCodeSiteNameIhs, Status, Status_Hours, Status_Counts,
                                        dr["HoursWorked"], dr["IFE"], dr["FA"], dr["RW"], dr["MT"], dr["LWD"],
                                        Hours, IFE, FA, RW, MT, LWD, StatusDesc});
                        }
                        else
                        {
                            Status = false;
                            Status_Hours = false;
                            Status_Counts = false;

                            //set as true if hours = 0 and didn't find in ehsims..
                            try
                            {
                                if (company.CompanyName.StartsWith("[") == false && dr["HoursWorked"].ToString() == "0")
                                {
                                    Status = true;
                                    Status_Hours = true;
                                    Status_Counts = true;
                                }
                            }
                            catch (Exception)
                            {
                            }

                            StatusDesc = "Cannot find in " + EhsimsIhs + " raw data.";
                            dtComparison.Rows.Add(new object[] {dateKpiMonthYear.Year.ToString() + "-" +  _Month2, company.CompanyName, RegionName, SiteNameLocCodeSiteNameIhs, Status, Status_Hours, Status_Counts,
                                        dr["HoursWorked"], dr["IFE"], dr["FA"], dr["RW"], dr["MT"], dr["LWD"],
                                        "-", "-", "-", "-", "-", "-", StatusDesc});
                        }
                    }
                    else
                    {
                        throw new Exception("Duplicate Data in " + EhsimsIhs + " Raw Data: [" + ABN + "] " + company.CompanyName + " - " + SiteNameLocCodeSiteNameIhs + " - " + foundRows_Count + " rows found.\n");
                    }
                }
                else
                {
                    Status = false;
                    Status_Hours = false;
                    Status_Counts = false;
                    StatusDesc = "Does not exist within " + EhsimsIhs + " Companies Map.";
                    dtComparison.Rows.Add(new object[] {dateKpiMonthYear.Year.ToString() + "-" +  _Month2, company.CompanyName, RegionName,
                                            SiteNameLocCodeSiteNameIhs, Status, Status_Hours, Status_Counts, dr["HoursWorked"],
                                            dr["IFE"], dr["FA"], dr["RW"], dr["MT"], dr["LWD"],
                                            "-", "-", "-", "-", "-", "-", StatusDesc});
                }
            }
            catch (Exception ex)
            {
                TextBox1.Text += "Error Exception Type-3: " + ex.Message.ToString() + "\n";
            }

        }


        return dtComparison;
    }
    private DataTable compareEhsimsToCsms(DataTable dtCompareCsmsToEhsimsIhs, DataTable dtCsmsKpi, DataTable dtEhsims)
    {
        foreach (DataRow dr in dtEhsims.Rows)
        {
            string CompanyName = "";
            string SiteName = "";

            bool Status = true;
            bool Status_Hours = true;
            bool Status_Counts = true;
            string StatusDesc = "Match.";

            string RegionName = "?";

            string Year = dr["Year"].ToString();
            string _Month2 = dr["Month"].ToString();
            if (_Month2 != "10" && _Month2 != "11" && _Month2 != "12") _Month2 = "0" + _Month2;

            //DataSet dsCEM = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeEhsimsLocCode(
                                               // dr["CompanyCode"].ToString(), dr["LocCode"].ToString());
            /* -----------------------------
             *                  Column Names
             * -----------------------------
             * [0]  EhsimsMapId
             * [1]  CompanyId ***
             * [2]  ContCompanyCode ***
             * [3]  SiteId
             * [4]  SiteNameIhs
             * [5]  LocCode
             * [6]  LocCodeSiteNameIhs
             * [7]  SiteNameLocCodeSiteNameIhs ***
             * [8]  SiteName
             * [9]  SiteAbbrev
             * [10] CompanyName
             * -----------------------------
             */

            int CompanyId = -1;
            try
            {
                SitesService sitesService = new SitesService();
                TList<Sites> sitesList = sitesService.GetByLocCode(dr["LocCode"].ToString());
                if (sitesList != null)
                {
                    if (sitesList.Count > 0)
                    {
                        string SiteNameIhs = sitesList[0].SiteNameIhs;
                        if (String.IsNullOrEmpty(SiteNameIhs)) SiteNameIhs = "???";
                        SiteName = sitesList[0].SiteName + " - " + sitesList[0].LocCode + " - " + SiteNameIhs;

                        
                        bool wao = false;
                        bool vic = false;
                        RegionsSitesService rsService = new RegionsSitesService();
                        TList<RegionsSites> rsTlist = rsService.GetBySiteId(sitesList[0].SiteId);
                        foreach (RegionsSites rs in rsTlist)
                        {
                            if (rs.RegionId == 1) wao = true;
                            if (rs.RegionId == 4) vic = true;
                        }
                        if (wao & !vic) RegionName = "WAO";
                        if (vic & !wao) RegionName = "VIC OPS";
                    }
                    else
                    {
                        SiteName = "??? - [" + dr["LocCode"].ToString() + "] - ???";
                    }
                }
                if (dr["LocCode"].ToString() == "WAO")
                {
                    Sites s = sitesService.GetBySiteName("Western Australian Operations");
                    SiteName = s.SiteName + " - " + s.LocCode + " - " + s.SiteNameIhs;

                }
                if (dr["LocCode"].ToString() == "WAM")
                {
                    Sites s = sitesService.GetBySiteName("All Mines");
                    SiteName = s.SiteName + " - " + s.LocCode + " - " + s.SiteNameIhs;
                }

                CompaniesService cService = new CompaniesService();
                Companies cs = cService.GetByCompanyAbn(dr["CompanyCode"].ToString());
                
                if(cs!=null)
                {
                    CompanyName = cs.CompanyName;
                }
                else
                {
                    CompanyName = "[" + dr["CompanyCode"].ToString() + "] " + dr["CompanyName"].ToString();
                }

                

            }
            catch (Exception ex)
            {
                TextBox1.Text += "Error Exception Type-4: " + ex.Message.ToString() + "\n";
            }

            if (!String.IsNullOrEmpty(CompanyName))
            {
                if (CompanyId == -1)
                {
                    Status = false;
                    Status_Hours = false;
                    Status_Counts = false;
                    StatusDesc = "Does not exist within EHSIMS Companies Map.";

                    string Hours = dr["Hours"].ToString();
                    string IFE = dr["IFE"].ToString();
                    string FA = dr["FA"].ToString();
                    string RW = dr["RW"].ToString();
                    string MT = dr["MT"].ToString();
                    string LWD = dr["LWD"].ToString();
                    if (String.IsNullOrEmpty(Hours)) Hours = "0";
                    if (String.IsNullOrEmpty(IFE)) IFE = "0";
                    if (String.IsNullOrEmpty(FA)) FA = "0";
                    if (String.IsNullOrEmpty(RW)) RW = "0";
                    if (String.IsNullOrEmpty(MT)) MT = "0";
                    if (String.IsNullOrEmpty(LWD)) LWD = "0";

                    if (CompanyName.StartsWith("[") == false && Hours.ToString() == "0" && IFE == "0" && FA == "0" && RW == "0" && MT == "0" && LWD == "0")
                    {
                        Status = true;
                        Status_Hours = true;
                        Status_Counts = true;
                    }

                    dtCompareCsmsToEhsimsIhs.Rows.Add(new object[] {Year + "-" +  _Month2, CompanyName, RegionName, SiteName, Status, Status_Hours, Status_Counts,
                                        "-", "-", "-", "-", "-", "-",
                                        Hours, IFE, FA, RW, MT, LWD, StatusDesc});
                }
                else
                {
                    DataRow[] foundRows;
                    foundRows = dtCsmsKpi.Select("CompanyId = " + CompanyId + " AND Ehsims_LocCode = '" + dr["LocCode"].ToString() +
                                                "' AND MonthYear = '" + dr["Month"].ToString() + "/1/" + Year + "'");

                    int foundRows_Count = 0;
                    foreach (DataRow foundRows_dr in foundRows) foundRows_Count++;

                    if (foundRows_Count <= 1)
                    {
                        string Hours = dr["Hours"].ToString();
                        string IFE = dr["IFE"].ToString();
                        string FA = dr["FA"].ToString();
                        string RW = dr["RW"].ToString();
                        string MT = dr["MT"].ToString();
                        string LWD = dr["LWD"].ToString();
                        if (String.IsNullOrEmpty(Hours)) Hours = "0";
                        if (String.IsNullOrEmpty(IFE)) IFE = "0";
                        if (String.IsNullOrEmpty(FA)) FA = "0";
                        if (String.IsNullOrEmpty(RW)) RW = "0";
                        if (String.IsNullOrEmpty(MT)) MT = "0";
                        if (String.IsNullOrEmpty(LWD)) LWD = "0";

                        if (foundRows_Count <= 1)
                        {
                            if(foundRows_Count == 1)
                            {
                            //Data Matches. Should already be in dataset from previous csms -> ehsims check.
                            }
                            else
                            {
                                Status = false;
                                Status_Hours = false;
                                Status_Counts = false;
                                StatusDesc = "Cannot find in CSMS raw data.";

                                if (CompanyName.StartsWith("[") == false && Hours.ToString() == "0" && IFE == "0" && FA == "0" && RW == "0" && MT == "0" && LWD == "0")
                                {
                                    Status = true;
                                    Status_Hours = true;
                                    Status_Counts = true;
                                }


                                dtCompareCsmsToEhsimsIhs.Rows.Add(new object[] {Year + "-" +  _Month2, CompanyName, RegionName, SiteName, Status, Status_Hours, Status_Counts,
                                            "-", "-", "-", "-", "-", "-",
                                            Hours, IFE, FA, RW, MT, LWD, StatusDesc});
                            }
                        }
                        else
                        {
                            //throw new Exception("Duplicate 1!");

                            string CompanyCode1 = foundRows[0]["CompanyCode"].ToString();
                            string CompanyName1 = foundRows[0]["CompanyName"].ToString();
                            string LocCode1 = foundRows[0]["LocCode"].ToString();

                            TextBox1.Text += "Duplicate Data in CSMS Raw Data: [" + CompanyCode1 + "] " + CompanyName1 + " - " + LocCode1 + " - " + foundRows_Count + " rows found.\n";
                        }
                    }
                }
            }
        }
        return dtCompareCsmsToEhsimsIhs;
    }
    private DataTable compareIhsToCsms(DataTable dtCompareCsmsToEhsimsIhs, DataTable dtCsmsKpi, DataTable dtIhs)
    {
        //Modified by Ashley Goldstraw to allow for date range across years.  This section improves performance by only going to the DB once.  DT421 15/12/2015
        repo.ISiteService siteServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.ISiteService>();
        List<model.Site> siteFullList = siteServiceRepo.GetMany(null, null, null, null);
        repo.IRegionsSiteService rsServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.IRegionsSiteService>();
        List<model.RegionsSite> rsFullList = rsServiceRepo.GetMany(null,null,null,null);
        repo.ICompanyService companyServiceRepo = ALCOA.CSMS.Website.Global.GetInstance<repo.ICompanyService>();
        List<model.Company> companyFullList = companyServiceRepo.GetMany(null, null, null, null);

        foreach (DataRow dr in dtIhs.Rows)
        {
            string CompanyName = "";
            string SiteName = "";

            bool Status = true;
            bool Status_Hours = true;
            bool Status_Counts = true;
            string StatusDesc = "Match.";

            //string RegionName = dr["RegionName"].ToString();
            string RegionName = "?";
            string Year = dr["Year"].ToString();
            string _Month2 = dr["Month"].ToString();
            if (_Month2 != "10" && _Month2 != "11" && _Month2 != "12") _Month2 = "0" + _Month2;


            //DataSet dsCEM = DataRepository.CompaniesEhsimsMapSitesEhsimsIhsListProvider.GetByContCompanyCodeSiteNameIhs(
                                                //dr["CompanyCode"].ToString(), dr["LocCode"].ToString());

            /* -----------------------------
             *                  Column Names
             * -----------------------------
             * [0]  EhsimsMapId
             * [1]  CompanyId ***
             * [2]  ContCompanyCode ***
             * [3]  SiteId
             * [4]  SiteNameIhs
             * [5]  LocCode
             * [6]  LocCodeSiteNameIhs
             * [7]  SiteNameLocCodeSiteNameIhs ***
             * [8]  SiteName
             * [9]  SiteAbbrev
             * [10] CompanyName
             * -----------------------------
             */

            int CompanyId = -1;

            try
            {
                SitesService sitesService = new SitesService();
                //TList<Sites> sitesList = sitesService.GetBySiteNameIhs(dr["LocCode"].ToString().Replace("C} ",""));
                string siteNameIhs = dr["LocCode"].ToString().Replace("C} ", "");
                List<model.Site> sitesList = siteFullList.FindAll(s => s.SiteNameIhs == siteNameIhs);
                if (sitesList != null)
                {
                    if (sitesList.Count > 0)
                    {
                        string SiteNameIhs = sitesList[0].SiteNameIhs;
                        if (String.IsNullOrEmpty(SiteNameIhs)) SiteNameIhs = "???";
                        SiteName = sitesList[0].SiteName + " - " + sitesList[0].LocCode + " - " + SiteNameIhs;

                        bool wao = false;
                        bool vic = false;
                        //RegionsSitesService rsService = new RegionsSitesService();
                        //TList<RegionsSites> rsTlist = rsService.GetBySiteId(sitesList[0].SiteId);
                        //TList<RegionsSites> rsTlist = rsTlist.FindAllBy((i=>i.SiteId == sitesList[0].SiteId);
                        List<model.RegionsSite> rsSelectList = rsFullList.FindAll(r => r.SiteId == sitesList[0].SiteId);
                        foreach (model.RegionsSite rs in rsSelectList)
                        {
                            if (rs.RegionId == 1) wao = true;
                            if (rs.RegionId == 4) vic = true;
                        }
                        if (wao & !vic) RegionName = "WAO";
                        if (vic & !wao) RegionName = "VIC OPS";
                    }
                    else
                    {
                        SiteName = "??? - [???] - " + dr["LocCode"].ToString();
                    }
                }

                /*
                if (dr["LocCode"].ToString() == "WAO")
                {
                    Sites s = sitesService.GetBySiteName("Western Australian Operations");
                    SiteName = s.SiteName + " - " + s.LocCode + " - " + s.SiteNameIhs;

                }
                if (dr["LocCode"].ToString() == "WAM")
                {
                    Sites s = sitesService.GetBySiteName("All Mines");
                    SiteName = s.SiteName + " - " + s.LocCode + " - " + s.SiteNameIhs;
                }
                 */
                //CompaniesService cService = new CompaniesService();
                //Companies cs = cService.GetByCompanyAbn(dr["CompanyCode"].ToString());
                string abn = dr["CompanyCode"].ToString();
                model.Company cs = companyFullList.Find(c => c.CompanyAbn == abn);
                if (cs !=null)
                {
                    CompanyName = cs.CompanyName;
                    CompanyId = cs.CompanyId;
                }
                else
                {
                    CompanyName = "[" + dr["CompanyCode"].ToString() + "] " + dr["CompanyName"].ToString();
                }
            }
            catch (Exception ex)
            {
                TextBox1.Text += "Error Exception Type-4: " + ex.Message.ToString() + "\n";
            }
            
            if (!String.IsNullOrEmpty(CompanyName))
            {
                if (CompanyId == -1)
                {
                    Status = false;
                    Status_Hours = false;
                    Status_Counts = false;
                    StatusDesc = "Does not exist within IHS Companies Map.";

                    string Hours = dr["Hours"].ToString();
                    string IFE = dr["IFE"].ToString();
                    string FA = dr["FA"].ToString();
                    string RW = dr["RW"].ToString();
                    string MT = dr["MT"].ToString();
                    string LWD = dr["LWD"].ToString();
                    if (String.IsNullOrEmpty(Hours)) Hours = "0";
                    if (String.IsNullOrEmpty(IFE)) IFE = "0";
                    if (String.IsNullOrEmpty(FA)) FA = "0";
                    if (String.IsNullOrEmpty(RW)) RW = "0";
                    if (String.IsNullOrEmpty(MT)) MT = "0";
                    if (String.IsNullOrEmpty(LWD)) LWD = "0";

                    if (CompanyName.StartsWith("[") == false && Hours.ToString() == "0" && IFE == "0" && FA == "0" && RW == "0" && MT == "0" && LWD == "0")
                    {
                        Status = true;
                        Status_Hours = true;
                        Status_Counts = true;
                    }

                    dtCompareCsmsToEhsimsIhs.Rows.Add(new object[] {Year + "-" +  _Month2, CompanyName, RegionName, SiteName, Status, Status_Hours, Status_Counts,
                                        "-", "-", "-", "-", "-", "-",
                                        Hours, IFE, FA, RW, MT, LWD, StatusDesc});
                }
                else
                {
                    DataRow[] foundRows;
                    foundRows = dtCsmsKpi.Select("CompanyId = " + CompanyId + " AND Ihs_SiteNameIhs = '" + dr["LocCode"].ToString() +
                                                "' AND MonthYear = '" + dr["Month"].ToString() + "/1/" + Year + "'");

                    int foundRows_Count = 0;
                    foreach (DataRow foundRows_dr in foundRows) foundRows_Count++;

                    if (foundRows_Count <= 1)
                    {
                        string Hours = dr["Hours"].ToString();
                        string IFE = dr["IFE"].ToString();
                        string FA = dr["FA"].ToString();
                        string RW = dr["RW"].ToString();
                        string MT = dr["MT"].ToString();
                        string LWD = dr["LWD"].ToString();
                        if (String.IsNullOrEmpty(Hours)) Hours = "0";
                        if (String.IsNullOrEmpty(IFE)) IFE = "0";
                        if (String.IsNullOrEmpty(FA)) FA = "0";
                        if (String.IsNullOrEmpty(RW)) RW = "0";
                        if (String.IsNullOrEmpty(MT)) MT = "0";
                        if (String.IsNullOrEmpty(LWD)) LWD = "0";


                        if (foundRows_Count <= 1)
                        {
                            if (foundRows_Count == 1)
                            {
                                //Data Matches. Should already be in dataset from previous csms -> ehsims check.
                            }
                            else
                            {
                                Status = false;
                                Status_Hours = false;
                                Status_Counts = false;
                                StatusDesc = "Cannot find in CSMS raw data.";

                                if (CompanyName.StartsWith("[") == false && Hours.ToString() == "0" && IFE == "0" && FA == "0" && RW == "0" && MT == "0" && LWD == "0")
                                {
                                    Status = true;
                                    Status_Hours = true;
                                    Status_Counts = true;
                                }

                                dtCompareCsmsToEhsimsIhs.Rows.Add(new object[] {Year + "-" +  _Month2, CompanyName, RegionName, SiteName, Status, Status_Hours, Status_Counts,
                                            "-", "-", "-", "-", "-", "-",
                                            Hours, IFE, FA, RW, MT, LWD, StatusDesc});
                            }
                        }
                        else
                        {
                            //throw new Exception("Duplicate 1!");

                            string CompanyCode1 = foundRows[0]["CompanyCode"].ToString();
                            string CompanyName1 = foundRows[0]["CompanyName"].ToString();
                            string LocCode1 = foundRows[0]["LocCode"].ToString();

                            TextBox1.Text += "Duplicate Data in CSMS Raw Data: [" + CompanyCode1 + "] " + CompanyName1 + " - " + LocCode1 + " - " + foundRows_Count + " rows found.\n";
                        }
                    }
                }
            }
        }
        return dtCompareCsmsToEhsimsIhs;
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        bool status = (bool)grid.GetRowValues(e.VisibleIndex, "Status");

        System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage")
                                    as System.Web.UI.WebControls.Image;
        if (img != null) //change by debashis for deployment issue
        {
            img.Visible = false;
        
            if (status)
            {
                img.Visible = true;
                img.ImageUrl = "~/Images/greentick.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
            }
            else
            {
                img.Visible = true;
                img.ImageUrl = "~/Images/redcross.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
            }
        }

        bool status_Hours = (bool)grid.GetRowValues(e.VisibleIndex, "Status-Hours");
        bool status_Counts = (bool)grid.GetRowValues(e.VisibleIndex, "Status-Counts");

        System.Web.UI.WebControls.Image imgHours = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImageHours")
                                    as System.Web.UI.WebControls.Image;
        System.Web.UI.WebControls.Image imgCounts = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImageCounts")
                                    as System.Web.UI.WebControls.Image;
        if (imgHours != null && imgCounts != null) //change by debashis for deployment issue
        {
        
        imgHours.Visible = false;
        imgCounts.Visible = false;
            if (status_Hours)
            {
                imgHours.Visible = true;
                imgHours.ImageUrl = "~/Images/greentick.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
            }
            else
            {
                imgHours.Visible = true;
                imgHours.ImageUrl = "~/Images/redcross.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
            }

            if (status_Counts)
            {
                imgCounts.Visible = true;
                imgCounts.ImageUrl = "~/Images/greentick.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
            }
            else
            {
                imgCounts.Visible = true;
                imgCounts.ImageUrl = "~/Images/redcross.gif";
                //img.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
            }
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        //Modified by Ashley Goldstraw to allow for date range across years.  DT421 15/12/2015
        DateTime fromDate = new DateTime((int)ddlYear.Value, (int)ddlMonth.Value,1);
        DateTime toDate = new DateTime((int)ddlYearTo.Value,(int)ddlMonthTo.Value,1);
        //DT421 AG 15/12/2015.  Check that from date is less than to date.
        if (toDate < fromDate)
        { 
            lblErrorMessage.Visible = true;
            return; 
        }
        else
        { lblErrorMessage.Visible = false; }

        Compare((int)ddlMonth.Value, (int)ddlYear.Value,(int)ddlMonthTo.Value,(int)ddlYearTo.Value);
    }
}
