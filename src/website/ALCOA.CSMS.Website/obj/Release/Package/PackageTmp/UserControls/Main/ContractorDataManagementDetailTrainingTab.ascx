﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractorDataManagementDetailTrainingTab" Codebehind="ContractorDataManagementDetailTrainingTab.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>

<style type="text/css">
    .td-label {
        text-align:right;
    }
        .td-label label {
            padding-right:10px;
        }

    h3 {
        color:#000;
        margin-bottom:0;
    }

    fieldset {
        margin-bottom:20px;
    }
</style>

<script type="text/javascript">
    function OnNewClick(s, e) {
        grid.AddNewRow();
    }
</script>

<fieldset>
    <legend>Training Details</legend>

    <table  border="0" cellpadding="2" cellspacing="5" style="padding: 10px">
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Employee ID"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="lblEmplId" runat="server" Font-Bold="true"></dx:ASPxLabel>
            </td>
        </tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="ASPxLabel33" runat="server" Text="Full Name"></dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxLabel ID="lblFullName" runat="server" Font-Bold="true"></dx:ASPxLabel>
            </td>
        </tr>
    </table>

    <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
        <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <dx:ASPxButton
                        runat="server" 
                        AutoPostBack="false"
                        ID="btnAdd"                         
                        Text="Add New"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" >
                        <ClientSideEvents Click="function (s, e) { OnNewClick(s, e); }" />
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dxwgv:ASPxGridView ID="grid" 
                        runat="server" 
                        AutoGenerateColumns="False" 
                        ClientInstanceName="grid"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" 
                        DataSourceID="TrainingDataSource"
                        KeyFieldName="EMPL_ID" 
                        Width="100%" 
                        OnCustomColumnDisplayText="grid_CustomColumnDisplayText" 
                        OnRowInserting="grid_RowInserting">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn 
                                Caption="Contractor Number" 
                                Name="EMPL_ID"
                                FieldName="EMPL_ID" 
                                ReadOnly="True"  
                                VisibleIndex="0"
                                Visible="false">
                            </dxwgv:GridViewDataTextColumn>
                            
                            <dxwgv:GridViewDataDateColumn 
                                Caption="Completion Date" 
                                FieldName="COURSE_END_DATE" 
                                Name="COURSE_END_DATE"
                                VisibleIndex="1" 
                                SortOrder="Descending"
                                Width="100px">    
                                <PropertiesDateEdit DisplayFormatString="yyyy/MM/dd">
                                    <ValidationSettings 
                                        RequiredField-IsRequired="true" 
                                        RequiredField-ErrorText="Please select a Completion Date">
                                    </ValidationSettings>
                                </PropertiesDateEdit>                            
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataDateColumn> 

                            <dxwgv:GridViewDataComboBoxColumn
                                Caption="Induction Description" 
                                FieldName="COURSE_NAME" 
                                Name="COURSE_NAME"
                                VisibleIndex="2">
                                <PropertiesComboBox
                                    TextField="COURSE_NAME"
                                    ValueField="COURSE_NAME"
                                    DataSourceId="CoursesDataSource" 
                                    ValidationSettings-RequiredField-IsRequired="true" 
                                    ValidationSettings-RequiredField-ErrorText="Please select from the list">
                                </PropertiesComboBox>
                            </dxwgv:GridViewDataComboBoxColumn>

                            <dxwgv:GridViewDataTextColumn 
                                Caption="Training Title" 
                                FieldName="TRAINING_TITLE" 
                                Name="TRAINING_TITLE"
                                VisibleIndex="2"
                                Width="200px">
                                <PropertiesTextEdit 
                                    MaxLength="80" 
                                    ValidationSettings-RequiredField-IsRequired="true" 
                                    ValidationSettings-RequiredField-ErrorText="Please enter a Training Title">
                                </PropertiesTextEdit>
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            
                            <dxwgv:GridViewDataComboBoxColumn 
                                Caption="Induction Status" 
                                FieldName="RECORD_TYPE" 
                                SortIndex="0" 
                                VisibleIndex="4"
                                Width="120px">
                                <PropertiesComboBox
                                    TextField="RECORD_TYPE"
                                    ValueField="RECORD_TYPE"
                                    DataSourceId="RecordTypeDataSource"
                                    ValidationSettings-RequiredField-IsRequired="true" 
                                    ValidationSettings-RequiredField-ErrorText="Please select from the list">
                                </PropertiesComboBox>
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataComboBoxColumn>

                            <dxwgv:GridViewDataDateColumn 
                                Caption="Expiry Date" 
                                FieldName="EXPIRATION_DATE" 
                                Name="EXPIRATION_DATE"
                                SortIndex="0" 
                                VisibleIndex="5"
                                Width="100px">
                                <PropertiesDateEdit DisplayFormatString="yyyy/MM/dd">
                                    <ValidationSettings 
                                        RequiredField-IsRequired="true" 
                                        RequiredField-ErrorText="Please select an Expiration Date">
                                    </ValidationSettings>
                                </PropertiesDateEdit> 
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataDateColumn>

                            <dxwgv:GridViewDataTextColumn 
                                Caption="Duration" 
                                FieldName="DURATION" 
                                Name="DURATION"
                                SortIndex="0" 
                                VisibleIndex="7"
                                Width="75px">
                                <PropertiesTextEdit
                                    ValidationSettings-RequiredField-IsRequired="true" 
                                    ValidationSettings-RequiredField-ErrorText="Please enter a Duration">
                                    <%--Restrict to numeric data entry only--%>
                                    <ClientSideEvents 
                                        KeyPress="function(s,e){if (e.htmlEvent.keyCode < 48 || e.htmlEvent.keyCode > 56) ASPxClientUtils.PreventEvent(e.htmlEvent);}" />
                                </PropertiesTextEdit>
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataTextColumn>

                            <dxwgv:GridViewDataComboBoxColumn
                                Caption="Units" 
                                FieldName="DURATION_UNITS" 
                                Name="DURATION_UNITS"
                                SortIndex="0" 
                                VisibleIndex="8" 
                                Width="75px">
                                <PropertiesComboBox
                                    ValidationSettings-RequiredField-IsRequired="true" 
                                    ValidationSettings-RequiredField-ErrorText="Please select from the list">
                                </PropertiesComboBox>
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataComboBoxColumn>
                                                       
                        </Columns>
                        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" />
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <Settings ShowFilterRow="False" ShowGroupPanel="False" ShowHeaderFilterButton="True" />
                        <SettingsCustomizationWindow Enabled="True" />
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <CollapsedButton Height="12px" Width="11px">
                            </CollapsedButton>
                            <ExpandedButton Height="12px" Width="11px">
                            </ExpandedButton>
                            <DetailCollapsedButton Height="12px" Width="11px">
                            </DetailCollapsedButton>
                            <DetailExpandedButton Height="12px" Width="11px">
                            </DetailExpandedButton>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                    </dxwgv:ASPxGridView>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<asp:SqlDataSource  
    ID="TrainingDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="[HR].[XXHR_ADD_TRNG_SelectByEmplId]" 
    SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="CoursesDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT [COURSE_NAME]
                  FROM [HR].[XXHR_ADD_TRNG]
                  ORDER BY [COURSE_NAME]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="RecordTypeDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT DISTINCT [RECORD_TYPE]
                  FROM [HR].[XXHR_ADD_TRNG]
                  ORDER BY [RECORD_TYPE]"
    SelectCommandType="Text">
</asp:SqlDataSource>
