﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ConfigText2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ConfigText2DataSourceDesigner))]
	public class ConfigText2DataSource : ProviderDataSource<ConfigText2, ConfigText2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2DataSource class.
		/// </summary>
		public ConfigText2DataSource() : base(new ConfigText2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ConfigText2DataSourceView used by the ConfigText2DataSource.
		/// </summary>
		protected ConfigText2DataSourceView ConfigText2View
		{
			get { return ( View as ConfigText2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ConfigText2DataSource control invokes to retrieve data.
		/// </summary>
		public ConfigText2SelectMethod SelectMethod
		{
			get
			{
				ConfigText2SelectMethod selectMethod = ConfigText2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ConfigText2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ConfigText2DataSourceView class that is to be
		/// used by the ConfigText2DataSource.
		/// </summary>
		/// <returns>An instance of the ConfigText2DataSourceView class.</returns>
		protected override BaseDataSourceView<ConfigText2, ConfigText2Key> GetNewDataSourceView()
		{
			return new ConfigText2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ConfigText2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ConfigText2DataSourceView : ProviderDataSourceView<ConfigText2, ConfigText2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ConfigText2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ConfigText2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ConfigText2DataSourceView(ConfigText2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ConfigText2DataSource ConfigText2Owner
		{
			get { return Owner as ConfigText2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ConfigText2SelectMethod SelectMethod
		{
			get { return ConfigText2Owner.SelectMethod; }
			set { ConfigText2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ConfigText2Service ConfigText2Provider
		{
			get { return Provider as ConfigText2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<ConfigText2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<ConfigText2> results = null;
			ConfigText2 item;
			count = 0;
			
			System.Int32 _configText2Id;
			System.Int32 _configTextTypeId;

			switch ( SelectMethod )
			{
				case ConfigText2SelectMethod.Get:
					ConfigText2Key entityKey  = new ConfigText2Key();
					entityKey.Load(values);
					item = ConfigText2Provider.Get(entityKey);
					results = new TList<ConfigText2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ConfigText2SelectMethod.GetAll:
                    results = ConfigText2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ConfigText2SelectMethod.GetPaged:
					results = ConfigText2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ConfigText2SelectMethod.Find:
					if ( FilterParameters != null )
						results = ConfigText2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ConfigText2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ConfigText2SelectMethod.GetByConfigText2Id:
					_configText2Id = ( values["ConfigText2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ConfigText2Id"], typeof(System.Int32)) : (int)0;
					item = ConfigText2Provider.GetByConfigText2Id(_configText2Id);
					results = new TList<ConfigText2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case ConfigText2SelectMethod.GetByConfigTextTypeId:
					_configTextTypeId = ( values["ConfigTextTypeId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ConfigTextTypeId"], typeof(System.Int32)) : (int)0;
					results = ConfigText2Provider.GetByConfigTextTypeId(_configTextTypeId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ConfigText2SelectMethod.Get || SelectMethod == ConfigText2SelectMethod.GetByConfigText2Id )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				ConfigText2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ConfigText2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<ConfigText2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ConfigText2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ConfigText2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ConfigText2DataSource class.
	/// </summary>
	public class ConfigText2DataSourceDesigner : ProviderDataSourceDesigner<ConfigText2, ConfigText2Key>
	{
		/// <summary>
		/// Initializes a new instance of the ConfigText2DataSourceDesigner class.
		/// </summary>
		public ConfigText2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigText2SelectMethod SelectMethod
		{
			get { return ((ConfigText2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ConfigText2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ConfigText2DataSourceActionList

	/// <summary>
	/// Supports the ConfigText2DataSourceDesigner class.
	/// </summary>
	internal class ConfigText2DataSourceActionList : DesignerActionList
	{
		private ConfigText2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ConfigText2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ConfigText2DataSourceActionList(ConfigText2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ConfigText2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ConfigText2DataSourceActionList
	
	#endregion ConfigText2DataSourceDesigner
	
	#region ConfigText2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ConfigText2DataSource.SelectMethod property.
	/// </summary>
	public enum ConfigText2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByConfigText2Id method.
		/// </summary>
		GetByConfigText2Id,
		/// <summary>
		/// Represents the GetByConfigTextTypeId method.
		/// </summary>
		GetByConfigTextTypeId
	}
	
	#endregion ConfigText2SelectMethod

	#region ConfigText2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2Filter : SqlFilter<ConfigText2Column>
	{
	}
	
	#endregion ConfigText2Filter

	#region ConfigText2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2ExpressionBuilder : SqlExpressionBuilder<ConfigText2Column>
	{
	}
	
	#endregion ConfigText2ExpressionBuilder	

	#region ConfigText2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ConfigText2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="ConfigText2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ConfigText2Property : ChildEntityProperty<ConfigText2ChildEntityTypes>
	{
	}
	
	#endregion ConfigText2Property
}

