using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;

using System.IO;

/// <summary>
/// Last Updated: 11/09/2009
/// User control with 3 buttons to export an aspxgridview to rtf(doc)/pdf/
/// </summary>
public partial class UserControls_Other_ExportButtonsPivot : System.Web.UI.UserControl
{
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    //btnExcel.ClientSideEvents.Init =
    //    //string.Format("function(s, e) {{ s._autoPostback = {0}; }}", btnExcel.AutoPostBack ? "true" : "false");

    //}
    protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
    {
        using (MemoryStream stream = new MemoryStream())
        {
            string contentType = "application/ms-excel";
            gridPivotExporter.ExportToRtf(stream);
            byte[] buffer = stream.GetBuffer();
            string disposition = "attachment";
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", contentType);
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", disposition + "; filename=csm.doc");
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }
    protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
    {
        using (MemoryStream stream = new MemoryStream())
        {
            string contentType = "application/ms-excel";
            gridPivotExporter.ExportToPdf(stream);
            byte[] buffer = stream.GetBuffer();
            string disposition = "attachment";
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", contentType);
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", disposition + "; filename=csm.pdf");
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }
    protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
    {
        //Response.Clear();
        //XlsExportOptions opt = new XlsExportOptions();
        //opt.SheetName = gridExporter.FileName;
        //opt.ShowGridLines = true;
        //opt.TextExportMode = TextExportMode.Value;
        ////opt.UseNativeFormat = true;
        //gridExporter.WriteXlsToResponse(opt);
        //Response.Close();

        using (MemoryStream stream = new MemoryStream())
        {
            string contentType = "application/ms-excel";
            gridPivotExporter.ExportToXls(stream);
            byte[] buffer = stream.GetBuffer();               
            string disposition = "attachment";
            Response.Clear();
            Response.Buffer = false;
            Response.AppendHeader("Content-Type", contentType);
            Response.AppendHeader("Content-Transfer-Encoding", "binary");
            Response.AppendHeader("Content-Disposition", disposition + "; filename=csm.xls");
            Response.BinaryWrite(buffer);
            Response.End();
        }
    }

    //protected void OnButtonClick(object sender, EventArgs e)
    //{
    //    Response.Clear();
    //    XlsExportOptions opt = new XlsExportOptions();
    //    opt.SheetName = gridExporter.FileName;
    //    opt.ShowGridLines = true;
    //    opt.TextExportMode = TextExportMode.Value;
    //    //opt.UseNativeFormat = true;
    //    gridExporter.WriteXlsToResponse(opt);
    //    Response.Close();
    //}
}
