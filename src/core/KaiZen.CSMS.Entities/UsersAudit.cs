﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'UsersAudit' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UsersAudit : UsersAuditBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UsersAudit"/> instance.
		///</summary>
		public UsersAudit():base(){}	
		
		#endregion
	}
}
