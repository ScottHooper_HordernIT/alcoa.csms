﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialContactAuditService
    {
        QuestionnaireInitialContactAudit Get(Expression<Func<QuestionnaireInitialContactAudit, bool>> where, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes);
        List<QuestionnaireInitialContactAudit> GetMany(int? top, Expression<Func<QuestionnaireInitialContactAudit, bool>> where, Func<IQueryable<QuestionnaireInitialContactAudit>, IOrderedQueryable<QuestionnaireInitialContactAudit>> orderBy, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes);
        List<QuestionnaireInitialContactAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactAudit, bool>> where, Func<IQueryable<QuestionnaireInitialContactAudit>, IOrderedQueryable<QuestionnaireInitialContactAudit>> orderBy, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialContactAudit, bool>> where, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes);
        void Insert(QuestionnaireInitialContactAudit item, bool saveChanges = true);
        void Update(QuestionnaireInitialContactAudit item, bool saveChanges = true);
        void Delete(QuestionnaireInitialContactAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialContactAuditService : IQuestionnaireInitialContactAuditService
    {
        private readonly IRepository<QuestionnaireInitialContactAudit> _repository;

        public QuestionnaireInitialContactAuditService(IRepository<QuestionnaireInitialContactAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialContactAudit Get(Expression<Func<QuestionnaireInitialContactAudit, bool>> where, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialContactAudit> GetMany(int? top, Expression<Func<QuestionnaireInitialContactAudit, bool>> where, Func<IQueryable<QuestionnaireInitialContactAudit>, IOrderedQueryable<QuestionnaireInitialContactAudit>> orderBy, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialContactAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactAudit, bool>> where, Func<IQueryable<QuestionnaireInitialContactAudit>, IOrderedQueryable<QuestionnaireInitialContactAudit>> orderBy, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialContactAudit, bool>> where, List<Expression<Func<QuestionnaireInitialContactAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialContactAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialContactAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialContactAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
