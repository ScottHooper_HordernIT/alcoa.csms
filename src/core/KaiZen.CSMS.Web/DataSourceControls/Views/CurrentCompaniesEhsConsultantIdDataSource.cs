﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CurrentCompaniesEhsConsultantIdProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CurrentCompaniesEhsConsultantIdDataSourceDesigner))]
	public class CurrentCompaniesEhsConsultantIdDataSource : ReadOnlyDataSource<CurrentCompaniesEhsConsultantId>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdDataSource class.
		/// </summary>
		public CurrentCompaniesEhsConsultantIdDataSource() : base(new CurrentCompaniesEhsConsultantIdService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CurrentCompaniesEhsConsultantIdDataSourceView used by the CurrentCompaniesEhsConsultantIdDataSource.
		/// </summary>
		protected CurrentCompaniesEhsConsultantIdDataSourceView CurrentCompaniesEhsConsultantIdView
		{
			get { return ( View as CurrentCompaniesEhsConsultantIdDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CurrentCompaniesEhsConsultantIdDataSourceView class that is to be
		/// used by the CurrentCompaniesEhsConsultantIdDataSource.
		/// </summary>
		/// <returns>An instance of the CurrentCompaniesEhsConsultantIdDataSourceView class.</returns>
		protected override BaseDataSourceView<CurrentCompaniesEhsConsultantId, Object> GetNewDataSourceView()
		{
			return new CurrentCompaniesEhsConsultantIdDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CurrentCompaniesEhsConsultantIdDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CurrentCompaniesEhsConsultantIdDataSourceView : ReadOnlyDataSourceView<CurrentCompaniesEhsConsultantId>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CurrentCompaniesEhsConsultantIdDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CurrentCompaniesEhsConsultantIdDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CurrentCompaniesEhsConsultantIdDataSourceView(CurrentCompaniesEhsConsultantIdDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CurrentCompaniesEhsConsultantIdDataSource CurrentCompaniesEhsConsultantIdOwner
		{
			get { return Owner as CurrentCompaniesEhsConsultantIdDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CurrentCompaniesEhsConsultantIdService CurrentCompaniesEhsConsultantIdProvider
		{
			get { return Provider as CurrentCompaniesEhsConsultantIdService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CurrentCompaniesEhsConsultantIdDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CurrentCompaniesEhsConsultantIdDataSource class.
	/// </summary>
	public class CurrentCompaniesEhsConsultantIdDataSourceDesigner : ReadOnlyDataSourceDesigner<CurrentCompaniesEhsConsultantId>
	{
	}

	#endregion CurrentCompaniesEhsConsultantIdDataSourceDesigner

	#region CurrentCompaniesEhsConsultantIdFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdFilter : SqlFilter<CurrentCompaniesEhsConsultantIdColumn>
	{
	}

	#endregion CurrentCompaniesEhsConsultantIdFilter

	#region CurrentCompaniesEhsConsultantIdExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CurrentCompaniesEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CurrentCompaniesEhsConsultantIdExpressionBuilder : SqlExpressionBuilder<CurrentCompaniesEhsConsultantIdColumn>
	{
	}
	
	#endregion CurrentCompaniesEhsConsultantIdExpressionBuilder		
}

