using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using KaiZen.CSMS.Entities;

using DevExpress.Web.ASPxEditors;

public partial class UserControls_Medical_Req : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            btnEdit.Visible = false;
            SessionHandler.ViewMode = "View";
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader): 

                    break;
                case ((int)RoleList.Contractor):

                    break;
                case ((int)RoleList.Administrator):
                    btnEdit.Visible = true;
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        if (SessionHandler.ViewMode != "")
        {
            if (SessionHandler.ViewMode == "View")
            {
                grid.Columns[0].Visible = true;
                SessionHandler.ViewMode = "Edit";
                btnEdit.Text = "(View)";
            }
            else
            {
                grid.Columns[0].Visible = false;
                SessionHandler.ViewMode = "View";
                btnEdit.Text = "(Edit)";
            }
        }
    }

    protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["DocumentType"] = "MM";
    }
    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        e.NewValues["DocumentType"] = "MM";
    }

    protected void grid_OnRowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        e.NewValues["DocumentType"] = "MM";
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data) return;

        string fileName = (string)grid.GetRowValues(e.VisibleIndex, "DocumentFileName");

        ASPxHyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFileDownload") as ASPxHyperLink;
        if (hl != null)
        {
            hl.Text = fileName;
            hl.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=MMR&File=" + fileName));
        }
    }
}