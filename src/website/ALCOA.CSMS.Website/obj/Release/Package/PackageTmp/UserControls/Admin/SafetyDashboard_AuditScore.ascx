﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SafetyDashboard_AuditScore.ascx.cs" Inherits="SafetyDashboard_AuditScore" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPanel" tagprefix="dx" %>
    <%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
  
 

  
 
  
  <table border="0" cellpadding="2" cellspacing="0" style="width: 92%">
 
   <tr>
                    <%--<td style="text-align: right" class="style4">
                        <strong>Site:</strong>
                    </td>
                    <td style="text-align: right; width: 200px" class="style3">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>--%>
                    
                       <td style="width: 5%;  text-align: right">
                                                    <strong>Year:</strong>
                                                 </td>
                                               <%-- <td style="width: 90px; height: 10px; text-align: left">
                                                    <dx:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                                                        ValueType="System.Int32" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Width="90px" ID="ddlMonth" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                        <Items>
                                                            <dx:ListEditItem Text="January" Value="1"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="February" Value="2"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="March" Value="3"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="April" Value="4"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="May" Value="5"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="June" Value="6"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="July" Value="7"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="August" Value="8"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="September" Value="9"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="October" Value="10"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="November" Value="11"></dx:ListEditItem>
                                                            <dx:ListEditItem Text="December" Value="12"></dx:ListEditItem>
                                                        </Items>
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dx:ASPxComboBox>
                                                </td>--%>

                    <%--<td style="width: 46px; height: 10px; text-align: right">
                        <strong>Year:</strong>
                    </td>
                    --%>
                    
                    <td style="text-align: right; width:8%" >
                        <dxe:ASPxComboBox ID="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                            Width="60px" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                            style="margin-left: 0px">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Cursor="pointer" Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="text-align: left; width: 95%" >
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="Go / Refresh" 
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"  
                            >
                        </dxe:ASPxButton>
                    </td>
                </tr>
  
 
  </table>

  <br />
  <br />

  <dxwgv:ASPxGridView
    ID="grid"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="SqlDataSource1"
    KeyFieldName="AuditScoreId"
    Width="900px"
    OnRowUpdating="grid_RowUpdating" onrowinserting="grid_RowInserting"  >
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dxwgv:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="20%">
           <NewButton Visible="true" />
           <EditButton Visible="True" />
           <DeleteButton Visible = "true" />
           <ClearFilterButton Visible="True" />
            <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewCommandColumn>


        <dxwgv:GridViewDataTextColumn FieldName="AuditScoreId" ReadOnly="True" Visible="False" VisibleIndex="0">
            <EditFormSettings Visible="False" />
             <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>


        <dxwgv:GridViewDataTextColumn Caption="Audit Score" FieldName="Description" VisibleIndex="1" Width="40%">
         <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataTextColumn>

        <dxwgv:GridViewDataComboBoxColumn FieldName="SiteId" VisibleIndex="2" 
                ReadOnly="false" SortIndex="0" SortOrder="Ascending" Caption="Sites" Width="40%">
                <HeaderStyle HorizontalAlign="Center" />
                <PropertiesComboBox DataSourceID="SqlDataSourceSites" TextField="SiteName" ValueField="SiteId"
                IncrementalFilteringMode="StartsWith">
                </PropertiesComboBox>
                <Settings SortMode="DisplayText"></Settings>
                 <HeaderStyle HorizontalAlign="Center" />
        </dxwgv:GridViewDataComboBoxColumn>

       <%-- <dxwgv:GridViewDataTextColumn Caption="Month" ReadOnly="true" FieldName="Month" VisibleIndex="3">
       
        </dxwgv:GridViewDataTextColumn>--%>
        <dxwgv:GridViewDataTextColumn Caption="Year" ReadOnly="true" FieldName="Year" VisibleIndex="3" 
        Settings-AllowSort="False" Settings-AllowHeaderFilter="False" Visible="false">
        <EditItemTemplate>

               <asp:Label ID="lblYear" runat="server" Visible="false"
               CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
               ></asp:Label>
                
            </EditItemTemplate>
             <HeaderStyle HorizontalAlign="Center" />
             <CellStyle HorizontalAlign="Left"/>
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Settings ShowHeaderFilterButton="true" />
    <SettingsPager Visible="False" Mode="ShowAllRecords" />
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>
  
   


    <asp:sqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SafetyDashBoard_AuditScore" SelectCommandType="StoredProcedure"
   
    DeleteCommand="SafetyDashBoard_AuditScore_Delete" DeleteCommandType="StoredProcedure"
    >

    <%-- UpdateCommand="SafetyDashBoard_AuditScore_UpdateGrid" UpdateCommandType="StoredProcedure"
    InsertCommand="SafetyDashBoard_AuditScore_InsertGrid" InsertCommandType="StoredProcedure"--%>
    
    <SelectParameters>
   <%-- <asp:ControlParameter ControlID="cbRegionSite" Name="SiteId"  Type="Int32"  />--%>
    <%--<asp:ControlParameter ControlID="ddlMonth" Name="Month" Type="Int32" />--%>
    <asp:ControlParameter ControlID="ddlYear" Name="Year" Type="Int32" />
    </SelectParameters>
   
   <%-- <UpdateParameters>
    <asp:Parameter Name="SiteId" Type="Int32" />
        
    <asp:ControlParameter ControlID="ddlYear" Name="Year" Type="Int32" />
        <asp:Parameter Name="AuditScoreId" Type="Int32" />
        <asp:Parameter Name="Description" Type="String" />
    </UpdateParameters>
    
    <InsertParameters>
    <asp:Parameter Name="SiteId" Type="Int32" />
        
    <asp:ControlParameter ControlID="ddlYear" Name="Year" Type="Int32" />
        <asp:Parameter Name="Description" Type="String" />
    </InsertParameters>--%>

    <DeleteParameters>
      
      <asp:Parameter Name="AuditScoreId" Type="Int32" />
    </DeleteParameters>

    </asp:sqlDataSource>

    

 <asp:sqlDataSource ID="SqlDataSourceSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="GetSitesRegions" SelectCommandType="StoredProcedure">
    
 </asp:sqlDataSource>
    

