﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Repo.CSMS.Common.Helpers
{
    #region Excel Export Data Annotation

    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
    public class ExcelSerializableAttribute : Attribute
    {
        public ExcelSerializableAttribute(int ordinal)
        {
            this.Ordinal = ordinal;
        }
        public int Ordinal { get; set; }
    }

    #endregion

    #region Pdf Export Data Annotation

    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
    public class PdfSerializableAttribute : Attribute
    {
        private float width = 100f;
        private string format = "";

        public PdfSerializableAttribute(int ordinal)
        {
            this.Ordinal = ordinal;
        }
        public int Ordinal { get; set; }
        public float Width { get { return this.width; } set { this.width = value; } }
        public string Format { get { return this.format; } set { this.format = value; } }
    }

    #endregion

    #region Edit Name Data Annotation

    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
    public class EditNameAttribute : System.Attribute
    {
        string editName = "";

        public EditNameAttribute(string editName)
        {
            this.editName = editName;
        }

        public string EditName
        {
            get { return this.editName; }
            set { this.editName = value; }
        }
    }

    #endregion

    #region File Download Annotation

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class FileDownloadAttribute : ActionFilterAttribute
    {
        public FileDownloadAttribute(string cookieName = "fileDownload", string cookiePath = "/")
        {
            CookieName = cookieName;
            CookiePath = cookiePath;
        }

        public string CookieName { get; set; }

        public string CookiePath { get; set; }

        /// <summary>
        /// If the current response is a FileResult (an MVC base class for files) then write a
        /// cookie to inform jquery.fileDownload that a successful file download has occured
        /// </summary>
        /// <param name="filterContext"></param>
        private void CheckAndHandleFileResult(ActionExecutedContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var response = httpContext.Response;

            if (filterContext.Result is FileResult)
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                response.AppendCookie(new HttpCookie(CookieName, "true") { Path = CookiePath });
            else
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                if (httpContext.Request.Cookies[CookieName] != null)
                {
                    response.AppendCookie(new HttpCookie(CookieName, "true") { Expires = DateTime.Now.AddYears(-1), Path = CookiePath });
                }
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            CheckAndHandleFileResult(filterContext);

            base.OnActionExecuted(filterContext);
        }
    }

    #endregion
}
