﻿using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUpdateReasonTypeService
    {
        UpdateReasonType Get(Expression<Func<UpdateReasonType, bool>> where, List<Expression<Func<UpdateReasonType, object>>> includes);
        List<UpdateReasonType> GetMany(int? top, Expression<Func<UpdateReasonType, bool>> where, Func<IQueryable<UpdateReasonType>, IOrderedQueryable<UpdateReasonType>> orderBy, List<Expression<Func<UpdateReasonType, object>>> includes);
        List<UpdateReasonType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UpdateReasonType, bool>> where, Func<IQueryable<UpdateReasonType>, IOrderedQueryable<UpdateReasonType>> orderBy, List<Expression<Func<UpdateReasonType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UpdateReasonType, bool>> where, List<Expression<Func<UpdateReasonType, object>>> includes);
        void Insert(UpdateReasonType item, bool saveChanges = true);
        void Update(UpdateReasonType item, bool saveChanges = true);
        void Delete(UpdateReasonType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UpdateReasonTypeService : IUpdateReasonTypeService
    {
        private readonly IRepository<UpdateReasonType> _repository;

        public UpdateReasonTypeService(IRepository<UpdateReasonType> repository)
        {
            this._repository = repository;
        }

        public UpdateReasonType Get(Expression<Func<UpdateReasonType, bool>> where, List<Expression<Func<UpdateReasonType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UpdateReasonType> GetMany(int? top, Expression<Func<UpdateReasonType, bool>> where, Func<IQueryable<UpdateReasonType>, IOrderedQueryable<UpdateReasonType>> orderBy, List<Expression<Func<UpdateReasonType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UpdateReasonType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UpdateReasonType, bool>> where, Func<IQueryable<UpdateReasonType>, IOrderedQueryable<UpdateReasonType>> orderBy, List<Expression<Func<UpdateReasonType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UpdateReasonType, bool>> where, List<Expression<Func<UpdateReasonType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UpdateReasonType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UpdateReasonType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UpdateReasonType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UpdateReasonType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UpdateReasonType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UpdateReasonType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
