﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CompanyNote' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CompanyNote : CompanyNoteBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CompanyNote"/> instance.
		///</summary>
		public CompanyNote():base(){}	
		
		#endregion
    }
}
