﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnairePresentlyWithActionService
    {
        QuestionnairePresentlyWithAction Get(Expression<Func<QuestionnairePresentlyWithAction, bool>> where, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes);
        List<QuestionnairePresentlyWithAction> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithAction, bool>> where, Func<IQueryable<QuestionnairePresentlyWithAction>, IOrderedQueryable<QuestionnairePresentlyWithAction>> orderBy, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes);
        List<QuestionnairePresentlyWithAction> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithAction, bool>> where, Func<IQueryable<QuestionnairePresentlyWithAction>, IOrderedQueryable<QuestionnairePresentlyWithAction>> orderBy, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnairePresentlyWithAction, bool>> where, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes);
        void Insert(QuestionnairePresentlyWithAction item, bool saveChanges = true);
        void Update(QuestionnairePresentlyWithAction item, bool saveChanges = true);
        void Delete(QuestionnairePresentlyWithAction item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnairePresentlyWithActionService : IQuestionnairePresentlyWithActionService
    {
        private readonly IRepository<QuestionnairePresentlyWithAction> _repository;

        public QuestionnairePresentlyWithActionService(IRepository<QuestionnairePresentlyWithAction> repository)
        {
            this._repository = repository;
        }

        public QuestionnairePresentlyWithAction Get(Expression<Func<QuestionnairePresentlyWithAction, bool>> where, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnairePresentlyWithAction> GetMany(int? top, Expression<Func<QuestionnairePresentlyWithAction, bool>> where, Func<IQueryable<QuestionnairePresentlyWithAction>, IOrderedQueryable<QuestionnairePresentlyWithAction>> orderBy, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnairePresentlyWithAction> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnairePresentlyWithAction, bool>> where, Func<IQueryable<QuestionnairePresentlyWithAction>, IOrderedQueryable<QuestionnairePresentlyWithAction>> orderBy, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnairePresentlyWithAction, bool>> where, List<Expression<Func<QuestionnairePresentlyWithAction, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnairePresentlyWithAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithAction");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnairePresentlyWithAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithAction");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnairePresentlyWithAction item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnairePresentlyWithAction");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
