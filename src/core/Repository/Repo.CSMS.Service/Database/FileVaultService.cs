﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IFileVaultService
    {
        FileVault Get(Expression<Func<FileVault, bool>> where, List<Expression<Func<FileVault, object>>> includes);
        List<FileVault> GetMany(int? top, Expression<Func<FileVault, bool>> where, Func<IQueryable<FileVault>, IOrderedQueryable<FileVault>> orderBy, List<Expression<Func<FileVault, object>>> includes);
        List<FileVault> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVault, bool>> where, Func<IQueryable<FileVault>, IOrderedQueryable<FileVault>> orderBy, List<Expression<Func<FileVault, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<FileVault, bool>> where, List<Expression<Func<FileVault, object>>> includes);
        void Insert(FileVault item, bool saveChanges = true);
        void Update(FileVault item, bool saveChanges = true);
        void Delete(FileVault item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class FileVaultService : IFileVaultService
    {
        private readonly IRepository<FileVault> _repository;

        public FileVaultService(IRepository<FileVault> repository)
        {
            this._repository = repository;
        }

        public FileVault Get(Expression<Func<FileVault, bool>> where, List<Expression<Func<FileVault, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<FileVault> GetMany(int? top, Expression<Func<FileVault, bool>> where, Func<IQueryable<FileVault>, IOrderedQueryable<FileVault>> orderBy, List<Expression<Func<FileVault, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<FileVault> GetManyPaged(int pageNumber, int pageSize, Expression<Func<FileVault, bool>> where, Func<IQueryable<FileVault>, IOrderedQueryable<FileVault>> orderBy, List<Expression<Func<FileVault, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<FileVault, bool>> where, List<Expression<Func<FileVault, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(FileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVault");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(FileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVault");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(FileVault item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("FileVault");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
