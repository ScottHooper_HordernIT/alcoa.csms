﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using LumenWorks.Framework.IO.Csv;

using System.IO;
using System.Data;
using System.Collections;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class BulkUpdateCompanySiteCategoryStandard : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        static string sessionName = "dsAdminBulkUploadCompanySiteCategoryStandard";

        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxGridView1.DataSource = Session[sessionName];
            ASPxGridView1.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                Upload(true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            try
            {
            Upload(false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Upload(bool preview)
        {
            if (Page.IsPostBack)
            {
                Configuration configuration = new Configuration();
                string tempPath = @configuration.GetValue(ConfigList.TempDir);

                if (uc.UploadedFiles != null && uc.UploadedFiles.Length > 0)
                {
                    if (!String.IsNullOrEmpty(uc.UploadedFiles[0].FileName) && uc.UploadedFiles[0].IsValid)
                    {
                        //string filename = uc.UploadedFiles[0].FileName;
                        string fileName = System.Guid.NewGuid().ToString();
                        string fileNameInclPath = tempPath + fileName;
                        uc.UploadedFiles[0].SaveAs(fileNameInclPath, true);

                        DataSet ds = new DataSet();
                        DataTable dt = new DataTable();
                        ds.Tables.Add(dt);
                        dt.Columns.Add("Valid", typeof(string));
                        dt.Columns.Add("System Comments", typeof(string));
                        dt.Columns.Add("Company Name", typeof(string));
                        dt.Columns.Add("Site", typeof(string));
                        dt.Columns.Add("Residential Category", typeof(string));
                        dt.Columns.Add("Approved", typeof(string));
                        dt.Columns.Add("Location Sponsor", typeof(string));
                        dt.Columns.Add("Location SPA", typeof(string));
                        dt.Columns.Add("ARP", typeof(string));
                        dt.Columns.Add("CRP", typeof(string));

                        using (CsvReader csv = new CsvReader(new StreamReader(fileNameInclPath), true))
                        {
                            int fieldCount = csv.FieldCount;
                            string[] headers = csv.GetFieldHeaders();

                            CompaniesService cService = new CompaniesService();
                            TList<Companies> companiesList = cService.GetAll();

                            SitesService sService = new SitesService();
                            TList<Sites> sitesList = sService.GetAll();

                            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                            TList<CompanySiteCategory> companySiteCategoryList = cscService.GetAll();

                            CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();

                            UsersAlcoanService uaService = new UsersAlcoanService();
                            VList<UsersAlcoan> usersAlcoanList = uaService.GetAll();

                            while (csv.ReadNextRecord())
                            {
                                DataRow dr = dt.NewRow();
                                dr["Company Name"] = csv["Company Name"];
                                dr["Site"] = csv["Site"];
                                dr["Residential Category"] = csv["Residential Category"];
                                dr["Approved"] = csv["Approved"];
                                dr["Location Sponsor"] = csv["Location Sponsor"];
                                dr["Location SPA"] = csv["Location SPA"];
                                dr["ARP"] = csv["ARP"];
                                dr["CRP"] = csv["CRP"];

                                try
                                {
                                    string CouldNotReadOrFind = "";

                                    int? companyId = null;
                                    int? siteId = null;
                                    int? residentialCategory = null;
                                    bool? approved = null;
                                    int? locationSponsorUserId = null;
                                    int? locationSpaUserId = null;
                                    int? arpUserId = null;
                                    int? crpUserId = null;

                                    //Company Name
                                    if (!String.IsNullOrEmpty(dr["Company Name"].ToString()))
                                    {
                                        Companies c = companiesList.Find(CompaniesColumn.CompanyName, dr["Company Name"]);
                                        if (c == null)
                                            CouldNotReadOrFind += "Company Name; ";
                                        else
                                            companyId = c.CompanyId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Company Name; ";
                                    }

                                    //Site
                                    if (!String.IsNullOrEmpty(dr["Site"].ToString()))
                                    {
                                        Sites s = sitesList.Find(SitesColumn.SiteName, dr["Site"]);
                                        if (s == null)
                                            CouldNotReadOrFind += "Site; ";
                                        else
                                            siteId = s.SiteId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Site; ";
                                    }

                                    //Residential Category
                                    if (!String.IsNullOrEmpty(dr["Residential Category"].ToString()))
                                    {
                                        CompanySiteCategory csc = companySiteCategoryList.Find(CompanySiteCategoryColumn.CategoryDesc, dr["Residential Category"]);
                                        if (csc == null)
                                            CouldNotReadOrFind += "Residential Category; ";
                                        else
                                            residentialCategory = csc.CompanySiteCategoryId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Residential Category; ";
                                    }

                                    //Approved
                                    if (dr["Approved"].ToString() == "Yes" || dr["Approved"].ToString() == "No" || String.IsNullOrEmpty(dr["Approved"].ToString()))
                                    {
                                        if (dr["Approved"].ToString() == "Yes")
                                            approved = true;
                                        if (dr["Approved"].ToString() == "No")
                                            approved = false;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Approved; ";
                                    }


                                    //Location Sponsor
                                    if (!String.IsNullOrEmpty(dr["Location Sponsor"].ToString()))
                                    {
                                        UsersAlcoan uaLocationSponsor = usersAlcoanList.Find(UsersAlcoanColumn.UserFullName, dr["Location Sponsor"]);
                                        if (uaLocationSponsor == null)
                                            CouldNotReadOrFind += "Location Sponsor; ";
                                        else
                                            locationSponsorUserId = uaLocationSponsor.UserId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Location Sponsor; ";
                                    }

                                    //Location SPA
                                    if (!String.IsNullOrEmpty(dr["Location SPA"].ToString()))
                                    {
                                        UsersAlcoan uaLocationSpa = usersAlcoanList.Find(UsersAlcoanColumn.UserFullName, dr["Location SPA"]);
                                        if (uaLocationSpa == null)
                                            CouldNotReadOrFind += "Location SPA; ";
                                        else
                                            locationSpaUserId = uaLocationSpa.UserId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "Location SPA; ";
                                    }

                                    //ARP
                                    if (!String.IsNullOrEmpty(dr["ARP"].ToString()))
                                    {
                                        UsersAlcoan uaArp = usersAlcoanList.Find(UsersAlcoanColumn.UserFullName, dr["ARP"]);
                                        if (uaArp == null)
                                            CouldNotReadOrFind += "ARP; ";
                                        else
                                            arpUserId = uaArp.UserId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "ARP; ";
                                    }

                                    //CRP
                                    if (!String.IsNullOrEmpty(dr["CRP"].ToString()))
                                    {
                                        UsersAlcoan uaCrp = usersAlcoanList.Find(UsersAlcoanColumn.UserFullName, dr["CRP"]);
                                        if (uaCrp == null)
                                            CouldNotReadOrFind += "CRP; ";
                                        else
                                            crpUserId = uaCrp.UserId;
                                    }
                                    else
                                    {
                                        CouldNotReadOrFind += "CRP; ";
                                    }

                                    if (String.IsNullOrEmpty(CouldNotReadOrFind))
                                    {
                                        dr["Valid"] = "Yes";

                                        if (!preview)
                                        {
                                            TransactionManager transactionManager = null;
                                            try
                                            {
                                                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                                                KaiZen.CSMS.Entities.CompanySiteCategoryStandard csc = cscsService.GetByCompanyIdSiteId(Convert.ToInt32(companyId), Convert.ToInt32(siteId));
                                                if (csc == null)
                                                {
                                                    csc = new KaiZen.CSMS.Entities.CompanySiteCategoryStandard();
                                                }
                                                csc.CompanySiteCategoryId = residentialCategory;
                                                csc.Approved = approved;
                                                csc.LocationSponsorUserId = locationSponsorUserId;
                                                csc.LocationSpaUserId = locationSpaUserId;
                                                csc.ArpUserId = arpUserId;
                                                //csc.CrpUserId = crpUserId;

                                                if (csc.EntityState == EntityState.Changed)
                                                {
                                                    csc.ModifiedByUserId = auth.UserId;
                                                    csc.ModifiedDate = DateTime.Now;
                                                }

                                                DataRepository.CompanySiteCategoryStandardProvider.Update(transactionManager, csc);
                                                transactionManager.Commit();
                                            }
                                            catch (Exception ex)
                                            {
                                                throw new Exception("Could Not Update Record, Error Message: " + ex.Message);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dr["Valid"] = "No";
                                        dr["System Comments"] = "Invalid Values: " + CouldNotReadOrFind;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    dr["Valid"] = "No";
                                    dr["System Comments"] = "Error: " + ex.Message;
                                }

                                dt.Rows.Add(dr);
                            }
                        }
                        File.Delete(fileNameInclPath);

                        Session[sessionName] = ds;
                        ASPxGridView1.DataSource = Session[sessionName];
                        ASPxGridView1.DataBind();
                    }
                }
            }
        }
    }
}