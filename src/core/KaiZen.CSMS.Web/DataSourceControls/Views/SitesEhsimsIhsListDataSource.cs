﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SitesEhsimsIhsListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(SitesEhsimsIhsListDataSourceDesigner))]
	public class SitesEhsimsIhsListDataSource : ReadOnlyDataSource<SitesEhsimsIhsList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListDataSource class.
		/// </summary>
		public SitesEhsimsIhsListDataSource() : base(new SitesEhsimsIhsListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SitesEhsimsIhsListDataSourceView used by the SitesEhsimsIhsListDataSource.
		/// </summary>
		protected SitesEhsimsIhsListDataSourceView SitesEhsimsIhsListView
		{
			get { return ( View as SitesEhsimsIhsListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SitesEhsimsIhsListDataSourceView class that is to be
		/// used by the SitesEhsimsIhsListDataSource.
		/// </summary>
		/// <returns>An instance of the SitesEhsimsIhsListDataSourceView class.</returns>
		protected override BaseDataSourceView<SitesEhsimsIhsList, Object> GetNewDataSourceView()
		{
			return new SitesEhsimsIhsListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SitesEhsimsIhsListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SitesEhsimsIhsListDataSourceView : ReadOnlyDataSourceView<SitesEhsimsIhsList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SitesEhsimsIhsListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SitesEhsimsIhsListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SitesEhsimsIhsListDataSourceView(SitesEhsimsIhsListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SitesEhsimsIhsListDataSource SitesEhsimsIhsListOwner
		{
			get { return Owner as SitesEhsimsIhsListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SitesEhsimsIhsListService SitesEhsimsIhsListProvider
		{
			get { return Provider as SitesEhsimsIhsListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region SitesEhsimsIhsListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SitesEhsimsIhsListDataSource class.
	/// </summary>
	public class SitesEhsimsIhsListDataSourceDesigner : ReadOnlyDataSourceDesigner<SitesEhsimsIhsList>
	{
	}

	#endregion SitesEhsimsIhsListDataSourceDesigner

	#region SitesEhsimsIhsListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListFilter : SqlFilter<SitesEhsimsIhsListColumn>
	{
	}

	#endregion SitesEhsimsIhsListFilter

	#region SitesEhsimsIhsListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SitesEhsimsIhsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SitesEhsimsIhsListExpressionBuilder : SqlExpressionBuilder<SitesEhsimsIhsListColumn>
	{
	}
	
	#endregion SitesEhsimsIhsListExpressionBuilder		
}

