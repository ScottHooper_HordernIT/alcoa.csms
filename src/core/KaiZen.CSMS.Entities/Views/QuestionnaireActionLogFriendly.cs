﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'QuestionnaireActionLogFriendly' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class QuestionnaireActionLogFriendly : QuestionnaireActionLogFriendlyBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="QuestionnaireActionLogFriendly"/> instance.
		///</summary>
		public QuestionnaireActionLogFriendly():base(){}	
		
		#endregion
	}
}
