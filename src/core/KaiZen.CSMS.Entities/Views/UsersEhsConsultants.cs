﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'UsersEhsConsultants' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UsersEhsConsultants : UsersEhsConsultantsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UsersEhsConsultants"/> instance.
		///</summary>
		public UsersEhsConsultants():base(){}	
		
		#endregion
	}
}
