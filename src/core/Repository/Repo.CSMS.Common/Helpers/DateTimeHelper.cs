﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using System.Globalization;

namespace Repo.CSMS.Common.Helpers
{
    /// <summary>
    /// Represents a datetime helper
    /// </summary>
    public static class DateTimeHelper
    {
        #region Methods

        public static DateTime? ParseNullableDateTime(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return DateTime.Parse(s);
        }

        /// <summary>
        /// Converts the date and time to current user date and time
        /// </summary>
        /// <param name="dt">The date and time (respesents local system time or UTC time) to convert.</param>
        /// <returns>A DateTime value that represents time that corresponds to the dateTime parameter in customer time zone.</returns>
        public static DateTime ConvertToUserTime(DateTime dt)
        {
            return ConvertToUserTime(dt, dt.Kind);
        }

        /// <summary>
        /// Converts the date and time to current user date and time
        /// </summary>
        /// <param name="dt">The date and time (respesents local system time or UTC time) to convert.</param>
        /// <param name="sourceDateTimeKind">The source datetimekind</param>
        /// <returns>A DateTime value that represents time that corresponds to the dateTime parameter in customer time zone.</returns>
        public static DateTime ConvertToUserTime(DateTime dt, DateTimeKind sourceDateTimeKind)
        {
            dt = DateTime.SpecifyKind(dt, sourceDateTimeKind);
            var currentUserTimeZoneInfo = TimeZoneInfo.Local;
            return TimeZoneInfo.ConvertTime(dt, currentUserTimeZoneInfo);
        }

        /// <summary>
        /// Converts the date and time to current user date and time
        /// </summary>
        /// <param name="dt">The date and time to convert.</param>
        /// <param name="sourceTimeZone">The time zone of dateTime.</param>
        /// <returns>A DateTime value that represents time that corresponds to the dateTime parameter in customer time zone.</returns>
        public static DateTime ConvertToUserTime(DateTime dt, TimeZoneInfo sourceTimeZone)
        {
            var currentUserTimeZoneInfo = TimeZoneInfo.Local;
            return ConvertToUserTime(dt, sourceTimeZone, currentUserTimeZoneInfo);
        }

        /// <summary>
        /// Converts the date and time to current user date and time
        /// </summary>
        /// <param name="dt">The date and time to convert.</param>
        /// <param name="sourceTimeZone">The time zone of dateTime.</param>
        /// <param name="destinationTimeZone">The time zone to convert dateTime to.</param>
        /// <returns>A DateTime value that represents time that corresponds to the dateTime parameter in customer time zone.</returns>
        public static DateTime ConvertToUserTime(DateTime dt, TimeZoneInfo sourceTimeZone, TimeZoneInfo destinationTimeZone)
        {
            return TimeZoneInfo.ConvertTime(dt, sourceTimeZone, destinationTimeZone);
        }

        /// <summary>
        /// Converts the date and time to Coordinated Universal Time (UTC)
        /// </summary>
        /// <param name="dt">The date and time (respesents local system time or UTC time) to convert.</param>
        /// <returns>A DateTime value that represents the Coordinated Universal Time (UTC) that corresponds to the dateTime parameter. The DateTime value's Kind property is always set to DateTimeKind.Utc.</returns>
        public static DateTime ConvertToUtcTime(DateTime dt)
        {
            return ConvertToUtcTime(dt, dt.Kind);
        }

        /// <summary>
        /// Converts the date and time to Coordinated Universal Time (UTC)
        /// </summary>
        /// <param name="dt">The date and time (respesents local system time or UTC time) to convert.</param>
        /// <param name="sourceDateTimeKind">The source datetimekind</param>
        /// <returns>A DateTime value that represents the Coordinated Universal Time (UTC) that corresponds to the dateTime parameter. The DateTime value's Kind property is always set to DateTimeKind.Utc.</returns>
        public static DateTime ConvertToUtcTime(DateTime dt, DateTimeKind sourceDateTimeKind)
        {
            dt = DateTime.SpecifyKind(dt, sourceDateTimeKind);
            return TimeZoneInfo.ConvertTimeToUtc(dt);
        }

        /// <summary>
        /// Converts the date and time to Coordinated Universal Time (UTC)
        /// </summary>
        /// <param name="dt">The date and time to convert.</param>
        /// <param name="sourceTimeZone">The time zone of dateTime.</param>
        /// <returns>A DateTime value that represents the Coordinated Universal Time (UTC) that corresponds to the dateTime parameter. The DateTime value's Kind property is always set to DateTimeKind.Utc.</returns>
        public static DateTime ConvertToUtcTime(DateTime dt, TimeZoneInfo sourceTimeZone)
        {
            if (sourceTimeZone.IsInvalidTime(dt))
            {
                //could not convert
                return dt;
            }
            else
            {
                return TimeZoneInfo.ConvertTimeToUtc(dt, sourceTimeZone);
            }
        }

        /// <summary>
        /// Convert the parsed date to the end of the current day (ie. last minute of current day)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>End time on current day</returns>
        public static DateTime EndOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        /// <summary>
        /// Convert the parsed date to the start of the current day (ie. first minute of current day)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>Start time on current day</returns>
        public static DateTime StartOfDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
        }

        /// <summary>
        /// Get the first day of the month for the given date (midnight)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>First day of month</returns>
        public static DateTime FirstDayOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1, 0, 0, 0);
        }

        /// <summary>
        /// Get the last day of the month for the given date (11:59:59 pm)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>Last day of month</returns>
        public static DateTime LastDayOfMonth(DateTime date)
        {
            DateTime firstDayOfMonth = FirstDayOfMonth(date);
            return date.AddMonths(1).AddSeconds(-1);
        }

        /// <summary>
        /// Get the first day of the week for the given date (midnight)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>First day of week</returns>        
        public static DateTime FirstDayOfWeek(DateTime date, DayOfWeek firstDayOfWeek)
        {
            int offset = date.DayOfWeek - firstDayOfWeek;
            DateTime firstDay = date.AddDays(-offset);
            return new DateTime(firstDay.Year, firstDay.Month, firstDay.Day, 0, 0, 0); 
        }

        /// <summary>
        /// Get the last day of the week for the given date (11:59:59 pm)
        /// </summary>
        /// <param name="date">Given date</param>
        /// <returns>Last day of week</returns>
        public static DateTime LastDayOfWeek(DateTime date, DayOfWeek firstDayOfWeek)
        {
            int offset = date.DayOfWeek - firstDayOfWeek;
            DateTime firstDay = date.AddDays(-offset);
            DateTime lastDay = firstDay.AddDays(6);
            return new DateTime(lastDay.Year, lastDay.Month, lastDay.Day, 23, 59, 59); 
        }

        /// <summary>
        /// Get the number of week difference between given first date and last date
        /// </summary>
        /// <param name="firstDate">Given first date</param>
        /// <param name="lastDate">Given last date</param>
        /// <returns>Whole week difference between two dates</returns>
        public static int WeekDifference(DateTime firstDate, DateTime lastDate)
        {
            return (int)((lastDate - firstDate).TotalDays / 7);
        }

        #endregion
    }

    #region DateTime Binders

    public class DateTimeBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            var date = value.ConvertTo(typeof(DateTime), CultureInfo.CurrentCulture);

            return date;
        }
    }
    public class NullableDateTimeBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null)
            {
                var date = value.ConvertTo(typeof(DateTime), CultureInfo.CurrentCulture);
                return date;
            }
            return null;
        }
    }

    #endregion
}
