﻿<%@ Control Language="C#" AutoEventWireup="true"
    CodeBehind="SafetyRecognitionProgram.ascx.cs" Inherits="UserControls_SafetyRecognitionProgram" %>

<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Src="~/UserControls/Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<style type="text/css">
    .style1 {
        height: 10px;
        width: 336px;
    }

    .style2 {
        height: 10px;
        width: 1187px;
    }

    .style3 {
        height: 10px;
        width: 174px;
    }
</style>

<table border="0" cellpadding="2" cellspacing="0" width="80%">
    <tr>
        <td class="pageName" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Recognition Program</span><br />

                <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;</span>
        </td>
    </tr>
    <tr>
        <td style="height: 17px">
            <table border="0">
                <tr>
                    <td style="width: 100px; height: 10px; text-align: right">
                        <strong>Site/Region:</strong>
                    </td>
                    <td style="text-align: right; width: 200px" class="style3">
                        <dxe:ASPxComboBox ID="cbRegionSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" SelectedIndex="0"
                            ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>

                    <td style="width: 46px; height: 10px; text-align: right">
                        <strong>Year:</strong>
                    </td>

                    <td style="text-align: left; width: 86px" class="style2">
                        <dxe:ASPxComboBox ID="ddlYear" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith" ValueType="System.Int32"
                            Width="60px"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            Style="margin-left: 0px">
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Cursor="pointer" Width="13px">
                            </ButtonStyle>
                        </dxe:ASPxComboBox>
                    </td>
                    <td style="text-align: left; width: 400px" class="style1">
                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" Text="Go / Refresh"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            OnClick="ASPxButton1_Click">
                        </dxe:ASPxButton>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr align="center">
        </tr> <!-- This and line below added by AG to replace lines at bottom that were causing grid to not render properly.  DT407 27/11/2015-->
    </table>
        <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            Width="897px">
            <TabPages>
                <dx:TabPage Text="Safety Rating By TRIFR">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl2" runat="server" SupportsDisabledAttribute="True">
                            <%--Add First Grid Sayani--%>

                            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <dxwgv:ASPxGridView ID="grid"
                                            ClientInstanceName="grid"
                                            runat="server"
                                            AutoGenerateColumns="False"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue"
                                            Width="100%"
                                            KeyFieldName="CompanyID"
                                            OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                            OnHtmlRowPrepared="grid_HtmlRowPrepared">
                                            <Columns>
                                                <%--  <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyID" Name="CompanyBox"
                        VisibleIndex="0">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataComboBoxColumn>--%>

                                                <%-- <dxwgv:GridViewDataTextColumn Caption="Safety Compliance Category" SortIndex="0" SortOrder="Ascending"
                                                    FieldName="CategoryDesc" VisibleIndex="1" >
                        <Settings SortMode="DisplayText" />
                    </dxwgv:GridViewDataTextColumn>--%>
                                                <dxwgv:GridViewDataTextColumn Caption="Company Name" FieldName="CompanyName" VisibleIndex="0">
                                                    <HeaderStyle HorizontalAlign="left"></HeaderStyle>

                                                    <CellStyle HorizontalAlign="left"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>

                                                <dxwgv:GridViewDataTextColumn Caption="TRIFR" FieldName="TRIFR" VisibleIndex="1">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>

                                                <dxwgv:GridViewDataTextColumn Caption="AIFR" FieldName="AIFR" VisibleIndex="2">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>

                                                <dxwgv:GridViewDataTextColumn Caption="Color" FieldName="Color" VisibleIndex="3"
                                                    Visible="false">
                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                                    <CellStyle HorizontalAlign="Center"></CellStyle>
                                                </dxwgv:GridViewDataTextColumn>




                                            </Columns>
                                            <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                                </LoadingPanelOnStatusBar>
                                                <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif">
                                                    <%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                                </LoadingPanel>
                                            </Images>
                                            <Settings ShowGroupedColumns="True" ShowFilterRow="True" />
                                            <%--<SettingsPager Visible="true" PageSize="20" Mode="ShowPager">
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                </SettingsPager>--%>

                                            <SettingsPager Visible="true" PageSize="15" Mode="ShowPager">
                                                <AllButton Visible="true">
                                                </AllButton>
                                            </SettingsPager>
                                            <SettingsCustomizationWindow Enabled="True" />
                                            <ImagesFilterControl>
                                                <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                                </LoadingPanel>
                                            </ImagesFilterControl>
                                            <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue">
                                                <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                                </Header>

                                                <LoadingPanel ImageSpacing="10px">
                                                </LoadingPanel>
                                            </Styles>
                                            <SettingsBehavior ConfirmDelete="True" />


                                            <StylesEditors>
                                                <ProgressBar Height="25px">
                                                </ProgressBar>
                                            </StylesEditors>


                                        </dxwgv:ASPxGridView>








                                    </td>
                                </tr>
                                <tr align="right">
                                    <td align="right" class="pageName" style="height: 30px; text-align: right; text-align: -moz-right; width: 900px;">
                                        <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                                    </td>
                                </tr>
                            </table>





                        </dx:ContentControl>
                    </ContentCollection>

                </dx:TabPage>

                <dx:TabPage Text="Injury Free">
                    <ContentCollection>
                        <dx:ContentControl ID="ContentControl1" runat="server" SupportsDisabledAttribute="True">
                            <%--Add Second Grid Sayani--%>
                            
                            <table runat="server" id="Tlb_InjuryFree" border="1" cellpadding="2" cellspacing="0" width="100%">
                                <%-- 
                    <tr>
                    <td >sa</td>
                    <td >rr</td>
                    <td>aa</td>
                    </tr>
                    <tr>
                    <td >dd</td>
                    <td >x</td>
                    <td>xx</td>
                    </tr>--%>
                            </table>

                        </dx:ContentControl>
                    </ContentCollection>
                </dx:TabPage>
            </TabPages>
            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
            </LoadingPanelImage>
            <LoadingPanelStyle ImageSpacing="6px">
            </LoadingPanelStyle>
            <ContentStyle>
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
                <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            </ContentStyle>
        </dx:ASPxPageControl>

    <%--</tr> Commented out by AG 27/11/2015 as per support item DT407
</table>--%>
