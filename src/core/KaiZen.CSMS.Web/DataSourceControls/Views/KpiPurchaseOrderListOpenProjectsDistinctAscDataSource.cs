﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiPurchaseOrderListOpenProjectsDistinctAscProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceDesigner))]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscDataSource : ReadOnlyDataSource<KpiPurchaseOrderListOpenProjectsDistinctAsc>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource class.
		/// </summary>
		public KpiPurchaseOrderListOpenProjectsDistinctAscDataSource() : base(new KpiPurchaseOrderListOpenProjectsDistinctAscService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView used by the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource.
		/// </summary>
		protected KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView KpiPurchaseOrderListOpenProjectsDistinctAscView
		{
			get { return ( View as KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView class that is to be
		/// used by the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource.
		/// </summary>
		/// <returns>An instance of the KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiPurchaseOrderListOpenProjectsDistinctAsc, Object> GetNewDataSourceView()
		{
			return new KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView : ReadOnlyDataSourceView<KpiPurchaseOrderListOpenProjectsDistinctAsc>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceView(KpiPurchaseOrderListOpenProjectsDistinctAscDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiPurchaseOrderListOpenProjectsDistinctAscDataSource KpiPurchaseOrderListOpenProjectsDistinctAscOwner
		{
			get { return Owner as KpiPurchaseOrderListOpenProjectsDistinctAscDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiPurchaseOrderListOpenProjectsDistinctAscService KpiPurchaseOrderListOpenProjectsDistinctAscProvider
		{
			get { return Provider as KpiPurchaseOrderListOpenProjectsDistinctAscService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiPurchaseOrderListOpenProjectsDistinctAscDataSource class.
	/// </summary>
	public class KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiPurchaseOrderListOpenProjectsDistinctAsc>
	{
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscDataSourceDesigner

	#region KpiPurchaseOrderListOpenProjectsDistinctAscFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscFilter : SqlFilter<KpiPurchaseOrderListOpenProjectsDistinctAscColumn>
	{
	}

	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscFilter

	#region KpiPurchaseOrderListOpenProjectsDistinctAscExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderListOpenProjectsDistinctAsc"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListOpenProjectsDistinctAscExpressionBuilder : SqlExpressionBuilder<KpiPurchaseOrderListOpenProjectsDistinctAscColumn>
	{
	}
	
	#endregion KpiPurchaseOrderListOpenProjectsDistinctAscExpressionBuilder		
}

