﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EbiIssueProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(EbiIssueDataSourceDesigner))]
	public class EbiIssueDataSource : ProviderDataSource<EbiIssue, EbiIssueKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueDataSource class.
		/// </summary>
		public EbiIssueDataSource() : base(new EbiIssueService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EbiIssueDataSourceView used by the EbiIssueDataSource.
		/// </summary>
		protected EbiIssueDataSourceView EbiIssueView
		{
			get { return ( View as EbiIssueDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the EbiIssueDataSource control invokes to retrieve data.
		/// </summary>
		public EbiIssueSelectMethod SelectMethod
		{
			get
			{
				EbiIssueSelectMethod selectMethod = EbiIssueSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (EbiIssueSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EbiIssueDataSourceView class that is to be
		/// used by the EbiIssueDataSource.
		/// </summary>
		/// <returns>An instance of the EbiIssueDataSourceView class.</returns>
		protected override BaseDataSourceView<EbiIssue, EbiIssueKey> GetNewDataSourceView()
		{
			return new EbiIssueDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EbiIssueDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EbiIssueDataSourceView : ProviderDataSourceView<EbiIssue, EbiIssueKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiIssueDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EbiIssueDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EbiIssueDataSourceView(EbiIssueDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EbiIssueDataSource EbiIssueOwner
		{
			get { return Owner as EbiIssueDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal EbiIssueSelectMethod SelectMethod
		{
			get { return EbiIssueOwner.SelectMethod; }
			set { EbiIssueOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EbiIssueService EbiIssueProvider
		{
			get { return Provider as EbiIssueService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<EbiIssue> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<EbiIssue> results = null;
			EbiIssue item;
			count = 0;
			
			System.Int32 _ebiIssueId;
			System.String _issueName;

			switch ( SelectMethod )
			{
				case EbiIssueSelectMethod.Get:
					EbiIssueKey entityKey  = new EbiIssueKey();
					entityKey.Load(values);
					item = EbiIssueProvider.Get(entityKey);
					results = new TList<EbiIssue>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case EbiIssueSelectMethod.GetAll:
                    results = EbiIssueProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case EbiIssueSelectMethod.GetPaged:
					results = EbiIssueProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case EbiIssueSelectMethod.Find:
					if ( FilterParameters != null )
						results = EbiIssueProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = EbiIssueProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case EbiIssueSelectMethod.GetByEbiIssueId:
					_ebiIssueId = ( values["EbiIssueId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["EbiIssueId"], typeof(System.Int32)) : (int)0;
					item = EbiIssueProvider.GetByEbiIssueId(_ebiIssueId);
					results = new TList<EbiIssue>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case EbiIssueSelectMethod.GetByIssueName:
					_issueName = ( values["IssueName"] != null ) ? (System.String) EntityUtil.ChangeType(values["IssueName"], typeof(System.String)) : string.Empty;
					item = EbiIssueProvider.GetByIssueName(_issueName);
					results = new TList<EbiIssue>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == EbiIssueSelectMethod.Get || SelectMethod == EbiIssueSelectMethod.GetByEbiIssueId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				EbiIssue entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					EbiIssueProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<EbiIssue> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			EbiIssueProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region EbiIssueDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EbiIssueDataSource class.
	/// </summary>
	public class EbiIssueDataSourceDesigner : ProviderDataSourceDesigner<EbiIssue, EbiIssueKey>
	{
		/// <summary>
		/// Initializes a new instance of the EbiIssueDataSourceDesigner class.
		/// </summary>
		public EbiIssueDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiIssueSelectMethod SelectMethod
		{
			get { return ((EbiIssueDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new EbiIssueDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region EbiIssueDataSourceActionList

	/// <summary>
	/// Supports the EbiIssueDataSourceDesigner class.
	/// </summary>
	internal class EbiIssueDataSourceActionList : DesignerActionList
	{
		private EbiIssueDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the EbiIssueDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public EbiIssueDataSourceActionList(EbiIssueDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public EbiIssueSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion EbiIssueDataSourceActionList
	
	#endregion EbiIssueDataSourceDesigner
	
	#region EbiIssueSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the EbiIssueDataSource.SelectMethod property.
	/// </summary>
	public enum EbiIssueSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByEbiIssueId method.
		/// </summary>
		GetByEbiIssueId,
		/// <summary>
		/// Represents the GetByIssueName method.
		/// </summary>
		GetByIssueName
	}
	
	#endregion EbiIssueSelectMethod

	#region EbiIssueFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueFilter : SqlFilter<EbiIssueColumn>
	{
	}
	
	#endregion EbiIssueFilter

	#region EbiIssueExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueExpressionBuilder : SqlExpressionBuilder<EbiIssueColumn>
	{
	}
	
	#endregion EbiIssueExpressionBuilder	

	#region EbiIssueProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;EbiIssueChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="EbiIssue"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiIssueProperty : ChildEntityProperty<EbiIssueChildEntityTypes>
	{
	}
	
	#endregion EbiIssueProperty
}

