﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using System.Data;

using DevExpress.Web.ASPxEditors;

public partial class UserControls_Other_help : System.Web.UI.UserControl
{
        Auth auth = new Auth();
        DataSet ds = null;

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                FileVaultTableListHelpFilesFilters fvtlhfFilters = new FileVaultTableListHelpFilesFilters();
                fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Everyone).ToString());

                switch (auth.RoleId)
                {
                    case (int)RoleList.Contractor:
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_Requal).ToString());
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_ViewEngineeringHours).ToString());
                        break;
                    case (int)RoleList.PreQual:
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_Prequal).ToString());
                        break;
                    case (int)RoleList.Reader:
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader).ToString());
                        //HS Assessor
                        if (Helper.General.isEhsConsultant(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor).ToString());
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                        }
                        //HS Assessor - Lead
                        Configuration configuration = new Configuration();
                        if (auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor_Lead).ToString());
                        }
                        //Procurement
                        if (Helper.General.isProcurementUser(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor).ToString());
                        }
                        //Reader - EBI Data Access
                        if (Helper.General.isEbiUser(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                        }
                        //Reader - Safety Qualification Read Access
                        if (Helper.General.isSQReadAccessUser(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_SafetyQualificationReadAccess).ToString());
                        }
                        //Reader - Financial Reporting Access
                        if (Helper.General.isPrivilegedUser(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_FinancialReportingAccess).ToString());
                        }
                        //Reader - Training Package Trainers
                        if (Helper.General.isPrivileged2User(auth.UserId))
                        {
                            fvtlhfFilters.Junction = "OR";
                            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_TrainingPackageTrainers).ToString());
                        }
                        break;
                    case (int)RoleList.Administrator:
                        //fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Administrator).ToString());

                        //show all for admins.
                        fvtlhfFilters.Clear();
                        break;
                    default:
                        break;
                }

                int count = 0;
                VList<FileVaultTableListHelpFiles> fvtlhfList = DataRepository.FileVaultTableListHelpFilesProvider.GetPaged(fvtlhfFilters.ToString(), "", 0, 999, out count);
                Session["dsHelpFiles"] = fvtlhfList.ToDataSet(false);
            }
            if (Session["dsHelpFiles"] != null)
            {
                ds = (DataSet)Session["dsHelpFiles"];
                gridHelp.DataSource = ds.Tables[0];
                gridHelp.DataBind();
                gridHelp.ExpandAll();
            }
            else
            {
                fillFileVaule();
                ds = (DataSet)Session["dsHelpFiles"];
                gridHelp.DataSource = ds.Tables[0];
                gridHelp.DataBind();
                gridHelp.ExpandAll();
            }

        }

        protected void fillFileVaule()
        {

            FileVaultTableListHelpFilesFilters fvtlhfFilters = new FileVaultTableListHelpFilesFilters();
            fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Everyone).ToString());

            switch (auth.RoleId)
            {
                case (int)RoleList.Contractor:
                    fvtlhfFilters.Junction = "OR";
                    fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_Requal).ToString());
                    fvtlhfFilters.Junction = "OR";
                    fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_ViewEngineeringHours).ToString());
                    break;
                case (int)RoleList.PreQual:
                    fvtlhfFilters.Junction = "OR";
                    fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Contractor_Prequal).ToString());
                    break;
                case (int)RoleList.Reader:
                    fvtlhfFilters.Junction = "OR";
                    fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader).ToString());
                    //HS Assessor
                    if (Helper.General.isEhsConsultant(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor).ToString());
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                    }
                    //HS Assessor - Lead
                    Configuration configuration = new Configuration();
                    if (auth.Email == configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor_Lead).ToString());
                    }
                    //Procurement
                    if (Helper.General.isProcurementUser(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_HsAssessor).ToString());
                    }
                    //Reader - EBI Data Access
                    if (Helper.General.isEbiUser(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Procurement).ToString());
                    }
                    //Reader - Safety Qualification Read Access
                    if (Helper.General.isSQReadAccessUser(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_SafetyQualificationReadAccess).ToString());
                    }
                    //Reader - Financial Reporting Access
                    if (Helper.General.isPrivilegedUser(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_FinancialReportingAccess).ToString());
                    }
                    //Reader - Training Package Trainers
                    if (Helper.General.isPrivileged2User(auth.UserId))
                    {
                        fvtlhfFilters.Junction = "OR";
                        fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Reader_TrainingPackageTrainers).ToString());
                    }
                    break;
                case (int)RoleList.Administrator:
                    //fvtlhfFilters.Append(FileVaultTableListHelpFilesColumn.FileVaultSubCategoryId, ((int)FileVaultSubCategoryList.HelpFiles_Administrator).ToString());

                    //show all for admins.
                    fvtlhfFilters.Clear();
                    break;
                default:
                    break;
            }

            int count = 0;
            VList<FileVaultTableListHelpFiles> fvtlhfList = DataRepository.FileVaultTableListHelpFilesProvider.GetPaged(fvtlhfFilters.ToString(), "", 0, 999, out count);
            Session["dsHelpFiles"] = fvtlhfList.ToDataSet(false);
        }



        protected void gridHelp_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

            ASPxHyperLink hlFile = gridHelp.FindRowCellTemplateControl(e.VisibleIndex, null, "hlFile") as ASPxHyperLink;
            if (hlFile != null)
            {
                hlFile.Text = (string)gridHelp.GetRowValues(e.VisibleIndex, "FileName");
                int FileVaultId = (int)gridHelp.GetRowValues(e.VisibleIndex, "FileVaultId");
                hlFile.NavigateUrl = String.Format("javascript:popUp2alt('PopUps/GetFile.aspx{0}');",
                    QueryStringModule.Encrypt("q=" + FileVaultId));

            }
        }
}
