﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IKpiAuditService
    {
        KpiAudit Get(Expression<Func<KpiAudit, bool>> where, List<Expression<Func<KpiAudit, object>>> includes);
        List<KpiAudit> GetMany(int? top, Expression<Func<KpiAudit, bool>> where, Func<IQueryable<KpiAudit>, IOrderedQueryable<KpiAudit>> orderBy, List<Expression<Func<KpiAudit, object>>> includes);
        List<KpiAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiAudit, bool>> where, Func<IQueryable<KpiAudit>, IOrderedQueryable<KpiAudit>> orderBy, List<Expression<Func<KpiAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<KpiAudit, bool>> where, List<Expression<Func<KpiAudit, object>>> includes);
        void Insert(KpiAudit item, bool saveChanges = true);
        void Update(KpiAudit item, bool saveChanges = true);
        void Delete(KpiAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class KpiAuditService : IKpiAuditService
    {
        private readonly IRepository<KpiAudit> _repository;

        public KpiAuditService(IRepository<KpiAudit> repository)
        {
            this._repository = repository;
        }

        public KpiAudit Get(Expression<Func<KpiAudit, bool>> where, List<Expression<Func<KpiAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<KpiAudit> GetMany(int? top, Expression<Func<KpiAudit, bool>> where, Func<IQueryable<KpiAudit>, IOrderedQueryable<KpiAudit>> orderBy, List<Expression<Func<KpiAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<KpiAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<KpiAudit, bool>> where, Func<IQueryable<KpiAudit>, IOrderedQueryable<KpiAudit>> orderBy, List<Expression<Func<KpiAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<KpiAudit, bool>> where, List<Expression<Func<KpiAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(KpiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(KpiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(KpiAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("KpiAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
