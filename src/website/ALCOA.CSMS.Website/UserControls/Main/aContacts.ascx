﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_aContacts" Codebehind="aContacts.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1" Namespace="DevExpress.Web.ASPxPopupControl"
    TagPrefix="dxpc" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3">
            <span class="bodycopy"><span class="title">Alcoa Contractor Management Contacts
                <asp:Button ID="btnEdit" runat="server" OnClick="btnEdit_Click" Text="(New/Edit/Delete)"
                    BackColor="White" BorderColor="White" BorderStyle="None" Font-Names="Verdana"
                    Font-Size="XX-Small" Font-Underline="True" ForeColor="Red" ToolTip="Click me to switch between modification and viewing modes of the contacts list." />
            </span>
                <br>
                <span class="date"><span style="color: navy">Your primary contacts within Alcoa for
                    Contractor Services Management.</span>
                    <br />
                    Please direct all General Enquiries to the team at the
                    <dxe:ASPxHyperLink ID="ASPxHyperLinkContact" runat="server" Text="AUA Alcoa Contractor Services">
                    </dxe:ASPxHyperLink>
                    mailbox. </span>
                <br />
                <img src="images/grfc_dottedline.gif" alt="..." width="24" height="1">
                <br>
            </span>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="text-align: right">
            <a href="javascript:ShowHideCustomizationWindow()"><span style="color: #0000ff; text-decoration: underline">Customize</span></a>
    </tr>
    <tr>
        <td class="pageName" colspan="3">
            <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                DataSourceID="ContactsAlcoaDataSource" KeyFieldName="ContactId" OnInitNewRow="grid_InitNewRow"
                OnRowUpdating="grid_RowUpdating" OnRowValidating="grid_RowValidating" OnStartRowEditing="grid_StartRowEditing"
                Width="100%">
                <Columns>
                    <dxwgv:GridViewCommandColumn Caption="Action" Name="commandCol" ShowInCustomizationForm="False"
                        Visible="False" VisibleIndex="0">
                        <EditButton Text="Edit" Visible="True">
                            <Image AlternateText="Edit" Url="~/Images/gridEdit.gif" />
                        </EditButton>
                        <NewButton Text="New" Visible="True">
                            <Image AlternateText="New" Url="~/Images/gridNew.gif" />
                        </NewButton>
                        <DeleteButton Text="Delete" Visible="True">
                            <Image AlternateText="Delete" Url="~/Images/gridDelete.gif" />
                        </DeleteButton>
                        <CancelButton Visible="True">
                        </CancelButton>
                    </dxwgv:GridViewCommandColumn>
                    <dxwgv:GridViewDataComboBoxColumn Name="SiteBox" Caption="Site" FieldName="SiteId"
                        GroupIndex="0" SortIndex="0" SortOrder="Ascending" UnboundType="String" VisibleIndex="0">
                        <PropertiesComboBox DataSourceID="SitesDataSource" DropDownHeight="150px" TextField="SiteName"
                            ValueField="SiteId" ValueType="System.Int32">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="First Name" FieldName="ContactFirstName" SortIndex="2"
                        SortOrder="Ascending" VisibleIndex="1">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Last Name" FieldName="ContactLastName" SortIndex="3"
                        SortOrder="Ascending" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Position/Role" FieldName="ContactTitlePosition"
                        SortIndex="1" SortOrder="Ascending" VisibleIndex="3">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataHyperLinkColumn Caption="Email" FieldName="ContactEmail" VisibleIndex="4">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="mailto:{0}" TextField="ContactEmail">
                        </PropertiesHyperLinkEdit>
                    </dxwgv:GridViewDataHyperLinkColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Phone No" FieldName="ContactMainNo" VisibleIndex="5"
                        Width="150px">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="6" Width="30px">
                        <ClearFilterButton Visible="True">
                        </ClearFilterButton>
                    </dxwgv:GridViewCommandColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
                <Settings ShowGroupPanel="True" ShowGroupedColumns="True" ShowVerticalScrollBar="True"
                    VerticalScrollableHeight="282" ShowHeaderFilterButton="True" />
                <SettingsBehavior ConfirmDelete="True" />
                <SettingsPager PageSize="100" Position="Top">
                    <AllButton Visible="True"></AllButton>
                </SettingsPager>
                <ClientSideEvents ContextMenu="function(s, e) { if(e.objectType == 'header') headerMenu.ShowAtPos(e.htmlEvent.x, e.htmlEvent.y); }"
                    SelectionChanged="function(s, e) { OnGridSelectionChanged(); }" />
                <SettingsCustomizationWindow Enabled="True" />
                <SettingsCookies CookiesID="aContacts" Version="0.1" />
            </dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="text-align: right">
            <a id="mailLink" href="javascript:window.alert('No Contacts Selected');">E-mail Selected</a>
            (<a id="selCount" type="text/plain">0</a>)
        </td>
    </tr>
    <tr align="right">
        <td colspan="3" style="height: 35px; text-align: right; text-align: -moz-right;"
            align="right">
            <div align="right">
                <uc1:ExportButtons ID="ucExportButtons" runat="server" />
            </div>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="3" style="text-align: right">
            <data:ContactsAlcoaDataSource ID="ContactsAlcoaDataSource" runat="server" SelectMethod="GetPaged"
                EnablePaging="True" EnableSorting="True">
            </data:ContactsAlcoaDataSource>
            <data:SitesDataSource ID="SitesDataSource" runat="server" EnableDeepLoad="False"
                EnableSorting="true" SelectMethod="GetAll">
            </data:SitesDataSource>
        </td>
    </tr>
</table>
