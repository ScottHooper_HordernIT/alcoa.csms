﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using System.IO;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class CsmsFileUpload : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

                lblFile.Visible = false;
                hlFile.Visible = false;
                if (Request.QueryString["id"] != null)
                {
                    int CsmsFileId = Convert.ToInt32(Request.QueryString["id"].ToString());

                    CsmsFileService cfService = new CsmsFileService();
                    CsmsFile cf = cfService.GetByCsmsFileId(CsmsFileId);
                    if (cf != null)
                    {
                        tbNewFileName.Text = cf.FileName;
                        tbNewFileDescription.Text = cf.Description;
                        cbNewFileType.Value = cf.CsmsFileTypeId;
                        lblFile.Visible = true;
                        hlFile.Visible = true;
                        hlFile.Text = cf.FileName;
                        btnUpload.Text = "Edit / Upload";
                        if (cf.IsVisible)
                        {
                            cbVisible.Checked = true;
                        }
                        else
                        {
                            cbVisible.Checked = false;
                        }
                        hlFile.NavigateUrl = String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=CSMSFiles&File=" + cf.CsmsFileId));
                    }
                }
                else
                {
                    cbVisible.Checked = true;
                }
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //new
            int count = 0;
            CsmsFileService cfService = new CsmsFileService();
            CsmsFileFilters cfFilters = new CsmsFileFilters();
            cfFilters.Append(CsmsFileColumn.FileName, tbNewFileName.Text);
            TList<CsmsFile> cfTlist = cfService.GetPaged(cfFilters.ToString(), "CsmsFileId ASC", 0, 1000, out count);


            Configuration configuration = new Configuration();
            string tempPath = @configuration.GetValue(ConfigList.TempDir);
            string fileName = "";

            if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                {
                    CsmsFile cf = new CsmsFile();

                    if (Request.QueryString["id"] != null)
                    {
                        int CsmsFileId = Convert.ToInt32(Request.QueryString["id"].ToString());

                        cf = cfService.GetByCsmsFileId(CsmsFileId);
                    }
                    else
                    {
                        cf = new CsmsFile();
                        cf.CreatedByUserId = auth.UserId;
                        cf.CreatedDate = DateTime.Now;

                        if (count > 0)
                        {
                            throw new Exception("File already exists with same filename specified.");
                        }
                    }


                    //1. Dump file into dropbox for virus scanning...
                    fileName = tempPath + ucNewFile.UploadedFiles[0].FileName;
                    ucNewFile.UploadedFiles[0].SaveAs(tempPath + ucNewFile.UploadedFiles[0].FileName, true);

                    //FileUpload1.Dispose();
                    //2. Upload into database
                    fileName = tempPath + ucNewFile.UploadedFiles[0].FileName;
                    FileStream fs = File.OpenRead(fileName);
                    int fileLength = Convert.ToInt32(fs.Length);
                    byte[] fileBytes = FileUtilities.Read.ReadFully(fs, 51200);
                    byte[] fileHash = FileUtilities.Calculate.HashFile(fileBytes);



                    cf.ModifiedByUserId = auth.UserId;
                    cf.ModifiedDate = DateTime.Now;

                    cf.FileName = tbNewFileName.Text;
                    cf.Description = tbNewFileDescription.Text;
                    cf.CsmsFileTypeId = (int)cbNewFileType.SelectedItem.Value;

                    cf.Content = FileUtilities.Read.ReadFully(fs, 51200);
                    cf.ContentLength = Convert.ToInt32(fs.Length);
                    cf.FileHash = FileUtilities.Calculate.HashFile(cf.Content);

                    if (cbVisible.Checked)
                    {
                        cf.IsVisible = true;
                    }
                    else
                    {
                        cf.IsVisible = false;
                    }

                    cfService.Save(cf);
                    Response.Redirect("~/admin.aspx?s=CsmsFiles", true);
                }
                else
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int CsmsFileId = Convert.ToInt32(Request.QueryString["id"].ToString());

                        CsmsFile cf = cfService.GetByCsmsFileId(CsmsFileId);

                        cf.ModifiedByUserId = auth.UserId;
                        cf.ModifiedDate = DateTime.Now;

                        cf.FileName = tbNewFileName.Text;
                        cf.Description = tbNewFileDescription.Text;
                        cf.CsmsFileTypeId = (int)cbNewFileType.SelectedItem.Value;

                        cfService.Save(cf);
                        Response.Redirect("~/admin.aspx?s=CsmsFiles", true);
                    }
                    else
                    {
                        throw new Exception("Please Complete Required Fields.");
                    }
                }
            }
            else
            {
                throw new Exception("Could not Parse Upload File");
            }
        }
    }
}