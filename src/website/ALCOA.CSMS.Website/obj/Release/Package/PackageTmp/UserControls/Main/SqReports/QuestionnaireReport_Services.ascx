<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Services" Codebehind="QuestionnaireReport_Services.ascx.cs" %>
<%@ Register Src="QuestionnaireReport_Services_Other.ascx" TagName="QuestionnaireReport_Services_Other"
    TagPrefix="uc2" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px"><a name="save"></a><span class="bodycopy"><span class="title">Reports</span><br />
                <span class="date">Safety Qualification &gt; Services</span><br />
                <img height="1" src="images/grfc_dottedline.gif" width="24" />&nbsp;</span>
        </td>
    </tr>
</table>
<br />
<dxtc:ASPxPageControl ID="Aspxpagecontrol3" runat="server" ActiveTabIndex="0" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    Width="900px">
    <TabPages>
        <dxtc:TabPage Text="Categories">
            <ContentCollection>
                <dxw:ContentControl runat="server">
                    <asp:PlaceHolder ID="phMain" runat="server"></asp:PlaceHolder>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
    </TabPages>
    <%--<LoadingPanelImage Url="~/App_Themes/Office2003BlueOld/Web/Loading.gif">--%>
    <LoadingPanelImage Url="~/App_Themes/Office2003BlueOld/Web/Loading.gif"> <%--Enhancement_023:change by debashis for testing platform upgradation--%>
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dxtc:ASPxPageControl>
<br />
<dxtc:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="0" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue">
    <TabPages>
        <dxtc:TabPage Text="Other">
            <ContentCollection>
                <dxw:ContentControl runat="server">
                        <asp:PlaceHolder ID="phOther" runat="server"></asp:PlaceHolder>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003BlueOld/Web/Loading.gif"> <%--Enhancement_023:change by debashis for testing platform upgradation--%>
    <%--<LoadingPanelImage Url="~/App_Themes/Office2003BlueOld/Web/Loading.gif">--%>
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dxtc:ASPxPageControl>
<br />
