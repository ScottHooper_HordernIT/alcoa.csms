﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IAdminTaskEmailTemplateRecipientService
    {
        AdminTaskEmailTemplateRecipient Get(Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes);
        List<AdminTaskEmailTemplateRecipient> GetMany(int? top, Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, Func<IQueryable<AdminTaskEmailTemplateRecipient>, IOrderedQueryable<AdminTaskEmailTemplateRecipient>> orderBy, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes);
        List<AdminTaskEmailTemplateRecipient> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, Func<IQueryable<AdminTaskEmailTemplateRecipient>, IOrderedQueryable<AdminTaskEmailTemplateRecipient>> orderBy, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes);
        void Insert(AdminTaskEmailTemplateRecipient item, bool saveChanges = true);
        void Update(AdminTaskEmailTemplateRecipient item, bool saveChanges = true);
        void Delete(AdminTaskEmailTemplateRecipient item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class AdminTaskEmailTemplateRecipientService : IAdminTaskEmailTemplateRecipientService
    {
        private readonly IRepository<AdminTaskEmailTemplateRecipient> _repository;

        public AdminTaskEmailTemplateRecipientService(IRepository<AdminTaskEmailTemplateRecipient> repository)
        {
            this._repository = repository;
        }

        public AdminTaskEmailTemplateRecipient Get(Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<AdminTaskEmailTemplateRecipient> GetMany(int? top, Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, Func<IQueryable<AdminTaskEmailTemplateRecipient>, IOrderedQueryable<AdminTaskEmailTemplateRecipient>> orderBy, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<AdminTaskEmailTemplateRecipient> GetManyPaged(int pageNumber, int pageSize, Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, Func<IQueryable<AdminTaskEmailTemplateRecipient>, IOrderedQueryable<AdminTaskEmailTemplateRecipient>> orderBy, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<AdminTaskEmailTemplateRecipient, bool>> where, List<Expression<Func<AdminTaskEmailTemplateRecipient, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(AdminTaskEmailTemplateRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateRecipient");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(AdminTaskEmailTemplateRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateRecipient");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(AdminTaskEmailTemplateRecipient item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("AdminTaskEmailTemplateRecipient");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
