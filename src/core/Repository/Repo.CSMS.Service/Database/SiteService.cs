﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISiteService
    {
        Site Get(Expression<Func<Site, bool>> where, List<Expression<Func<Site, object>>> includes);
        List<Site> GetMany(int? top, Expression<Func<Site, bool>> where, Func<IQueryable<Site>, IOrderedQueryable<Site>> orderBy, List<Expression<Func<Site, object>>> includes);
        List<Site> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Site, bool>> where, Func<IQueryable<Site>, IOrderedQueryable<Site>> orderBy, List<Expression<Func<Site, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Site, bool>> where, List<Expression<Func<Site, object>>> includes);
        void Insert(Site item, bool saveChanges = true);
        void Update(Site item, bool saveChanges = true);
        void Delete(Site item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SiteService : ISiteService
    {
        private readonly IRepository<Site> _repository;

        public SiteService(IRepository<Site> repository)
        {
            this._repository = repository;
        }

        public Site Get(Expression<Func<Site, bool>> where, List<Expression<Func<Site, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Site> GetMany(int? top, Expression<Func<Site, bool>> where, Func<IQueryable<Site>, IOrderedQueryable<Site>> orderBy, List<Expression<Func<Site, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Site> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Site, bool>> where, Func<IQueryable<Site>, IOrderedQueryable<Site>> orderBy, List<Expression<Func<Site, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Site, bool>> where, List<Expression<Func<Site, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Site item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Site");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Site item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Site");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Site item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Site");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
