﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportOverviewProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportOverviewDataSourceDesigner))]
	public class QuestionnaireReportOverviewDataSource : ReadOnlyDataSource<QuestionnaireReportOverview>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewDataSource class.
		/// </summary>
		public QuestionnaireReportOverviewDataSource() : base(new QuestionnaireReportOverviewService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportOverviewDataSourceView used by the QuestionnaireReportOverviewDataSource.
		/// </summary>
		protected QuestionnaireReportOverviewDataSourceView QuestionnaireReportOverviewView
		{
			get { return ( View as QuestionnaireReportOverviewDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireReportOverviewDataSource control invokes to retrieve data.
		/// </summary>
		public new QuestionnaireReportOverviewSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireReportOverviewSelectMethod selectMethod = QuestionnaireReportOverviewSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireReportOverviewSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportOverviewDataSourceView class that is to be
		/// used by the QuestionnaireReportOverviewDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportOverviewDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportOverview, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportOverviewDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportOverviewDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportOverviewDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportOverview>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportOverviewDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportOverviewDataSourceView(QuestionnaireReportOverviewDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportOverviewDataSource QuestionnaireReportOverviewOwner
		{
			get { return Owner as QuestionnaireReportOverviewDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal new QuestionnaireReportOverviewSelectMethod SelectMethod
		{
			get { return QuestionnaireReportOverviewOwner.SelectMethod; }
			set { QuestionnaireReportOverviewOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportOverviewService QuestionnaireReportOverviewProvider
		{
			get { return Provider as QuestionnaireReportOverviewService; }
		}

		#endregion Properties
		
		#region Methods
		
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
	    /// <param name="values"></param>
		/// <param name="count">The total number of rows in the DataSource.</param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireReportOverview> GetSelectData(IDictionary values, out int count)
		{	
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			
			IList<QuestionnaireReportOverview> results = null;
			// QuestionnaireReportOverview item;
			count = 0;
			
			System.String sp247_Type;
			System.Int32? sp247_SiteId;
			System.Int32? sp247_CompanyId;
			System.Int32? sp247_TypeOfServiceId;

			switch ( SelectMethod )
			{
				case QuestionnaireReportOverviewSelectMethod.Get:
					results = QuestionnaireReportOverviewProvider.Get(WhereClause, OrderBy, StartIndex, PageSize, out count);
                    break;
				case QuestionnaireReportOverviewSelectMethod.GetPaged:
					results = QuestionnaireReportOverviewProvider.GetPaged(WhereClause, OrderBy, StartIndex, PageSize, out count);
					break;
				case QuestionnaireReportOverviewSelectMethod.GetAll:
					results = QuestionnaireReportOverviewProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireReportOverviewSelectMethod.Find:
					results = QuestionnaireReportOverviewProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
                    break;
				// Custom
				case QuestionnaireReportOverviewSelectMethod.GetByTypeSiteCompanyService:
					sp247_Type = (System.String) EntityUtil.ChangeType(values["Type"], typeof(System.String));
					sp247_SiteId = (System.Int32?) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32?));
					sp247_CompanyId = (System.Int32?) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32?));
					sp247_TypeOfServiceId = (System.Int32?) EntityUtil.ChangeType(values["TypeOfServiceId"], typeof(System.Int32?));
					results = QuestionnaireReportOverviewProvider.GetByTypeSiteCompanyService(sp247_Type, sp247_SiteId, sp247_CompanyId, sp247_TypeOfServiceId, StartIndex, PageSize);
					break;
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;
				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}				
			}
			
			return results;
		}
		
		#endregion Methods
	}

	#region QuestionnaireReportOverviewSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireReportOverviewDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireReportOverviewSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByTypeSiteCompanyService method.
		/// </summary>
		GetByTypeSiteCompanyService
	}
	
	#endregion QuestionnaireReportOverviewSelectMethod
	
	#region QuestionnaireReportOverviewDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportOverviewDataSource class.
	/// </summary>
	public class QuestionnaireReportOverviewDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportOverview>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewDataSourceDesigner class.
		/// </summary>
		public QuestionnaireReportOverviewDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public new QuestionnaireReportOverviewSelectMethod SelectMethod
		{
			get { return ((QuestionnaireReportOverviewDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireReportOverviewDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireReportOverviewDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireReportOverviewDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireReportOverviewDataSourceActionList : DesignerActionList
	{
		private QuestionnaireReportOverviewDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireReportOverviewDataSourceActionList(QuestionnaireReportOverviewDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireReportOverviewSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireReportOverviewDataSourceActionList

	#endregion QuestionnaireReportOverviewDataSourceDesigner

	#region QuestionnaireReportOverviewFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewFilter : SqlFilter<QuestionnaireReportOverviewColumn>
	{
	}

	#endregion QuestionnaireReportOverviewFilter

	#region QuestionnaireReportOverviewExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverview"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportOverviewColumn>
	{
	}
	
	#endregion QuestionnaireReportOverviewExpressionBuilder		
}

