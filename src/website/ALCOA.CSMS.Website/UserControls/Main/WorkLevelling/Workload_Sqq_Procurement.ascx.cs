﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;

public class dsQuestionnaire_GetProcurementWorkLoad
{
    public dsQuestionnaire_GetProcurementWorkLoad()
    {
        
    }
}
public partial class UserControls_Tiny_Workload_Sqq : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            cmbFunctionalProcurementManager.DataSourceID = "dsQuestionnaire_GetAssignedFunctionalProcurementManagers";
            cmbFunctionalProcurementManager.TextField = "UserFullName";
            cmbFunctionalProcurementManager.ValueField = "UserId";
            cmbFunctionalProcurementManager.DataBind();
            cmbFunctionalProcurementManager.Items.Add("-----------------------------", "0");
            cmbFunctionalProcurementManager.Items.Add("All", "-1");
            cmbFunctionalProcurementManager.Value = "-1";
            cmbFunctionalProcurementManager.Text = "All";


            SessionHandler.spVar_Year = "%";

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    //full access
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            // Export
            String exportFileName = @"ALCOA CSMS - Work Levelling Tool - Safety Qualification Questionnaires - Procurement"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, "grid2");

            grid2.FilterExpression = "[CompanyStatusName] not like 'InActive%'";
        }
    }

    protected void cmbFunctionalProcurementManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbFunctionalProcurementManager.SelectedItem.Value != null)
        {
            if (!String.IsNullOrEmpty(cmbFunctionalProcurementManager.SelectedItem.Value.ToString()))
            {
                Session["FunctionalProcurementManagerUserId"] = cmbFunctionalProcurementManager.SelectedItem.Value.ToString();
                LoadData2();
            }
        }
    }

    protected void LoadData2()
    {
        dsQuestionnaire_GetProcurementWorkLoad.DataBind();
        grid2.DataBind();
    }

}