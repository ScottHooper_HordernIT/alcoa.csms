//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class QuestionnaireVerificationSection
    {
        public int SectionResponseId { get; set; }
        public int QuestionnaireId { get; set; }
        public Nullable<bool> Section1_Complete { get; set; }
        public Nullable<bool> Section2_Complete { get; set; }
        public Nullable<bool> Section3_Complete { get; set; }
        public Nullable<bool> Section4_Complete { get; set; }
        public Nullable<bool> Section5_Complete { get; set; }
        public Nullable<bool> Section6_Complete { get; set; }
        public Nullable<bool> Section7_Complete { get; set; }
        public Nullable<bool> Section8_Complete { get; set; }
    
        public virtual Questionnaire Questionnaire { get; set; }
    }
}
