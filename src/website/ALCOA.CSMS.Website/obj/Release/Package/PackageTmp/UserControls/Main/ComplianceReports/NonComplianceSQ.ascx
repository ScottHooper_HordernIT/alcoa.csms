<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_NonComplianceSQ" Codebehind="NonComplianceSQ.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
    <%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="../../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<div class="pageName" style="width: 900px">
    <span class="bodycopy"><span class="title">Compliance Report</span><br />
    <span class="date">Safety Qualification Report</span><br />
    <img src="images/grfc_dottedline.gif" width="24" height="1"></span><br />
    <br />

    <table style="width: 900px">
        <tr>
            <td align="right" style="width: 900px; height: 38px">
                <button name="btnPrint" onclick="javascript:print();" style="font-weight: bold; width: 80px;
                    height: 25px" type="button">
                    Print</button>
            </td>
        </tr>
    </table>
            
    <dxwgv:ASPxGridView ID="grid"
        ClientInstanceName="grid"
        runat="server"
        AutoGenerateColumns="False"
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue"
        DataSourceID="sqldsNonComplianceReportYearCSA"
        Width="900px"
        OnHtmlRowCreated="grid_RowCreated" KeyFieldName="CompanyId"
        OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
        OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate"
        OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter"
        OnCustomColumnSort="grid_CustomColumnSort"
        Settings-ShowFilterBar="Visible"
     >
    <Columns>
     
     
         <dxwgv:GridViewDataComboBoxColumn FieldName="CompanyId" Caption="Company" 
                    ReadOnly="True" Width="250px"
             VisibleIndex="0" SortIndex="1" SortOrder="Ascending">
             <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith"></PropertiesComboBox>
             <EditFormSettings Visible="False" />
             <Settings SortMode="DisplayText" />
         </dxwgv:GridViewDataComboBoxColumn>
    
        <dxwgv:GridViewDataTextColumn FieldName="Type" ReadOnly="True"
             VisibleIndex="1">
         </dxwgv:GridViewDataTextColumn>
     
        
         <dxwgv:GridViewDataTextColumn FieldName="QuestionnaireId" ReadOnly="True" Width="100px"
             VisibleIndex="1" Visible="False" Caption="Submitted">
            <DataItemTemplate>
                         <table cellpadding="0" cellspacing="0"><tr>
                         <td><asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/redcross.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                         </tr></table>
            </DataItemTemplate>
            <CellStyle HorizontalAlign="Center" />
         </dxwgv:GridViewDataTextColumn>   
     
       
         <dxwgv:GridViewDataComboBoxColumn FieldName="Status" ReadOnly="True"
             VisibleIndex="2">
             <PropertiesComboBox DataSourceID="QuestionnaireStatusDataSource" DropDownHeight="150px"
                    TextField="QuestionnaireStatusDesc" ValueField="QuestionnaireStatusId" ValueType="System.String">
                </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
         </dxwgv:GridViewDataComboBoxColumn>
     
        <dxwgv:GridViewDataTextColumn FieldName="Recommended" ReadOnly="True" Width="100px"
            VisibleIndex="3">
            <DataItemTemplate>
                         <table cellpadding="0" cellspacing="0"><tr>
                         <td><asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/clear.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                         </tr></table>
            </DataItemTemplate>
            <CellStyle HorizontalAlign="Center" />
         </dxwgv:GridViewDataTextColumn>   
            
        <dxwgv:GridViewDataTextColumn FieldName="ExpiresIn" Caption="Expires in" ToolTip="Days Left Till Expiry" VisibleIndex="4" SortIndex="0" SortOrder="Ascending">
            <Settings SortMode="Custom" FilterMode="Value"></Settings>
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>     
                         <td style="width:120px"><asp:Label ID="lblExpiresIn" runat="server" Text="" ></asp:Label></td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
            <CellStyle HorizontalAlign="Center"></CellStyle>
        </dxwgv:GridViewDataTextColumn>
            
    </Columns>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Settings ShowGroupedColumns="True" ShowFilterRow="True" ShowGroupPanel="True" />
    <SettingsPager Visible="true" PageSize="20" Mode="ShowPager">
        <AllButton Visible="true">
        </AllButton>
    </SettingsPager>
    <SettingsCustomizationWindow Enabled="True" />
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <AlternatingRow Enabled="True">
        </AlternatingRow>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn"/>
    <SettingsCookies CookiesID="NonComplianceCSA" Version="0.1" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
    </dxwgv:ASPxGridView>        
</div>

<table width="900px">
<tr align="right">
        <td align="right" class="pageName" style="height: 30px; text-align: right; text-align:-moz-right; width: 900px;">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
</tr>
</table>


<asp:SqlDataSource ID="sqldsNonComplianceReportYearCSA" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_NonComplianceReportYearSafetyQualification" 
    SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<data:QuestionnaireStatusDataSource ID="QuestionnaireStatusDataSource" runat="server">
    <DeepLoadProperties Method="IncludeChildren" Recursive="False">
    </DeepLoadProperties>
</data:QuestionnaireStatusDataSource>
