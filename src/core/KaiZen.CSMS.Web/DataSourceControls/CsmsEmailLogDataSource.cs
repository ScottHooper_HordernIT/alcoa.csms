﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsmsEmailLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsmsEmailLogDataSourceDesigner))]
	public class CsmsEmailLogDataSource : ProviderDataSource<CsmsEmailLog, CsmsEmailLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogDataSource class.
		/// </summary>
		public CsmsEmailLogDataSource() : base(new CsmsEmailLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsmsEmailLogDataSourceView used by the CsmsEmailLogDataSource.
		/// </summary>
		protected CsmsEmailLogDataSourceView CsmsEmailLogView
		{
			get { return ( View as CsmsEmailLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsmsEmailLogDataSource control invokes to retrieve data.
		/// </summary>
		public CsmsEmailLogSelectMethod SelectMethod
		{
			get
			{
				CsmsEmailLogSelectMethod selectMethod = CsmsEmailLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsmsEmailLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsmsEmailLogDataSourceView class that is to be
		/// used by the CsmsEmailLogDataSource.
		/// </summary>
		/// <returns>An instance of the CsmsEmailLogDataSourceView class.</returns>
		protected override BaseDataSourceView<CsmsEmailLog, CsmsEmailLogKey> GetNewDataSourceView()
		{
			return new CsmsEmailLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsmsEmailLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsmsEmailLogDataSourceView : ProviderDataSourceView<CsmsEmailLog, CsmsEmailLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsmsEmailLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsmsEmailLogDataSourceView(CsmsEmailLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsmsEmailLogDataSource CsmsEmailLogOwner
		{
			get { return Owner as CsmsEmailLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsmsEmailLogSelectMethod SelectMethod
		{
			get { return CsmsEmailLogOwner.SelectMethod; }
			set { CsmsEmailLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsmsEmailLogService CsmsEmailLogProvider
		{
			get { return Provider as CsmsEmailLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsmsEmailLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsmsEmailLog> results = null;
			CsmsEmailLog item;
			count = 0;
			
			System.Int32 _csmsEmailLogId;

			switch ( SelectMethod )
			{
				case CsmsEmailLogSelectMethod.Get:
					CsmsEmailLogKey entityKey  = new CsmsEmailLogKey();
					entityKey.Load(values);
					item = CsmsEmailLogProvider.Get(entityKey);
					results = new TList<CsmsEmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsmsEmailLogSelectMethod.GetAll:
                    results = CsmsEmailLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsmsEmailLogSelectMethod.GetPaged:
					results = CsmsEmailLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsmsEmailLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsmsEmailLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsmsEmailLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsmsEmailLogSelectMethod.GetByCsmsEmailLogId:
					_csmsEmailLogId = ( values["CsmsEmailLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsmsEmailLogId"], typeof(System.Int32)) : (int)0;
					item = CsmsEmailLogProvider.GetByCsmsEmailLogId(_csmsEmailLogId);
					results = new TList<CsmsEmailLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsmsEmailLogSelectMethod.Get || SelectMethod == CsmsEmailLogSelectMethod.GetByCsmsEmailLogId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsmsEmailLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsmsEmailLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsmsEmailLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsmsEmailLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsmsEmailLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsmsEmailLogDataSource class.
	/// </summary>
	public class CsmsEmailLogDataSourceDesigner : ProviderDataSourceDesigner<CsmsEmailLog, CsmsEmailLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogDataSourceDesigner class.
		/// </summary>
		public CsmsEmailLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailLogSelectMethod SelectMethod
		{
			get { return ((CsmsEmailLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsmsEmailLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsmsEmailLogDataSourceActionList

	/// <summary>
	/// Supports the CsmsEmailLogDataSourceDesigner class.
	/// </summary>
	internal class CsmsEmailLogDataSourceActionList : DesignerActionList
	{
		private CsmsEmailLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsmsEmailLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsmsEmailLogDataSourceActionList(CsmsEmailLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsmsEmailLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsmsEmailLogDataSourceActionList
	
	#endregion CsmsEmailLogDataSourceDesigner
	
	#region CsmsEmailLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsmsEmailLogDataSource.SelectMethod property.
	/// </summary>
	public enum CsmsEmailLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsmsEmailLogId method.
		/// </summary>
		GetByCsmsEmailLogId
	}
	
	#endregion CsmsEmailLogSelectMethod

	#region CsmsEmailLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogFilter : SqlFilter<CsmsEmailLogColumn>
	{
	}
	
	#endregion CsmsEmailLogFilter

	#region CsmsEmailLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogExpressionBuilder : SqlExpressionBuilder<CsmsEmailLogColumn>
	{
	}
	
	#endregion CsmsEmailLogExpressionBuilder	

	#region CsmsEmailLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsmsEmailLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsmsEmailLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsmsEmailLogProperty : ChildEntityProperty<CsmsEmailLogChildEntityTypes>
	{
	}
	
	#endregion CsmsEmailLogProperty
}

