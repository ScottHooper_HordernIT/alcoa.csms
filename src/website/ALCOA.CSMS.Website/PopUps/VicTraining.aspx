﻿<%@ Page Title="Victorian Operations Training" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_VicTraining" Codebehind="VicTraining.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" language="javascript">  
         function doClose(e) // note: takes the event as an arg (IE doesn't)  
         {  
             if (!e) e = window.event; // fix IE  

             if (e.keyCode) // IE  
             {  
                 if (e.keyCode == "27") window.close();  
             }  
             else if (e.charCode) // Netscape/Firefox/Opera  
             {  
                 if (e.keyCode == "27") window.close();  
             } 
         }
         document.onkeydown = doClose;  
 </script> 
    <div align="center">
        <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" 
            Text="Close (ESC)" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        </dx:ASPxButton>
    </div>
    <table border="0" cellpadding="2" cellspacing="0" width="100%" id="TABLE1">
        <tr>
            <td class="pageName" colspan="3" style="height: 17px; text-align: left;">
                <p class="MsoNormal">
                    <b><span style="font-size: 14pt; color: #002060">Alcoa World Alumina - Victorian Operations</span></b><span
                        style="color: #002060; font-family: 'Arial','sans-serif'"><span></p>
                <p class="MsoNormal">
                    <b><span style="font-size: 14pt; color: #002060"></span></b><b><span style="font-size: 12pt;
                        color: #002060">CONTRACTOR TRAINING</span></b><span style="color: #002060; font-family: 'Arial','sans-serif'"><span></p>
                <p class="MsoNormal">
                    <b><span style="color: #002060"></span></b><span style="color: #002060; font-family: 'Arial','sans-serif'">
                        <span><span style="color: #002060; font-family: 'Arial','sans-serif'; font-size: 12pt;">
                            It is the responsibility of any new or short duration contractor organisations to
                            refer to their Contract to confirm training requirements prior to commencement of
                            any work on Alcoa sites. </span></span></span>
                </p>
                <p class="MsoNormal">
                    <span style="color: #002060; font-family: 'Arial','sans-serif'"><span><span style="color: #002060;
                        font-family: 'Arial','sans-serif'"></span></span></span><span style="color: #002060;
                            font-family: 'Arial','sans-serif'; font-size: 12pt;">The ongoing training requirements
                            for embedded/on-site contractors (also agreed with their Contract Sponsor) are detailed
                            in our Learning Management System (LMS) and the status of key training can be identified
                            using the Victorian Operations Training Dashboard.</span></p>
                <p class="MsoNormal">
                    &nbsp;</p>
                <p class="MsoNormal">
                    <span style="font-size: 12pt">&nbsp; </span><a href="http://dashman.aua.alcoa.com/default.aspx?projectid=8"
                        target="_blank"><span style="font-size: 13pt">Click here to access the Victorian Operations
                            Training Dashboard</span></a><span style="font-size: 13pt"> &nbsp;</span></p>
                <p class="MsoNormal">
                    <span style="font-size: 12pt">&nbsp; <span style="color: red">(nb. This will only work
                        when logged onto an Alcoa PC on the Alcoa network!)<?xml namespace="" prefix="o" ?><o:p></o:p></span></span></p>
            </td>
        </tr>
        <tr>
        </tr>
    </table>
</asp:Content>
