﻿using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace Repo.CSMS.Service.Database.View
{
    #region Interface

    public interface IvwCompanyContractorReviewDetailService
    {
        vwCompanyContractorReviewDetail Get(Expression<Func<vwCompanyContractorReviewDetail, bool>> where, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes);
        List<vwCompanyContractorReviewDetail> GetMany(int? top, Expression<Func<vwCompanyContractorReviewDetail, bool>> where, Func<IQueryable<vwCompanyContractorReviewDetail>, IOrderedQueryable<vwCompanyContractorReviewDetail>> orderBy, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes);
        List<vwCompanyContractorReviewDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<vwCompanyContractorReviewDetail, bool>> where, Func<IQueryable<vwCompanyContractorReviewDetail>, IOrderedQueryable<vwCompanyContractorReviewDetail>> orderBy, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<vwCompanyContractorReviewDetail, bool>> where, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes);
    }
    #endregion

    #region Interface Implementation

    public class vwCompanyContractorReviewDetailService : IvwCompanyContractorReviewDetailService
    {
        private readonly IRepository<vwCompanyContractorReviewDetail> _repository;

        public vwCompanyContractorReviewDetailService(IRepository<vwCompanyContractorReviewDetail> repository)
        {
            this._repository = repository;
        }

        public vwCompanyContractorReviewDetail Get(Expression<Func<vwCompanyContractorReviewDetail, bool>> where, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<vwCompanyContractorReviewDetail> GetMany(int? top, Expression<Func<vwCompanyContractorReviewDetail, bool>> where, Func<IQueryable<vwCompanyContractorReviewDetail>, IOrderedQueryable<vwCompanyContractorReviewDetail>> orderBy, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<vwCompanyContractorReviewDetail> GetManyPaged(int pageNumber, int pageSize, Expression<Func<vwCompanyContractorReviewDetail, bool>> where, Func<IQueryable<vwCompanyContractorReviewDetail>, IOrderedQueryable<vwCompanyContractorReviewDetail>> orderBy, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<vwCompanyContractorReviewDetail, bool>> where, List<Expression<Func<vwCompanyContractorReviewDetail, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }
    }

    #endregion
}
