using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;

using System.Drawing;

using DevExpress.Web.ASPxGridView;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using repo = Repo.CSMS.DAL.EntityModels;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

public partial class UserControls_Tiny_QuestionnaireReports_QuestionnaireReport_Overview : System.Web.UI.UserControl
{
    Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService companySiteCategoryExceptionService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryExceptionService>();
    Repo.CSMS.Service.Database.ISiteService siteService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ISiteService>();
    Repo.CSMS.Service.Database.View.IQuestionnaireReportOverviewService questionnaireReportOverviewService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.View.IQuestionnaireReportOverviewService>();
    Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService companySiteCategoryStandardService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanySiteCategoryStandardService>();
    Repo.CSMS.Service.Database.IRegionsSiteService regionsSiteService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IRegionsSiteService>();
    Repo.CSMS.Service.Database.ICompanyService companyService = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.ICompanyService>();
    const int ShowAllFilterId = -999;

    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    int i = 4;

    private List<repo.QuestionnaireReportOverview> checkForExceptions(List<repo.QuestionnaireReportOverview> reportOverviewList, DateTime startOfCurrentMonth)
    {
        List<repo.CompanySiteCategoryException> csceList = companySiteCategoryExceptionService.GetMany(null, i => startOfCurrentMonth >= i.MonthYearFrom && startOfCurrentMonth <= i.MonthYearTo, null, null);
        int qroListIndex;
        repo.QuestionnaireReportOverview qro = new repo.QuestionnaireReportOverview();
        repo.Site site = new repo.Site();
        string siteAbbrev;
        List<repo.Site> sites = siteService.GetMany(null, null, null, null);
        foreach (repo.CompanySiteCategoryException csce in csceList)
        {
            qroListIndex = reportOverviewList.FindIndex(x => x.CompanyId == csce.CompanyId);
            siteAbbrev = sites.Find(s => s.SiteId == csce.SiteId).SiteAbbrev;
            repo.QuestionnaireReportOverview tmp = new repo.QuestionnaireReportOverview();
            //Type type = qroList[qroListIndex].GetType();
            Type type = tmp.GetType();
            string fldName = siteAbbrev + "_CompanySiteCategory";
            PropertyInfo po = type.GetProperty(fldName);
            if (po != null && qroListIndex != -1)
                po.SetValue(reportOverviewList[qroListIndex], csce.CompanySiteCategory.CategoryDesc, null);
        }
        return reportOverviewList;
    }

    private void moduleInit(bool postBack)
    {
        ////   09-04-2009 Commented out, as Contractors are not allowed to see this now (Controlled by Header.ascx.cs)
        ////   
        //if (auth.RoleId == (int)RoleList.Contractor)
        //{
        //    QuestionnaireReportOverviewDataSource.Filter = "CompanyId = " + auth.CompanyId.ToString();
        //    grid.Columns["Company"].Visible = false;
        //    QuestionnaireReportOverviewDataSource.DataBind();
        //}

        SessionHandler.spVar_Questionnaire_FrontPageLink = "QuestionnaireReport_Overview";
        
        if (!postBack)
        {
            cbFilterType.DataSourceID = "FilterTypeDS";
            cbFilterType.TextField = "Type";
            cbFilterType.ValueField = "Type";
            cbFilterType.DataBind();
            cbFilterType.Items.Add("-----------------------------", "0");
            cbFilterType.Items.Add("All", "-1");
            cbFilterType.Value = "Contractor";
            cbFilterType.Text = "Contractor";

            int _i = 0;
            cbFilterSite.Items.Clear();
            SitesFilters sitesFilters = new SitesFilters();
            sitesFilters.Append(SitesColumn.IsVisible, "1");
            TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
            foreach (Sites s in sitesList)
            {
                cbFilterSite.Items.Add(s.SiteName, s.SiteId);
            }
            cbFilterSite.Items.Add("----------------", 0);

            RegionsFilters regionsFilters = new RegionsFilters();
            regionsFilters.Append(RegionsColumn.IsVisible, "1");
            TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
            foreach (Regions r in regionsList)
            {
                if (r.RegionInternalName == "AU" || r.RegionInternalName == "WAO" || r.RegionInternalName == "VICOPS")
                {
                    //if (r.RegionInternalName != "AU")
                    cbFilterSite.Items.Add(r.RegionName, r.RegionValue);
                }
            }
            cbFilterSite.Text = "Australia";

            cbFilterCompany.DataSourceID = "QuestionnaireReportOverviewDataSource_FilterCompanies";
            cbFilterCompany.TextField = "CompanyName";
            cbFilterCompany.ValueField = "CompanyId";
            cbFilterCompany.DataBind();
            cbFilterCompany.Items.Add("-----------------------------", 0);
            cbFilterCompany.Items.Add("All", -1);
            cbFilterCompany.SelectedIndex = -1;
            cbFilterCompany.Text = "All";

            cbFilterABN.DataSourceID = "QuestionnaireReportOverviewDataSource_FilterABN";
            cbFilterABN.TextField = "CompanyAbn";
            cbFilterABN.ValueField = "CompanyAbn";
            cbFilterABN.DataBind();
            cbFilterABN.Items.Add("-----------------------------", 0);
            cbFilterABN.Items.Add("All", -1);
            cbFilterABN.SelectedIndex = -1;
            cbFilterABN.Text = "All";

            cbFilterService.DataSourceID = "QuestionnaireReportOverviewDataSource_FilterServices";
            cbFilterService.TextField = "CategoryText";
            cbFilterService.ValueField = "CategoryId";
            cbFilterService.DataBind();
            cbFilterService.Items.Add("-----------------------------", 0);
            cbFilterService.Items.Add("All", -1);
            cbFilterService.Items.Add("(Empty)", null);
            cbFilterService.SelectedIndex = -1;
            cbFilterService.Text = "All";

            //Default Datasource - Direct-Australia-All-All

            int count = 0;
            //QuestionnaireReportOverviewService qroService = new QuestionnaireReportOverviewService();
            //QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();
            //qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.Type, cbFilterType.Text);
            //VList<QuestionnaireReportOverview> qroVlist = DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 999999999, out count);
            int compId = convertObjToInt32(cbFilterCompany.SelectedItem.Value);
            string atype = (string)cbFilterType.SelectedItem.Value;
            string compAbn = (string)cbFilterABN.SelectedItem.Value;

            List<repo.QuestionnaireReportOverview> qroList = questionnaireReportOverviewService.GetMany(compId, atype, -1, compAbn);
            DateTime startOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month,1);
            //List<repo.CompanySiteCategoryException> csceList = companySiteCategoryExceptionService.GetMany(null, i => startOfCurrentMonth >= i.MonthYearFrom && startOfCurrentMonth <= i.MonthYearTo, null, null);

            //int qroListIndex;
            //repo.QuestionnaireReportOverview qro = new repo.QuestionnaireReportOverview();
            //repo.Site site = new repo.Site();
            //string siteAbbrev;
            //List<repo.Site> sites = siteService.GetMany(null, null, null, null);
            //foreach (repo.CompanySiteCategoryException csce in csceList)
            //{
            //    qroListIndex = qroList.FindIndex(x => x.CompanyId == csce.CompanyId);
            //    siteAbbrev = sites.Find(s => s.SiteId == csce.SiteId).SiteAbbrev;
            //    repo.QuestionnaireReportOverview tmp = new repo.QuestionnaireReportOverview();
            //    //Type type = qroList[qroListIndex].GetType();
            //    Type type = tmp.GetType();
            //    string fldName = siteAbbrev +"_CompanySiteCategory";
            //    PropertyInfo po = type.GetProperty(fldName);
            //    if (po != null && qroListIndex != -1)
            //        po.SetValue(qroList[qroListIndex], csce.CompanySiteCategory.CategoryDesc, null);
            //}

            Session["vlQuestionnaireReportOverview"] = checkForExceptions(qroList,startOfCurrentMonth);
            Session["QuestionnaireReportOverview_ColSite"] = "";

            // Export
            String exportFileName = @"ALCOA CSMS - Safety Qualification Report - Qualified Contractors"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true, true);

        }
        else
        {
        }

        //VList<QuestionnaireReportOverview> qroVlist2 = (VList<QuestionnaireReportOverview>)Session["vlQuestionnaireReportOverview"];
        //DataSet ds = qroVlist2.ToDataSet(false);
        //grid.DataSource = ds.Tables[0];
        List<repo.QuestionnaireReportOverview> qroList2 = (List<repo.QuestionnaireReportOverview>)Session["vlQuestionnaireReportOverview"];
        grid.DataSource = qroList2;
        grid.DataBind();

        if (auth.RoleId == (int)RoleList.Reader)
        {
            if (!Helper.General.isEhsConsultant(auth.UserId) && !Helper.General.isPrivilegedUser(auth.UserId) && !Helper.General.isProcurementUser(auth.UserId))
            {
                if (Helper.General.isSQReadAccessUser(auth.UserId)) grid.Columns["QuestionnaireId"].Visible = false;
            }
        }


        if (!Page.IsPostBack)
        {
            ShowHideCols();
        }
    }

    protected void ShowHideCols()
    {
        //VList<QuestionnaireReportOverview> qroVlist2 = (VList<QuestionnaireReportOverview>)Session["vlQuestionnaireReportOverview"];
        List<repo.QuestionnaireReportOverview> qroList2 = (List<repo.QuestionnaireReportOverview>)Session["vlQuestionnaireReportOverview"];

        //gridColShow("Type", false, true);
        //gridColShow("FinalRiskRating", false, true);
        gridColShow("KWI", false);
        gridColShow("PIN", false);
        gridColShow("WGP", false);
        gridColShow("HUN", false);
        gridColShow("WDL", false);
        gridColShow("BUN", false);
        gridColShow("FML", false);
        gridColShow("BGN", false);
        gridColShow("CE", false);
        gridColShow("ANG", false);
        gridColShow("PTL", false);
        gridColShow("PTH", false);
        gridColShow("PEEL", false);
        gridColShow("PTH (ARP)", false);
        gridColShow("YEN (ARP)", false);
        //gridColShow("DescriptionOfWork", false, true);
        //gridColShow("LevelOfSupervision", false, true);
        //gridColShow("ProcurementContact", false, true);
        //gridColShow("SafetyAssessor", false, true);
        //gridColShow("ContractManager", false, true);
        //gridColShow("TypeOfService", false, true);
        //gridColShow("MainAssessmentValidTo", false, true);
        //gridColShow("CompanyAbn", false, true);

        //gridColShow("Type", true, true);
        //gridColShow("FinalRiskRating", true, true);


        string ColSite = "";
        string Region = "";

        if (Session["QuestionnaireReportOverview_ColSite"] != null)
        {
            ColSite = Session["QuestionnaireReportOverview_ColSite"].ToString();
        }

        if (Session["QuestionnaireReportOverview_Region"] != null)
        {
            Region = Session["QuestionnaireReportOverview_Region"].ToString();
        }

        if (!String.IsNullOrEmpty(Region))
        {
            bool WAO = false;
            bool VICOPS = false;

            if (Region == "AU")
            {
                WAO = true;
                VICOPS = true;
            }
            if (Region == "WAO") WAO = true;
            if (Region == "VICOPS") VICOPS = true;

            foreach (repo.QuestionnaireReportOverview qro in qroList2)
            {
                if (WAO) gridColShow("KWI", true);
                if (WAO) gridColShow("PIN", true);
                if (WAO) gridColShow("WGP", true);
                if (WAO) gridColShow("HUN", true);
                if (WAO) gridColShow("WDL", true);
                if (WAO) gridColShow("BUN", true);
                if (WAO) gridColShow("FML", true);
                if (WAO) gridColShow("BGN", true);
                if (VICOPS) gridColShow("CE", true);
                if (VICOPS) gridColShow("ANG", true);
                if (VICOPS) gridColShow("PTL", true);
                if (VICOPS) gridColShow("PTH", true);
                if (VICOPS) gridColShow("PEEL", true);
                if (VICOPS) gridColShow("PTH (ARP)", true);
                if (VICOPS) gridColShow("YEN (ARP)", true);
            }
        }
        else
        {
            if (String.IsNullOrEmpty(ColSite))
            {
                foreach (repo.QuestionnaireReportOverview qro in qroList2)
                {
                    //if (qro.Kwi != "-") { gridColShow("KWI", true); gridColShow("KWI Sponsor", true); };
                    if (qro.KWI != "-") gridColShow("KWI", true);
                    if (qro.PIN != "-") gridColShow("PIN", true);
                    if (qro.WGP != "-") gridColShow("WGP", true);
                    if (qro.HUN != "-") gridColShow("HUN", true);
                    if (qro.WDL != "-") gridColShow("WDL", true);
                    if (qro.BUN != "-") gridColShow("BUN", true);
                    if (qro.FML != "-") gridColShow("FML", true);
                    if (qro.BGN != "-") gridColShow("BGN", true);
                    if (qro.CE != "-") gridColShow("CE", true);
                    if (qro.ANG != "-") gridColShow("ANG", true);
                    if (qro.PTD != "-") gridColShow("PTL", true);
                    if (qro.PTH != "-") gridColShow("PTH", true);
                    if (qro.PNJ != "-") gridColShow("PEEL", true);
                    if (qro.KPH != "-") gridColShow("PTH (ARP)", true);
                    if (qro.YEN != "-") gridColShow("YEN (ARP)", true);

                }
            }
            else
            {
                gridColShow(ColSite, true);
            }
        }

        gridColShow("DescriptionOfWork", true, true);
        gridColShow("LevelOfSupervision", true, true);
        gridColShow("ProcurementContactUser", true, true);
        gridColShow("SafetyAssessor", true, true);
        gridColShow("ContractManagerUser", true, true);
        gridColShow("TypeOfService", true, true);
        gridColShow("MainAssessmentValidTo", true, true);
        gridColShow("CompanyAbn", true, true);
        gridColShow("CompanyStatusDesc", true, true);
        gridColShow("IsMainRequired", true, true);
        gridColShow("Validity", true, true);

        btnFilter.Enabled = true;
    }
    protected static void approvalText(Label l, object _i)
    {
        try
        {
            if (_i != null)
            {
                string i = _i.ToString();
                l.Font.Bold = true;
                switch (i)
                {
                    case "Tentative":
                        l.Text = "Tentative";
                        l.ForeColor = Color.Black;
                        break;
                    case "No":
                        l.Text = "No";
                        l.ForeColor = Color.Red;
                        break;
                    case "Yes":
                        l.Text = "Yes";
                        l.ForeColor = Color.Green;
                        break;
                    default:
                        l.Text = "";
                        l.ForeColor = Color.Black;
                        break;
                }
            }
        }
        catch (Exception)
        {
            try
            {
                l.Text = "";
                l.ForeColor = Color.Black;
            }
            catch (Exception)
            {
                Elmah.ErrorSignal.FromCurrentContext();
            }
        }

    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        //Label lblColumnCompanyName = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblColumnCompanyName") as Label;
        //lblColumnCompanyName.Text = (string)grid.GetRowValues(e.VisibleIndex, "CompanyName");

        System.Web.UI.WebControls.Image imgColumnCompanyTrafficLight = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "imgColumnCompanyTrafficLight") as System.Web.UI.WebControls.Image;

        string TrafficLight = (string)grid.GetRowValues(e.VisibleIndex, "TrafficLight");
        if (imgColumnCompanyTrafficLight != null) //change by Debashis for deployment issue
        {

            switch (TrafficLight)
            {
                case "Green":
                    imgColumnCompanyTrafficLight.ImageUrl = "~/Images/TrafficLights/tlGreen.gif";
                    break;
                case "Yellow":
                    imgColumnCompanyTrafficLight.ImageUrl = "~/Images/TrafficLights/tlYellow.gif";
                    break;
                case "Red":
                    imgColumnCompanyTrafficLight.ImageUrl = "~/Images/TrafficLights/tlRed.gif";
                    break;
                case "Black":
                    imgColumnCompanyTrafficLight.ImageUrl = "~/Images/TrafficLights/tlBlack.gif";
                    break;
                case "Blue":
                    imgColumnCompanyTrafficLight.ImageUrl = "~/Images/TrafficLights/tlBlue.gif";
                    break;
                default:
                    break;
            }
        }
        Label lblKWI = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblKWI") as Label;
        Label lblPIN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPIN") as Label;
        Label lblWGP = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblWGP") as Label;
        Label lblHUN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblHUN") as Label;
        Label lblWDL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblWDL") as Label;
        Label lblBUN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblBUN") as Label;
        Label lblFML = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblFML") as Label;
        Label lblBGN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblBGN") as Label;
        Label lblCE = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblCE") as Label;
        Label lblANG = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblANG") as Label;
        Label lblPTL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPTL") as Label;
        Label lblPTH = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPTH") as Label;
        Label lblPEEL = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblPEEL") as Label;
        Label lblARP = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblARP") as Label;
        Label lblYEN = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblYEN") as Label;

        approvalText(lblKWI, ((object)grid.GetRowValues(e.VisibleIndex, "KWI")));
        approvalText(lblPIN, ((object)grid.GetRowValues(e.VisibleIndex, "PIN")));
        approvalText(lblWGP, ((object)grid.GetRowValues(e.VisibleIndex, "WGP")));
        approvalText(lblHUN, ((object)grid.GetRowValues(e.VisibleIndex, "HUN")));
        approvalText(lblWDL, ((object)grid.GetRowValues(e.VisibleIndex, "WDL")));
        approvalText(lblBUN, ((object)grid.GetRowValues(e.VisibleIndex, "BUN")));
        approvalText(lblFML, ((object)grid.GetRowValues(e.VisibleIndex, "FML")));
        approvalText(lblBGN, ((object)grid.GetRowValues(e.VisibleIndex, "BGN")));
        approvalText(lblCE, ((object)grid.GetRowValues(e.VisibleIndex, "CE")));
        approvalText(lblANG, ((object)grid.GetRowValues(e.VisibleIndex, "ANG")));
        approvalText(lblPTL, ((object)grid.GetRowValues(e.VisibleIndex, "PTD")));
        approvalText(lblPTH, ((object)grid.GetRowValues(e.VisibleIndex, "PTH")));
        approvalText(lblPEEL, ((object)grid.GetRowValues(e.VisibleIndex, "PNJ")));
        approvalText(lblARP, ((object)grid.GetRowValues(e.VisibleIndex, "KPH")));
        approvalText(lblYEN, ((object)grid.GetRowValues(e.VisibleIndex, "YEN")));

        HyperLink hlAssess = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "hlAssess") as HyperLink;
        if (hlAssess != null)
        {
            int QuestionnaireId = (int)grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");
            hlAssess.NavigateUrl = String.Format("../../../SafetyPQ_QuestionnaireModify.aspx{0}",
                                    QueryStringModule.Encrypt("q=" + QuestionnaireId.ToString()));

        }

        object o1 = grid.GetRowValues(e.VisibleIndex, "MainScorePOverall");
        object o2 = grid.GetRowValues(e.VisibleIndex, "QuestionnaireId");
        object o3 = grid.GetRowValues(e.VisibleIndex, "MainAssessmentRiskRating");

        string temp = "";
        if (grid.FindRowCellTemplateControl(e.VisibleIndex, null, "RiskLevel") != null)
        {
            HyperLink hl = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "RiskLevel") as HyperLink;
            if (hl != null) //change by Debashis for deployment issue
            {
                if (o1 != null && o2 != null && o3 != null)
                {
                    hl.Text = String.Format("{0}", o3);
                    hl.NavigateUrl = String.Format("javascript:popUp('./PopUps/SafetyPQ_QuestionnaireReport.aspx?id={0}');", o2);
                }
                else
                {
                    hl.Text = "";
                }
            }
        }

        try
        {
            temp = String.Format("{0}", o3);
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
    }

    public int getRiskLevel(string riskLevel)
    {
        int i = 0;
        if (riskLevel.Contains("Low")) i = 1;
        if (riskLevel.Contains("Medium")) i = 2;
        if (riskLevel.Contains("High")) i = 3;
        if (riskLevel.Contains("Forced High")) i = 4;

        return i;

    }
    protected void gridColShow(string Loc, bool visible)
    {
        if (grid.Columns[Loc].Visible != visible)
        {
            if (visible)
            {
                grid.Columns[Loc].VisibleIndex = i;
                i++;
                grid.Columns[Loc + " Residential Category"].VisibleIndex = i;
                i++;
                grid.Columns[Loc + " Sponsor"].VisibleIndex = i;
                i++;
                grid.Columns[Loc + " SPA"].VisibleIndex = i;
                i++;
                grid.Columns[Loc + " ARP"].VisibleIndex = i;
                i++;
                //grid.Columns[Loc + " CRP"].VisibleIndex = i;
                //i++;
            }

            grid.Columns[Loc].Visible = visible;
            grid.Columns[Loc + " SPA"].Visible = visible;
            grid.Columns[Loc + " Sponsor"].Visible = visible;
            grid.Columns[Loc + " Residential Category"].Visible = visible;
            grid.Columns[Loc + " ARP"].Visible = visible;
            //grid.Columns[Loc + " CRP"].Visible = visible;
        }
    }

    protected void gridColShow(string Loc, bool visible, bool ok)
    {
        if (ok)
        {
            if (visible)
            {
                grid.Columns[Loc].VisibleIndex = i;
                i++;
            }

            if (grid.Columns[Loc].Visible != visible)
            {
                grid.Columns[Loc].Visible = visible;
            } 
        }
    }

    protected void btnFilter_Click(object sender, EventArgs e)
    {
        int compId = convertObjToInt32(cbFilterCompany.SelectedItem.Value);
        string atype = (string)cbFilterType.SelectedItem.Value;
        string compAbn = (string)cbFilterABN.SelectedItem.Value;
        int questionnaireServiceCategoryId = convertObjToInt32(cbFilterService.SelectedItem.Value);
        List<repo.QuestionnaireReportOverview> qroList = questionnaireReportOverviewService.GetMany(compId, atype, questionnaireServiceCategoryId, compAbn);

        int siteRegionId = convertObjToInt32(cbFilterSite.SelectedItem.Value);
        List<int> companyIds = new List<int>();
        if (siteRegionId > 0)
        {
            List<repo.CompanySiteCategoryStandard> cscsList = companySiteCategoryStandardService.GetMany(null, st => st.SiteId == siteRegionId, null, null);
            foreach (repo.CompanySiteCategoryStandard cscs in cscsList)
            {
                companyIds.Add(cscs.CompanyId);
            }
        }
        else if (siteRegionId < 0)
        {
            List<repo.RegionsSite> regionSites = regionsSiteService.GetMany(null, st => st.Region.RegionValue == siteRegionId, null, null);
            foreach (repo.RegionsSite regionSite in regionSites)
            {
                foreach (repo.CompanySiteCategoryStandard cscs in regionSite.Site.CompanySiteCategoryStandards)
                {
                    companyIds.Add(cscs.CompanyId);
                }
            }
        }
        List<repo.QuestionnaireReportOverview> qroList1 = new List<repo.QuestionnaireReportOverview>();
        if (siteRegionId != 0)
        {
            companyIds = companyIds.Distinct().ToList();
            foreach (repo.QuestionnaireReportOverview r in qroList)
            {
                if (companyIds.Contains(r.CompanyId))
                    qroList1.Add(r);
            }
        }
        else
            qroList1 = qroList;


        

        DateTime startOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        List<repo.QuestionnaireReportOverview> qroList2 = checkForExceptions(qroList1, startOfCurrentMonth);
        Session["vlQuestionnaireReportOverview"] = qroList2;
        grid.DataSource = qroList2;
        grid.DataBind();
    }
    private void notUsed()
    {
        /*int count = 0;
            //QuestionnaireReportOverviewService qroService = new QuestionnaireReportOverviewService();
            QuestionnaireReportOverviewFilters qroFilters = new QuestionnaireReportOverviewFilters();

       

            //Sites Filter - HardCoded...
            if (convertObjToInt32(cbFilterSite.SelectedItem.Value) > 0)
            {
                qroFilters.Clear();
                qroFilters.Junction = string.Empty; // This prevents the ParameterBuilder from throwing an "AND" before next line's output

                //Sites
                int SiteId = convertObjToInt32(cbFilterSite.SelectedItem.Value);
                SitesService sService = new SitesService();
                Sites s = sService.GetBySiteId(SiteId);
                qroFilters.BeginGroup();
                switch(s.SiteAbbrev)
                {
                    case "KWI":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Kwi, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Kwi);
                        Session["QuestionnaireReportOverview_ColSite"] = "KWI";
                        break;
                    case "PIN":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pin, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pin);
                        Session["QuestionnaireReportOverview_ColSite"] = "PIN";
                        break;
                    case "WGP":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
                        Session["QuestionnaireReportOverview_ColSite"] = "WGP";
                        break;
                    case "HUN":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Hun, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Hun);
                        Session["QuestionnaireReportOverview_ColSite"] = "HUN";
                        break;
                    case "WDL":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wdl, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wdl);
                        Session["QuestionnaireReportOverview_ColSite"] = "WDL";
                        break;
                    case "FML":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Fml, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Fml);
                        Session["QuestionnaireReportOverview_ColSite"] = "FML";
                        break;
                    case "BUN":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bun, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bun);
                        Session["QuestionnaireReportOverview_ColSite"] = "BUN";
                        break;
                    case "BGN":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bgn, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bgn);
                        Session["QuestionnaireReportOverview_ColSite"] = "BGN";
                        break;
                    case "ANG":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ang, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ang);
                        Session["QuestionnaireReportOverview_ColSite"] = "ANG";
                        break;
                    case "PTH":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pth, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pth);
                        Session["QuestionnaireReportOverview_ColSite"] = "PTH";
                        break;
                    case "PTD": //Portland
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ptl, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ptl);
                        Session["QuestionnaireReportOverview_ColSite"] = "PTL";
                        break;
                    case "CE": //Central Engineering
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ce, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ce);
                        Session["QuestionnaireReportOverview_ColSite"] = "CE";
                        break;
                    case "PNJ": //Peel
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pel, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pel);
                        Session["QuestionnaireReportOverview_ColSite"] = "PEEL";
                        break;
                    case "YEN":
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Yen, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Yen);
                        Session["QuestionnaireReportOverview_ColSite"] = "Yen - ARP";
                        break;
                    case "KPH": //ARP Point Henry
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Arp, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Arp);
                        Session["QuestionnaireReportOverview_ColSite"] = "PTH - ARP";
                        break;
                }
          
                qroFilters.Junction = "AND";
                qroFilters.EndGroup();
            }
            else if (convertObjToInt32(cbFilterSite.SelectedItem.Value) < 0)
            {
                //Regions
                int RegionId = -1 * convertObjToInt32(cbFilterSite.SelectedItem.Value);

                int _s = convertObjToInt32(cbFilterSite.SelectedItem.Value);
                bool VICOPS = false;
                bool WAO = false;
                if (_s == -2)
                {
                    VICOPS = true;
                    WAO = true;
                    Session["QuestionnaireReportOverview_Region"] = "AU";
                }
                if (_s == -1)
                {
                    WAO = true;
                    Session["QuestionnaireReportOverview_Region"] = "WAO";
                }
                if (_s == -4)
                {
                    VICOPS = true;
                    Session["QuestionnaireReportOverview_Region"] = "VICOPS";
                }

                if (WAO && VICOPS)
                {
                    //do nothing cos not expected?!!!
                }
                else
                {
                    qroFilters.Clear();
                    qroFilters.Junction = string.Empty; // This prevents the ParameterBuilder from throwing an "AND" before next line's output
                    qroFilters.BeginGroup();
                    if (WAO)
                    {
                        qroFilters.Junction = string.Empty;
                        qroFilters.BeginGroup();
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Kwi, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Kwi);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pin, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pin);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Hun, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Hun);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wdl, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wdl);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Fml, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Fml);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bun, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bun);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Wgp, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Wgp);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Bgn, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Bgn);
                        qroFilters.EndGroup();
                    }
                    if (VICOPS)
                    {
                        qroFilters.Junction = string.Empty;
                        qroFilters.BeginGroup();
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ang, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ang);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pth, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pth);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ptl, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ptl);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Ce, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Ce);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Pel, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Pel);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Yen, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Yen);
                        qroFilters.EndGroup();

                        //qroFilters.Junction = "AND";
                        qroFilters.BeginGroup();
                        qroFilters.Junction = string.Empty;
                        qroFilters.AppendNotEquals(QuestionnaireReportOverviewColumn.Arp, "-");
                        qroFilters.Junction = "OR";
                        qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.Arp);
                        qroFilters.EndGroup();
                    }

                    qroFilters.Junction = "AND";
                    qroFilters.EndGroup();
                }
            }

            if (cbFilterType.SelectedItem.Value != null)
            {
                if (cbFilterType.SelectedItem.Value.ToString() == "Sub Contractor" || cbFilterType.SelectedItem.Value.ToString() == "Contractor")
                {
                    qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.Type, cbFilterType.SelectedItem.Value.ToString());
                }
            }
            if (cbFilterService.SelectedItem.Value != null)
            {
                if (convertObjToInt32(cbFilterService.SelectedItem.Value) > 0)
                {
                    qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.TypeOfService, cbFilterService.Value.ToString());
                }
            }
            else
            {
                qroFilters.AppendIsNull(QuestionnaireReportOverviewColumn.TypeOfService);
            }
        
            if (convertObjToInt32(cbFilterCompany.SelectedItem.Value) > 0)
            {
                qroFilters.AppendEquals(QuestionnaireReportOverviewColumn.CompanyId, cbFilterCompany.Value.ToString());
            }

            VList<QuestionnaireReportOverview> qroVlist = DataRepository.QuestionnaireReportOverviewProvider.GetPaged(qroFilters.ToString(), null, 0, 99999999, out count);
            Session["vlQuestionnaireReportOverview"] = qroVlist;
            DataSet ds = qroVlist.ToDataSet(false);
            grid.DataSource = ds.Tables[0];
            grid.DataBind();
            ShowHideCols();
          */
    }
     

    protected int convertObjToInt32(object o)
    {
        int i = 0;
        try
        {
            i = Convert.ToInt32(o);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            i = 0;
        }
        return i;
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (!IsCustomColumnFiltering(e.Column)) return;
        if (e.Editor is ASPxComboBox)
        {
            if (Session[GetSessionFilterName(e.Column)] != null)
            {
                ((ASPxComboBox)e.Editor).SelectedIndex = (int)Session[GetSessionFilterName(e.Column)] + 1; // + 1 for "Show All"
            }
            else
            {
                ((ASPxComboBox)e.Editor).SelectedIndex = 0; //Show All, set the SelectedIndex to -1 to show the empty string for "Show all"
            }
        }
    }
    string GetSessionFilterName(GridViewDataColumn column)
    {
        return column != null ? column.FieldName + "FilterSelIndex" : string.Empty;
    }
    bool IsCustomColumnFiltering(GridViewDataColumn column)
    {
        return column != null && (Equals(column, grid.Columns["Validity"]));
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, ASPxGridViewEditorCreateEventArgs e)
    {
        if (!IsCustomColumnFiltering(e.Column)) return;
        ComboBoxProperties combo = new ComboBoxProperties();
        combo.Items.Add("Show All", ShowAllFilterId);
        if (Equals(e.Column, grid.Columns["UnitPrice"]))
        {
            combo.Items.Add("Current", "");
            combo.Items.Add("Expires in <60 Days", "");
            combo.Items.Add("Expired", "");
        }
        e.EditorProperties = combo;
    }
    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (!IsCustomColumnFiltering(e.Column)) return;
        if (e.Kind != GridViewAutoFilterEventKind.CreateCriteria) return;
        int selectedIndex;
        if (!int.TryParse(e.Value, out selectedIndex)) return;
        if (selectedIndex == ShowAllFilterId)
        {
            Session[GetSessionFilterName(e.Column)] = null;
            e.Criteria = null;
        }
        else
        {
            Session[GetSessionFilterName(e.Column)] = selectedIndex;
            //if (Equals(e.Column, grid.Columns["Quantity"]))
            //{
            //    e.Criteria = new GroupOperator(GroupOperatorType.And,
            //        new BinaryOperator("Quantity", selectedIndex * IntStep, BinaryOperatorType.GreaterOrEqual),
            //        new BinaryOperator("Quantity", (selectedIndex + 1) * IntStep, BinaryOperatorType.Less));
            //}
            //if (Equals(e.Column, grid.Columns["UnitPrice"]))
            //{
            //    e.Criteria = new BinaryOperator("UnitPrice", (selectedIndex + 1) * DecimalStep, BinaryOperatorType.Less);
            //}
            //if (Equals(e.Column, grid.Columns["CompanyName"]))
            //{
            //    char[] values = GetSymbolValue(selectedIndex);
            //    e.Criteria = new GroupOperator(GroupOperatorType.And,
            //        new BinaryOperator("CompanyName", values[0], BinaryOperatorType.GreaterOrEqual),
            //        new BinaryOperator("CompanyName", values[1] + "zzz", BinaryOperatorType.Less));
            //}
        }
    }




}
