﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiPurchaseOrderListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(KpiPurchaseOrderListDataSourceDesigner))]
	public class KpiPurchaseOrderListDataSource : ProviderDataSource<KpiPurchaseOrderList, KpiPurchaseOrderListKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListDataSource class.
		/// </summary>
		public KpiPurchaseOrderListDataSource() : base(new KpiPurchaseOrderListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiPurchaseOrderListDataSourceView used by the KpiPurchaseOrderListDataSource.
		/// </summary>
		protected KpiPurchaseOrderListDataSourceView KpiPurchaseOrderListView
		{
			get { return ( View as KpiPurchaseOrderListDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the KpiPurchaseOrderListDataSource control invokes to retrieve data.
		/// </summary>
		public KpiPurchaseOrderListSelectMethod SelectMethod
		{
			get
			{
				KpiPurchaseOrderListSelectMethod selectMethod = KpiPurchaseOrderListSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (KpiPurchaseOrderListSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiPurchaseOrderListDataSourceView class that is to be
		/// used by the KpiPurchaseOrderListDataSource.
		/// </summary>
		/// <returns>An instance of the KpiPurchaseOrderListDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiPurchaseOrderList, KpiPurchaseOrderListKey> GetNewDataSourceView()
		{
			return new KpiPurchaseOrderListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiPurchaseOrderListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiPurchaseOrderListDataSourceView : ProviderDataSourceView<KpiPurchaseOrderList, KpiPurchaseOrderListKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiPurchaseOrderListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiPurchaseOrderListDataSourceView(KpiPurchaseOrderListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiPurchaseOrderListDataSource KpiPurchaseOrderListOwner
		{
			get { return Owner as KpiPurchaseOrderListDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal KpiPurchaseOrderListSelectMethod SelectMethod
		{
			get { return KpiPurchaseOrderListOwner.SelectMethod; }
			set { KpiPurchaseOrderListOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiPurchaseOrderListService KpiPurchaseOrderListProvider
		{
			get { return Provider as KpiPurchaseOrderListService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<KpiPurchaseOrderList> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<KpiPurchaseOrderList> results = null;
			KpiPurchaseOrderList item;
			count = 0;
			
			System.Int32 _kpiPurchaseOrderListId;
			System.String _area;
			System.String _projectNumber;
			System.String _projectStatus;
			System.String _taskNumber;
			System.String _projectDesc_nullable;
			System.String _purchaseOrderNumber;
			System.String _purchaseOrderLineNumber;

			switch ( SelectMethod )
			{
				case KpiPurchaseOrderListSelectMethod.Get:
					KpiPurchaseOrderListKey entityKey  = new KpiPurchaseOrderListKey();
					entityKey.Load(values);
					item = KpiPurchaseOrderListProvider.Get(entityKey);
					results = new TList<KpiPurchaseOrderList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case KpiPurchaseOrderListSelectMethod.GetAll:
                    results = KpiPurchaseOrderListProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case KpiPurchaseOrderListSelectMethod.GetPaged:
					results = KpiPurchaseOrderListProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.Find:
					if ( FilterParameters != null )
						results = KpiPurchaseOrderListProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = KpiPurchaseOrderListProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case KpiPurchaseOrderListSelectMethod.GetByKpiPurchaseOrderListId:
					_kpiPurchaseOrderListId = ( values["KpiPurchaseOrderListId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["KpiPurchaseOrderListId"], typeof(System.Int32)) : (int)0;
					item = KpiPurchaseOrderListProvider.GetByKpiPurchaseOrderListId(_kpiPurchaseOrderListId);
					results = new TList<KpiPurchaseOrderList>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case KpiPurchaseOrderListSelectMethod.GetByArea:
					_area = ( values["Area"] != null ) ? (System.String) EntityUtil.ChangeType(values["Area"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByArea(_area, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByProjectNumber:
					_projectNumber = ( values["ProjectNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["ProjectNumber"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByProjectNumber(_projectNumber, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByProjectStatus:
					_projectStatus = ( values["ProjectStatus"] != null ) ? (System.String) EntityUtil.ChangeType(values["ProjectStatus"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByProjectStatus(_projectStatus, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByTaskNumber:
					_taskNumber = ( values["TaskNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["TaskNumber"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByTaskNumber(_taskNumber, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByProjectDesc:
					_projectDesc_nullable = (System.String) EntityUtil.ChangeType(values["ProjectDesc"], typeof(System.String));
					results = KpiPurchaseOrderListProvider.GetByProjectDesc(_projectDesc_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByPurchaseOrderNumberPurchaseOrderLineNumber:
					_purchaseOrderNumber = ( values["PurchaseOrderNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["PurchaseOrderNumber"], typeof(System.String)) : string.Empty;
					_purchaseOrderLineNumber = ( values["PurchaseOrderLineNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["PurchaseOrderLineNumber"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByPurchaseOrderNumberPurchaseOrderLineNumber(_purchaseOrderNumber, _purchaseOrderLineNumber, this.StartIndex, this.PageSize, out count);
					break;
				case KpiPurchaseOrderListSelectMethod.GetByPurchaseOrderNumber:
					_purchaseOrderNumber = ( values["PurchaseOrderNumber"] != null ) ? (System.String) EntityUtil.ChangeType(values["PurchaseOrderNumber"], typeof(System.String)) : string.Empty;
					results = KpiPurchaseOrderListProvider.GetByPurchaseOrderNumber(_purchaseOrderNumber, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == KpiPurchaseOrderListSelectMethod.Get || SelectMethod == KpiPurchaseOrderListSelectMethod.GetByKpiPurchaseOrderListId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				KpiPurchaseOrderList entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					KpiPurchaseOrderListProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<KpiPurchaseOrderList> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			KpiPurchaseOrderListProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region KpiPurchaseOrderListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiPurchaseOrderListDataSource class.
	/// </summary>
	public class KpiPurchaseOrderListDataSourceDesigner : ProviderDataSourceDesigner<KpiPurchaseOrderList, KpiPurchaseOrderListKey>
	{
		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListDataSourceDesigner class.
		/// </summary>
		public KpiPurchaseOrderListDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiPurchaseOrderListSelectMethod SelectMethod
		{
			get { return ((KpiPurchaseOrderListDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new KpiPurchaseOrderListDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region KpiPurchaseOrderListDataSourceActionList

	/// <summary>
	/// Supports the KpiPurchaseOrderListDataSourceDesigner class.
	/// </summary>
	internal class KpiPurchaseOrderListDataSourceActionList : DesignerActionList
	{
		private KpiPurchaseOrderListDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the KpiPurchaseOrderListDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public KpiPurchaseOrderListDataSourceActionList(KpiPurchaseOrderListDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public KpiPurchaseOrderListSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion KpiPurchaseOrderListDataSourceActionList
	
	#endregion KpiPurchaseOrderListDataSourceDesigner
	
	#region KpiPurchaseOrderListSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the KpiPurchaseOrderListDataSource.SelectMethod property.
	/// </summary>
	public enum KpiPurchaseOrderListSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByKpiPurchaseOrderListId method.
		/// </summary>
		GetByKpiPurchaseOrderListId,
		/// <summary>
		/// Represents the GetByArea method.
		/// </summary>
		GetByArea,
		/// <summary>
		/// Represents the GetByProjectNumber method.
		/// </summary>
		GetByProjectNumber,
		/// <summary>
		/// Represents the GetByProjectStatus method.
		/// </summary>
		GetByProjectStatus,
		/// <summary>
		/// Represents the GetByTaskNumber method.
		/// </summary>
		GetByTaskNumber,
		/// <summary>
		/// Represents the GetByProjectDesc method.
		/// </summary>
		GetByProjectDesc,
		/// <summary>
		/// Represents the GetByPurchaseOrderNumberPurchaseOrderLineNumber method.
		/// </summary>
		GetByPurchaseOrderNumberPurchaseOrderLineNumber,
		/// <summary>
		/// Represents the GetByPurchaseOrderNumber method.
		/// </summary>
		GetByPurchaseOrderNumber
	}
	
	#endregion KpiPurchaseOrderListSelectMethod

	#region KpiPurchaseOrderListFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListFilter : SqlFilter<KpiPurchaseOrderListColumn>
	{
	}
	
	#endregion KpiPurchaseOrderListFilter

	#region KpiPurchaseOrderListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListExpressionBuilder : SqlExpressionBuilder<KpiPurchaseOrderListColumn>
	{
	}
	
	#endregion KpiPurchaseOrderListExpressionBuilder	

	#region KpiPurchaseOrderListProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;KpiPurchaseOrderListChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="KpiPurchaseOrderList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiPurchaseOrderListProperty : ChildEntityProperty<KpiPurchaseOrderListChildEntityTypes>
	{
	}
	
	#endregion KpiPurchaseOrderListProperty
}

