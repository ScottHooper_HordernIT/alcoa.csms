﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Web;

using System.Data;

public partial class UserControls_Main_KpiEngineeringProjectHours : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

        if (!postBack)
        {
            //DateTime PreviousMonth = DateTime.Now.AddMonths(-1);
            //grid.FilterExpression = String.Format("[Year] = {1} and [Month] = {0}", PreviousMonth.Month.ToString(), PreviousMonth.Year.ToString());
            //grid.DataBind();

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    if (!Helper.General.hasActivePrivilege(auth.UserId, (int)PrivilegeList.ViewKpiEngineeringProjectHours))
                    {
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    }
                    break;
                case ((int)RoleList.Administrator):

                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            BindData();

            // Export
            String exportFileName = @"ALCOA CSMS - Kpi Engineering Project Hours"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName, true);
        }
        else
        {
            grid.DataSource = (DataTable)Session["dtKpiEngineeringProjectHours"];
            grid.DataBind();
        }
    }

    private void BindData()
    {
        Session["dtKpiEngineeringProjectHours"] = GetKpiEngineeringProjectHours();
        grid.DataSource = (DataTable)Session["dtKpiEngineeringProjectHours"];
        grid.DataBind();
    }

    private DataTable GetKpiEngineeringProjectHours()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("kpiDateTime", typeof(DateTime));
        dt.Columns.Add("kpiDateTimeYear", typeof(int));
        dt.Columns.Add("kpiDateTimeMonth", typeof(int));
        dt.Columns.Add("CompanyName", typeof(string));
        dt.Columns.Add("SiteName", typeof(string));
        dt.Columns.Add("ProjectName", typeof(string));
        dt.Columns.Add("ProjectHours", typeof(decimal));

        KpiEngineeringProjectHoursService kService = new KpiEngineeringProjectHoursService();
        VList<KpiEngineeringProjectHours> kList = kService.GetAll();
        foreach (KpiEngineeringProjectHours k in kList)
        {
            if (!String.IsNullOrEmpty(k.ProjectCapital1Title) || k.ProjectCapital1Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital1Title, k.ProjectCapital1Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital2Title) || k.ProjectCapital2Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital2Title, k.ProjectCapital2Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital3Title) || k.ProjectCapital3Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital3Title, k.ProjectCapital3Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital4Title) || k.ProjectCapital4Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital4Title, k.ProjectCapital4Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital5Title) || k.ProjectCapital5Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital5Title, k.ProjectCapital5Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital6Title) || k.ProjectCapital6Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital6Title, k.ProjectCapital6Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital7Title) || k.ProjectCapital7Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital7Title, k.ProjectCapital7Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital8Title) || k.ProjectCapital8Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital8Title, k.ProjectCapital8Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital9Title) || k.ProjectCapital9Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital9Title, k.ProjectCapital9Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital10Title) || k.ProjectCapital10Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital10Title, k.ProjectCapital10Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital11Title) || k.ProjectCapital11Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital11Title, k.ProjectCapital11Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital12Title) || k.ProjectCapital12Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital12Title, k.ProjectCapital12Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital13Title) || k.ProjectCapital13Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital13Title, k.ProjectCapital13Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital14Title) || k.ProjectCapital14Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital14Title, k.ProjectCapital14Hours);
            }
            if (!String.IsNullOrEmpty(k.ProjectCapital15Title) || k.ProjectCapital15Hours != null)
            {
                dt.Rows.Add(k.KpiDateTime, k.KpiDateTimeYear, k.KpiDateTimeMonth, k.CompanyName, k.SiteName, k.ProjectCapital15Title, k.ProjectCapital15Hours);
            }

        }

        return dt;
    }
}
