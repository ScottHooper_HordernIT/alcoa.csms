﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SafetyPlanStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SafetyPlanStatusProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.SafetyPlanStatus, KaiZen.CSMS.Entities.SafetyPlanStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlanStatusKey key)
		{
			return Delete(transactionManager, key.StatusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_statusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _statusId)
		{
			return Delete(null, _statusId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _statusId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.SafetyPlanStatus Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlanStatusKey key, int start, int pageLength)
		{
			return GetByStatusId(transactionManager, key.StatusId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(System.Int32 _statusId)
		{
			int count = -1;
			return GetByStatusId(null,_statusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(System.Int32 _statusId, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusId(null, _statusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId)
		{
			int count = -1;
			return GetByStatusId(transactionManager, _statusId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusId(transactionManager, _statusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(System.Int32 _statusId, int start, int pageLength, out int count)
		{
			return GetByStatusId(null, _statusId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusId(TransactionManager transactionManager, System.Int32 _statusId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(null,_statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength)
		{
			int count = -1;
			return GetByStatusName(transactionManager, _statusName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(System.String _statusName, int start, int pageLength, out int count)
		{
			return GetByStatusName(null, _statusName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_SafetyPlanStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_statusName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.SafetyPlanStatus GetByStatusName(TransactionManager transactionManager, System.String _statusName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SafetyPlanStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SafetyPlanStatus&gt;"/></returns>
		public static TList<SafetyPlanStatus> Fill(IDataReader reader, TList<SafetyPlanStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.SafetyPlanStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SafetyPlanStatus")
					.Append("|").Append((System.Int32)reader[((int)SafetyPlanStatusColumn.StatusId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SafetyPlanStatus>(
					key.ToString(), // EntityTrackingKey
					"SafetyPlanStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.SafetyPlanStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.StatusId = (System.Int32)reader[((int)SafetyPlanStatusColumn.StatusId - 1)];
					c.StatusName = (System.String)reader[((int)SafetyPlanStatusColumn.StatusName - 1)];
					c.StatusDescription = (System.String)reader[((int)SafetyPlanStatusColumn.StatusDescription - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.SafetyPlanStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.StatusId = (System.Int32)reader[((int)SafetyPlanStatusColumn.StatusId - 1)];
			entity.StatusName = (System.String)reader[((int)SafetyPlanStatusColumn.StatusName - 1)];
			entity.StatusDescription = (System.String)reader[((int)SafetyPlanStatusColumn.StatusDescription - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.SafetyPlanStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.StatusId = (System.Int32)dataRow["StatusId"];
			entity.StatusName = (System.String)dataRow["StatusName"];
			entity.StatusDescription = (System.String)dataRow["StatusDescription"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlanStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlanStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlanStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SafetyPlanStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.SafetyPlanStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlanStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.SafetyPlanStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SafetyPlanStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.SafetyPlanStatus</c>
	///</summary>
	public enum SafetyPlanStatusChildEntityTypes
	{
	}
	
	#endregion SafetyPlanStatusChildEntityTypes
	
	#region SafetyPlanStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SafetyPlanStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusFilterBuilder : SqlFilterBuilder<SafetyPlanStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilterBuilder class.
		/// </summary>
		public SafetyPlanStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlanStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlanStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlanStatusFilterBuilder
	
	#region SafetyPlanStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SafetyPlanStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SafetyPlanStatusParameterBuilder : ParameterizedSqlFilterBuilder<SafetyPlanStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusParameterBuilder class.
		/// </summary>
		public SafetyPlanStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SafetyPlanStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SafetyPlanStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SafetyPlanStatusParameterBuilder
	
	#region SafetyPlanStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SafetyPlanStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SafetyPlanStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SafetyPlanStatusSortBuilder : SqlSortBuilder<SafetyPlanStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SafetyPlanStatusSqlSortBuilder class.
		/// </summary>
		public SafetyPlanStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SafetyPlanStatusSortBuilder
	
} // end namespace
