﻿
/*
	File generated by NetTiers templates [www.nettiers.com]
	Important: Do not modify this file. Edit the file RegionsSites.cs instead.
*/

#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using KaiZen.CSMS.Entities.Validation;
#endregion

namespace KaiZen.CSMS.Entities
{
	///<summary>
	/// An object representation of the 'RegionsSites' table. [No description found the database]	
	///</summary>
	[Serializable]
	[DataObject, CLSCompliant(true)]
	public abstract partial class RegionsSitesBase : EntityBase, IRegionsSites, IEntityId<RegionsSitesKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
	{		
		#region Variable Declarations
		
		/// <summary>
		///  Hold the inner data of the entity.
		/// </summary>
		private RegionsSitesEntityData entityData;
		
		/// <summary>
		/// 	Hold the original data of the entity, as loaded from the repository.
		/// </summary>
		private RegionsSitesEntityData _originalData;
		
		/// <summary>
		/// 	Hold a backup of the inner data of the entity.
		/// </summary>
		private RegionsSitesEntityData backupData; 
		
		/// <summary>
		/// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
		/// </summary>
		private string entityTrackingKey;
		
		/// <summary>
		/// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
		/// </summary>
		/// <remark>Mostly used for databinding</remark>
		[NonSerialized]
		private TList<RegionsSites> parentCollection;
		
		private bool inTxn = false;
		
		/// <summary>
		/// Occurs when a value is being changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event RegionsSitesEventHandler ColumnChanging;		
		
		/// <summary>
		/// Occurs after a value has been changed for the specified column.
		/// </summary>
		[field:NonSerialized]
		public event RegionsSitesEventHandler ColumnChanged;
		
		#endregion Variable Declarations
		
		#region Constructors
		///<summary>
		/// Creates a new <see cref="RegionsSitesBase"/> instance.
		///</summary>
		public RegionsSitesBase()
		{
			this.entityData = new RegionsSitesEntityData();
			this.backupData = null;
		}		
		
		///<summary>
		/// Creates a new <see cref="RegionsSitesBase"/> instance.
		///</summary>
		///<param name="_regionId"></param>
		///<param name="_siteId"></param>
		public RegionsSitesBase(System.Int32 _regionId, System.Int32 _siteId)
		{
			this.entityData = new RegionsSitesEntityData();
			this.backupData = null;

			this.RegionId = _regionId;
			this.SiteId = _siteId;
		}
		
		///<summary>
		/// A simple factory method to create a new <see cref="RegionsSites"/> instance.
		///</summary>
		///<param name="_regionId"></param>
		///<param name="_siteId"></param>
		public static RegionsSites CreateRegionsSites(System.Int32 _regionId, System.Int32 _siteId)
		{
			RegionsSites newRegionsSites = new RegionsSites();
			newRegionsSites.RegionId = _regionId;
			newRegionsSites.SiteId = _siteId;
			return newRegionsSites;
		}
				
		#endregion Constructors
			
		#region Properties	
		
		#region Data Properties		
		/// <summary>
		/// 	Gets or sets the RegionSiteId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[ReadOnlyAttribute(false)/*, XmlIgnoreAttribute()*/, DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(true, true, false)]
		public virtual System.Int32 RegionSiteId
		{
			get
			{
				return this.entityData.RegionSiteId; 
			}
			
			set
			{
				if (this.entityData.RegionSiteId == value)
					return;
				
                OnPropertyChanging("RegionSiteId");                    
				OnColumnChanging(RegionsSitesColumn.RegionSiteId, this.entityData.RegionSiteId);
				this.entityData.RegionSiteId = value;
				this.EntityId.RegionSiteId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(RegionsSitesColumn.RegionSiteId, this.entityData.RegionSiteId);
				OnPropertyChanged("RegionSiteId");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the RegionId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 RegionId
		{
			get
			{
				return this.entityData.RegionId; 
			}
			
			set
			{
				if (this.entityData.RegionId == value)
					return;
				
                OnPropertyChanging("RegionId");                    
				OnColumnChanging(RegionsSitesColumn.RegionId, this.entityData.RegionId);
				this.entityData.RegionId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(RegionsSitesColumn.RegionId, this.entityData.RegionId);
				OnPropertyChanged("RegionId");
			}
		}
		
		/// <summary>
		/// 	Gets or sets the SiteId property. 
		///		
		/// </summary>
		/// <value>This type is int.</value>
		/// <remarks>
		/// This property can not be set to null. 
		/// </remarks>
		
		




		[DescriptionAttribute(@""), System.ComponentModel.Bindable( System.ComponentModel.BindableSupport.Yes)]
		[DataObjectField(false, false, false)]
		public virtual System.Int32 SiteId
		{
			get
			{
				return this.entityData.SiteId; 
			}
			
			set
			{
				if (this.entityData.SiteId == value)
					return;
				
                OnPropertyChanging("SiteId");                    
				OnColumnChanging(RegionsSitesColumn.SiteId, this.entityData.SiteId);
				this.entityData.SiteId = value;
				if (this.EntityState == EntityState.Unchanged)
					this.EntityState = EntityState.Changed;
				OnColumnChanged(RegionsSitesColumn.SiteId, this.entityData.SiteId);
				OnPropertyChanged("SiteId");
			}
		}
		
		#endregion Data Properties		

		#region Source Foreign Key Property
				
		/// <summary>
		/// Gets or sets the source <see cref="Regions"/>.
		/// </summary>
		/// <value>The source Regions for RegionId.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual Regions RegionIdSource
      	{
            get { return entityData.RegionIdSource; }
            set { entityData.RegionIdSource = value; }
      	}
		/// <summary>
		/// Gets or sets the source <see cref="Sites"/>.
		/// </summary>
		/// <value>The source Sites for SiteId.</value>
        [XmlIgnore()]
		[Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
		public virtual Sites SiteIdSource
      	{
            get { return entityData.SiteIdSource; }
            set { entityData.SiteIdSource = value; }
      	}
		#endregion
		
		#region Children Collections
		#endregion Children Collections
		
		#endregion
		#region Validation
		
		/// <summary>
		/// Assigns validation rules to this object based on model definition.
		/// </summary>
		/// <remarks>This method overrides the base class to add schema related validation.</remarks>
		protected override void AddValidationRules()
		{
			//Validation rules based on database schema.
		}
   		#endregion
		
		#region Table Meta Data
		/// <summary>
		///		The name of the underlying database table.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string TableName
		{
			get { return "RegionsSites"; }
		}
		
		/// <summary>
		///		The name of the underlying database table's columns.
		/// </summary>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public override string[] TableColumns
		{
			get
			{
				return new string[] {"RegionSiteId", "RegionId", "SiteId"};
			}
		}
		#endregion 
		
		#region IEditableObject
		
		#region  CancelAddNew Event
		/// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		public delegate void CancelAddNewEventHandler(object sender, EventArgs e);
    
    	/// <summary>
		/// The CancelAddNew event.
		/// </summary>
		[field:NonSerialized]
		public event CancelAddNewEventHandler CancelAddNew ;

		/// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {    
			if (!SuppressEntityEvents)
			{
	            CancelAddNewEventHandler handler = CancelAddNew ;
            	if (handler != null)
	            {    
    	            handler(this, EventArgs.Empty) ;
        	    }
	        }
        }
		#endregion 
		
		/// <summary>
		/// Begins an edit on an object.
		/// </summary>
		void IEditableObject.BeginEdit() 
	    {
	        //Console.WriteLine("Start BeginEdit");
	        if (!inTxn) 
	        {
	            this.backupData = this.entityData.Clone() as RegionsSitesEntityData;
	            inTxn = true;
	            //Console.WriteLine("BeginEdit");
	        }
	        //Console.WriteLine("End BeginEdit");
	    }
	
		/// <summary>
		/// Discards changes since the last <c>BeginEdit</c> call.
		/// </summary>
	    void IEditableObject.CancelEdit() 
	    {
	        //Console.WriteLine("Start CancelEdit");
	        if (this.inTxn) 
	        {
	            this.entityData = this.backupData;
	            this.backupData = null;
				this.inTxn = false;

				if (this.bindingIsNew)
	        	//if (this.EntityState == EntityState.Added)
	        	{
					if (this.parentCollection != null)
						this.parentCollection.Remove( (RegionsSites) this ) ;
				}	            
	        }
	        //Console.WriteLine("End CancelEdit");
	    }
	
		/// <summary>
		/// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
		/// </summary>
	    void IEditableObject.EndEdit() 
	    {
	        //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
	        if (this.inTxn) 
	        {
	            this.backupData = null;
				if (this.IsDirty) 
				{
					if (this.bindingIsNew) {
						this.EntityState = EntityState.Added;
						this.bindingIsNew = false ;
					}
					else
						if (this.EntityState == EntityState.Unchanged) 
							this.EntityState = EntityState.Changed ;
				}

				this.bindingIsNew = false ;
	            this.inTxn = false;	            
	        }
	        //Console.WriteLine("End EndEdit");
	    }
	    
	    /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
	    [XmlIgnore]
		[Browsable(false)]
	    public override object ParentCollection
	    {
	        get 
	        {
	            return this.parentCollection;
	        }
	        set 
	        {
	            this.parentCollection = value as TList<RegionsSites>;
	        }
	    }
	    
	    /// <summary>
        /// Called when the entity is changed.
        /// </summary>
	    private void OnEntityChanged() 
	    {
	        if (!SuppressEntityEvents && !inTxn && this.parentCollection != null) 
	        {
	            this.parentCollection.EntityChanged(this as RegionsSites);
	        }
	    }


		#endregion
		
		#region ICloneable Members
		///<summary>
		///  Returns a Typed RegionsSites Entity 
		///</summary>
		protected virtual RegionsSites Copy(IDictionary existingCopies)
		{
			if (existingCopies == null)
			{
				// This is the root of the tree to be copied!
				existingCopies = new Hashtable();
			}

			//shallow copy entity
			RegionsSites copy = new RegionsSites();
			existingCopies.Add(this, copy);
			copy.SuppressEntityEvents = true;
				copy.RegionSiteId = this.RegionSiteId;
				copy.RegionId = this.RegionId;
				copy.SiteId = this.SiteId;
			
			if (this.RegionIdSource != null && existingCopies.Contains(this.RegionIdSource))
				copy.RegionIdSource = existingCopies[this.RegionIdSource] as Regions;
			else
				copy.RegionIdSource = MakeCopyOf(this.RegionIdSource, existingCopies) as Regions;
			if (this.SiteIdSource != null && existingCopies.Contains(this.SiteIdSource))
				copy.SiteIdSource = existingCopies[this.SiteIdSource] as Sites;
			else
				copy.SiteIdSource = MakeCopyOf(this.SiteIdSource, existingCopies) as Sites;
		
			copy.EntityState = this.EntityState;
			copy.SuppressEntityEvents = false;
			return copy;
		}		
		
		
		
		///<summary>
		///  Returns a Typed RegionsSites Entity 
		///</summary>
		public virtual RegionsSites Copy()
		{
			return this.Copy(null);	
		}
		
		///<summary>
		/// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone()
		{
			return this.Copy(null);
		}
		
		///<summary>
		/// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
		///</summary>
		public object Clone(IDictionary existingCopies)
		{
			return this.Copy(existingCopies);
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x)
		{
			if (x == null)
				return null;
				
			if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
		}
		
		///<summary>
		/// Returns a deep copy of the child collection object passed in.
		///</summary>
		public static object MakeCopyOf(object x, IDictionary existingCopies)
		{
			if (x == null)
				return null;
			
			if (x is ICloneableEx)
			{
				// Return a deep copy of the object
				return ((ICloneableEx)x).Clone(existingCopies);
			}
			else if (x is ICloneable)
			{
				// Return a deep copy of the object
				return ((ICloneable)x).Clone();
			}
			else
				throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
		}
		
		
		///<summary>
		///  Returns a Typed RegionsSites Entity which is a deep copy of the current entity.
		///</summary>
		public virtual RegionsSites DeepCopy()
		{
			return EntityHelper.Clone<RegionsSites>(this as RegionsSites);	
		}
		#endregion
		
		#region Methods	
			
		///<summary>
		/// Revert all changes and restore original values.
		///</summary>
		public override void CancelChanges()
		{
			IEditableObject obj = (IEditableObject) this;
			obj.CancelEdit();

			this.entityData = null;
			if (this._originalData != null)
			{
				this.entityData = this._originalData.Clone() as RegionsSitesEntityData;
			}
			else
			{
				//Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
				this.entityData = new RegionsSitesEntityData();
			}
		}	
		
		/// <summary>
		/// Accepts the changes made to this object.
		/// </summary>
		/// <remarks>
		/// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
		/// </remarks>
		public override void AcceptChanges()
		{
			base.AcceptChanges();

			// we keep of the original version of the data
			this._originalData = null;
			this._originalData = this.entityData.Clone() as RegionsSitesEntityData;
		}
		
		#region Comparision with original data
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="column">The column.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyChanged(RegionsSitesColumn column)
		{
			switch(column)
			{
					case RegionsSitesColumn.RegionSiteId:
					return entityData.RegionSiteId != _originalData.RegionSiteId;
					case RegionsSitesColumn.RegionId:
					return entityData.RegionId != _originalData.RegionId;
					case RegionsSitesColumn.SiteId:
					return entityData.SiteId != _originalData.SiteId;
			
				default:
					return false;
			}
		}
		
		/// <summary>
		/// Determines whether the property value has changed from the original data.
		/// </summary>
		/// <param name="columnName">The column name.</param>
		/// <returns>
		/// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyChanged(string columnName)
		{
			return 	IsPropertyChanged(EntityHelper.GetEnumValue< RegionsSitesColumn >(columnName));
		}
		
		/// <summary>
		/// Determines whether the data has changed from original.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
		/// </returns>
		public bool HasDataChanged()
		{
			bool result = false;
			result = result || entityData.RegionSiteId != _originalData.RegionSiteId;
			result = result || entityData.RegionId != _originalData.RegionId;
			result = result || entityData.SiteId != _originalData.SiteId;
			return result;
		}	
		
		///<summary>
		///  Returns a RegionsSites Entity with the original data.
		///</summary>
		public RegionsSites GetOriginalEntity()
		{
			if (_originalData != null)
				return CreateRegionsSites(
				_originalData.RegionId,
				_originalData.SiteId
				);
				
			return (RegionsSites)this.Clone();
		}
		#endregion
	
	#region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="RegionsSitesBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
			// Cast exception if Object1 is null or DbNull
			if (Object1 != null && Object1 != DBNull.Value && Object1 is RegionsSitesBase)
				return ValueEquals(this, (RegionsSitesBase)Object1);
			else
				return false;
        }

        /// <summary>
		/// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="RegionsSitesBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
			return this.RegionSiteId.GetHashCode() ^ 
					this.RegionId.GetHashCode() ^ 
					this.SiteId.GetHashCode();
        }
		
		///<summary>
		/// Returns a value indicating whether this instance is equal to a specified object using value semantics.
		///</summary>
		///<param name="toObject">An object to compare to this instance.</param>
		///<returns>true if toObject is a <see cref="RegionsSitesBase"/> and has the same value as this instance; otherwise, false.</returns>
		public virtual bool Equals(RegionsSitesBase toObject)
		{
			if (toObject == null)
				return false;
			return ValueEquals(this, toObject);
		}
		#endregion
		
		///<summary>
		/// Determines whether the specified <see cref="RegionsSitesBase"/> instances are considered equal using value semantics.
		///</summary>
		///<param name="Object1">The first <see cref="RegionsSitesBase"/> to compare.</param>
		///<param name="Object2">The second <see cref="RegionsSitesBase"/> to compare. </param>
		///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
		public static bool ValueEquals(RegionsSitesBase Object1, RegionsSitesBase Object2)
		{
			// both are null
			if (Object1 == null && Object2 == null)
				return true;

			// one or the other is null, but not both
			if (Object1 == null ^ Object2 == null)
				return false;
				
			bool equal = true;
			if (Object1.RegionSiteId != Object2.RegionSiteId)
				equal = false;
			if (Object1.RegionId != Object2.RegionId)
				equal = false;
			if (Object1.SiteId != Object2.SiteId)
				equal = false;
					
			return equal;
		}
		
		#endregion
		
		#region IComparable Members
		///<summary>
		/// Compares this instance to a specified object and returns an indication of their relative values.
		///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
		///</summary>
		///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
		public virtual int CompareTo(object obj)
		{
			throw new NotImplementedException();
			//return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((RegionsSitesBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
		}
		
		/*
		// static method to get a Comparer object
        public static RegionsSitesComparer GetComparer()
        {
            return new RegionsSitesComparer();
        }
        */

        // Comparer delegates back to RegionsSites
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

/*
        // Special implementation to be called by custom comparer
        public int CompareTo(RegionsSites rhs, RegionsSitesColumn which)
        {
            switch (which)
            {
            	
            	
            	case RegionsSitesColumn.RegionSiteId:
            		return this.RegionSiteId.CompareTo(rhs.RegionSiteId);
            		
            		                 
            	
            	
            	case RegionsSitesColumn.RegionId:
            		return this.RegionId.CompareTo(rhs.RegionId);
            		
            		                 
            	
            	
            	case RegionsSitesColumn.SiteId:
            		return this.SiteId.CompareTo(rhs.SiteId);
            		
            		                 
            }
            return 0;
        }
        */
	
		#endregion
		
		#region IComponent Members
		
		private ISite _site = null;

		/// <summary>
		/// Gets or Sets the site where this data is located.
		/// </summary>
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public ISite Site
		{
			get{ return this._site; }
			set{ this._site = value; }
		}

		#endregion

		#region IDisposable Members
		
		/// <summary>
		/// Notify those that care when we dispose.
		/// </summary>
		[field:NonSerialized]
		public event System.EventHandler Disposed;

		/// <summary>
		/// Clean up. Nothing here though.
		/// </summary>
		public virtual void Dispose()
		{
			this.parentCollection = null;
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		/// <summary>
		/// Clean up.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				EventHandler handler = Disposed;
				if (handler != null)
					handler(this, EventArgs.Empty);
			}
		}
		
		#endregion
				
		#region IEntityKey<RegionsSitesKey> Members
		
		// member variable for the EntityId property
		private RegionsSitesKey _entityId;

		/// <summary>
		/// Gets or sets the EntityId property.
		/// </summary>
		[XmlIgnore]
		public virtual RegionsSitesKey EntityId
		{
			get
			{
				if ( _entityId == null )
				{
					_entityId = new RegionsSitesKey(this);
				}

				return _entityId;
			}
			set
			{
				if ( value != null )
				{
					value.Entity = this;
				}
				
				_entityId = value;
			}
		}
		
		#endregion
		
		#region EntityState
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false) , XmlIgnoreAttribute()]
		public override EntityState EntityState 
		{ 
			get{ return entityData.EntityState;	 } 
			set{ entityData.EntityState = value; } 
		}
		#endregion 
		
		#region EntityTrackingKey
		///<summary>
		/// Provides the tracking key for the <see cref="EntityLocator"/>
		///</summary>
		[XmlIgnore]
		public override string EntityTrackingKey
		{
			get
			{
				if(entityTrackingKey == null)
					entityTrackingKey = new System.Text.StringBuilder("RegionsSites")
					.Append("|").Append( this.RegionSiteId.ToString()).ToString();
				return entityTrackingKey;
			}
			set
		    {
		        if (value != null)
                    entityTrackingKey = value;
		    }
		}
		#endregion 
		
		#region ToString Method
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return string.Format(System.Globalization.CultureInfo.InvariantCulture,
				"{4}{3}- RegionSiteId: {0}{3}- RegionId: {1}{3}- SiteId: {2}{3}{5}", 
				this.RegionSiteId,
				this.RegionId,
				this.SiteId,
				System.Environment.NewLine, 
				this.GetType(),
				this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n",this.Error));
		}
		
		#endregion ToString Method
		
		#region Inner data class
		
	/// <summary>
	///		The data structure representation of the 'RegionsSites' table.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	[EditorBrowsable(EditorBrowsableState.Never)]
	[Serializable]
	internal protected class RegionsSitesEntityData : ICloneable, ICloneableEx
	{
		#region Variable Declarations
		private EntityState currentEntityState = EntityState.Added;
		
		#region Primary key(s)
		/// <summary>			
		/// RegionSiteId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "RegionsSites"</remarks>
		public System.Int32 RegionSiteId;
			
		#endregion
		
		#region Non Primary key(s)
		
		/// <summary>
		/// RegionId : 
		/// </summary>
		public System.Int32 RegionId = (int)0;
		
		/// <summary>
		/// SiteId : 
		/// </summary>
		public System.Int32 SiteId = (int)0;
		#endregion
			
		#region Source Foreign Key Property
				
		private Regions _regionIdSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="Regions"/>.
		/// </summary>
		/// <value>The source Regions for RegionId.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual Regions RegionIdSource
      	{
            get { return this._regionIdSource; }
            set { this._regionIdSource = value; }
      	}
		private Sites _siteIdSource = null;
		
		/// <summary>
		/// Gets or sets the source <see cref="Sites"/>.
		/// </summary>
		/// <value>The source Sites for SiteId.</value>
		[XmlIgnore()]
		[Browsable(false)]
		public virtual Sites SiteIdSource
      	{
            get { return this._siteIdSource; }
            set { this._siteIdSource = value; }
      	}
		#endregion
        
		#endregion Variable Declarations

		#region Data Properties

		#endregion Data Properties
		#region Clone Method

		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public Object Clone()
		{
			RegionsSitesEntityData _tmp = new RegionsSitesEntityData();
						
			_tmp.RegionSiteId = this.RegionSiteId;
			
			_tmp.RegionId = this.RegionId;
			_tmp.SiteId = this.SiteId;
			
			#region Source Parent Composite Entities
			if (this.RegionIdSource != null)
				_tmp.RegionIdSource = MakeCopyOf(this.RegionIdSource) as Regions;
			if (this.SiteIdSource != null)
				_tmp.SiteIdSource = MakeCopyOf(this.SiteIdSource) as Sites;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		public object Clone(IDictionary existingCopies)
		{
			if (existingCopies == null)
				existingCopies = new Hashtable();
				
			RegionsSitesEntityData _tmp = new RegionsSitesEntityData();
						
			_tmp.RegionSiteId = this.RegionSiteId;
			
			_tmp.RegionId = this.RegionId;
			_tmp.SiteId = this.SiteId;
			
			#region Source Parent Composite Entities
			if (this.RegionIdSource != null && existingCopies.Contains(this.RegionIdSource))
				_tmp.RegionIdSource = existingCopies[this.RegionIdSource] as Regions;
			else
				_tmp.RegionIdSource = MakeCopyOf(this.RegionIdSource, existingCopies) as Regions;
			if (this.SiteIdSource != null && existingCopies.Contains(this.SiteIdSource))
				_tmp.SiteIdSource = existingCopies[this.SiteIdSource] as Sites;
			else
				_tmp.SiteIdSource = MakeCopyOf(this.SiteIdSource, existingCopies) as Sites;
			#endregion
		
			#region Child Collections
			#endregion Child Collections
			
			//EntityState
			_tmp.EntityState = this.EntityState;
			
			return _tmp;
		}
		
		#endregion Clone Method
		
		/// <summary>
		///		Indicates state of object
		/// </summary>
		/// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
		[BrowsableAttribute(false), XmlIgnoreAttribute()]
		public EntityState	EntityState
		{
			get { return currentEntityState;  }
			set { currentEntityState = value; }
		}
	
	}//End struct

		#endregion
		
				
		
		#region Events trigger
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="RegionsSitesColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanging(RegionsSitesColumn column)
		{
			OnColumnChanging(column, null);
			return;
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="RegionsSitesColumn"/> which has raised the event.</param>
		public virtual void OnColumnChanged(RegionsSitesColumn column)
		{
			OnColumnChanged(column, null);
			return;
		} 
		
		
		/// <summary>
		/// Raises the <see cref="ColumnChanging" /> event.
		/// </summary>
		/// <param name="column">The <see cref="RegionsSitesColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanging(RegionsSitesColumn column, object value)
		{
			if(IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);
                
			if (!SuppressEntityEvents)
			{
				RegionsSitesEventHandler handler = ColumnChanging;
				if(handler != null)
				{
					handler(this, new RegionsSitesEventArgs(column, value));
				}
			}
		}
		
		/// <summary>
		/// Raises the <see cref="ColumnChanged" /> event.
		/// </summary>
		/// <param name="column">The <see cref="RegionsSitesColumn"/> which has raised the event.</param>
		/// <param name="value">The changed value.</param>
		public virtual void OnColumnChanged(RegionsSitesColumn column, object value)
		{
			if (!SuppressEntityEvents)
			{
				RegionsSitesEventHandler handler = ColumnChanged;
				if(handler != null)
				{
					handler(this, new RegionsSitesEventArgs(column, value));
				}
			
				// warn the parent list that i have changed
				OnEntityChanged();
			}
		} 
		#endregion
			
	} // End Class
	
	
	#region RegionsSitesEventArgs class
	/// <summary>
	/// Provides data for the ColumnChanging and ColumnChanged events.
	/// </summary>
	/// <remarks>
	/// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
	/// of a property of a <see cref="RegionsSites"/> object.
	/// </remarks>
	public class RegionsSitesEventArgs : System.EventArgs
	{
		private RegionsSitesColumn column;
		private object value;
		
		///<summary>
		/// Initalizes a new Instance of the RegionsSitesEventArgs class.
		///</summary>
		public RegionsSitesEventArgs(RegionsSitesColumn column)
		{
			this.column = column;
		}
		
		///<summary>
		/// Initalizes a new Instance of the RegionsSitesEventArgs class.
		///</summary>
		public RegionsSitesEventArgs(RegionsSitesColumn column, object value)
		{
			this.column = column;
			this.value = value;
		}
		
		///<summary>
		/// The RegionsSitesColumn that was modified, which has raised the event.
		///</summary>
		///<value cref="RegionsSitesColumn" />
		public RegionsSitesColumn Column { get { return this.column; } }
		
		/// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
		public object Value{ get { return this.value; } }

	}
	#endregion
	
	///<summary>
	/// Define a delegate for all RegionsSites related events.
	///</summary>
	public delegate void RegionsSitesEventHandler(object sender, RegionsSitesEventArgs e);
	
	#region RegionsSitesComparer
		
	/// <summary>
	///	Strongly Typed IComparer
	/// </summary>
	public class RegionsSitesComparer : System.Collections.Generic.IComparer<RegionsSites>
	{
		RegionsSitesColumn whichComparison;
		
		/// <summary>
        /// Initializes a new instance of the <see cref="T:RegionsSitesComparer"/> class.
        /// </summary>
		public RegionsSitesComparer()
        {            
        }               
        
        /// <summary>
        /// Initializes a new instance of the <see cref="T:RegionsSitesComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public RegionsSitesComparer(RegionsSitesColumn column)
        {
            this.whichComparison = column;
        }

		/// <summary>
        /// Determines whether the specified <see cref="RegionsSites"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="RegionsSites"/> to compare.</param>
        /// <param name="b">The second <c>RegionsSites</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(RegionsSites a, RegionsSites b)
        {
            return this.Compare(a, b) == 0;
        }

		/// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(RegionsSites entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(RegionsSites a, RegionsSites b)
        {
        	EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
        	return entityPropertyComparer.Compare(a, b);
        }

		/// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public RegionsSitesColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
	}
	
	#endregion
	
	#region RegionsSitesKey Class

	/// <summary>
	/// Wraps the unique identifier values for the <see cref="RegionsSites"/> object.
	/// </summary>
	[Serializable]
	[CLSCompliant(true)]
	public class RegionsSitesKey : EntityKeyBase
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the RegionsSitesKey class.
		/// </summary>
		public RegionsSitesKey()
		{
		}
		
		/// <summary>
		/// Initializes a new instance of the RegionsSitesKey class.
		/// </summary>
		public RegionsSitesKey(RegionsSitesBase entity)
		{
			this.Entity = entity;

			#region Init Properties

			if ( entity != null )
			{
				this.RegionSiteId = entity.RegionSiteId;
			}

			#endregion
		}
		
		/// <summary>
		/// Initializes a new instance of the RegionsSitesKey class.
		/// </summary>
		public RegionsSitesKey(System.Int32 _regionSiteId)
		{
			#region Init Properties

			this.RegionSiteId = _regionSiteId;

			#endregion
		}
		
		#endregion Constructors

		#region Properties
		
		// member variable for the Entity property
		private RegionsSitesBase _entity;
		
		/// <summary>
		/// Gets or sets the Entity property.
		/// </summary>
		public RegionsSitesBase Entity
		{
			get { return _entity; }
			set { _entity = value; }
		}
		
		// member variable for the RegionSiteId property
		private System.Int32 _regionSiteId;
		
		/// <summary>
		/// Gets or sets the RegionSiteId property.
		/// </summary>
		public System.Int32 RegionSiteId
		{
			get { return _regionSiteId; }
			set
			{
				if ( this.Entity != null )
					this.Entity.RegionSiteId = value;
				
				_regionSiteId = value;
			}
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Reads values from the supplied <see cref="IDictionary"/> object into
		/// properties of the current object.
		/// </summary>
		/// <param name="values">An <see cref="IDictionary"/> instance that contains
		/// the key/value pairs to be used as property values.</param>
		public override void Load(IDictionary values)
		{
			#region Init Properties

			if ( values != null )
			{
				RegionSiteId = ( values["RegionSiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RegionSiteId"], typeof(System.Int32)) : (int)0;
			}

			#endregion
		}

		/// <summary>
		/// Creates a new <see cref="IDictionary"/> object and populates it
		/// with the property values of the current object.
		/// </summary>
		/// <returns>A collection of name/value pairs.</returns>
		public override IDictionary ToDictionary()
		{
			IDictionary values = new Hashtable();

			#region Init Dictionary

			values.Add("RegionSiteId", RegionSiteId);

			#endregion Init Dictionary

			return values;
		}
		
		///<summary>
		/// Returns a String that represents the current object.
		///</summary>
		public override string ToString()
		{
			return String.Format("RegionSiteId: {0}{1}",
								RegionSiteId,
								System.Environment.NewLine);
		}

		#endregion Methods
	}
	
	#endregion	

	#region RegionsSitesColumn Enum
	
	/// <summary>
	/// Enumerate the RegionsSites columns.
	/// </summary>
	[Serializable]
	public enum RegionsSitesColumn : int
	{
		/// <summary>
		/// RegionSiteId : 
		/// </summary>
		[EnumTextValue("RegionSiteId")]
		[ColumnEnum("RegionSiteId", typeof(System.Int32), System.Data.DbType.Int32, true, true, false)]
		RegionSiteId = 1,
		/// <summary>
		/// RegionId : 
		/// </summary>
		[EnumTextValue("RegionId")]
		[ColumnEnum("RegionId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		RegionId = 2,
		/// <summary>
		/// SiteId : 
		/// </summary>
		[EnumTextValue("SiteId")]
		[ColumnEnum("SiteId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
		SiteId = 3
	}//End enum

	#endregion RegionsSitesColumn Enum

} // end namespace
