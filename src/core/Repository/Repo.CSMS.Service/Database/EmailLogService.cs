﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;
using System.Web.Mvc;



namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEmailLogService
    {
        EmailLog Get(Expression<Func<EmailLog, bool>> where, List<Expression<Func<EmailLog, object>>> includes);
        List<EmailLog> GetMany(int? top, Expression<Func<EmailLog, bool>> where, Func<IQueryable<EmailLog>, IOrderedQueryable<EmailLog>> orderBy, List<Expression<Func<EmailLog, object>>> includes);
        List<EmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EmailLog, bool>> where, Func<IQueryable<EmailLog>, IOrderedQueryable<EmailLog>> orderBy, List<Expression<Func<EmailLog, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EmailLog, bool>> where, List<Expression<Func<EmailLog, object>>> includes);
        void Insert(EmailLog item, bool saveChanges = true);
        void Insert(string from, string[] to, string[] cc, string[] bcc, string subject, string body, string emailLogTypeName, bool IsBodyHtml, string attachmentPath, string attachmentFileFilter);
        void Insert(string[] to, string[] cc, string[] bcc, string subject, string body, string emailLogTypeName, bool IsBodyHtml, string attachmentPath, string attachmentFileFilter);
        void Insert(string to, string cc, string bcc, string subject, string body, string emailLogTypeName, bool IsBodyHtml, string attachmentPath, string attachmentFileFilter);
        void Update(EmailLog item, bool saveChanges = true);
        void Delete(EmailLog item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion



    #region Interface Implementation

    public class EmailLogService : IEmailLogService
    {
        private readonly IRepository<EmailLog> _repository;
        private readonly IRepository<EmailLogType> _emailLogTypeRepository;
        public EmailLogService(IRepository<EmailLog> repository, IRepository<EmailLogType> emailLogTypeRepository)
        {
            this._repository = repository;
            this._emailLogTypeRepository = emailLogTypeRepository;
        }

        public EmailLog Get(Expression<Func<EmailLog, bool>> where, List<Expression<Func<EmailLog, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EmailLog> GetMany(int? top, Expression<Func<EmailLog, bool>> where, Func<IQueryable<EmailLog>, IOrderedQueryable<EmailLog>> orderBy, List<Expression<Func<EmailLog, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EmailLog> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EmailLog, bool>> where, Func<IQueryable<EmailLog>, IOrderedQueryable<EmailLog>> orderBy, List<Expression<Func<EmailLog, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EmailLog, bool>> where, List<Expression<Func<EmailLog, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }


        #region Insert overloads

        // Array to/cc/bcc version of insert with from parameter
        public void Insert(string from, string[] to, string[] cc, string[] bcc, string subject, string body, string emailLogTypeName, bool isBodyHtml, string attachmentPath, string attachmentFileFilter)
        {
            Insert(from, CommonHelper.ConvertStringArrayToString(to), CommonHelper.ConvertStringArrayToString(cc), CommonHelper.ConvertStringArrayToString(bcc), subject, body, emailLogTypeName, isBodyHtml, attachmentPath, attachmentFileFilter);
        }

        // Array to/cc/bcc version of insert WITHOUT from parameter
        public void Insert(string[] to, string[] cc, string[] bcc, string subject, string body, string emailLogTypeName, bool isBodyHtml, string attachmentPath, string attachmentFileFilter)
        {
            Insert(null, CommonHelper.ConvertStringArrayToString(to), CommonHelper.ConvertStringArrayToString(cc), CommonHelper.ConvertStringArrayToString(bcc), subject, body, emailLogTypeName, isBodyHtml, attachmentPath, attachmentFileFilter);
        }

        // String to/cc/bcc version of insert WITHOUT from parameter
        public void Insert(string to, string cc, string bcc, string subject, string body, string emailLogTypeName, bool isBodyHtml, string attachmentPath, string attachmentFileFilter)
        {
            Insert(null, to, cc, bcc, subject, body, emailLogTypeName, isBodyHtml, attachmentPath, attachmentFileFilter);
        }
        
        // String to/cc/bcc version of insert with from parameter
        public void Insert(string from, string to, string cc, string bcc, string subject, string body, string emailLogTypeName, bool isBodyHtml, string attachmentPath, string attachmentFileFilter)
        {
            Repo.CSMS.DAL.EntityModels.EmailLog el = new Repo.CSMS.DAL.EntityModels.EmailLog();

            // lookup the email log type id from the name
            var elt = _emailLogTypeRepository.Get(p=>p.EmailLogTypeName == emailLogTypeName, null);

            if (elt != null)
                el.EmailLogTypeId = elt.EmailLogTypeId;
            else // if the emailLogType cannot be found
            {
                elt = _emailLogTypeRepository.Get(p => p.EmailLogTypeName == "General", null);
                if (elt != null) 
                    el.EmailLogTypeId = elt.EmailLogTypeId;
                else //If it is still not found, use 5.
                    el.EmailLogTypeId = 5;
            }
            el.EmailDateTime = DateTime.Now;

            if (from != null)
                el.EmailFrom = from;

            el.EmailTo = CommonHelper.ConvertToCSV(to);
            el.EmailCc = CommonHelper.ConvertToCSV(cc);
            el.EmailBcc = CommonHelper.ConvertToCSV(bcc);

            el.EmailLogMessageSubject = subject;

            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            el.EmailLogMessageBody = (byte[])encoding.GetBytes(body);

            el.IsHTML = isBodyHtml;
            el.Handled = false;
            el.AttachmentPath = attachmentPath;
            el.AttachmentFileFilter = attachmentFileFilter;

            Insert(el); //call the default insert
        }


        public void Insert(EmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailLog");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }
        #endregion


        public void Update(EmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailLog");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EmailLog item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailLog");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion

}
