﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireReportOverviewSubQuery2Repeater</c>
    /// </summary>
	public class QuestionnaireReportOverviewSubQuery2RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery2RepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery2RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireReportOverviewSubQuery2Repeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireReportOverviewSubQuery2Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireReportOverviewSubQuery2Repeater z = (QuestionnaireReportOverviewSubQuery2Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireReportOverviewSubQuery2Repeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireReportOverviewSubQuery2RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireReportOverviewSubQuery2Repeater runat=\"server\"></{0}:QuestionnaireReportOverviewSubQuery2Repeater>")]
	public class QuestionnaireReportOverviewSubQuery2Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery2Repeater"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery2Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery2Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery2Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery2Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery2Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireReportOverviewSubQuery2Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery2 entity = o as KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery2;
						QuestionnaireReportOverviewSubQuery2Item container = new QuestionnaireReportOverviewSubQuery2Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireReportOverviewSubQuery2Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery2 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery2Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery2Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireReportOverviewSubQuery2Item"/> class.
        /// </summary>
		public QuestionnaireReportOverviewSubQuery2Item(KaiZen.CSMS.Entities.QuestionnaireReportOverviewSubQuery2 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CompanyId
        /// </summary>
        /// <value>The CompanyId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CompanyId
		{
			get { return _entity.CompanyId; }
		}
        /// <summary>
        /// Gets the KwiArpName
        /// </summary>
        /// <value>The KwiArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiArpName
		{
			get { return _entity.KwiArpName; }
		}
        /// <summary>
        /// Gets the KwiCrpName
        /// </summary>
        /// <value>The KwiCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String KwiCrpName
		{
			get { return _entity.KwiCrpName; }
		}
        /// <summary>
        /// Gets the PinArpName
        /// </summary>
        /// <value>The PinArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinArpName
		{
			get { return _entity.PinArpName; }
		}
        /// <summary>
        /// Gets the PinCrpName
        /// </summary>
        /// <value>The PinCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PinCrpName
		{
			get { return _entity.PinCrpName; }
		}
        /// <summary>
        /// Gets the WgpArpName
        /// </summary>
        /// <value>The WgpArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpArpName
		{
			get { return _entity.WgpArpName; }
		}
        /// <summary>
        /// Gets the WgpCrpName
        /// </summary>
        /// <value>The WgpCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WgpCrpName
		{
			get { return _entity.WgpCrpName; }
		}
        /// <summary>
        /// Gets the HunArpName
        /// </summary>
        /// <value>The HunArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunArpName
		{
			get { return _entity.HunArpName; }
		}
        /// <summary>
        /// Gets the HunCrpName
        /// </summary>
        /// <value>The HunCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HunCrpName
		{
			get { return _entity.HunCrpName; }
		}
        /// <summary>
        /// Gets the WdlArpName
        /// </summary>
        /// <value>The WdlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlArpName
		{
			get { return _entity.WdlArpName; }
		}
        /// <summary>
        /// Gets the WdlCrpName
        /// </summary>
        /// <value>The WdlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String WdlCrpName
		{
			get { return _entity.WdlCrpName; }
		}
        /// <summary>
        /// Gets the BunArpName
        /// </summary>
        /// <value>The BunArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunArpName
		{
			get { return _entity.BunArpName; }
		}
        /// <summary>
        /// Gets the BunCrpName
        /// </summary>
        /// <value>The BunCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BunCrpName
		{
			get { return _entity.BunCrpName; }
		}
        /// <summary>
        /// Gets the FmlArpName
        /// </summary>
        /// <value>The FmlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlArpName
		{
			get { return _entity.FmlArpName; }
		}
        /// <summary>
        /// Gets the FmlCrpName
        /// </summary>
        /// <value>The FmlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FmlCrpName
		{
			get { return _entity.FmlCrpName; }
		}
        /// <summary>
        /// Gets the BgnArpName
        /// </summary>
        /// <value>The BgnArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnArpName
		{
			get { return _entity.BgnArpName; }
		}
        /// <summary>
        /// Gets the BgnCrpName
        /// </summary>
        /// <value>The BgnCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String BgnCrpName
		{
			get { return _entity.BgnCrpName; }
		}
        /// <summary>
        /// Gets the CeArpName
        /// </summary>
        /// <value>The CeArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeArpName
		{
			get { return _entity.CeArpName; }
		}
        /// <summary>
        /// Gets the CeCrpName
        /// </summary>
        /// <value>The CeCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CeCrpName
		{
			get { return _entity.CeCrpName; }
		}
        /// <summary>
        /// Gets the AngArpName
        /// </summary>
        /// <value>The AngArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngArpName
		{
			get { return _entity.AngArpName; }
		}
        /// <summary>
        /// Gets the AngCrpName
        /// </summary>
        /// <value>The AngCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AngCrpName
		{
			get { return _entity.AngCrpName; }
		}
        /// <summary>
        /// Gets the PtlArpName
        /// </summary>
        /// <value>The PtlArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlArpName
		{
			get { return _entity.PtlArpName; }
		}
        /// <summary>
        /// Gets the PtlCrpName
        /// </summary>
        /// <value>The PtlCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PtlCrpName
		{
			get { return _entity.PtlCrpName; }
		}
        /// <summary>
        /// Gets the PthArpName
        /// </summary>
        /// <value>The PthArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthArpName
		{
			get { return _entity.PthArpName; }
		}
        /// <summary>
        /// Gets the PthCrpName
        /// </summary>
        /// <value>The PthCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PthCrpName
		{
			get { return _entity.PthCrpName; }
		}
        /// <summary>
        /// Gets the PelArpName
        /// </summary>
        /// <value>The PelArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelArpName
		{
			get { return _entity.PelArpName; }
		}
        /// <summary>
        /// Gets the PelCrpName
        /// </summary>
        /// <value>The PelCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String PelCrpName
		{
			get { return _entity.PelCrpName; }
		}
        /// <summary>
        /// Gets the ArpArpName
        /// </summary>
        /// <value>The ArpArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpArpName
		{
			get { return _entity.ArpArpName; }
		}
        /// <summary>
        /// Gets the ArpCrpName
        /// </summary>
        /// <value>The ArpCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ArpCrpName
		{
			get { return _entity.ArpCrpName; }
		}
        /// <summary>
        /// Gets the YenArpName
        /// </summary>
        /// <value>The YenArpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenArpName
		{
			get { return _entity.YenArpName; }
		}
        /// <summary>
        /// Gets the YenCrpName
        /// </summary>
        /// <value>The YenCrpName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String YenCrpName
		{
			get { return _entity.YenCrpName; }
		}

	}
}
