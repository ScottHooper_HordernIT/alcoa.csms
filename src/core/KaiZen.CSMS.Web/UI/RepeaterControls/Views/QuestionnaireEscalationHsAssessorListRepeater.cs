﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>QuestionnaireEscalationHsAssessorListRepeater</c>
    /// </summary>
	public class QuestionnaireEscalationHsAssessorListRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireEscalationHsAssessorListRepeaterDesigner"/> class.
        /// </summary>
		public QuestionnaireEscalationHsAssessorListRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is QuestionnaireEscalationHsAssessorListRepeater))
			{ 
				throw new ArgumentException("Component is not a QuestionnaireEscalationHsAssessorListRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			QuestionnaireEscalationHsAssessorListRepeater z = (QuestionnaireEscalationHsAssessorListRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="QuestionnaireEscalationHsAssessorListRepeater"/> Type.
    /// </summary>
	[Designer(typeof(QuestionnaireEscalationHsAssessorListRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:QuestionnaireEscalationHsAssessorListRepeater runat=\"server\"></{0}:QuestionnaireEscalationHsAssessorListRepeater>")]
	public class QuestionnaireEscalationHsAssessorListRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireEscalationHsAssessorListRepeater"/> class.
        /// </summary>
		public QuestionnaireEscalationHsAssessorListRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireEscalationHsAssessorListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireEscalationHsAssessorListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(QuestionnaireEscalationHsAssessorListItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireEscalationHsAssessorListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(QuestionnaireEscalationHsAssessorListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.QuestionnaireEscalationHsAssessorList entity = o as KaiZen.CSMS.Entities.QuestionnaireEscalationHsAssessorList;
						QuestionnaireEscalationHsAssessorListItem container = new QuestionnaireEscalationHsAssessorListItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class QuestionnaireEscalationHsAssessorListItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.QuestionnaireEscalationHsAssessorList _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireEscalationHsAssessorListItem"/> class.
        /// </summary>
		public QuestionnaireEscalationHsAssessorListItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:QuestionnaireEscalationHsAssessorListItem"/> class.
        /// </summary>
		public QuestionnaireEscalationHsAssessorListItem(KaiZen.CSMS.Entities.QuestionnaireEscalationHsAssessorList entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the QuestionnaireId
        /// </summary>
        /// <value>The QuestionnaireId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 QuestionnaireId
		{
			get { return _entity.QuestionnaireId; }
		}
        /// <summary>
        /// Gets the HsAssessorRegionId
        /// </summary>
        /// <value>The HsAssessorRegionId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HsAssessorRegionId
		{
			get { return _entity.HsAssessorRegionId; }
		}
        /// <summary>
        /// Gets the HsAssessorSiteId
        /// </summary>
        /// <value>The HsAssessorSiteId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HsAssessorSiteId
		{
			get { return _entity.HsAssessorSiteId; }
		}
        /// <summary>
        /// Gets the UserId
        /// </summary>
        /// <value>The UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? UserId
		{
			get { return _entity.UserId; }
		}
        /// <summary>
        /// Gets the SupervisorUserId
        /// </summary>
        /// <value>The SupervisorUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? SupervisorUserId
		{
			get { return _entity.SupervisorUserId; }
		}
        /// <summary>
        /// Gets the Level3UserId
        /// </summary>
        /// <value>The Level3UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level3UserId
		{
			get { return _entity.Level3UserId; }
		}
        /// <summary>
        /// Gets the Level4UserId
        /// </summary>
        /// <value>The Level4UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level4UserId
		{
			get { return _entity.Level4UserId; }
		}
        /// <summary>
        /// Gets the Level5UserId
        /// </summary>
        /// <value>The Level5UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level5UserId
		{
			get { return _entity.Level5UserId; }
		}
        /// <summary>
        /// Gets the Level6UserId
        /// </summary>
        /// <value>The Level6UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level6UserId
		{
			get { return _entity.Level6UserId; }
		}
        /// <summary>
        /// Gets the Level7UserId
        /// </summary>
        /// <value>The Level7UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level7UserId
		{
			get { return _entity.Level7UserId; }
		}
        /// <summary>
        /// Gets the Level8UserId
        /// </summary>
        /// <value>The Level8UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level8UserId
		{
			get { return _entity.Level8UserId; }
		}
        /// <summary>
        /// Gets the Level9UserId
        /// </summary>
        /// <value>The Level9UserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Level9UserId
		{
			get { return _entity.Level9UserId; }
		}
        /// <summary>
        /// Gets the HsAssessorFirstLastName
        /// </summary>
        /// <value>The HsAssessorFirstLastName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HsAssessorFirstLastName
		{
			get { return _entity.HsAssessorFirstLastName; }
		}
        /// <summary>
        /// Gets the HsAssessorLastFirstName
        /// </summary>
        /// <value>The HsAssessorLastFirstName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HsAssessorLastFirstName
		{
			get { return _entity.HsAssessorLastFirstName; }
		}
        /// <summary>
        /// Gets the HsAssessorContactEmail
        /// </summary>
        /// <value>The HsAssessorContactEmail.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String HsAssessorContactEmail
		{
			get { return _entity.HsAssessorContactEmail; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the ActionDescription
        /// </summary>
        /// <value>The ActionDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ActionDescription
		{
			get { return _entity.ActionDescription; }
		}
        /// <summary>
        /// Gets the UserDescription
        /// </summary>
        /// <value>The UserDescription.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String UserDescription
		{
			get { return _entity.UserDescription; }
		}
        /// <summary>
        /// Gets the QuestionnairePresentlyWithSince
        /// </summary>
        /// <value>The QuestionnairePresentlyWithSince.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? QuestionnairePresentlyWithSince
		{
			get { return _entity.QuestionnairePresentlyWithSince; }
		}
        /// <summary>
        /// Gets the DaysWith
        /// </summary>
        /// <value>The DaysWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DaysWith
		{
			get { return _entity.DaysWith; }
		}

	}
}
