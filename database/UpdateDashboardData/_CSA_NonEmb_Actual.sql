USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[_CSA_NonEmb_Actual]    Script Date: 30/07/2015 4:25:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_CSA_NonEmb_Actual]                              
(                              
 @SiteId int,                              
 @Month int,                              
 @Year int                              
)                              
AS                    
DECLARE @Companies VARCHAR(MAX)          
                  
IF (@SiteId > 0)                               
 BEGIN     
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM    
  (SELECT DISTINCT(C.CompanyName)            
  FROM CompanySiteCategoryStandard B     
  INNER JOIN Companies C     
  ON C.CompanyId = B.CompanyId           
  WHERE B.SiteId = @SiteId     
  AND B.CompanySiteCategoryId = 2    
  AND B.CompanyId NOT IN   
  (SELECT DISTINCT A.CompanyId FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B            
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId        
  WHERE B.SiteId = @SiteId      
  AND A.[Year] = @Year             
  AND B.CompanySiteCategoryId = 2            
  AND A.TotalRating > 0)) X    
      
 END                        
ELSE                        
 BEGIN    
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM    
  (SELECT DISTINCT(C.CompanyName)            
  FROM CompanySiteCategoryStandard B     
  INNER JOIN Companies C     
  ON C.CompanyId = B.CompanyId           
  WHERE ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = B.SiteId) > 0)     
  AND B.CompanySiteCategoryId = 2    
  AND B.CompanyId NOT IN   
  (SELECT DISTINCT A.CompanyId FROM CSA A LEFT OUTER JOIN CompanySiteCategoryStandard B            
  ON A.CompanyId = B.CompanyId AND A.SiteId = B.SiteId        
  WHERE B.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)      
  AND A.[Year] = @Year             
  AND B.CompanySiteCategoryId = 2            
  AND A.TotalRating > 0)) X    
      
 END            
---------------------------------          
        
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year               
 AND [Month] = @Month AND [ItemName] = 'Non-Embedded 1 � Actual completed')              
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]              
 SET [NonCompliantCompanies] = @Companies              
 WHERE [SiteId] = @SiteId              
   AND [Month] = @Month              
   AND [Year] = @Year              
   AND [ItemName] = 'Non-Embedded 1 � Actual completed'              
ELSE              
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]              
           ([SiteId]              
           ,[Month]              
           ,[Year]              
           ,[ItemName]              
           ,[NonCompliantCompanies])              
     VALUES              
           (@SiteId              
           ,@Month              
           ,@Year              
           ,'Non-Embedded 1 � Actual completed'              
           ,@Companies)
----------------------------------------

GO


