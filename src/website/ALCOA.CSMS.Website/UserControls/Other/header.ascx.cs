﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Entities;
using System.Configuration;
using System.Net;

/* Last Updated: 11/09/2009
 * Description: Menu/Header
 */

public partial class UserControls_Other_header : System.Web.UI.UserControl
{
    #region Services

    public Repo.CSMS.Service.Database.Cprs.IUserService CPRS_UserService { get; set; }
    public Repo.CSMS.Service.Database.IUsefulLinkService UsefulLinkService { get; set; }

    #endregion

    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        Configuration config = new Configuration();

        if (config.GetValue(ConfigList.SqExemptionFormApssNumber) != "0")
        {
            ASPxMenu1.Items.FindByName("SqExemptionForm").NavigateUrl = 
                String.Format("javascript:popUp2('Common/GetFile.aspx{0}')", QueryStringModule.Encrypt("Type=APSS&SubType=Sq&File=" + config.GetValue(ConfigList.SqExemptionFormApssNumber)));
        }
        else
        {
            ASPxMenu1.Items.FindByName("SqExemptionForm").NavigateUrl = "~/SqExemptionForm.aspx";
        }

        ASPxMenu1.Items.FindByText("Contractor Contact Template").NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=Templates&File=cContactsInfo"); //Changed by Jolly for Spec # 72
        ASPxMenu1.Items.FindByName("Template_Medical").NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=Templates&File=Medical");
        ASPxMenu1.Items.FindByName("Template_MedicalVicOps").NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=Templates&File=MedicalVicOps");
        ASPxMenu1.Items.FindByName("Template_Training").NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=Templates&File=Training");
        ASPxMenu1.Items.FindByName("Template_Medical2").NavigateUrl = ASPxMenu1.Items.FindByName("Template_Medical").NavigateUrl;
        ASPxMenu1.Items.FindByName("Template_Training2").NavigateUrl = ASPxMenu1.Items.FindByName("Template_Training").NavigateUrl;

        ASPxMenu1.Items.FindByName("Bicycle_External").NavigateUrl = "~/compliancechart2p.aspx" + QueryStringModule.Encrypt("internal=0");
        ASPxMenu1.Items.FindByName("Bicycle_Internal").NavigateUrl = "~/compliancechart2p.aspx" + QueryStringModule.Encrypt("internal=1");

        if (ConfigurationManager.AppSettings["cprs_menu_navigation_url"] != null)
        {
            string url = ConfigurationManager.AppSettings["cprs_menu_navigation_url"]; 
            //ASPxMenu1.Items.FindByName("Cprs").NavigateUrl = ConfigurationManager.AppSettings["cprs_menu_navigation_url"];
            bool fromProxy = false;
            //Determine where the request is coming from, if it is coming from a proxy server, set the fromProxy variable to true
            System.Collections.Specialized.NameValueCollection headers = Context.Request.Headers;
            string[] keys = Context.Request.Headers.AllKeys;
            if (keys.Contains("X-Forwarded-Server"))
            {
                fromProxy = true;
            }
            if (!auth.isAlcoaDirect && auth.UserLogon.Substring(0,14).ToLower() == "australia_asia" ) //Normal Active Directory Login
            {
                ASPxMenu1.Items.FindByName("Cprs").NavigateUrl = url;
            }
            else if (auth.isAlcoaDirect && fromProxy == false && auth.UserLogon.Substring(0, 14).ToLower() == "australia_asia") //Logged in as another user where the "another user" is AUSRALIA_ASIA
            {
                string userLogon2 = KaiZen.CSMS.Encryption.Crypt.EncryptTripleDES(auth.UserLogon, "AlcoaSecretKey");
                ASPxMenu1.Items.FindByName("Cprs").NavigateUrl = url + String.Format("cprsproxy?{0}={1}", auth._AlcoaDirectHTTPParameter, userLogon2);
            }
            else if (auth.isAlcoaDirect && fromProxy == false && auth.UserLogon.Substring(0, 14).ToLower() != "australia_asia") //Logged in as another user where the "another user" is Alcoa Direct user
            {
                string userLogon1 = KaiZen.CSMS.Encryption.Crypt.EncryptTripleDES(auth.AlcoaDirectEmail, "AlcoaSecretKey");
                string url1 = url + String.Format("cprsproxy?{0}={1}", auth._AlcoaDirectHTTPParameter, userLogon1);
                ASPxMenu1.Items.FindByName("Cprs").NavigateUrl = url1;
            }

            else //When being accessed from Alcoa Direct
            {
                //Added by Ashley Goldstraw 1/12/2015 to set up link for Alcoa Direct
                string userLogon = KaiZen.CSMS.Encryption.Crypt.EncryptTripleDES(auth.AlcoaDirectEmail, "AlcoaSecretKey");
                string alcoaDirectPrefix;
                if (ConfigurationManager.AppSettings["cprs_menu_navigation_url"].ToLower().Contains("dev"))
                    alcoaDirectPrefix = ConfigurationManager.AppSettings["AlcoaDirectDevPrefix"].ToLower();
                else if (ConfigurationManager.AppSettings["cprs_menu_navigation_url"].ToLower().Contains("test"))
                    alcoaDirectPrefix = ConfigurationManager.AppSettings["AlcoaDirectTestPrefix"].ToLower();
                else
                    alcoaDirectPrefix = ConfigurationManager.AppSettings["AlcoaDirectProdPrefix"].ToLower(); ;
                ASPxMenu1.Items.FindByName("Cprs").NavigateUrl = String.Format("https://{0}.alcoadirect.com/cprsproxy?{1}={2}", alcoaDirectPrefix, auth._AlcoaDirectHTTPParameter, userLogon);
            }
                        
        }

        //ASPxMenu1.Items.FindByName("Training_Attendance").NavigateUrl = "~/Common/GetFile.aspx" + QueryStringModule.Encrypt("Type=APSS&File=43734.pdf");

        if (!postBack)
        { //first time load
            //StayAlive sa = new StayAlive();

            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "reconn key", sa.KeepAlive());
        }

        if (auth.RoleId == (int)RoleList.Administrator)
        {
            ASPxMenu1.Items.FindByText("Financial Reports").Visible = true;
            ASPxMenu1.Items.FindByText("Contract Reviews & Presentations").Visible = true;
            ASPxMenu1.Items.FindByText("Audit S8.12").Visible = true;
            ASPxMenu1.Items.FindByText("Incident Report").Visible = true;
            ASPxMenu1.Items.FindByText("Injury Free Days Report").Visible = true;
            ASPxMenu1.Items.FindByText("Work Levelling Tool").Visible = true;
            ASPxMenu1.Items.FindByText("CSA - 2008").Visible = true;
            ASPxMenu1.Items.FindByText("Validation Reports").Visible = true;

            ASPxMenu1.Items.FindByText("Australian Safety Qualification Information").Visible = true;

            ASPxMenu1.Items.FindByText("Australian Safety Qualification Process").Visible = true;

            ASPxMenu1.Items.FindByText("Australia").Visible = true;

            ASPxMenu1.Items.FindByText("Contractors on site (KPI)").Visible = true;

            //ASPxMenu1.Items.FindByText("WA SubContractor Process").Visible = true;
            //ASPxMenu1.Items.FindByName("SQlink").NavigateUrl = "~/SafetyPQ_Questionnaire_Select.aspx";

            //ASPxMenu1.Items.FindByText("Medical Submitted").Visible = true;
            //ASPxMenu1.Items.FindByText("Training Submitted").Visible = true;
            ASPxMenu1.Items.FindByName("Ebi").Visible = true;

            ASPxMenu1.Items.FindByName("PerformanceManagement_Tools").Visible = true;

            ASPxMenu1.Items.FindByName("KpiEngineeringProjectHours").Visible = true;

            ASPxMenu1.Items.FindByName("Cpms").Visible = true;

            ASPxMenu1.Items.FindByText("Add/Maintain Contractor Person Record").Visible = true;
        }
        else
        {
            switch (auth.RoleId)
            {
                case (int)RoleList.PreQual:
                    ASPxMenu1.Items.FindByText("Health & Safety").Visible = false;
                    ASPxMenu1.Items.FindByText("Training").Visible = false;
                    ASPxMenu1.Items.FindByText("Library").Visible = false;
                    ASPxMenu1.Items.FindByText("Performance Management").Visible = false;
                    ASPxMenu1.Items.FindByText("Contact Details").Visible = false;
                    ASPxMenu1.Items.FindByText("Safety Pre-Qualification").Visible = true;
                    ASPxMenu1.Items.FindByText("Safety Pre-Qualification").Text = "Safety Qualification";
                    ASPxMenu1.Items.FindByText("Add/Maintain Contractor Person Record").Visible = false;
                    break;
                case (int)RoleList.Reader:
                    //ASPxMenu1.Items.FindByText("Injury Free Days Report").Visible = true;
                    //Privileged Users
                    ASPxMenu1.Items.FindByText("Australia").Visible = true;

                    if (config.GetValue(ConfigList.CpmsLinkVisibleToReaders) == "1")
                        ASPxMenu1.Items.FindByName("Cpms").Visible = true;

                    if (Helper.General.isEbiUser(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByName("Ebi").Visible = true;
                        ASPxMenu1.Items.FindByText("Contractors on site (KPI)").Visible = true;
                    }

                    if (Helper.General.isPrivilegedUser(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByText("Financial Reports").Visible = true;
                        ASPxMenu1.Items.FindByText("Contract Reviews & Presentations").Visible = true;
                    }

                    if(Helper.General.isEhsConsultant(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByText("Contract Reviews & Presentations").Visible = true; //added 19-Sep-2011, requested by peter wisdom.
                    }

                    //Ehs Consultant or Procurement User
                    if (Helper.General.isEhsConsultant(auth.UserId) || Helper.General.isProcurementUser(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByText("Australian Safety Qualification Information").Visible = true;
                        ASPxMenu1.Items.FindByText("Australian Safety Qualification Process").Visible = true;
                        ASPxMenu1.Items.FindByText("Work Levelling Tool").Visible = true;
                        //ASPxMenu1.Items.FindByText("WA SubContractor Process").Visible = true;
                        //ASPxMenu1.Items.FindByName("SQlink").NavigateUrl = "~/SafetyPQ_Questionnaire_Select.aspx";
                        
                    }

                    //15-03-2010 [CSMS-114] Remove Procurement Access to Status Report
                    //01-06-2010 [CSMS-139] Re-Allow Procurement Access to Status Report & Modify Status Report
                    //if (Helper.General.isProcurementUser(auth.UserId))
                    //{
                    //    ASPxMenu1.Items.FindByName("SQ_Status").Visible = false;
                    //}

                    if (Helper.General.isSQReadAccessUser(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByText("Australian Safety Qualification Information").Visible = true;
                        ASPxMenu1.Items.FindByName("SQ_Database").Visible = true;
                        ASPxMenu1.Items.FindByName("SQ_Expiry").Visible = false;
                        ASPxMenu1.Items.FindByName("SQ_Services").Visible = false;
                        ASPxMenu1.Items.FindByName("SQ_Status").Visible = false;
                    }

                    //Ehs Consultant
                    if (Helper.General.isEhsConsultant(auth.UserId))
                    {
                        ASPxMenu1.Items.FindByText("Audit S8.12").Visible = true;
                        ASPxMenu1.Items.FindByName("PerformanceManagement_Tools").Visible = true;
                    }

                    ASPxMenu1.Items.FindByName("KpiEngineeringProjectHours").Visible = true;

                    ASPxMenu1.Items.FindByText("Add/Maintain Contractor Person Record").Visible = false;
                    break;
                case (int)RoleList.Contractor:
                    ASPxMenu1.Items.FindByText("Contractors").Text = "Your Company";
                    //ASPxMenu1.Items.FindByText("Safety KPI Compliance Chart (old)").NavigateUrl = "~/Bicycle.aspx?Draw=1&CompanyId=" + auth.CompanyId.ToString() + "&SiteId=-5&StatusId=3&Year=" + DateTime.Now.Year.ToString();

                    ASPxMenu1.Items.FindByText("Safety KPI Compliance Chart (Radar)").NavigateUrl = "~/ComplianceChart2.aspx?id=ComplianceChart2_S_A";
                    ASPxMenu1.Items.FindByText("Summary Compliance Report By Company").NavigateUrl = "~/Reports_SummaryCompliance.aspx?id=go";

                    ASPxMenu1.Items.FindByText("Australian Safety Qualification Process").Visible = true;

                    ASPxMenu1.Items.FindByText("Summary Compliance Report By Company").Text = "Summary Compliance Report";
                    ASPxMenu1.Items.FindByText("Summary Compliance Report By Site").Visible = false;


                    if (Helper.General.hasActivePrivilege(auth.UserId, (int)PrivilegeList.ViewKpiEngineeringProjectHours))
                    {
                        ASPxMenu1.Items.FindByName("KpiEngineeringProjectHours").Visible = true;
                    }

                    ASPxMenu1.Items.FindByText("Add/Maintain Contractor Person Record").Visible = false;

                    break;
                case (int)RoleList.CWKMaintainer:
                    foreach (DevExpress.Web.ASPxMenu.MenuItem menuItem in ASPxMenu1.Items)
                    {
                        ASPxMenu1.Items.FindByText("HOME").Enabled = false;
                        ASPxMenu1.Items.FindByText("Safety Pre-Qualification").Enabled = false;
                        ASPxMenu1.Items.FindByText("Health & Safety").Enabled = false;
                        ASPxMenu1.Items.FindByText("Training").Enabled = false;
                        ASPxMenu1.Items.FindByText("Library").Enabled = false;
                        ASPxMenu1.Items.FindByText("Performance Management").Enabled = false;
                        ASPxMenu1.Items.FindByText("Contact Details").Enabled = true;
                        
                        ASPxMenu1.Items.FindByText("Contractors").Visible = false;
                        ASPxMenu1.Items.FindByText("Contractor Contact Template").Visible = false;
                        ASPxMenu1.Items.FindByText("Alcoa").Visible = false;
                        ASPxMenu1.Items.FindByText("Who Has Access").Visible = false;
                        //ASPxMenu1.Items.FindByText("Company Contractor Review").Visible = false;
                    }
                    break;
                case (int)RoleList.CWKEnquiry:
                    foreach (DevExpress.Web.ASPxMenu.MenuItem menuItem in ASPxMenu1.Items)
                    {
                        ASPxMenu1.Items.FindByText("HOME").Enabled = false;
                        ASPxMenu1.Items.FindByText("Safety Pre-Qualification").Enabled = false;
                        ASPxMenu1.Items.FindByText("Health & Safety").Enabled = false;
                        ASPxMenu1.Items.FindByText("Training").Enabled = false;
                        ASPxMenu1.Items.FindByText("Library").Enabled = false;
                        ASPxMenu1.Items.FindByText("Performance Management").Enabled = false;
                        ASPxMenu1.Items.FindByText("Contact Details").Enabled = true;

                        ASPxMenu1.Items.FindByText("Contractors").Visible = false;
                        ASPxMenu1.Items.FindByText("Contractor Contact Template").Visible = false;
                        ASPxMenu1.Items.FindByText("Alcoa").Visible = false;
                        ASPxMenu1.Items.FindByText("Who Has Access").Visible = false;
                        ASPxMenu1.Items.FindByText("Company Contractor Review").Visible = false;
                    }
                    break;
                default:

                    break;
            }
        }

        //Show cprs menu item only if the user has permission to access it.
        var cprsUser = this.CPRS_UserService.Get(e => e.Username == auth.UserLogon, null);
        if (cprsUser != null)
        {
            ASPxMenu1.Items.FindByName("Cprs").Visible = true;
        }

        //Add useful links from the database
        var usefulLinksMenuItem = ASPxMenu1.Items.FindByName("UsefulLinks");
        if (usefulLinksMenuItem != null)
        {
            var links = this.UsefulLinkService.GetMany(null, e => e.IsVisible == true, o => o.OrderBy(b => b.Ordinal), null, false);
            if (links.Count > 0)
            {
                foreach (var link in links)
                {
                    usefulLinksMenuItem.Items.Add(link.Text, null, null, link.NavigateUrl, link.Target);
                }
            }
            else
            {
                usefulLinksMenuItem.Visible = false;
            }
        }
    }
}
