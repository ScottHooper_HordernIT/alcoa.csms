﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProcurementListProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class UsersProcurementListProviderBaseCore : EntityViewProviderBase<UsersProcurementList>
	{
		#region Custom Methods
		
		
		#region _UsersProcurementList_ListAssignedSq_FunctionalManager
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager()
		{
			return ListAssignedSq_FunctionalManager(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager(int start, int pageLength)
		{
			return ListAssignedSq_FunctionalManager(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager(TransactionManager transactionManager)
		{
			return ListAssignedSq_FunctionalManager(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedSq_FunctionalManager(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _UsersProcurementList_ListAssignedSq
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq()
		{
			return ListAssignedSq(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq(int start, int pageLength)
		{
			return ListAssignedSq(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq(TransactionManager transactionManager)
		{
			return ListAssignedSq(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedSq(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _UsersProcurementList_GetByUserId
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_GetByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserId(System.Int32? userId)
		{
			return GetByUserId(null, 0, int.MaxValue , userId);
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_GetByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserId(int start, int pageLength, System.Int32? userId)
		{
			return GetByUserId(null, start, pageLength , userId);
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_GetByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByUserId(TransactionManager transactionManager, System.Int32? userId)
		{
			return GetByUserId(transactionManager, 0, int.MaxValue , userId);
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_GetByUserId' stored procedure. 
		/// </summary>
		/// <param name="userId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByUserId(TransactionManager transactionManager, int start, int pageLength, System.Int32? userId);
		
		#endregion

		
		#region _UsersProcurementList_ListAssignedSq_FunctionalManager_Latest
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager_Latest' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager_Latest()
		{
			return ListAssignedSq_FunctionalManager_Latest(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager_Latest(int start, int pageLength)
		{
			return ListAssignedSq_FunctionalManager_Latest(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager_Latest' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_FunctionalManager_Latest(TransactionManager transactionManager)
		{
			return ListAssignedSq_FunctionalManager_Latest(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_FunctionalManager_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedSq_FunctionalManager_Latest(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _UsersProcurementList_ListAssignedSq_Latest
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_Latest' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_Latest()
		{
			return ListAssignedSq_Latest(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_Latest(int start, int pageLength)
		{
			return ListAssignedSq_Latest(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_Latest' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListAssignedSq_Latest(TransactionManager transactionManager)
		{
			return ListAssignedSq_Latest(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListAssignedSq_Latest' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListAssignedSq_Latest(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#region _UsersProcurementList_ListOrdered
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListOrdered' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered()
		{
			return ListOrdered(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListOrdered' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered(int start, int pageLength)
		{
			return ListOrdered(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListOrdered' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet ListOrdered(TransactionManager transactionManager)
		{
			return ListOrdered(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UsersProcurementList_ListOrdered' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet ListOrdered(TransactionManager transactionManager, int start, int pageLength);
		
		#endregion

		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;UsersProcurementList&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;UsersProcurementList&gt;"/></returns>
		protected static VList&lt;UsersProcurementList&gt; Fill(DataSet dataSet, VList<UsersProcurementList> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<UsersProcurementList>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;UsersProcurementList&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<UsersProcurementList>"/></returns>
		protected static VList&lt;UsersProcurementList&gt; Fill(DataTable dataTable, VList<UsersProcurementList> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					UsersProcurementList c = new UsersProcurementList();
					c.UsersProcurementId = (Convert.IsDBNull(row["UsersProcurementId"]))?(int)0:(System.Int32)row["UsersProcurementId"];
					c.UserId = (Convert.IsDBNull(row["UserId"]))?(int)0:(System.Int32)row["UserId"];
					c.UserFullName = (Convert.IsDBNull(row["UserFullName"]))?string.Empty:(System.String)row["UserFullName"];
					c.UserFullNameLogon = (Convert.IsDBNull(row["UserFullNameLogon"]))?string.Empty:(System.String)row["UserFullNameLogon"];
					c.Enabled = (Convert.IsDBNull(row["Enabled"]))?false:(System.Boolean)row["Enabled"];
					c.Email = (Convert.IsDBNull(row["Email"]))?string.Empty:(System.String)row["Email"];
					c.NoQuestionnairesAssignedTo = (Convert.IsDBNull(row["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)row["NoQuestionnairesAssignedTo"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;UsersProcurementList&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;UsersProcurementList&gt;"/></returns>
		protected VList<UsersProcurementList> Fill(IDataReader reader, VList<UsersProcurementList> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					UsersProcurementList entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<UsersProcurementList>("UsersProcurementList",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new UsersProcurementList();
					}
					
					entity.SuppressEntityEvents = true;

					entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementListColumn.UsersProcurementId)];
					//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
					entity.UserId = (System.Int32)reader[((int)UsersProcurementListColumn.UserId)];
					//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
					entity.UserFullName = (System.String)reader[((int)UsersProcurementListColumn.UserFullName)];
					//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
					entity.UserFullNameLogon = (System.String)reader[((int)UsersProcurementListColumn.UserFullNameLogon)];
					//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
					entity.Enabled = (System.Boolean)reader[((int)UsersProcurementListColumn.Enabled)];
					//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
					entity.Email = (System.String)reader[((int)UsersProcurementListColumn.Email)];
					//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
					entity.NoQuestionnairesAssignedTo = (reader.IsDBNull(((int)UsersProcurementListColumn.NoQuestionnairesAssignedTo)))?null:(System.Int32?)reader[((int)UsersProcurementListColumn.NoQuestionnairesAssignedTo)];
					//entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(reader["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)reader["NoQuestionnairesAssignedTo"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementList"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementList"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, UsersProcurementList entity)
		{
			reader.Read();
			entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementListColumn.UsersProcurementId)];
			//entity.UsersProcurementId = (Convert.IsDBNull(reader["UsersProcurementId"]))?(int)0:(System.Int32)reader["UsersProcurementId"];
			entity.UserId = (System.Int32)reader[((int)UsersProcurementListColumn.UserId)];
			//entity.UserId = (Convert.IsDBNull(reader["UserId"]))?(int)0:(System.Int32)reader["UserId"];
			entity.UserFullName = (System.String)reader[((int)UsersProcurementListColumn.UserFullName)];
			//entity.UserFullName = (Convert.IsDBNull(reader["UserFullName"]))?string.Empty:(System.String)reader["UserFullName"];
			entity.UserFullNameLogon = (System.String)reader[((int)UsersProcurementListColumn.UserFullNameLogon)];
			//entity.UserFullNameLogon = (Convert.IsDBNull(reader["UserFullNameLogon"]))?string.Empty:(System.String)reader["UserFullNameLogon"];
			entity.Enabled = (System.Boolean)reader[((int)UsersProcurementListColumn.Enabled)];
			//entity.Enabled = (Convert.IsDBNull(reader["Enabled"]))?false:(System.Boolean)reader["Enabled"];
			entity.Email = (System.String)reader[((int)UsersProcurementListColumn.Email)];
			//entity.Email = (Convert.IsDBNull(reader["Email"]))?string.Empty:(System.String)reader["Email"];
			entity.NoQuestionnairesAssignedTo = (reader.IsDBNull(((int)UsersProcurementListColumn.NoQuestionnairesAssignedTo)))?null:(System.Int32?)reader[((int)UsersProcurementListColumn.NoQuestionnairesAssignedTo)];
			//entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(reader["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)reader["NoQuestionnairesAssignedTo"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="UsersProcurementList"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="UsersProcurementList"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, UsersProcurementList entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UsersProcurementId = (Convert.IsDBNull(dataRow["UsersProcurementId"]))?(int)0:(System.Int32)dataRow["UsersProcurementId"];
			entity.UserId = (Convert.IsDBNull(dataRow["UserId"]))?(int)0:(System.Int32)dataRow["UserId"];
			entity.UserFullName = (Convert.IsDBNull(dataRow["UserFullName"]))?string.Empty:(System.String)dataRow["UserFullName"];
			entity.UserFullNameLogon = (Convert.IsDBNull(dataRow["UserFullNameLogon"]))?string.Empty:(System.String)dataRow["UserFullNameLogon"];
			entity.Enabled = (Convert.IsDBNull(dataRow["Enabled"]))?false:(System.Boolean)dataRow["Enabled"];
			entity.Email = (Convert.IsDBNull(dataRow["Email"]))?string.Empty:(System.String)dataRow["Email"];
			entity.NoQuestionnairesAssignedTo = (Convert.IsDBNull(dataRow["NoQuestionnairesAssignedTo"]))?(int)0:(System.Int32?)dataRow["NoQuestionnairesAssignedTo"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region UsersProcurementListFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListFilterBuilder : SqlFilterBuilder<UsersProcurementListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilterBuilder class.
		/// </summary>
		public UsersProcurementListFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListFilterBuilder

	#region UsersProcurementListParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListParameterBuilder : ParameterizedSqlFilterBuilder<UsersProcurementListColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListParameterBuilder class.
		/// </summary>
		public UsersProcurementListParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementListParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementListParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementListParameterBuilder
	
	#region UsersProcurementListSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementList"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersProcurementListSortBuilder : SqlSortBuilder<UsersProcurementListColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListSqlSortBuilder class.
		/// </summary>
		public UsersProcurementListSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersProcurementListSortBuilder

} // end namespace
