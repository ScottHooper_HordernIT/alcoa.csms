﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;  

using System.Collections.Generic;





public partial class UserControls_Main_SafetyCategoryUpdateManagerNew : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }



        private void moduleInit(bool postBack)
        {
            if (!postBack)
            { //first time load
                if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
                {
                    //grid.ExpandAll();
                    //grid.SettingsPager.PageSize = 50;
                    //grid.DataBind();
                }
                else
                {
                    Response.Redirect("default.aspx");
                }

                string exportFileName = @"ALCOA CSMS - Safety Categories, Sponsors - Approvals"; //Chrome & IE is fine.
                String userAgent = Request.Headers.Get("User-Agent");
                if (
                    (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                    (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                    )
                {
                    exportFileName = exportFileName.Replace(" ", "_");
                }
                Helper.ExportGrid.Settings(ucExportButtons, exportFileName, null, true);

                DataTable dtTable = ((DataView)dsCompanySiteCategoryStandard.Select(DataSourceSelectArguments.Empty)).ToTable();
                Session["dtTable"] = dtTable;
            }
            
        }

        protected void gridStandard_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "CompanySiteCategoryId" || e.Column.FieldName == "LocationSponsorUserId" || e.Column.FieldName == "Approved")
            {
                ASPxComboBox combo = (ASPxComboBox)e.Editor;
                combo.DataBindItems();

                string _text = string.Empty;
                if (e.Column.FieldName == "Approved") _text = "Tentative";

                ListEditItem item = new ListEditItem(_text, null);
                combo.Items.Insert(0, item);
            }
        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                //DT2973:ARP issue
                int CompanyId = Convert.ToInt32(grid.GetRowValues(grid.EditingRowVisibleIndex, "CompanyId").ToString());
                //int CompanyId = Convert.ToInt32(e.OldValues["CompanyId"]);
                int SiteId = Convert.ToInt32(e.OldValues["SiteId"]);

                int count = 0;
                int QuestionnaireId = 0;

                QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                QuestionnaireWithLocationApprovalViewLatestFilters qwlavlFilters = new QuestionnaireWithLocationApprovalViewLatestFilters();
                qwlavlFilters.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, CompanyId.ToString());
                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlTlist = qwlavlService.GetPaged(qwlavlFilters.ToString(), "", 0, 999, out count);

                if(count != 1) throw new Exception("Error Occurred. Could not find Company.");

                QuestionnaireId = qwlavlTlist[0].QuestionnaireId;

                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);
                if (cscs == null) throw new Exception("Unexpected Error. Please refresh (or re-open) this page.");
                cscs.CompanyId = CompanyId;
                cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
                if (e.NewValues["LocationSponsorUserId"] != null)
                {
                    cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
                }
                else
                {
                    cscs.LocationSponsorUserId = null;
                }
                if (e.NewValues["LocationSpaUserId"] != null)
                {
                    cscs.LocationSpaUserId = Convert.ToInt32(e.NewValues["LocationSpaUserId"]);
                }
                else
                {
                    cscs.LocationSpaUserId = null;
                }
                if (e.NewValues["CompanySiteCategoryId"] != null)
                {
                    cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
                }
                else
                {
                    cscs.CompanySiteCategoryId = null;
                }

                //Change By Sayani Sil For Item # 19

                GridViewDataColumn colARP = grid.Columns["ARP"] as GridViewDataColumn;
                ASPxDropDownEdit ddlEdit = grid.FindEditRowCellTemplateControl(colARP, "ASPxDropDownEdit1") as ASPxDropDownEdit;

                string str = ddlEdit.Text;


                if (str != null)
                {
                    int ArpUserId = 0;
                    int Val = Convert.ToInt32(e.Keys["CompanySiteCategoryStandardId"]);
                    DataView dvData = new DataView((DataTable)Session["dtTable"]);
                    dvData.RowFilter = "CompanySiteCategoryStandardId = '" + Val + "'";//Change

                    if (dvData.ToTable().Rows.Count > 0)

                        if (dvData.ToTable().Rows[0]["ArpUserId"].ToString() != "" && dvData.ToTable().Rows[0]["ARPName"].ToString() != "")
                        {
                            ArpUserId = Convert.ToInt32(dvData.ToTable().Rows[0]["ArpUserId"].ToString());
                            int ArpSiteId = 0;
                            string personIdARP = hdnPersonIds.Value.ToString();
                            int ArpMasterId = DataRepository.CompanySiteCategoryStandardProvider.Update_Arp(str, ArpUserId, ArpSiteId, personIdARP);
                        }
                        else
                        {
                            string personIdARP = hdnPersonIds.Value.ToString();
                            ArpUserId = DataRepository.CompanySiteCategoryStandardProvider.Set_Arp(str, 0, personIdARP);
                        }

                     cscs.ArpUserId = ArpUserId;
                }
                
                //if (e.NewValues["CrpUserId"] != null)
                //{
                //    cscs.CrpUserId = Convert.ToInt32(e.NewValues["CrpUserId"]);
                //}

                cscs.ModifiedByUserId = auth.UserId;
                cscs.ModifiedDate = DateTime.Now;
                if (e.NewValues["Approved"] != null)
                {
                    switch (e.NewValues["Approved"].ToString())
                    {
                        case "True":
                            cscs.Approved = true;
                            cscs.ApprovedByUserId = auth.UserId;
                            break;
                        case "False":
                            cscs.Approved = false;
                            cscs.ApprovedByUserId = auth.UserId;
                            break;
                        default:
                            break;
                    }
                }

                //cscsService.Save(cscs);

                CompanySiteCategoryStandard ObjCSCS = cscsService.Save(cscs);
                e.Cancel = true;
                grid.CancelEdit();

                //DataTable dt = (DataTable)Session["dtTable"];
                //GetUpdatedCSCS(dt, ObjCSCS, str);
                //grid.DataSource = (DataTable)Session["dtTable"];
                //grid.DataBind();

                string SiteName = "-";
                SitesService sService = new SitesService();
                Sites s = sService.GetBySiteId(cscs.SiteId);
                if (s != null) SiteName = s.SiteName;

                string SafetyCategory = "-";
                if (cscs.CompanySiteCategoryId != null)
                {
                    CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                    CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                    if (cscs != null) SafetyCategory = csc.CategoryName;
                }

                string LocationSponsorUser = "-";
                if (cscs.LocationSponsorUserId != null)
                {
                    UsersService uService = new UsersService();
                    Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                    if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
                }

                string Approval = "-";
                if (cscs.Approved != null)
                {
                    if (cscs.Approved == true)
                    {
                        Approval = "Yes";
                    }
                    else
                    {
                        Approval = "No";
                    }
                }

                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalModified,
                                                        QuestionnaireId,                            //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        "Edited in CMT Shop Front Tools section",   //Comments
                                                        null,                                       //OldAssigned
                                                        SiteName,                                   //NewAssigned
                                                        SafetyCategory,                             //NewAssigned2
                                                        LocationSponsorUser,                        //NewAssigned3
                                                        Approval)                                   //NewAssigned4
                                                        == false)
                {
                    //for now we do nothing if action log fails.
                }

            }
            catch (Exception ex)
            {
                e.Cancel = true;
                grid.CancelEdit();
                throw new Exception(ex.Message);
            }
        }


        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            try
            {
                //DT2973:ARP issue
                e.NewValues["CompanyName"] = GetMemoText();
                int CompanyId = Convert.ToInt32(e.NewValues["CompanyName"]);
                if(CompanyId == 0) throw new Exception("Error Occurred. Could not find Company.");
                //int CompanyId = Convert.ToInt32(e.NewValues["CompanyId"]);
                int SiteId = Convert.ToInt32(e.NewValues["SiteId"]);

                int count = 0;
                int QuestionnaireId = 0;

                QuestionnaireWithLocationApprovalViewLatestService qwlavlService = new QuestionnaireWithLocationApprovalViewLatestService();
                QuestionnaireWithLocationApprovalViewLatestFilters qwlavlFilters = new QuestionnaireWithLocationApprovalViewLatestFilters();
                qwlavlFilters.Append(QuestionnaireWithLocationApprovalViewLatestColumn.CompanyId, CompanyId.ToString());
                VList<QuestionnaireWithLocationApprovalViewLatest> qwlavlTlist = qwlavlService.GetPaged(qwlavlFilters.ToString(), "", 0, 999, out count);

                if(count != 1) throw new Exception("Error Occurred. Could not find Company.");

                QuestionnaireId = qwlavlTlist[0].QuestionnaireId;

                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                CompanySiteCategoryStandard cscs = new CompanySiteCategoryStandard();
                cscs.CompanyId = CompanyId;
                cscs.SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
                if (e.NewValues["LocationSponsorUserId"] != null)
                {
                    cscs.LocationSponsorUserId = Convert.ToInt32(e.NewValues["LocationSponsorUserId"]);
                }
                if (e.NewValues["LocationSpaUserId"] != null)
                {
                    cscs.LocationSpaUserId = Convert.ToInt32(e.NewValues["LocationSpaUserId"]);
                }
                if (e.NewValues["CompanySiteCategoryId"] != null)
                {
                    cscs.CompanySiteCategoryId = Convert.ToInt32(e.NewValues["CompanySiteCategoryId"]);
                }

                //Change by Sayani Sil For Item# 19
                GridViewDataColumn colARP = grid.Columns["ARP"] as GridViewDataColumn;
                ASPxDropDownEdit ddlEdit = grid.FindEditRowCellTemplateControl(colARP, "ASPxDropDownEdit1") as ASPxDropDownEdit;

                string str = ddlEdit.Text;
                int ArpMasterId = 0;

                if (str != null)
                {
                    string personIdARP = hdnPersonIds.Value.ToString();
                    ArpMasterId = DataRepository.CompanySiteCategoryStandardProvider.Set_Arp(str, 0, personIdARP);
                    cscs.ArpUserId = ArpMasterId;
                }
                

                //if (e.NewValues["CrpUserId"] != null)
                //{
                //    cscs.CrpUserId = Convert.ToInt32(e.NewValues["CrpUserId"]);
                //}

                cscs.ModifiedByUserId = auth.UserId;
                cscs.ModifiedDate = DateTime.Now;
                if (e.NewValues["Approved"] != null)
                {
                    switch (e.NewValues["Approved"].ToString())
                    {
                        case "True":
                            cscs.Approved = true;
                            cscs.ApprovedByUserId = auth.UserId;
                            break;
                        case "False":
                            cscs.Approved = false;
                            cscs.ApprovedByUserId = auth.UserId;
                            break;
                        default:
                            break;
                    }
                }

                cscsService.Save(cscs);

                //Added By Sayani Sil For Item# 19

                int companySiteCategoryId = cscs.CompanySiteCategoryStandardId;

                DataTable dt = (DataTable)Session["dtTable"];
                DataRow dr = dt.NewRow();
                dr["CompanySiteCategoryStandardId"] = companySiteCategoryId;
                dr["CompanyId"] = cscs.CompanyId;
                dr["SiteId"] = cscs.SiteId;
                if (cscs.CompanySiteCategoryId == null)
                {
                    dr["CompanySiteCategoryId"] = System.DBNull.Value;
                }
                else
                    dr["CompanySiteCategoryId"] = Convert.ToInt32(cscs.CompanySiteCategoryId);
                if (cscs.LocationSponsorUserId == null)
                {
                    dr["LocationSponsorUserId"] = System.DBNull.Value;
                }
                else
                    dr["LocationSponsorUserId"] = Convert.ToString(cscs.LocationSponsorUserId);
                if (cscs.Approved == null)
                {
                    dr["Approved"] = System.DBNull.Value;
                }
                else
                    dr["Approved"] = Convert.ToBoolean(cscs.Approved);
                if (cscs.LocationSpaUserId == null)
                {
                    dr["LocationSpaUserId"] = System.DBNull.Value;
                }
                else
                    dr["LocationSpaUserId"] = Convert.ToInt32(cscs.LocationSpaUserId);
                dr["ArpUserId"] = cscs.ArpUserId;
                dr["ARPName"] = str;
                dt.Rows.Add(dr);
                Session["dtTable"] = dt;



                //END

                e.Cancel = true;
                grid.CancelEdit();

                //grid.DataSource = (DataTable)Session["dtTable"];
                //grid.DataBind();

                string SiteName = "-";
                SitesService sService = new SitesService();
                Sites s = sService.GetBySiteId(cscs.SiteId);
                if (s != null) SiteName = s.SiteName;

                string SafetyCategory = "-";
                if (cscs.CompanySiteCategoryId != null)
                {
                    CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                    CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(Convert.ToInt32(cscs.CompanySiteCategoryId));
                    if (cscs != null) SafetyCategory = csc.CategoryName;
                }

                string LocationSponsorUser = "-";
                if (cscs.LocationSponsorUserId != null)
                {
                    UsersService uService = new UsersService();
                    Users u = uService.GetByUserId(Convert.ToInt32(cscs.LocationSponsorUserId));
                    if (u != null) LocationSponsorUser = u.FirstName + " " + u.LastName;
                }

                string Approval = "-";
                if (cscs.Approved != null)
                {
                    if (cscs.Approved == true)
                    {
                        Approval = "Yes";
                    }
                    else
                    {
                        Approval = "No";
                    }
                }

                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.LocationApprovalAdded,
                                                        QuestionnaireId,                           //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        "Edited in CMT Shop Front Tools section",   //Comments
                                                        null,                                       //OldAssigned
                                                        SiteName,                                   //NewAssigned
                                                        SafetyCategory,                             //NewAssigned2
                                                        LocationSponsorUser,                        //NewAssigned3
                                                        Approval)                                   //NewAssigned4
                                                        == false)
                {
                    //for now we do nothing if action log fails.
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void grid_StartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
        {
            int Val = Convert.ToInt32(e.EditingKeyValue);
            DataSet ArpPerson = DataRepository.CompanySiteCategoryStandardProvider.GetArp_PersonId(Val);
            hdnPersonIds.Value = ArpPerson.Tables[0].Rows[0]["ARPPersonId"].ToString();
        }

        protected int GetMemoText()
        {//DT2973:ARP issue
            try
            {
                GridViewDataColumn cl = grid.Columns["CompanyName"] as GridViewDataColumn;
                ASPxComboBox cmbEdit = grid.FindEditRowCellTemplateControl(cl, "cbComp") as ASPxComboBox;

                if (cmbEdit != null)
                {
                    return Convert.ToInt32(cmbEdit.SelectedItem.Value);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }    
}
