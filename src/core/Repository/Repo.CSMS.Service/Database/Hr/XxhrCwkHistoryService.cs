﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database.Hr
{
    #region Interface

    public interface IXxhrCwkHistoryService
    {
        XXHR_CWK_HISTORY Get(Expression<Func<XXHR_CWK_HISTORY, bool>> where, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes);
        List<XXHR_CWK_HISTORY> GetMany(int? top, Expression<Func<XXHR_CWK_HISTORY, bool>> where, Func<IQueryable<XXHR_CWK_HISTORY>, IOrderedQueryable<XXHR_CWK_HISTORY>> orderBy, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes);
        List<XXHR_CWK_HISTORY> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_CWK_HISTORY, bool>> where, Func<IQueryable<XXHR_CWK_HISTORY>, IOrderedQueryable<XXHR_CWK_HISTORY>> orderBy, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<XXHR_CWK_HISTORY, bool>> where, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes);
        int? GetMax(Expression<Func<XXHR_CWK_HISTORY, bool>> where, Expression<Func<XXHR_CWK_HISTORY, int>> selector);

        void Insert(XXHR_CWK_HISTORY item, bool saveChanges = true);
        void Update(XXHR_CWK_HISTORY item, bool saveChanges = true);
        void Delete(XXHR_CWK_HISTORY item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class XxhrCwkHistoryService : IXxhrCwkHistoryService
    {
        private readonly IRepository<XXHR_CWK_HISTORY> _repository;

        public XxhrCwkHistoryService(IRepository<XXHR_CWK_HISTORY> repository)
        {
            this._repository = repository;
        }

        public XXHR_CWK_HISTORY Get(Expression<Func<XXHR_CWK_HISTORY, bool>> where, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<XXHR_CWK_HISTORY> GetMany(int? top, Expression<Func<XXHR_CWK_HISTORY, bool>> where, Func<IQueryable<XXHR_CWK_HISTORY>, IOrderedQueryable<XXHR_CWK_HISTORY>> orderBy, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<XXHR_CWK_HISTORY> GetManyPaged(int pageNumber, int pageSize, Expression<Func<XXHR_CWK_HISTORY, bool>> where, Func<IQueryable<XXHR_CWK_HISTORY>, IOrderedQueryable<XXHR_CWK_HISTORY>> orderBy, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<XXHR_CWK_HISTORY, bool>> where, List<Expression<Func<XXHR_CWK_HISTORY, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public int? GetMax(Expression<Func<XXHR_CWK_HISTORY, bool>> where, Expression<Func<XXHR_CWK_HISTORY, int>> selector)
        {
            return _repository.GetMax(where, selector, null);
        }

        public void Insert(XXHR_CWK_HISTORY item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_CWK_HISTORY");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(XXHR_CWK_HISTORY item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_CWK_HISTORY");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(XXHR_CWK_HISTORY item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("XXHR_CWK_HISTORY");

            _repository.Delete(item);
            //_userRepository.ExecuteCustomFunction("spDeactivateEntity", new ObjectParameter("TableName", typeof(XXHR_CWK_HISTORY).Name), new ObjectParameter("Value", user.ID));

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
