﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationResponseProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationResponseDataSourceDesigner))]
	public class QuestionnaireVerificationResponseDataSource : ProviderDataSource<QuestionnaireVerificationResponse, QuestionnaireVerificationResponseKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseDataSource class.
		/// </summary>
		public QuestionnaireVerificationResponseDataSource() : base(new QuestionnaireVerificationResponseService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationResponseDataSourceView used by the QuestionnaireVerificationResponseDataSource.
		/// </summary>
		protected QuestionnaireVerificationResponseDataSourceView QuestionnaireVerificationResponseView
		{
			get { return ( View as QuestionnaireVerificationResponseDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationResponseDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationResponseSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationResponseSelectMethod selectMethod = QuestionnaireVerificationResponseSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationResponseSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationResponseDataSourceView class that is to be
		/// used by the QuestionnaireVerificationResponseDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationResponseDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationResponse, QuestionnaireVerificationResponseKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationResponseDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationResponseDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationResponseDataSourceView : ProviderDataSourceView<QuestionnaireVerificationResponse, QuestionnaireVerificationResponseKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationResponseDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationResponseDataSourceView(QuestionnaireVerificationResponseDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationResponseDataSource QuestionnaireVerificationResponseOwner
		{
			get { return Owner as QuestionnaireVerificationResponseDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationResponseSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationResponseOwner.SelectMethod; }
			set { QuestionnaireVerificationResponseOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationResponseService QuestionnaireVerificationResponseProvider
		{
			get { return Provider as QuestionnaireVerificationResponseService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationResponse> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationResponse> results = null;
			QuestionnaireVerificationResponse item;
			count = 0;
			
			System.Int32 _responseId;
			System.Int32 _questionnaireId;
			System.Int32 _sectionId;
			System.Int32 _questionId;
			System.Int32 _modifiedByUserId;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationResponseSelectMethod.Get:
					QuestionnaireVerificationResponseKey entityKey  = new QuestionnaireVerificationResponseKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationResponseProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationResponseSelectMethod.GetAll:
                    results = QuestionnaireVerificationResponseProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationResponseSelectMethod.GetPaged:
					results = QuestionnaireVerificationResponseProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationResponseSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationResponseProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationResponseProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationResponseSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationResponseProvider.GetByResponseId(_responseId);
					results = new TList<QuestionnaireVerificationResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationResponseSelectMethod.GetByQuestionnaireIdSectionIdQuestionId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					_questionId = ( values["QuestionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationResponseProvider.GetByQuestionnaireIdSectionIdQuestionId(_questionnaireId, _sectionId, _questionId);
					results = new TList<QuestionnaireVerificationResponse>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireVerificationResponseSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationResponseProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationResponseSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationResponseProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationResponseSelectMethod.Get || SelectMethod == QuestionnaireVerificationResponseSelectMethod.GetByResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationResponse entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationResponseProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationResponse> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationResponseProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationResponseDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationResponseDataSource class.
	/// </summary>
	public class QuestionnaireVerificationResponseDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationResponse, QuestionnaireVerificationResponseKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationResponseDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationResponseSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationResponseDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationResponseDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationResponseDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationResponseDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationResponseDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationResponseDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationResponseDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationResponseDataSourceActionList(QuestionnaireVerificationResponseDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationResponseSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationResponseDataSourceActionList
	
	#endregion QuestionnaireVerificationResponseDataSourceDesigner
	
	#region QuestionnaireVerificationResponseSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationResponseDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationResponseSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdSectionIdQuestionId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion QuestionnaireVerificationResponseSelectMethod

	#region QuestionnaireVerificationResponseFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseFilter : SqlFilter<QuestionnaireVerificationResponseColumn>
	{
	}
	
	#endregion QuestionnaireVerificationResponseFilter

	#region QuestionnaireVerificationResponseExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationResponseColumn>
	{
	}
	
	#endregion QuestionnaireVerificationResponseExpressionBuilder	

	#region QuestionnaireVerificationResponseProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationResponseChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationResponseProperty : ChildEntityProperty<QuestionnaireVerificationResponseChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationResponseProperty
}

