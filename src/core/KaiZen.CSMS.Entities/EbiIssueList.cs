﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'EbiIssue' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table EbiIssue</remark>
	[Serializable]
	public enum EbiIssueList
	{
		/// <summary> 
		/// 1) Yes - Safety Qualification Current / Access to Site Granted
		/// </summary>
		[EnumTextValue(@"1) Yes - Safety Qualification Current / Access to Site Granted")]
		Yes_CurrentSqAndAccessToSite = 1, 

		/// <summary> 
		/// 2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted
		/// </summary>
		[EnumTextValue(@"2) Yes - Safety Qualification Current (expiring in 60 days) / Access to Site Granted")]
		Yes_CurrentSqButExpiringAndAccessToSite = 2, 

		/// <summary> 
		/// 3) No - Current Safety Qualification not found / Access to Site Not Granted
		/// </summary>
		[EnumTextValue(@"3) No - Current Safety Qualification not found / Access to Site Not Granted")]
		No_CurrentSqFoundAndOrAccessToSiteNotGranted = 3, 

		/// <summary> 
		/// 4) No - Safety Qualification Expired / Access to Site Not Granted
		/// </summary>
		[EnumTextValue(@"4) No - Safety Qualification Expired / Access to Site Not Granted")]
		No_ExpiredSqAndOrAccessToSiteNotGranted = 4, 

		/// <summary> 
		/// 5) No - Access to Site Not Granted
		/// </summary>
		[EnumTextValue(@"5) No - Access to Site Not Granted")]
		No_AccessToSiteNotGranted = 5, 

		/// <summary> 
		/// 6) No - Safety Qualification not found
		/// </summary>
		[EnumTextValue(@"6) No - Safety Qualification not found")]
		No_SqNotFound = 6, 

		/// <summary> 
		/// 7) No - Information could not be found (Check that Company exists in CSMS)
		/// </summary>
		[EnumTextValue(@"7) No - Information could not be found (Check that Company exists in CSMS)")]
		No_CompanyNotFound = 7, 

		/// <summary> 
		/// 8) Safety Qualification Exemption Exists
		/// </summary>
		[EnumTextValue(@"8) Safety Qualification Exemption Exists")]
		SqExempt = 8

	}
}
