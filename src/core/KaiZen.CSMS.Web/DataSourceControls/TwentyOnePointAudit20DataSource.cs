﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit20Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit20DataSourceDesigner))]
	public class TwentyOnePointAudit20DataSource : ProviderDataSource<TwentyOnePointAudit20, TwentyOnePointAudit20Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20DataSource class.
		/// </summary>
		public TwentyOnePointAudit20DataSource() : base(new TwentyOnePointAudit20Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit20DataSourceView used by the TwentyOnePointAudit20DataSource.
		/// </summary>
		protected TwentyOnePointAudit20DataSourceView TwentyOnePointAudit20View
		{
			get { return ( View as TwentyOnePointAudit20DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit20DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit20SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit20SelectMethod selectMethod = TwentyOnePointAudit20SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit20SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit20DataSourceView class that is to be
		/// used by the TwentyOnePointAudit20DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit20DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit20, TwentyOnePointAudit20Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit20DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit20DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit20DataSourceView : ProviderDataSourceView<TwentyOnePointAudit20, TwentyOnePointAudit20Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit20DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit20DataSourceView(TwentyOnePointAudit20DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit20DataSource TwentyOnePointAudit20Owner
		{
			get { return Owner as TwentyOnePointAudit20DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit20SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit20Owner.SelectMethod; }
			set { TwentyOnePointAudit20Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit20Service TwentyOnePointAudit20Provider
		{
			get { return Provider as TwentyOnePointAudit20Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit20> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit20> results = null;
			TwentyOnePointAudit20 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit20SelectMethod.Get:
					TwentyOnePointAudit20Key entityKey  = new TwentyOnePointAudit20Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit20Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit20>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit20SelectMethod.GetAll:
                    results = TwentyOnePointAudit20Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit20SelectMethod.GetPaged:
					results = TwentyOnePointAudit20Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit20SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit20Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit20Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit20SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit20Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit20>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit20SelectMethod.Get || SelectMethod == TwentyOnePointAudit20SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit20 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit20Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit20> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit20Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit20DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit20DataSource class.
	/// </summary>
	public class TwentyOnePointAudit20DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit20, TwentyOnePointAudit20Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit20DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit20SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit20DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit20DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit20DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit20DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit20DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit20DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit20DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit20DataSourceActionList(TwentyOnePointAudit20DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit20SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit20DataSourceActionList
	
	#endregion TwentyOnePointAudit20DataSourceDesigner
	
	#region TwentyOnePointAudit20SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit20DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit20SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit20SelectMethod

	#region TwentyOnePointAudit20Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit20"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit20Filter : SqlFilter<TwentyOnePointAudit20Column>
	{
	}
	
	#endregion TwentyOnePointAudit20Filter

	#region TwentyOnePointAudit20ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit20"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit20ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit20Column>
	{
	}
	
	#endregion TwentyOnePointAudit20ExpressionBuilder	

	#region TwentyOnePointAudit20Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit20ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit20"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit20Property : ChildEntityProperty<TwentyOnePointAudit20ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit20Property
}

