﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class UserControls_Other_userProfile2 : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            if (auth.Role != null) { LoadUserInfo(); LogUser(); }
            else { Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unauthorized user")); }
        }
    }

    protected void btnEdit_Click(object sender, EventArgs e) { EditMode(true); }
    protected void btnCancel_Click(object sender, EventArgs e) { EditMode(false); }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ValidatedPageOk())
        {
            UsersService usersService = new UsersService();
            Users user = new Users();
            user = usersService.GetByUserId(auth.UserId);
            user.UserId = auth.UserId;
            user.Title = tbTitle.Text;
            user.Email = tbEmail.Text;
            user.FirstName = tbFirstName.Text;
            user.LastName = tbLastName.Text;
            user.ModifiedByUserId = auth.UserId;
            try
            {
                usersService.Save(user);
                EditMode(false);
                lblTitle.Text = user.Title;
                lblName.Text = String.Format("{0} {1}", user.FirstName, user.LastName);
                hlEmail.Text = user.Email;
                if (hlEmail.Text.Length > 23)
                {
                    hlEmail.Text = hlEmail.Text.Remove(23, hlEmail.Text.Length - 23) + "...";
                    hlEmail.ToolTip = auth.Email;
                }
                hlEmail.NavigateUrl = "mailto:" + user.Email;
                user.Dispose();
            }
            catch (Exception ex)
            {
                //lblError.Text = "User Profile not saved. Error Details: " + ex.Message.ToString();
            }
        }
    }

    protected bool ValidatedPageOk()
    {
        bool valid = true;

        if (String.IsNullOrEmpty(tbFirstName.Text)) valid = false;
        if (String.IsNullOrEmpty(tbLastName.Text)) valid = false;
        if (String.IsNullOrEmpty(tbTitle.Text)) valid = false;
        if (String.IsNullOrEmpty(tbEmail.Text)) valid = false;

        return valid;
    }

    protected void EditMode(bool setting)
    {
        if (setting == true)
        {
            LoadUserInfo();
            lblName.Visible = false;
            tbFirstName.Visible = true;
            tbLastName.Visible = true;

            lblTitle.Visible = false;
            tbTitle.Visible = true;

            hlEmail.Visible = false;
            tbEmail.Visible = true;

            btnEdit.Enabled = false;
            btnSave.Enabled = true;
            btnCancel.Enabled = true;
            
            btnEdit.ForeColor = System.Drawing.Color.Silver;
            btnSave.ForeColor = System.Drawing.Color.Red;
            btnCancel.ForeColor = System.Drawing.Color.Red; 
        }

        if (setting == false)
        {
            lblName.Visible = true;
            tbFirstName.Visible = false;
            tbLastName.Visible = false;

            lblTitle.Visible = true;
            tbTitle.Visible = false;

            hlEmail.Visible = true;
            tbEmail.Visible = false;

            btnEdit.Enabled = true;
            btnCancel.Enabled = false;
            btnSave.Enabled = false;

            btnEdit.ForeColor = System.Drawing.Color.Red;
            btnSave.ForeColor = System.Drawing.Color.Silver;
            btnCancel.ForeColor = System.Drawing.Color.Silver;
        }
    }

    protected void LoadUserInfo()
    {
        ClearTextBoxLabels();
        lblName.Text = String.Format("{0} {1}", auth.FirstName, auth.LastName);
        tbFirstName.Text = auth.FirstName;
        tbLastName.Text = auth.LastName;

        lblCompany.Text = auth.CompanyName;
        if (lblCompany.Text.Length > 23)
        {
            lblCompany.Text = lblCompany.Text.Remove(23, lblCompany.Text.Length - 23) + "...";
            lblCompany.ToolTip = auth.CompanyName;
        }

        UsersService usersService = new UsersService();
        Users userEntity = usersService.GetByUserId(auth.UserId);

        if (userEntity.Title == null) { lblTitle.Text = "-"; } else { lblTitle.Text = userEntity.Title; }
        tbTitle.Text = userEntity.Title;
        hlEmail.Text = auth.Email;
        hlEmail.ToolTip = auth.Email;
        if (hlEmail.Text.Length > 23)
        {
            hlEmail.Text = hlEmail.Text.Remove(23, hlEmail.Text.Length - 23) + "...";
            hlEmail.ToolTip = auth.Email;
        }
        hlEmail.NavigateUrl = "mailto:" + auth.Email;
        tbEmail.Text = auth.Email; 
    }

    protected void LogUser()
    {
        //log user...
        try
        {
            if (!String.IsNullOrEmpty(SessionHandler.LoggedIn))
            {
                if (SessionHandler.LoggedIn == "Yes")
                {
                    if (SessionHandler.AccessLogged != "Yes")
                    {
                        using (AccessLogs accessLogs = new AccessLogs())
                        {
                            accessLogs.UserId = auth.UserId;
                            accessLogs.LogDateTime = DateTime.Now;
                            AccessLogsService accessLogsService = new AccessLogsService();
                            accessLogsService.Insert(accessLogs);
                        }
                        SessionHandler.AccessLogged = "Yes";
                    }
                }
            }
        }
        catch (Exception)
        {
            //ToDo: Handle Exception
        }
    }
    protected void ClearTextBoxLabels()
    {
        lblName.Text = "";
        tbFirstName.Text = "";
        tbLastName.Text = "";

        lblTitle.Text = "";
        tbTitle.Text = "";

        lblCompany.Text = "";
        
        hlEmail.Text = "";
        tbEmail.Text = "";

        //lblError.Text = "";
    }

    //DT228 - new properties to allow form to be read only
    public bool EnableBtnEdit
    {
        get { return btnEdit.Enabled; }
        set { btnEdit.Enabled = value; }
    }

    public bool EnableEmailLink
    {
        get { return hlEmail.Enabled; }
        set { hlEmail.Enabled = value; }
    }
}
