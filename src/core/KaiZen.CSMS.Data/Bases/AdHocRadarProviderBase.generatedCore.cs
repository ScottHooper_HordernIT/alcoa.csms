﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AdHocRadarProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AdHocRadarProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.AdHocRadar, KaiZen.CSMS.Entities.AdHocRadarKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarKey key)
		{
			return Delete(transactionManager, key.AdHocRadarId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_adHocRadarId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _adHocRadarId)
		{
			return Delete(null, _adHocRadarId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _adHocRadarId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		FK_AdHoc_Radar_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		public TList<AdHocRadar> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(_companySiteCategoryId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		FK_AdHoc_Radar_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		/// <remarks></remarks>
		public TList<AdHocRadar> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		FK_AdHoc_Radar_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		public TList<AdHocRadar> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryId(transactionManager, _companySiteCategoryId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		fkAdHocRadarCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		public TList<AdHocRadar> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		fkAdHocRadarCompanySiteCategory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		public TList<AdHocRadar> GetByCompanySiteCategoryId(System.Int32 _companySiteCategoryId, int start, int pageLength,out int count)
		{
			return GetByCompanySiteCategoryId(null, _companySiteCategoryId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_AdHoc_Radar_CompanySiteCategory key.
		///		FK_AdHoc_Radar_CompanySiteCategory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.AdHocRadar objects.</returns>
		public abstract TList<AdHocRadar> GetByCompanySiteCategoryId(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.AdHocRadar Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadarKey key, int start, int pageLength)
		{
			return GetByAdHocRadarId(transactionManager, key.AdHocRadarId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="_adHocRadarId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(null,_adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength)
		{
			int count = -1;
			return GetByAdHocRadarId(transactionManager, _adHocRadarId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(System.Int32 _adHocRadarId, int start, int pageLength, out int count)
		{
			return GetByAdHocRadarId(null, _adHocRadarId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_adHocRadarId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadar GetByAdHocRadarId(TransactionManager transactionManager, System.Int32 _adHocRadarId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(System.Int32 _companySiteCategoryId, System.String _kpiName)
		{
			int count = -1;
			return GetByCompanySiteCategoryIdKpiName(null,_companySiteCategoryId, _kpiName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(System.Int32 _companySiteCategoryId, System.String _kpiName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryIdKpiName(null, _companySiteCategoryId, _kpiName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, System.String _kpiName)
		{
			int count = -1;
			return GetByCompanySiteCategoryIdKpiName(transactionManager, _companySiteCategoryId, _kpiName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, System.String _kpiName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanySiteCategoryIdKpiName(transactionManager, _companySiteCategoryId, _kpiName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(System.Int32 _companySiteCategoryId, System.String _kpiName, int start, int pageLength, out int count)
		{
			return GetByCompanySiteCategoryIdKpiName(null, _companySiteCategoryId, _kpiName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_AdHoc_Radar index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companySiteCategoryId"></param>
		/// <param name="_kpiName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.AdHocRadar GetByCompanySiteCategoryIdKpiName(TransactionManager transactionManager, System.Int32 _companySiteCategoryId, System.String _kpiName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AdHocRadar&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AdHocRadar&gt;"/></returns>
		public static TList<AdHocRadar> Fill(IDataReader reader, TList<AdHocRadar> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.AdHocRadar c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AdHocRadar")
					.Append("|").Append((System.Int32)reader[((int)AdHocRadarColumn.AdHocRadarId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AdHocRadar>(
					key.ToString(), // EntityTrackingKey
					"AdHocRadar",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.AdHocRadar();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarColumn.AdHocRadarId - 1)];
					c.CompanySiteCategoryId = (System.Int32)reader[((int)AdHocRadarColumn.CompanySiteCategoryId - 1)];
					c.KpiOrdinal = (System.Int32)reader[((int)AdHocRadarColumn.KpiOrdinal - 1)];
					c.KpiName = (System.String)reader[((int)AdHocRadarColumn.KpiName - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.AdHocRadar entity)
		{
			if (!reader.Read()) return;
			
			entity.AdHocRadarId = (System.Int32)reader[((int)AdHocRadarColumn.AdHocRadarId - 1)];
			entity.CompanySiteCategoryId = (System.Int32)reader[((int)AdHocRadarColumn.CompanySiteCategoryId - 1)];
			entity.KpiOrdinal = (System.Int32)reader[((int)AdHocRadarColumn.KpiOrdinal - 1)];
			entity.KpiName = (System.String)reader[((int)AdHocRadarColumn.KpiName - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)AdHocRadarColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.AdHocRadar entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AdHocRadarId = (System.Int32)dataRow["AdHoc_RadarId"];
			entity.CompanySiteCategoryId = (System.Int32)dataRow["CompanySiteCategoryId"];
			entity.KpiOrdinal = (System.Int32)dataRow["KpiOrdinal"];
			entity.KpiName = (System.String)dataRow["KpiName"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.AdHocRadar"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadar Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadar entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CompanySiteCategoryIdSource	
			if (CanDeepLoad(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepLoadType, innerList) 
				&& entity.CompanySiteCategoryIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CompanySiteCategoryId;
				CompanySiteCategory tmpEntity = EntityManager.LocateEntity<CompanySiteCategory>(EntityLocator.ConstructKeyFromPkItems(typeof(CompanySiteCategory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CompanySiteCategoryIdSource = tmpEntity;
				else
					entity.CompanySiteCategoryIdSource = DataRepository.CompanySiteCategoryProvider.GetByCompanySiteCategoryId(transactionManager, entity.CompanySiteCategoryId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompanySiteCategoryIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CompanySiteCategoryIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompanySiteCategoryProvider.DeepLoad(transactionManager, entity.CompanySiteCategoryIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CompanySiteCategoryIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByAdHocRadarId methods when available
			
			#region AdHocRadarItems2Collection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarItems2Collection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarItems2Collection = DataRepository.AdHocRadarItems2Provider.GetByAdHocRadarId(transactionManager, entity.AdHocRadarId);

				if (deep && entity.AdHocRadarItems2Collection.Count > 0)
				{
					deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadarItems2>) DataRepository.AdHocRadarItems2Provider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarItems2Collection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AdHocRadarItemsCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AdHocRadarItems>|AdHocRadarItemsCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AdHocRadarItemsCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AdHocRadarItemsCollection = DataRepository.AdHocRadarItemsProvider.GetByAdHocRadarId(transactionManager, entity.AdHocRadarId);

				if (deep && entity.AdHocRadarItemsCollection.Count > 0)
				{
					deepHandles.Add("AdHocRadarItemsCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AdHocRadarItems>) DataRepository.AdHocRadarItemsProvider.DeepLoad,
						new object[] { transactionManager, entity.AdHocRadarItemsCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.AdHocRadar object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.AdHocRadar instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.AdHocRadar Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.AdHocRadar entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CompanySiteCategoryIdSource
			if (CanDeepSave(entity, "CompanySiteCategory|CompanySiteCategoryIdSource", deepSaveType, innerList) 
				&& entity.CompanySiteCategoryIdSource != null)
			{
				DataRepository.CompanySiteCategoryProvider.Save(transactionManager, entity.CompanySiteCategoryIdSource);
				entity.CompanySiteCategoryId = entity.CompanySiteCategoryIdSource.CompanySiteCategoryId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<AdHocRadarItems2>
				if (CanDeepSave(entity.AdHocRadarItems2Collection, "List<AdHocRadarItems2>|AdHocRadarItems2Collection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadarItems2 child in entity.AdHocRadarItems2Collection)
					{
						if(child.AdHocRadarIdSource != null)
						{
							child.AdHocRadarId = child.AdHocRadarIdSource.AdHocRadarId;
						}
						else
						{
							child.AdHocRadarId = entity.AdHocRadarId;
						}

					}

					if (entity.AdHocRadarItems2Collection.Count > 0 || entity.AdHocRadarItems2Collection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarItems2Provider.Save(transactionManager, entity.AdHocRadarItems2Collection);
						
						deepHandles.Add("AdHocRadarItems2Collection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadarItems2 >) DataRepository.AdHocRadarItems2Provider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarItems2Collection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AdHocRadarItems>
				if (CanDeepSave(entity.AdHocRadarItemsCollection, "List<AdHocRadarItems>|AdHocRadarItemsCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AdHocRadarItems child in entity.AdHocRadarItemsCollection)
					{
						if(child.AdHocRadarIdSource != null)
						{
							child.AdHocRadarId = child.AdHocRadarIdSource.AdHocRadarId;
						}
						else
						{
							child.AdHocRadarId = entity.AdHocRadarId;
						}

					}

					if (entity.AdHocRadarItemsCollection.Count > 0 || entity.AdHocRadarItemsCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AdHocRadarItemsProvider.Save(transactionManager, entity.AdHocRadarItemsCollection);
						
						deepHandles.Add("AdHocRadarItemsCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AdHocRadarItems >) DataRepository.AdHocRadarItemsProvider.DeepSave,
							new object[] { transactionManager, entity.AdHocRadarItemsCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AdHocRadarChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.AdHocRadar</c>
	///</summary>
	public enum AdHocRadarChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CompanySiteCategory</c> at CompanySiteCategoryIdSource
		///</summary>
		[ChildEntityType(typeof(CompanySiteCategory))]
		CompanySiteCategory,
	
		///<summary>
		/// Collection of <c>AdHocRadar</c> as OneToMany for AdHocRadarItems2Collection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadarItems2>))]
		AdHocRadarItems2Collection,

		///<summary>
		/// Collection of <c>AdHocRadar</c> as OneToMany for AdHocRadarItemsCollection
		///</summary>
		[ChildEntityType(typeof(TList<AdHocRadarItems>))]
		AdHocRadarItemsCollection,
	}
	
	#endregion AdHocRadarChildEntityTypes
	
	#region AdHocRadarFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AdHocRadarColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarFilterBuilder : SqlFilterBuilder<AdHocRadarColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilterBuilder class.
		/// </summary>
		public AdHocRadarFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarFilterBuilder
	
	#region AdHocRadarParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AdHocRadarColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarParameterBuilder : ParameterizedSqlFilterBuilder<AdHocRadarColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarParameterBuilder class.
		/// </summary>
		public AdHocRadarParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AdHocRadarParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AdHocRadarParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AdHocRadarParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AdHocRadarParameterBuilder
	
	#region AdHocRadarSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AdHocRadarColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadar"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AdHocRadarSortBuilder : SqlSortBuilder<AdHocRadarColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarSqlSortBuilder class.
		/// </summary>
		public AdHocRadarSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AdHocRadarSortBuilder
	
} // end namespace
