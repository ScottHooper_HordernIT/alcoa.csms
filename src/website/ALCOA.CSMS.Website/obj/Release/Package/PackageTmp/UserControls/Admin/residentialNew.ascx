﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="residentialNew.ascx.cs" Inherits="residentialNew" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>



<script type="text/javascript">
    // <![CDATA[
    var textSeparator = "; ";
    var IdSeparator = ", ";
    function OnListBoxSelectionChanged(listBox, args) {
        //if (args.index == 0)
        //args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
        UpdateSelectAllItemState();
        UpdateText();
    }
    function UpdateSelectAllItemState() {
        IsAllSelected() //? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
    }
    function IsAllSelected() {
        var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
        return checkListBox.GetSelectedItems().length == selectedDataItemCount;
    }
    function UpdateText() {
        var selectedItems = checkListBox.GetSelectedItems();
        var values = GetSelectedItemsValue(selectedItems);
        document.getElementById("ctl00_body_ctl00_hdnPersonIds").value = values;
        checkComboBox.SetText(GetSelectedItemsText(selectedItems));
    }
    function SynchronizeListBoxValues(dropDown, args) {
        checkListBox.UnselectAll();
        var texts = dropDown.GetText().split(textSeparator);
        var values = GetValuesByTexts(texts);
        checkListBox.SelectValues(values);
        UpdateSelectAllItemState();
        UpdateText(); // for remove non-existing texts
    }
    function GetSelectedItemsText(items) {
        var texts = [];
        for (var i = 0; i < items.length; i++)
        //if (items[i].index != 0)
            texts.push(items[i].text);
        return texts.join(textSeparator);
    }
    function GetSelectedItemsValue(items) {
        var values = [];
        var texts = [];
        for (var i = 0; i < items.length; i++)
            texts.push(items[i].text);
        var values = GetValuesByTexts(texts);
        return values.join(IdSeparator);
    }
    function GetValuesByTexts(texts) {
        var actualValues = [];
        var item;
        for (var i = 0; i < texts.length; i++) {
            item = checkListBox.FindItemByText(texts[i]);
            if (item != null)
                actualValues.push(item.value);
        }
        return actualValues;
    }
    // ]]>
    </script>

<input type="hidden" runat="server" id="hdnPersonIds" />

<dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
     KeyFieldName="CompanySiteCategoryStandardId" DataSourceID ="dsCompanySiteCategoryStandard"
    OnCellEditorInitialize="gridStandard_CellEditorInitialize" Width="900px" 
     OnRowUpdating="grid_RowUpdating" OnRowInserting="grid_RowInserting"
    >
    
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="130px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dxwgv:GridViewCommandColumn>

        <dxwgv:GridViewDataTextColumn FieldName="CompanySiteCategoryStandardId" VisibleIndex="0"
            ReadOnly="True" Visible="False">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <%--DT2973:ARP issue--%>
        <dxwgv:GridViewDataTextColumn FieldName="CompanyId" VisibleIndex="1"
            ReadOnly="True" Visible="False">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>

        <%--<dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" VisibleIndex="1"
            SortIndex="1" SortOrder="Ascending" Visible="false">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" TextField="CompanyName" ValueField="CompanyId"
                ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>            
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>--%>        
        <dxwgv:GridViewDataTextColumn Caption="Company" name="CompanyName" FieldName="CompanyName" VisibleIndex="2"
            ReadOnly="false" Visible="True" SortIndex="1" SortOrder="Ascending">
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
            <EditItemTemplate>
            <dxe:ASPxComboBox ID="cbComp" runat="server" DataSourceID="sqldsCompaniesList"  TextField="CompanyName" ValueField="CompanyId" ValueType="System.Int32" Value='<%# Eval("CompanyName")%>' IncrementalFilteringMode="StartsWith" Width="100%" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"  SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxComboBox>
            </EditItemTemplate>
        </dxwgv:GridViewDataTextColumn>
        

        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="3"
            SortIndex="2" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="dsSitesFilter" TextField="SiteName" ValueField="SiteId"
                ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>            
            <Settings SortMode="DisplayText" AllowHeaderFilter="False" />
        </dxwgv:GridViewDataComboBoxColumn>

        <dxwgv:GridViewDataComboBoxColumn Caption="Residential Category" FieldName="CompanySiteCategoryId"
            VisibleIndex="4" SortOrder="Ascending">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" TextField="CategoryDesc"
                ValueField="CompanySiteCategoryId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">            
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Location Sponsor" FieldName="LocationSponsorUserId"
            VisibleIndex="5">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="sqlUsersAlcoan2"
                TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>            
            <Settings SortMode="DisplayText" AllowHeaderFilter="False"/>
        </dxwgv:GridViewDataComboBoxColumn>
         <%--<dxwgv:GridViewDataTextColumn Caption="LocationSponsor UserId" Name="LocationSponsorUserId" FieldName="LocationSponsorUserId" VisibleIndex="4">
         
         </dxwgv:GridViewDataTextColumn>--%>
        <dxwgv:GridViewDataComboBoxColumn FieldName="Approved" VisibleIndex="6">
            <PropertiesComboBox ValueType="System.Int32" NullDisplayText="Tentative">
                <Items>
                    <dxe:ListEditItem Text="Yes" Value="1"></dxe:ListEditItem>
                    <dxe:ListEditItem Text="No" Value="0"></dxe:ListEditItem>
                    <%--<dxe:ListEditItem Text="Tentative" Value=""></dxe:ListEditItem>--%>
                </Items>
            </PropertiesComboBox>
            <Settings AllowHeaderFilter="False" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Caption="CRP" Name="ARP" FieldName="ARPName" VisibleIndex="7">
            <Settings AllowHeaderFilter="False" />
            <EditItemTemplate>
            <dx:ASPxDropDownEdit ClientInstanceName="checkComboBox" ID="ASPxDropDownEdit1" Width="125px" runat="server" EnableAnimation="False"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                         CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                         Text='<%# Bind("ARPName") %>'>
        <DropDownWindowStyle BackColor="#EDEDED" />
        <DropDownWindowTemplate>
            
            <%--DT2973:ARP issue code here 300%--%>
               <dx:ASPxListBox Width="300px" ID="listBox" ClientInstanceName="checkListBox" SelectionMode="CheckColumn"
              DataSourceID="dsGetValidARPddl" TextField="Full_Name" ValueField="Person_id"
               runat="server" ItemStyle-Wrap="True"
              >
                
                <Border BorderStyle="None" />
                <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                <%--<Items>
                    <dx:ListEditItem Text="(Select all)" />
                    <dx:ListEditItem Text="Chrome" Value="1" />
                    <dx:ListEditItem Text="Firefox" Value="2" />
                    <dx:ListEditItem Text="IE" Value="3" />
                    <dx:ListEditItem Text="Opera" Value="4" />
                    <dx:ListEditItem Text="Safari" Value="5" />
                </Items>--%>
                <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
            </dx:ASPxListBox>
            <table style="width: 200px" cellspacing="0" cellpadding="4">
                <tr>
                    <td align="right">
                        <dx:ASPxButton ID="ASPxButton1" AutoPostBack="False" runat="server" Text="Close"
                         CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                         CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <ClientSideEvents Click="function(s, e){ checkComboBox.HideDropDown(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </DropDownWindowTemplate>
        <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
    </dx:ASPxDropDownEdit>

            
            </EditItemTemplate>
        </dxwgv:GridViewDataTextColumn>
        <%--<dxwgv:GridViewDataComboBoxColumn Caption="CRP" FieldName="CrpUserId" VisibleIndex="7">
            <PropertiesComboBox IncrementalFilteringMode="StartsWith" DataSourceID="UsersCompaniesActiveContractorsDataSource"
                TextField="CompanyUserFullNameLogon" ValueField="UserId" ValueType="System.Int32" DropDownHeight="150px">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>--%>
    </Columns>
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
    <SettingsPager>
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <SettingsEditing Mode="Inline" />
    <Settings ShowFilterBar="Visible" ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="true" />
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px">
        </CollapsedButton>
        <ExpandedButton Height="12px" Width="11px">
        </ExpandedButton>
        <DetailCollapsedButton Height="12px" Width="11px">
        </DetailCollapsedButton>
        <DetailExpandedButton Height="12px" Width="11px">
        </DetailExpandedButton>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
</dxwgv:ASPxGridView>

<br />
<dx:ASPxHyperLink ID="ASPxHyperLink1" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" runat="server" Text="View Residential Category Exceptions List"
    NavigateUrl="javascript:popUp('SafetyComplianceCategoryExceptions.aspx?Help=Default');">
</dx:ASPxHyperLink>

<br />
<dx:ASPxHyperLink ID="hlArpCrp" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" runat="server" Text="View Alcoa Responsible Persons (ARPs – Alcoa Employees) listing"
    NavigateUrl="javascript:popUp('ViewArpListing.aspx?Help=Default');">
</dx:ASPxHyperLink>


<asp:SqlDataSource ID="dsCompanySiteCategoryStandard" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="get_CompanySiteCategoryStandard_ARP" SelectCommandType="StoredProcedure"
     DeleteCommand="CompanySiteCategoryStandardId_Delete" DeleteCommandType="StoredProcedure">
     <DeleteParameters>
      <asp:Parameter Name="CompanySiteCategoryStandardId" Type="Int32" />
    </DeleteParameters>
</asp:SqlDataSource>

<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetNotDeactivatedCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>


<asp:SqlDataSource ID="dsSitesFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="getAllSite" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>



<asp:SqlDataSource ID="CompanySiteCategoryDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_CompanySiteCategory_SiteCategory" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

 <asp:SqlDataSource ID="sqlUsersAlcoan2" runat="server"
     ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand=" SELECT  
     [UserId],  
     [UserFullName],  
     [UserFullNameLogon],  
     [Email]  
    FROM  
     [dbo].[UsersAlcoan] Order By  [UserFullNameLogon]" SelectCommandType="Text">
</asp:SqlDataSource>


<asp:SqlDataSource ID="dsGetValidARPddl" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Get_Valid_ARP_DATASOURCE" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
