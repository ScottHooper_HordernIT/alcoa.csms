﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainAttachmentProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainAttachmentProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainAttachment, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentKey key)
		{
			return Delete(transactionManager, key.AttachmentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_attachmentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _attachmentId)
		{
			return Delete(null, _attachmentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attachmentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _attachmentId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		FK_QuestionnaireMainAttachment_Users Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		FK_QuestionnaireMainAttachment_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainAttachment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		FK_QuestionnaireMainAttachment_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		fkQuestionnaireMainAttachmentUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		fkQuestionnaireMainAttachmentUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
		///		FK_QuestionnaireMainAttachment_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public abstract TList<QuestionnaireMainAttachment> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainAttachment_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(_answerId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainAttachment_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainAttachment> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainAttachment_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		fkQuestionnaireMainAttachmentQuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAnswerId(null, _answerId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		fkQuestionnaireMainAttachmentQuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public TList<QuestionnaireMainAttachment> GetByAnswerId(System.Int32 _answerId, int start, int pageLength,out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainAttachment_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainAttachment objects.</returns>
		public abstract TList<QuestionnaireMainAttachment> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainAttachment Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachmentKey key, int start, int pageLength)
		{
			return GetByAttachmentId(transactionManager, key.AttachmentId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_attachmentId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(System.Int32 _attachmentId)
		{
			int count = -1;
			return GetByAttachmentId(null,_attachmentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_attachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(System.Int32 _attachmentId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttachmentId(null, _attachmentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attachmentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(TransactionManager transactionManager, System.Int32 _attachmentId)
		{
			int count = -1;
			return GetByAttachmentId(transactionManager, _attachmentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(TransactionManager transactionManager, System.Int32 _attachmentId, int start, int pageLength)
		{
			int count = -1;
			return GetByAttachmentId(transactionManager, _attachmentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_attachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(System.Int32 _attachmentId, int start, int pageLength, out int count)
		{
			return GetByAttachmentId(null, _attachmentId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_attachmentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByAttachmentId(TransactionManager transactionManager, System.Int32 _attachmentId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null,_questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainAttachment GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachment> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachment> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public TList<QuestionnaireMainAttachment> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
		public abstract TList<QuestionnaireMainAttachment> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainAttachment&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/></returns>
		public static TList<QuestionnaireMainAttachment> Fill(IDataReader reader, TList<QuestionnaireMainAttachment> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainAttachment c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainAttachment")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.AttachmentId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainAttachment>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainAttachment",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainAttachment();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AttachmentId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.AttachmentId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.QuestionnaireId - 1)];
					c.AnswerId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.AnswerId - 1)];
					c.FileName = (System.String)reader[((int)QuestionnaireMainAttachmentColumn.FileName - 1)];
					c.FileHash = (System.Byte[])reader[((int)QuestionnaireMainAttachmentColumn.FileHash - 1)];
					c.ContentLength = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.ContentLength - 1)];
					c.Content = (System.Byte[])reader[((int)QuestionnaireMainAttachmentColumn.Content - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireMainAttachmentColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainAttachment entity)
		{
			if (!reader.Read()) return;
			
			entity.AttachmentId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.AttachmentId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.QuestionnaireId - 1)];
			entity.AnswerId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.AnswerId - 1)];
			entity.FileName = (System.String)reader[((int)QuestionnaireMainAttachmentColumn.FileName - 1)];
			entity.FileHash = (System.Byte[])reader[((int)QuestionnaireMainAttachmentColumn.FileHash - 1)];
			entity.ContentLength = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.ContentLength - 1)];
			entity.Content = (System.Byte[])reader[((int)QuestionnaireMainAttachmentColumn.Content - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireMainAttachmentColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireMainAttachmentColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainAttachment entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AttachmentId = (System.Int32)dataRow["AttachmentId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.AnswerId = (System.Int32)dataRow["AnswerId"];
			entity.FileName = (System.String)dataRow["FileName"];
			entity.FileHash = (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = (System.Int32)dataRow["ContentLength"];
			entity.Content = (System.Byte[])dataRow["Content"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainAttachment"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAttachment Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachment entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region AnswerIdSource	
			if (CanDeepLoad(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepLoadType, innerList) 
				&& entity.AnswerIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AnswerId;
				QuestionnaireMainAnswer tmpEntity = EntityManager.LocateEntity<QuestionnaireMainAnswer>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireMainAnswer), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AnswerIdSource = tmpEntity;
				else
					entity.AnswerIdSource = DataRepository.QuestionnaireMainAnswerProvider.GetByAnswerId(transactionManager, entity.AnswerId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnswerIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AnswerIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireMainAnswerProvider.DeepLoad(transactionManager, entity.AnswerIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AnswerIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainAttachment object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainAttachment instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainAttachment Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainAttachment entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region AnswerIdSource
			if (CanDeepSave(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepSaveType, innerList) 
				&& entity.AnswerIdSource != null)
			{
				DataRepository.QuestionnaireMainAnswerProvider.Save(transactionManager, entity.AnswerIdSource);
				entity.AnswerId = entity.AnswerIdSource.AnswerId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainAttachmentChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainAttachment</c>
	///</summary>
	public enum QuestionnaireMainAttachmentChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireMainAnswer</c> at AnswerIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireMainAnswer))]
		QuestionnaireMainAnswer,
		}
	
	#endregion QuestionnaireMainAttachmentChildEntityTypes
	
	#region QuestionnaireMainAttachmentFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentFilterBuilder : SqlFilterBuilder<QuestionnaireMainAttachmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentFilterBuilder
	
	#region QuestionnaireMainAttachmentParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainAttachmentParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainAttachmentColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainAttachmentParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainAttachmentParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainAttachmentParameterBuilder
	
	#region QuestionnaireMainAttachmentSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainAttachmentSortBuilder : SqlSortBuilder<QuestionnaireMainAttachmentColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainAttachmentSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainAttachmentSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainAttachmentSortBuilder
	
} // end namespace
