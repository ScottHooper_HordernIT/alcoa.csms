﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IContractReviewsRegisterService
    {
        ContractReviewsRegister Get(Expression<Func<ContractReviewsRegister, bool>> where, List<Expression<Func<ContractReviewsRegister, object>>> includes);
        List<ContractReviewsRegister> GetMany(int? top, Expression<Func<ContractReviewsRegister, bool>> where, Func<IQueryable<ContractReviewsRegister>, IOrderedQueryable<ContractReviewsRegister>> orderBy, List<Expression<Func<ContractReviewsRegister, object>>> includes);
        List<ContractReviewsRegister> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContractReviewsRegister, bool>> where, Func<IQueryable<ContractReviewsRegister>, IOrderedQueryable<ContractReviewsRegister>> orderBy, List<Expression<Func<ContractReviewsRegister, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<ContractReviewsRegister, bool>> where, List<Expression<Func<ContractReviewsRegister, object>>> includes);
        void Insert(ContractReviewsRegister item, bool saveChanges = true);
        void Update(ContractReviewsRegister item, bool saveChanges = true);
        void Delete(ContractReviewsRegister item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ContractReviewsRegisterService : IContractReviewsRegisterService
    {
        private readonly IRepository<ContractReviewsRegister> _repository;

        public ContractReviewsRegisterService(IRepository<ContractReviewsRegister> repository)
        {
            this._repository = repository;
        }

        public ContractReviewsRegister Get(Expression<Func<ContractReviewsRegister, bool>> where, List<Expression<Func<ContractReviewsRegister, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<ContractReviewsRegister> GetMany(int? top, Expression<Func<ContractReviewsRegister, bool>> where, Func<IQueryable<ContractReviewsRegister>, IOrderedQueryable<ContractReviewsRegister>> orderBy, List<Expression<Func<ContractReviewsRegister, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<ContractReviewsRegister> GetManyPaged(int pageNumber, int pageSize, Expression<Func<ContractReviewsRegister, bool>> where, Func<IQueryable<ContractReviewsRegister>, IOrderedQueryable<ContractReviewsRegister>> orderBy, List<Expression<Func<ContractReviewsRegister, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<ContractReviewsRegister, bool>> where, List<Expression<Func<ContractReviewsRegister, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(ContractReviewsRegister item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContractReviewsRegister");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(ContractReviewsRegister item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContractReviewsRegister");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(ContractReviewsRegister item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("ContractReviewsRegister");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
