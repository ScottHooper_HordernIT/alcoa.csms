﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IPermissionService
    {
        Permission Get(Expression<Func<Permission, bool>> where, List<Expression<Func<Permission, object>>> includes);
        List<Permission> GetMany(int? top, Expression<Func<Permission, bool>> where, Func<IQueryable<Permission>, IOrderedQueryable<Permission>> orderBy, List<Expression<Func<Permission, object>>> includes);
        List<Permission> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Permission, bool>> where, Func<IQueryable<Permission>, IOrderedQueryable<Permission>> orderBy, List<Expression<Func<Permission, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Permission, bool>> where, List<Expression<Func<Permission, object>>> includes);
        void Insert(Permission item, bool saveChanges = true);
        void Update(Permission item, bool saveChanges = true);
        void Delete(Permission item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class PermissionService : IPermissionService
    {
        private readonly IRepository<Permission> _repository;

        public PermissionService(IRepository<Permission> repository)
        {
            this._repository = repository;
        }

        public Permission Get(Expression<Func<Permission, bool>> where, List<Expression<Func<Permission, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Permission> GetMany(int? top, Expression<Func<Permission, bool>> where, Func<IQueryable<Permission>, IOrderedQueryable<Permission>> orderBy, List<Expression<Func<Permission, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Permission> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Permission, bool>> where, Func<IQueryable<Permission>, IOrderedQueryable<Permission>> orderBy, List<Expression<Func<Permission, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Permission, bool>> where, List<Expression<Func<Permission, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Permission item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Permission");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Permission item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Permission");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Permission item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Permission");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
