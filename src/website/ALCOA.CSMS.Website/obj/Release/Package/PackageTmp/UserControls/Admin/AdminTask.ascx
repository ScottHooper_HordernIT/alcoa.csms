﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminTask.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.AdminTask" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>
<style type="text/css">
    .style1
    {
        width: 431px;
        height: 187px;
    }
    .style2
    {
        width: 603px;
    }
    .style3
    {
        width: 448px;
    }
    .style4
    {
        width: 879px;
    }
    .style5
    {
        width: 163px;
        text-align: right;
    }
    .style6
    {
        width: 163px;
        text-align: right;
        height: 13px;
    }
    .style7
    {
        height: 13px;
    }
    .style8
    {
        width: 603px;
        text-align: right;
    }
    .style9
    {
        font-size: small;
        color: #FF0000;
    }
    .style10
    {
        font-size: small;
        color: #0000CC;
    }
    .style11
    {
        color: #FF0000;
    }
</style>
<p>
    &nbsp;<a href="AdminTasks.aspx">Admin Tasks Worklist</a> &gt; <strong>Task Number:</strong>
    <dx:ASPxLabel ID="lblTaskNo" runat="server" Text="(NEW)">
    </dx:ASPxLabel>
</p>
<p class="style9">
    <strong>1) Verify/Edit Task Information</strong></p>
<p>
    <em>Please enter the following information (may be pre-filled) and verify that it
    is correct, as the user will be automatically created in CSMS if they do not exist
    based upon the details listed below.</em></p>
<table class="style1">
    <tr>
        <td class="style8">
            Source:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbSource" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="AdminTaskSourceDataSource1"
                TextField="AdminTaskSourceDesc" ValueField="AdminTaskSourceId">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Type:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="AdminTaskTypeDataSource1"
                TextField="AdminTaskTypeName" ValueField="AdminTaskTypeId">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Access:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbAccess" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="CsmsAccessDataSource1" TextField="AccessDesc"
                ValueField="CsmsAccessId">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Login:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbLogin" runat="server" Style="margin-bottom: 8px" Width="270px"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            E-Mail Address:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbEmail" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RegularExpression ErrorText="Not a Valid E-Mail." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Company:
        </td>
        <td class="style3">
            <dx:ASPxComboBox ID="cbCompany" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                ValueType="System.Int32" Width="270px" DataSourceID="CompaniesDataSource1" TextField="CompanyName"
                ValueField="CompanyId">
                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                </LoadingPanelImage>
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxComboBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            First Name:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbFirstName" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Last Name:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbLastName" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="style8">
            Position Held:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbJobTitle" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Mobile Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbMobileNumber" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Telephone Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbTelephoneNo" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            Fax Number:
        </td>
        <td class="style3">
            <dx:ASPxTextBox ID="tbFaxNo" runat="server" Width="270px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                    <RequiredField IsRequired="True" />
                </ValidationSettings>
            </dx:ASPxTextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;
        </td>
        <td class="style3">
            &nbsp;
        </td>
    </tr>
</table>
<p>
    <data:CompaniesDataSource ID="CompaniesDataSource1" runat="server" EnableCaching="False"
         Sort="CompanyName ASC">
    </data:CompaniesDataSource>
    <data:AdminTaskTypeDataSource ID="AdminTaskTypeDataSource1" runat="server">
    </data:AdminTaskTypeDataSource>
    <data:AdminTaskSourceDataSource ID="AdminTaskSourceDataSource1" runat="server">
    </data:AdminTaskSourceDataSource>
    <data:CsmsAccessDataSource ID="CsmsAccessDataSource1" runat="server">
    </data:CsmsAccessDataSource>
</p>
<p>
    <span class="style9"><strong>2) If AlcoaDirect (External) access is selected please ensure that the user is in
    </strong></span>
    <a href="https://www.alcoadirect.com" target="_blank" class="style10"><strong>AlcoaDirect</strong></a><span 
        class="style9"><strong> and Active</strong></span></p>
<p class="style9">
    <strong>3) Did you have to reset the user&#39;s password or create a new AlcoaDirect account
    for the user?</strong></p>
<p>
    <dx:ASPxRadioButtonList ID="rblNewPassword" runat="server" SelectedIndex="2" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <Items>
            <dx:ListEditItem Text="Yes" Value="1" />
            <dx:ListEditItem Text="No" Value="0" />
            <dx:ListEditItem Selected="True" Text="(n/a)" Value="-1" />
        </Items>
        <ValidationSettings CausesValidation="True">
            <RequiredField IsRequired="True" />
        </ValidationSettings>
    </dx:ASPxRadioButtonList>
</p>
<span class="style9"><strong>4) Please enter any task comments below (optional):</strong></span><br />
<dx:ASPxMemo ID="mTaskComments" runat="server" Height="71px" Width="700px" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
</dx:ASPxMemo>
<p>
    &nbsp;</p>
<p>
    <dx:ASPxButton ID="btnPreviewChanges" runat="server" Text="Preview Changes to be Made"
        Width="900px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
        OnClick="btnPreviewChanges_Click" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    </dx:ASPxButton>
</p>
<dx:ASPxLabel ID="lblHappy" runat="server" Font-Bold="True" 
    ForeColor="Red" 
    Text="If you are happy with the below changes to be made, please click the button below and the changes specified will occur, else adjust your reponses to the questions above (and click preview again).">
</dx:ASPxLabel>
<dx:ASPxButton ID="btnAcceptChanges" runat="server" Enabled="true"
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" OnClick="btnAcceptChanges_Click" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Accept Changes and Set Task Status to Closed" Width="900px">
        <clientsideevents click="function(s, e) {
 
	e.processOnServer = confirm('Are you sure you wish to accept changes and set task status to closed?');
}"></clientsideevents>
    </dx:ASPxButton>
<p>
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue" EnableDefaultAppearance="False" 
        GroupBoxCaptionOffsetY="-25px" HeaderText="Changes" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="True" 
        Width="900px">
        <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
        <HeaderStyle>
        <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" 
            PaddingRight="2px" />
        </HeaderStyle>
        <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <span class="style11">The following changes will be actioned:</span><br />
    <dx:ASPxLabel ID="lblChanges" runat="server" EncodeHtml="False" 
        style="color: #0000CC" Text="No Changes.">
    </dx:ASPxLabel>
            </dx:PanelContent>
</PanelCollection>
    </dx:ASPxRoundPanel>
</p>
<dx:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" EnableDefaultAppearance="False" 
    GroupBoxCaptionOffsetY="-25px" HeaderText="Email (Preview)" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="True" 
    Width="900px">
    <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
    <HeaderStyle>
    <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" 
        PaddingRight="2px" />
    </HeaderStyle>
    <PanelCollection>
<dx:PanelContent runat="server" SupportsDisabledAttribute="True">
    <table cellpadding="0" cellspacing="0" class="style4">
        <tr>
            <td class="style5">
                From:
            </td>
            <td>
                <dx:ASPxTextBox ID="tbFrom" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="600px" ReadOnly="True">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="style6">
                To:
            </td>
            <td class="style7">
                <dx:ASPxTextBox ID="tbTo" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="600px" ReadOnly="True">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Cc:
            </td>
            <td>
                <dx:ASPxTextBox ID="tbCc" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="600px" ReadOnly="True">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Bcc:
            </td>
            <td>
                <dx:ASPxTextBox ID="tbBcc" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="600px" ReadOnly="True">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style5">
                Subject:
            </td>
            <td>
                <dx:ASPxTextBox ID="tbSubject" runat="server" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" 
                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
                    Width="600px" ReadOnly="True">
                </dx:ASPxTextBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                Attached:
            </td>
            <td>
                <dx:ASPxLabel ID="lblNoAttachments" runat="server" Text="(No Attachments)">
                </dx:ASPxLabel>
                <dx:ASPxHyperLink ID="hlAttachment1" runat="server" Visible="true">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="hlAttachment2" runat="server" Visible="true">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="hlAttachment3" runat="server" Visible="true">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="hlAttachment4" runat="server" Visible="true">
                </dx:ASPxHyperLink>
                <br />
                <dx:ASPxHyperLink ID="hlAttachment5" runat="server" Visible="true">
                </dx:ASPxHyperLink>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;
            </td>
            <td>
                <dx:ASPxHtmlEditor ID="heBody" runat="server" ActiveView="Preview" 
                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue" Width="600px">
                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue">
                        <ViewArea>
                            <Border BorderColor="#002D96" />
                        </ViewArea>
                    </Styles>
                    <Settings AllowDesignView="False" AllowHtmlView="False" />
                    <SettingsImageUpload>
                        <ValidationSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png">
                        </ValidationSettings>
                    </SettingsImageUpload>
                    <SettingsImageSelector>
                        <CommonSettings AllowedFileExtensions=".jpe, .jpeg, .jpg, .gif, .png" />
                    </SettingsImageSelector>
                    <SettingsDocumentSelector>
                        <CommonSettings AllowedFileExtensions=".rtf, .pdf, .doc, .docx, .odt, .txt, .xls, .xlsx, .ods, .ppt, .pptx, .odp" />
                    </SettingsDocumentSelector>
                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/HtmlEditor/Loading.gif">
                        </LoadingPanel>
                    </Images>
                    <ImagesFileManager>
                        <FolderContainerNodeLoadingPanel Url="~/App_Themes/Office2003Blue/Web/tvNodeLoading.gif">
                        </FolderContainerNodeLoadingPanel>
                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanel>
                    </ImagesFileManager>
                </dx:ASPxHtmlEditor>
            </td>
        </tr>
    </table>
    <br />

        </dx:PanelContent>
</PanelCollection>
</dx:ASPxRoundPanel>


<br />


