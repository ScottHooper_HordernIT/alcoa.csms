
/****** Object:  StoredProcedure [dbo].[GetCompanyABNByEbiClockIdSwipeDateTime]    Script Date: 3/08/2015 4:58:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCompanyABNByEbiClockIdSwipeDateTime]          
(          
  @live varchar(max),        
 @ClockId int,          
 @SwipeDateTime DateTime            
)                
AS                
     if @live='live'    
 begin  
   IF @ClockId IS NOT NULL                 
    BEGIN                 
   SELECT TOP 1 B.Tax_Registration_Number           
   FROM HR..XXHR_CWK_HISTORY_V A           
   INNER JOIN CompanyVendor B          
   on A.CWK_VENDOR_NO = B.Vendor_Number          
   WHERE A.CWK_NBR=@ClockId AND A.EFFECTIVE_END_DATE>=@SwipeDateTime              
   ORDER BY A.EFFECTIVE_END_DATE ASC              
     END    
end  
else  
 begin       
 IF @ClockId IS NOT NULL                 
    BEGIN     
 Select CompanyABN from EbiCompaniesSitesMap where ClockId=@ClockId     
 and SwipeDateTime=@SwipeDateTime  
end     
    END    
-----------------------------------------------------------------------------------

GO


