﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_RoboCopy" Codebehind="RoboCopy.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<table border="0" cellpadding="0" cellspacing="0" style="width: 900px">
    <tr>
        <td style="width: 100px; text-align: center">
<dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" DataSourceID="ApssLogsDataSource" 
                KeyFieldName="EntryId">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <FilterRowButton Height="13px" Width="13px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Columns>
        <dxwgv:GridViewDataTextColumn FieldName="EntryId" ReadOnly="True" Visible="False"
            VisibleIndex="0">
            <EditFormSettings Visible="False" />
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataDateColumn FieldName="TimeStamp" SortIndex="0" SortOrder="Descending"
            VisibleIndex="0">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataTextColumn FieldName="FileName" SortIndex="1" SortOrder="Ascending"
            VisibleIndex="1">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="FileTag" Caption="Change" VisibleIndex="2">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn FieldName="FileType" VisibleIndex="3">
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Settings ShowHeaderFilterButton="true" />
    <SettingsBehavior ConfirmDelete="True"></SettingsBehavior>
</dxwgv:ASPxGridView>
        </td>
    </tr>
    <tr>
        <td style="width: 100px; text-align: center">
            <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnClick="ASPxButton1_Click" Text="Update Database with latest RoboCopy Logs Information and Reset appropriate Read Flags"
                Width="521px" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxButton>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 100px; text-align: center">
            <dxe:ASPxLabel ID="lblStatus" runat="server" Width="232px" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue">
            </dxe:ASPxLabel>
        </td>
    </tr>
    <tr>
        <td style="width: 100px; height: 39px; text-align: left">
            <dxe:ASPxCheckBox ID="cbShowDebug" runat="server" AutoPostBack="True" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                CssPostfix="Office2003Blue" OnCheckedChanged="cbShowDebug_CheckedChanged" Text="Show Debug Information"
                Width="188px" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
            </dxe:ASPxCheckBox>
        </td>
    </tr>
    <tr>
        <td style="width: 100px; height: 39px; text-align: center">
<dxe:ASPxMemo ID="ASPxMemo1" runat="server" Height="400px" Width="880px" Visible="False" 
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                CssPostfix="Office2003Blue" 
                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
</dxe:ASPxMemo>
        </td>
    </tr>
</table>

<data:ApssLogsDataSource ID="ApssLogsDataSource" runat="server"
		SelectMethod="GetPaged"
		EnablePaging="True"
		EnableSorting="True"
		EnableDeepLoad="False"
		>
</data:ApssLogsDataSource>
