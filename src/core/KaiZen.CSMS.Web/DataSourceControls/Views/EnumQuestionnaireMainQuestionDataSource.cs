﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.EnumQuestionnaireMainQuestionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(EnumQuestionnaireMainQuestionDataSourceDesigner))]
	public class EnumQuestionnaireMainQuestionDataSource : ReadOnlyDataSource<EnumQuestionnaireMainQuestion>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionDataSource class.
		/// </summary>
		public EnumQuestionnaireMainQuestionDataSource() : base(new EnumQuestionnaireMainQuestionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the EnumQuestionnaireMainQuestionDataSourceView used by the EnumQuestionnaireMainQuestionDataSource.
		/// </summary>
		protected EnumQuestionnaireMainQuestionDataSourceView EnumQuestionnaireMainQuestionView
		{
			get { return ( View as EnumQuestionnaireMainQuestionDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the EnumQuestionnaireMainQuestionDataSourceView class that is to be
		/// used by the EnumQuestionnaireMainQuestionDataSource.
		/// </summary>
		/// <returns>An instance of the EnumQuestionnaireMainQuestionDataSourceView class.</returns>
		protected override BaseDataSourceView<EnumQuestionnaireMainQuestion, Object> GetNewDataSourceView()
		{
			return new EnumQuestionnaireMainQuestionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the EnumQuestionnaireMainQuestionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class EnumQuestionnaireMainQuestionDataSourceView : ReadOnlyDataSourceView<EnumQuestionnaireMainQuestion>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the EnumQuestionnaireMainQuestionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public EnumQuestionnaireMainQuestionDataSourceView(EnumQuestionnaireMainQuestionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal EnumQuestionnaireMainQuestionDataSource EnumQuestionnaireMainQuestionOwner
		{
			get { return Owner as EnumQuestionnaireMainQuestionDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal EnumQuestionnaireMainQuestionService EnumQuestionnaireMainQuestionProvider
		{
			get { return Provider as EnumQuestionnaireMainQuestionService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region EnumQuestionnaireMainQuestionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the EnumQuestionnaireMainQuestionDataSource class.
	/// </summary>
	public class EnumQuestionnaireMainQuestionDataSourceDesigner : ReadOnlyDataSourceDesigner<EnumQuestionnaireMainQuestion>
	{
	}

	#endregion EnumQuestionnaireMainQuestionDataSourceDesigner

	#region EnumQuestionnaireMainQuestionFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionFilter : SqlFilter<EnumQuestionnaireMainQuestionColumn>
	{
	}

	#endregion EnumQuestionnaireMainQuestionFilter

	#region EnumQuestionnaireMainQuestionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionExpressionBuilder : SqlExpressionBuilder<EnumQuestionnaireMainQuestionColumn>
	{
	}
	
	#endregion EnumQuestionnaireMainQuestionExpressionBuilder		
}

