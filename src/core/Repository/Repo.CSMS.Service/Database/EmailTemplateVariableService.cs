﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEmailTemplateVariableService
    {
        EmailTemplateVariable Get(Expression<Func<EmailTemplateVariable, bool>> where, List<Expression<Func<EmailTemplateVariable, object>>> includes);
        List<EmailTemplateVariable> GetMany(int? top, Expression<Func<EmailTemplateVariable, bool>> where, Func<IQueryable<EmailTemplateVariable>, IOrderedQueryable<EmailTemplateVariable>> orderBy, List<Expression<Func<EmailTemplateVariable, object>>> includes);
        List<EmailTemplateVariable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EmailTemplateVariable, bool>> where, Func<IQueryable<EmailTemplateVariable>, IOrderedQueryable<EmailTemplateVariable>> orderBy, List<Expression<Func<EmailTemplateVariable, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EmailTemplateVariable, bool>> where, List<Expression<Func<EmailTemplateVariable, object>>> includes);
        void Insert(EmailTemplateVariable item, bool saveChanges = true);
        void Update(EmailTemplateVariable item, bool saveChanges = true);
        void Delete(EmailTemplateVariable item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EmailTemplateVariableService : IEmailTemplateVariableService
    {
        private readonly IRepository<EmailTemplateVariable> _repository;

        public EmailTemplateVariableService(IRepository<EmailTemplateVariable> repository)
        {
            this._repository = repository;
        }

        public EmailTemplateVariable Get(Expression<Func<EmailTemplateVariable, bool>> where, List<Expression<Func<EmailTemplateVariable, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EmailTemplateVariable> GetMany(int? top, Expression<Func<EmailTemplateVariable, bool>> where, Func<IQueryable<EmailTemplateVariable>, IOrderedQueryable<EmailTemplateVariable>> orderBy, List<Expression<Func<EmailTemplateVariable, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EmailTemplateVariable> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EmailTemplateVariable, bool>> where, Func<IQueryable<EmailTemplateVariable>, IOrderedQueryable<EmailTemplateVariable>> orderBy, List<Expression<Func<EmailTemplateVariable, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EmailTemplateVariable, bool>> where, List<Expression<Func<EmailTemplateVariable, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EmailTemplateVariable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailTemplateVariable");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EmailTemplateVariable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailTemplateVariable");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EmailTemplateVariable item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EmailTemplateVariable");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
