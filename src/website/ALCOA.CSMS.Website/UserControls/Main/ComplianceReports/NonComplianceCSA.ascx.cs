using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;

public partial class UserControls_NonComplianceCSA : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            //if (String.IsNullOrEmpty(SessionHandler.spVar_ResidentialStatus2))
            //{
            SessionHandler.spVar_CategoryId2 = "1";
            //}

            string group = "AU";
            if (Request.QueryString["Group"] != null) { group = Request.QueryString["Group"]; }
            RegionsService rService = new RegionsService();
            Regions rList = rService.GetByRegionInternalName(group);
            cbRegion.Value = rList.RegionId;

            cbYear.DataSourceID = "sqldsCSAYear";
            cbYear.TextField = "Year";
            cbYear.ValueField = "Year";
            cbYear.Value = DateTime.Now.Year.ToString();
            cbYear.DataBind();

            SessionHandler.spVar_Year = DateTime.Now.Year.ToString();
            
            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    //full access
                    break;
                case ((int)RoleList.Contractor):
                    grid.Settings.ShowFilterRow = false;
                    grid.FilterExpression = String.Format("CompanyId = {0}", auth.CompanyId);
                    
                    break;
                case ((int)RoleList.Administrator):
                    //full access
                    break;
                default:
                    // do something
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            sqldsNonComplianceReportYearCSA.DataBind();
            grid.Columns[6].Visible = false;
            grid.Columns[7].Visible = false;
            grid.DataBind();

            // Export
            String exportFileName = @"ALCOA CSMS - Non Compliance Report - Contractor Services Audit"; //Chrome & IE is fine.
            String userAgent = Request.Headers.Get("User-Agent");
            if (
                (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
                (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
                )
            {
                exportFileName = exportFileName.Replace(" ", "_");
            }
            Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
        }
    }

    protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {

        #region "Columns"
        int month;
        if (SessionHandler.spVar_Year == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
        }
        else
        {
            month = 12;
        }

        if (e.RowType != GridViewRowType.Data) return;
        //hide labels if nonembedded.
        bool embedded = false;
        int CompanySiteCategoryId = (int)grid.GetRowValues(e.VisibleIndex, "CompanySiteCategoryId");
      
        if (CompanySiteCategoryId == 1) embedded = true;

        //Added By Sayani Sil Fro Task:6
        //string selectedCatId = SessionHandler.spVar_CategoryId2.ToString();
        string selectedCatId = cbStatus.SelectedItem.Value.ToString();
        if (selectedCatId == "2")
        {
            
            if (month >= 0 || month >= 4)
            {
                #region Half 1
                
                if (e.RowType != GridViewRowType.Data) return;
                Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmittedHalf1") as Label;
                int change = 0;
                if (((int)grid.GetRowValues(e.VisibleIndex, "Qtr 1"))> 0)
                {
                     change = ((int)grid.GetRowValues(e.VisibleIndex, "Qtr 1"));
                }
                else if (((int)grid.GetRowValues(e.VisibleIndex, "Qtr 2"))> 0)
                {
                     change = ((int)grid.GetRowValues(e.VisibleIndex, "Qtr 2"));
                }
                label.Text = "";
                System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage6")
                                            as System.Web.UI.WebControls.Image;
                //img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                //                                as System.Web.UI.WebControls.Image;
                img.Visible = false;
                if (change != 0)
                {
                    img.Visible = true;
                    img.ImageUrl = "~/Images/greentick.gif";
                    label.ForeColor = Color.Green;
                    label.Text = String.Format("({0})", change);
                }
                else
                {
                    img.ImageUrl = "~/Images/redcross.gif";
                    label.ForeColor = Color.Red;
                    if (!embedded)
                        img.Visible = false;
                    else
                        img.Visible = true;
                }

                #endregion
                
                if (month >= 7 || month >= 10)
                #region Half 2
                {
                    change = 0;
                    label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmittedHalf2") as Label;
                    //change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 3") + (int)grid.GetRowValues(e.VisibleIndex, "Qtr 4");
                    
                    if (((int)grid.GetRowValues(e.VisibleIndex, "Qtr 3")) > 0)
                    {
                         change = ((int)grid.GetRowValues(e.VisibleIndex, "Qtr 3"));
                    }
                    else if (((int)grid.GetRowValues(e.VisibleIndex, "Qtr 4")) > 0)
                    {
                         change = ((int)grid.GetRowValues(e.VisibleIndex, "Qtr 4"));
                    }
                    
                    label.Text = "";
                    img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage7")
                                                as System.Web.UI.WebControls.Image;
                    img.Visible = false;
                    if (change != 0)
                    {
                        img.Visible = true;
                        img.ImageUrl = "~/Images/greentick.gif";
                        label.ForeColor = Color.Green;
                        label.Text = String.Format("({0})", change);
                    }
                    else
                    {
                        img.ImageUrl = "~/Images/redcross.gif";
                        label.ForeColor = Color.Red;
                        if (!embedded)
                            img.Visible = false;
                        else
                            img.Visible = true;
                    }
                }

                #endregion
            }

            
        }
        else
        {
            if (month >= 0)
            {
                #region Qtr 1
                if (e.RowType != GridViewRowType.Data) return;
                Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted1") as Label;
                int change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 1");

                //Start:Change by Debashis for Export issue
                if (label != null)
                {
                    label.Text = "";
                    System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage1")
                                                as System.Web.UI.WebControls.Image;
                    img.Visible = false;
                    if (change != 0)
                    {
                        img.Visible = true;
                        img.ImageUrl = "~/Images/greentick.gif";
                        label.ForeColor = Color.Green;
                        label.Text = String.Format("({0})", change);
                    }
                    else
                    {
                        img.ImageUrl = "~/Images/redcross.gif";
                        label.ForeColor = Color.Red;
                        if (!embedded)
                            img.Visible = false;
                        else
                            img.Visible = true;
                    }
                #endregion
                    if (month >= 4)
                    {
                        #region Qtr 2
                        label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted2") as Label;
                        change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 2");
                        label.Text = "";
                        img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                                                    as System.Web.UI.WebControls.Image;
                        img.Visible = false;
                        if (change != 0)
                        {
                            img.Visible = true;
                            img.ImageUrl = "~/Images/greentick.gif";
                            label.ForeColor = Color.Green;
                            label.Text = String.Format("({0})", change);
                        }
                        else
                        {
                            img.ImageUrl = "~/Images/redcross.gif";
                            label.ForeColor = Color.Red;
                            if (!embedded)
                                img.Visible = false;
                            else
                                img.Visible = true;
                        }
                        #endregion
                        if (month >= 7)
                        {
                            #region Qtr 3
                            label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted3") as Label;
                            change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 3");
                            label.Text = "";
                            img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage3")
                                                        as System.Web.UI.WebControls.Image;
                            img.Visible = false;
                            if (change != 0)
                            {
                                img.Visible = true;
                                img.ImageUrl = "~/Images/greentick.gif";
                                label.ForeColor = Color.Green;
                                label.Text = String.Format("({0})", change);
                            }
                            else
                            {
                                img.ImageUrl = "~/Images/redcross.gif";
                                label.ForeColor = Color.Red;
                                if (!embedded)
                                    img.Visible = false;
                                else
                                    img.Visible = true;
                            }
                            #endregion
                            if (month >= 10)
                            {
                                #region Qtr 4
                                label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted4") as Label;
                                change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr 4");
                                label.Text = "";
                                img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage4")
                                                            as System.Web.UI.WebControls.Image;
                                img.Visible = false;
                                if (change != 0)
                                {
                                    img.Visible = true;
                                    img.ImageUrl = "~/Images/greentick.gif";
                                    label.ForeColor = Color.Green;
                                    label.Text = String.Format("({0})", change);
                                }
                                else
                                {
                                    img.ImageUrl = "~/Images/redcross.gif";
                                    label.ForeColor = Color.Red;
                                    if (!embedded)
                                        img.Visible = false;
                                    else
                                        img.Visible = true;
                                }
                                #endregion
                            }
                        }
                    }
                }
            }
            

        }

        #region Alcoa Annual Audit
        Label _label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted5") as Label;
        int _change = (int)grid.GetRowValues(e.VisibleIndex, "Alcoa Audit");
        if (_label != null)
        {
            _label.Text = "";
            System.Web.UI.WebControls.Image _img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage5")
                                        as System.Web.UI.WebControls.Image;
            _img.Visible = false;
            if (_change != 0)
            {
                _img.Visible = true;
                _img.ImageUrl = "~/Images/greentick.gif";
                _label.ForeColor = Color.Green;
                _label.Text = String.Format("({0})", _change);
            }
            else
            {
                _img.ImageUrl = "~/Images/redcross.gif";
                _label.ForeColor = Color.Red;
                if (!embedded)
                    _img.Visible = false;
                else
                    _img.Visible = true;
            }
        }
        //End:Change by Debashis for Export issue
        #endregion
        #endregion
    }


    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId" || e.Column.FieldName == "SiteId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
            ////comboBox.DataBind();
            ////comboBox.DataSourceID = "";
            ////ListEditItem liAll = new ListEditItem("(ALL)", null);
            ////comboBox.Items.Insert(0, liAll);
            //comboBox.Items.Clear();
            //comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem();}";
        }
    }
    protected void cbYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_Year = cbYear.SelectedItem.Value.ToString();
        sqldsNonComplianceReportYearCSA.DataBind();
        grid.DataBind();
    }

    protected void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_RegionId = cbRegion.Value.ToString();
    }

    protected void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        SessionHandler.spVar_CategoryId2 = cbStatus.SelectedItem.Value.ToString();
        sqldsNonComplianceReportYearCSA.DataBind();

        if (SessionHandler.spVar_CategoryId2 == "2")
        {
            grid.Columns[2].Visible = false;
            grid.Columns[3].Visible = false;
            grid.Columns[4].Visible = false;
            grid.Columns[5].Visible = false;
            grid.Columns[6].Visible = true;
            grid.Columns[7].Visible = true;

        }
        else
        {
            grid.Columns[2].Visible = true;
            grid.Columns[3].Visible = true;
            grid.Columns[4].Visible = true;
            grid.Columns[5].Visible = true;
            grid.Columns[6].Visible = false;
            grid.Columns[7].Visible = false;
        }

        grid.DataBind();
    }
}
