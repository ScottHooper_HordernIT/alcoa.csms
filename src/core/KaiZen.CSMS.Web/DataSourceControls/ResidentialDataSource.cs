﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.ResidentialProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(ResidentialDataSourceDesigner))]
	public class ResidentialDataSource : ProviderDataSource<Residential, ResidentialKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResidentialDataSource class.
		/// </summary>
		public ResidentialDataSource() : base(new ResidentialService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the ResidentialDataSourceView used by the ResidentialDataSource.
		/// </summary>
		protected ResidentialDataSourceView ResidentialView
		{
			get { return ( View as ResidentialDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the ResidentialDataSource control invokes to retrieve data.
		/// </summary>
		public ResidentialSelectMethod SelectMethod
		{
			get
			{
				ResidentialSelectMethod selectMethod = ResidentialSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (ResidentialSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the ResidentialDataSourceView class that is to be
		/// used by the ResidentialDataSource.
		/// </summary>
		/// <returns>An instance of the ResidentialDataSourceView class.</returns>
		protected override BaseDataSourceView<Residential, ResidentialKey> GetNewDataSourceView()
		{
			return new ResidentialDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the ResidentialDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class ResidentialDataSourceView : ProviderDataSourceView<Residential, ResidentialKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ResidentialDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the ResidentialDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public ResidentialDataSourceView(ResidentialDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal ResidentialDataSource ResidentialOwner
		{
			get { return Owner as ResidentialDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal ResidentialSelectMethod SelectMethod
		{
			get { return ResidentialOwner.SelectMethod; }
			set { ResidentialOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal ResidentialService ResidentialProvider
		{
			get { return Provider as ResidentialService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<Residential> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<Residential> results = null;
			Residential item;
			count = 0;
			
			System.Int32 _residentialId;
			System.Int32 _companyId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case ResidentialSelectMethod.Get:
					ResidentialKey entityKey  = new ResidentialKey();
					entityKey.Load(values);
					item = ResidentialProvider.Get(entityKey);
					results = new TList<Residential>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case ResidentialSelectMethod.GetAll:
                    results = ResidentialProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case ResidentialSelectMethod.GetPaged:
					results = ResidentialProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case ResidentialSelectMethod.Find:
					if ( FilterParameters != null )
						results = ResidentialProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = ResidentialProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case ResidentialSelectMethod.GetByResidentialId:
					_residentialId = ( values["ResidentialId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResidentialId"], typeof(System.Int32)) : (int)0;
					item = ResidentialProvider.GetByResidentialId(_residentialId);
					results = new TList<Residential>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case ResidentialSelectMethod.GetByCompanyIdSiteId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = ResidentialProvider.GetByCompanyIdSiteId(_companyId, _siteId);
					results = new TList<Residential>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case ResidentialSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = ResidentialProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case ResidentialSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = ResidentialProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == ResidentialSelectMethod.Get || SelectMethod == ResidentialSelectMethod.GetByResidentialId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				Residential entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					ResidentialProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<Residential> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			ResidentialProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region ResidentialDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the ResidentialDataSource class.
	/// </summary>
	public class ResidentialDataSourceDesigner : ProviderDataSourceDesigner<Residential, ResidentialKey>
	{
		/// <summary>
		/// Initializes a new instance of the ResidentialDataSourceDesigner class.
		/// </summary>
		public ResidentialDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ResidentialSelectMethod SelectMethod
		{
			get { return ((ResidentialDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new ResidentialDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region ResidentialDataSourceActionList

	/// <summary>
	/// Supports the ResidentialDataSourceDesigner class.
	/// </summary>
	internal class ResidentialDataSourceActionList : DesignerActionList
	{
		private ResidentialDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the ResidentialDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public ResidentialDataSourceActionList(ResidentialDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public ResidentialSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion ResidentialDataSourceActionList
	
	#endregion ResidentialDataSourceDesigner
	
	#region ResidentialSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the ResidentialDataSource.SelectMethod property.
	/// </summary>
	public enum ResidentialSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResidentialId method.
		/// </summary>
		GetByResidentialId,
		/// <summary>
		/// Represents the GetByCompanyIdSiteId method.
		/// </summary>
		GetByCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion ResidentialSelectMethod

	#region ResidentialFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Residential"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResidentialFilter : SqlFilter<ResidentialColumn>
	{
	}
	
	#endregion ResidentialFilter

	#region ResidentialExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Residential"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResidentialExpressionBuilder : SqlExpressionBuilder<ResidentialColumn>
	{
	}
	
	#endregion ResidentialExpressionBuilder	

	#region ResidentialProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;ResidentialChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="Residential"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ResidentialProperty : ChildEntityProperty<ResidentialChildEntityTypes>
	{
	}
	
	#endregion ResidentialProperty
}

