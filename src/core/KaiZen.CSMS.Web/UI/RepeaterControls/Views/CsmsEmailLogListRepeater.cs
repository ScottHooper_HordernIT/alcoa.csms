﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>CsmsEmailLogListRepeater</c>
    /// </summary>
	public class CsmsEmailLogListRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CsmsEmailLogListRepeaterDesigner"/> class.
        /// </summary>
		public CsmsEmailLogListRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is CsmsEmailLogListRepeater))
			{ 
				throw new ArgumentException("Component is not a CsmsEmailLogListRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			CsmsEmailLogListRepeater z = (CsmsEmailLogListRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="CsmsEmailLogListRepeater"/> Type.
    /// </summary>
	[Designer(typeof(CsmsEmailLogListRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:CsmsEmailLogListRepeater runat=\"server\"></{0}:CsmsEmailLogListRepeater>")]
	public class CsmsEmailLogListRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:CsmsEmailLogListRepeater"/> class.
        /// </summary>
		public CsmsEmailLogListRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsmsEmailLogListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsmsEmailLogListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(CsmsEmailLogListItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsmsEmailLogListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(CsmsEmailLogListItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.CsmsEmailLogList entity = o as KaiZen.CSMS.Entities.CsmsEmailLogList;
						CsmsEmailLogListItem container = new CsmsEmailLogListItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class CsmsEmailLogListItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.CsmsEmailLogList _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CsmsEmailLogListItem"/> class.
        /// </summary>
		public CsmsEmailLogListItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CsmsEmailLogListItem"/> class.
        /// </summary>
		public CsmsEmailLogListItem(KaiZen.CSMS.Entities.CsmsEmailLogList entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the CsmsEmailLogId
        /// </summary>
        /// <value>The CsmsEmailLogId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 CsmsEmailLogId
		{
			get { return _entity.CsmsEmailLogId; }
		}
        /// <summary>
        /// Gets the SentByUserId
        /// </summary>
        /// <value>The SentByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 SentByUserId
		{
			get { return _entity.SentByUserId; }
		}
        /// <summary>
        /// Gets the SentByUserFullName
        /// </summary>
        /// <value>The SentByUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SentByUserFullName
		{
			get { return _entity.SentByUserFullName; }
		}
        /// <summary>
        /// Gets the EmailSentDateTime
        /// </summary>
        /// <value>The EmailSentDateTime.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime EmailSentDateTime
		{
			get { return _entity.EmailSentDateTime; }
		}
        /// <summary>
        /// Gets the Subject
        /// </summary>
        /// <value>The Subject.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Subject
		{
			get { return _entity.Subject; }
		}
        /// <summary>
        /// Gets the Body
        /// </summary>
        /// <value>The Body.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Byte[] Body
		{
			get { return _entity.Body; }
		}
        /// <summary>
        /// Gets the EmailTo
        /// </summary>
        /// <value>The EmailTo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailTo
		{
			get { return _entity.EmailTo; }
		}
        /// <summary>
        /// Gets the EmailCc
        /// </summary>
        /// <value>The EmailCc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailCc
		{
			get { return _entity.EmailCc; }
		}
        /// <summary>
        /// Gets the EmailBcc
        /// </summary>
        /// <value>The EmailBcc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailBcc
		{
			get { return _entity.EmailBcc; }
		}
        /// <summary>
        /// Gets the AdminTaskId
        /// </summary>
        /// <value>The AdminTaskId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 AdminTaskId
		{
			get { return _entity.AdminTaskId; }
		}
        /// <summary>
        /// Gets the AdminTaskSourceDesc
        /// </summary>
        /// <value>The AdminTaskSourceDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AdminTaskSourceDesc
		{
			get { return _entity.AdminTaskSourceDesc; }
		}
        /// <summary>
        /// Gets the AdminTaskTypeName
        /// </summary>
        /// <value>The AdminTaskTypeName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AdminTaskTypeName
		{
			get { return _entity.AdminTaskTypeName; }
		}
        /// <summary>
        /// Gets the AdminTaskTypeDesc
        /// </summary>
        /// <value>The AdminTaskTypeDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AdminTaskTypeDesc
		{
			get { return _entity.AdminTaskTypeDesc; }
		}
        /// <summary>
        /// Gets the StatusDesc
        /// </summary>
        /// <value>The StatusDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String StatusDesc
		{
			get { return _entity.StatusDesc; }
		}
        /// <summary>
        /// Gets the AdminTaskComments
        /// </summary>
        /// <value>The AdminTaskComments.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AdminTaskComments
		{
			get { return _entity.AdminTaskComments; }
		}
        /// <summary>
        /// Gets the DateOpened
        /// </summary>
        /// <value>The DateOpened.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime DateOpened
		{
			get { return _entity.DateOpened; }
		}
        /// <summary>
        /// Gets the DateClosed
        /// </summary>
        /// <value>The DateClosed.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime? DateClosed
		{
			get { return _entity.DateClosed; }
		}
        /// <summary>
        /// Gets the OpenedByUser
        /// </summary>
        /// <value>The OpenedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String OpenedByUser
		{
			get { return _entity.OpenedByUser; }
		}
        /// <summary>
        /// Gets the ClosedByUser
        /// </summary>
        /// <value>The ClosedByUser.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String ClosedByUser
		{
			get { return _entity.ClosedByUser; }
		}
        /// <summary>
        /// Gets the DaysWith
        /// </summary>
        /// <value>The DaysWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? DaysWith
		{
			get { return _entity.DaysWith; }
		}
        /// <summary>
        /// Gets the BusinessDaysWith
        /// </summary>
        /// <value>The BusinessDaysWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? BusinessDaysWith
		{
			get { return _entity.BusinessDaysWith; }
		}
        /// <summary>
        /// Gets the HoursWith
        /// </summary>
        /// <value>The HoursWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? HoursWith
		{
			get { return _entity.HoursWith; }
		}
        /// <summary>
        /// Gets the MinsWith
        /// </summary>
        /// <value>The MinsWith.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? MinsWith
		{
			get { return _entity.MinsWith; }
		}
        /// <summary>
        /// Gets the AccessDesc
        /// </summary>
        /// <value>The AccessDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String AccessDesc
		{
			get { return _entity.AccessDesc; }
		}
        /// <summary>
        /// Gets the CompanyName
        /// </summary>
        /// <value>The CompanyName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String CompanyName
		{
			get { return _entity.CompanyName; }
		}
        /// <summary>
        /// Gets the Login
        /// </summary>
        /// <value>The Login.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Login
		{
			get { return _entity.Login; }
		}
        /// <summary>
        /// Gets the EmailAddress
        /// </summary>
        /// <value>The EmailAddress.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailAddress
		{
			get { return _entity.EmailAddress; }
		}
        /// <summary>
        /// Gets the FirstName
        /// </summary>
        /// <value>The FirstName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FirstName
		{
			get { return _entity.FirstName; }
		}
        /// <summary>
        /// Gets the LastName
        /// </summary>
        /// <value>The LastName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String LastName
		{
			get { return _entity.LastName; }
		}
        /// <summary>
        /// Gets the JobRole
        /// </summary>
        /// <value>The JobRole.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String JobRole
		{
			get { return _entity.JobRole; }
		}
        /// <summary>
        /// Gets the JobTitle
        /// </summary>
        /// <value>The JobTitle.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String JobTitle
		{
			get { return _entity.JobTitle; }
		}
        /// <summary>
        /// Gets the MobileNo
        /// </summary>
        /// <value>The MobileNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String MobileNo
		{
			get { return _entity.MobileNo; }
		}
        /// <summary>
        /// Gets the TelephoneNo
        /// </summary>
        /// <value>The TelephoneNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String TelephoneNo
		{
			get { return _entity.TelephoneNo; }
		}
        /// <summary>
        /// Gets the FaxNo
        /// </summary>
        /// <value>The FaxNo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String FaxNo
		{
			get { return _entity.FaxNo; }
		}

	}
}
