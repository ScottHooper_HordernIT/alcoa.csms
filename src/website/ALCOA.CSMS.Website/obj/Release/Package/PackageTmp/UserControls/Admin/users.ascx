﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Adminv2_UserControls_Users"
    CodeBehind="users.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxTabControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>



<style type="text/css">
    .hypercolor
    {
        color:#0d45b7;
    }
    .TabBackColor 
    {
        background-color:#ffffff;
    }
</style>
<input type="hidden" id ="hdnCheckbox" runat="server" />
<dxtc:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    Width="900px">
    <TabPages>
        <dxtc:TabPage Text="All Users">
            <ContentCollection>
                <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 900px" id="test">
                        <tr>
                            <%--<Start:Enhancement_082 by Debashis>--%>                                                      
                            <td align="right" style="width: 900px; height: 16px; text-align: right">
                                <table style="width:100%">
                                        <tr>
                                            <td style="width:93%">
                                                <div style="float:right;width:17%">
                                                   <%-- <dxe:ASPxCheckBox ID="chkDisabledUser" AutoPostBack="true"  OnCheckedChanged="chkDisabledUser_CheckedChanged" Checked="true" TextAlign="Right" runat="server"   Text="Hide 'Disabled' User">--%>
                                                     <dxe:ASPxCheckBox ID="chkDisabledUser" AutoPostBack="true"  TextAlign="Right" runat="server" Text="Hide 'Disabled' User">
                                                   <%-- <ClientSideEvents CheckedChanged="chkDisabledUser_CheckedChanged()"></ClientSideEvents>--%>
                                                     </dxe:ASPxCheckBox>
                                                </div>
                                            </td>
                                            <td style="width:7%">
                                                    <a href="javascript:ShowHideCustomizationWindow();"><span style="color: #0000ff;
                                                    text-decoration: none">Customize</span></a>
                                                
                                            </td>		
                                        </tr>
                                 </table>  
                            </td>
                            <%--<End:Enhancement_082 by Debashis>--%>
                        </tr>
                        <tr>
                            <td id="firstgrid" style="width: 900px; text-align: left; height: 331px; display:none" runat="server" >
                                <dxwgv:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" 
                                    KeyFieldName="UserId" OnInitNewRow="grid_InitNewRow" DataSourceID="UsersDataSource2"
                                    OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" Width="900px">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn AllowDragDrop="False" Caption="Action" ShowInCustomizationForm="True"
                                            ShowSelectCheckbox="True" VisibleIndex="0" Width="95px">
                                            <EditButton Visible="True">
                                            </EditButton>
                                            <NewButton Visible="True">
                                            </NewButton>
                                            <ClearFilterButton Visible="True">
                                            </ClearFilterButton>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" ShowInCustomizationForm="False"
                                            Visible="False" VisibleIndex="1">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="UserLogon" ShowInCustomizationForm="True"
                                            SortIndex="0" SortOrder="Ascending" VisibleIndex="2">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Full Name (Last, First)" FieldName="LastFirstName"
                                            ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3" Width="150px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Full Name (First Last)" FieldName="FirstLastName"
                                            ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="7"
                                            Width="150px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="LastName" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="4">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="FirstName" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="5">
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" ShowInCustomizationForm="True"
                                            UnboundType="String" VisibleIndex="6">
                                            <PropertiesComboBox DataSourceID="CompaniesDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                                TextField="CompanyName" ValueField="CompanyId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" AutoFilterCondition="Contains" FilterMode="DisplayText" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Role" FieldName="RoleId" ShowInCustomizationForm="True"
                                            UnboundType="String" VisibleIndex="8" Width="110px">
                                            <PropertiesComboBox DataSourceID="RoleDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                                TextField="Role" ValueField="RoleId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Title" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="9">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Email" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="10">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="PhoneNo" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="11">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="FaxNo" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="12">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="MobNo" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="13">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ShowInCustomizationForm="False"
                                            Visible="False" VisibleIndex="14">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" />
                                    <SettingsPager>
                                        <AllButton Visible="True">
                                        </AllButton>
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <CollapsedButton Height="12px" Width="11px">
                                        </CollapsedButton>
                                        <ExpandedButton Height="12px" Width="11px">
                                        </ExpandedButton>
                                        <DetailCollapsedButton Height="12px" Width="11px">
                                        </DetailCollapsedButton>
                                        <DetailExpandedButton Height="12px" Width="11px">
                                        </DetailExpandedButton>
                                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                </dxwgv:ASPxGridView>
                            </td>
                        </tr>

                        <%--addded by subhro--%>

                        <tr>
                            <td id="secondgrid" style="width: 900px; text-align: left; height: 331px;" runat="server" >
                                <dxwgv:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" ClientInstanceName="grid1"
                                    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue" 
                                    KeyFieldName="UserId" OnInitNewRow="grid_InitNewRow" DataSourceID="UsersDataSource2"
                                    OnRowInserting="grid_RowInserting" OnRowUpdating="grid_RowUpdating" Width="900px">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn AllowDragDrop="False" Caption="Action" ShowInCustomizationForm="True"
                                            ShowSelectCheckbox="True" VisibleIndex="0" Width="95px">
                                            <EditButton Visible="True">
                                            </EditButton>
                                            <NewButton Visible="True">
                                            </NewButton>
                                            <ClearFilterButton Visible="True">
                                            </ClearFilterButton>
                                            <HeaderStyle HorizontalAlign="Center" />
                                        </dxwgv:GridViewCommandColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" ShowInCustomizationForm="False"
                                            Visible="False" VisibleIndex="1">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="UserLogon" ShowInCustomizationForm="True"
                                            SortIndex="0" SortOrder="Ascending" VisibleIndex="2">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Full Name (Last, First)" FieldName="LastFirstName"
                                            ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="3" Width="150px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Full Name (First Last)" FieldName="FirstLastName"
                                            ReadOnly="True" ShowInCustomizationForm="True" Visible="False" VisibleIndex="7"
                                            Width="150px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="False" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="LastName" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="4">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="FirstName" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="5">
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Company" FieldName="CompanyId" ShowInCustomizationForm="True"
                                            UnboundType="String" VisibleIndex="6">
                                            <PropertiesComboBox DataSourceID="CompaniesDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                                TextField="CompanyName" ValueField="CompanyId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" AutoFilterCondition="Contains" FilterMode="DisplayText" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Role" FieldName="RoleId" ShowInCustomizationForm="True"
                                            UnboundType="String" VisibleIndex="8" Width="110px">
                                            <PropertiesComboBox DataSourceID="RoleDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                                                TextField="Role" ValueField="RoleId">
                                            </PropertiesComboBox>
                                            <Settings SortMode="DisplayText" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Title" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="9">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="Email" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="10">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="PhoneNo" ShowInCustomizationForm="True"
                                            Visible="False" VisibleIndex="11">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="FaxNo" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="12">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="MobNo" ShowInCustomizationForm="True" Visible="False"
                                            VisibleIndex="13">
                                            <Settings AutoFilterCondition="Contains" />
                                            <EditFormSettings Visible="True" />
                                            <CellStyle HorizontalAlign="Left">
                                            </CellStyle>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn FieldName="ModifiedByUserId" ShowInCustomizationForm="False"
                                            Visible="False" VisibleIndex="14">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                    </Columns>
                                    <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" />
                                    <SettingsPager>
                                        <AllButton Visible="True">
                                        </AllButton>
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                                        </LoadingPanelOnStatusBar>
                                        <CollapsedButton Height="12px" Width="11px">
                                        </CollapsedButton>
                                        <ExpandedButton Height="12px" Width="11px">
                                        </ExpandedButton>
                                        <DetailCollapsedButton Height="12px" Width="11px">
                                        </DetailCollapsedButton>
                                        <DetailExpandedButton Height="12px" Width="11px">
                                        </DetailExpandedButton>
                                        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                                        </LoadingPanel>
                                    </Images>
                                    <ImagesFilterControl>
                                        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                                        </LoadingPanel>
                                    </ImagesFilterControl>
                                    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                                        <Header ImageSpacing="5px" SortingImageSpacing="5px">
                                        </Header>
                                        <LoadingPanel ImageSpacing="10px">
                                        </LoadingPanel>
                                    </Styles>
                                </dxwgv:ASPxGridView>
                            </td>
                        </tr>

                        <%--ended by subhro--%>

                        <tr>
                            <td style="width: 900px; text-align: left; padding-top: 2px">
                                <table border="0" cellpadding="0" cellspacing="0" style="height: 8px">
                                    <tr>
                                        <td style="width: 81px; height: 40px;">
                                            <dxe:ASPxCheckBox ID="ASPxCheckBox1" runat="server" CheckState="Unchecked" Text="Select All">
                                                <ClientSideEvents CheckedChanged="function(s, e) {
	                if (s.GetValue() == true)
	                {
		                grid.SelectAllRowsOnPage();
	                }
	                else
	                {
		                grid.UnselectAllRowsOnPage();
	                }
                }" />
                                            </dxe:ASPxCheckBox>
                                        </td>
                                        <td style="width: 103px; height: 40px;">
                                            <dxe:ASPxButton ID="ASPxButton3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                CssPostfix="Office2003Blue" OnClick="ASPxButton3_Click" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                Text="Generate E-Mail Link" Width="140px">
                                            </dxe:ASPxButton>
                                        </td>
                                        <td style="width: 162px; height: 40px">
                                            &nbsp;<dxe:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Cursor="default" NavigateUrl="javascript:window.alert('No Contacts Selected');"
                                                Text="E-Mail Users" CssClass="hypercolor">
                                            </dxe:ASPxHyperLink>
                                        </td>
                                        <td style="width: 266px; height: 40px">
                                        </td>
                                        <td style="width: 250px; height: 40px">
                                            Number of Rows to show per page:<asp:DropDownList ID="DropDownList1" runat="server"
                                                AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>50</asp:ListItem>
                                                <asp:ListItem>All</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="nb. If you select All or a large no. of users, the 'email users' link may not work due to browser limitations. Copy &amp; Paste the Emails below into Outlook."
                                    Visible="False"></asp:Label>
                                <dxe:ASPxMemo ID="ASPxMemo1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    CssPostfix="Office2003Blue" Height="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    Visible="False" Width="900px">
                                </dxe:ASPxMemo>
                                <table border="0" cellpadding="0" cellspacing="0" width="900">
                                    <tr>
                                        <td style="height: 10px; text-align: right">
                                            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <asp:UpdatePanel ID="upLogin" runat="server">
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="900">
                                <tr>
                                    <td colspan="2" style="height: 10px; text-align: left">
                                        <strong>See What They See (Login as Another User)</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="height: 10px; text-align: left">
                                        Warning: This will log you out of CSMS and in again as the selected user. <span style="color: red">
                                            Use with care</span>.
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 98px; height: 10px; text-align: left">
                                        <dxe:ASPxComboBox ID="ASPxComboBox1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" DataSourceID="UsersGetUserLogonAscDataSource" ValueType="System.String"
                                            TextField="UserLogon" ValueField="UserLogon" IncrementalFilteringMode="StartsWith"
                                            Width="332px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                            </LoadingPanelImage>
                                            <ButtonStyle Width="13px">
                                            </ButtonStyle>
                                        </dxe:ASPxComboBox>
                                    </td>
                                    <td colspan="1" style="height: 10px; text-align: left">
                                        <dxe:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            CssPostfix="Office2003Blue" Text="Login as Selected User" OnClick="ASPxButton1_Click"
                                            AutoPostBack="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ASPxButton1"></asp:PostBackTrigger>
                        </Triggers>
                    </asp:UpdatePanel>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
        <dxtc:TabPage Text="+Additional Alcoa Reader Privileges">
            <ContentCollection>
                <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dxtc:ASPxPageControl ID="ASPxPageControl2" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css"
                        CssPostfix="Office2010Blue" SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css"
                        TabSpacing="0px" Width="880px" ActiveTabStyle-BackColor="White" ContentStyle-BackColor="White" ActiveTabStyle-BorderBottom-BorderColor="White">
                        <TabPages>
                            <dxtc:TabPage Text="+H&amp;S Assessor" >
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <strong>Default EHS Consultants</strong><br />
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 100px; text-align: right">
                                                                    WAO:
                                                                </td>
                                                                <td style="width: 100px">
                                                                    <dxe:ASPxComboBox ID="cbDefaultEhsConsultant_WAO" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" DataSourceID="UsersEhsConsultantsDataSource" IncrementalFilteringMode="StartsWith"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextField="UserFullName"
                                                                        ValueField="Email" ValueType="System.String">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td rowspan="2" style="width: 100px">
                                                                    <dxe:ASPxButton ID="btnSaveDefaultEhsConsultants" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" Height="48px" OnClick="btnSaveDefaultEhsConsultants_Click"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Text="Save" Width="69px">
                                                                        <ClientSideEvents Click="function(s, e) {
	alert('Saved.');
}" />
                                                                    </dxe:ASPxButton>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 100px; height: 38px; text-align: right">
                                                                    VIC OPS:
                                                                </td>
                                                                <td style="width: 100px; height: 38px">
                                                                    <dxe:ASPxComboBox ID="cbDefaultEhsConsultant_VICOPS" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                        CssPostfix="Office2003Blue" DataSourceID="UsersEhsConsultantsDataSource" IncrementalFilteringMode="StartsWith"
                                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TextField="UserFullName"
                                                                        ValueField="Email" ValueType="System.String">
                                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                                        </LoadingPanelImage>
                                                                        <ButtonStyle Width="13px">
                                                                        </ButtonStyle>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSaveDefaultEhsConsultants" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                            <br />
                                            <strong>EHS Consultants / H&amp;S Assessors</strong><br />
                                        <asp:PlaceHolder ID="phHsAssessor" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Text="+Procurement">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <strong>Procurement Users</strong><br />
                                        <asp:PlaceHolder ID="phProcurement" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage NewLine="True" Text="+EBI Data Access">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <strong>EBI Data Access<br />
                                        </strong>
                                        <asp:PlaceHolder ID="phEbiDataAccess" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Text="+Safety Qualification Read Access">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <asp:PlaceHolder ID="phSafetyQualificationReadAccess" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Text="+Financial Reporting Access">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <asp:PlaceHolder ID="phFinancialReportingAccess" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                            <dxtc:TabPage Text="+Training Package Trainer">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <asp:PlaceHolder ID="phTrainingPackageTrainer" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                        </TabPages>
                        <LoadingPanelImage Url="~/App_Themes/Office2010BlueNew/Web/Loading.gif">
                        </LoadingPanelImage>
                        <LoadingPanelStyle ImageSpacing="5px">
                        </LoadingPanelStyle>
                        <Paddings Padding="2px" PaddingLeft="5px" PaddingRight="5px" />

<ActiveTabStyle BackColor="White">
<BorderBottom BorderColor="White"></BorderBottom>
</ActiveTabStyle>

                        <ContentStyle>
                            <Paddings Padding="12px" />
                            <Border BorderColor="#859EBF" BorderStyle="Solid" BorderWidth="1px" />
                        </ContentStyle>
                    </dxtc:ASPxPageControl>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
        <dxtc:TabPage Text="+Additional Contractor Privileges">
            <ContentCollection>
                <dxw:ContentControl runat="server" SupportsDisabledAttribute="True">
                    <dxtc:ASPxPageControl ID="ASPxPageControl3" runat="server" ActiveTabIndex="0" CssFilePath="~/App_Themes/Office2010BlueNew/{0}/styles.css"
                        CssPostfix="Office2010Blue" SpriteCssFilePath="~/App_Themes/Office2010BlueNew/{0}/sprite.css"
                        TabSpacing="0px" Width="880px" ActiveTabStyle-BackColor="White" ContentStyle-BackColor="White" ActiveTabStyle-BorderBottom-BorderColor="White">
                        <TabPages>
                            <dxtc:TabPage Text="+View Engineering Project Hours">
                                <ContentCollection>
                                    <dxw:ContentControl runat="server">
                                        <asp:PlaceHolder ID="phViewEngineeringProjectHours" runat="server"></asp:PlaceHolder>
                                    </dxw:ContentControl>
                                </ContentCollection>
                            </dxtc:TabPage>
                        </TabPages>
                        <LoadingPanelImage Url="~/App_Themes/Office2010BlueNew/Web/Loading.gif">
                        </LoadingPanelImage>
                        <LoadingPanelStyle ImageSpacing="5px">
                        </LoadingPanelStyle>
                        <Paddings Padding="2px" PaddingLeft="5px" PaddingRight="5px" />

<ActiveTabStyle BackColor="White">
<BorderBottom BorderColor="White"></BorderBottom>
</ActiveTabStyle>

                        <ContentStyle>
                            <Paddings Padding="12px" />
                            <Border BorderColor="#859EBF" BorderStyle="Solid" BorderWidth="1px" />
                        </ContentStyle>
                    </dxtc:ASPxPageControl>
                </dxw:ContentControl>
            </ContentCollection>
        </dxtc:TabPage>
    </TabPages>
    <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
    </LoadingPanelImage>
    <LoadingPanelStyle ImageSpacing="6px">
    </LoadingPanelStyle>
    <ContentStyle>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
    </ContentStyle>
</dxtc:ASPxPageControl>
<data:CompaniesDataSource ID="CompaniesDataSource" runat="server" Sort="CompanyName ASC">
</data:CompaniesDataSource>
<data:RoleDataSource ID="RoleDataSource" runat="server" EnableDeepLoad="False" EnablePaging="True"
    EnableSorting="True" SelectMethod="GetPaged">
</data:RoleDataSource>
<asp:SqlDataSource ID="UsersGetUserLogonAscDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Users_GetUserLogon_Asc" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

<%--<asp:SqlDataSource ID="UsersDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Users_Filter" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>--%>

<asp:ObjectDataSource ID="UsersDataSource2" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.Users"
    DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetCustom" TypeName="KaiZen.CSMS.Services.UsersService" UpdateMethod="Update">
    <DeleteParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="UsersDataSource1" runat="server" DataObjectTypeName="KaiZen.CSMS.Entities.Users"
    DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}"
     TypeName="KaiZen.CSMS.Services.UsersService" UpdateMethod="Update"  SelectMethod="GetCustom">
    <SelectParameters>
        <asp:Parameter DefaultValue="true" Name="IsChecked" Type="Boolean" />

    </SelectParameters>
    <DeleteParameters>
        <asp:Parameter Name="UserId" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>

<data:UsersEhsConsultantsDataSource ID="UsersEhsConsultantsDataSource" runat="server"
    EnableSorting="true" SelectMethod="GetPaged" Filter="Enabled=1" Sort="UserFullName ASC">
</data:UsersEhsConsultantsDataSource>
<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
    EnableHotTrack="False" HeaderText="Warning" Height="111px" Modal="True" PopupHorizontalAlign="WindowCenter"
    PopupVerticalAlign="WindowCenter" Width="439px">
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <ContentCollection>
        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server"
            SupportsDisabledAttribute="True">
            <dxe:ASPxLabel runat="server" ID="ASPxLabel1" Font-Size="14px" Text=""
                Height="30px">
                <Border BorderWidth="10px" BorderColor="White"></Border>
            </dxe:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>