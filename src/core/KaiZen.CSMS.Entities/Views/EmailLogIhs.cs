﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'EmailLog_Ihs' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class EmailLogIhs : EmailLogIhsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="EmailLogIhs"/> instance.
		///</summary>
		public EmailLogIhs():base(){}	
		
		#endregion
	}
}
