﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersProcurementListWithLocationProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersProcurementListWithLocationDataSourceDesigner))]
	public class UsersProcurementListWithLocationDataSource : ReadOnlyDataSource<UsersProcurementListWithLocation>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationDataSource class.
		/// </summary>
		public UsersProcurementListWithLocationDataSource() : base(new UsersProcurementListWithLocationService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersProcurementListWithLocationDataSourceView used by the UsersProcurementListWithLocationDataSource.
		/// </summary>
		protected UsersProcurementListWithLocationDataSourceView UsersProcurementListWithLocationView
		{
			get { return ( View as UsersProcurementListWithLocationDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersProcurementListWithLocationDataSourceView class that is to be
		/// used by the UsersProcurementListWithLocationDataSource.
		/// </summary>
		/// <returns>An instance of the UsersProcurementListWithLocationDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersProcurementListWithLocation, Object> GetNewDataSourceView()
		{
			return new UsersProcurementListWithLocationDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersProcurementListWithLocationDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersProcurementListWithLocationDataSourceView : ReadOnlyDataSourceView<UsersProcurementListWithLocation>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementListWithLocationDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersProcurementListWithLocationDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersProcurementListWithLocationDataSourceView(UsersProcurementListWithLocationDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersProcurementListWithLocationDataSource UsersProcurementListWithLocationOwner
		{
			get { return Owner as UsersProcurementListWithLocationDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersProcurementListWithLocationService UsersProcurementListWithLocationProvider
		{
			get { return Provider as UsersProcurementListWithLocationService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersProcurementListWithLocationDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersProcurementListWithLocationDataSource class.
	/// </summary>
	public class UsersProcurementListWithLocationDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersProcurementListWithLocation>
	{
	}

	#endregion UsersProcurementListWithLocationDataSourceDesigner

	#region UsersProcurementListWithLocationFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationFilter : SqlFilter<UsersProcurementListWithLocationColumn>
	{
	}

	#endregion UsersProcurementListWithLocationFilter

	#region UsersProcurementListWithLocationExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementListWithLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementListWithLocationExpressionBuilder : SqlExpressionBuilder<UsersProcurementListWithLocationColumn>
	{
	}
	
	#endregion UsersProcurementListWithLocationExpressionBuilder		
}

