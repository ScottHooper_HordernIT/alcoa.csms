﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" Inherits="PopUps_SafetyPQ_QuestionnaireReport" Codebehind="SafetyPQ_QuestionnaireReport.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1.Web, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraCharts.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ScriptManager ID="scriptmanager1" runat="Server" EnablePageMethods="true" EnablePartialRendering="true" AsyncPostBackTimeOut="600" Loadscriptsbeforeui="true">
    </asp:ScriptManager>
        
        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(initializeRequest);
            prm.add_pageLoaded(pageLoaded);

            function initializeRequest(sender, args)
            {
                loadingPanel.ShowInElement(args._postBackElement);
            }

            function pageLoaded(sender, args)
            {
                var panels = args.get_panelsUpdated();
                if (panels.length > 0)
                {
                    loadingPanel.Hide();
                }
            }
        </script>

    <dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" clientinstancename="loadingPanel" 
             runat="server" Modal="True" VerticalAlign="Middle" HorizontalAlign="Center"
             CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
             CssPostfix="Office2003Blue">
        <LoadingDivStyle BackColor="Silver" Opacity="10">
        </LoadingDivStyle>
        <Image Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </Image>
    </dx:ASPxLoadingPanel>
    
    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" BackColor="#DDECFE" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
        CssPostfix="Office2003Blue" EnableDefaultAppearance="False" HeaderText="Safety Qualification Risk Assessment Summary"
        Width="740px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <TopEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </TopEdge>
        <ContentPaddings Padding="2px" PaddingBottom="10px" PaddingTop="10px" />
        <HeaderRightEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderRightEdge>
        <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        <HeaderLeftEdge>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderLeftEdge>
        <HeaderStyle BackColor="#7BA4E0" HorizontalAlign="Center">
            <Paddings Padding="0px" PaddingBottom="7px" PaddingLeft="2px" PaddingRight="2px" />
            <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px" />
        </HeaderStyle>
        <HeaderContent>
            <BackgroundImage HorizontalPosition="left" ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png"
                Repeat="RepeatX" VerticalPosition="top" />
        </HeaderContent>
        <DisabledStyle ForeColor="Gray">
        </DisabledStyle>
        <NoHeaderTopEdge BackColor="#DDECFE">
        </NoHeaderTopEdge>
        <PanelCollection>
            <dx:PanelContent ID="PanelContent1" runat="server">
                <table border="0" cellpadding="0" cellspacing="0" style="width: 700px; border-collapse: collapse"
                    width="588">
                    <colgroup>
                        <col style="width: 8pt; mso-width-source: userset; mso-width-alt: 365" width="10" />
                        <col style="width: 156pt; mso-width-source: userset; mso-width-alt: 7606" width="208" />
                        <col span="3" style="width: 90pt; mso-width-source: userset; mso-width-alt: 4388"
                            width="120" />
                        <col style="width: 8pt; mso-width-source: userset; mso-width-alt: 365" width="10" />
                    </colgroup>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 34px;">
                            &nbsp;
                        </td>
                        <td class="xl78" style="height: 34px; text-align: left; width: 186pt;">
                            Assessment Date:
                        </td>
                        <td class="xl88" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            border-bottom: #c0c0c0; background-color: transparent; height: 34px; width: 95pt;">
                            <dx:ASPxDateEdit ID="dtAssessmentDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                runat="server" Width="100px" Date="2009-01-07" EditFormatString="dd/MM/yyyy"
                                EditFormat="Custom" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                                    <RequiredField IsRequired="True"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td class="xl77" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            border-bottom: #c0c0c0; background-color: transparent; text-align: right; height: 34px;
                            width: 118pt;">
                            Valid Until:
                        </td>
                        <td class="xl87" style="width: 91pt; height: 34px; text-align: left;">
                            <dx:ASPxDateEdit ID="dtValid" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                runat="server" Width="100px" Date="2009-01-07" EditFormatString="dd/MM/yyyy"
                                EditFormat="Custom" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                                    <RequiredField IsRequired="True"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxDateEdit>
                        </td>
                        <td class="xl76" style="height: 34px;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" style="height: 18px; text-align: left; width: 186pt;">
                            Assessed By:
                        </td>
                        <td class="xl87" colspan="3" style="height: 18px">
                            <asp:Label ID="lblAssessedBy" runat="server" Font-Bold="False" Width="100%">-</asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 12px;">
                        </td>
                        <td class="xl78" style="text-align: left; width: 186pt; height: 12px;">
                            Legal Name of Company:
                        </td>
                        <td class="xl88" colspan="3" style="height: 12px">
                            <asp:Label ID="lblLegalName" runat="server" Width="100%"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 12px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 13px">
                        </td>
                        <td class="xl78" style="width: 186pt; height: 13px; text-align: left">
                            Operating Name of Company:
                        </td>
                        <td class="xl77" colspan="2" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            border-bottom: #c0c0c0; height: 13px; background-color: transparent; text-align: left">
                            <dx:ASPxComboBox ID="cbOperatingName" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                runat="server" Width="231px" ClientInstanceName="cmbCompanies" ValueField="CompanyID"
                                TextField="CompanyName" DataSourceID="CompaniesDataSourceExcludeAlcoa" ValueType="System.Int32"
                                IncrementalFilteringMode="StartsWith" Enabled="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                </LoadingPanelImage>
                                <ButtonStyle Width="13px">
                                </ButtonStyle>
                                <ValidationSettings CausesValidation="True" SetFocusOnError="True" ErrorDisplayMode="ImageWithTooltip">
                                    <RequiredField IsRequired="True"></RequiredField>
                                </ValidationSettings>
                            </dx:ASPxComboBox>
                        </td>
                        <td class="xl87" style="width: 91pt; height: 13px">
                            <dx:ASPxButton ID="ASPxButton1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" PostBackUrl="javascript:window.close();" Text="Close"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dx:ASPxButton>
                        </td>
                        <td class="xl76" style="height: 13px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 13px">
                        </td>
                        <td class="xl78" style="width: 186pt; height: 13px; text-align: right">
                            <asp:collapsiblepanelextender id="CollapsiblePanelExtender1" runat="server"
                                targetcontrolid="ContentPanel" expandcontrolid="TitlePanel" collapsed="true"
                                textlabelid="TextToControl" expandedtext="" collapsedtext="" imagecontrolid="ImageToControl"
                                expandedimage="~/Images/gvExpandedButton.png" collapsedimage="~/Images/gvCollapsedButton.png"
                                suppresspostback="False" collapsecontrolid="TitlePanel">
</asp:collapsiblepanelextender>
                        </td>
                        <td class="xl88" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            width: 95pt; border-bottom: #c0c0c0; height: 13px; background-color: transparent">
                        </td>
                        <td class="xl77" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            width: 118pt; border-bottom: #c0c0c0; height: 13px; background-color: transparent;
                            text-align: right">
                        </td>
                        <td class="xl87" style="width: 91pt; height: 13px">
                        </td>
                        <td class="xl76" style="height: 13px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" colspan="4" style="height: 18px; text-align: right">
                            <!-- Collapsible Panel Title Bar-->
                            <div id="Div1" class="collapsePanel">
                                <asp:Panel ID="TitlePanel" runat="server" Height="23px" Width="100%">
                                    <div style="padding: 5px; cursor: hand; vertical-align: middle;">
                                        <div style="float: left; vertical-align: middle;">
                                            <asp:Image ID="ImageToControl" runat="server" ImageUrl="../Images/expand_blue.jpg" />
                                        </div>
                                        <div style="float: left; font-size: 10pt">
                                            <strong>Company Details</strong></div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <!-- Collapsible Panel Target -->
                            <div id="Div2" class="collapseTargetPanel">
                                <asp:Panel ID="ContentPanel" runat="server" Height="50px" Width="590px">
                                    <table border="0" cellpadding="0" cellspacing="0" style="width: 442pt; border-collapse: collapse"
                                        width="588">
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>ABN:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; height: 13pt; background-color: transparent; text-align: left">
                                                &nbsp;<asp:Label ID="lblABN" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; height: 13pt;
                                                text-align: right;">
                                                <strong>Street Address:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; height: 13pt; text-align: left;">
                                                &nbsp;<asp:Label ID="lblStreetAddress" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;
                                                height: 16px;">
                                                <strong>City:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; height: 16px; text-align: left;">
                                                &nbsp;<asp:Label ID="lblCity" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; height: 14pt;
                                                text-align: right;">
                                                <strong>State:</strong>
                                            </td>
                                            <td style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0; border-bottom: #c0c0c0;
                                                background-color: transparent; height: 14pt; text-align: left;">
                                                &nbsp;<asp:Label ID="lblState" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                            <td style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0; border-bottom: #c0c0c0;
                                                background-color: transparent; height: 14pt; text-align: right; width: 129pt;">
                                                <strong>Country:</strong>
                                            </td>
                                            <td style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0; border-bottom: #c0c0c0;
                                                background-color: transparent; height: 14pt; width: 90pt; text-align: left;">
                                                &nbsp;<asp:Label ID="lblCountry" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;
                                                height: 16px;">
                                                <strong>PostCode:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; height: 16px; text-align: left;">
                                                &nbsp;<asp:Label ID="lblPostCode" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>Contact Name:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: left;">
                                                &nbsp;<asp:Label ID="lblContactName" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>Contact Title:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: left;">
                                                &nbsp;<asp:Label ID="lblContactTitle" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>Contact E-mail Address:</strong>
                                            </td>
                                            <td class="xl97" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: left;">
                                                &nbsp;<asp:Label ID="lblContactEmail" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>Contact Phone Number:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: left;">
                                                &nbsp;<asp:Label ID="lblContactPhone" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="xl78" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                width: 129pt; border-bottom: #c0c0c0; background-color: transparent; text-align: right;">
                                                <strong>Contact Fax Number:</strong>
                                            </td>
                                            <td class="xl90" colspan="3" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                                                border-bottom: #c0c0c0; background-color: transparent; text-align: left;">
                                                &nbsp;<asp:Label ID="lblContactFax" runat="server" Font-Bold="False">-</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <br />
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" style="font-size: 10pt; width: 186pt; height: 18px; text-align: left">
                            Overall Company Risk Rating:
                        </td>
                        <td class="xl87" colspan="3" style="height: 18px">
                            &nbsp;<dx:ASPxImage ID="ASPxImage1" runat="server">
                            </dx:ASPxImage>
                            &nbsp;<asp:Label ID="lblRiskRating" runat="server" Font-Bold="True" Font-Size="12pt"
                                Height="22px"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" style="font-size: 10pt; width: 186pt; height: 18px; text-align: right">
                        </td>
                        <td class="xl88" style="border-right: #c0c0c0; border-top: #c0c0c0; font-size: 10pt;
                            border-left: #c0c0c0; width: 95pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent">
                        </td>
                        <td class="xl77" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            width: 118pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent;
                            text-align: right">
                        </td>
                        <td class="xl87" style="width: 91pt; height: 18px">
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl69" style="border-right: #c0c0c0; border-top: windowtext 1pt solid;
                            border-left: windowtext 1pt solid; width: 186pt; border-bottom: windowtext 1pt solid;
                            background-color: transparent; text-align: center;">
                            <strong>Area</strong>
                        </td>
                        <td class="xl65" style="border-right: windowtext 1pt solid; border-top: windowtext 1pt solid;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 95pt;">
                            <strong>Score</strong>
                        </td>
                        <td class="xl81" style="border-right: #c0c0c0; border-top: windowtext 1pt solid;
                            border-left: #c0c0c0; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 118pt;">
                            <strong>Maximum</strong>
                        </td>
                        <td class="xl65" style="border-right: windowtext 1pt solid; border-top: windowtext 1pt solid;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 91pt;">
                            <strong>Contribution (Weighted Score)</strong>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 20px">
                        </td>
                        <td class="xl70" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: windowtext 1pt solid;
                            width: 186pt; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            height: 20px;">
                            &nbsp;Systems
                        </td>
                        <td class="xl85" style="border-right: windowtext 1pt solid; border-top: windowtext;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            text-align: center; font-weight: bold; height: 20px; width: 95pt;">
                            <asp:Label ID="lblSystemsScore" runat="server" Font-Bold="False"></asp:Label>
                        </td>
                        <td class="xl82" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                            border-left: #c0c0c0; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            text-align: center; height: 20px; width: 118pt;">
                            <asp:Label ID="lblSystemsScoreMax" runat="server" Font-Bold="False">23</asp:Label>
                        </td>
                        <td class="xl66" style="width: 91pt; height: 20px; text-align: center;">
                            <asp:Label ID="lblSystemsScoreP" runat="server" Font-Bold="True" Style="text-align: center"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 20px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl71" style="border-right: #c0c0c0; border-top: windowtext; border-left: windowtext 1pt solid;
                            width: 186pt; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            height: 14pt;">
                            &nbsp;Staffing
                        </td>
                        <td class="xl67" style="border-right: windowtext 1pt solid; border-top: windowtext;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            height: 14pt; text-align: center; width: 95pt;">
                            <asp:Label ID="lblStaffingScore" runat="server" Font-Bold="False"></asp:Label>
                        </td>
                        <td class="xl83" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                            border-left: #c0c0c0; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            height: 14pt; text-align: center; width: 118pt;">
                            <asp:Label ID="lblStaffingScoreMax" runat="server" Font-Bold="False">15</asp:Label>
                        </td>
                        <td class="xl67" style="width: 91pt; text-align: center;">
                            <asp:Label ID="lblStaffingScoreP" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl71" style="border-right: #c0c0c0; border-top: windowtext; border-left: windowtext 1pt solid;
                            width: 186pt; border-bottom: windowtext 0.5pt solid; background-color: transparent">
                            &nbsp;Expectations
                        </td>
                        <td class="xl67" style="border-right: windowtext 1pt solid; border-top: windowtext;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            text-align: center; width: 95pt;">
                            <asp:Label ID="lblExpectationsScore" runat="server" Font-Bold="False"></asp:Label>
                        </td>
                        <td class="xl83" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                            border-left: #c0c0c0; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            text-align: center; width: 118pt;">
                            <asp:Label ID="lblExpectationsScoreMax" runat="server" Font-Bold="False">16</asp:Label>
                        </td>
                        <td class="xl67" style="width: 91pt; text-align: center;">
                            <asp:Label ID="lblExpectationsScoreP" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl72" style="border-right: #c0c0c0; border-top: windowtext; border-left: windowtext 1pt solid;
                            width: 186pt; border-bottom: #c0c0c0; background-color: transparent">
                            &nbsp;Results
                        </td>
                        <td class="xl67" style="border-right: windowtext 1pt solid; border-top: windowtext;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 0.5pt solid; background-color: transparent;
                            text-align: center; width: 95pt;">
                            <asp:Label ID="lblResultsScore" runat="server" Font-Bold="False"></asp:Label>
                        </td>
                        <td class="xl84" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                            border-left: #c0c0c0; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 118pt;">
                            <asp:Label ID="lblResultsScoreMax" runat="server" Font-Bold="False">20</asp:Label>
                        </td>
                        <td class="xl68" style="width: 91pt; text-align: center;">
                            <asp:Label ID="lblResultsScoreP" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl69" style="border-right: #c0c0c0; border-top: windowtext 1pt solid;
                            border-left: windowtext 1pt solid; width: 186pt; border-bottom: windowtext 1pt solid;
                            background-color: transparent">
                            &nbsp;Overall Score
                        </td>
                        <td class="xl64" style="border-right: windowtext 0.5pt solid; border-top: windowtext 1pt solid;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 95pt;">
                            <asp:Label ID="lblOverallScore" runat="server" Font-Bold="False" Visible="False"></asp:Label>
                        </td>
                        <td class="xl64" style="border-right: windowtext 0.5pt solid; border-top: windowtext;
                            border-left: windowtext 1pt solid; border-bottom: windowtext 1pt solid; background-color: transparent;
                            text-align: center; width: 118pt;">
                            <asp:Label ID="lblOverallScoreMax" runat="server" Font-Bold="False" Visible="False">74</asp:Label>
                        </td>
                        <td class="xl65" style="width: 91pt; text-align: center;">
                            <asp:Label ID="lblOverallScoreP" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" style="font-size: 10pt; width: 186pt; height: 18px; text-align: right">
                        </td>
                        <td class="xl88" style="border-right: #c0c0c0; border-top: #c0c0c0; font-size: 10pt;
                            border-left: #c0c0c0; width: 95pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent">
                        </td>
                        <td class="xl77" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            width: 118pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent;
                            text-align: right">
                        </td>
                        <td class="xl87" style="width: 91pt; height: 18px">
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl87" colspan="4" style="height: 18px; text-align: center">
                            <dx:WebChartControl ID="WebChartControl1" runat="server" DataSourceID="SqlDataSource1"
                                Height="262px" Width="600px"  AppearanceName="Northern Lights"
                                PaletteName="Palette 1">
                                <FillStyle >
                                    <OptionsSerializable>
<dx:SolidFillOptions HiddenSerializableString="to be serialized" />
</OptionsSerializable>
                                </FillStyle>
                                <SeriesSerializable>
                                    <dx:Series  Name="Risk Profile" 
                                        >
                                        <ViewSerializable>
<dx:SideBySideBarSeriesView coloreach="True" HiddenSerializableString="to be serialized">
                                            <financialindicators><TrendLines>
<dx:TrendLine Name="Trendline 1">
<LineStyle DashStyle="Dash"></LineStyle>

<Point1 ArgumentSerializable="0"></Point1>

<Point2 ArgumentSerializable="4"></Point2>
</dx:TrendLine>
</TrendLines>
</financialindicators>
                                        </dx:SideBySideBarSeriesView>
</ViewSerializable>
                                        <PointOptionsSerializable>
<dx:PointOptions HiddenSerializableString="to be serialized">
                                        </dx:PointOptions>
</PointOptionsSerializable>
                                        <LegendPointOptionsSerializable>
<dx:PointOptions HiddenSerializableString="to be serialized">
                                        </dx:PointOptions>
</LegendPointOptionsSerializable>
                                    </dx:Series>
                                </SeriesSerializable>
                                <SeriesTemplate  
                                    >
                                    <ViewSerializable>
<dx:SideBySideBarSeriesView HiddenSerializableString="to be serialized">
                                    </dx:SideBySideBarSeriesView>
</ViewSerializable>
                                    <PointOptionsSerializable>
<dx:PointOptions HiddenSerializableString="to be serialized">
                                    </dx:PointOptions>
</PointOptionsSerializable>
                                    <LegendPointOptionsSerializable>
<dx:PointOptions HiddenSerializableString="to be serialized">
                                    </dx:PointOptions>
</LegendPointOptionsSerializable>
                                </SeriesTemplate>
                                <DiagramSerializable>
<dx:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">

</axisx>
                                    <axisy visibleinpanesserializable="-1" gridspacing="20" gridspacingauto="False" alignment="Far"
                                        title-text=""><Strips>
<dx:Strip Color="Salmon" LegendText="High Risk (0-39)" AxisLabelText="High Risk (0-39)" ShowAxisLabel="True" Name="Strip1">
<MinLimit AxisValueSerializable="0"></MinLimit>

<MaxLimit AxisValueSerializable="40"></MaxLimit>

<FillStyle FillMode="Hatch" >
<OptionsSerializable>
<dx:HatchFillOptions HatchStyle="Percent05" HiddenSerializableString="to be serialized"></dx:HatchFillOptions>
</OptionsSerializable>
</FillStyle>
</dx:Strip>
<dx:Strip Color="255, 255, 192" LegendText="Medium Risk (40-74)" AxisLabelText="Medium Risk (40-74)" ShowAxisLabel="True" Name="Strip2">
<MinLimit AxisValueSerializable="40"></MinLimit>

<MaxLimit AxisValueSerializable="75"></MaxLimit>

<FillStyle FillMode="Hatch" >
<OptionsSerializable>
<dx:HatchFillOptions HatchStyle="Percent10" HiddenSerializableString="to be serialized"></dx:HatchFillOptions>
</OptionsSerializable>
</FillStyle>
</dx:Strip>
<dx:Strip Color="192, 255, 192" LegendText="Low Risk (75-100)" AxisLabelText="Low Risk (75-100)" ShowAxisLabel="True" Name="Strip3">
<MinLimit AxisValueSerializable="75"></MinLimit>

<MaxLimit AxisValueSerializable="100"></MaxLimit>

<FillStyle FillMode="Hatch" >
<OptionsSerializable>
<dx:HatchFillOptions HatchStyle="Percent20" HiddenSerializableString="to be serialized"></dx:HatchFillOptions>
</OptionsSerializable>
</FillStyle>
</dx:Strip>
</Strips>

<Tickmarks Visible="False" MinorLength="5"></Tickmarks>

<Label Font="Tahoma, 8pt, style=Bold"></Label>

<Range Auto="False" MinValueSerializable="0" MaxValueSerializable="100" SideMarginsEnabled="True"></Range>

<NumericOptions Format="Number"></NumericOptions>
</axisy>
                                </dx:XYDiagram>
</DiagramSerializable>
                                <Legend Visible="False"></Legend>
                                <PaletteWrappers>
                                    <dx:PaletteWrapper Name="Palette 1" ScaleMode="Repeat">
                                        <Palette>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                            <dx:PaletteEntry Color="0, 128, 192" Color2="0, 128, 192"></dx:PaletteEntry>
                                        </Palette>
                                    </dx:PaletteWrapper>
                                </PaletteWrappers>
                            </dx:WebChartControl>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
                                SelectCommand="_QuestionnaireMain_ScoreTable" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter DefaultValue="0" Name="QuestionnaireId" SessionField="spVar_SafetyPlans_QuestionnaireId"
                                        Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" colspan="4" style="font-size: 10pt; height: 18px; text-align: left">
                            <asp:Label ID="lblSave" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" colspan="4" style="font-size: 10pt; height: 18px; text-align: right">
                            <dx:ASPxButton ID="btnSave" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                CssPostfix="Office2003Blue" Text="Save Assessment" OnClick="btnSave_Click" Width="130px"
                                SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            </dx:ASPxButton>
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                    <tr height="18" style="font-size: 10pt; font-family: Arial; height: 13.5pt; mso-height-source: userset">
                        <td class="xl75" style="width: 8pt; height: 18px">
                        </td>
                        <td class="xl78" style="font-size: 10pt; width: 186pt; height: 18px; text-align: right">
                        </td>
                        <td class="xl88" style="border-right: #c0c0c0; border-top: #c0c0c0; font-size: 10pt;
                            border-left: #c0c0c0; width: 95pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent">
                        </td>
                        <td class="xl77" style="border-right: #c0c0c0; border-top: #c0c0c0; border-left: #c0c0c0;
                            width: 118pt; border-bottom: #c0c0c0; height: 18px; background-color: transparent;
                            text-align: right">
                        </td>
                        <td class="xl87" style="width: 91pt; height: 18px">
                        </td>
                        <td class="xl76" style="height: 18px">
                        </td>
                    </tr>
                </table>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
    <data:CompaniesDataSource ID="CompaniesDataSourceExcludeAlcoa" runat="server" Filter="CompanyName != 'Alcoa'"
        Sort="CompanyName ASC">
    </data:CompaniesDataSource>
</asp:Content>
