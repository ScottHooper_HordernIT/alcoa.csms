﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProcurementAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UsersProcurementAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.UsersProcurementAudit, KaiZen.CSMS.Entities.UsersProcurementAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.UsersProcurementAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurementAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UsersProcurementAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UsersProcurementAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UsersProcurementAudit&gt;"/></returns>
		public static TList<UsersProcurementAudit> Fill(IDataReader reader, TList<UsersProcurementAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.UsersProcurementAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UsersProcurementAudit")
					.Append("|").Append((System.Int32)reader[((int)UsersProcurementAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UsersProcurementAudit>(
					key.ToString(), // EntityTrackingKey
					"UsersProcurementAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.UsersProcurementAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)UsersProcurementAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)UsersProcurementAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)UsersProcurementAuditColumn.AuditEventId - 1)];
					c.UsersProcurementId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.UsersProcurementId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.UsersProcurementId - 1)];
					c.UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.UserId - 1)];
					c.Enabled = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Enabled - 1)))?null:(System.Boolean?)reader[((int)UsersProcurementAuditColumn.Enabled - 1)];
					c.SiteId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.SiteId - 1)];
					c.RegionId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.RegionId - 1)];
					c.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.SupervisorUserId - 1)];
					c.Level3UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level3UserId - 1)];
					c.Level4UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level4UserId - 1)];
					c.Level5UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level5UserId - 1)];
					c.Level6UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level6UserId - 1)];
					c.Level7UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level7UserId - 1)];
					c.Level8UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level8UserId - 1)];
					c.Level9UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level9UserId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)UsersProcurementAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)UsersProcurementAuditColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.UsersProcurementAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)UsersProcurementAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)UsersProcurementAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)UsersProcurementAuditColumn.AuditEventId - 1)];
			entity.UsersProcurementId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.UsersProcurementId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.UsersProcurementId - 1)];
			entity.UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.UserId - 1)];
			entity.Enabled = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Enabled - 1)))?null:(System.Boolean?)reader[((int)UsersProcurementAuditColumn.Enabled - 1)];
			entity.SiteId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.SiteId - 1)];
			entity.RegionId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.RegionId - 1)];
			entity.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.SupervisorUserId - 1)];
			entity.Level3UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level3UserId - 1)];
			entity.Level4UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level4UserId - 1)];
			entity.Level5UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level5UserId - 1)];
			entity.Level6UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level6UserId - 1)];
			entity.Level7UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level7UserId - 1)];
			entity.Level8UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level8UserId - 1)];
			entity.Level9UserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.Level9UserId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)UsersProcurementAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)UsersProcurementAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)UsersProcurementAuditColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.UsersProcurementAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.UsersProcurementId = Convert.IsDBNull(dataRow["UsersProcurementId"]) ? null : (System.Int32?)dataRow["UsersProcurementId"];
			entity.UserId = Convert.IsDBNull(dataRow["UserId"]) ? null : (System.Int32?)dataRow["UserId"];
			entity.Enabled = Convert.IsDBNull(dataRow["Enabled"]) ? null : (System.Boolean?)dataRow["Enabled"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.SupervisorUserId = Convert.IsDBNull(dataRow["SupervisorUserId"]) ? null : (System.Int32?)dataRow["SupervisorUserId"];
			entity.Level3UserId = Convert.IsDBNull(dataRow["Level3UserId"]) ? null : (System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = Convert.IsDBNull(dataRow["Level4UserId"]) ? null : (System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = Convert.IsDBNull(dataRow["Level5UserId"]) ? null : (System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = Convert.IsDBNull(dataRow["Level6UserId"]) ? null : (System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = Convert.IsDBNull(dataRow["Level7UserId"]) ? null : (System.Int32?)dataRow["Level7UserId"];
			entity.Level8UserId = Convert.IsDBNull(dataRow["Level8UserId"]) ? null : (System.Int32?)dataRow["Level8UserId"];
			entity.Level9UserId = Convert.IsDBNull(dataRow["Level9UserId"]) ? null : (System.Int32?)dataRow["Level9UserId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurementAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersProcurementAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.UsersProcurementAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.UsersProcurementAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersProcurementAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UsersProcurementAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.UsersProcurementAudit</c>
	///</summary>
	public enum UsersProcurementAuditChildEntityTypes
	{
	}
	
	#endregion UsersProcurementAuditChildEntityTypes
	
	#region UsersProcurementAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UsersProcurementAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditFilterBuilder : SqlFilterBuilder<UsersProcurementAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilterBuilder class.
		/// </summary>
		public UsersProcurementAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementAuditFilterBuilder
	
	#region UsersProcurementAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UsersProcurementAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementAuditParameterBuilder : ParameterizedSqlFilterBuilder<UsersProcurementAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditParameterBuilder class.
		/// </summary>
		public UsersProcurementAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementAuditParameterBuilder
	
	#region UsersProcurementAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UsersProcurementAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurementAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersProcurementAuditSortBuilder : SqlSortBuilder<UsersProcurementAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementAuditSqlSortBuilder class.
		/// </summary>
		public UsersProcurementAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersProcurementAuditSortBuilder
	
} // end namespace
