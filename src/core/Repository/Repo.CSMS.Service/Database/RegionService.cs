﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IRegionService
    {
        Region Get(Expression<Func<Region, bool>> where, List<Expression<Func<Region, object>>> includes);
        List<Region> GetMany(int? top, Expression<Func<Region, bool>> where, Func<IQueryable<Region>, IOrderedQueryable<Region>> orderBy, List<Expression<Func<Region, object>>> includes);
        List<Region> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Region, bool>> where, Func<IQueryable<Region>, IOrderedQueryable<Region>> orderBy, List<Expression<Func<Region, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Region, bool>> where, List<Expression<Func<Region, object>>> includes);
        void Insert(Region item, bool saveChanges = true);
        void Update(Region item, bool saveChanges = true);
        void Delete(Region item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class RegionService : IRegionService
    {
        private readonly IRepository<Region> _repository;

        public RegionService(IRepository<Region> repository)
        {
            this._repository = repository;
        }

        public Region Get(Expression<Func<Region, bool>> where, List<Expression<Func<Region, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Region> GetMany(int? top, Expression<Func<Region, bool>> where, Func<IQueryable<Region>, IOrderedQueryable<Region>> orderBy, List<Expression<Func<Region, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Region> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Region, bool>> where, Func<IQueryable<Region>, IOrderedQueryable<Region>> orderBy, List<Expression<Func<Region, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Region, bool>> where, List<Expression<Func<Region, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Region item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Region");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Region item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Region");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Region item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Region");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
