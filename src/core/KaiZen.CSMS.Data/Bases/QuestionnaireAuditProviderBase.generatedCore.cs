﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireAudit, KaiZen.CSMS.Entities.QuestionnaireAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _QuestionnaireAudit_History_ByQuestionnaireId 
		
		/// <summary>
		///	This method wrap the '_QuestionnaireAudit_History_ByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet History_ByQuestionnaireId(System.Int32? questionnaireId)
		{
			return History_ByQuestionnaireId(null, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireAudit_History_ByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet History_ByQuestionnaireId(int start, int pageLength, System.Int32? questionnaireId)
		{
			return History_ByQuestionnaireId(null, start, pageLength , questionnaireId);
		}
				
		/// <summary>
		///	This method wrap the '_QuestionnaireAudit_History_ByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet History_ByQuestionnaireId(TransactionManager transactionManager, System.Int32? questionnaireId)
		{
			return History_ByQuestionnaireId(transactionManager, 0, int.MaxValue , questionnaireId);
		}
		
		/// <summary>
		///	This method wrap the '_QuestionnaireAudit_History_ByQuestionnaireId' stored procedure. 
		/// </summary>
		/// <param name="questionnaireId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet History_ByQuestionnaireId(TransactionManager transactionManager, int start, int pageLength , System.Int32? questionnaireId);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireAudit&gt;"/></returns>
		public static TList<QuestionnaireAudit> Fill(IDataReader reader, TList<QuestionnaireAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireAudit")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireAudit>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)QuestionnaireAuditColumn.AuditId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)QuestionnaireAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)QuestionnaireAuditColumn.AuditEventId - 1)];
					c.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.QuestionnaireId - 1)];
					c.CompanyId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.CompanyId - 1)];
					c.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.CreatedByUserId - 1)];
					c.CreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.CreatedDate - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.ModifiedDate - 1)];
					c.Status = (reader.IsDBNull(((int)QuestionnaireAuditColumn.Status - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.Status - 1)];
					c.IsReQualification = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsReQualification - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsReQualification - 1)];
					c.IsMainRequired = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsMainRequired - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsMainRequired - 1)];
					c.IsVerificationRequired = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsVerificationRequired - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsVerificationRequired - 1)];
					c.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCategoryHigh - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.InitialCategoryHigh - 1)];
					c.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialCreatedByUserId - 1)];
					c.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialCreatedDate - 1)];
					c.InitialSubmittedByuserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialSubmittedByuserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialSubmittedByuserId - 1)];
					c.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialSubmittedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialSubmittedDate - 1)];
					c.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialModifiedByUserId - 1)];
					c.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialModifiedDate - 1)];
					c.InitialStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialStatus - 1)];
					c.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialRiskAssessment - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.InitialRiskAssessment - 1)];
					c.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainCreatedByUserId - 1)];
					c.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainCreatedDate - 1)];
					c.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainModifiedByUserId - 1)];
					c.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainModifiedDate - 1)];
					c.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreExpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreExpectations - 1)];
					c.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreOverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreOverall - 1)];
					c.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreResults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreResults - 1)];
					c.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreStaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreStaffing - 1)];
					c.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreSystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreSystems - 1)];
					c.MainStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainStatus - 1)];
					c.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainAssessmentByUserId - 1)];
					c.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentComments - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentComments - 1)];
					c.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainAssessmentDate - 1)];
					c.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentRiskRating - 1)];
					c.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentStatus - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentStatus - 1)];
					c.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentValidTo - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainAssessmentValidTo - 1)];
					c.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePexpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePexpectations - 1)];
					c.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePoverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePoverall - 1)];
					c.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePresults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePresults - 1)];
					c.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePstaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePstaffing - 1)];
					c.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePsystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePsystems - 1)];
					c.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationCreatedByUserId - 1)];
					c.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.VerificationCreatedDate - 1)];
					c.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationModifiedByUserId - 1)];
					c.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.VerificationModifiedDate - 1)];
					c.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.VerificationRiskRating - 1)];
					c.VerificationStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationStatus - 1)];
					c.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.ApprovedByUserId - 1)];
					c.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.ApprovedDate - 1)];
					c.Recommended = (reader.IsDBNull(((int)QuestionnaireAuditColumn.Recommended - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.Recommended - 1)];
					c.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireAuditColumn.RecommendedComments - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.RecommendedComments - 1)];
					c.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireAuditColumn.LevelOfSupervision - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.LevelOfSupervision - 1)];
					c.ProcurementModified = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ProcurementModified - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.ProcurementModified - 1)];
					c.SubContractor = (reader.IsDBNull(((int)QuestionnaireAuditColumn.SubContractor - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.SubContractor - 1)];
					c.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.AssessedByUserId - 1)];
					c.AssessedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.AssessedDate - 1)];
					c.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireAuditColumn.LastReminderEmailSentOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.LastReminderEmailSentOn - 1)];
					c.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireAuditColumn.NoReminderEmailsSent - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.NoReminderEmailsSent - 1)];
					c.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireAuditColumn.SupplierContactVerifiedOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.SupplierContactVerifiedOn - 1)];
					c.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithActionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithActionId - 1)];
					c.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithSince - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithSince - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)QuestionnaireAuditColumn.AuditId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)QuestionnaireAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)QuestionnaireAuditColumn.AuditEventId - 1)];
			entity.QuestionnaireId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnaireId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.QuestionnaireId - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.CompanyId - 1)];
			entity.CreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.CreatedByUserId - 1)];
			entity.CreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.CreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.CreatedDate - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.ModifiedDate - 1)];
			entity.Status = (reader.IsDBNull(((int)QuestionnaireAuditColumn.Status - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.Status - 1)];
			entity.IsReQualification = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsReQualification - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsReQualification - 1)];
			entity.IsMainRequired = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsMainRequired - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsMainRequired - 1)];
			entity.IsVerificationRequired = (reader.IsDBNull(((int)QuestionnaireAuditColumn.IsVerificationRequired - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.IsVerificationRequired - 1)];
			entity.InitialCategoryHigh = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCategoryHigh - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.InitialCategoryHigh - 1)];
			entity.InitialCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialCreatedByUserId - 1)];
			entity.InitialCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialCreatedDate - 1)];
			entity.InitialSubmittedByuserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialSubmittedByuserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialSubmittedByuserId - 1)];
			entity.InitialSubmittedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialSubmittedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialSubmittedDate - 1)];
			entity.InitialModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialModifiedByUserId - 1)];
			entity.InitialModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.InitialModifiedDate - 1)];
			entity.InitialStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.InitialStatus - 1)];
			entity.InitialRiskAssessment = (reader.IsDBNull(((int)QuestionnaireAuditColumn.InitialRiskAssessment - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.InitialRiskAssessment - 1)];
			entity.MainCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainCreatedByUserId - 1)];
			entity.MainCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainCreatedDate - 1)];
			entity.MainModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainModifiedByUserId - 1)];
			entity.MainModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainModifiedDate - 1)];
			entity.MainScoreExpectations = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreExpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreExpectations - 1)];
			entity.MainScoreOverall = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreOverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreOverall - 1)];
			entity.MainScoreResults = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreResults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreResults - 1)];
			entity.MainScoreStaffing = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreStaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreStaffing - 1)];
			entity.MainScoreSystems = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScoreSystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScoreSystems - 1)];
			entity.MainStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainStatus - 1)];
			entity.MainAssessmentByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainAssessmentByUserId - 1)];
			entity.MainAssessmentComments = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentComments - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentComments - 1)];
			entity.MainAssessmentDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainAssessmentDate - 1)];
			entity.MainAssessmentRiskRating = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentRiskRating - 1)];
			entity.MainAssessmentStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentStatus - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.MainAssessmentStatus - 1)];
			entity.MainAssessmentValidTo = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainAssessmentValidTo - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.MainAssessmentValidTo - 1)];
			entity.MainScorePexpectations = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePexpectations - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePexpectations - 1)];
			entity.MainScorePoverall = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePoverall - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePoverall - 1)];
			entity.MainScorePresults = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePresults - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePresults - 1)];
			entity.MainScorePstaffing = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePstaffing - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePstaffing - 1)];
			entity.MainScorePsystems = (reader.IsDBNull(((int)QuestionnaireAuditColumn.MainScorePsystems - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.MainScorePsystems - 1)];
			entity.VerificationCreatedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationCreatedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationCreatedByUserId - 1)];
			entity.VerificationCreatedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationCreatedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.VerificationCreatedDate - 1)];
			entity.VerificationModifiedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationModifiedByUserId - 1)];
			entity.VerificationModifiedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationModifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.VerificationModifiedDate - 1)];
			entity.VerificationRiskRating = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationRiskRating - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.VerificationRiskRating - 1)];
			entity.VerificationStatus = (reader.IsDBNull(((int)QuestionnaireAuditColumn.VerificationStatus - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.VerificationStatus - 1)];
			entity.ApprovedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ApprovedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.ApprovedByUserId - 1)];
			entity.ApprovedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ApprovedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.ApprovedDate - 1)];
			entity.Recommended = (reader.IsDBNull(((int)QuestionnaireAuditColumn.Recommended - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.Recommended - 1)];
			entity.RecommendedComments = (reader.IsDBNull(((int)QuestionnaireAuditColumn.RecommendedComments - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.RecommendedComments - 1)];
			entity.LevelOfSupervision = (reader.IsDBNull(((int)QuestionnaireAuditColumn.LevelOfSupervision - 1)))?null:(System.String)reader[((int)QuestionnaireAuditColumn.LevelOfSupervision - 1)];
			entity.ProcurementModified = (reader.IsDBNull(((int)QuestionnaireAuditColumn.ProcurementModified - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.ProcurementModified - 1)];
			entity.SubContractor = (reader.IsDBNull(((int)QuestionnaireAuditColumn.SubContractor - 1)))?null:(System.Boolean?)reader[((int)QuestionnaireAuditColumn.SubContractor - 1)];
			entity.AssessedByUserId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.AssessedByUserId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.AssessedByUserId - 1)];
			entity.AssessedDate = (reader.IsDBNull(((int)QuestionnaireAuditColumn.AssessedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.AssessedDate - 1)];
			entity.LastReminderEmailSentOn = (reader.IsDBNull(((int)QuestionnaireAuditColumn.LastReminderEmailSentOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.LastReminderEmailSentOn - 1)];
			entity.NoReminderEmailsSent = (reader.IsDBNull(((int)QuestionnaireAuditColumn.NoReminderEmailsSent - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.NoReminderEmailsSent - 1)];
			entity.SupplierContactVerifiedOn = (reader.IsDBNull(((int)QuestionnaireAuditColumn.SupplierContactVerifiedOn - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.SupplierContactVerifiedOn - 1)];
			entity.QuestionnairePresentlyWithActionId = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithActionId - 1)))?null:(System.Int32?)reader[((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithActionId - 1)];
			entity.QuestionnairePresentlyWithSince = (reader.IsDBNull(((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithSince - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireAuditColumn.QuestionnairePresentlyWithSince - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.QuestionnaireId = Convert.IsDBNull(dataRow["QuestionnaireId"]) ? null : (System.Int32?)dataRow["QuestionnaireId"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.CreatedByUserId = Convert.IsDBNull(dataRow["CreatedByUserId"]) ? null : (System.Int32?)dataRow["CreatedByUserId"];
			entity.CreatedDate = Convert.IsDBNull(dataRow["CreatedDate"]) ? null : (System.DateTime?)dataRow["CreatedDate"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.Status = Convert.IsDBNull(dataRow["Status"]) ? null : (System.Int32?)dataRow["Status"];
			entity.IsReQualification = Convert.IsDBNull(dataRow["IsReQualification"]) ? null : (System.Boolean?)dataRow["IsReQualification"];
			entity.IsMainRequired = Convert.IsDBNull(dataRow["IsMainRequired"]) ? null : (System.Boolean?)dataRow["IsMainRequired"];
			entity.IsVerificationRequired = Convert.IsDBNull(dataRow["IsVerificationRequired"]) ? null : (System.Boolean?)dataRow["IsVerificationRequired"];
			entity.InitialCategoryHigh = Convert.IsDBNull(dataRow["InitialCategoryHigh"]) ? null : (System.Boolean?)dataRow["InitialCategoryHigh"];
			entity.InitialCreatedByUserId = Convert.IsDBNull(dataRow["InitialCreatedByUserId"]) ? null : (System.Int32?)dataRow["InitialCreatedByUserId"];
			entity.InitialCreatedDate = Convert.IsDBNull(dataRow["InitialCreatedDate"]) ? null : (System.DateTime?)dataRow["InitialCreatedDate"];
			entity.InitialSubmittedByuserId = Convert.IsDBNull(dataRow["InitialSubmittedByuserId"]) ? null : (System.Int32?)dataRow["InitialSubmittedByuserId"];
			entity.InitialSubmittedDate = Convert.IsDBNull(dataRow["InitialSubmittedDate"]) ? null : (System.DateTime?)dataRow["InitialSubmittedDate"];
			entity.InitialModifiedByUserId = Convert.IsDBNull(dataRow["InitialModifiedByUserId"]) ? null : (System.Int32?)dataRow["InitialModifiedByUserId"];
			entity.InitialModifiedDate = Convert.IsDBNull(dataRow["InitialModifiedDate"]) ? null : (System.DateTime?)dataRow["InitialModifiedDate"];
			entity.InitialStatus = Convert.IsDBNull(dataRow["InitialStatus"]) ? null : (System.Int32?)dataRow["InitialStatus"];
			entity.InitialRiskAssessment = Convert.IsDBNull(dataRow["InitialRiskAssessment"]) ? null : (System.String)dataRow["InitialRiskAssessment"];
			entity.MainCreatedByUserId = Convert.IsDBNull(dataRow["MainCreatedByUserId"]) ? null : (System.Int32?)dataRow["MainCreatedByUserId"];
			entity.MainCreatedDate = Convert.IsDBNull(dataRow["MainCreatedDate"]) ? null : (System.DateTime?)dataRow["MainCreatedDate"];
			entity.MainModifiedByUserId = Convert.IsDBNull(dataRow["MainModifiedByUserId"]) ? null : (System.Int32?)dataRow["MainModifiedByUserId"];
			entity.MainModifiedDate = Convert.IsDBNull(dataRow["MainModifiedDate"]) ? null : (System.DateTime?)dataRow["MainModifiedDate"];
			entity.MainScoreExpectations = Convert.IsDBNull(dataRow["MainScoreExpectations"]) ? null : (System.Int32?)dataRow["MainScoreExpectations"];
			entity.MainScoreOverall = Convert.IsDBNull(dataRow["MainScoreOverall"]) ? null : (System.Int32?)dataRow["MainScoreOverall"];
			entity.MainScoreResults = Convert.IsDBNull(dataRow["MainScoreResults"]) ? null : (System.Int32?)dataRow["MainScoreResults"];
			entity.MainScoreStaffing = Convert.IsDBNull(dataRow["MainScoreStaffing"]) ? null : (System.Int32?)dataRow["MainScoreStaffing"];
			entity.MainScoreSystems = Convert.IsDBNull(dataRow["MainScoreSystems"]) ? null : (System.Int32?)dataRow["MainScoreSystems"];
			entity.MainStatus = Convert.IsDBNull(dataRow["MainStatus"]) ? null : (System.Int32?)dataRow["MainStatus"];
			entity.MainAssessmentByUserId = Convert.IsDBNull(dataRow["MainAssessmentByUserId"]) ? null : (System.Int32?)dataRow["MainAssessmentByUserId"];
			entity.MainAssessmentComments = Convert.IsDBNull(dataRow["MainAssessmentComments"]) ? null : (System.String)dataRow["MainAssessmentComments"];
			entity.MainAssessmentDate = Convert.IsDBNull(dataRow["MainAssessmentDate"]) ? null : (System.DateTime?)dataRow["MainAssessmentDate"];
			entity.MainAssessmentRiskRating = Convert.IsDBNull(dataRow["MainAssessmentRiskRating"]) ? null : (System.String)dataRow["MainAssessmentRiskRating"];
			entity.MainAssessmentStatus = Convert.IsDBNull(dataRow["MainAssessmentStatus"]) ? null : (System.String)dataRow["MainAssessmentStatus"];
			entity.MainAssessmentValidTo = Convert.IsDBNull(dataRow["MainAssessmentValidTo"]) ? null : (System.DateTime?)dataRow["MainAssessmentValidTo"];
			entity.MainScorePexpectations = Convert.IsDBNull(dataRow["MainScorePExpectations"]) ? null : (System.Int32?)dataRow["MainScorePExpectations"];
			entity.MainScorePoverall = Convert.IsDBNull(dataRow["MainScorePOverall"]) ? null : (System.Int32?)dataRow["MainScorePOverall"];
			entity.MainScorePresults = Convert.IsDBNull(dataRow["MainScorePResults"]) ? null : (System.Int32?)dataRow["MainScorePResults"];
			entity.MainScorePstaffing = Convert.IsDBNull(dataRow["MainScorePStaffing"]) ? null : (System.Int32?)dataRow["MainScorePStaffing"];
			entity.MainScorePsystems = Convert.IsDBNull(dataRow["MainScorePSystems"]) ? null : (System.Int32?)dataRow["MainScorePSystems"];
			entity.VerificationCreatedByUserId = Convert.IsDBNull(dataRow["VerificationCreatedByUserId"]) ? null : (System.Int32?)dataRow["VerificationCreatedByUserId"];
			entity.VerificationCreatedDate = Convert.IsDBNull(dataRow["VerificationCreatedDate"]) ? null : (System.DateTime?)dataRow["VerificationCreatedDate"];
			entity.VerificationModifiedByUserId = Convert.IsDBNull(dataRow["VerificationModifiedByUserId"]) ? null : (System.Int32?)dataRow["VerificationModifiedByUserId"];
			entity.VerificationModifiedDate = Convert.IsDBNull(dataRow["VerificationModifiedDate"]) ? null : (System.DateTime?)dataRow["VerificationModifiedDate"];
			entity.VerificationRiskRating = Convert.IsDBNull(dataRow["VerificationRiskRating"]) ? null : (System.String)dataRow["VerificationRiskRating"];
			entity.VerificationStatus = Convert.IsDBNull(dataRow["VerificationStatus"]) ? null : (System.Int32?)dataRow["VerificationStatus"];
			entity.ApprovedByUserId = Convert.IsDBNull(dataRow["ApprovedByUserId"]) ? null : (System.Int32?)dataRow["ApprovedByUserId"];
			entity.ApprovedDate = Convert.IsDBNull(dataRow["ApprovedDate"]) ? null : (System.DateTime?)dataRow["ApprovedDate"];
			entity.Recommended = Convert.IsDBNull(dataRow["Recommended"]) ? null : (System.Boolean?)dataRow["Recommended"];
			entity.RecommendedComments = Convert.IsDBNull(dataRow["RecommendedComments"]) ? null : (System.String)dataRow["RecommendedComments"];
			entity.LevelOfSupervision = Convert.IsDBNull(dataRow["LevelOfSupervision"]) ? null : (System.String)dataRow["LevelOfSupervision"];
			entity.ProcurementModified = Convert.IsDBNull(dataRow["ProcurementModified"]) ? null : (System.Boolean?)dataRow["ProcurementModified"];
			entity.SubContractor = Convert.IsDBNull(dataRow["SubContractor"]) ? null : (System.Boolean?)dataRow["SubContractor"];
			entity.AssessedByUserId = Convert.IsDBNull(dataRow["AssessedByUserId"]) ? null : (System.Int32?)dataRow["AssessedByUserId"];
			entity.AssessedDate = Convert.IsDBNull(dataRow["AssessedDate"]) ? null : (System.DateTime?)dataRow["AssessedDate"];
			entity.LastReminderEmailSentOn = Convert.IsDBNull(dataRow["LastReminderEmailSentOn"]) ? null : (System.DateTime?)dataRow["LastReminderEmailSentOn"];
			entity.NoReminderEmailsSent = Convert.IsDBNull(dataRow["NoReminderEmailsSent"]) ? null : (System.Int32?)dataRow["NoReminderEmailsSent"];
			entity.SupplierContactVerifiedOn = Convert.IsDBNull(dataRow["SupplierContactVerifiedOn"]) ? null : (System.DateTime?)dataRow["SupplierContactVerifiedOn"];
			entity.QuestionnairePresentlyWithActionId = Convert.IsDBNull(dataRow["QuestionnairePresentlyWithActionId"]) ? null : (System.Int32?)dataRow["QuestionnairePresentlyWithActionId"];
			entity.QuestionnairePresentlyWithSince = Convert.IsDBNull(dataRow["QuestionnairePresentlyWithSince"]) ? null : (System.DateTime?)dataRow["QuestionnairePresentlyWithSince"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireAudit</c>
	///</summary>
	public enum QuestionnaireAuditChildEntityTypes
	{
	}
	
	#endregion QuestionnaireAuditChildEntityTypes
	
	#region QuestionnaireAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditFilterBuilder : SqlFilterBuilder<QuestionnaireAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilterBuilder class.
		/// </summary>
		public QuestionnaireAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireAuditFilterBuilder
	
	#region QuestionnaireAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditParameterBuilder class.
		/// </summary>
		public QuestionnaireAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireAuditParameterBuilder
	
	#region QuestionnaireAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireAuditSortBuilder : SqlSortBuilder<QuestionnaireAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditSqlSortBuilder class.
		/// </summary>
		public QuestionnaireAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireAuditSortBuilder
	
} // end namespace
