﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnaireVerificationSection' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaireVerificationSection 
	{
		/// <summary>			
		/// SectionResponseId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnaireVerificationSection"</remarks>
		System.Int32 SectionResponseId { get; set; }
				
		
		
		/// <summary>
		/// QuestionnaireId : 
		/// </summary>
		System.Int32  QuestionnaireId  { get; set; }
		
		/// <summary>
		/// Section1_Complete : 
		/// </summary>
		System.Boolean?  Section1Complete  { get; set; }
		
		/// <summary>
		/// Section2_Complete : 
		/// </summary>
		System.Boolean?  Section2Complete  { get; set; }
		
		/// <summary>
		/// Section3_Complete : 
		/// </summary>
		System.Boolean?  Section3Complete  { get; set; }
		
		/// <summary>
		/// Section4_Complete : 
		/// </summary>
		System.Boolean?  Section4Complete  { get; set; }
		
		/// <summary>
		/// Section5_Complete : 
		/// </summary>
		System.Boolean?  Section5Complete  { get; set; }
		
		/// <summary>
		/// Section6_Complete : 
		/// </summary>
		System.Boolean?  Section6Complete  { get; set; }
		
		/// <summary>
		/// Section7_Complete : 
		/// </summary>
		System.Boolean?  Section7Complete  { get; set; }
		
		/// <summary>
		/// Section8_Complete : 
		/// </summary>
		System.Boolean?  Section8Complete  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


