﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RegionsSitesProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RegionsSitesProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.RegionsSites, KaiZen.CSMS.Entities.RegionsSitesKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsSitesKey key)
		{
			return Delete(transactionManager, key.RegionSiteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_regionSiteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _regionSiteId)
		{
			return Delete(null, _regionSiteId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionSiteId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _regionSiteId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		FK_RegionSiteId_RegionSiteId Description: 
		/// </summary>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetByRegionId(System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(_regionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		FK_RegionSiteId_RegionSiteId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		/// <remarks></remarks>
		public TList<RegionsSites> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		FK_RegionSiteId_RegionSiteId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		fkRegionSiteIdRegionSiteId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetByRegionId(System.Int32 _regionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRegionId(null, _regionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		fkRegionSiteIdRegionSiteId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetByRegionId(System.Int32 _regionId, int start, int pageLength,out int count)
		{
			return GetByRegionId(null, _regionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionSiteId_RegionSiteId key.
		///		FK_RegionSiteId_RegionSiteId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public abstract TList<RegionsSites> GetByRegionId(TransactionManager transactionManager, System.Int32 _regionId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		FK_RegionsSites_Sites Description: 
		/// </summary>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetBySiteId(System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(_siteId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		FK_RegionsSites_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		/// <remarks></remarks>
		public TList<RegionsSites> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		FK_RegionsSites_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteId(transactionManager, _siteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		fkRegionsSitesSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetBySiteId(System.Int32 _siteId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySiteId(null, _siteId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		fkRegionsSitesSites Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_siteId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public TList<RegionsSites> GetBySiteId(System.Int32 _siteId, int start, int pageLength,out int count)
		{
			return GetBySiteId(null, _siteId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_RegionsSites_Sites key.
		///		FK_RegionsSites_Sites Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.RegionsSites objects.</returns>
		public abstract TList<RegionsSites> GetBySiteId(TransactionManager transactionManager, System.Int32 _siteId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.RegionsSites Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsSitesKey key, int start, int pageLength)
		{
			return GetByRegionSiteId(transactionManager, key.RegionSiteId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_RegionSiteId index.
		/// </summary>
		/// <param name="_regionSiteId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(System.Int32 _regionSiteId)
		{
			int count = -1;
			return GetByRegionSiteId(null,_regionSiteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RegionSiteId index.
		/// </summary>
		/// <param name="_regionSiteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(System.Int32 _regionSiteId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionSiteId(null, _regionSiteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionSiteId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(TransactionManager transactionManager, System.Int32 _regionSiteId)
		{
			int count = -1;
			return GetByRegionSiteId(transactionManager, _regionSiteId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionSiteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(TransactionManager transactionManager, System.Int32 _regionSiteId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionSiteId(transactionManager, _regionSiteId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RegionSiteId index.
		/// </summary>
		/// <param name="_regionSiteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(System.Int32 _regionSiteId, int start, int pageLength, out int count)
		{
			return GetByRegionSiteId(null, _regionSiteId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionSiteId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.RegionsSites GetByRegionSiteId(TransactionManager transactionManager, System.Int32 _regionSiteId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_RegionSiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(System.Int32 _siteId, System.Int32 _regionId)
		{
			int count = -1;
			return GetBySiteIdRegionId(null,_siteId, _regionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_RegionSiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(System.Int32 _siteId, System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdRegionId(null, _siteId, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _regionId)
		{
			int count = -1;
			return GetBySiteIdRegionId(transactionManager, _siteId, _regionId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetBySiteIdRegionId(transactionManager, _siteId, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_RegionSiteId index.
		/// </summary>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(System.Int32 _siteId, System.Int32 _regionId, int start, int pageLength, out int count)
		{
			return GetBySiteIdRegionId(null, _siteId, _regionId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_RegionSiteId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_siteId"></param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.RegionsSites GetBySiteIdRegionId(TransactionManager transactionManager, System.Int32 _siteId, System.Int32 _regionId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RegionsSites&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RegionsSites&gt;"/></returns>
		public static TList<RegionsSites> Fill(IDataReader reader, TList<RegionsSites> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.RegionsSites c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RegionsSites")
					.Append("|").Append((System.Int32)reader[((int)RegionsSitesColumn.RegionSiteId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RegionsSites>(
					key.ToString(), // EntityTrackingKey
					"RegionsSites",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.RegionsSites();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RegionSiteId = (System.Int32)reader[((int)RegionsSitesColumn.RegionSiteId - 1)];
					c.RegionId = (System.Int32)reader[((int)RegionsSitesColumn.RegionId - 1)];
					c.SiteId = (System.Int32)reader[((int)RegionsSitesColumn.SiteId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.RegionsSites"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.RegionsSites entity)
		{
			if (!reader.Read()) return;
			
			entity.RegionSiteId = (System.Int32)reader[((int)RegionsSitesColumn.RegionSiteId - 1)];
			entity.RegionId = (System.Int32)reader[((int)RegionsSitesColumn.RegionId - 1)];
			entity.SiteId = (System.Int32)reader[((int)RegionsSitesColumn.SiteId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.RegionsSites"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.RegionsSites"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.RegionsSites entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RegionSiteId = (System.Int32)dataRow["RegionSiteId"];
			entity.RegionId = (System.Int32)dataRow["RegionId"];
			entity.SiteId = (System.Int32)dataRow["SiteId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.RegionsSites"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.RegionsSites Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsSites entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RegionIdSource	
			if (CanDeepLoad(entity, "Regions|RegionIdSource", deepLoadType, innerList) 
				&& entity.RegionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RegionId;
				Regions tmpEntity = EntityManager.LocateEntity<Regions>(EntityLocator.ConstructKeyFromPkItems(typeof(Regions), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RegionIdSource = tmpEntity;
				else
					entity.RegionIdSource = DataRepository.RegionsProvider.GetByRegionId(transactionManager, entity.RegionId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RegionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RegionsProvider.DeepLoad(transactionManager, entity.RegionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RegionIdSource

			#region SiteIdSource	
			if (CanDeepLoad(entity, "Sites|SiteIdSource", deepLoadType, innerList) 
				&& entity.SiteIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SiteId;
				Sites tmpEntity = EntityManager.LocateEntity<Sites>(EntityLocator.ConstructKeyFromPkItems(typeof(Sites), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SiteIdSource = tmpEntity;
				else
					entity.SiteIdSource = DataRepository.SitesProvider.GetBySiteId(transactionManager, entity.SiteId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SiteIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SiteIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SitesProvider.DeepLoad(transactionManager, entity.SiteIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SiteIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.RegionsSites object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.RegionsSites instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.RegionsSites Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.RegionsSites entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RegionIdSource
			if (CanDeepSave(entity, "Regions|RegionIdSource", deepSaveType, innerList) 
				&& entity.RegionIdSource != null)
			{
				DataRepository.RegionsProvider.Save(transactionManager, entity.RegionIdSource);
				entity.RegionId = entity.RegionIdSource.RegionId;
			}
			#endregion 
			
			#region SiteIdSource
			if (CanDeepSave(entity, "Sites|SiteIdSource", deepSaveType, innerList) 
				&& entity.SiteIdSource != null)
			{
				DataRepository.SitesProvider.Save(transactionManager, entity.SiteIdSource);
				entity.SiteId = entity.SiteIdSource.SiteId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RegionsSitesChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.RegionsSites</c>
	///</summary>
	public enum RegionsSitesChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Regions</c> at RegionIdSource
		///</summary>
		[ChildEntityType(typeof(Regions))]
		Regions,
			
		///<summary>
		/// Composite Property for <c>Sites</c> at SiteIdSource
		///</summary>
		[ChildEntityType(typeof(Sites))]
		Sites,
		}
	
	#endregion RegionsSitesChildEntityTypes
	
	#region RegionsSitesFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RegionsSitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesFilterBuilder : SqlFilterBuilder<RegionsSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilterBuilder class.
		/// </summary>
		public RegionsSitesFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsSitesFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsSitesFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsSitesFilterBuilder
	
	#region RegionsSitesParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RegionsSitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RegionsSitesParameterBuilder : ParameterizedSqlFilterBuilder<RegionsSitesColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesParameterBuilder class.
		/// </summary>
		public RegionsSitesParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RegionsSitesParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RegionsSitesParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RegionsSitesParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RegionsSitesParameterBuilder
	
	#region RegionsSitesSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RegionsSitesColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RegionsSites"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RegionsSitesSortBuilder : SqlSortBuilder<RegionsSitesColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RegionsSitesSqlSortBuilder class.
		/// </summary>
		public RegionsSitesSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RegionsSitesSortBuilder
	
} // end namespace
