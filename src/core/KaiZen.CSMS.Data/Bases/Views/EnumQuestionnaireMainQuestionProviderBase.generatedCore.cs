﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EnumQuestionnaireMainQuestionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class EnumQuestionnaireMainQuestionProviderBaseCore : EntityViewProviderBase<EnumQuestionnaireMainQuestion>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainQuestion&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;EnumQuestionnaireMainQuestion&gt;"/></returns>
		protected static VList&lt;EnumQuestionnaireMainQuestion&gt; Fill(DataSet dataSet, VList<EnumQuestionnaireMainQuestion> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<EnumQuestionnaireMainQuestion>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;EnumQuestionnaireMainQuestion&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<EnumQuestionnaireMainQuestion>"/></returns>
		protected static VList&lt;EnumQuestionnaireMainQuestion&gt; Fill(DataTable dataTable, VList<EnumQuestionnaireMainQuestion> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					EnumQuestionnaireMainQuestion c = new EnumQuestionnaireMainQuestion();
					c.QuestionNo = (Convert.IsDBNull(row["QuestionNo"]))?string.Empty:(System.String)row["QuestionNo"];
					c.QuestionNo2 = (Convert.IsDBNull(row["QuestionNo2"]))?string.Empty:(System.String)row["QuestionNo2"];
					c.QuestionText = (Convert.IsDBNull(row["QuestionText"]))?string.Empty:(System.String)row["QuestionText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;EnumQuestionnaireMainQuestion&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;EnumQuestionnaireMainQuestion&gt;"/></returns>
		protected VList<EnumQuestionnaireMainQuestion> Fill(IDataReader reader, VList<EnumQuestionnaireMainQuestion> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					EnumQuestionnaireMainQuestion entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<EnumQuestionnaireMainQuestion>("EnumQuestionnaireMainQuestion",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new EnumQuestionnaireMainQuestion();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionNo = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionNo)];
					//entity.QuestionNo = (Convert.IsDBNull(reader["QuestionNo"]))?string.Empty:(System.String)reader["QuestionNo"];
					entity.QuestionNo2 = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionNo2)];
					//entity.QuestionNo2 = (Convert.IsDBNull(reader["QuestionNo2"]))?string.Empty:(System.String)reader["QuestionNo2"];
					entity.QuestionText = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionText)];
					//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainQuestion"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainQuestion"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, EnumQuestionnaireMainQuestion entity)
		{
			reader.Read();
			entity.QuestionNo = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionNo)];
			//entity.QuestionNo = (Convert.IsDBNull(reader["QuestionNo"]))?string.Empty:(System.String)reader["QuestionNo"];
			entity.QuestionNo2 = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionNo2)];
			//entity.QuestionNo2 = (Convert.IsDBNull(reader["QuestionNo2"]))?string.Empty:(System.String)reader["QuestionNo2"];
			entity.QuestionText = (System.String)reader[((int)EnumQuestionnaireMainQuestionColumn.QuestionText)];
			//entity.QuestionText = (Convert.IsDBNull(reader["QuestionText"]))?string.Empty:(System.String)reader["QuestionText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="EnumQuestionnaireMainQuestion"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="EnumQuestionnaireMainQuestion"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, EnumQuestionnaireMainQuestion entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionNo = (Convert.IsDBNull(dataRow["QuestionNo"]))?string.Empty:(System.String)dataRow["QuestionNo"];
			entity.QuestionNo2 = (Convert.IsDBNull(dataRow["QuestionNo2"]))?string.Empty:(System.String)dataRow["QuestionNo2"];
			entity.QuestionText = (Convert.IsDBNull(dataRow["QuestionText"]))?string.Empty:(System.String)dataRow["QuestionText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region EnumQuestionnaireMainQuestionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionFilterBuilder : SqlFilterBuilder<EnumQuestionnaireMainQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionFilterBuilder

	#region EnumQuestionnaireMainQuestionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EnumQuestionnaireMainQuestionParameterBuilder : ParameterizedSqlFilterBuilder<EnumQuestionnaireMainQuestionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EnumQuestionnaireMainQuestionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EnumQuestionnaireMainQuestionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EnumQuestionnaireMainQuestionParameterBuilder
	
	#region EnumQuestionnaireMainQuestionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="EnumQuestionnaireMainQuestion"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EnumQuestionnaireMainQuestionSortBuilder : SqlSortBuilder<EnumQuestionnaireMainQuestionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EnumQuestionnaireMainQuestionSqlSortBuilder class.
		/// </summary>
		public EnumQuestionnaireMainQuestionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EnumQuestionnaireMainQuestionSortBuilder

} // end namespace
