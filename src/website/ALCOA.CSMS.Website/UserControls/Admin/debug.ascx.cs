﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using System.Configuration;

using System.Data;
using System.Data.SqlClient;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;

using System.Web.Configuration;

namespace ALCOA.CSMS.Website.UserControls.Admin
{
    public partial class debug : System.Web.UI.UserControl
    {
        Auth auth = new Auth();
        static string sqldb = "";

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (auth.RoleId != (int)RoleList.Administrator) Response.Redirect("default.aspx");

            if (!postBack)
            {
                if (Request.QueryString["q"] != null)
                {
                    if (Request.QueryString["debug"].ToString() == "1")
                    {
                        ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                        Configuration config = new Configuration();
                        //if (config.GetValue(ConfigList.Debug))
                        Response.Write(String.Format("Connecting via:{0}/{1} [{2}]<br /><br />Using: {3}",
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_domain"),
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_user"),
                                                        WebConfigurationManager.AppSettings.Get("WAOCSM_pass"),
                                                        conString.ToString()));
                    }
                }
            }
        }
    }
}