using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.Library;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;

public partial class UserControls_SqQuestionnaire_Questionnaire2Supplier : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    const string forcedHigh = "Forced High Risk";
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }
    
    private void moduleInit(bool postBack)
    {
        switch (SessionHandler.spVar_Questionnaire_FrontPageLink)
        {
            case "QuestionnaireReport_Overview":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Qualified Contractors";
                hlQuestionnaire.ToolTip = "Go back to Qualified Contractors Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQOverview.aspx";
                break;
            case "QuestionnaireReport_Expire":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Expiry Date";
                hlQuestionnaire.ToolTip = "Go back to SQ Expiry Date Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQ.aspx?q=expire";
                break;
            case "QuestionnaireReport_Progress":
                hlQuestionnaire.Text = "Australian Safety Qualification Information > Status";
                hlQuestionnaire.ToolTip = "Go back to SQ Status Page";
                hlQuestionnaire.NavigateUrl = "~/Reports_SafetyPQStatus.aspx";
                break;
            case "EbiContractorsOnSite":
                hlQuestionnaire.Text = "Reports / Charts > Compliance Reports > Contractors On-Site (EBI)";
                hlQuestionnaire.ToolTip = "Go back to Contractors On-Site (EBI) Page";
                hlQuestionnaire.NavigateUrl = "~/NonComplianceEbiOnSite.aspx";
                break;
            default: //Questionnaire
                hlQuestionnaire.Text = "Australian Safety Qualification Process";
                hlQuestionnaire.ToolTip = "Go back to Safety Qualification Page";
                hlQuestionnaire.NavigateUrl = "~/SafetyPQ_Questionnaire.aspx";
                break;
        }

        LoadQuestionsAnswersText();

        SitesDataSource.DataBind();
        qAnswer30E1.DataSourceID = "SitesDataSource";
        qAnswer30E1.ValueField = "SiteId";
        qAnswer30E1.TextField = "SiteName";
        qAnswer30E1.ValueType = typeof(int);
        qAnswer30E1.DataBind();
        qAnswer30F1.DataSourceID = "SitesDataSource";
        qAnswer30F1.ValueField = "SiteId";
        qAnswer30F1.TextField = "SiteName";
        qAnswer30F1.ValueType = typeof(int);
        qAnswer30F1.DataBind();
        qAnswer30G1.DataSourceID = "SitesDataSource";
        qAnswer30G1.ValueField = "SiteId";
        qAnswer30G1.TextField = "SiteName";
        qAnswer30G1.ValueType = typeof(int);
        qAnswer30G1.DataBind();

        if (!postBack)
        {
            bool print = false;
            //hide assessor panels.
            rpAssess1.Visible = false;
            rpAssess11.Visible = false;
            rpAssess12.Visible = false;
            rpAssess13.Visible = false;
            rpAssess14.Visible = false;
            rpAssess15.Visible = false;
            rpAssess16.Visible = false;
            rpAssess17.Visible = false;
            rpAssess18.Visible = false;
            rpAssess19.Visible = false;
            rpAssess20.Visible = false;
            rpAssess21.Visible = false;
            rpAssess22.Visible = false;
            rpAssess23.Visible = false;
            rpAssess24.Visible = false;
            rpAssess25.Visible = false;
            rpAssess26.Visible = false;
            rpAssess27.Visible = false;
            rpAssess28.Visible = false;
            rpAssess29i.Visible = false;
            rpAssess29ii.Visible = false;
            rpAssess30.Visible = false;

            //qAnswer1.Enabled = false;
            string q = "";
            SessionHandler.spVar_Questionnaire_Category = "";

            try
            {
                q = Request.QueryString["q"];

                if (Request.QueryString["printlayout"] != null)
                {
                    if (Request.QueryString["printlayout"] == "yes")
                    {
                        print = true;
                    }
                }

                LinkButton1.PostBackUrl = String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + q));

                if (q != "New")
                { //Load Answers
                    QuestionnaireService _qs = new QuestionnaireService();
                    Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));

                    if (_q == null)
                    {
                        throw new Exception("Invalid Questionnaire Id");
                    }
                    else
                    {
                        if (_q.SubContractor == true)
                        {


                            if (auth.RoleId == (int)RoleList.PreQual)
                            {
                                hlQuestionnaire.Text = "WA SubContractor Process";
                                hlQuestionnaire.ToolTip = "Go back to WA SubContractor Process Page";
                                hlQuestionnaire.NavigateUrl = "~/Default.aspx";
                            }
                        }

                        qAnswer1.Date = _q.CreatedDate;
                        qAnswer3.Value = _q.CompanyId;
                        qAnswer3.Enabled = false;

                        AutoFill_CompanyDetails(_q.CompanyId);

                        QuestionnaireStatus qs = new QuestionnaireStatus();
                        QuestionnaireStatusService qss = new QuestionnaireStatusService();
                        qs = qss.GetByQuestionnaireStatusId(_q.MainStatus);
                        lblStatus.Text = qs.QuestionnaireStatusDesc;
                        if (qs.QuestionnaireStatusDesc == "Being Assessed") lblStatus.Text = "Complete";

                        //bool NotContractor = false;
                        //if (auth.RoleId == (int)RoleList.PreQual || auth.RoleId == (int)RoleList.Contractor) NotContractor = true;
                        
                        if (_q.MainStatus >= (int)QuestionnaireStatusList.BeingAssessed || _q.IsReQualification)
                        {
                            SetInivisbleAllGetHelpOverlayDivs();
                        }

                        //if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed || _q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        if (_q.Status == (int)QuestionnaireStatusList.BeingAssessed && _q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
                        {
                            btnSubmit.Enabled = false;
                            if (auth.RoleId != (int)RoleList.Administrator && !Helper.General.isEhsConsultant(auth.UserId))
                            {
                                btnSave.Enabled = false;
                                ASPxButton2.Enabled = false;
                            }
                        }
                        CompaniesService cService = new CompaniesService();
                        Companies c = cService.GetByCompanyId(_q.CompanyId);
                        if (_q.Status == (int)QuestionnaireStatusList.AssessmentComplete
                            || c.Deactivated == true
                            || c.CompanyStatusId == (int)CompanyStatusList.InActive_SQ_Incomplete
                                || c.CompanyStatusId == (int)CompanyStatusList.InActive_No_Requal_Required )
                        {
                            ReadOnly(true);
                            if (c.Deactivated == true)
                            {
                                lblSave.Text += " - Company De-Activated";
                            }
                            //qAnswer11A.Columns[0].Visible = false;
                            //qAnswer11A.Enabled = false;
                            ASPxCheckBox1.Checked = true;
                            ASPxCheckBox1.Enabled = false;
                            qAnswer12AExampleText.Text = "The following file is currently uploaded:";
                            qAnswer17BExampleText.Text = "The following file is currently uploaded:";
                            qAnswer22ExampleText.Text = "The following file is currently uploaded:";
                            qAnswer23ExampleText.Text = "The following file is currently uploaded:";
                            qAnswer25AExampleText.Text = "The following file is currently uploaded:";
                            qAnswer25BExampleText.Text = "The following file is currently uploaded:";
                            qAnswer25CExampleText.Text = "The following file is currently uploaded:";
                            qAnswer26AExampleText.Text = "The following file is currently uploaded:";
                            qAnswer28LetterText.Text = "The following file is currently uploaded:"; //DT232 Cindi Thornton - new upload functionality
                            qAnswer29iiAExampleText.Text = "The following file is currently uploaded:";
                        }
                        int count = 0;
                        QuestionnaireMainResponseFilters qmrFilters = new QuestionnaireMainResponseFilters();
                        qmrFilters.Append(QuestionnaireMainResponseColumn.QuestionnaireId, _q.QuestionnaireId.ToString());
                        TList<QuestionnaireMainResponse> qmrTList =
                                DataRepository.QuestionnaireMainResponseProvider.GetPaged(qmrFilters.ToString(), null, 0, 100, out count);
                        if (count > 0)
                        {
                            LoadAnswers(Convert.ToInt32(q));
                        }
                        else
                        {
                            QuestionnaireMainResponseService qmrService = new QuestionnaireMainResponseService();
                            QuestionnaireMainResponse qmr = qmrService.GetByQuestionnaireIdAnswerId(Convert.ToInt32(q), Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "B"));
                            if (qmr == null)
                            {
                                QuestionnaireInitialResponseService qirService = new QuestionnaireInitialResponseService();
                                QuestionnaireInitialResponse qir = qirService.GetByQuestionnaireIdQuestionId(Convert.ToInt32(q), "4_1");
                                if (qir != null)
                                {
                                    qAnswer11B.Text = qir.AnswerText;
                                }
                            }
                        }

                        try
                        {
                            if (Request.QueryString["s"].ToString() == "1") lblSave.Text = "Saved Successfully.";
                        }
                        catch { }

                        qAnswer3.Enabled = false; //Disable Company field from being changed.

                        LoadAssessment(_q.QuestionnaireId);

                        AssessmentView(_q.QuestionnaireId, _q.Status, _q.MainStatus);
                    }
                }
                SetYears(DateTime.Now.Year);
                //qAnswer12C.Attributes["style"] = "visibility:hidden ";

                

                switch (auth.RoleId)
                {
                    case ((int)RoleList.Reader):
                        //Readers can do as they wish..
                        //btnSave.Enabled = false;
                        //btnSubmit.Enabled = false;
                        SetInivisbleAllGetHelpOverlayDivs();
                        break;
                    case ((int)RoleList.Contractor):
                        break;
                    case ((int)RoleList.Administrator):
                        SetInivisbleAllGetHelpOverlayDivs();
                        break;
                    case ((int)RoleList.PreQual):
                        break;
                    default:
                        // RoleList.Disabled or No Role
                        Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                        break;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext();
                Response.Redirect(String.Format("AccessDenied.aspx?error={0}", ex.Message));
            }

            if (print)
            {
                //disable save options etc.
                btnHome.Enabled = false;
                btnSave.Enabled = false;
                btnSaveAssessment.Enabled = false;
                btnSaveAssessmentGoHome.Enabled = false;
                btnSaveAssessment2.Enabled = false;
                btnSaveAssessment2GoHome.Enabled = false;
                btnSubmit.Enabled = false;
                pnlPrint.Visible = false;
                ASPxButton1.Enabled = false;
                ASPxButton2.Enabled = false;
                ASPxButton3.Enabled = false;
                ReadOnly(true);
            }
        }
    }

    protected void ShowAssessorReviewCommentsPanel()
    {
        if (auth.RoleId == (int)RoleList.Contractor || auth.RoleId == (int)RoleList.PreQual)
        {
            //Read Only.
            
        }
        else
        {
            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
            {

            }
        }
    }

    protected void cmbCompanies_Callback(object source, CallbackEventArgsBase e)
    {
        AutoFill_CompanyDetails(Convert.ToInt32(e.Parameter));
    }

    protected void q3Answer_SelectedIndexChanged(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(1000);
        AutoFill_CompanyDetails(Convert.ToInt32(qAnswer3.SelectedItem.Value));
    }

    protected void AutoFill()
    {
        qAnswer3.Value = auth.CompanyId;
        AutoFill_CompanyDetails(auth.CompanyId);
    }
    protected void AutoFill_CompanyDetails(int CompanyId)
    {
        //SessionHandler.spVar_CompanyId = CompanyId.ToString();
        CompaniesService cs = new CompaniesService();
        Companies c = cs.GetByCompanyId(CompanyId);
        qAnswer4.Text = c.CompanyAbn;
        qAnswer5A.Text = c.AddressBusiness;
        //q5Answer2.Text = c.Site;
    }

    protected void LoadQuestionsAnswersText()
    {
        EnumQuestionnaireMainQuestionAnswerService qqas = new EnumQuestionnaireMainQuestionAnswerService();
        VList<EnumQuestionnaireMainQuestionAnswer> qqaList = qqas.GetAll();
        EnumQuestionnaireMainQuestionAnswer qa;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "01");
        qText1.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "02");
        qText2.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "03");
        qText3.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "04");
        qText4.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "05A");
        qText5.Text = qa.QuestionText;
        qText5A.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "05B");
        qText5B.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "05C");
        qText5C.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "05D");
        qText5D.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "05E");
        qText5E.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "06");
        qText6.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "07");
        qText7.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "08");
        qText8.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "09");
        qText9.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "10");
        qText10.Text = qa.QuestionText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "11A");
        qText11.Text = qa.QuestionText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "11B");
        qAnswerText11B.Text = qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "12A");
        qText12.Text = qa.QuestionText;
        ListEditItem q12A = qAnswer12.Items.FindByValue("A");
        q12A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "12B");
        ListEditItem q12B = qAnswer12.Items.FindByValue("B");
        q12B.Text = "b) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "13A");
        qText13.Text = qa.QuestionText;
        ListEditItem q13A = qAnswer13.Items.FindByValue("A");
        q13A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "13B");
        ListEditItem q13B = qAnswer13.Items.FindByValue("B");
        q13B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "13C");
        ListEditItem q13C = qAnswer13.Items.FindByValue("C");
        q13C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "13D");
        ListEditItem q13D = qAnswer13.Items.FindByValue("D");
        q13D.Text = "d) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "13E");
        ListEditItem q13E = qAnswer13.Items.FindByValue("E");
        q13E.Text = "e) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "14A");
        qText14.Text = qa.QuestionText;
        qAnswerText14A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "14B");
        qAnswerText14B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "14C");
        qAnswerText14C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "14D");
        qAnswerText14D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "15A");
        qText15.Text = qa.QuestionText;
        qAnswerText15A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "15B");
        qAnswerText15B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "15C");
        qAnswerText15C.Text = "c) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "16A");
        qText16.Text = qa.QuestionText;
        qAnswerText16A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "16B");
        qAnswerText16B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "16C");
        qAnswerText16C.Text = "c) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "17A");
        qText17.Text = qa.QuestionText;
        qAnswerText17A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "17B");
        qAnswerText17B.Text = "b) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "18A");
        qText18.Text = qa.QuestionText;
        ListEditItem q18A = qAnswer18.Items.FindByValue("A");
        q18A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "18B");
        ListEditItem q18B = qAnswer18.Items.FindByValue("B");
        q18B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "18C");
        ListEditItem q18C = qAnswer18.Items.FindByValue("C");
        q18C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "18D");
        ListEditItem q18D = qAnswer18.Items.FindByValue("D");
        q18D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "19A");
        qText19.Text = qa.QuestionText;
        ListEditItem q19A = qAnswer19.Items.FindByValue("A");
        q19A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "19B");
        ListEditItem q19B = qAnswer19.Items.FindByValue("B");
        q19B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "19C");
        ListEditItem q19C = qAnswer19.Items.FindByValue("C");
        q19C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "19D");
        ListEditItem q19D = qAnswer19.Items.FindByValue("D");
        q19D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "20A");
        qText20.Text = qa.QuestionText;
        qAnswerText20A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "20B");
        qAnswerText20B.Text = "b) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "21A");
        qText21.Text = qa.QuestionText;
        ListEditItem q21A = qAnswer21.Items.FindByValue("A");
        q21A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "21B");
        ListEditItem q21B = qAnswer21.Items.FindByValue("B");
        q21B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "21C");
        ListEditItem q21C = qAnswer21.Items.FindByValue("C");
        q21C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "21D");
        ListEditItem q21D = qAnswer21.Items.FindByValue("D");
        q21D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "22A");
        qText22.Text = qa.QuestionText;
        qAnswerText22A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "22B");
        qAnswerText22B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "22C");
        qAnswerText22C.Text = "c) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "23A");
        qText23.Text = qa.QuestionText;
        ListEditItem q23A = qAnswer23.Items.FindByValue("A");
        q23A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "23B");
        ListEditItem q23B = qAnswer23.Items.FindByValue("B");
        q23B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "23C");
        ListEditItem q23C = qAnswer23.Items.FindByValue("C");
        q23C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "23D");
        ListEditItem q23D = qAnswer23.Items.FindByValue("D");
        q23D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "24A");
        qText24.Text = qa.QuestionText;
        ListEditItem q24A = qAnswer24.Items.FindByValue("A");
        q24A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "24B");
        ListEditItem q24B = qAnswer24.Items.FindByValue("B");
        q24B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "24C");
        ListEditItem q24C = qAnswer24.Items.FindByValue("C");
        q24C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "24D");
        ListEditItem q24D = qAnswer24.Items.FindByValue("D");
        q24D.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "25A");
        qText25.Text = qa.QuestionText;
        qAnswerText25A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "25B");
        qAnswerText25B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "25C");
        qAnswerText25C.Text = "c) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "26A");
        qText26.Text = qa.QuestionText;
        if(!String.IsNullOrEmpty(qa.AnswerText)) qAnswerText26A.Text = "a) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "27A1");
        qText27.Text = qa.QuestionText;
        qAnswerText27A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "27B1");
        qAnswerText27B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "27C1");
        qAnswerText27C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "27D1");
        qAnswerText27D.Text = "d) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "27E1");
        qAnswerText27E.Text = "e) " + qa.AnswerText;
        //load years TODO
        //qAnswer27Year3.Text = DateTime.Now.Year.ToString();
        //qAnswer27Year2.Text = (DateTime.Now.Year - 1).ToString();
        //qAnswer27Year1.Text = (DateTime.Now.Year - 2).ToString();

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A1");
        qText28.Text = qa.QuestionText;
        qAnswerText28A1.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A2");
        qAnswerText28A2.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A3");
        qAnswerText28A3.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A4");

        ////--Q/A Removed per e-mail request 22-Apr-2009
        ////--
        //qAnswerText28A4.Text = qa.AnswerText;
        //qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A5");
        //qAnswerText28A5.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "28A6");
        qAnswerText28A6.Text = qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "29iA");

        //Start:Enhancement_016 by Debashis

        string q = "";
        q = Request.QueryString["q"];
        QuestionnaireService _qs = new QuestionnaireService();
        Questionnaire _q = _qs.GetByQuestionnaireId(Convert.ToInt32(q));
        if (_q.MainStatus == 1)
        {
            qText29i.Text = "In the last 3 years did your company, or any of its directors, officers or related bodies corporate, receive any health or safety related notices, direction, order, demand, fine or penalty, that is in any way related to serious, repeat or criminal issues?";
        }
        else
        {
            qText29i.Text = qa.QuestionText;
        }
        //End:Enhancement_016 by Debashis

        qAnswerText29iA.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "29iB");
        qAnswerText29iB.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "29iC");
        qAnswerText29iC.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "29iD");
        qAnswerText29iD.Text = "d) " + qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "29iiA");
        qText29ii.Text = qa.QuestionText;
        qAnswerText29iiA.Text = qa.AnswerText;

        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30A");
        qText30.Text = qa.QuestionText;
        ListEditItem q30A = qAnswer30.Items.FindByValue("A");
        q30A.Text = "a) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30B");
        ListEditItem q30B = qAnswer30.Items.FindByValue("B");
        q30B.Text = "b) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30C");
        ListEditItem q30C = qAnswer30.Items.FindByValue("C");
        q30C.Text = "c) " + qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30D");
        qAnswerText30D.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30E1");
        qAnswerText30E1.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30E2");
        qAnswerText30E2.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30E3");
        qAnswerText30E3.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30E3B");
        qAnswerText30E3B.Text = qa.AnswerText;
        qa = qqaList.Find(EnumQuestionnaireMainQuestionAnswerColumn.QuestionAnswerNo, "30E4");
        qAnswerText30E4.Text = qa.AnswerText;
    }

    protected void Save()
    {
        try
        {
            string q = Request.QueryString["q"].ToString();
            QuestionnaireService qs = new QuestionnaireService();
            Questionnaire qu = qs.GetByQuestionnaireId(Convert.ToInt32(q));
            qu.MainModifiedByUserId = auth.UserId;
            qu.MainModifiedDate = DateTime.Now;
            //qu.ModifiedDate = DateTime.Now;
            //qu.ModifiedByUserId = auth.UserId;
            if (qu.MainStatus != (int)QuestionnaireStatusList.BeingAssessed)
            {
                qu.MainStatus = (int)QuestionnaireStatusList.Incomplete;
            }

            qs.Save(qu);
            SaveAnswers(Convert.ToInt32(qu.QuestionnaireId.ToString()));
            SessionHandler.spVar_SafetyPlans_QuestionnaireId = qu.QuestionnaireId.ToString();
            lblSave.Text = "Saved Successfully. Click Home to return to the list of Questionnaires.";
            Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireMain.aspx{0}", QueryStringModule.Encrypt(String.Format("q={0}&s=1", qu.QuestionnaireId))), true);
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            lblSave.Text = String.Format("Save Unsuccessful. Error: {0}", ex.Message);
        }
    }
    protected void LoadAnswers(int q)
    {

        SessionHandler.spVar_SafetyPlans_QuestionnaireId = q.ToString();
        //qAnswer1.Text = 
        qAnswer1.Date = DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy",
                                                System.Globalization.CultureInfo.InvariantCulture);
        qAnswer2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", ""));

        if (!String.IsNullOrEmpty(qAnswer2.Text)) div1.Visible = false;

        try
        {
            if (!String.IsNullOrEmpty(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""))))
                qAnswer3.Value = Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", "")));
        }
        catch { }
        qAnswer4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", ""));
        qAnswer5A.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A"));
        qAnswer5B.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B"));
        qAnswer5C.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C"));
        qAnswer5D.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D"));
        qAnswer5E.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E"));
        qAnswer6.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", ""));
        qAnswer7.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", ""));
        qAnswer8.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", ""));
        qAnswer9.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", ""));
        qAnswer10.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", ""));

        string selected = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "A"));
        SessionHandler.spVar_Questionnaire_Category = selected;
        if (!String.IsNullOrEmpty(selected)) div11.Visible = false;

        qAnswer11B.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "B"));

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "A")) == "Yes") { qAnswer12.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "B")) == "Yes") { qAnswer12.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "C")) == "Yes") { qAnswer12.Value = "C"; };
        if (qAnswer12.Value != null) div12.Visible = false;

        qAnswer12Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments"));

        LoadAttachment(q, "12", "AExample", qAnswer12AExampleText, qAnswer12AExampleLink);

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "A")) == "Yes") { qAnswer13.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "B")) == "Yes") { qAnswer13.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "C")) == "Yes") { qAnswer13.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "D")) == "Yes") { qAnswer13.Value = "D"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "E")) == "Yes") { qAnswer13.Value = "E"; };
        if (qAnswer13.Value != null) div13.Visible = false;

        qAnswer13Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments"));

        qAnswer14A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "A"));
        qAnswer14B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "B"));
        qAnswer14C.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "C"));
        qAnswer14D.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "D"));
        if (qAnswer14A.Value != null || qAnswer14B.Value != null || qAnswer14C.Value != null | qAnswer14D.Value != null) div14.Visible = false;

        qAnswer14Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments"));

        qAnswer15A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "A"));
        qAnswer15AComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments"));
        qAnswer15B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "B"));
        qAnswer15BComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments"));
        qAnswer15C.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "C"));
        qAnswer15CComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments"));
        if (qAnswer15A.Value != null || qAnswer15B.Value != null || qAnswer15C.Value != null) div15.Visible = false;

        qAnswer16A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "A"));
        try //Added. 20-JUL-2010.
        {
            qAnswer16AComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "AComments"));
        }
        catch (Exception) { Elmah.ErrorSignal.FromCurrentContext(); }
        qAnswer16B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "B"));
        qAnswer16BComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments"));
        qAnswer16C.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "C"));
        qAnswer16CComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments"));
        if (qAnswer16A.Value != null || qAnswer16B.Value != null || qAnswer16C.Value != null) div16.Visible = false;

        qAnswer17A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "A"));
        qAnswer17B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "B"));
        if (qAnswer17A.Value != null || qAnswer17B.Value != null) div17.Visible = false;
        LoadAttachment(q, "17", "BExample", qAnswer17BExampleText, qAnswer17BExampleLink);

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "A")) == "Yes") { qAnswer18.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "B")) == "Yes") { qAnswer18.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "C")) == "Yes") { qAnswer18.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "D")) == "Yes") { qAnswer18.Value = "D"; };
        if (qAnswer18.Value != null) div18.Visible = false;
        qAnswer18Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments"));

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "A")) == "Yes") { qAnswer19.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "B")) == "Yes") { qAnswer19.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "C")) == "Yes") { qAnswer19.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "D")) == "Yes") { qAnswer19.Value = "D"; };
        if (qAnswer19.Value != null) div19.Visible = false;
        qAnswer19Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments"));

        qAnswer20A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "A"));
        qAnswer20AComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments"));
        qAnswer20B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "B"));
        qAnswer20BComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments"));
        if (qAnswer20A.Value != null || qAnswer20B.Value != null) div20.Visible = false;

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "A")) == "Yes") { qAnswer21.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "B")) == "Yes") { qAnswer21.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "C")) == "Yes") { qAnswer21.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "D")) == "Yes") { qAnswer21.Value = "D"; };
        if (qAnswer21.Value != null) div21.Visible = false;
        qAnswer21Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments"));

        qAnswer22A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "A"));
        qAnswer22B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "B"));
        qAnswer22C.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "C"));
        if (qAnswer22A.Value != null || qAnswer22B.Value != null || qAnswer22C.Value != null) div22.Visible = false;
        qAnswer22Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments"));
        try //Added. 20-JUL-2010
        {
            LoadAttachment(q, "22", "Example", qAnswer22ExampleText, qAnswer22ExampleLink);
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "A")) == "Yes") { qAnswer23.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "B")) == "Yes") { qAnswer23.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "C")) == "Yes") { qAnswer23.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "D")) == "Yes") { qAnswer23.Value = "D"; };
        if (qAnswer23.Value != null) div23.Visible = false;
        qAnswer23Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments"));
        LoadAttachment(q, "23", "Example", qAnswer23ExampleText, qAnswer23ExampleLink);

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "A")) == "Yes") { qAnswer24.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "B")) == "Yes") { qAnswer24.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "C")) == "Yes") { qAnswer24.Value = "C"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "D")) == "Yes") { qAnswer24.Value = "D"; };
        if (qAnswer24.Value != null) div24.Visible = false;
        qAnswer24Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments"));

        qAnswer25A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "A"));
        LoadAttachment(q, "25", "AExample", qAnswer25AExampleText, qAnswer25AExampleLink);
        qAnswer25B.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "B"));
        LoadAttachment(q, "25", "BExample", qAnswer25BExampleText, qAnswer25BExampleLink);
        qAnswer25C.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "C"));
        LoadAttachment(q, "25", "CExample", qAnswer25CExampleText, qAnswer25CExampleLink);
        if (qAnswer25A.Value != null || qAnswer25B.Value != null || qAnswer25C.Value != null) div25.Visible = false;

        qAnswer26A.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "A"));
        if (qAnswer26A.Value != null) div26.Visible = false;
        qAnswer26AComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments"));
        LoadAttachment(q, "26", "AExample", qAnswer26AExampleText, qAnswer26AExampleLink);

        qAnswer27A1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1"));
        qAnswer27A2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2"));
        qAnswer27A3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3"));
        qAnswer27B1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1"));
        qAnswer27B2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2"));
        qAnswer27B3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3"));
        qAnswer27C1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1"));
        qAnswer27C2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2"));
        qAnswer27C3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3"));
        qAnswer27D1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1"));
        qAnswer27D2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2"));
        qAnswer27D3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3"));
        qAnswer27E1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1"));
        qAnswer27E2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2"));
        qAnswer27E3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3"));
        qAnswer27Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "Comments")); //DT 224 Cindi Thornton 14/9/15
        if (!String.IsNullOrEmpty(qAnswer27A1.Text) || !String.IsNullOrEmpty(qAnswer27A2.Text) || !String.IsNullOrEmpty(qAnswer27A3.Text) ||
            !String.IsNullOrEmpty(qAnswer27B1.Text) || !String.IsNullOrEmpty(qAnswer27B2.Text) || !String.IsNullOrEmpty(qAnswer27B3.Text) ||
            !String.IsNullOrEmpty(qAnswer27C1.Text) || !String.IsNullOrEmpty(qAnswer27C2.Text) || !String.IsNullOrEmpty(qAnswer27C3.Text) ||
            !String.IsNullOrEmpty(qAnswer27D1.Text) || !String.IsNullOrEmpty(qAnswer27D2.Text) || !String.IsNullOrEmpty(qAnswer27D3.Text) ||
            !String.IsNullOrEmpty(qAnswer27E1.Text) || !String.IsNullOrEmpty(qAnswer27E2.Text) || !String.IsNullOrEmpty(qAnswer27E3.Text) ||
            !String.IsNullOrEmpty(qAnswer27Comments.Text)) //DT 224 Cindi Thornton 25/9/15
        {
            div27.Visible = false;
        }

        



        qAnswer28A1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A1"));
        qAnswer28A2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2"));
        qAnswer28A3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3"));
        ////--Q/A Removed per e-mail request 22-Apr-2009
        ////--
        //qAnswer28A4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A4"));
        //qAnswer28A5.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A5"));
        qAnswer28A6.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6"));
        qAnswer28B1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B1"));
        qAnswer28B2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2"));
        qAnswer28B3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3"));
        //qAnswer28B4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B4"));
        //qAnswer28B5.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B5"));
        qAnswer28B6.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6"));
        qAnswer28C1.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C1"));
        qAnswer28C2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2"));
        qAnswer28C3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3"));
        //qAnswer28C4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C4"));
        //qAnswer28C5.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C5"));
        qAnswer28C6.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6"));
        qAnswer28Comments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "Comments")); //DT 224 Cindi Thornton 14/9/15
        LoadAttachment(q, "28", "Letter", qAnswer28LetterText, qAnswer28LetterLink); //DT 232 Cindi Thornton 25/9/15

        if (!String.IsNullOrEmpty(qAnswer28A2.Text) || !String.IsNullOrEmpty(qAnswer28A3.Text) || !String.IsNullOrEmpty(qAnswer28A6.Text) ||
            !String.IsNullOrEmpty(qAnswer28B2.Text) || !String.IsNullOrEmpty(qAnswer28B3.Text) || !String.IsNullOrEmpty(qAnswer28B6.Text) ||
            !String.IsNullOrEmpty(qAnswer28C2.Text) || !String.IsNullOrEmpty(qAnswer28C3.Text) || !String.IsNullOrEmpty(qAnswer28C6.Text)
            ||!String.IsNullOrEmpty(qAnswer28Comments.Text)) //DT 224 Cindi Thornton 14/9/15
        {
            div28.Visible = false;
        }

        


        qAnswer29iA.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "A"));
        qAnswer29iAComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments"));
        qAnswer29iB.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "B"));
        qAnswer29iBComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments"));
        qAnswer29iC.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "C"));
        qAnswer29iCComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments"));
        qAnswer29iD.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "D"));
        qAnswer29iDComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments"));
        if (qAnswer29iA.Value != null || qAnswer29iB.Value != null | qAnswer29iC.Value != null || qAnswer29iD.Value != null)
        {
            div29.Visible = false;
        }

        qAnswer29iiA.Value = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "A"));
        if (qAnswer29iiA.Value != null) div30.Visible = false;
        qAnswer29iiComments.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments"));
        LoadAttachment(q, "29ii", "AExample", qAnswer29iiAExampleText, qAnswer29iiAExampleLink);

        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "A")) == "Yes") { qAnswer30.Value = "A"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "B")) == "Yes") { qAnswer30.Value = "B"; };
        if (Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "C")) == "Yes") { qAnswer30.Value = "C"; };
        if (qAnswer30.Value != null) div31.Visible = false;

        int E1 = 0;
        int F1 = 0;
        int G1 = 0;
        try { E1 = Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E1"))); }
        catch { };
        try { F1 = Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F1"))); }
        catch { };
        try { G1 = Convert.ToInt32(Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G1"))); }
        catch { };


        if (E1 > 0) qAnswer30E1.Value = E1;
        qAnswer30E2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E2"));
        qAnswer30E3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3"));
        qAnswer30E3B.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3B"));
        qAnswer30E4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E4"));
        if (F1 > 0) qAnswer30F1.Value = F1;
        qAnswer30F2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F2"));
        qAnswer30F3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3"));
        qAnswer30F3B.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3B"));
        qAnswer30F4.Text =  Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F4"));
        if (G1 > 0) qAnswer30G1.Value = G1;
        qAnswer30G2.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G2"));
        qAnswer30G3.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3"));
        qAnswer30G3B.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3B"));
        qAnswer30G4.Text = Helper.Questionnaire.Supplier.Main.LoadAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G4"));
    }
    protected bool SaveAnswers(int q)
    {
        bool _forcedHigh = false;
        Configuration configuration = new Configuration();
        string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("01", ""), StringUtilities.Convert.NullSafeObjectToString(qAnswer1.Text), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("02", ""), qAnswer2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("03", ""), StringUtilities.Convert.NullSafeObjectToString(qAnswer3.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("04", ""), qAnswer4.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "A"), qAnswer5A.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "B"), qAnswer5B.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "C"), qAnswer5C.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "D"), qAnswer5D.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("05", "E"), qAnswer5E.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("06", ""), qAnswer6.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("07", ""), qAnswer7.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("08", ""), qAnswer8.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("09", ""), qAnswer9.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("10", ""), qAnswer10.Text, auth.UserId);

        System.Collections.Generic.List<object> keyValues = qAnswer11A.GetSelectedFieldValues("CategoryId");
        string selected = "";
        Hashtable ht = new Hashtable();

        QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();

        //Clear Selected...
        TList<QuestionnaireServicesSelected> qssList = qssService.GetByQuestionnaireIdQuestionnaireTypeId(q, (int)QuestionnaireTypeList.Main_1);
        foreach (QuestionnaireServicesSelected _qss in qssList)
        {
            qssService.Delete(_qss.QuestionnaireServiceId);
        }
        
        foreach (int _key in keyValues)
        {
            string key = _key.ToString();
            string add;
            if ((key == null) || (key == "")) { add = ""; }
            else { add = key.ToString(); }

            if (!ht.ContainsKey(add)) //Anti-Duplication
            {
                //ht[add] = 0;
                //selected += ";" + add;
                //lblEmailList.Text += "<br />" + add + ",";
                
                //Add Selected...
                try
                {
                    QuestionnaireServicesSelected qss = new QuestionnaireServicesSelected();
                    qss.QuestionnaireTypeId = (int)QuestionnaireTypeList.Main_1;
                    qss.QuestionnaireId = q;
                    qss.CategoryId = Convert.ToInt32(add);
                    qss.ModifiedByUserId = auth.UserId;
                    qss.ModifiedDate = DateTime.Now;
                    qssService.Save(qss);

                    try
                    {
                        QuestionnaireServicesCategoryService qscS = new QuestionnaireServicesCategoryService();
                        QuestionnaireServicesCategory qsc = qscS.GetByCategoryId(Convert.ToInt32(add));
                        if (qsc.HighRisk == true) { _forcedHigh = true; };
                    }
                    catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
                }
                catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
            }
        }



        if (!String.IsNullOrEmpty(selected))
        {
            Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "A"), selected, auth.UserId);
        }

        if (!String.IsNullOrEmpty(qAnswer11B.Text))
        {
            Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("11", "B"), qAnswer11B.Text, auth.UserId);
        }

        string Response = "";
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer12.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer12.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer12.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "C"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "Comments"), qAnswer12Comments.Text, auth.UserId);

        try
        {
            if (qAnswer12AExample.UploadedFiles != null && qAnswer12AExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer12AExample.UploadedFiles[0].FileName) && qAnswer12AExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("12", "AExample"), qAnswer12AExample);
                }
            }
        }
        catch { }


        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "D"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value) == "E") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "E"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("13", "Comments"), qAnswer13Comments.Text, auth.UserId);


        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer14A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer14B.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer14C.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "D"), StringUtilities.Convert.NullSafeObjectToString(qAnswer14D.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("14", "Comments"), qAnswer14Comments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer15A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "AComments"), qAnswer15AComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer15B.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "BComments"), qAnswer15BComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer15C.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("15", "CComments"), qAnswer15CComments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer16A.Value), auth.UserId);
        try //Added. 20-JUL-2010
        {
            Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "AComments"), StringUtilities.Convert.NullSafeObjectToString(qAnswer16AComments.Text), auth.UserId);
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer16B.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "BComments"), qAnswer16BComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer16C.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("16", "CComments"), qAnswer16CComments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer17A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer17B.Value), auth.UserId);

        try
        {
            if (qAnswer17BExample.UploadedFiles != null && qAnswer17BExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer17BExample.UploadedFiles[0].FileName) && qAnswer17BExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("17", "BExample"), qAnswer17BExample);
                }
            }
        }
        catch { }

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer18.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer18.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer18.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer18.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "D"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("18", "Comments"), qAnswer18Comments.Text, auth.UserId);

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer19.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer19.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer19.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer19.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "D"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("19", "Comments"), qAnswer19Comments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer20A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "AComments"), qAnswer20AComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer20B.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("20", "BComments"), qAnswer20BComments.Text, auth.UserId);

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer21.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer21.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer21.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer21.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "D"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("21", "Comments"), qAnswer21Comments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer22A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer22B.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer22C.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Comments"), qAnswer22Comments.Text, auth.UserId);

        try //Added. 20-JUL-2010
        {
            if (qAnswer22Example.UploadedFiles != null && qAnswer22Example.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer22Example.UploadedFiles[0].FileName) && qAnswer22Example.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("22", "Example"), qAnswer22Example);
                }
            }
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer23.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer23.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer23.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer23.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "D"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Comments"), qAnswer23Comments.Text, auth.UserId);

        try
        {
            if (qAnswer23Example.UploadedFiles != null && qAnswer23Example.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer23Example.UploadedFiles[0].FileName) && qAnswer23Example.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("23", "Example"), qAnswer23Example);
                }
            }
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer24.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer24.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer24.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "C"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer24.Value) == "D") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "D"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("24", "Comments"), qAnswer24Comments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer25A.Value), auth.UserId);


        try
        {
            if (qAnswer25AExample.UploadedFiles != null && qAnswer25AExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer25AExample.UploadedFiles[0].FileName) && qAnswer25AExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "AExample"), qAnswer25AExample);
                }
            }
        }
        catch { }

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer25B.Value), auth.UserId);

        try
        {
            if (qAnswer25BExample.UploadedFiles != null && qAnswer25BExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer25BExample.UploadedFiles[0].FileName) && qAnswer25BExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "BExample"), qAnswer25BExample);
                }
            }
        }
        catch { }

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer25C.Value), auth.UserId);

        try
        {
            if (qAnswer25CExample.UploadedFiles != null && qAnswer25CExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer25CExample.UploadedFiles[0].FileName) && qAnswer25CExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("25", "CExample"), qAnswer25CExample);
                }
            }
        }
        catch { }

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer26A.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AComments"), qAnswer26AComments.Text, auth.UserId);

        try
        {
            if (qAnswer26AExample.UploadedFiles != null && qAnswer26AExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer26AExample.UploadedFiles[0].FileName) && qAnswer26AExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("26", "AExample"), qAnswer26AExample);
                }
            }
        }
        catch { }

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A1"), qAnswer27A1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A2"), qAnswer27A2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "A3"), qAnswer27A3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B1"), qAnswer27B1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B2"), qAnswer27B2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "B3"), qAnswer27B3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C1"), qAnswer27C1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C2"), qAnswer27C2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "C3"), qAnswer27C3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D1"), qAnswer27D1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D2"), qAnswer27D2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "D3"), qAnswer27D3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E1"), qAnswer27E1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E2"), qAnswer27E2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "E3"), qAnswer27E3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("27", "Comments"), qAnswer27Comments.Text, auth.UserId); //DT224 Cindi Thornton 14/9/15

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A1"), qAnswer28A1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A2"), qAnswer28A2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A3"), qAnswer28A3.Text, auth.UserId);
        ////--Q/A Removed per e-mail request 22-Apr-2009
        ////--
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A4"), qAnswer28A4.Text);
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A5"), qAnswer28A5.Text);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "A6"), qAnswer28A6.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B1"), qAnswer28B1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B2"), qAnswer28B2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B3"), qAnswer28B3.Text, auth.UserId);
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B4"), qAnswer28B4.Text);
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B5"), qAnswer28B5.Text);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "B6"), qAnswer28B6.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C1"), qAnswer28C1.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C2"), qAnswer28C2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C3"), qAnswer28C3.Text, auth.UserId);
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C4"), qAnswer28C4.Text);
        //Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C5"), qAnswer28C5.Text);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "C6"), qAnswer28C6.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "Comments"), qAnswer28Comments.Text, auth.UserId); //DT224 Cindi Thornton 14/9/15


        //DT232 Cindi Thornton - new upload functionality
        try
        {
            if (qAnswer28Letter.UploadedFiles != null && qAnswer28Letter.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer28Letter.UploadedFiles[0].FileName) && qAnswer28Letter.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("28", "Letter"), qAnswer28Letter);
                }
            }
        }
        catch { }
        //End DT232

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer29iA.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "AComments"), qAnswer29iAComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "B"), StringUtilities.Convert.NullSafeObjectToString(qAnswer29iB.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "BComments"), qAnswer29iBComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "C"), StringUtilities.Convert.NullSafeObjectToString(qAnswer29iC.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "CComments"), qAnswer29iCComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "D"), StringUtilities.Convert.NullSafeObjectToString(qAnswer29iD.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29i", "DComments"), qAnswer29iDComments.Text, auth.UserId);

        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "A"), StringUtilities.Convert.NullSafeObjectToString(qAnswer29iiA.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "Comments"), qAnswer29iiComments.Text, auth.UserId);

        try
        {
            if (qAnswer29iiAExample.UploadedFiles != null && qAnswer29iiAExample.UploadedFiles.Length > 0)
            {
                if (!String.IsNullOrEmpty(qAnswer29iiAExample.UploadedFiles[0].FileName) && qAnswer29iiAExample.UploadedFiles[0].IsValid)
                {
                    SaveAttachment(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("29ii", "AExample"), qAnswer29iiAExample);
                }
            }
        }
        catch { }

        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer30.Value) == "A") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "A"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer30.Value) == "B") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "B"), Response, auth.UserId);
        if (StringUtilities.Convert.NullSafeObjectToString(qAnswer30.Value) == "C") { Response = "Yes"; } else { Response = "No"; };
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "C"), Response, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E1"), StringUtilities.Convert.NullSafeObjectToString(qAnswer30E1.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E2"), qAnswer30E2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3"), qAnswer30E3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E3B"), qAnswer30E3B.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "E4"), qAnswer30E4.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F1"), StringUtilities.Convert.NullSafeObjectToString(qAnswer30F1.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F2"), qAnswer30F2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3"), qAnswer30F3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F3B"), qAnswer30F3B.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "F4"), qAnswer30F4.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G1"), StringUtilities.Convert.NullSafeObjectToString(qAnswer30G1.Value), auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G2"), qAnswer30G2.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3"), qAnswer30G3.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G3B"), qAnswer30G3B.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAnswer(q, Helper.Questionnaire.Supplier.Main.GetAnswerId("30", "G4"), qAnswer30G4.Text, auth.UserId);

        return _forcedHigh;
    }

    protected bool SaveAttachment(int q, int AnswerId, ASPxUploadControl fu)
    {
        Configuration configuration = new Configuration();
        string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
        string fullPath = tempPath + fu.UploadedFiles[0].FileName;
        fu.UploadedFiles[0].SaveAs(fullPath);
        FileStream fs = new FileStream(fullPath, FileMode.Open,
                                                 FileAccess.Read,
                                                 FileShare.ReadWrite);

        QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
        QuestionnaireMainAttachment qa = new QuestionnaireMainAttachment();
        qa = qas.GetByQuestionnaireIdAnswerId(q, AnswerId);
        if (qa == null) { qa = new QuestionnaireMainAttachment(); qa.QuestionnaireId = q; qa.AnswerId = AnswerId; };
        qa.FileName = fu.UploadedFiles[0].FileName;
        qa.Content = FileUtilities.Read.ReadFully(fs, 51200);
        qa.FileHash = FileUtilities.Calculate.HashFile(qa.Content);
        qa.ContentLength = Convert.ToInt32(fs.Length);
        qa.ModifiedByUserId = auth.UserId;
        qa.ModifiedDate = DateTime.Now;

        bool fail = false;
        try { qas.Save(qa); }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); fail = true; };

        fs.Close();
        fu.Dispose();
        File.Delete(fullPath);

        if (fail != true) { return true; } else { return false; };

    }
    protected void LoadAttachment(int q, string QuestionId, string AnswerId, Label Text, ASPxHyperLink Link)
    {
            try
            {
                //Label Text = (Label)this.FindControl("qAnswer" + QuestionId + AnswerId + "Text");
                //ASPxHyperLink Link = (ASPxHyperLink)this.FindControl("qAnswer" + QuestionId + AnswerId + "Link");

                QuestionnaireMainAttachmentService qas = new QuestionnaireMainAttachmentService();
                QuestionnaireMainAttachment qa = qas.GetByQuestionnaireIdAnswerId(q, Helper.Questionnaire.Supplier.Main.GetAnswerId(QuestionId, AnswerId));
                if (qa != null)
                {
                    Text.Visible = true;
                    Link.Text = qa.FileName;
                    Link.NavigateUrl = String.Format("~/Common/GetFile.aspx{0}",
                                        QueryStringModule.Encrypt(String.Format("Type=SQ&SubType=Supplier&File={0}&Seed={1}", qa.AttachmentId, StringUtilities.RandomKey())));
                    Link.Target = "_blank";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                //Response.Write(ex.Message.ToString());
            }
    }
    //Added by Pankaj Dikshit for <Issue ID# 2396, 14-May-12>
    protected string GetProcurementQuesRiskStatus(int q)
    {
        string strReturnValue = string.Empty;
        string qAnswer10_1 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_1");
        string qAnswer10_2 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_2");
        string qAnswer10_3 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_3");
        string qAnswer10_4 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_4");
        string qAnswer10_5 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_5");
        string qAnswer10_6 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_6");
        string qAnswer10_7 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_7");
        string qAnswer10_8 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_8");
        string qAnswer10_9 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_9");
        string qAnswer10_10 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_10");
        string qAnswer10_11 = Helper.Questionnaire.Procurement.LoadAnswer(q, "10_11");

        if (qAnswer10_11 == "Yes") { strReturnValue = forcedHigh; }
        else if (qAnswer10_10 == "Yes") { strReturnValue = forcedHigh; }
        else if (qAnswer10_9 == "Yes") { strReturnValue = forcedHigh; }
        else if (qAnswer10_8 == "Yes") { strReturnValue = forcedHigh; }
        else if (qAnswer10_7 == "Yes") { strReturnValue = "High Risk"; }
        else if (qAnswer10_6 == "Yes") { strReturnValue = "High Risk"; }
        else if (qAnswer10_5 == "Yes") { strReturnValue = "Medium Risk"; }
        else if (qAnswer10_4 == "Yes") { strReturnValue = "Medium Risk"; }
        else if (qAnswer10_3 == "Yes") { strReturnValue = "Low Risk"; }
        else if (qAnswer10_2 == "Yes") { strReturnValue = "Low Risk"; }
        else if (qAnswer10_1 == "Yes") { strReturnValue = "Low Risk"; }

        return strReturnValue;
    }

    protected void Submit()
    {
        try
        {
            if (Page.IsValid)
            {
                string q = Request.QueryString["q"].ToString();
                QuestionnaireService qs = new QuestionnaireService();
                Questionnaire qu = qs.GetByQuestionnaireId(Convert.ToInt32(q));
                qu.MainModifiedByUserId = auth.UserId;
                qu.MainModifiedDate = DateTime.Now;
                qu.MainStatus = (int)QuestionnaireStatusList.BeingAssessed;

                string[] score = GenerateReport(Convert.ToInt32(q));
                if (score == null) { throw new ArgumentException("Could not calculate score"); };
                qu.MainScoreOverall = Convert.ToInt32(score[0]);
                qu.MainScoreSystems = Convert.ToInt32(score[1]);
                qu.MainScoreStaffing = Convert.ToInt32(score[2]);
                qu.MainScoreExpectations = Convert.ToInt32(score[3]);
                qu.MainScoreResults = Convert.ToInt32(score[4]);

                Decimal d1 = ((Convert.ToDecimal(qu.MainScoreSystems) / 23) * 100);
                qu.MainScorePsystems = Convert.ToInt32(d1);
                Decimal d2 = ((Convert.ToDecimal(qu.MainScoreStaffing) / 15) * 100);
                qu.MainScorePstaffing = Convert.ToInt32(d2);
                Decimal d3 = ((Convert.ToDecimal(qu.MainScoreExpectations) / 16) * 100);
                qu.MainScorePexpectations = Convert.ToInt32(d3);
                Decimal d4 = ((Convert.ToDecimal(qu.MainScoreResults) / 20) * 100);
                qu.MainScorePresults = Convert.ToInt32(d4);

                Decimal d = (d1 * Convert.ToDecimal(0.3)) + (d2 * Convert.ToDecimal(0.25)) + (d3 * Convert.ToDecimal(0.25)) + (d4 * Convert.ToDecimal(0.2));
                qu.MainScorePoverall = Convert.ToInt32(d);
                //qu.MainScorePoverall = Convert.ToInt32(Decimal.Round(Convert.ToDecimal(qu.MainScorePsystems + qu.MainScorePstaffing + qu.MainScorePexpectations + qu.MainScorePresults) / Convert.ToDecimal(74),1));

                if (score[5] != "1")
                {
                    if (qu.MainScorePoverall > 0) qu.MainAssessmentRiskRating = "High Risk";
                    if (qu.MainScorePoverall > 39) qu.MainAssessmentRiskRating = "Medium Risk";
                    if (qu.MainScorePoverall > 74) qu.MainAssessmentRiskRating = "Low Risk";
                }
                else
                {
                    qu.MainAssessmentRiskRating = "Forced High";
                }

                if (qu.MainAssessmentRiskRating == "High Risk" || qu.MainAssessmentRiskRating == "Forced High")
                {
                    QuestionnaireInitialResponseService _qirService = new QuestionnaireInitialResponseService();
                    QuestionnaireInitialResponse _qir = _qirService.GetByQuestionnaireIdQuestionId(Convert.ToInt32(q), "9");
                    if (_qir != null)
                    {
                        if (_qir.AnswerText != "Sub-Contract")
                        {
                            qu.IsVerificationRequired = true;
                        }
                    }


                }
                else // Added 'else block' by Pankaj Dikshit for <Issue ID# 2396, 14-May-12>
                {
                    string strProcurementQuesRiskStatus = GetProcurementQuesRiskStatus(Convert.ToInt32(q));
                    string strAnswer12_1 = Helper.Questionnaire.Procurement.LoadAnswer(Convert.ToInt32(q), "12_1");

                    if (strProcurementQuesRiskStatus == "High Risk" || strProcurementQuesRiskStatus == forcedHigh || strAnswer12_1 == "Yes")
                    {
                        qu.IsVerificationRequired = true;
                    }
                    else
                    {
                        qu.IsVerificationRequired = false;
                    }
                }

                qu.MainAssessmentStatus = "Preliminary";

                //DateTime DatePlusYear = DateTime.ParseExact(Helper.Questionnaire.Supplier.Main.LoadAnswer(Convert.ToInt32(q), Helper.Questionnaire.Supplier.Main.GetAnswerId("01", "")), "dd/MM/yyyy",
                //                                    System.Globalization.CultureInfo.InvariantCulture);
                //DateTime DatePlusYear = qu.CreatedDate;

                //qu.MainAssessmentDate = DatePlusYear;
                //qu.MainAssessmentValidTo = DatePlusYear.AddYears(1);

                if (qu.SubContractor == true) qu.IsVerificationRequired = false; //If SubContractor, Verification Questionnaire is not required. 

                qs.Save(qu);
                bool _forcedHigh = SaveAnswers(Convert.ToInt32(qu.QuestionnaireId.ToString()));
                if (_forcedHigh) { qu.MainAssessmentRiskRating = "Forced High"; qs.Save(qu); };
                SessionHandler.spVar_SafetyPlans_QuestionnaireId = "";
                lblSave.Text = "Submitted Successfully. Click Home to return to the list of Questionnaires.";
                //Response.Redirect("~/SafetyPQ_QuestionnaireModify.aspx?q=" + qu.QuestionnaireId.ToString(), true);

                if (Helper.Questionnaire.ActionLog.Add((int)QuestionnaireActionList.SupplierQuestionnaireCompleted,
                                                        qu.QuestionnaireId,                         //QuestionnaireId
                                                        auth.UserId,                                //UserId
                                                        auth.FirstName,                             //FirstName
                                                        auth.LastName,                              //LastName
                                                        auth.RoleId,                                //RoleId
                                                        auth.CompanyId,                             //CompanyId
                                                        auth.CompanyName,                           //CompanyName
                                                        null,                                       //Comments
                                                        null,                                       //OldAssigned
                                                        auth.FirstName + " " + auth.LastName,       //NewAssigned
                                                        null,                                       //NewAssigned2
                                                        null,                                       //NewAssigned3
                                                        null)                                       //NewAssigned4
                                                        == false)
                {
                    //for now we do nothing if action log fails.
                }



                Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + qu.QuestionnaireId.ToString())), true);
            }
            else
            {
                lblSave.Text = "Submit unsuccessful. Error: Please ensure all mandatory answers are entered";
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
            lblSave.Text = "Submit unsuccessful. Error: " + ex.Message.ToString();
        }
    }

    protected string[] GenerateReport(int q)
    {
        int score = 0;

        int ScoreSystems = 0;
        int ScoreStaffing = 0;
        int ScoreExpectations = 0;
        int ScoreResults = 0;

        string forcedHigh = "0";

        bool hasExample = false;
        try
        {
            //12
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer12.Value))
            {
                case "A":
                     hasExample = false;
                     hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "12AExample");
                    if(hasExample)
                    {
                        ScoreSystems += 2;
                    }
                    else
                    {
                        ScoreSystems++;
                    }
                    break;
                default:
                    break;
            }
            //13
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer13.Value))
            {
                case "A":
                    ScoreExpectations += 2;
                    break;
                case "B":
                    ScoreExpectations++;
                    break;
                default:
                    break;
            }
            //14A
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer14A.Value))
            {
                case "Yes":
                    ScoreExpectations += 2;
                    break;
                case "No":
                    ScoreExpectations++;
                    break;
                default:
                    break;
            }
            //14B
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer14B.Value))
            {
                case "Yes":
                    ScoreExpectations += 2;
                    break;
                default:
                    break;
            }
            //14C
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer14C.Value))
            {
                case "Yes":
                    ScoreExpectations += 2;
                    break;
                default:
                    break;
            }
            //14D
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer14D.Value))
            {
                case "Yes":
                    if (!String.IsNullOrEmpty(qAnswer14Comments.Text))
                    {
                        ScoreExpectations += 2;
                    }
                    else
                    {
                        ScoreExpectations++;
                    }
                    break;
                case "No":
                    ScoreExpectations++;
                    break;
                default:
                    break;
            }
            //15A
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer15A.Value))
            {
                case "No":
                    ScoreSystems += 2;
                    break;
                default:
                    break;
            }
            //15B
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer15B.Value))
            {
                case "Yes":
                    ScoreSystems++;
                    break;
                case "No":
                    ScoreSystems += 2;
                    break;
                default:
                    break;
            }
            //15C
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer15C.Value))
            {
                case "Yes":
                    if (!String.IsNullOrEmpty(qAnswer15CComments.Text))
                    {
                        ScoreSystems += 2;
                    }
                    break;
                default:
                    break;
            }
            //16A
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer16A.Value))
            {
                case "Yes":
                    ScoreStaffing += 2;
                    break;
                default:
                    break;
            }
            //16B
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer16B.Value))
            {
                case "Yes":
                    if (!String.IsNullOrEmpty(qAnswer16BComments.Text))
                    {
                        ScoreStaffing += 2;
                    }
                    else
                    {
                        ScoreStaffing++;
                    }
                    break;
                default:
                    ScoreStaffing++;
                    break;
            }
            //16C
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer16C.Value))
            {
                case "Yes":
                    if (!String.IsNullOrEmpty(qAnswer16CComments.Text))
                    {
                        ScoreStaffing += 2;
                    }
                    else
                    {
                        ScoreStaffing++;
                    }
                    break;
                default:
                    ScoreStaffing++;
                    break;
            }
            //17A
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer17A.Value))
            {
                case "Yes":
                    ScoreSystems++;
                    break;
                default:
                    break;
            }
            //17B
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer17B.Value))
            {
                case "Yes":
                    hasExample = false;
                    hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "17BExample");
                    if (hasExample)
                    {
                        ScoreSystems += 2;
                    }
                    else
                    {
                        ScoreSystems++;
                    }
                    break;
                default:
                    break;
            }
            //18
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer18.Value))
            {
                case "C":
                    ScoreStaffing++;
                    break;
                case "D":
                    ScoreStaffing += 2;
                    break;
                default:
                    break;
            }
            //19
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer19.Value))
            {
                case "A":
                    ScoreStaffing += 2;
                    break;
                case "B":
                    ScoreStaffing++;
                    break;
                default:
                    break;
            }
            //20A
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer20A.Value))
            {
                case "Yes":
                    ScoreStaffing++;
                    break;
                default:
                    break;
            }
            //20B
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer20B.Value))
            {
                case "Yes":
                    if (!String.IsNullOrEmpty(qAnswer20AComments.Text))
                    {
                        ScoreStaffing += 2;
                    }
                    else
                    {
                        ScoreStaffing++;
                    }
                    break;
                default:
                    break;
            }
            //21
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer21.Value))
            {
                case "A":
                    ScoreExpectations += 2;
                    break;
                case "B":
                    ScoreExpectations++;
                    break;
                case "D":
                    ScoreExpectations += 2;
                    break;
                default:
                    break;
            }
            //22
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer22A.Value) == "Yes")
            {
                ScoreStaffing += 2;
            }
            else
            {
                if (StringUtilities.Convert.NullSafeObjectToString(qAnswer22B.Value) == "Yes" && StringUtilities.Convert.NullSafeObjectToString(qAnswer22C.Value) == "Yes")
                {
                    ScoreStaffing++;
                }
            }
            //23
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer23.Value))
            {
                case "A":
                    hasExample = false;
                    hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "23Example");
                    //TODO: check if example exists
                    if (hasExample)
                    {
                        ScoreExpectations += 2;
                    }
                    break;
                case "B":
                    hasExample = false;
                    hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "23BExample");
                    //TODO: check if example exists
                    if (hasExample)
                    {
                        ScoreExpectations++;
                    }
                    break;
                default:
                    break;
            }
            //24
            switch (StringUtilities.Convert.NullSafeObjectToString(qAnswer24.Value))
            {
                case "A":
                    ScoreExpectations += 2;
                    break;
                case "B":
                    ScoreExpectations++;
                    break;
                case "D":
                    if (!String.IsNullOrEmpty(qAnswer24Comments.Text))
                    {
                        ScoreExpectations++;
                    }
                    break;
                default:
                    break;
            }
            //25A
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25A.Value) == "No") ScoreResults += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25A.Value) == "Yes")
            {
                hasExample = false;
                hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "25AExample");
                if (!hasExample) forcedHigh = "1";
            }

            //25B
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25B.Value) == "No") ScoreResults += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25B.Value) == "Yes")
            {
                hasExample = false;
                hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "25BExample");
                //TODO: check if example exists
                if (hasExample)
                {
                    ScoreResults++;
                }
            }
            //25C
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25C.Value) == "No") ScoreResults += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer25C.Value) == "Yes")
            {
                hasExample = false;
                hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "25CExample");
                //if (!hasExample) forcedHigh = "1";
            }

            //26
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer26A.Value) == "No") ScoreResults++;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer26A.Value) == "Yes")
            {
                hasExample = false;
                hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "26AExample");
                //TODO: check if example exists
                if (hasExample)
                {
                    ScoreResults += 2;
                }
            } 
            
            //27A
            
            int? total = NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A1.Value) + NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A2.Value) + NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A3.Value);
            if (total == 0 && total != null)
            {
                ScoreResults += 2;
            }
            else
            {
                int? count0 = 0;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A1.Value) == 0) count0++;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A2.Value) == 0) count0++;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A3.Value) == 0) count0++;

                if (count0 > 1)
                {
                    ScoreResults++;
                }
            }

            //27B
            int? total2 = NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B1.Value) + NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B2.Value) + NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B3.Value);
            if (total2 == 0 && total2 != null)
            {
                ScoreResults += 2;
            }
            else
            {
                int? count02 = 0;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B1.Value) == 0) count02++;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B2.Value) == 0) count02++;
                if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B3.Value) == 0) count02++;

                if (count02 > 1)
                {
                    ScoreResults++;
                }
            }

            //27C
            //If 27E.value != 0 
            //if (27c3.value / 27e) < 0.000025 , = 2 points. 
            //else if (27c3.value / 27e3) > 0.000025 and < 0.0005, = 1 point

            int? _27C = 0;
            _27C = NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27C3.Value);
            int? _27E = 0;
            _27E = NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27E3.Value);
            if (_27E != 0 & _27C != 0)
            {
                Double _27C_27E = (Convert.ToDouble(_27C) / Convert.ToDouble(_27E));
                Double _1 = 0.000025d;
                Double _2 = 0.0005;

                if (_27C_27E < _1)
                {
                    ScoreResults += 2;
                }
                else
                {
                    if (_27C_27E > 1 && _27C_27E < _2)
                    {
                        ScoreResults++;
                    }
                }

            }
            else
            {
                if (_27C == 0)
                {
                    ScoreResults += 2;
                }
            }

            //27D
            //if 27d.value < (5*(27a.value + 27b.value + 27c.value)), = 1 point.
            //if 27d.value > (5*(27a.value + 27b.value + 27c.value)), = 2 points
            Double _27A = 0;
            _27A = Convert.ToDouble(NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27A3.Value));
            Double _27B = 0;
            _27B = Convert.ToDouble(NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27B3.Value));
            Double _27C_2 = 0;
            _27C_2 = Convert.ToDouble(NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27C3.Value));
            Double _27D = 0;
            _27D = Convert.ToDouble(NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27D3.Value));

            if (_27D < (5 * (_27A + _27B + _27C_2))) ScoreResults++;
            if (_27D >= (5 * (_27A + _27B + _27C_2))) ScoreResults += 2;

            //27E
            int? count03 = 0;
            if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27E1.Value) != null) count03++;
            if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27E2.Value) != null) count03++;
            if (NumberUtilities.Convert.parseObjectToNullableInt(qAnswer27E3.Value) != null) count03++;
            if (count03 == 3) ScoreResults += 2;



            //28
            int? count04 = 0;
            if (NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28A6.Value) < NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28A3.Value)) count04++;
            if (NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28B6.Value) < NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28B3.Value)) count04++;
            if (NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28C6.Value) < NumberUtilities.Convert.parseObjectToNullableDecimal(qAnswer28C3.Value)) count04++;
            if (count04 == 2) ScoreResults++;
            if (count04 == 3) ScoreResults += 2;

            //29iA
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iA.Value) == "No") ScoreSystems += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iA.Value) == "Yes") ScoreSystems++;
            //29iB
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iB.Value) == "No") ScoreSystems += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iB.Value) == "Yes") forcedHigh = "1";
            //29iC
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iC.Value) == "No") ScoreSystems += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iC.Value) == "Yes") ScoreSystems++;
            //29iD
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iD.Value) == "No") ScoreSystems += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iD.Value) == "Yes") forcedHigh = "1";

            //29iiA
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iiA.Value) == "No") ScoreSystems += 2;
            if (StringUtilities.Convert.NullSafeObjectToString(qAnswer29iiA.Value) == "Yes")
            {
                hasExample = false;
                hasExample = Helper.Questionnaire.Supplier.Main.HasExample(q, "29iiAExample");
                //TODO: check if example exists
                if (hasExample && !String.IsNullOrEmpty(qAnswer29iiComments.Text))
                {
                    ScoreSystems++;
                }
            }

            //30
            //if (!String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30E1.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30E2.Text)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30E3.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30E3B.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30E4.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30F1.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30F2.Text)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30F3.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30F3B.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30F4.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30G1.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30G2.Text)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30G3.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30G3B.Value)) &&
            //    !String.IsNullOrEmpty(StringUtilities.Convert.NullSafeObjectToString(qAnswer30G4.Value))
            //   )
            //{
                if (StringUtilities.Convert.NullSafeObjectToString(qAnswer30.Value) == "A") ScoreSystems += 2;
                if (StringUtilities.Convert.NullSafeObjectToString(qAnswer30.Value) == "B") ScoreSystems++;
            //}

            score = ScoreSystems + ScoreStaffing + ScoreExpectations + ScoreResults;

            string[] _score = { score.ToString(), ScoreSystems.ToString(), ScoreStaffing.ToString(),
                                ScoreExpectations.ToString(), ScoreResults.ToString(), forcedHigh};

            return _score;
        }
        catch(Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            return null;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //Dt 3077
        try
        {
            if (Page.IsPostBack)
            {
                //System.Threading.Thread.Sleep(1000);                
                if (ValidateControls(false) == false)
                {
                    throw new Exception("Some Controls in the Page are not valid anymore..Please Close the browser and Refill/Resubmit the Questionnaire again..");
                }

                if (ValidateUploadControls(false) == true)
                {
                    Save();
                }
                else
                {
                    lblSave.Text = "Error Occured: Could not Save. Scroll down and Check that the files you are intending to upload are correctly formatted and sized, then try again.";
                    ASPxPopupControl2.ShowOnPageLoad = true;
                }

            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);                                    
            lblSave.Text = String.Format("<img src=\"Images/error.gif\" /> Error: Questionnaire Not Saved :<br /> {0}", ex.Message);
            lblBox1_PopUp.Text = "<img src=\"Images/error.gif\" /> Error: Questionnaire Not Saved.";
            lblBox2_PopUp.Text = "Some Controls in the Page are not valid anymore..Please Close the browser and Refill/Resubmit the Questionnaire again..";

            ASPxPopupControl2.ShowOnPageLoad = true;
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            //System.Threading.Thread.Sleep(1000);
            //Validation();
            //bool allgood = false;            
            //allgood = ValidateConditionalControls();
            //allgood = ValidateUploadControls(true);
            //if (allgood)
            //DT3077
            //Made 2 variables instead single valiable(may be overwritten to true)

            //Dt 3077
            try
            {
                bool allgoodcc = false;
                bool allgooduc = false;
                bool allCheckOk = false;
                allgoodcc = ValidateConditionalControls();
                allgooduc = ValidateUploadControls(true);
                allCheckOk = ValidateControls(true);

                if (allCheckOk == false)
                {
                    throw new Exception("Some Controls in the Page are not valid anymore..Please Close the browser and Refill/Resubmit the Questionnaire again..");
                }
            
                if (allgoodcc == true && allgooduc == true)
                {
                    Submit();
                }
                else
                {
                    lblSave.Text = "<img src=\"Images/error.gif\" /> Error: Questionnaire Not Submitted.<br /> Required areas were not completed and/or your files intended to be uploaded are correctly formatted and sized.<br />Please scroll down and correct all sections marked with an red exclamation mark and try to save again.<br /><br />";
                    lblBox1_PopUp.Text = "<img src=\"Images/error.gif\" /> Error: Questionnaire Not Submitted.";
                    lblBox2_PopUp.Text = "Required areas were not completed and/or your files intended to be uploaded are correctly formatted and sized.<br />Please scroll down and correct all sections marked with an red exclamation mark and try to save again.";

                    ASPxPopupControl2.ShowOnPageLoad = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                lblSave.Text = String.Format("<img src=\"Images/error.gif\" /> Error: Questionnaire Not Submitted :<br /> {0}", ex.Message);
                lblBox1_PopUp.Text = "<img src=\"Images/error.gif\" /> Error: Questionnaire Not Submitted.";
                lblBox2_PopUp.Text = "Some Controls in the Page are not valid anymore..Please Close the browser and Refill/Resubmit the Questionnaire again..";

                ASPxPopupControl2.ShowOnPageLoad = true;
            }

        }
    }
    protected void btnSaveAssessment_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            string q = Request.QueryString["q"].ToString();
            SaveAssessment(Convert.ToInt32(q), false);
        }
    }
    protected void btnSaveAssessmentGoHome_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            string q = Request.QueryString["q"].ToString();
            SaveAssessment(Convert.ToInt32(q), true);
        }
    }

    protected void btnHome_Click(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(500);
        //Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx?q={0}", Request.QueryString["q"]), true);
        Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + Request.QueryString["q"])), true);
    }
    protected void qAnswer1_DateChanged(object sender, EventArgs e)
    {
        //System.Threading.Thread.Sleep(500);
        SetYears(qAnswer1.Date.Year);
    }

    protected void SetYears(int? _Year)
    {
        int Year = DateTime.Now.Year;
        if (_Year != null) Year = Convert.ToInt32(_Year);
        int Year1 = Year - 3;
        int Year2 = Year - 2;
        int Year3 = Year - 1;
        qAnswer27Year1.Text = Year1.ToString();
        qAnswer27Year2.Text = Year2.ToString();
        qAnswer27Year3.Text = Year3.ToString();
        qAnswer28A1.Text = Year1.ToString();
        qAnswer28B1.Text = Year2.ToString();
        qAnswer28C1.Text = Year3.ToString();
    }

    protected void grid_DataBound(object sender, EventArgs e)
    {
        try
        {
            string q = Request.QueryString["q"].ToString();
            if (q != "New")
            {
                QuestionnaireServicesSelectedService qssService = new QuestionnaireServicesSelectedService();
                TList<QuestionnaireServicesSelected> qssList = qssService.GetByQuestionnaireIdQuestionnaireTypeId(Convert.ToInt32(q), (int)QuestionnaireTypeList.Main_1);

                TList<QuestionnaireServicesSelected> qssList2 = qssService.GetByQuestionnaireIdQuestionnaireTypeId(Convert.ToInt32(q), (int)QuestionnaireTypeList.Initial_1);
                int? InitialSelected = null;
                if (qssList2.Count > 0)
                {
                    foreach (QuestionnaireServicesSelected _qs in qssList2)
                    {
                        InitialSelected = _qs.CategoryId;
                    }
                }

                if (qssList.Count > 0) //Questionnaire not new, select from main selected
                {
                    foreach (QuestionnaireServicesSelected _qss in qssList)
                    {
                        for (int i = 0; i < qAnswer11A.VisibleRowCount; i++)
                        {
                            int o = Convert.ToInt32(qAnswer11A.GetRowValues(i, new string[] { "CategoryId" }));
                            if (o == _qss.CategoryId)
                            {
                                qAnswer11A.Selection.SelectRow(i);
                                div11.Visible = false;
                            }
                        }
                    }
                }
                else //Questionnaire is new, select from initial selected
                {
                    foreach (QuestionnaireServicesSelected _qs in qssList2)
                    {
                        for (int i = 0; i < qAnswer11A.VisibleRowCount; i++)
                        {
                            int o = Convert.ToInt32(qAnswer11A.GetRowValues(i, new string[] { "CategoryId" }));
                            if (o == InitialSelected)
                            {
                                qAnswer11A.Selection.SelectRow(i);
                                div11.Visible = false;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
        }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            Label lblCategoryText = qAnswer11A.FindRowCellTemplateControl(e.VisibleIndex, null, "lblCategoryText") as Label;
            String CategoryText = (string)qAnswer11A.GetRowValues(e.VisibleIndex, "CategoryText");
            int plusone = e.VisibleIndex + 1;
            lblCategoryText.Text = String.Format("{0} - {1}", plusone, CategoryText);
        }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
    }

    //protected void gridServicesPrimary_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    //{
    //    e.NewValues["QuestionnaireId"] = Convert.ToInt32(Request.QueryString["q"].ToString());
    //    e.NewValues["QuestionnaireTypeId"] = 3;
    //}
    //protected void gridServicesPrimary_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    //{
    //    e.NewValues["QuestionnaireId"] = Convert.ToInt32(Request.QueryString["q"].ToString());
    //    e.NewValues["QuestionnaireTypeId"] = 3;
    //}

    protected void SaveAssessment(int QuestionnaireId, bool submit)
    {
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "1", assess1Approval.Value, assess1Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "11", assess11Approval.Value, assess11Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "12", assess12Approval.Value, assess12Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "13", assess13Approval.Value, assess13Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "14", assess14Approval.Value, assess14Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "15", assess15Approval.Value, assess15Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "16", assess16Approval.Value, assess16Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "17", assess17Approval.Value, assess17Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "18", assess18Approval.Value, assess18Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "19", assess19Approval.Value, assess19Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "20", assess20Approval.Value, assess20Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "21", assess21Approval.Value, assess21Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "22", assess22Approval.Value, assess22Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "23", assess23Approval.Value, assess23Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "24", assess24Approval.Value, assess24Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "25", assess25Approval.Value, assess25Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "26", assess26Approval.Value, assess26Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "27", assess27Approval.Value, assess27Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "28", assess28Approval.Value, assess28Comments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "29i", assess29iApproval.Value, assess29iComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "29ii", assess29iiApproval.Value, assess29iiComments.Text, auth.UserId);
        Helper.Questionnaire.Supplier.Main.SaveAssessment(QuestionnaireId, "30", assess30Approval.Value, assess30Comments.Text, auth.UserId);
        AssessmentView(QuestionnaireId);
        if (submit)
        {
            lblSave.Text = "Saved Successfully. Click Home to return to the list of Questionnaires.";
            Response.Redirect(String.Format("~/SafetyPQ_QuestionnaireModify.aspx{0}", QueryStringModule.Encrypt("q=" + QuestionnaireId)), true);
        }
    }

    protected void LoadAssessment(int QuestionnaireId)
    {
        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
        QuestionnaireMainAssessment qma;

        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "1");
        if (qma != null) { assess1Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess1Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "11");
        if (qma != null) { assess11Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess11Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "12");
        if (qma != null) { assess12Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess12Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "13");
        if (qma != null) { assess13Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess13Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "14");
        if (qma != null) { assess14Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess14Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "15");
        if (qma != null) { assess15Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess15Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "16");
        if (qma != null) { assess16Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess16Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "17");
        if (qma != null) { assess17Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess17Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "18");
        if (qma != null) { assess18Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess18Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "19");
        if (qma != null) { assess19Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess19Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "20");
        if (qma != null) { assess20Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess20Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "21");
        if (qma != null) { assess21Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess21Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "22");
        if (qma != null) { assess22Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess22Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "23");
        if (qma != null) { assess23Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess23Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "24");
        if (qma != null) { assess24Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess24Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "25");
        if (qma != null) { assess25Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess25Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "26");
        if (qma != null) { assess26Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess26Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "27");
        if (qma != null) { assess27Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess27Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "28");
        if (qma != null) { assess28Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess28Comments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "29i");
        if (qma != null) { assess29iApproval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess29iComments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "29ii");
        if (qma != null) { assess29iiApproval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess29iiComments.Text = qma.AssessorComment; }
        qma = null;
        qma = qmaService.GetByQuestionnaireIdQuestionNo(QuestionnaireId, "30");
        if (qma != null) { assess30Approval.Value = NumberUtilities.Convert.BoolToInt(qma.AssessorApproval); assess30Comments.Text = qma.AssessorComment; }
        qma = null;
    }

    protected void AssessmentView(int QuestionnaireId, int Status, int MainStatus)
    {
        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
        TList<QuestionnaireMainAssessment> qmaTlist_all;
        qmaTlist_all = qmaService.GetByQuestionnaireId(QuestionnaireId);
        bool approvednull = false;
        bool notapproved = false;

        if (Status == (int)QuestionnaireStatusList.BeingAssessed && MainStatus == (int)QuestionnaireStatusList.BeingAssessed)
        {
            ReadOnly(true);

            if (qmaTlist_all.Count != 22)
            {
                approvednull = true;
            }
            else
            {
                foreach (QuestionnaireMainAssessment qma in qmaTlist_all)
                {
                    if (qma.AssessorApproval == null) approvednull = true;
                    if (qma.AssessorApproval == false) notapproved = true;
                }
            }
            if (!notapproved && !approvednull) lblSave.Text = "Assessed: Approved";
            if (notapproved && !approvednull) lblSave.Text = "Assessed: Not Approved";
            if (approvednull)
            {
                lblSave.Text = "Being Assessed";
            }

            if (auth.RoleId == (int)RoleList.Administrator || Helper.General.isEhsConsultant(auth.UserId))
            {
                rpAssess1.Visible = true;
                rpAssess11.Visible = true;
                rpAssess12.Visible = true;
                rpAssess13.Visible = true;
                rpAssess14.Visible = true;
                rpAssess15.Visible = true;
                rpAssess16.Visible = true;
                rpAssess17.Visible = true;
                rpAssess18.Visible = true;
                rpAssess19.Visible = true;
                rpAssess20.Visible = true;
                rpAssess21.Visible = true;
                rpAssess22.Visible = true;
                rpAssess23.Visible = true;
                rpAssess24.Visible = true;
                rpAssess25.Visible = true;
                rpAssess26.Visible = true;
                rpAssess27.Visible = true;
                rpAssess28.Visible = true;
                rpAssess29i.Visible = true;
                rpAssess29ii.Visible = true;
                rpAssess30.Visible = true;
                ASPxButton2.Visible = false;
                btnSubmit.Visible = false;
                ASPxCheckBox1.Visible = false;
                btnSave.Visible = false;
                btnSaveAssessment.Visible = true;
                btnSaveAssessment2.Visible = true;
                btnSaveAssessmentGoHome.Visible = true;
                btnSaveAssessment2GoHome.Visible = true;
            }
        }

        QuestionnaireService qService = new QuestionnaireService();
        Questionnaire q = qService.GetByQuestionnaireId(QuestionnaireId);

        if ((Status == (int)QuestionnaireStatusList.Incomplete && MainStatus == (int)QuestionnaireStatusList.Incomplete) ||
                (Status == (int)QuestionnaireStatusList.AssessmentComplete) || q.QuestionnairePresentlyWithActionId == (int)QuestionnairePresentlyWithActionList.SupplierQuestionnaireIncomplete)
        {
            //check if reviewal comments exist etc.
            if (qmaTlist_all.Count == 22) //if they do, show reviewal comments.
            {
                rpAssess1.Visible = true;
                rpAssess11.Visible = true;
                rpAssess12.Visible = true;
                rpAssess13.Visible = true;
                rpAssess14.Visible = true;
                rpAssess15.Visible = true;
                rpAssess16.Visible = true;
                rpAssess17.Visible = true;
                rpAssess18.Visible = true;
                rpAssess19.Visible = true;
                rpAssess20.Visible = true;
                rpAssess21.Visible = true;
                rpAssess22.Visible = true;
                rpAssess23.Visible = true;
                rpAssess24.Visible = true;
                rpAssess25.Visible = true;
                rpAssess26.Visible = true;
                rpAssess27.Visible = true;
                rpAssess28.Visible = true;
                rpAssess29i.Visible = true;
                rpAssess29ii.Visible = true;
                rpAssess30.Visible = true;

                assess1Approval.ReadOnly = true;
                assess11Approval.ReadOnly = true;
                assess12Approval.ReadOnly = true;
                assess13Approval.ReadOnly = true;
                assess14Approval.ReadOnly = true;
                assess15Approval.ReadOnly = true;
                assess16Approval.ReadOnly = true;
                assess17Approval.ReadOnly = true;
                assess18Approval.ReadOnly = true;
                assess19Approval.ReadOnly = true;
                assess20Approval.ReadOnly = true;
                assess21Approval.ReadOnly = true;
                assess22Approval.ReadOnly = true;
                assess23Approval.ReadOnly = true;
                assess24Approval.ReadOnly = true;
                assess25Approval.ReadOnly = true;
                assess26Approval.ReadOnly = true;
                assess27Approval.ReadOnly = true;
                assess28Approval.ReadOnly = true;
                assess29iApproval.ReadOnly = true;
                assess29iiApproval.ReadOnly = true;
                assess30Approval.ReadOnly = true;
                assess1Comments.ReadOnly = true;
                assess11Comments.ReadOnly = true;
                assess12Comments.ReadOnly = true;
                assess13Comments.ReadOnly = true;
                assess14Comments.ReadOnly = true;
                assess15Comments.ReadOnly = true;
                assess16Comments.ReadOnly = true;
                assess17Comments.ReadOnly = true;
                assess18Comments.ReadOnly = true;
                assess19Comments.ReadOnly = true;
                assess20Comments.ReadOnly = true;
                assess21Comments.ReadOnly = true;
                assess22Comments.ReadOnly = true;
                assess23Comments.ReadOnly = true;
                assess24Comments.ReadOnly = true;
                assess25Comments.ReadOnly = true;
                assess26Comments.ReadOnly = true;
                assess27Comments.ReadOnly = true;
                assess28Comments.ReadOnly = true;
                assess29iComments.ReadOnly = true;
                assess29iiComments.ReadOnly = true;
                assess30Comments.ReadOnly = true;

                if (auth.RoleId != (int)RoleList.Administrator)
                {
                    //Changed by Pankaj Dikshit from '.ToString' to 'Convert.ToString' for <Issue ID# 2397, 25-May-2012>
                    if (Convert.ToString(assess1Approval.SelectedItem.Value) == "1")
                    {
                        //ASPxRoundPanel1.Enabled = false;
                        qAnswer1.ReadOnly = true;
                        qAnswer2.ReadOnly = true;
                        qAnswer3.ReadOnly = true;
                        qAnswer4.ReadOnly = true;
                        qAnswer5A.ReadOnly = true;
                        qAnswer5B.ReadOnly = true;
                        qAnswer5C.ReadOnly = true;
                        qAnswer5D.ReadOnly = true;
                        qAnswer5E.ReadOnly = true;
                        qAnswer6.ReadOnly = true;
                        qAnswer7.ReadOnly = true;
                        qAnswer8.ReadOnly = true;
                        qAnswer9.ReadOnly = true;
                        qAnswer10.ReadOnly = true;
                    }
                    if (assess11Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer11A.Enabled = false;
                        qAnswer11B.ReadOnly = true;
                    }
                    if (assess12Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer12.ReadOnly = true;
                        qAnswer12Comments.ReadOnly = true;
                        qAnswer12AExample.Enabled = false;
                    }
                    if (assess13Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer13.ReadOnly = true;
                        qAnswer13Comments.ReadOnly = true;
                    }
                    if (assess14Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer14A.ReadOnly = true;
                        qAnswer14B.ReadOnly = true;
                        qAnswer14C.ReadOnly = true;
                        qAnswer14D.ReadOnly = true;
                        qAnswer14Comments.ReadOnly = true;
                    }
                    if (assess15Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer15A.ReadOnly = true;
                        qAnswer15AComments.ReadOnly = true;
                        qAnswer15B.ReadOnly = true;
                        qAnswer15BComments.ReadOnly = true;
                        qAnswer15C.ReadOnly = true;
                        qAnswer15CComments.ReadOnly = true;
                    }
                    if (assess16Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer16A.ReadOnly = true;
                        qAnswer16AComments.ReadOnly = true;
                        qAnswer16B.ReadOnly = true;
                        qAnswer16BComments.ReadOnly = true;
                        qAnswer16C.ReadOnly = true;
                        qAnswer16CComments.ReadOnly = true;
                    }
                    if (assess17Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer17A.ReadOnly = true;
                        qAnswer17B.ReadOnly = true;
                        qAnswer17BExample.Enabled = false;
                    }
                    if (assess18Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer18.ReadOnly = true;
                        qAnswer18Comments.ReadOnly = true;
                    }
                    if (assess19Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer19.ReadOnly = true;
                        qAnswer19Comments.ReadOnly = true;
                    }
                    if (assess20Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer20A.ReadOnly = true;
                        qAnswer20AComments.ReadOnly = true;
                        qAnswer20B.ReadOnly = true;
                        qAnswer20BComments.ReadOnly = true;
                    }
                    if (assess21Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer21.ReadOnly = true;
                        qAnswer21Comments.ReadOnly = true;
                    }
                    if (assess22Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer22A.ReadOnly = true;
                        qAnswer22B.ReadOnly = true;
                        qAnswer22C.ReadOnly = true;
                        qAnswer22Comments.ReadOnly = true;
                        qAnswer22Example.Enabled = false;
                    }
                    if (assess23Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer23.ReadOnly = true;
                        qAnswer23Comments.ReadOnly = true;
                        qAnswer23Example.Enabled = false;
                    }
                    if (assess24Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer24.ReadOnly = true;
                        qAnswer24Comments.ReadOnly = true;
                    }
                    if (assess25Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer25A.ReadOnly = true;
                        qAnswer25AExample.Enabled = false;
                        qAnswer25B.ReadOnly = true;
                        qAnswer25BExample.Enabled = false;
                        qAnswer25C.ReadOnly = true;
                        qAnswer25CExample.Enabled = false;
                    }
                    if (assess26Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer26A.ReadOnly = true;
                        qAnswer26AComments.ReadOnly = true;
                        qAnswer26AExample.Enabled = false;
                    }
                    if (assess27Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer27A1.ReadOnly = true;
                        qAnswer27A2.ReadOnly = true;
                        qAnswer27A3.ReadOnly = true;
                        qAnswer27B1.ReadOnly = true;
                        qAnswer27B2.ReadOnly = true;
                        qAnswer27B2.ReadOnly = true;
                        qAnswer27B3.ReadOnly = true;
                        qAnswer27C1.ReadOnly = true;
                        qAnswer27C2.ReadOnly = true;
                        qAnswer27C3.ReadOnly = true;
                        qAnswer27D1.ReadOnly = true;
                        qAnswer27D2.ReadOnly = true;
                        qAnswer27D3.ReadOnly = true;
                        qAnswer27E1.ReadOnly = true;
                        qAnswer27E2.ReadOnly = true;
                        qAnswer27E3.ReadOnly = true;
                        qAnswer27Comments.ReadOnly = true; //DT 224 Cindi Thornton 23/9/2015
                    }
                    if (assess28Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer28A1.ReadOnly = true;
                        qAnswer28A2.ReadOnly = true;
                        qAnswer28A3.ReadOnly = true;
                        qAnswer28A6.ReadOnly = true;
                        qAnswer28B1.ReadOnly = true;
                        qAnswer28B2.ReadOnly = true;
                        qAnswer28B3.ReadOnly = true;
                        qAnswer28B6.ReadOnly = true;
                        qAnswer28C1.ReadOnly = true;
                        qAnswer28C2.ReadOnly = true;
                        qAnswer28C3.ReadOnly = true;
                        qAnswer28C6.ReadOnly = true;
                        qAnswer28Comments.ReadOnly = true; //DT 224 Cindi Thornton 23/9/2015
                        qAnswer28Letter.Enabled = false; //DT232 Cindi Thornton 25/9/2015

                    }
                    if (assess29iApproval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer29iA.ReadOnly = true;
                        qAnswer29iAComments.ReadOnly = true;
                        qAnswer29iB.ReadOnly = true;
                        qAnswer29iBComments.ReadOnly = true;
                        qAnswer29iC.ReadOnly = true;
                        qAnswer29iCComments.ReadOnly = true;
                        qAnswer29iD.ReadOnly = true;
                        qAnswer29iDComments.ReadOnly = true;
                    }
                    if (assess29iiApproval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer29iiA.ReadOnly = true;
                        qAnswer29iiComments.ReadOnly = true;
                        qAnswer29iiAExample.Enabled = false;
                    }
                    if (assess30Approval.SelectedItem.Value.ToString() == "1")
                    {
                        qAnswer30.ReadOnly = true;
                        qAnswer30E1.ReadOnly = true;
                        qAnswer30E2.ReadOnly = true;
                        qAnswer30E3.ReadOnly = true;
                        qAnswer30E3B.ReadOnly = true;
                        qAnswer30E4.ReadOnly = true;
                        qAnswer30F1.ReadOnly = true;
                        qAnswer30F2.ReadOnly = true;
                        qAnswer30F3.ReadOnly = true;
                        qAnswer30F3B.ReadOnly = true;
                        qAnswer30F4.ReadOnly = true;
                        qAnswer30G1.ReadOnly = true;
                        qAnswer30G2.ReadOnly = true;
                        qAnswer30G3.ReadOnly = true;
                        qAnswer30G3B.ReadOnly = true;
                        qAnswer30G4.ReadOnly = true;
                    }
                }
            }
        }
    }
    protected void AssessmentView(int QuestionnaireId)
    {
        QuestionnaireMainAssessmentService qmaService = new QuestionnaireMainAssessmentService();
        TList<QuestionnaireMainAssessment> qmaTlist_all;
        qmaTlist_all = qmaService.GetByQuestionnaireId(QuestionnaireId);
        bool approvednull = false;
        bool notapproved = false;
        if (qmaTlist_all.Count != 22)
        {
            approvednull = true;
        }
        else
        {
            foreach (QuestionnaireMainAssessment qma in qmaTlist_all)
            {
                if (qma.AssessorApproval == null) approvednull = true;
                if (qma.AssessorApproval == false) notapproved = true;
            }
        }
        if (!notapproved && !approvednull) lblSave.Text = "Assessed: Approved";
        if (notapproved && !approvednull) lblSave.Text = "Assessed: Not Approved";
        if (approvednull)
        {
            lblSave.Text = "Being Assessed";
        }
    }

    protected void ReadOnly(bool status)
    {
        lblSave.Text = "Read Only";

        bool ostatus = true;
        if (status)
        {
            ostatus = false;
            Label1.Text = "";
            pnlTime.Visible = false;
        }
        qAnswer11A.Enabled = ostatus;
        qAnswer12AExample.Enabled = ostatus;
        qAnswer17BExample.Enabled = ostatus;
        qAnswer22Example.Enabled = ostatus;
        qAnswer23Example.Enabled = ostatus;
        qAnswer25AExample.Enabled = ostatus;
        qAnswer25BExample.Enabled = ostatus;
        qAnswer25CExample.Enabled = ostatus;
        qAnswer26AExample.Enabled = ostatus;
        qAnswer28Letter.Enabled = ostatus; //DT232 Cindi Thornton 25/9/15 - new upload
        qAnswer29iiAExample.Enabled = ostatus;
        btnSave.Enabled = ostatus;
        ASPxButton2.Enabled = ostatus;
        btnSubmit.Enabled = ostatus;



        qAnswer1.ReadOnly = status;
        qAnswer2.ReadOnly = status;
        qAnswer3.ReadOnly = status;
        qAnswer4.ReadOnly = status;
        qAnswer5A.ReadOnly = status;
        qAnswer5B.ReadOnly = status;
        qAnswer5C.ReadOnly = status;
        qAnswer5D.ReadOnly = status;
        qAnswer5E.ReadOnly = status;
        qAnswer6.ReadOnly = status;
        qAnswer7.ReadOnly = status;
        qAnswer8.ReadOnly = status;
        qAnswer9.ReadOnly = status;
        qAnswer10.ReadOnly = status;
        qAnswer11B.ReadOnly = status;
        qAnswer12.ReadOnly = status;
        qAnswer12Comments.ReadOnly = status;
        qAnswer13.ReadOnly = status;
        qAnswer13Comments.ReadOnly = status;
        qAnswer14A.ReadOnly = status;
        qAnswer14B.ReadOnly = status;
        qAnswer14C.ReadOnly = status;
        qAnswer14D.ReadOnly = status;
        qAnswer14Comments.ReadOnly = status;
        qAnswer15A.ReadOnly = status;
        qAnswer15AComments.ReadOnly = status;
        qAnswer15B.ReadOnly = status;
        qAnswer15BComments.ReadOnly = status;
        qAnswer15C.ReadOnly = status;
        qAnswer15CComments.ReadOnly = status;
        qAnswer16A.ReadOnly = status;
        qAnswer16AComments.ReadOnly = status;
        qAnswer16B.ReadOnly = status;
        qAnswer16BComments.ReadOnly = status;
        qAnswer16C.ReadOnly = status;
        qAnswer16CComments.ReadOnly = status;
        qAnswer17A.ReadOnly = status;
        qAnswer17B.ReadOnly = status;
        qAnswer18.ReadOnly = status;
        qAnswer18Comments.ReadOnly = status;
        qAnswer19.ReadOnly = status;
        qAnswer19Comments.ReadOnly = status;
        qAnswer20A.ReadOnly = status;
        qAnswer20AComments.ReadOnly = status;
        qAnswer20B.ReadOnly = status;
        qAnswer20BComments.ReadOnly = status;
        qAnswer21.ReadOnly = status;
        qAnswer21Comments.ReadOnly = status;
        qAnswer22A.ReadOnly = status;
        qAnswer22B.ReadOnly = status;
        qAnswer22C.ReadOnly = status;
        qAnswer22Comments.ReadOnly = status;
        qAnswer23.ReadOnly = status;
        qAnswer23Comments.ReadOnly = status;
        qAnswer24.ReadOnly = status;
        qAnswer24Comments.ReadOnly = status;
        qAnswer25A.ReadOnly = status;
        qAnswer25B.ReadOnly = status;
        qAnswer25C.ReadOnly = status;
        qAnswer26A.ReadOnly = status;
        qAnswer26AComments.ReadOnly = status;
        qAnswer27A1.ReadOnly = status;
        qAnswer27A2.ReadOnly = status;
        qAnswer27A3.ReadOnly = status;
        qAnswer27B1.ReadOnly = status;
        qAnswer27B2.ReadOnly = status;
        qAnswer27B3.ReadOnly = status;
        qAnswer27C1.ReadOnly = status;
        qAnswer27C2.ReadOnly = status;
        qAnswer27C3.ReadOnly = status;
        qAnswer27D1.ReadOnly = status;
        qAnswer27D2.ReadOnly = status;
        qAnswer27D3.ReadOnly = status;
        qAnswer27E1.ReadOnly = status;
        qAnswer27E2.ReadOnly = status;
        qAnswer27E3.ReadOnly = status;
        qAnswer27Year1.ReadOnly = status;
        qAnswer27Year2.ReadOnly = status;
        qAnswer27Year3.ReadOnly = status;
        qAnswer27Comments.ReadOnly = status; //DT224 Cindi Thornton 23/9/2015
        qAnswer28A1.ReadOnly = status;
        qAnswer28A2.ReadOnly = status;
        qAnswer28A3.ReadOnly = status;
        qAnswer28A6.ReadOnly = status;
        qAnswer28B1.ReadOnly = status;
        qAnswer28B2.ReadOnly = status;
        qAnswer28B3.ReadOnly = status;
        qAnswer28B6.ReadOnly = status;
        qAnswer28C1.ReadOnly = status;
        qAnswer28C2.ReadOnly = status;
        qAnswer28C3.ReadOnly = status;
        qAnswer28C6.ReadOnly = status;
        qAnswer28Comments.ReadOnly = status; //DT224 Cindi Thornton 23/9/2015
        qAnswer29iA.ReadOnly = status;
        qAnswer29iAComments.ReadOnly = status;
        qAnswer29iB.ReadOnly = status;
        qAnswer29iBComments.ReadOnly = status;
        qAnswer29iC.ReadOnly = status;
        qAnswer29iCComments.ReadOnly = status;
        qAnswer29iD.ReadOnly = status;
        qAnswer29iDComments.ReadOnly = status;
        qAnswer29iiA.ReadOnly = status;
        qAnswer29iiComments.ReadOnly = status;
        qAnswer30.ReadOnly = status;
        qAnswer30E1.ReadOnly = status;
        qAnswer30E2.ReadOnly = status;
        qAnswer30E3.ReadOnly = status;
        qAnswer30E3B.ReadOnly = status;
        qAnswer30E4.ReadOnly = status;
        qAnswer30F1.ReadOnly = status;
        qAnswer30F2.ReadOnly = status;
        qAnswer30F3.ReadOnly = status;
        qAnswer30F3B.ReadOnly = status;
        qAnswer30F4.ReadOnly = status;
        qAnswer30G1.ReadOnly = status;
        qAnswer30G2.ReadOnly = status;
        qAnswer30G3.ReadOnly = status;
        qAnswer30G4.ReadOnly = status;
    }

    protected bool ValidateControls(bool Vcheck)
    {
        //DT3077
        bool allok = true;
        try
        {           
            if (Page.IsValid)
            {                
                if (qAnswer1 == null || qAnswer2 == null || qAnswer3 == null || qAnswer4 == null)
                { allok = false; }
                if (qAnswer5A == null || qAnswer5B == null || qAnswer5C == null || qAnswer5D == null || qAnswer5E == null)
                { allok = false; }
                if (qAnswer6 == null || qAnswer7 == null || qAnswer8 == null || qAnswer9 == null || qAnswer10 == null)
                { allok = false; }
                if (qAnswer11A == null || qAnswer11B == null || qAnswer12 == null || qAnswer12Comments == null)
                { allok = false; }
                if (qAnswer13 == null || qAnswer13Comments == null || qAnswer14A == null || qAnswer14B == null || qAnswer14C == null || qAnswer14Comments == null || qAnswer14D == null)
                { allok = false; }
                if (qAnswer15A == null || qAnswer15AComments == null || qAnswer15B == null || qAnswer15BComments == null || qAnswer15C == null || qAnswer15CComments == null)
                { allok = false; }
                if (qAnswer16A == null || qAnswer16AComments == null || qAnswer16B == null || qAnswer16BComments == null || qAnswer16C == null || qAnswer16CComments == null)
                { allok = false; }
                if (qAnswer17A == null || qAnswer17B == null || qAnswer18 == null || qAnswer18Comments == null || qAnswer19 == null || qAnswer19Comments == null)
                { allok = false; }
                if (qAnswer20A == null || qAnswer20AComments == null || qAnswer20B == null || qAnswer20BComments == null || qAnswer21 == null || qAnswer21Comments == null)
                { allok = false; }
                if (qAnswer22A == null || qAnswer22B == null || qAnswer22C == null || qAnswer22Comments == null || qAnswer23 == null || qAnswer23Comments == null)
                { allok = false; }
                if (qAnswer24 == null || qAnswer24Comments == null || qAnswer25A == null || qAnswer25B == null || qAnswer25C == null)
                { allok = false; }
                if (qAnswer26A == null || qAnswer26AComments == null || qAnswer27A1 == null || qAnswer27A2 == null || qAnswer27A3 == null)
                { allok = false; }
                if (qAnswer27B1 == null || qAnswer27B2 == null || qAnswer27B3 == null || qAnswer27C1 == null || qAnswer27C2 == null || qAnswer27C3 == null)
                { allok = false; }
                if (qAnswer27D1 == null || qAnswer27D2 == null || qAnswer27D3 == null || qAnswer27E1 == null || qAnswer27E2 == null || qAnswer27E3 == null)
                { allok = false; }
                if (qAnswer28A2 == null || qAnswer28A2 == null || qAnswer28A3 == null || qAnswer28A6 == null || qAnswer28B2 == null || qAnswer28B2 == null || qAnswer28B3 == null || qAnswer28B6 == null)
                { allok = false; }
                if (qAnswer28C2 == null || qAnswer28C3 == null || qAnswer28C6 == null || qAnswer29iA == null || qAnswer29iAComments == null || qAnswer29iB == null || qAnswer29iBComments == null || qAnswer29iC == null || qAnswer29iCComments == null)
                { allok = false; }
                if (qAnswer29iD == null || qAnswer29iDComments == null || qAnswer29iiA == null || qAnswer29iiComments == null)
                { allok = false; }
                if (qAnswer30 == null || qAnswer30E1 == null || qAnswer30E2 == null || qAnswer30E4 == null || qAnswer30F2 == null || qAnswer30F4 == null || qAnswer30G2 == null || qAnswer30G4 == null)
                { allok = false; }

                if (Vcheck == true)
                {
                    if (qAnswer12.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer13.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer14A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer14B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer14C.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer15A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer15B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer15C.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer16A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer16B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer16C.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer17A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer17B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer18.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer19.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer20A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer20B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer21.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer22A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer22B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer22C.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer23.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer24.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer25A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer25B.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer25C.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer26A.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer29iA.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer29iB.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer29iC.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer29iD.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer29iiA.SelectedItem.Value == null)
                    { allok = false; }
                    if (qAnswer30.SelectedItem.Value == null)
                    { allok = false; }
                }
            }            
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return allok;
    }

    protected bool ValidateUploadControls(bool submit)
    {
        int q = Convert.ToInt32(Request.QueryString["q"].ToString());
        bool allgood = true;

        qAnswer25AExample_Error.Visible = false;
        qAnswer25BExample_Error.Visible = false;
        qAnswer25CExample_Error.Visible = false;
        qAnswer23Example_Error.Visible = false;
        qAnswer22Example_Error.Visible = false;
        qAnswer17BExample_Error.Visible = false;
        qAnswer12AExample_Error.Visible = false;

        bool qAnswer25AExample_HasFile = false;
        if (qAnswer25AExample.UploadedFiles != null && qAnswer25AExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25AExample.UploadedFiles[0].FileName) && qAnswer25AExample.UploadedFiles[0].IsValid)
            {
                qAnswer25AExample_HasFile = true;
            }
            else
            {
                qAnswer25AExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25AExample");
            }
        }
        else qAnswer25AExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25AExample");

        bool qAnswer25BExample_HasFile = false;
        if (qAnswer25BExample.UploadedFiles != null && qAnswer25BExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25BExample.UploadedFiles[0].FileName) && qAnswer25BExample.UploadedFiles[0].IsValid)
            {
                qAnswer25BExample_HasFile = true;
            }
            else
            {
                qAnswer25BExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25BExample");
            }
        }
        else qAnswer25BExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25BExample");

        bool qAnswer25CExample_HasFile = false;
        if (qAnswer25CExample.UploadedFiles != null && qAnswer25CExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25CExample.UploadedFiles[0].FileName) && qAnswer25CExample.UploadedFiles[0].IsValid)
            {
                qAnswer25CExample_HasFile = true;
            }
            else
            {
                qAnswer25CExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25CExample");
            }
        }
        else qAnswer25CExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "25CExample");

        bool qAnswer23Example_HasFile = false;
        if (qAnswer23Example.UploadedFiles != null && qAnswer23Example.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer23Example.UploadedFiles[0].FileName) && qAnswer23Example.UploadedFiles[0].IsValid)
            {
                qAnswer23Example_HasFile = true;
            }
            else
            {
                qAnswer23Example_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "23Example");
            }
        }
        else qAnswer23Example_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "23Example");

        bool qAnswer22Example_HasFile = false;
        if (qAnswer22Example.UploadedFiles != null && qAnswer22Example.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer22Example.UploadedFiles[0].FileName) && qAnswer22Example.UploadedFiles[0].IsValid)
            {
                qAnswer22Example_HasFile = true;
            }
            else
            {
                qAnswer22Example_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "22Example");
            }
        }
        else qAnswer22Example_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "22Example");

        bool qAnswer17BExample_HasFile = false;
        if (qAnswer17BExample.UploadedFiles != null && qAnswer17BExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer17BExample.UploadedFiles[0].FileName) && qAnswer17BExample.UploadedFiles[0].IsValid)
            {
                qAnswer17BExample_HasFile = true;
            }
            else
            {
                qAnswer17BExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "17BExample");
            }
        }
        else qAnswer17BExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "17BExample");

        bool qAnswer12AExample_HasFile = false;
        if (qAnswer12AExample.UploadedFiles != null && qAnswer12AExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer12AExample.UploadedFiles[0].FileName) && qAnswer12AExample.UploadedFiles[0].IsValid)
            {
                qAnswer12AExample_HasFile = true;
            }
            else
            {
                qAnswer12AExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "12AExample");
            }
        }
        else qAnswer12AExample_HasFile = Helper.Questionnaire.Supplier.Main.HasExample(q, "12AExample");


        if (submit)
        {
            if (qAnswer25A.SelectedItem.Value != null)
            {
                if (qAnswer25A.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer25AExample_HasFile) { allgood = false; qAnswer25AExample_Error.Visible = true; };
                }
            }
            if (qAnswer25B.SelectedItem.Value != null)
            {
                if (qAnswer25B.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer25BExample_HasFile) { allgood = false; qAnswer25BExample_Error.Visible = true; };
                }
            }
            if (qAnswer25C.SelectedItem.Value != null)
            {
                if (qAnswer25C.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer25CExample_HasFile) { allgood = false; qAnswer25CExample_Error.Visible = true;  };
                }
            }

            if (qAnswer23.SelectedItem.Value != null)
            {
                if (qAnswer23.SelectedItem.Value.ToString() != "D")
                {
                    if (!qAnswer23Example_HasFile) { allgood = false; qAnswer23Example_Error.Visible = true; };
                }
            }
            
            //Mandatory File
            //if (!qAnswer23Example_HasFile) { allgood = false; qAnswer23Example_Error.Visible = true; };



            if (qAnswer22A.SelectedItem.Value != null)
            {
                if (qAnswer22A.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer22Example_HasFile) { allgood = false; qAnswer22Example_Error.Visible = true; };
                }
            }
            if (qAnswer22B.SelectedItem.Value != null)
            {
                if (qAnswer22B.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer22Example_HasFile) { allgood = false; qAnswer22Example_Error.Visible = true; };
                }
            }
            if (qAnswer22C.SelectedItem.Value != null)
            {
                if (qAnswer22C.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer22Example_HasFile) { allgood = false; qAnswer22Example_Error.Visible = true; };
                }

            }

            //NOT Mandatory File
            if (qAnswer17B.SelectedItem.Value != null)
            {
                if (qAnswer17B.SelectedItem.Value.ToString() == "Yes")
                {
                    if (!qAnswer17BExample_HasFile) { allgood = false; qAnswer17BExample_Error.Visible = true; };
                }
            }
            
            if (qAnswer12.SelectedItem.Value != null)
            {
                if (qAnswer12.SelectedItem.Value.ToString() == "Yes" && qAnswer12Comments.Text != "")
                {
                    if (!qAnswer12AExample_HasFile) { allgood = false; qAnswer12AExample_Error.Visible = true; };
                }
            }
        }

        if (qAnswer29iiAExample.UploadedFiles != null && qAnswer29iiAExample.UploadedFiles.Length > 0)
        {
            if(!String.IsNullOrEmpty(qAnswer29iiAExample.UploadedFiles[0].FileName) && !qAnswer29iiAExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        if (qAnswer26AExample.UploadedFiles != null && qAnswer26AExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer26AExample.UploadedFiles[0].FileName) && !qAnswer26AExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        if (qAnswer25AExample.UploadedFiles != null && qAnswer25AExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25AExample.UploadedFiles[0].FileName) && !qAnswer25AExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        if (qAnswer25BExample.UploadedFiles != null && qAnswer25BExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25BExample.UploadedFiles[0].FileName) && !qAnswer25BExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        if (qAnswer25CExample.UploadedFiles != null && qAnswer25CExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer25CExample.UploadedFiles[0].FileName) && !qAnswer25CExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        //if (qAnswer23Example.UploadedFiles != null && qAnswer23Example.UploadedFiles.Length > 0)
        //{
        //    if (!String.IsNullOrEmpty(qAnswer23Example.UploadedFiles[0].FileName) && !qAnswer23Example.UploadedFiles[0].IsValid) { allgood = false; }
        //}
        if (qAnswer17BExample.UploadedFiles != null && qAnswer17BExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer17BExample.UploadedFiles[0].FileName) && !qAnswer17BExample.UploadedFiles[0].IsValid) { allgood = false; }
        }
        if (qAnswer12AExample.UploadedFiles != null && qAnswer12AExample.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer12AExample.UploadedFiles[0].FileName) && !qAnswer12AExample.UploadedFiles[0].IsValid) { allgood = false; }
        }

        //DT232 - Cindi Thornton 25/9/15 new upload functionality
        if (qAnswer28Letter.UploadedFiles != null && qAnswer28Letter.UploadedFiles.Length > 0)
        {
            if (!String.IsNullOrEmpty(qAnswer28Letter.UploadedFiles[0].FileName) && !qAnswer28Letter.UploadedFiles[0].IsValid) { allgood = false; }
        } //End DT232
        
        return allgood;
    }

    protected bool ValidateConditionalControls()
    {
        bool allgood = true;
        qAnswer14Comments.Validate();
        qAnswer15AComments.Validate();
        qAnswer15BComments.Validate();
        qAnswer15CComments.Validate();
        qAnswer16AComments.Validate();
        qAnswer16BComments.Validate();
        qAnswer16CComments.Validate();
        qAnswer20AComments.Validate();
        qAnswer20BComments.Validate();
        qAnswer26AComments.Validate();
        qAnswer29iAComments.Validate();
        qAnswer29iBComments.Validate();
        qAnswer29iCComments.Validate();
        qAnswer29iDComments.Validate();

        if (!Validation_CommentsRequiredIf("Yes", qAnswer14D, qAnswer14Comments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer15A, qAnswer15AComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer15B, qAnswer15BComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer15C, qAnswer15CComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer16A, qAnswer16AComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer16B, qAnswer16BComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer16C, qAnswer16CComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer20A, qAnswer20AComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer20B, qAnswer20BComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer26A, qAnswer26AComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer29iA, qAnswer29iAComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer29iB, qAnswer29iBComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer29iC, qAnswer29iCComments)) allgood = false;
        if (!Validation_CommentsRequiredIf("Yes", qAnswer29iD, qAnswer29iDComments)) allgood = false;
        return allgood;
    }

    protected bool Validation_CommentsRequiredIf(string ExpectedValue, ASPxRadioButtonList rbl, ASPxMemo memo)
    {
        bool validity = true;
        try
        {
            bool selected = false;
            if (rbl.SelectedItem.Value != null)
            {
                if (rbl.SelectedItem.Value.ToString() == ExpectedValue) selected = true;
            }


            if (selected)
            {
                if (memo.Text == "") validity = false;
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
        }
        return validity;
    }
    protected bool Validation_ExampleRequiredIf(string ExpectedValue, ASPxRadioButtonList rbl, ASPxUploadControl uc)
    {
        bool validity = true;
        try
        {
            bool selected = false;
            if (rbl.SelectedItem.Value != null)
            {
                if (rbl.SelectedItem.Value.ToString() == ExpectedValue) selected = true;
            }


            if (selected)
            {
                if (uc.UploadedFiles == null) validity = false;
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromCurrentContext();
        }
        return validity;

    }

    protected void qAnswer14Comments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer14D, qAnswer14Comments);
    }
    protected void qAnswer15AComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer15A, qAnswer15AComments);
    }
    protected void qAnswer15BComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer15B, qAnswer15BComments);
    }
    protected void qAnswer15CComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer15C, qAnswer15CComments);
    }
    protected void qAnswer16AComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer16A, qAnswer16AComments);
    }
    protected void qAnswer16BComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer16B, qAnswer16BComments);
    }
    protected void qAnswer16CComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer16C, qAnswer16CComments);
    }
    protected void qAnswer20AComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer20A, qAnswer20AComments);
    }
    protected void qAnswer20BComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer20B, qAnswer20BComments);
    }
    protected void qAnswer26AComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer26A, qAnswer26AComments);
    }
    protected void qAnswer29iAComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer29iA, qAnswer29iAComments);
    }
    protected void qAnswer29iBComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer29iB, qAnswer29iBComments);
    }
    protected void qAnswer29iCComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer29iC, qAnswer29iCComments);
    }
    protected void qAnswer29iDComments_Validation(object sender, DevExpress.Web.ASPxEditors.ValidationEventArgs e)
    {
        e.IsValid = Validation_CommentsRequiredIf("Yes", qAnswer29iD, qAnswer29iDComments);
    }

    protected void SetInivisbleAllGetHelpOverlayDivs()
    {
        div1.Visible = false;
        div11.Visible = false;
        div12.Visible = false;
        div13.Visible = false;
        div14.Visible = false;
        div15.Visible = false;
        div16.Visible = false;
        div17.Visible = false;
        div18.Visible = false;
        div19.Visible = false;
        div20.Visible = false;
        div21.Visible = false;
        div22.Visible = false;
        div23.Visible = false;
        div24.Visible = false;
        div25.Visible = false;
        div26.Visible = false;
        div27.Visible = false;
        div28.Visible = false;
        div29.Visible = false;
        div30.Visible = false;
        div31.Visible = false;

        //int i = 1;
        //while (i <= 30)
        //{
        //    HtmlGenericControl hgc = (HtmlGenericControl)this.Page.FindControl("div" + i.ToString());
        //    if (hgc != null)
        //    {
        //        hgc.Visible = false;
        //    }
        //    i++;
        //}

    }
  
    //examples
    //23 - mandatory
    //17b - mandatory

    //12 - if yes and support comments for c)
    //26a - if yes example and comments required

    //22 - if any yes response

    //25a - if yes
    //25b - if yes
    //25c - if yes
    
}
