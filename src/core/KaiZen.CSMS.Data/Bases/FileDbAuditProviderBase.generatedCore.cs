﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FileDbAuditProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FileDbAuditProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.FileDbAudit, KaiZen.CSMS.Entities.FileDbAuditKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAuditKey key)
		{
			return Delete(transactionManager, key.AuditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _auditId)
		{
			return Delete(null, _auditId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _auditId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.FileDbAudit Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAuditKey key, int start, int pageLength)
		{
			return GetByAuditId(transactionManager, key.AuditId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_FileDBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(null,_auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength)
		{
			int count = -1;
			return GetByAuditId(transactionManager, _auditId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBAudit index.
		/// </summary>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(System.Int32 _auditId, int start, int pageLength, out int count)
		{
			return GetByAuditId(null, _auditId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_FileDBAudit index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_auditId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.FileDbAudit GetByAuditId(TransactionManager transactionManager, System.Int32 _auditId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;FileDbAudit&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;FileDbAudit&gt;"/></returns>
		public static TList<FileDbAudit> Fill(IDataReader reader, TList<FileDbAudit> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.FileDbAudit c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("FileDbAudit")
					.Append("|").Append((System.Int32)reader[((int)FileDbAuditColumn.AuditId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<FileDbAudit>(
					key.ToString(), // EntityTrackingKey
					"FileDbAudit",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.FileDbAudit();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AuditId = (System.Int32)reader[((int)FileDbAuditColumn.AuditId - 1)];
					c.FileId = (reader.IsDBNull(((int)FileDbAuditColumn.FileId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.FileId - 1)];
					c.CompanyId = (reader.IsDBNull(((int)FileDbAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.CompanyId - 1)];
					c.ModifiedByUserId = (reader.IsDBNull(((int)FileDbAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (reader.IsDBNull(((int)FileDbAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbAuditColumn.ModifiedDate - 1)];
					c.Description = (reader.IsDBNull(((int)FileDbAuditColumn.Description - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.Description - 1)];
					c.FileName = (reader.IsDBNull(((int)FileDbAuditColumn.FileName - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.FileName - 1)];
					c.FileHash = (reader.IsDBNull(((int)FileDbAuditColumn.FileHash - 1)))?null:(System.Byte[])reader[((int)FileDbAuditColumn.FileHash - 1)];
					c.ContentLength = (reader.IsDBNull(((int)FileDbAuditColumn.ContentLength - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.ContentLength - 1)];
					c.Content = (reader.IsDBNull(((int)FileDbAuditColumn.Content - 1)))?null:(System.Byte[])reader[((int)FileDbAuditColumn.Content - 1)];
					c.StatusModifiedByUserId = (reader.IsDBNull(((int)FileDbAuditColumn.StatusModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.StatusModifiedByUserId - 1)];
					c.AuditedOn = (System.DateTime)reader[((int)FileDbAuditColumn.AuditedOn - 1)];
					c.AuditEventId = (System.String)reader[((int)FileDbAuditColumn.AuditEventId - 1)];
					c.EhsConsultantId = (reader.IsDBNull(((int)FileDbAuditColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.EhsConsultantId - 1)];
					c.SelfAssessmentResponseId = (reader.IsDBNull(((int)FileDbAuditColumn.SelfAssessmentResponseId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.SelfAssessmentResponseId - 1)];
					c.StatusComments = (reader.IsDBNull(((int)FileDbAuditColumn.StatusComments - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.StatusComments - 1)];
					c.StatusId = (reader.IsDBNull(((int)FileDbAuditColumn.StatusId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.StatusId - 1)];
					c.StatusModifiedDate = (reader.IsDBNull(((int)FileDbAuditColumn.StatusModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbAuditColumn.StatusModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.FileDbAudit entity)
		{
			if (!reader.Read()) return;
			
			entity.AuditId = (System.Int32)reader[((int)FileDbAuditColumn.AuditId - 1)];
			entity.FileId = (reader.IsDBNull(((int)FileDbAuditColumn.FileId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.FileId - 1)];
			entity.CompanyId = (reader.IsDBNull(((int)FileDbAuditColumn.CompanyId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.CompanyId - 1)];
			entity.ModifiedByUserId = (reader.IsDBNull(((int)FileDbAuditColumn.ModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (reader.IsDBNull(((int)FileDbAuditColumn.ModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbAuditColumn.ModifiedDate - 1)];
			entity.Description = (reader.IsDBNull(((int)FileDbAuditColumn.Description - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.Description - 1)];
			entity.FileName = (reader.IsDBNull(((int)FileDbAuditColumn.FileName - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.FileName - 1)];
			entity.FileHash = (reader.IsDBNull(((int)FileDbAuditColumn.FileHash - 1)))?null:(System.Byte[])reader[((int)FileDbAuditColumn.FileHash - 1)];
			entity.ContentLength = (reader.IsDBNull(((int)FileDbAuditColumn.ContentLength - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.ContentLength - 1)];
			entity.Content = (reader.IsDBNull(((int)FileDbAuditColumn.Content - 1)))?null:(System.Byte[])reader[((int)FileDbAuditColumn.Content - 1)];
			entity.StatusModifiedByUserId = (reader.IsDBNull(((int)FileDbAuditColumn.StatusModifiedByUserId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.StatusModifiedByUserId - 1)];
			entity.AuditedOn = (System.DateTime)reader[((int)FileDbAuditColumn.AuditedOn - 1)];
			entity.AuditEventId = (System.String)reader[((int)FileDbAuditColumn.AuditEventId - 1)];
			entity.EhsConsultantId = (reader.IsDBNull(((int)FileDbAuditColumn.EhsConsultantId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.EhsConsultantId - 1)];
			entity.SelfAssessmentResponseId = (reader.IsDBNull(((int)FileDbAuditColumn.SelfAssessmentResponseId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.SelfAssessmentResponseId - 1)];
			entity.StatusComments = (reader.IsDBNull(((int)FileDbAuditColumn.StatusComments - 1)))?null:(System.String)reader[((int)FileDbAuditColumn.StatusComments - 1)];
			entity.StatusId = (reader.IsDBNull(((int)FileDbAuditColumn.StatusId - 1)))?null:(System.Int32?)reader[((int)FileDbAuditColumn.StatusId - 1)];
			entity.StatusModifiedDate = (reader.IsDBNull(((int)FileDbAuditColumn.StatusModifiedDate - 1)))?null:(System.DateTime?)reader[((int)FileDbAuditColumn.StatusModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.FileDbAudit entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AuditId = (System.Int32)dataRow["AuditId"];
			entity.FileId = Convert.IsDBNull(dataRow["FileId"]) ? null : (System.Int32?)dataRow["FileId"];
			entity.CompanyId = Convert.IsDBNull(dataRow["CompanyId"]) ? null : (System.Int32?)dataRow["CompanyId"];
			entity.ModifiedByUserId = Convert.IsDBNull(dataRow["ModifiedByUserId"]) ? null : (System.Int32?)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = Convert.IsDBNull(dataRow["ModifiedDate"]) ? null : (System.DateTime?)dataRow["ModifiedDate"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.FileName = Convert.IsDBNull(dataRow["FileName"]) ? null : (System.String)dataRow["FileName"];
			entity.FileHash = Convert.IsDBNull(dataRow["FileHash"]) ? null : (System.Byte[])dataRow["FileHash"];
			entity.ContentLength = Convert.IsDBNull(dataRow["ContentLength"]) ? null : (System.Int32?)dataRow["ContentLength"];
			entity.Content = Convert.IsDBNull(dataRow["Content"]) ? null : (System.Byte[])dataRow["Content"];
			entity.StatusModifiedByUserId = Convert.IsDBNull(dataRow["StatusModifiedByUserId"]) ? null : (System.Int32?)dataRow["StatusModifiedByUserId"];
			entity.AuditedOn = (System.DateTime)dataRow["AuditedOn"];
			entity.AuditEventId = (System.String)dataRow["AuditEventId"];
			entity.EhsConsultantId = Convert.IsDBNull(dataRow["EHSConsultantId"]) ? null : (System.Int32?)dataRow["EHSConsultantId"];
			entity.SelfAssessmentResponseId = Convert.IsDBNull(dataRow["SelfAssessment_ResponseId"]) ? null : (System.Int32?)dataRow["SelfAssessment_ResponseId"];
			entity.StatusComments = Convert.IsDBNull(dataRow["StatusComments"]) ? null : (System.String)dataRow["StatusComments"];
			entity.StatusId = Convert.IsDBNull(dataRow["StatusId"]) ? null : (System.Int32?)dataRow["StatusId"];
			entity.StatusModifiedDate = Convert.IsDBNull(dataRow["StatusModifiedDate"]) ? null : (System.DateTime?)dataRow["StatusModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.FileDbAudit"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbAudit Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAudit entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.FileDbAudit object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.FileDbAudit instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.FileDbAudit Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.FileDbAudit entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FileDbAuditChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.FileDbAudit</c>
	///</summary>
	public enum FileDbAuditChildEntityTypes
	{
	}
	
	#endregion FileDbAuditChildEntityTypes
	
	#region FileDbAuditFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FileDbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditFilterBuilder : SqlFilterBuilder<FileDbAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilterBuilder class.
		/// </summary>
		public FileDbAuditFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAuditFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAuditFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAuditFilterBuilder
	
	#region FileDbAuditParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FileDbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileDbAuditParameterBuilder : ParameterizedSqlFilterBuilder<FileDbAuditColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditParameterBuilder class.
		/// </summary>
		public FileDbAuditParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FileDbAuditParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FileDbAuditParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FileDbAuditParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FileDbAuditParameterBuilder
	
	#region FileDbAuditSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FileDbAuditColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileDbAudit"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FileDbAuditSortBuilder : SqlSortBuilder<FileDbAuditColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileDbAuditSqlSortBuilder class.
		/// </summary>
		public FileDbAuditSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FileDbAuditSortBuilder
	
} // end namespace
