﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersPrivileged3Service
    {
        UsersPrivileged3 Get(Expression<Func<UsersPrivileged3, bool>> where, List<Expression<Func<UsersPrivileged3, object>>> includes);
        List<UsersPrivileged3> GetMany(int? top, Expression<Func<UsersPrivileged3, bool>> where, Func<IQueryable<UsersPrivileged3>, IOrderedQueryable<UsersPrivileged3>> orderBy, List<Expression<Func<UsersPrivileged3, object>>> includes);
        List<UsersPrivileged3> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersPrivileged3, bool>> where, Func<IQueryable<UsersPrivileged3>, IOrderedQueryable<UsersPrivileged3>> orderBy, List<Expression<Func<UsersPrivileged3, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<UsersPrivileged3, bool>> where, List<Expression<Func<UsersPrivileged3, object>>> includes);
        void Insert(UsersPrivileged3 item, bool saveChanges = true);
        void Update(UsersPrivileged3 item, bool saveChanges = true);
        void Delete(UsersPrivileged3 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersPrivileged3Service : IUsersPrivileged3Service
    {
        private readonly IRepository<UsersPrivileged3> _repository;

        public UsersPrivileged3Service(IRepository<UsersPrivileged3> repository)
        {
            this._repository = repository;
        }

        public UsersPrivileged3 Get(Expression<Func<UsersPrivileged3, bool>> where, List<Expression<Func<UsersPrivileged3, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersPrivileged3> GetMany(int? top, Expression<Func<UsersPrivileged3, bool>> where, Func<IQueryable<UsersPrivileged3>, IOrderedQueryable<UsersPrivileged3>> orderBy, List<Expression<Func<UsersPrivileged3, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersPrivileged3> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersPrivileged3, bool>> where, Func<IQueryable<UsersPrivileged3>, IOrderedQueryable<UsersPrivileged3>> orderBy, List<Expression<Func<UsersPrivileged3, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersPrivileged3, bool>> where, List<Expression<Func<UsersPrivileged3, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersPrivileged3 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged3");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersPrivileged3 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged3");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersPrivileged3 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersPrivileged3");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
