﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="UserControls_SqQuestionnaire_Questionnaire2Supplier" CodeBehind="Questionnaire2Supplier.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxrp" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<style type="text/css">
    .transparency {
        filter: alpha(opacity=86);
        -moz-opacity: .86;
        opacity: 0.86;
        background-color: #EEE;
    }

    .redbold {
        color: Red;
        font-weight: bold;
    }
</style>

<%: System.Web.Optimization.Scripts.Render("~/bundles/countdown_timer_scripts") %>

<script language="JavaScript">
    var needToConfirm = false;

    var confirmMsg = "Are you sure want to navigate away from this page?\n\nYou have attempted to leave this page.  If you have made any changes to the fields without clicking the Save or Submit button, your changes will be lost.  Are you sure you want to exit this page?\n\nPress OK to continue, or Cancel to stay on the current page.";
    //window.onbeforeload = setNeedToConfirmTrue;

    function setNeedToConfirmTrue() {
        if (needToConfirm == false) {
            needToConfirm = true;
        }
    }
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (needToConfirm)
            return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save or Submit button, your changes will be lost.  Are you sure you want to exit this page?";
    }
    function confirmSave() {
        needtoConfirm = false;
    }

    function getStyle(divId) {
        var temp = document.getElementById(divId).style.visibility;

        return temp;
    }

    function switchVisibility(divId) {

        var current = getStyle();

        if (current == "visible") {
            document.getElementById(divId).style.visibility = "hidden";
        }
        else {
            document.getElementById(divId).style.visibility = "visible";
        }
    }

    function setInvisible(divId) {
        if (document.getElementById(divId) != null) {
            document.getElementById(divId).style.visibility = "hidden";
        }
    }
</script>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td class="pageName" colspan="3" style="height: 17px">
            <span class="bodycopy"><span class="title">Safety Qualification</span><br />
                <span class="date">
                    <dxe:aspxhyperlink id="hlQuestionnaire" runat="server" text="Questionnaire" tooltip="Go back to Safety Qualification Page"
                        navigateurl="~/SafetyPQ_Questionnaire.aspx">
                    </dxe:aspxhyperlink>
                    >
                    <asp:linkbutton id="LinkButton1" runat="server">Assess/Review</asp:linkbutton>
                </span>&nbsp;
                &gt; Supplier Questionnaire</span><br />
            <img src="images/grfc_dottedline.gif" width="24" height="1">&nbsp;
        </td>
    </tr>
</table>
<asp:panel id="pnlTime" runat="server">
    <div style="z-index: 12; position: absolute">
        <table width="100%">
            <tr>
                <td width="100%" align="right">
                    <span class="timeClassHeader">Time Remaining (to Save):</span>
                </td>
                <td width="100%" align="right">
                    <span id="theTime" class="timeClass"></span>
                </td>
            </tr>
        </table>
    </div>
</asp:panel>
<asp:updatepanel runat="server" updatemode="Conditional" id="UpdatePanel2">
    <contenttemplate>
        <table width="100%">
            <tbody>
                <tr>
                    <td align="center">
                        <strong><span style="font-size: 10pt; color: #ff0000"><asp:Label ID="Label1" runat="server" Text="Warning: You must save your work at least every 30 min (see clock) to ensure data being saved correctly"></asp:Label></span></strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <a name="save"></a>
        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="height: 69px; text-align: center">
                        <div align="center">
                        <table cellspacing="1" cellpadding="1" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        
                                    </td>                                      
                                    <td>
                                        <asp:Panel ID="pnlPrint" runat="server">
                                        <input id="btnPrint" onclick="window.print();" type="Button" value="Print" style="width: 50px; height: 29px;"/>
                                    </asp:Panel>
                                </tr>
                                <tr>
                                <td>
                                </td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <asp:Label ID="lblSave" runat="server" ForeColor="Red" Font-Italic="True" Font-Bold="False"></asp:Label>
                        </div>
                        </td>
                </tr>
                <tr>
                <td align="left" style="text-align: left">
                <dxe:ASPxButton ID="btnSave" OnClick="btnSave_Click" Text="Save Draft (Information may still be missing but may be added in at a later stage)" runat="server"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" UseSubmitBehavior="False" Visible="true">
                                            <ClientSideEvents Click="function(s, e) {                                            
                                                  
                                                  confirmSave();
                                                  
                                                  if (document.getElementById('ctl00_body_Questionnaire2Supplier1_ASPxButton2_CD') != null)
	                                                    { e.processOnServer = confirm('Are you sure you wish to save your changes?')}
                                                  else
                                                        {e.processOnServer = false;document.getElementById('ctl00_body_Questionnaire2Supplier1_lblSave').innerHTML='Error: Questionnaire Not Saved. Some controls in the page are not loaded properly..';
                                                        alert(' Error: Questionnaire Not Saved.\n Some controls in the page are not loaded properly.\n\n Please reload the page and try again..')                                                                   
                                                         };
                                                        }">
                                            </ClientSideEvents>
                                        </dxe:ASPxButton>
                                        <br />
                                        <dxe:ASPxButton ID="btnHome" OnClick="btnHome_Click" Text="Home" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Visible="true"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                            <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm(confirmMsg);
}" />
                                        </dxe:ASPxButton>
                                        <br />
                                        <dxe:ASPxButton ID="btnSaveAssessment2" OnClick="btnSaveAssessment_Click" Text="Save Assessment"
                                            runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" ValidationGroup="Assess_Submit" Width="150px" Visible="false">
                                            <ClientSideEvents Click="function(s, e) {
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                                        <dxe:ASPxButton ID="btnSaveAssessment2GoHome" OnClick="btnSaveAssessmentGoHome_Click"
                                            Text="Save Assessment & Go Home" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" ValidationGroup="Assess_Submit" Width="200px" Visible="false">
                                            <ClientSideEvents Click="function(s, e) {
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                </td>
                </tr>
                <tr>
                    <td style="height: 14px; text-align: right" align="right">
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="width: 79px; height: 16px; text-align: right">
                                        <strong><span style="font-family: verdana,arial,sans-serif">Status:</span> &nbsp;</strong></td>
                                    <td style="height: 16px; text-align: left">
                                        <dxe:ASPxLabel ID="lblStatus" Text="Not Saved" runat="server" Width="100%" ForeColor="#C04000"
                                            Font-Bold="False" Font-Size="12pt">
                                        </dxe:ASPxLabel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td style="width: 900px; text-align: center">
                    <div align="center">
                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            Width="550px" HeaderText="Company Information" EnableDefaultAppearance="False"
                            BackColor="#DDECFE">
                            <HeaderTemplate>
                                <table>
                                    <tr>
                                    <td align="left" style="text-align: left; width: 320px">
                                        <b>Company Information</b>
                                    </td>
                                        <td align="right" style="text-align: right; width: 580px;">
                                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=1-10');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel1_div1');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <TopEdge>
                                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                            </TopEdge>
                            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
                            <HeaderRightEdge>
                                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                            </HeaderRightEdge>
                            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
                            <HeaderLeftEdge>
                                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                            </HeaderLeftEdge>
                            <HeaderStyle BackColor="#7BA4E0">
                                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
                            </HeaderStyle>
                            <HeaderContent>
                                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
                            </HeaderContent>
                            <DisabledStyle ForeColor="Gray">
                            </DisabledStyle>
                            <NoHeaderTopEdge BackColor="#DDECFE">
                            </NoHeaderTopEdge>
                            <PanelCollection>
                                <dxp:PanelContent runat="server">
                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="UpdatePanel1">
                                        <ContentTemplate>
                                            <div style="z-index: 10; height: 320px; width: 600px" visible="true">
                                            <div id="div1" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 330px; width: 600px; visibility: visible;">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=1-10');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel1_div1');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                            <table cellspacing="0" cellpadding="0" width="600" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                            <strong>1</strong></td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText1" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxDateEdit ID="qAnswer1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px" Enabled="False" EditFormat="Custom" EditFormatString="dd/MM/yyyy"
                                                                Date="2009-01-07" AutoPostBack="False" OnDateChanged="qAnswer1_DateChanged">
                                                                <ButtonStyle Width="13px">
                                                                </ButtonStyle>
                                                                <ValidationSettings CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False">
                                                                    <RequiredField IsRequired="true"></RequiredField>
                                                                </ValidationSettings>
                                                            </dxe:ASPxDateEdit>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                            <strong>2</strong></td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText2" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                                    ValidationGroup="Submit" RequiredField-ErrorText="Question 2 - Legal Name of Company - is required.">
                                                                    <RequiredField IsRequired="True"></RequiredField>
                                                                </ValidationSettings>
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                            <strong>3</strong></td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText3" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxComboBox ID="qAnswer3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px" IncrementalFilteringMode="StartsWith" ValueType="System.Int32" DataSourceID="sqldsCompaniesList"
                                                                TextField="CompanyName" ValueField="CompanyID" ClientInstanceName="cmbCompanies">
                                                                <ButtonStyle Width="13px">
                                                                </ButtonStyle>
                                                                <ValidationSettings CausesValidation="True" SetFocusOnError="False" ErrorDisplayMode="ImageWithTooltip">
                                                                    <RequiredField IsRequired="True"></RequiredField>
                                                                </ValidationSettings>
                                                            </dxe:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                            <strong>4</strong></td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText4" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer4" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 30px">
                                                            <strong>5</strong></td>
                                                        <td style="width: 200px; height: 30px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5" runat="server" Font-Bold="True" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 30px" width="200">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                        </td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5A" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer5A" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                        </td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5B" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer5B" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                        </td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5C" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer5C" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                        </td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5D" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer5D" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                        </td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText5E" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer5E" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                            <strong>6</strong></td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText6" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer6" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                            <strong>7</strong></td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText7" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer7" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                            <strong>8</strong></td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText8" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer8" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px; background-color: white">
                                                            <strong>9</strong></td>
                                                        <td style="width: 200px; height: 13px; background-color: white; text-align: left">
                                                            <dxe:ASPxLabel ID="qText9" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px; background-color: white" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer9" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 20px; height: 13px">
                                                            <strong>10</strong></td>
                                                        <td style="width: 200px; height: 13px; text-align: left">
                                                            <dxe:ASPxLabel ID="qText10" runat="server" EncodeHtml="False" EnableViewState="False">
                                                            </dxe:ASPxLabel>
                                                        </td>
                                                        <td style="height: 13px" width="200">
                                                            <dxe:ASPxTextBox ID="qAnswer10" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                                CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                                Width="250px">
                                                            </dxe:ASPxTextBox>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            </div>
                                            
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="padding-top: 4px; text-align: center">
                                                        <dxrp:ASPxRoundPanel id="rpAssess1" runat="server" HeaderText="Assessor Reviewal & Comments"
                                                            Width="530px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                                            <panelcollection>
<dxp:PanelContent runat="server"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 47px; TEXT-ALIGN: right"><dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess1Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>
 </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left"><dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="530px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess1Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>
 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</panelcollection>
                                                        </dxrp:ASPxRoundPanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxrp:ASPxRoundPanel>
                        </div>
                        <br />
                    </td>
                </tr>
            </tbody>
        </table>
        
        <div style="border-right: black 1px solid; padding-right: 10px; border-top: black 1px solid;
            padding-left: 10px; padding-bottom: 10px; border-left: black 1px solid; padding-top: 10px;
            border-bottom: black 1px solid; background-color: #ffffcc; text-align: center">
            <span style="font-size: 13pt; color: maroon"><strong><span style="font-family: Verdana">
                ALL QUESTIONS MUST BE ANSWERED</span><br />
            </strong><span style="color: maroon"><span style="font-size: 10pt; font-family: Verdana">
                Please note that you can save at any time but only submit this questionnaire once
                all sections have been completed.<br />
            </span></span></span>
        </div>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="11" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>11</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                           <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=11');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel2_div11');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />                 
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                 <div style="z-index: 10; height: 320px; width: 880px" visible="true">
                                            <div id="div11" class="transparency" runat="server" style="position: absolute; top: -50; z-index: 11; height: 330px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=11');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel2_div11');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                                <dxe:ASPxLabel runat="server" EncodeHtml="False"
                    CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                    ID="qText11" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxLabel>
 &nbsp;<br />&nbsp; <span style="color: black">To select more than one, hold down the CTRL key</span><br />&nbsp;<dxwgv:ASPxGridView runat="server" ClientInstanceName="qAnswer11A"
         CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
         KeyFieldName="CategoryId" AutoGenerateColumns="False" DataSourceID="QuestionnaireServicesCategoryDataSource"
         Width="100%" ID="qAnswer11A" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
         OnDataBound="grid_DataBound" OnHtmlRowCreated="grid_RowCreated"><Columns>
<dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" Width="20px" VisibleIndex="0"></dxwgv:GridViewCommandColumn>
<dxwgv:GridViewDataTextColumn FieldName="CategoryId" ReadOnly="True" Visible="False"
                 VisibleIndex="0">
<EditFormSettings Visible="False"></EditFormSettings>
</dxwgv:GridViewDataTextColumn>
<dxwgv:GridViewDataTextColumn FieldName="CategoryText" SortIndex="0" SortOrder="Ascending"
                 VisibleIndex="1"><DataItemTemplate>
                     <table cellpadding="0" cellspacing="0">
                         <tr>
                             <td>
                                 <asp:Label ID="lblCategoryText" runat="server"></asp:Label></td>
                         </tr>
                     </table>
                 
</DataItemTemplate>
</dxwgv:GridViewDataTextColumn>
</Columns>

<SettingsBehavior AllowSelectByRowClick="True"></SettingsBehavior>

<SettingsPager Mode="ShowAllRecords" SEOFriendly="Enabled" ShowNumericButtons="False"
             ShowDisabledButtons="False" Visible="False"></SettingsPager>

<Settings ShowGroupButtons="False" ShowColumnHeaders="False" ShowVerticalScrollBar="True"
             VerticalScrollableHeight="230" GridLines="Horizontal"></Settings>

<Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif"></LoadingPanelOnStatusBar>

<LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif"></LoadingPanel>
</Images>

<ImagesFilterControl>
<LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif"></LoadingPanel>
</ImagesFilterControl>

<Styles CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
<Header SortingImageSpacing="5px" ImageSpacing="5px"></Header>

<LoadingPanel ImageSpacing="10px"></LoadingPanel>
</Styles>

<StylesEditors>
<ProgressBar Height="25px"></ProgressBar>
</StylesEditors>
</dxwgv:ASPxGridView><dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue"
     CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswerText11B"
     EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"></dxe:ASPxLabel>
 <br /><dxe:ASPxTextBox ID="qAnswer11B" runat="server" MaxLength="160" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
 </dxe:ASPxTextBox>
 </div>
 <table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td style="padding-top: 4px; text-align: center"><dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                     Width="850px" ID="rpAssess11" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
<panelcollection>
<dxp:PanelContent runat="server"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 47px; TEXT-ALIGN: right"><dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess11Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>

 </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left"><dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess11Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>

 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</panelcollection>
</dxrp:ASPxRoundPanel>
 </td></tr></tbody></table></dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel3" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="12" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>12</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=12');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel3_div12');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 250px; width: 880px" visible="true">
                                            <div id="div12" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 260px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=12');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel3_div12');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                <dxe:ASPxLabel runat="server" EncodeHtml="False"
                    ForeColor="Black" ID="qText12" EnableViewState="False">
                </dxe:ASPxLabel>
 <br /><br /><dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
     CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
     ClientInstanceName="qAnswer12" Width="100%" ID="qAnswer12">
     <Items>
         <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
         <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
     </Items>
     <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 12 - Answer is required.">
         <RequiredField IsRequired="True"></RequiredField>
     </ValidationSettings>
 </dxe:ASPxRadioButtonList>
 <br />c) Explain your company's usual process to eliminate safety hazards below.<br /><dxe:ASPxMemo
     runat="server" Height="71px" Width="100%" AutoResizeWithContainer="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
     CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
     ClientInstanceName="qAnswer12Comments" ID="qAnswer12Comments">
     <DisabledStyle BackColor="#E0E0E0">
     </DisabledStyle>
     <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 12 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
 </dxe:ASPxMemo>
 <br /><strong>Example </strong><em>(Please Upload a file which contains examples or
     supports your answer given above)<br />
 </em><asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
     Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer12AExampleText"
     Visible="False"></asp:Label>
 <dxe:ASPxHyperLink runat="server" ClientInstanceName="qAnswer12AExampleLink" ID="qAnswer12AExampleLink">
 </dxe:ASPxHyperLink>
 <br />
 <table><tr><td width="600px">
 <dx:ASPxUploadControl ID="qAnswer12AExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        </td><td width="20px"><dxe:ASPxImage ID="qAnswer12AExample_Error" ImageUrl="~/Images/error.gif" ToolTip="Q12 A) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
        </div>
 <table border="0" cellpadding="0" cellspacing="0" width="100%">
     <tr>
         <td style="padding-top: 10px; text-align: center">
             <dxrp:ASPxRoundPanel id="rpAssess12" runat="server" HeaderText="Assessor Reviewal & Comments"
                 Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                 <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess12Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess12Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
             </dxrp:ASPxRoundPanel>
         </td>
     </tr>
 </table>
 </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel4" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="13" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>13</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=13');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel4_div13');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 270px; width: 880px" visible="true">
                                            <div id="div13" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=13');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel4_div13');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText13" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer13">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="E" Value="E"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 13 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments<br />
                    </strong>
                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" AutoResizeWithContainer="True"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswer13Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 13 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
                    </dxe:ASPxMemo>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess13" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess13Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess13Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel5" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="14" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>14</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=14');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel5_div14');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 250px; width: 880px" visible="true">
                                            <div id="div14" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 260px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=14');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel5_div14');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText14" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer14A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 14 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="7px" ForeColor="Black" ID="qAnswerText14A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer14B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 14 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText14B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer14C">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 14 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText14C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer14D">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings CausesValidation="True" SetFocusOnError="False" ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit" RequiredField-ErrorText="Question 14 d) - Answer is required.">
            <RequiredField IsRequired="True" />
        </ValidationSettings>
                                        <ClientSideEvents SelectedIndexChanged="function(s, e) {
	        client_qAnswer14Comments.Validate();
	    }" />
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText14D" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" AutoResizeWithContainer="True"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswer14Comments" ClientInstanceName="client_qAnswer14Comments" OnValidation="qAnswer14Comments_Validation">
                               <ValidationSettings SetFocusOnError="False" CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Q14 D) Comments must be specified as 'Yes' was selected." ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess14" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess14Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess14Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel6" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="15" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>15</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=15');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel6_div15');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 260px; width: 880px" visible="true">
                                            <div id="div15" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 270px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=15');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel6_div15');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText15" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer15A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 15 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText15A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer15AComments" OnValidation="qAnswer15AComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 15 a) Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                        
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer15B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 15 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText15B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer15BComments" OnValidation="qAnswer15BComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 15 b) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer15C">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 15 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText15C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer15CComments" OnValidation="qAnswer15CComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 15 c) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess15" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess15Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess15Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel7" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="16" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>16</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=16');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel7_div16');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 270px; width: 880px" visible="true">
                                            <div id="div16" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=16');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel7_div16');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table id="Table1" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText16" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer16A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 16 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                    <br />
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText16A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer16AComments" OnValidation="qAnswer16AComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 16 a) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer16B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 16 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText16B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer16BComments" OnValidation="qAnswer16BComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 16 b) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer16C">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 16 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText16C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer16CComments" OnValidation="qAnswer16CComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 16 c) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess16" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess16Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess16Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel8" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="17" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>17</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                           <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=17');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel8_div17');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 180px; width: 880px" visible="true">
                                            <div id="div17" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 190px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=17');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel8_div17');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table id="Table3" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText17" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer17A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 17 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 728px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText17A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer17B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 17 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 728px; height: 13px">
                                    <br />
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText17B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    &nbsp;</td>
                                <td style="width: 728px; height: 13px">
                                    <strong>Example </strong><em>(Please attach a copy of your completed training matrix
                                        or company procedure that identifies your companies training process that supports
                                        your answer given above)</em><br />
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer17BExampleText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" ClientInstanceName="qAnswer17BExampleLink" ID="qAnswer17BExampleLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <table><tr><td width="600px">
                                    <dx:ASPxUploadControl ID="qAnswer17BExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        </td><td width="20px"><dxe:ASPxImage ID="qAnswer17BExample_Error" ImageUrl="~/Images/error.gif" ToolTip="Q17 B) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess17" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess17Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess17Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel9" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" Font-Bold="False" HeaderText="18" EnableDefaultAppearance="False"
            BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>18</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=18');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel9_div18');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0" Font-Bold="True">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 240px; width: 880px" visible="true">
                                            <div id="div18" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 250px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=18');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel9_div18');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText18" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer18">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 18 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments (Optional)</strong><br />
                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ID="qAnswer18Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                    </dxe:ASPxMemo>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess18" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess18Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess18Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel10" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="19" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>19</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=19');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel10_div19');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 240px; width: 880px" visible="true">
                                            <div id="div19" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 250px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=19');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel10_div19');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText19" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer19">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 19 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments (Optional)</strong><br />
                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ID="qAnswer19Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                    </dxe:ASPxMemo>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess19" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess19Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess19Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel11" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="20" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>20</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=20');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel11_div20');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 210px; width: 880px" visible="true">
                                            <div id="div20" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 220px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=20');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel11_div20');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText20" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer20A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 20 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText20A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer20AComments" OnValidation="qAnswer20AComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 20 a) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer20B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 20 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                    <br />
                                </td>
                                <td style="width: 716px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="7px" ForeColor="Black" ID="qAnswerText20B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer20BComments" OnValidation="qAnswer20BComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 20 b) - Comments must be specified as 'Yes' was selected" ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess20" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess20Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess20Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel12" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" Font-Bold="False" HeaderText="21" EnableDefaultAppearance="False"
            BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>21</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=21');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel12_div21');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0" Font-Bold="True">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 230px; width: 880px" visible="true">
                                            <div id="div21" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 240px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=21');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel12_div21');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText21" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer21">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 21 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments</strong><br />
                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ID="qAnswer21Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 21 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
                    </dxe:ASPxMemo>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 10px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess21" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess21Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess21Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel13" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="22" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>22</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=22');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel13_div22');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 360px; width: 880px" visible="true">
                                            <div id="div22" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 370px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=22');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel13_div22');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText22" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer22A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 22 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="7px" ForeColor="Black" ID="qAnswerText22A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer22B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 22 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="7px" ForeColor="Black" ID="qAnswerText22B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer22C">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 22 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                    <br />
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="12px" ForeColor="Black" ID="qAnswerText22C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr style="font-size: 9pt; font-family: Tahoma, Verdana, Arial">
                                <td style="height: 13px" colspan="2">
                                    <strong>
                                        <br />
                                        Comments</strong><dxe:ASPxMemo runat="server" Height="71px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ID="qAnswer22Comments">
                                            <DisabledStyle BackColor="#E0E0E0">
                                            </DisabledStyle>
                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 22 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
                                        </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr style="font-size: 9pt; font-family: Tahoma, Verdana, Arial">
                                <td style="height: 13px" colspan="2">
                                    <strong>
                                        <br />
                                        Example </strong><em>(Please Upload a file which contains examples or supports your
                                            answers given above)<br />
                                        </em>
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer22ExampleText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" ClientInstanceName="qAnswer22ExampleLink" ID="qAnswer22ExampleLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <table><tr><td width="600px">
                                    <dx:ASPxUploadControl ID="qAnswer22Example" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        </td><td width="20px"><dxe:ASPxImage ID="qAnswer22Example_Error" ImageUrl="~/Images/error.gif" ToolTip="Q22) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 4px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess22" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess22Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>




                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess22Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>




                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel14" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="23" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>23</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=23');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel14_div23');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 300px; width: 880px" visible="true">
                                            <div id="div23" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 310px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=23');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel14_div23');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText23" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer23">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 23 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments</strong><br />
                    <dxe:ASPxMemo runat="server" Height="71px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ID="qAnswer23Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 23 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
                    </dxe:ASPxMemo>
                    <br />
                    <strong>Example </strong><em>(Please Upload a file which contains examples or supports
                        your answer given above)<br />
                    </em>
                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                        Font-Bold="False" Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer23ExampleText"
                        Visible="False"></asp:Label>
                    <dxe:ASPxHyperLink runat="server" ClientInstanceName="qAnswer23ExampleLink" ID="qAnswer23ExampleLink">
                    </dxe:ASPxHyperLink>
                    <br />
                    <table><tr><td width="600px">

                    <dx:ASPxUploadControl ID="qAnswer23Example" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        
</td><td width="20px"><dxe:ASPxImage ID="qAnswer23Example_Error" ImageUrl="~/Images/error.gif" ToolTip="Q23) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
</div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess23" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess23Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess23Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel15" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="24" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>24</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=24');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel15_div24');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 240px; width: 880px" visible="true">
                                            <div id="div24" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 250px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=24');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel15_div24');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        ForeColor="Black" ID="qText24" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    </dxe:ASPxLabel>
                    <br />
                    <br />
                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        Width="100%" ID="qAnswer24">
                        <Items>
                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                            <dxe:ListEditItem Text="D" Value="D"></dxe:ListEditItem>
                        </Items>
                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="False" ValidationGroup="Submit" RequiredField-ErrorText="Question 24 - Answer is required.">
                            <RequiredField IsRequired="True"></RequiredField>
                        </ValidationSettings>
                    </dxe:ASPxRadioButtonList>
                    <br />
                    <strong>Comments</strong><dxe:ASPxMemo runat="server" Height="71px" Width="100%"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswer24Comments">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                    </dxe:ASPxMemo>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess24" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess24Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess24Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel16" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="25" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>25</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=25');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel16_div25');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 380px; width: 880px" visible="true">
                                            <div id="div25" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 390px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=25');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel16_div25');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText25" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                        <br />
                                        <br />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer25A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 25 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText25A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                    <br />
                                    <span style="font-size: 9pt"><strong>Example </strong><em>(Please Upload a file which
                                        contains examples or supports your answer given above)<br />
                                    </em>
                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer25AExampleText"
                                            Visible="False"></asp:Label>
                                        <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer25AExampleLink">
                                        </dxe:ASPxHyperLink>
                                        <br />
                                    </span>
                                    <table><tr><td width="600px"><dx:ASPxUploadControl ID="qAnswer25AExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl></td><td width="20px"><dxe:ASPxImage ID="qAnswer25AExample_Error" ImageUrl="~/Images/error.gif" ToolTip="Q25 A) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
        
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer25B">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 25 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                    <br />
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText25B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                    <br />
                                    <span style="font-size: 9pt"><strong>Example </strong><em>(Please Upload a file which
                                        contains examples or supports your answer given above)</em><br />
                                    </span>
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer25BExampleText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer25BExampleLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <table><tr><td width="600px">
                                    <dx:ASPxUploadControl ID="qAnswer25BExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        </td><td width="20px"><dxe:ASPxImage ID="qAnswer25BExample_Error" ImageUrl="~/Images/error.gif" ToolTip="Q25 B) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer25C">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 25 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText25C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                    <br />
                                    <span style="font-size: 9pt"><strong>Example </strong><em>(Please Upload a file which
                                        contains examples or supports your answer given above)<br />
                                    </em>
                                        <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer25CExampleText"
                                            Visible="False"></asp:Label>
                                        <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer25CExampleLink">
                                        </dxe:ASPxHyperLink>
                                        <br />
                                    </span>
                                    <table><tr><td width="600px">
                                    <dx:ASPxUploadControl ID="qAnswer25CExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        </td><td width="20px"><dxe:ASPxImage ID="qAnswer25CExample_Error" ImageUrl="~/Images/error.gif" ToolTip="Q25 C) Example expected if 'Yes' selected" Visible="false" runat="server"></dxe:ASPxImage></td></tr></table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess25" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess25Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess25Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel17" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="26" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>26</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=26');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel17_div26');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 210px; width: 880px" visible="true">
                                            <div id="div26" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 220px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=26');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel17_div26');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText26" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer26A">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 26 - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 716px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText26A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <strong><span style="font-size: 9pt">Comments</span></strong><br />
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer26AComments">
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" SetFocusOnError="False"
         ValidationGroup="Submit" RequiredField-ErrorText="Question 26 - Comments are required.">
         <RequiredField IsRequired="True"></RequiredField>
    </ValidationSettings>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <span style="font-size: 9pt"><strong>Example </strong><em>(Please Upload a file which
                                        contains examples or supports your answer given above)<br />
                                    </em></span>
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer26AExampleText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer26AExampleLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <dx:ASPxUploadControl ID="qAnswer26AExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
                                    </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess26" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess26Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess26Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel18" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="27" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>27</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=27');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel18_div27');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 280px; width: 880px" visible="true"> <%--DT224 Cindi Thornton, increased height for new comment--%>
                                            <div id="div27" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 290px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=27');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel18_div27');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText27" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                        <br />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 555px">
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="Year 1" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ForeColor="Black"
                                                        ID="qAnswer27Year1" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="Year 2" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ForeColor="Black"
                                                        ID="qAnswer27Year2" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="Year 3" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ForeColor="Black"
                                                        ID="qAnswer27Year3" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 555px; height: 19px">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ForeColor="Black" ID="qAnswerText27A" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27A1">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 a) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27A2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 a) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27A3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 a) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 555px; height: 30px">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ForeColor="Black" ID="qAnswerText27B" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27B1">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 b) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27B2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 b) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 b) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27B3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 b) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 b) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 555px; height: 30px">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ForeColor="Black" ID="qAnswerText27C" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27C1">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 c) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 c) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27C2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 c) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 c) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27C3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 c) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 c) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 555px; height: 30px">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ForeColor="Black" ID="qAnswerText27D" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27D1">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 d) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 d) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27D2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" ValidationGroup="Submit" RequiredField-ErrorText="Question 27 d) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 d) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27D3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 d) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 d) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 555px; height: 30px">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ForeColor="Black" ID="qAnswerText27E" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27E1">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 e) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 e) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27E2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 e) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 e) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 115px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer27E3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 27 e) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 27 e) - Not Valid Number (eg. 50)" ValidationExpression="^\d*$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr><td>                     
                                <br />
                                <%--DT224 Cindi Thornton, new comment--%>
                    <strong>Comments</strong><dxe:ASPxMemo runat="server" Height="71px" Width="100%"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswer27Comments"  NullText ="Enter any comments in relation to data entered above. e.g. Data is per financial year or Data is per Calendar year">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                    </dxe:ASPxMemo></td></tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess27" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess27Approval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess27Comments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel19" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="28" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>28</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=28');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel19_div28');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 350px; width: 880px" visible="true"> <%--DT224 Cindi Thornton, increased height for new comment--%>
                                            <div id="div28" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 280px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=28');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel19_div28');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText28" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                        <br />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="padding-right: 7px; width: 510px; height: 19px; text-align: right">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText28A1" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="28A2" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ID="qAnswerText28A2"
                                                        EnableViewState="False">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="28A3" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ForeColor="Black"
                                                        ID="qAnswerText28A3" EnableViewState="False">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 129px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" Text="28A6" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" ForeColor="Black"
                                                        ID="qAnswerText28A6" EnableViewState="False">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 7px; width: 510px; height: 41px; text-align: right">
                                                    &nbsp;<dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" Font-Italic="True"
                                                        ForeColor="Black" ID="qAnswer28A1" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28A2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="Invalid Value"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 a) - Answer is required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28A3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 a) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 129px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28A6">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 a) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 a) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 7px; width: 510px; height: 41px; text-align: right">
                                                    &nbsp;<dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" Font-Italic="True"
                                                        ForeColor="Black" ID="qAnswer28B1" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28B2">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit" RequiredField-ErrorText="Question 28 b) - Answer is required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 130px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28B3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 b) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 b) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 129px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28B6">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 b) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 b) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 7px; width: 510px; height: 41px; text-align: right">
                                                    &nbsp;<dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue"
                                                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Font-Bold="True" Font-Italic="True"
                                                        ForeColor="Black" ID="qAnswer28C1" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 130px; height: 41px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28C2" ValidationGroup="Submit" RequiredField-ErrorText="Question 28 c) - Answer is required.">
                                                         <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ValidationGroup="Submit" RequiredField-ErrorText="Question 28 c) - Answer is required.">
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 130px; height: 41px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28C3">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                           ValidationGroup="Submit" RequiredField-ErrorText="Question 28 c) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 c) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 129px; height: 41px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer28C6">
                                                        <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" SetFocusOnError="False"
                                                            ValidationGroup="Submit" RequiredField-ErrorText="Question 28 c) - Answer is required.">
                                                            <RegularExpression ErrorText="Question 28 c) - Not Valid Number (eg. 50.5123)" ValidationExpression="^\d+(?:\.\d{0,4})?$">
                                                            </RegularExpression>
                                                            <RequiredField IsRequired="True"></RequiredField>
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 900px; height: 100px"><%--DT224 Cindi Thornton, new comment--%>
                                    <strong>Comments</strong><dxe:ASPxMemo runat="server" Height="71px" Width="100%"
                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" ID="qAnswer28Comments" NullText ="Enter any comments in relation to data entered above. e.g. Data is per financial year or Data is per Calendar year">
                        <DisabledStyle BackColor="#E0E0E0">
                        </DisabledStyle>
                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                                                        <tr>
                                <td colspan="2">
                                    <br />
                                    <span style="font-size: 9pt"><strong>Workers Compensation Approval Letter </strong><em>(for self insured only)<br />
                                    </em></span>
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer28LetterText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer28LetterLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <dx:ASPxUploadControl ID="qAnswer28Letter" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
                                    </td>
                            </tr>
                        </tbody>
                    </table>
           
                    
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess28" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess28Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>


                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess28Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>


                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel20" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="29" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 50px">
                            <b>29</b>
                        </td>
                        <td align="right" style="text-align: right; width: 850px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=29i');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel20_div29');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 350px; width: 880px" visible="true">
                                            <div id="div29" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 360px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=29i');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel20_div29');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText29i" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer29iA">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 29 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText29iA" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer29iAComments" OnValidation="qAnswer29iAComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 29 a) - Comments must be specified as 'Yes' was selected." ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer29iB">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 29 b) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText29iB" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer29iBComments" OnValidation="qAnswer29iBComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 29 b) - Comments must be specified as 'Yes' was selected." ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer29iC">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 29 c) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText29iC" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer29iCComments" OnValidation="qAnswer29iCComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 29 c) - Comments must be specified as 'Yes' was selected." ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer29iD">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 29 d) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText29iD" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                </td>
                                <td style="width: 717px; height: 13px">
                                    <dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ID="qAnswer29iDComments" OnValidation="qAnswer29iDComments_Validation">
                               <ValidationSettings CausesValidation="True" EnableCustomValidation="True" 
            ValidationGroup="submit" 
            ErrorText="Question 29 d) - Comments must be specified as 'Yes' was selected." ErrorDisplayMode="ImageWithTooltip">
        </ValidationSettings>
                                        <DisabledStyle BackColor="#E0E0E0">
                                        </DisabledStyle>
                                    </dxe:ASPxMemo>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 4px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess29i" runat="server" HeaderText="Assessor Reviewal & Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
                            <dxp:PanelContent runat="server">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td style="height: 47px; text-align: right">
                                            <dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess29iApproval" SelectedIndex="2">
                                                <Items>
                                                    <dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1" />
                                                    <dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0" />
                                                    <dxe:ListEditItem Selected="True" Text="(Select Approval)" Value="" />
                                                </Items>
                                                <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif">
                                                </LoadingPanelImage>
                                                <ValidationSettings CausesValidation="True" ValidateOnLeave="False" ValidationGroup="Assess_Submit" SetFocusOnError="False" Display="Dynamic">
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                    <RequiredField IsRequired="False" />
                                                </ValidationSettings>
                                            </dxe:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px; text-align: left">
                                            <dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess29iComments">
                                                <NullTextStyle ForeColor="Gray">
                                                </NullTextStyle>
                                                <ValidationSettings>
                                                    <ErrorFrameStyle ImageSpacing="4px">
                                                        <ErrorTextPaddings PaddingLeft="4px" />
                                                    </ErrorFrameStyle>
                                                </ValidationSettings>
                                            </dxe:ASPxMemo>
                                        </td>
                                    </tr>
                                </table>
                            </dxp:PanelContent>
                        </panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel21" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="30" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 50px">
                            <b>30</b>
                        </td>
                        <td align="right" style="text-align: right; width: 850px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=29ii');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel21_div30');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 210px; width: 880px" visible="true">
                                            <div id="div30" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 220px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=29ii');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel21_div30');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="height: 13px" colspan="2">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText29ii" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 160px; height: 13px">
                                    <br />
                                    <dxe:ASPxRadioButtonList runat="server" ItemSpacing="10px" RepeatDirection="Horizontal"
                                        ID="qAnswer29iiA">
                                        <Items>
                                            <dxe:ListEditItem Text="Yes" Value="Yes"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="No" Value="No"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 30 a) - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                </td>
                                <td style="width: 728px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Height="7px" ForeColor="Black" ID="qAnswerText29iiA" EnableViewState="False"
                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                </td>
                            </tr>
                            <tr style="font-size: 9pt">
                                <td style="height: 13px" colspan="2">
                                    <strong>
                                        <br />
                                        Comments (Optional)</strong><dxe:ASPxMemo runat="server" Height="39px" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ID="qAnswer29iiComments">
                                            <DisabledStyle BackColor="#E0E0E0">
                                            </DisabledStyle>
                                        </dxe:ASPxMemo>
                                    <br />
                                    <strong>Example </strong><em>(Please Upload a file which contains examples or supports
                                        your answer given above)<br />
                                    </em>
                                    <asp:Label runat="server" Text="The following file is currently saved. Uploading another file will overwrite this: "
                                        Font-Names="Tahoma" Font-Size="9pt" ForeColor="Red" ID="qAnswer29iiAExampleText"
                                        Visible="False"></asp:Label>
                                    <dxe:ASPxHyperLink runat="server" Font-Names="Tahoma" Font-Size="9pt" ID="qAnswer29iiAExampleLink">
                                    </dxe:ASPxHyperLink>
                                    <br />
                                    <dx:ASPxUploadControl ID="qAnswer29iiAExample" runat="server" Width="100%" ShowProgressPanel="true">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="16252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 15Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tbody>
                            <tr>
                                <td style="padding-top: 10px; text-align: center">
                                    <dxrp:ASPxRoundPanel runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                        Width="850px" ID="rpAssess29ii" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                        <panelcollection>
<dxp:PanelContent runat="server"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 47px; TEXT-ALIGN: right"><dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess29iiApproval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>

 </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left"><dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess29iiComments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>

 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</panelcollection>
                                    </dxrp:ASPxRoundPanel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel22" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
            Width="900px" HeaderText="31" EnableDefaultAppearance="False" BackColor="#DDECFE">
            <HeaderTemplate>
                <table>
                    <tr>
                        <td style="width: 20px">
                            <b>31</b>
                        </td>
                        <td align="right" style="text-align: right; width: 900px;">
                            <img src="Images/smileyFace.gif" />&nbsp;<span style="color: #ffff00;">For Assistance, use: &nbsp;<a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=30');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel22_div31');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><strong></strong>Get Help with this Question</a>&nbsp;<img src="Images/help.gif" />
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <TopEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpTopEdge.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </TopEdge>
            <ContentPaddings Padding="2px" PaddingTop="10px" PaddingBottom="10px"></ContentPaddings>
            <HeaderRightEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderRightEdge>
            <Border BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></Border>
            <HeaderLeftEdge>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderLeftEdge>
            <HeaderStyle BackColor="#7BA4E0">
                <Paddings Padding="0px" PaddingLeft="2px" PaddingRight="2px" PaddingBottom="7px"></Paddings>
                <BorderBottom BorderColor="#002D96" BorderStyle="Solid" BorderWidth="1px"></BorderBottom>
            </HeaderStyle>
            <HeaderContent>
                <BackgroundImage ImageUrl="~/App_Themes/Office2003Blue/Web/rpHeader.png" Repeat="RepeatX"
                    HorizontalPosition="left" VerticalPosition="top"></BackgroundImage>
            </HeaderContent>
            <DisabledStyle ForeColor="Gray">
            </DisabledStyle>
            <NoHeaderTopEdge BackColor="#DDECFE">
            </NoHeaderTopEdge>
            <PanelCollection>
                <dxp:PanelContent runat="server">
                <div style="z-index: 10; height: 260px; width: 880px" visible="true">
                                            <div id="div31" class="transparency" runat="server" style="position: absolute; z-index: 11; height: 270px; width: 880px; visibility: visible; text-align: center" align="center">
                                <br /><br /><br /><br /><br /><br /><br /><br /><br />
<span class="redbold">You will be unable to submit the answer to this question until you have clicked on the<br /><br />
                                <a href="javascript:popUp('PopUps/QuestionnaireRationale.aspx?Type=Supplier&SubType=Help&QuestionId=30');setInvisible('ctl00_body_Questionnaire2Supplier1_ASPxRoundPanel22_div31');" style="color: #ffff00; text-decoration: underline; font-weight: bold"><img src="Images/ForAssistanceR.PNG" /></a> or link (top right).<br /><br />
                                Please review the "Get help with this question" to assist you with your response.
                                </span>
                                </div>
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <span style="color: #ff0000">
                                        <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            ForeColor="Black" ID="qText30" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                        </dxe:ASPxLabel>
                                        <br />
                                        <br />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <dxe:ASPxRadioButtonList runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        Width="100%" ID="qAnswer30">
                                        <Items>
                                            <dxe:ListEditItem Text="A" Value="A"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="B" Value="B"></dxe:ListEditItem>
                                            <dxe:ListEditItem Text="C" Value="C"></dxe:ListEditItem>
                                        </Items>
                                        <ValidationSettings SetFocusOnError="False" ValidationGroup="Submit" ErrorDisplayMode="ImageWithTooltip" RequiredField-ErrorText="Question 31 - Answer is required.">
                                            <RequiredField IsRequired="True"></RequiredField>
                                        </ValidationSettings>
                                    </dxe:ASPxRadioButtonList>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 900px; height: 13px">
                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                        ForeColor="Black" ID="qAnswerText30D" EnableViewState="False" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                    </dxe:ASPxLabel>
                                    <br />
                                    <br />
                                    <table style="width: 100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td style="width: 136px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText30E1" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 145px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText30E2" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 104px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText30E3" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 105px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText30E3B" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                                <td style="width: 200px; height: 19px; text-align: center">
                                                    <dxe:ASPxLabel runat="server" EncodeHtml="False" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        Font-Bold="True" ForeColor="Black" ID="qAnswerText30E4" EnableViewState="False"
                                                        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                                                    </dxe:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 136px; height: 19px; text-align: center">
                                                    <dxe:ASPxComboBox runat="server" DropDownStyle="DropDown" ValueType="System.String"
                                                        Width="130px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30E1">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                                <td style="width: 145px; height: 40px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="140px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30E2">
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 104px; height: 40px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30E3">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 105px; height: 40px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30E3B">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 200px; height: 40px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30E4">
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 136px; height: 19px; text-align: center">
                                                    <dxe:ASPxComboBox runat="server" DropDownStyle="DropDown" ValueType="System.String"
                                                        Width="130px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30F1">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                                <td style="width: 145px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="140px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30F2">
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 104px; height: 19px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30F3">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 105px; height: 19px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30F3B">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 200px; height: 19px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30F4">
                                                        <ValidationSettings ValidateOnLeave="False">
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 136px; height: 41px; text-align: center">
                                                    <dxe:ASPxComboBox runat="server" DropDownStyle="DropDown" ValueType="System.String"
                                                        Width="130px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30G1">
                                                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                                                        </LoadingPanelImage>
                                                        <ButtonStyle Width="13px">
                                                        </ButtonStyle>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                                <td style="width: 145px; height: 41px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="140px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30G2">
                                                    </dxe:ASPxTextBox>
                                                </td>
                                                <td style="width: 104px; height: 41px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30G3">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 105px; height: 41px; text-align: center">
                                                    <dxe:ASPxDateEdit runat="server" Width="100px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30G3B">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                                <td style="width: 200px; height: 41px; text-align: center">
                                                    <dxe:ASPxTextBox runat="server" Width="100%" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                                        CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                                        ID="qAnswer30G4">
                                                        <ValidationSettings ValidateOnLeave="False">
                                                        </ValidationSettings>
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-top: 2px; text-align: center">
                                <dxrp:ASPxRoundPanel id="rpAssess30" runat="server" HeaderText="Assessor Reviewal &amp; Comments"
                                    Width="850px" CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" 
                    EnableDefaultAppearance="False" GroupBoxCaptionOffsetX="6px" 
                    GroupBoxCaptionOffsetY="-19px" 
                    SpriteCssFilePath="~/App_Themes/DevEx/{0}/sprite.css">
                                                            <ContentPaddings PaddingBottom="10px" PaddingLeft="7px" PaddingRight="11px" 
                        PaddingTop="10px" />
                                                            <headerstyle backcolor="#E9E9E9" font-bold="True" horizontalalign="Center" forecolor="#C00000">
                            <BorderBottom BorderStyle="None" />
                            <Paddings PaddingBottom="6px" PaddingLeft="7px" PaddingRight="11px" PaddingTop="1px" />
                        </headerstyle>
                                    <panelcollection>
<dxp:PanelContent runat="server"><TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 border=0><TBODY><TR><TD style="HEIGHT: 47px; TEXT-ALIGN: right"><dxe:ASPxComboBox runat="server" IncrementalFilteringMode="StartsWith" SelectedIndex="2" ShowImageInEditBox="True" ValueType="System.Int32" Width="200px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess30Approval"><Items>
<dxe:ListEditItem ImageUrl="~/Images/greentick.gif" Text="Approved" Value="1"></dxe:ListEditItem>
<dxe:ListEditItem ImageUrl="~/Images/redcross.gif" Text="Not Approved" Value="0"></dxe:ListEditItem>
<dxe:ListEditItem Text="(Select Approval)" Selected="True"></dxe:ListEditItem>
</Items>

<LoadingPanelImage Url="~/App_Themes/PlasticBlue/Editors/Loading.gif"></LoadingPanelImage>

<ValidationSettings CausesValidation="True" Display="Dynamic" ValidateOnLeave="False" SetFocusOnError="False" ValidationGroup="Assess_Submit">
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxComboBox>

 </TD></TR><TR><TD style="WIDTH: 100px; TEXT-ALIGN: left"><dxe:ASPxMemo runat="server" Height="80px" NullText="Enter Assessment Comments here..." Width="850px" SpriteCssFilePath="~/App_Themes/PlasticBlue/{0}/sprite.css" CssPostfix="PlasticBlue" CssFilePath="~/App_Themes/PlasticBlue/{0}/styles.css" ID="assess30Comments">
<NullTextStyle ForeColor="Gray"></NullTextStyle>

<ValidationSettings>
<ErrorFrameStyle ImageSpacing="4px">
<ErrorTextPaddings PaddingLeft="4px"></ErrorTextPaddings>
</ErrorFrameStyle>
</ValidationSettings>
</dxe:ASPxMemo>

 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</panelcollection>
                                </dxrp:ASPxRoundPanel>
                            </td>
                        </tr>
                    </table>
                </dxp:PanelContent>
            </PanelCollection>
        </dxrp:ASPxRoundPanel>
        <br />
        <asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
        <data:SitesDataSource ID="SitesDataSource" runat="server" SelectMethod="GetAll" EnableCaching="True">
            <DeepLoadProperties Method="IncludeChildren" Recursive="False">
            </DeepLoadProperties>
        </data:SitesDataSource>
        <data:QuestionnaireServicesCategoryDataSource ID="QuestionnaireServicesCategoryDataSource"
            runat="server">
            <DeepLoadProperties Method="IncludeChildren" Recursive="False">
            </DeepLoadProperties>
        </data:QuestionnaireServicesCategoryDataSource>
        <table width="900px">
            <tbody>
                <tr width="900px">
                <td style="text-align: left">
                <dxe:ASPxValidationSummary ID="ASPxValidationSummary1" runat="server" HeaderText="Validation Errors - You must correct these before being able to submit:"
                ShowErrorsInEditors="True" ValidationGroup="Submit">
            </dxe:ASPxValidationSummary>
                <br />
                </td>
                </tr>
                <tr width="900px">
                    <td style="text-align: left" align="left" class="pageName">
                        <dxe:ASPxCheckBox ID="ASPxCheckBox1" Text="By submitting this questionnaire I hereby certify that all statements provided herein are a true and correct representation of company wide information. I also acknowledge that all other previous submissions will become null and void."
                            runat="server" Width="726px" ForeColor="Maroon">
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" CausesValidation="True" RequiredField-ErrorText="Submission Verification - You are required to check this."
                                SetFocusOnError="False" ValidationGroup="Submit">
                                <RequiredField IsRequired="True"></RequiredField>
                            </ValidationSettings>
                        </dxe:ASPxCheckBox>
                        <br />
                        <dxe:ASPxButton ID="btnSubmit" OnClick="btnSubmit_Click" Text="Verify all information has been supplied, Save & Close"
                                            runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" ValidationGroup="Submit">
                                            <ClientSideEvents Click="function(s, e) {
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                                        <br />
                                        <dxe:ASPxButton ID="ASPxButton2" OnClick="btnSave_Click" Text="Save Draft (Information may still be missing but may be added in at a later stage)" runat="server"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" UseSubmitBehavior="False">
                                            <ClientSideEvents Click="function(s, e) {
    confirmSave();
	e.processOnServer = confirm('Are you sure you wish to save your changes?');
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                                        <br />
                                        <dxe:ASPxButton ID="ASPxButton1" OnClick="btnHome_Click" Text="Home" runat="server"
                                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" CssPostfix="Office2003Blue"
                                            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css">
                                            <ClientSideEvents Click="function(s, e) { e.processOnServer = confirm(confirmMsg);
}" />
                                        </dxe:ASPxButton>
                                        <br />
                                        <dxe:ASPxButton ID="btnSaveAssessment" OnClick="btnSaveAssessment_Click" Text="Save Assessment"
                                            runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" ValidationGroup="Assess_Submit" Width="150px" Visible="false">
                                            <ClientSideEvents Click="function(s, e) {
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                                        <dxe:ASPxButton ID="btnSaveAssessmentGoHome" OnClick="btnSaveAssessmentGoHome_Click"
                                            Text="Save Assessment & Go Home" runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" ValidationGroup="Assess_Submit" Width="200px" Visible="false">
                                            <ClientSideEvents Click="function(s, e) {
}"></ClientSideEvents>
                                        </dxe:ASPxButton>
                    </td>
                </tr>
                <tr width="900px">
                    <td style="text-align: right" class="pageName">
                        <a href="#top">Scroll to top of page</a>
                        <br />
                        <br />
                    </td>
                </tr>
            </tbody>
        </table>
        
        <dxpc:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" BackColor="Info" CloseAction="OuterMouseClick"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                EnableHotTrack="False" ForeColor="Black" Modal="True" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" HeaderText="Warning" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="800px">
                <ContentCollection>
                    <dxpc:PopupControlContentControl runat="server">
                    <table width="100%">
                    <tr><td width="100%" align="center" style="text-align: center">
                    <br /><br />
                        <DIV style="BORDER-RIGHT: black 1px solid; PADDING-RIGHT: 10px; BORDER-TOP: black 1px solid; PADDING-LEFT: 10px; PADDING-BOTTOM: 10px; BORDER-LEFT: black 1px solid; PADDING-TOP: 10px; BORDER-BOTTOM: black 1px solid; BACKGROUND-COLOR: #ffffcc; TEXT-ALIGN: center"><SPAN style="FONT-SIZE: 13pt; COLOR: maroon"><STRONG><SPAN style="FONT-FAMILY: Verdana">
    <asp:Label ID="lblBox1_PopUp" runat="server" Text="Error: Questionnaire Not Submitted."></asp:Label></SPAN><BR /></STRONG><SPAN style="COLOR: maroon"><SPAN style="FONT-SIZE: 10pt; FONT-FAMILY: Verdana">
    <asp:Label ID="lblBox2_PopUp" runat="server" Text="Your questionnaire could not be saved as one or more files you intended to upload is incorrectly formatted or sized. Please scroll down and correct this, then try saving again."></asp:Label>
        <br />
        <br />
        <dxe:ASPxButton ID="ASPxButton3"  Text="Close"
                                            runat="server" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                            CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                            UseSubmitBehavior="False" Width="150px" Visible="true" AutoPostBack="false">
                                            <ClientSideEvents Click="function(s, e) { ASPxPopupControl2.Hide();
}"></ClientSideEvents>
        </dxe:ASPxButton>
        <BR /></SPAN></SPAN></SPAN></DIV>
                        <br />
                        </td></tr>
                        </table>
                    </dxpc:PopupControlContentControl>
                </ContentCollection>
                <HeaderStyle>
                    <Paddings PaddingRight="6px" />
                </HeaderStyle>
            </dxpc:ASPxPopupControl>
            
    </contenttemplate>
    <triggers>
        <asp:PostBackTrigger ControlID="btnSave"></asp:PostBackTrigger>
        <asp:PostBackTrigger ControlID="btnSubmit"></asp:PostBackTrigger>
        <asp:PostBackTrigger ControlID="ASPxButton2"></asp:PostBackTrigger>
        <asp:PostBackTrigger ControlID="btnSaveAssessment"></asp:PostBackTrigger>
        <asp:PostBackTrigger ControlID="btnSaveAssessment2"></asp:PostBackTrigger>
    </triggers>
</asp:updatepanel>
