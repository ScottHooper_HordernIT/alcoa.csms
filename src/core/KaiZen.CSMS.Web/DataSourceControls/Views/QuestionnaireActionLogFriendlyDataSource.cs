﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireActionLogFriendlyProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireActionLogFriendlyDataSourceDesigner))]
	public class QuestionnaireActionLogFriendlyDataSource : ReadOnlyDataSource<QuestionnaireActionLogFriendly>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyDataSource class.
		/// </summary>
		public QuestionnaireActionLogFriendlyDataSource() : base(new QuestionnaireActionLogFriendlyService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireActionLogFriendlyDataSourceView used by the QuestionnaireActionLogFriendlyDataSource.
		/// </summary>
		protected QuestionnaireActionLogFriendlyDataSourceView QuestionnaireActionLogFriendlyView
		{
			get { return ( View as QuestionnaireActionLogFriendlyDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireActionLogFriendlyDataSourceView class that is to be
		/// used by the QuestionnaireActionLogFriendlyDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireActionLogFriendlyDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireActionLogFriendly, Object> GetNewDataSourceView()
		{
			return new QuestionnaireActionLogFriendlyDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireActionLogFriendlyDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireActionLogFriendlyDataSourceView : ReadOnlyDataSourceView<QuestionnaireActionLogFriendly>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireActionLogFriendlyDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireActionLogFriendlyDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireActionLogFriendlyDataSourceView(QuestionnaireActionLogFriendlyDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireActionLogFriendlyDataSource QuestionnaireActionLogFriendlyOwner
		{
			get { return Owner as QuestionnaireActionLogFriendlyDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireActionLogFriendlyService QuestionnaireActionLogFriendlyProvider
		{
			get { return Provider as QuestionnaireActionLogFriendlyService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireActionLogFriendlyDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireActionLogFriendlyDataSource class.
	/// </summary>
	public class QuestionnaireActionLogFriendlyDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireActionLogFriendly>
	{
	}

	#endregion QuestionnaireActionLogFriendlyDataSourceDesigner

	#region QuestionnaireActionLogFriendlyFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyFilter : SqlFilter<QuestionnaireActionLogFriendlyColumn>
	{
	}

	#endregion QuestionnaireActionLogFriendlyFilter

	#region QuestionnaireActionLogFriendlyExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireActionLogFriendly"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireActionLogFriendlyExpressionBuilder : SqlExpressionBuilder<QuestionnaireActionLogFriendlyColumn>
	{
	}
	
	#endregion QuestionnaireActionLogFriendlyExpressionBuilder		
}

