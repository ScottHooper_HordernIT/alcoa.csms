﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdHocRadarItems2Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdHocRadarItems2DataSourceDesigner))]
	public class AdHocRadarItems2DataSource : ProviderDataSource<AdHocRadarItems2, AdHocRadarItems2Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2DataSource class.
		/// </summary>
		public AdHocRadarItems2DataSource() : base(new AdHocRadarItems2Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdHocRadarItems2DataSourceView used by the AdHocRadarItems2DataSource.
		/// </summary>
		protected AdHocRadarItems2DataSourceView AdHocRadarItems2View
		{
			get { return ( View as AdHocRadarItems2DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdHocRadarItems2DataSource control invokes to retrieve data.
		/// </summary>
		public AdHocRadarItems2SelectMethod SelectMethod
		{
			get
			{
				AdHocRadarItems2SelectMethod selectMethod = AdHocRadarItems2SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdHocRadarItems2SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdHocRadarItems2DataSourceView class that is to be
		/// used by the AdHocRadarItems2DataSource.
		/// </summary>
		/// <returns>An instance of the AdHocRadarItems2DataSourceView class.</returns>
		protected override BaseDataSourceView<AdHocRadarItems2, AdHocRadarItems2Key> GetNewDataSourceView()
		{
			return new AdHocRadarItems2DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdHocRadarItems2DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdHocRadarItems2DataSourceView : ProviderDataSourceView<AdHocRadarItems2, AdHocRadarItems2Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdHocRadarItems2DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdHocRadarItems2DataSourceView(AdHocRadarItems2DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdHocRadarItems2DataSource AdHocRadarItems2Owner
		{
			get { return Owner as AdHocRadarItems2DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdHocRadarItems2SelectMethod SelectMethod
		{
			get { return AdHocRadarItems2Owner.SelectMethod; }
			set { AdHocRadarItems2Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdHocRadarItems2Service AdHocRadarItems2Provider
		{
			get { return Provider as AdHocRadarItems2Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdHocRadarItems2> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdHocRadarItems2> results = null;
			AdHocRadarItems2 item;
			count = 0;
			
			System.Int32 _adHocRadarItem2Id;
			System.Int32 _year;
			System.Int32 _adHocRadarId;
			System.Int32 _companyId;
			System.Int32 _siteId;

			switch ( SelectMethod )
			{
				case AdHocRadarItems2SelectMethod.Get:
					AdHocRadarItems2Key entityKey  = new AdHocRadarItems2Key();
					entityKey.Load(values);
					item = AdHocRadarItems2Provider.Get(entityKey);
					results = new TList<AdHocRadarItems2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdHocRadarItems2SelectMethod.GetAll:
                    results = AdHocRadarItems2Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdHocRadarItems2SelectMethod.GetPaged:
					results = AdHocRadarItems2Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdHocRadarItems2SelectMethod.Find:
					if ( FilterParameters != null )
						results = AdHocRadarItems2Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdHocRadarItems2Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdHocRadarItems2SelectMethod.GetByAdHocRadarItem2Id:
					_adHocRadarItem2Id = ( values["AdHocRadarItem2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarItem2Id"], typeof(System.Int32)) : (int)0;
					item = AdHocRadarItems2Provider.GetByAdHocRadarItem2Id(_adHocRadarItem2Id);
					results = new TList<AdHocRadarItems2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdHocRadarItems2SelectMethod.GetByYearAdHocRadarIdCompanyIdSiteId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = AdHocRadarItems2Provider.GetByYearAdHocRadarIdCompanyIdSiteId(_year, _adHocRadarId, _companyId, _siteId);
					results = new TList<AdHocRadarItems2>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdHocRadarItems2SelectMethod.GetByYearAdHocRadarId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetByYearAdHocRadarId(_year, _adHocRadarId, this.StartIndex, this.PageSize, out count);
					break;
				case AdHocRadarItems2SelectMethod.GetByYearAdHocRadarIdCompanyId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetByYearAdHocRadarIdCompanyId(_year, _adHocRadarId, _companyId, this.StartIndex, this.PageSize, out count);
					break;
				case AdHocRadarItems2SelectMethod.GetByYearAdHocRadarIdSiteId:
					_year = ( values["Year"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Year"], typeof(System.Int32)) : (int)0;
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetByYearAdHocRadarIdSiteId(_year, _adHocRadarId, _siteId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case AdHocRadarItems2SelectMethod.GetByAdHocRadarId:
					_adHocRadarId = ( values["AdHocRadarId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdHocRadarId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetByAdHocRadarId(_adHocRadarId, this.StartIndex, this.PageSize, out count);
					break;
				case AdHocRadarItems2SelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case AdHocRadarItems2SelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = AdHocRadarItems2Provider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdHocRadarItems2SelectMethod.Get || SelectMethod == AdHocRadarItems2SelectMethod.GetByAdHocRadarItem2Id )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdHocRadarItems2 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdHocRadarItems2Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdHocRadarItems2> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdHocRadarItems2Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdHocRadarItems2DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdHocRadarItems2DataSource class.
	/// </summary>
	public class AdHocRadarItems2DataSourceDesigner : ProviderDataSourceDesigner<AdHocRadarItems2, AdHocRadarItems2Key>
	{
		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2DataSourceDesigner class.
		/// </summary>
		public AdHocRadarItems2DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarItems2SelectMethod SelectMethod
		{
			get { return ((AdHocRadarItems2DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdHocRadarItems2DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdHocRadarItems2DataSourceActionList

	/// <summary>
	/// Supports the AdHocRadarItems2DataSourceDesigner class.
	/// </summary>
	internal class AdHocRadarItems2DataSourceActionList : DesignerActionList
	{
		private AdHocRadarItems2DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdHocRadarItems2DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdHocRadarItems2DataSourceActionList(AdHocRadarItems2DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdHocRadarItems2SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdHocRadarItems2DataSourceActionList
	
	#endregion AdHocRadarItems2DataSourceDesigner
	
	#region AdHocRadarItems2SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdHocRadarItems2DataSource.SelectMethod property.
	/// </summary>
	public enum AdHocRadarItems2SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdHocRadarItem2Id method.
		/// </summary>
		GetByAdHocRadarItem2Id,
		/// <summary>
		/// Represents the GetByYearAdHocRadarIdCompanyIdSiteId method.
		/// </summary>
		GetByYearAdHocRadarIdCompanyIdSiteId,
		/// <summary>
		/// Represents the GetByYearAdHocRadarId method.
		/// </summary>
		GetByYearAdHocRadarId,
		/// <summary>
		/// Represents the GetByYearAdHocRadarIdCompanyId method.
		/// </summary>
		GetByYearAdHocRadarIdCompanyId,
		/// <summary>
		/// Represents the GetByYearAdHocRadarIdSiteId method.
		/// </summary>
		GetByYearAdHocRadarIdSiteId,
		/// <summary>
		/// Represents the GetByAdHocRadarId method.
		/// </summary>
		GetByAdHocRadarId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId
	}
	
	#endregion AdHocRadarItems2SelectMethod

	#region AdHocRadarItems2Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2Filter : SqlFilter<AdHocRadarItems2Column>
	{
	}
	
	#endregion AdHocRadarItems2Filter

	#region AdHocRadarItems2ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2ExpressionBuilder : SqlExpressionBuilder<AdHocRadarItems2Column>
	{
	}
	
	#endregion AdHocRadarItems2ExpressionBuilder	

	#region AdHocRadarItems2Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdHocRadarItems2ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdHocRadarItems2"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdHocRadarItems2Property : ChildEntityProperty<AdHocRadarItems2ChildEntityTypes>
	{
	}
	
	#endregion AdHocRadarItems2Property
}

