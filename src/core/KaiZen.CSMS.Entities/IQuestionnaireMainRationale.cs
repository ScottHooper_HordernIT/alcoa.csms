﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'QuestionnaireMainRationale' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IQuestionnaireMainRationale 
	{
		/// <summary>			
		/// QuestionnaireMainRationaleId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "QuestionnaireMainRationale"</remarks>
		System.Int32 QuestionnaireMainRationaleId { get; set; }
				
		
		
		/// <summary>
		/// QuestionNo : 
		/// </summary>
		System.String  QuestionNo  { get; set; }
		
		/// <summary>
		/// Rationale : 
		/// </summary>
		System.Byte[]  Rationale  { get; set; }
		
		/// <summary>
		/// Assistance : 
		/// </summary>
		System.Byte[]  Assistance  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


