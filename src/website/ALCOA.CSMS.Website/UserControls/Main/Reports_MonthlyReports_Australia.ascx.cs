using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.Library;

using DevExpress.XtraCharts;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using DevExpress.XtraPrinting;

public partial class UserControls_Reports_MonthlyReports_Australia : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    decimal manhrsYTD, manhrsC;
    decimal lwdYTD, lwdC;
    decimal lwdiYTD, lwdiC;
    decimal rstYTD, rstC;
    int mtYTD, mtC;
    int faYTD, faC;
    int ifeYTD, ifeC;

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!Page.IsPostBack)
        {
            int YearMinus3 = DateTime.Now.Year - 3;
            int YearMinus2 = DateTime.Now.Year - 2;
            int YearMinus1 = DateTime.Now.Year - 1;
            int YearMinus0 = DateTime.Now.Year;

            cbYear.Items.Clear();
            //cbYear.Items.Add(YearMinus3.ToString(), YearMinus3);
            //cbYear.Items.Add(YearMinus2.ToString(), YearMinus2);
            //cbYear.Items.Add(YearMinus1.ToString(), YearMinus1);
            //cbYear.Items.Add(YearMinus0.ToString(), YearMinus0);

            //Added for ChangeItem#29
            int Current_year = DateTime.Now.Year;
            while (Current_year >= 2007)
            {
                cbYear.Items.Add(Current_year.ToString(), Convert.ToInt32(Current_year));
                Current_year--;
            } //End
            cbYear.Value = DateTime.Now.Year;

            cbMonth.Value = DateTime.Now.Month;
            if (DateTime.Now.Month >= 1) { cbMonth.Value = ((DateTime.Now).Month - 1); };
            if (DateTime.Now.Month == 1) { cbMonth.Value = 12; cbYear.Value = YearMinus1; };

            //cbMonth.SelectedIndex = (int)cbMonth.Value - 1;

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                default:
                    Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    break;
            }

            Grid_DataBind();
            GetMonthNames((int)cbMonth.Value, (int)cbYear.Value);

            try
            {
                if (Request.QueryString["ID"] != null)
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["ID"]))
                    {
                        LoadLayout(Convert.ToInt32(Request.QueryString["ID"]));
                    }
                }
            }
            catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); }
        }

        this.grid.CustomSummaryCalculate += new DevExpress.Data.CustomSummaryEventHandler(grid_CustomSummaryCalculate);
        ASPxGridView.RegisterBaseScript(this.Page);
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        Grid_DataBind();
        GetMonthNames((int)cbMonth.Value, (int)cbYear.Value);
    }

    protected void Grid_DataBind()
    {
        SessionHandler.spVar_MonthPrevious = (Convert.ToInt32(cbMonth.Value) - 1).ToString();
        SessionHandler.spVar_Month = cbMonth.Value.ToString();
        SessionHandler.spVar_Year = cbYear.Value.ToString();

        // Export
        String exportFileName = @"ALCOA CSMS - Monthly Management Summary"; //Chrome & IE is fine.
        String userAgent = Request.Headers.Get("User-Agent");
        if (
            (userAgent.Contains("Mozilla") && !userAgent.Contains("AppleWebKit") && !userAgent.Contains("MSIE")) || //Mozilla
            (userAgent.Contains("AppleWebKit") && !userAgent.Contains("Chrome"))      //Safari
            )
        {
            exportFileName = exportFileName.Replace(" ", "_");
        }
        Helper.ExportGrid.Settings(ucExportButtons, exportFileName);
    }

    protected void Generate_TotalSummary()
    {
        ASPxSummaryItem totalSummary = new ASPxSummaryItem();
        totalSummary.FieldName = "YTD TRIFR";
        totalSummary.ShowInColumn = "YTD TRIFR";
        totalSummary.DisplayFormat = "{0}";
        totalSummary.SummaryType = DevExpress.Data.SummaryItemType.Custom;
        grid.GetTotalSummaryValue(totalSummary);
        grid.TotalSummary.Add(totalSummary);
    }

    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "SiteId" || e.Column.FieldName == "CompanyName" || e.Column.FieldName == "CompanySiteCategoryId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }
    protected void grid_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Start)
        {
            reset();
        }
        if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Calculate)
        {

            manhrsYTD += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("MANHOURS YTD"));
            lwdYTD += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("YTD LWD"));
            lwdiYTD += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("YTD LWDi"));
            rstYTD += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("YTD RST"));
            mtYTD += NumberUtilities.Convert.parseObjectToInt(e.GetValue("YTD MT"));
            faYTD += NumberUtilities.Convert.parseObjectToInt(e.GetValue("YTD FA"));
            ifeYTD += NumberUtilities.Convert.parseObjectToInt(e.GetValue("YTD IFE"));

            manhrsC += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("TOTAL MANHOURS CURRENT MONTH"));
            lwdC += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("Current LWD"));
            lwdC += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("Current LWDi"));
            rstC += NumberUtilities.Convert.parseObjectToDecimal(e.GetValue("Current RST"));
            mtC += NumberUtilities.Convert.parseObjectToInt(e.GetValue("Current MT"));
            faC += NumberUtilities.Convert.parseObjectToInt(e.GetValue("Current FA"));
            ifeC += NumberUtilities.Convert.parseObjectToInt(e.GetValue("Current IFE"));
        }
        if (e.SummaryProcess == DevExpress.Data.CustomSummaryProcess.Finalize)
        {
            ASPxSummaryItem test = (ASPxSummaryItem)e.Item;

            e.TotalValue = 0;
            switch (test.FieldName)
            {
                case "YTD TRIFR":
                    if (manhrsYTD > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiYTD + rstYTD + mtYTD) * 200000) / manhrsYTD), 2);

                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "YTD LWDIFR":
                    if (manhrsYTD > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiYTD) * 200000) / manhrsYTD), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "YTD AIFR":
                    if (manhrsYTD > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiYTD + rstYTD + mtYTD + faYTD) * 200000) / manhrsYTD), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "YTD IFEFR":
                    if (manhrsYTD > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round(((ifeYTD * 200000) / manhrsYTD), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "YTD IFE/injury ytd":
                    decimal temp = lwdiYTD + rstYTD + mtYTD + faYTD;
                    if (temp > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((ifeYTD / (temp)), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "Current TRIFR":
                    if (manhrsC > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiC + rstC + mtC) * 200000) / manhrsC), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "Current LWDIFR":
                    if (manhrsC > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiC) * 200000) / manhrsC), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "Current AIFR":
                    if (manhrsC > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round((((lwdiC + rstC + mtC + faC) * 200000) / manhrsC), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                case "Current IFEFR":
                    if (manhrsC > 0)
                    {
                        //if (!e.IsGroupSummary)
                        //{
                        e.TotalValue = Decimal.Round(((ifeC * 200000) / manhrsC), 2);
                        //}
                    }
                    else
                    {
                        e.TotalValue = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    protected void reset()
    {
        manhrsYTD = 0;
        lwdYTD = 0;
        lwdiYTD = 0;
        rstYTD = 0;
        mtYTD = 0;
        faYTD = 0;
        ifeYTD = 0;
        manhrsC = 0;
        lwdC = 0;
        lwdiC = 0;
        rstC = 0;
        mtC = 0;
        faC = 0;
        ifeC = 0;
    }

    protected void GetMonthNames(int month, int year)
    {
        DateTime currentMonth = new DateTime(year, month, 1);
        grid.Columns["CurrentMonth"].Caption = String.Format("Total ManHours ({0:MMMM})", currentMonth);

        grid.Columns["Current TRIFR"].Caption = String.Format("TRIFR ({0:MMMM})", currentMonth);
        grid.Columns["Current LWDIFR"].Caption = String.Format("LWDIFR ({0:MMMM})", currentMonth);
        grid.Columns["Current AIFR"].Caption = String.Format("AIFR ({0:MMMM})", currentMonth);
        grid.Columns["Current IFEFR"].Caption = String.Format("IFEFR ({0:MMMM})", currentMonth);


        DateTime previousMonth;
        if (month <= 1)
        {
            previousMonth = new DateTime(year - 1, 12, 1);
            grid.Columns["PreviousMonth"].Visible = false;
            grid.Columns["PreviousMonth"].Caption = String.Format("Total ManHours ({0:MMMM})", previousMonth);
        }
        else
        {
            previousMonth = new DateTime(year, month - 1, 1);
            //grid.Columns["PreviousMonth"].Visible = true;
            grid.Columns["PreviousMonth"].Caption = String.Format("Total ManHours ({0:MMMM})", previousMonth);
        }
    }

    protected void grid2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        e.NewValues["Area"] = "MMSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        e.NewValues["ReportDescription"] = "";
    }
    protected void grid2_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        e.NewValues["Area"] = "MMSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        e.NewValues["ReportDescription"] = "";
    }
    protected void grid2_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        e.NewValues["Area"] = "MMSA";
        e.NewValues["ModifiedByUserId"] = auth.UserId;
        e.NewValues["ModifiedDate"] = DateTime.Now;
        e.NewValues["ReportLayout"] = grid.SaveClientLayout();
        e.NewValues["ReportDescription"] = "";
    }
    protected void grid2_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        //REQUIRED FIELDS (EVERYTHING ELSE IS DEFAULT TO 0)
        if (e.NewValues["ReportName"] == null) { e.Errors[grid2.Columns["ReportName"]] = "Required Field."; }

        int temp = 0;

        if (e.Errors.Count > 0) e.RowError = "Report Name can not be empty.";
    }
    protected void grid2_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        if (!grid2.IsNewRowEditing)
        {
            grid2.DoRowValidation();
        }
    }
    protected void grid2_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;

        int ReportId = (int)grid2.GetRowValues(e.VisibleIndex, "ReportId");
        ASPxHyperLink hl = grid2.FindRowCellTemplateControl(e.VisibleIndex, null, "hlLoad") as ASPxHyperLink;
        if (hl != null)
        {
            hl.NavigateUrl = "../../Reports_MonthlyReports_Australia.aspx" + QueryStringModule.Encrypt("ID=" + ReportId.ToString());
        }
    }

    protected void LoadLayout(int ReportId)
    {
        CustomReportingLayoutsService crlService = new CustomReportingLayoutsService();
        TList<CustomReportingLayouts> crlList = crlService.GetAll();
        TList<CustomReportingLayouts> crlList2 = crlList.FindAll(
            delegate(CustomReportingLayouts c2)
            {
                return
                    c2.ReportId == ReportId;
            }
        );
        if (crlList2.Count != 0)
        {
            foreach (CustomReportingLayouts crl in crlList2)
            {
                grid.LoadClientLayout(crl.ReportLayout);
                lblLayout.Text = crl.ReportName;
            }
        }
        else
        {
            lblLayout.Text = "(Default) | Selected Layout could not be loaded.";
        }
    }
}
