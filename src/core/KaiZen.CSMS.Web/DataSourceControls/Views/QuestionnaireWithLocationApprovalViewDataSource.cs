﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireWithLocationApprovalViewProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireWithLocationApprovalViewDataSourceDesigner))]
	public class QuestionnaireWithLocationApprovalViewDataSource : ReadOnlyDataSource<QuestionnaireWithLocationApprovalView>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewDataSource class.
		/// </summary>
		public QuestionnaireWithLocationApprovalViewDataSource() : base(new QuestionnaireWithLocationApprovalViewService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireWithLocationApprovalViewDataSourceView used by the QuestionnaireWithLocationApprovalViewDataSource.
		/// </summary>
		protected QuestionnaireWithLocationApprovalViewDataSourceView QuestionnaireWithLocationApprovalViewView
		{
			get { return ( View as QuestionnaireWithLocationApprovalViewDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireWithLocationApprovalViewDataSourceView class that is to be
		/// used by the QuestionnaireWithLocationApprovalViewDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireWithLocationApprovalViewDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireWithLocationApprovalView, Object> GetNewDataSourceView()
		{
			return new QuestionnaireWithLocationApprovalViewDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireWithLocationApprovalViewDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireWithLocationApprovalViewDataSourceView : ReadOnlyDataSourceView<QuestionnaireWithLocationApprovalView>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireWithLocationApprovalViewDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireWithLocationApprovalViewDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireWithLocationApprovalViewDataSourceView(QuestionnaireWithLocationApprovalViewDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireWithLocationApprovalViewDataSource QuestionnaireWithLocationApprovalViewOwner
		{
			get { return Owner as QuestionnaireWithLocationApprovalViewDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireWithLocationApprovalViewService QuestionnaireWithLocationApprovalViewProvider
		{
			get { return Provider as QuestionnaireWithLocationApprovalViewService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireWithLocationApprovalViewDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireWithLocationApprovalViewDataSource class.
	/// </summary>
	public class QuestionnaireWithLocationApprovalViewDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireWithLocationApprovalView>
	{
	}

	#endregion QuestionnaireWithLocationApprovalViewDataSourceDesigner

	#region QuestionnaireWithLocationApprovalViewFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewFilter : SqlFilter<QuestionnaireWithLocationApprovalViewColumn>
	{
	}

	#endregion QuestionnaireWithLocationApprovalViewFilter

	#region QuestionnaireWithLocationApprovalViewExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireWithLocationApprovalView"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireWithLocationApprovalViewExpressionBuilder : SqlExpressionBuilder<QuestionnaireWithLocationApprovalViewColumn>
	{
	}
	
	#endregion QuestionnaireWithLocationApprovalViewExpressionBuilder		
}

