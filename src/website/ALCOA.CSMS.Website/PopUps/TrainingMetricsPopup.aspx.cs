﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Drawing;
using DevExpress.Data.Filtering;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class TrainingMetricsPopup : System.Web.UI.Page
    {        
        string filteredText = "";        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            {
                Session["ShowAll"] = false;
                int companyId = Convert.ToInt32(Request.QueryString["C"]);
                int siteId = Convert.ToInt32(Request.QueryString["S"]);
                //int companyId = 59;
                //int siteId = 2;
                SqlDataSourceTrainingManagementValidity.SelectParameters["CompanyId"].DefaultValue = companyId.ToString();
                SqlDataSourceTrainingManagementValidity.SelectParameters["SiteId"].DefaultValue = siteId.ToString();
                //grid.DataSourceID = "SqlDataSourceTrainingManagementValidity";
                //grid.DataBind();

                CompaniesService comSer=new CompaniesService();
                Companies comp = comSer.GetByCompanyId(companyId);
                if (comp != null)
                {
                    lblCompanyName.Text = comp.CompanyName;                       
                }

                SitesService siteSer = new SitesService();
                Sites site = siteSer.GetBySiteId(siteId);
                if (site != null)
                {
                    lblSiteName.Text = site.SiteName;
                }
                Helper.ExportGrid.Settings(ucExportButtons2, "People Management - Training - Validity - " + lblCompanyName.Text + " - " + lblSiteName.Text, "grid");

                Session["CompanyFilter"] = Convert.ToString(Request.QueryString["C"]);
                Session["SiteFilter"] = Convert.ToString(Request.QueryString["S"]);
                
            }
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            if (e.RowType == GridViewRowType.Data)
            {
                string strExpires = Convert.ToString(grid.GetRowValues(e.VisibleIndex, "ExpiresIn"));
                ASPxLabel lblExpiresIn = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblExpiresIn") as ASPxLabel;
                if (strExpires == "-")
                {
                    lblExpiresIn.Text = strExpires;
                }
                else if (Convert.ToInt32(strExpires) < 0)
                {
                    lblExpiresIn.ForeColor = Color.Red;
                    lblExpiresIn.Text = "Expired " + (-1*Convert.ToInt32(strExpires)).ToString() + " Days ago";
                }
                else if (Convert.ToInt32(strExpires) > 0)
                {
                    lblExpiresIn.Text = "Expiring in " + strExpires + " Days";
                    if (Convert.ToInt32(strExpires) > 60)
                    {
                        lblExpiresIn.ForeColor = Color.Black;
                    }
                    else
                    {
                        lblExpiresIn.ForeColor = Color.Red;
                    }
                }
            }
        }

        

        protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
        {
            if (e.Column.FieldName != "ExpiresIn") return;
            e.EditorProperties = new ComboBoxProperties();
        }
        protected void grid_AutoFilterCellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName != "ExpiresIn") return;

            ASPxComboBox combo = (ASPxComboBox)e.Editor;
            combo.ValueType = typeof(string);
            combo.Items.Add("Expired", "Expired");
            combo.Items.Add(">60", ">60");
            combo.Items.Add("<=60", "<=60");
            combo.Items.Add("n/a", "n/a");
        }
        protected void grid_ProcessColumnAutoFilter(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAutoFilterEventArgs e)
        {
            int filterCount = 0;
            string whereCondition = string.Empty;

            if (e.Column.FieldName == "ExpiresIn")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    //string[] param = e.Value.ToString().Split('$');
                    if (e.Value.ToString() == "<=60")
                    {
                        e.Criteria = new OperandProperty("ExpiresIn") <= 60;
                        Session["ExpiresInVal"] = "<=60";
                        Session["filterText"] = "<=60";

                    }
                    else if (e.Value.ToString() == ">60")
                    {
                        e.Criteria = new OperandProperty("ExpiresIn") > 60;
                        Session["ExpiresInVal"] = ">60";
                        // e.Value = ">60";
                        Session["filterText"] = ">60";
                    }
                    else if (e.Value.ToString() == "n/a")
                    {
                        e.Criteria = new OperandProperty("ExpiresIn") == 0;
                        Session["ExpiresInVal"] = "n/a";
                        // e.Value = ">60";
                        Session["filterText"] = "n/a";
                    }
                    else if (e.Value.ToString() == "Expired")
                    {
                        e.Criteria = new OperandProperty("ExpiresIn") == 1;
                        Session["ExpiresInVal"] = "Expired";
                        Session["filterText"] = "Expired";
                    }

                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;

                }
                else
                {
                    e.Value = Session["filterText"].ToString();
                }
            }

            

            if (e.Column.FieldName == "COURSE_NAME")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("COURSE_NAME") == e.Value.ToString();
                    Session["COURSE_NAMEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "FULL_NAME")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("FULL_NAME") == e.Value.ToString();
                    Session["FULL_NAMEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "COURSE_END_DATE")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("COURSE_END_DATE") == e.Value.ToString();
                    Session["COURSE_END_DATEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                }
            }
            if (e.Column.FieldName == "EXPIRATION_DATE")
            {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                {
                    e.Criteria = new OperandProperty("EXPIRATION_DATE") == e.Value.ToString();
                    Session["EXPIRATION_DATEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                }
            }
            

        }


        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            if (e.CallbackName == "APPLYFILTER")
            {
                Session["CompanyIdVal"] = null;
                Session["SiteIdVal"] = null;
                Session["COURSE_NAMEVal"] = null;
                Session["FULL_NAMEVal"] = null;
                Session["COURSE_END_DATEVal"] = null;
                Session["EXPIRATION_DATEVal"] = null;
                Session["ExpiresInVal"] = null;
                Session["ExpiredVal"] = null;

                grid.DataBind();
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session["ShowAll"] != null)
            {
                if (Convert.ToBoolean(Session["ShowAll"]) == false)
                {
                    btnShowAll.Text = "Show Pager";
                    Session["ShowAll"] = true;
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;

                    grid.DataBind();
                }
                else
                {
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                    int companyId = Convert.ToInt32(Request.QueryString["C"]);
                    int siteId = Convert.ToInt32(Request.QueryString["S"]);
                    string redirectedUrl = "TrainingMetricsPopup.aspx?C=" + companyId.ToString() + "&S=" + siteId.ToString();
                    Response.Redirect(redirectedUrl, true);
                }
            }

        }

        protected void grid_CustomFilterExpressionDisplayText(object sender, CustomFilterExpressionDisplayTextEventArgs e)
        {
            if (e.FilterExpression.Contains("[ExpiresIn] = 0") || e.FilterExpression.Contains("[ExpiresIn] = 'n/a'"))
            {
                e.DisplayText = e.DisplayText.Replace("0", "n/a");
            }
            if (e.FilterExpression.Contains("[ExpiresIn] = 1") || e.FilterExpression.Contains("[ExpiresIn] = 'Expired'"))
            {
                e.DisplayText = e.DisplayText.Replace("1", "Expired");
            }
        }
    }
}