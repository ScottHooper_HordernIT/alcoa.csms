﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace ALCOA.CSMS.Website.App_Start
{
    public class BundlingConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Script bundles

            bundles.Add(new ScriptBundle("~/bundles/jquery_scripts")
                .Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/user_master_scripts")
                .Include("~/Scripts/main.js")
                //.Include("~/Common/webkit.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/countdown_timer_scripts")
                .Include("~/Common/countdownTimer.js"));

            #endregion

            #region Style bundles

            bundles.Add(new StyleBundle("~/bundles/user_master_styles")
                .Include("~/App_Themes/Content/default.css"));

            bundles.Add(new StyleBundle("~/bundles/Office2003BlueOld_styles")
                .Include("~/App_Themes/Office2003BlueOld/Web/styles.css")
                .Include("~/App_Themes/Office2003BlueOld/GridView/styles.css")
                .Include("~/App_Themes/Office2003BlueOld/Editors/styles.css"));

            #endregion

            // This overrides web.config debug setting.  True means bundle/minify, False means don't bundle/minify
            //BundleTable.EnableOptimizations = true;
        }
    }
}