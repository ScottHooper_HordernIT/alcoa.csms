﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailRecipientsListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(AdminTaskEmailRecipientsListDataSourceDesigner))]
	public class AdminTaskEmailRecipientsListDataSource : ReadOnlyDataSource<AdminTaskEmailRecipientsList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListDataSource class.
		/// </summary>
		public AdminTaskEmailRecipientsListDataSource() : base(new AdminTaskEmailRecipientsListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailRecipientsListDataSourceView used by the AdminTaskEmailRecipientsListDataSource.
		/// </summary>
		protected AdminTaskEmailRecipientsListDataSourceView AdminTaskEmailRecipientsListView
		{
			get { return ( View as AdminTaskEmailRecipientsListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailRecipientsListDataSourceView class that is to be
		/// used by the AdminTaskEmailRecipientsListDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailRecipientsListDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailRecipientsList, Object> GetNewDataSourceView()
		{
			return new AdminTaskEmailRecipientsListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailRecipientsListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailRecipientsListDataSourceView : ReadOnlyDataSourceView<AdminTaskEmailRecipientsList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientsListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailRecipientsListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailRecipientsListDataSourceView(AdminTaskEmailRecipientsListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailRecipientsListDataSource AdminTaskEmailRecipientsListOwner
		{
			get { return Owner as AdminTaskEmailRecipientsListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailRecipientsListService AdminTaskEmailRecipientsListProvider
		{
			get { return Provider as AdminTaskEmailRecipientsListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region AdminTaskEmailRecipientsListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailRecipientsListDataSource class.
	/// </summary>
	public class AdminTaskEmailRecipientsListDataSourceDesigner : ReadOnlyDataSourceDesigner<AdminTaskEmailRecipientsList>
	{
	}

	#endregion AdminTaskEmailRecipientsListDataSourceDesigner

	#region AdminTaskEmailRecipientsListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListFilter : SqlFilter<AdminTaskEmailRecipientsListColumn>
	{
	}

	#endregion AdminTaskEmailRecipientsListFilter

	#region AdminTaskEmailRecipientsListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipientsList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientsListExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailRecipientsListColumn>
	{
	}
	
	#endregion AdminTaskEmailRecipientsListExpressionBuilder		
}

