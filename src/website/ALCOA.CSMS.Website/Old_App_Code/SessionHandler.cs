using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SessionHandler
/// </summary>    // Static / shared class for handling session variables
public static class SessionHandler
{

    // Declare a string variable to hold the key name of the session variable
    // and use this string variable instead of typing the key name
    // in order to avoid spelling mistakes
    private static string _LoggedIn = "LoggedIn";
    private static string _UserId = "UserId";
    private static string _UserLogon = "UserLogon";
    private static string _UserLogon_UserEmail = "UserLogon_UserEmail";
    private static string _CompanyId = "CompanyId";
    private static string _AccessLogged = "AccessLogged"; //Have we written into logs that the user has logged in?
    private static string _IsPrivledged = "IsPrivledged";

    private static string _KPIState = "KPIState";

    private static string _ViewMode = "ViewMode";
    private static string _TemplateSelected = "TemplateSelected";

    private static string _spVar_KpiId = "spVar_KpiId";
    private static string _spVar_CategoryId = "spVar_CategoryId";
    private static string _spVar_CategoryId2 = "spVar_CategoryId2";
    private static string _spVar_CompanyId = "spVar_CompanyId";
    private static string _spVar_RegionId = "spVar_RegionId";
    private static string _spVar_SiteId = "spVar_SiteId";
    private static string _spVar_SiteName = "spVar_SiteName";
    private static string _spVar_UserId = "spVar_UserId";
    private static string _spVar_QtrId = "spVar_QtrId";
    private static string _spVar_Year = "spVar_Year";
    private static string _spVar_Month = "spVar_Month";
    private static string _spVar_Day = "spVar_Day";
    private static string _spVar_MonthPrevious = "spVar_MonthPrevious";
    private static string _spVar_DateFrom = "spVar_DateFrom";
    private static string _spVar_DateTo = "spVar_DateTo";
    private static string _spVar_Page = "spVar_Page";
    private static string _spVar_ComplianceCount = "spVar_ComplianceCount";
    private static string _spVar_MT = "spVar_MT";
    private static string _spVar_SafetyPlans_ResponseId = "spVar_SafetyPlans_ResponseId";
    private static string _spVar_SafetyPlans_QuestionnaireId = "spVar_SafetyPlans_QuestionnaireId";
    private static string _spVar_Questionnaire_Category = "spVar_Questionnaire_Category";
    private static string _spVar_Questionnaire_Category2 = "spVar_Questionnaire_Category2";
    private static string _spVar_Questionnaire_FrontPageLink = "spVar_Questionnaire_FrontPageLink";
    private static string _spVar_QuestionnaireId = "spVar_QuestionnaireId";
    private static string _spVar_RadarInternal = "spVar_RadarInternal";
    private static string _spVar_MonthId = "spVar_MonthId";
    private static string _Draw = "Draw";

    private static string _spVar_TopaState = "spVar_TopaState";
    private static string _spVar_TopaId = "spVar_TopaId";
    private static string _spVar_TopaId_01 = "spVar_TopaId_01";
    private static string _spVar_TopaId_02 = "spVar_TopaId_02";
    private static string _spVar_TopaId_03 = "spVar_TopaId_03";
    private static string _spVar_TopaId_04 = "spVar_TopaId_04";
    private static string _spVar_TopaId_05 = "spVar_TopaId_05";
    private static string _spVar_TopaId_06 = "spVar_TopaId_06";
    private static string _spVar_TopaId_07 = "spVar_TopaId_07";
    private static string _spVar_TopaId_08 = "spVar_TopaId_08";
    private static string _spVar_TopaId_09 = "spVar_TopaId_09";
    private static string _spVar_TopaId_10 = "spVar_TopaId_10";
    private static string _spVar_TopaId_11 = "spVar_TopaId_11";
    private static string _spVar_TopaId_12 = "spVar_TopaId_12";
    private static string _spVar_TopaId_13 = "spVar_TopaId_13";
    private static string _spVar_TopaId_14 = "spVar_TopaId_14";
    private static string _spVar_TopaId_15 = "spVar_TopaId_15";
    private static string _spVar_TopaId_16 = "spVar_TopaId_16";
    private static string _spVar_TopaId_17 = "spVar_TopaId_17";
    private static string _spVar_TopaId_18 = "spVar_TopaId_18";
    private static string _spVar_TopaId_19 = "spVar_TopaId_19";
    private static string _spVar_TopaId_20 = "spVar_TopaId_20";
    private static string _spVar_TopaId_21 = "spVar_TopaId_21";

    // Declare a static / shared strongly typed property to expose session variable
    public static string LoggedIn
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._LoggedIn] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._LoggedIn].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._LoggedIn] = value; }
    }
    public static string UserId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._UserId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._UserId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._UserId] = value; }
    }
    public static string UserLogon
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._UserLogon] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._UserLogon].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._UserLogon] = value; }
    }
    public static string UserLogon_UserEmail
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._UserLogon_UserEmail] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._UserLogon_UserEmail].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._UserLogon_UserEmail] = value; }
    }
    public static string CompanyId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._CompanyId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._CompanyId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._CompanyId] = value; }
    }
    public static string AccessLogged
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._AccessLogged] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._AccessLogged].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._AccessLogged] = value; }
    }
    public static string IsPrivledged
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._IsPrivledged] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._IsPrivledged].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._IsPrivledged] = value; }
    }

    public static string KPIState
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._KPIState] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._KPIState].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._KPIState] = value; }
    }

    public static string ViewMode
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._ViewMode] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._ViewMode].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._ViewMode] = value; }
    }
    public static string TemplateSelected
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._TemplateSelected] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._TemplateSelected].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._TemplateSelected] = value; }
    }

    public static string spVar_KpiId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_KpiId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_KpiId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_KpiId] = value; }
    }
    public static string spVar_CategoryId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_CategoryId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_CategoryId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_CategoryId] = value; }
    }
    public static string spVar_CategoryId2
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_CategoryId2] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_CategoryId2].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_CategoryId2] = value; }
    }
    public static string spVar_CompanyId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_CompanyId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_CompanyId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_CompanyId] = value; }
    }
    public static string spVar_UserId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_UserId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_UserId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_UserId] = value; }
    }
    public static string spVar_RegionId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_RegionId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_RegionId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_RegionId] = value; }
    }
    public static string spVar_SiteId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_SiteId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_SiteId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_SiteId] = value; }
    }
    public static string spVar_SiteName
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_SiteName] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_SiteName].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_SiteName] = value; }
    }
    public static string spVar_QtrId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_QtrId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_QtrId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_QtrId] = value; }
    }
    public static string spVar_Year
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Year] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Year].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Year] = value; }
    }
    public static string spVar_Month
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Month] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Month].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Month] = value; }
    }
    public static string spVar_Day
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Day] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Day].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Day] = value; }
    }
    public static string spVar_MonthPrevious
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_MonthPrevious] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_MonthPrevious].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_MonthPrevious] = value; }
    }
    public static string spVar_DateFrom
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_DateFrom] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_DateFrom].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_DateFrom] = value; }
    }
    public static string spVar_DateTo
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_DateTo] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_DateTo].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_DateTo] = value; }
    }
    public static string spVar_Page
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Page] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Page].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Page] = value; }
    }
    public static string spVar_ComplianceCount
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_ComplianceCount] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_ComplianceCount].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_ComplianceCount] = value; }
    }
    public static string spVar_MT
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_MT] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_MT].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_MT] = value; }
    }
    public static string spVar_SafetyPlans_ResponseId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_ResponseId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_ResponseId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_ResponseId] = value; }
    }
    public static string spVar_SafetyPlans_QuestionnaireId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_QuestionnaireId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_QuestionnaireId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_SafetyPlans_QuestionnaireId] = value; }
    }

    public static string spVar_Questionnaire_Category
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category] = value; }
    }
    public static string spVar_Questionnaire_Category2
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category2] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category2].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_Category2] = value; }
    }
    public static string spVar_Questionnaire_FrontPageLink
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_FrontPageLink] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_FrontPageLink].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_Questionnaire_FrontPageLink] = value; }
    }
    public static string spVar_QuestionnaireId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_QuestionnaireId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_QuestionnaireId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_QuestionnaireId] = value; }
    }
    public static string spVar_RadarInternal
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_RadarInternal] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_RadarInternal].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_RadarInternal] = value; }
    }
    public static string spVar_MonthId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_MonthId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_MonthId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_MonthId] = value; }
    }
    
    
    public static string Draw
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._Draw] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._Draw].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._Draw] = value; }
    }
    public static string spVar_TopaState
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaState] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaState].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaState] = value; }
    }
    public static string spVar_TopaId
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId] = value; }
    }
    public static string spVar_TopaId_01
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_01] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_01].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_01] = value; }
    }
    public static string spVar_TopaId_02
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_02] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_02].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_02] = value; }
    }
    public static string spVar_TopaId_03
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_03] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_03].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_03] = value; }
    }
    public static string spVar_TopaId_04
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_04] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_04].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_04] = value; }
    }
    public static string spVar_TopaId_05
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_05] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_05].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_05] = value; }
    }
    public static string spVar_TopaId_06
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_06] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_06].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_06] = value; }
    }
    public static string spVar_TopaId_07
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_07] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_07].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_07] = value; }
    }
    public static string spVar_TopaId_08
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_08] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_08].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_08] = value; }
    }
    public static string spVar_TopaId_09
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_09] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_09].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_09] = value; }
    }
    public static string spVar_TopaId_10
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_10] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_10].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_10] = value; }
    }
    public static string spVar_TopaId_11
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_11] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_10].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_11] = value; }
    }
    public static string spVar_TopaId_12
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_12] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_12].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_12] = value; }
    }
    public static string spVar_TopaId_13
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_13] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_13].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_13] = value; }
    }
    public static string spVar_TopaId_14
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_14] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_14].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_14] = value; }
    }
    public static string spVar_TopaId_15
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_15] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_15].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_15] = value; }
    }
    public static string spVar_TopaId_16
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_16] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_16].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_16] = value; }
    }
    public static string spVar_TopaId_17
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_17] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_17].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_17] = value; }
    }
    public static string spVar_TopaId_18
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_18] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_18].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_18] = value; }
    }
    public static string spVar_TopaId_19
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_19] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_19].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_19] = value; }
    }
    public static string spVar_TopaId_20
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_20] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_10].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_20] = value; }
    }
    public static string spVar_TopaId_21
    {
        get
        {
            if (HttpContext.Current.Session[SessionHandler._spVar_TopaId_21] == null) { return string.Empty; }
            else { return HttpContext.Current.Session[SessionHandler._spVar_TopaId_11].ToString(); }
        }
        set { HttpContext.Current.Session[SessionHandler._spVar_TopaId_21] = value; }
    }
 
}