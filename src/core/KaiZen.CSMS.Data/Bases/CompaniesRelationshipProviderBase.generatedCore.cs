﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesRelationshipProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesRelationshipProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompaniesRelationship, KaiZen.CSMS.Entities.CompaniesRelationshipKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesRelationshipKey key)
		{
			return Delete(transactionManager, key.ParentId, key.ChildId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_parentId">. Primary Key.</param>
		/// <param name="_childId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _parentId, System.Int32 _childId)
		{
			return Delete(null, _parentId, _childId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId">. Primary Key.</param>
		/// <param name="_childId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _parentId, System.Int32 _childId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		FK_CompaniesRelationship_ChildId Description: 
		/// </summary>
		/// <param name="_childId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByChildId(System.Int32 _childId)
		{
			int count = -1;
			return GetByChildId(_childId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		FK_CompaniesRelationship_ChildId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesRelationship> GetByChildId(TransactionManager transactionManager, System.Int32 _childId)
		{
			int count = -1;
			return GetByChildId(transactionManager, _childId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		FK_CompaniesRelationship_ChildId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByChildId(TransactionManager transactionManager, System.Int32 _childId, int start, int pageLength)
		{
			int count = -1;
			return GetByChildId(transactionManager, _childId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		fkCompaniesRelationshipChildId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_childId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByChildId(System.Int32 _childId, int start, int pageLength)
		{
			int count =  -1;
			return GetByChildId(null, _childId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		fkCompaniesRelationshipChildId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_childId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByChildId(System.Int32 _childId, int start, int pageLength,out int count)
		{
			return GetByChildId(null, _childId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ChildId key.
		///		FK_CompaniesRelationship_ChildId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public abstract TList<CompaniesRelationship> GetByChildId(TransactionManager transactionManager, System.Int32 _childId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		FK_CompaniesRelationship_ParentID Description: 
		/// </summary>
		/// <param name="_parentId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByParentId(System.Int32 _parentId)
		{
			int count = -1;
			return GetByParentId(_parentId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		FK_CompaniesRelationship_ParentID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesRelationship> GetByParentId(TransactionManager transactionManager, System.Int32 _parentId)
		{
			int count = -1;
			return GetByParentId(transactionManager, _parentId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		FK_CompaniesRelationship_ParentID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByParentId(TransactionManager transactionManager, System.Int32 _parentId, int start, int pageLength)
		{
			int count = -1;
			return GetByParentId(transactionManager, _parentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		fkCompaniesRelationshipParentId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByParentId(System.Int32 _parentId, int start, int pageLength)
		{
			int count =  -1;
			return GetByParentId(null, _parentId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		fkCompaniesRelationshipParentId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByParentId(System.Int32 _parentId, int start, int pageLength,out int count)
		{
			return GetByParentId(null, _parentId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ParentID key.
		///		FK_CompaniesRelationship_ParentID Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public abstract TList<CompaniesRelationship> GetByParentId(TransactionManager transactionManager, System.Int32 _parentId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		FK_CompaniesRelationship_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		FK_CompaniesRelationship_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		/// <remarks></remarks>
		public TList<CompaniesRelationship> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		FK_CompaniesRelationship_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		fkCompaniesRelationshipModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		fkCompaniesRelationshipModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public TList<CompaniesRelationship> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_CompaniesRelationship_ModifiedByUserId key.
		///		FK_CompaniesRelationship_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.CompaniesRelationship objects.</returns>
		public abstract TList<CompaniesRelationship> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompaniesRelationship Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesRelationshipKey key, int start, int pageLength)
		{
			return GetByParentIdChildId(transactionManager, key.ParentId, key.ChildId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompaniesRS index.
		/// </summary>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(System.Int32 _parentId, System.Int32 _childId)
		{
			int count = -1;
			return GetByParentIdChildId(null,_parentId, _childId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesRS index.
		/// </summary>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(System.Int32 _parentId, System.Int32 _childId, int start, int pageLength)
		{
			int count = -1;
			return GetByParentIdChildId(null, _parentId, _childId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesRS index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(TransactionManager transactionManager, System.Int32 _parentId, System.Int32 _childId)
		{
			int count = -1;
			return GetByParentIdChildId(transactionManager, _parentId, _childId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesRS index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(TransactionManager transactionManager, System.Int32 _parentId, System.Int32 _childId, int start, int pageLength)
		{
			int count = -1;
			return GetByParentIdChildId(transactionManager, _parentId, _childId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesRS index.
		/// </summary>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(System.Int32 _parentId, System.Int32 _childId, int start, int pageLength, out int count)
		{
			return GetByParentIdChildId(null, _parentId, _childId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesRS index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentId"></param>
		/// <param name="_childId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesRelationship GetByParentIdChildId(TransactionManager transactionManager, System.Int32 _parentId, System.Int32 _childId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
        #region Company Vendor Data

        public DataSet GetCompaniesByABN(System.String ABN)
        {
            return GetCompaniesByABN(null, 0, int.MaxValue, ABN);
        }
        public abstract DataSet GetCompaniesByABN(TransactionManager transactionManager, int start, int pageLength, System.String ABN);

        #endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompaniesRelationship&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompaniesRelationship&gt;"/></returns>
		public static TList<CompaniesRelationship> Fill(IDataReader reader, TList<CompaniesRelationship> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompaniesRelationship c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompaniesRelationship")
					.Append("|").Append((System.Int32)reader[((int)CompaniesRelationshipColumn.ParentId - 1)])
					.Append("|").Append((System.Int32)reader[((int)CompaniesRelationshipColumn.ChildId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompaniesRelationship>(
					key.ToString(), // EntityTrackingKey
					"CompaniesRelationship",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompaniesRelationship();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ParentId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ParentId - 1)];
					c.OriginalParentId = c.ParentId;
					c.ChildId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ChildId - 1)];
					c.OriginalChildId = c.ChildId;
					c.ModifiedByUserId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ModifiedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompaniesRelationship entity)
		{
			if (!reader.Read()) return;
			
			entity.ParentId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ParentId - 1)];
			entity.OriginalParentId = (System.Int32)reader["ParentId"];
			entity.ChildId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ChildId - 1)];
			entity.OriginalChildId = (System.Int32)reader["ChildId"];
			entity.ModifiedByUserId = (System.Int32)reader[((int)CompaniesRelationshipColumn.ModifiedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompaniesRelationship entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ParentId = (System.Int32)dataRow["ParentId"];
			entity.OriginalParentId = (System.Int32)dataRow["ParentId"];
			entity.ChildId = (System.Int32)dataRow["ChildId"];
			entity.OriginalChildId = (System.Int32)dataRow["ChildId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesRelationship"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesRelationship Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesRelationship entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ChildIdSource	
			if (CanDeepLoad(entity, "Companies|ChildIdSource", deepLoadType, innerList) 
				&& entity.ChildIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ChildId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ChildIdSource = tmpEntity;
				else
					entity.ChildIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.ChildId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ChildIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ChildIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.ChildIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ChildIdSource

			#region ParentIdSource	
			if (CanDeepLoad(entity, "Companies|ParentIdSource", deepLoadType, innerList) 
				&& entity.ParentIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ParentId;
				Companies tmpEntity = EntityManager.LocateEntity<Companies>(EntityLocator.ConstructKeyFromPkItems(typeof(Companies), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentIdSource = tmpEntity;
				else
					entity.ParentIdSource = DataRepository.CompaniesProvider.GetByCompanyId(transactionManager, entity.ParentId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CompaniesProvider.DeepLoad(transactionManager, entity.ParentIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompaniesRelationship object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompaniesRelationship instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesRelationship Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesRelationship entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ChildIdSource
			if (CanDeepSave(entity, "Companies|ChildIdSource", deepSaveType, innerList) 
				&& entity.ChildIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.ChildIdSource);
				entity.ChildId = entity.ChildIdSource.CompanyId;
			}
			#endregion 
			
			#region ParentIdSource
			if (CanDeepSave(entity, "Companies|ParentIdSource", deepSaveType, innerList) 
				&& entity.ParentIdSource != null)
			{
				DataRepository.CompaniesProvider.Save(transactionManager, entity.ParentIdSource);
				entity.ParentId = entity.ParentIdSource.CompanyId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesRelationshipChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompaniesRelationship</c>
	///</summary>
	public enum CompaniesRelationshipChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Companies</c> at ChildIdSource
		///</summary>
		[ChildEntityType(typeof(Companies))]
		Companies,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion CompaniesRelationshipChildEntityTypes
	
	#region CompaniesRelationshipFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesRelationshipColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipFilterBuilder : SqlFilterBuilder<CompaniesRelationshipColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilterBuilder class.
		/// </summary>
		public CompaniesRelationshipFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesRelationshipFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesRelationshipFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesRelationshipFilterBuilder
	
	#region CompaniesRelationshipParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesRelationshipColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesRelationshipParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesRelationshipColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipParameterBuilder class.
		/// </summary>
		public CompaniesRelationshipParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesRelationshipParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesRelationshipParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesRelationshipParameterBuilder
	
	#region CompaniesRelationshipSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesRelationshipColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesRelationship"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesRelationshipSortBuilder : SqlSortBuilder<CompaniesRelationshipColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesRelationshipSqlSortBuilder class.
		/// </summary>
		public CompaniesRelationshipSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesRelationshipSortBuilder
	
} // end namespace
