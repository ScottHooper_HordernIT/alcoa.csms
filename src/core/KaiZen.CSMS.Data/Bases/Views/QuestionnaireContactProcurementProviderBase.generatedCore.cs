﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireContactProcurementProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireContactProcurementProviderBaseCore : EntityViewProviderBase<QuestionnaireContactProcurement>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireContactProcurement&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireContactProcurement&gt;"/></returns>
		protected static VList&lt;QuestionnaireContactProcurement&gt; Fill(DataSet dataSet, VList<QuestionnaireContactProcurement> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireContactProcurement>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireContactProcurement&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireContactProcurement>"/></returns>
		protected static VList&lt;QuestionnaireContactProcurement&gt; Fill(DataTable dataTable, VList<QuestionnaireContactProcurement> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireContactProcurement c = new QuestionnaireContactProcurement();
					c.QuestionnaireId = (Convert.IsDBNull(row["QuestionnaireId"]))?(int)0:(System.Int32)row["QuestionnaireId"];
					c.ProcurementUserId = (Convert.IsDBNull(row["ProcurementUserId"]))?(int)0:(System.Int32?)row["ProcurementUserId"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireContactProcurement&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireContactProcurement&gt;"/></returns>
		protected VList<QuestionnaireContactProcurement> Fill(IDataReader reader, VList<QuestionnaireContactProcurement> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireContactProcurement entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireContactProcurement>("QuestionnaireContactProcurement",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireContactProcurement();
					}
					
					entity.SuppressEntityEvents = true;

					entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireContactProcurementColumn.QuestionnaireId)];
					//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
					entity.ProcurementUserId = (reader.IsDBNull(((int)QuestionnaireContactProcurementColumn.ProcurementUserId)))?null:(System.Int32?)reader[((int)QuestionnaireContactProcurementColumn.ProcurementUserId)];
					//entity.ProcurementUserId = (Convert.IsDBNull(reader["ProcurementUserId"]))?(int)0:(System.Int32?)reader["ProcurementUserId"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireContactProcurement"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireContactProcurement"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireContactProcurement entity)
		{
			reader.Read();
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireContactProcurementColumn.QuestionnaireId)];
			//entity.QuestionnaireId = (Convert.IsDBNull(reader["QuestionnaireId"]))?(int)0:(System.Int32)reader["QuestionnaireId"];
			entity.ProcurementUserId = (reader.IsDBNull(((int)QuestionnaireContactProcurementColumn.ProcurementUserId)))?null:(System.Int32?)reader[((int)QuestionnaireContactProcurementColumn.ProcurementUserId)];
			//entity.ProcurementUserId = (Convert.IsDBNull(reader["ProcurementUserId"]))?(int)0:(System.Int32?)reader["ProcurementUserId"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireContactProcurement"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireContactProcurement"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireContactProcurement entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireId = (Convert.IsDBNull(dataRow["QuestionnaireId"]))?(int)0:(System.Int32)dataRow["QuestionnaireId"];
			entity.ProcurementUserId = (Convert.IsDBNull(dataRow["ProcurementUserId"]))?(int)0:(System.Int32?)dataRow["ProcurementUserId"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireContactProcurementFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementFilterBuilder : SqlFilterBuilder<QuestionnaireContactProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilterBuilder class.
		/// </summary>
		public QuestionnaireContactProcurementFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContactProcurementFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContactProcurementFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContactProcurementFilterBuilder

	#region QuestionnaireContactProcurementParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireContactProcurementParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireContactProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementParameterBuilder class.
		/// </summary>
		public QuestionnaireContactProcurementParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireContactProcurementParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireContactProcurementParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireContactProcurementParameterBuilder
	
	#region QuestionnaireContactProcurementSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireContactProcurement"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireContactProcurementSortBuilder : SqlSortBuilder<QuestionnaireContactProcurementColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireContactProcurementSqlSortBuilder class.
		/// </summary>
		public QuestionnaireContactProcurementSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireContactProcurementSortBuilder

} // end namespace
