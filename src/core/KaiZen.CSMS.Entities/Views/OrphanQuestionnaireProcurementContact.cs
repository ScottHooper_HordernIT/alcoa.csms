﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the '_Orphan_Questionnaire_ProcurementContact' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class OrphanQuestionnaireProcurementContact : OrphanQuestionnaireProcurementContactBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="OrphanQuestionnaireProcurementContact"/> instance.
		///</summary>
		public OrphanQuestionnaireProcurementContact():base(){}	
		
		#endregion
	}
}
