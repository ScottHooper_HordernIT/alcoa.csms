﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using DevExpress.Web.ASPxEditors;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Web;
using KaiZen.Library;

using System.Collections;
using System.IO;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class EditKpiHelpFile : System.Web.UI.Page
    {
        bool EditMode = false;
        int FileVaultId = 0;
        Auth auth = new Auth();

        protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            try
            {
                if (Request.QueryString["q"] == null)
                {
                    //new
                    btnUpload.Text = "Add / Upload";
                }
                else
                {
                    Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (FileVaultId <= 0) throw new Exception("Invalid Url");
                    btnUpload.Text = "Edit / Upload";
                    EditMode = true;
                }

                if (!postBack)
                { //first time load
                    if (auth.RoleId == (int)RoleList.Administrator)
                    {
                        if (FileVaultId > 0)
                            Load(FileVaultId);
                    }
                    else
                    {
                        throw new Exception("Not Allowed");
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromContext(Context).Raise(ex);
                throw new Exception(ex.Message);
            }
        }

        protected void Load(int FileVaultId)
        {
            lblCurrentFile.Visible = true;
            hlCurrentFile.Visible = true;
            hlCurrentFile.Text = "Default";
            hlCurrentFile.NavigateUrl = "javascript:popUp('../PopUps/KpiHelpFilePopup.aspx?Help=Default');";
            btnDelete.Visible = true;

            KpiHelpFilesService kpihf = new KpiHelpFilesService();
            KpiHelpFiles kpiHelplist = kpihf.GetByKpiHelpFileId(FileVaultId);
            String selectedText = kpiHelplist.KpiHelpCaption;
            if (kpiHelplist != null)
            {
                foreach (ListEditItem item in cbKpiHelpCaption.Items)
                {
                    if (item.Text == selectedText)
                        cbKpiHelpCaption.SelectedIndex = item.Index;
                }
                cbKpiHelpCaption.Enabled = false;
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "No File Selected. Please click Browse and select a file for upload.";
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                            UploadFile(ucNewFile, FileVaultId);
                    }
                   // EditFileVaultTable(FileVaultId);
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        ClientScriptManager clientScript = Page.ClientScript;
                        clientScript.RegisterStartupScript(this.GetType(), "CloseWindowScript", "<script>javascript:window.close();</script>");

                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }

        protected void UploadFile(ASPxUploadControl ucNewFile, int fileVaultId)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                KpiHelpFiles fv = new KpiHelpFiles()
                {
                   
                    KpiHelpCaption = cbKpiHelpCaption.SelectedItem.Text,
                   
                };
                if (fileVaultId > 0)
                {
                    KpiHelpFilesService fvService = new KpiHelpFilesService();
                    fv = fvService.GetByKpiHelpFileId(fileVaultId);
                    
                }
                
                Configuration configuration = new Configuration();
                string tempPath = @configuration.GetValue(ConfigList.TempDir); //Need to comment to run in local
                //string tempPath = @"C:\Bishwajit\Files";  //Need to comment to run in server
                string fileName = ucNewFile.UploadedFiles[0].FileName;
                string fullPath = tempPath + fileName;
                ucNewFile.UploadedFiles[0].SaveAs(fullPath);

                FileStream fs = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                fv.FileName = fileName;
                fv.Content = FileUtilities.Read.ReadFully(fs, 51200);
                fv.FileHash = FileUtilities.Calculate.HashFile(fv.Content);
                fv.ContentLength = Convert.ToInt32(fs.Length);
                fv.ModifiedByUserId = auth.UserId;
                fv.ModifiedDate = DateTime.Now;

                fs.Close();
                fs.Dispose();
                System.Threading.Thread.Sleep(1000);
                File.Delete(fullPath);

                if (fileVaultId > 0)
                {
                    DataRepository.KpiHelpFilesProvider.Update(transactionManager, fv);
                    transactionManager.Commit();
                }
                else
                {
                    DataRepository.KpiHelpFilesProvider.Insert(transactionManager, fv);
                    transactionManager.Commit();
                }

               
            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

                bool success = false;
                string errorMsg = "Error Deleting File. Contact your Administrator.";
                try
                {
                    if (Request.QueryString["q"] != null)
                        Int32.TryParse(Request.QueryString["q"], out FileVaultId);

                    if (ucNewFile.UploadedFiles != null && ucNewFile.UploadedFiles.Length > 0)
                    {
                        //if (!String.IsNullOrEmpty(ucNewFile.UploadedFiles[0].FileName) && ucNewFile.UploadedFiles[0].IsValid)
                        //   UploadFile(ucNewFile, FileVaultId);
                    }
                    Delete(FileVaultId);
                    success = true;
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromContext(this.Context).Raise(ex);
                    errorMsg = ex.Message;
                }
                finally
                {
                    if (success)
                    {
                        //Close and refresh main page.
                        ClientScriptManager clientScript = Page.ClientScript;
                        clientScript.RegisterStartupScript(this.GetType(), "CloseWindowScript", "<script>javascript:window.close();</script>");
                    }
                    else
                    {
                        lblFileUploadMsg.Text = String.Format("Error Occured: {0}", errorMsg);
                    }
                }
            }
        }

        protected void Delete(int fileVaultId)
        {
            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);

                KpiHelpFilesService fvService = new KpiHelpFilesService();
                KpiHelpFiles fv = fvService.GetByKpiHelpFileId(fileVaultId);
                if (fv == null) throw new Exception("File Not Found.");

                DataRepository.KpiHelpFilesProvider.Delete(transactionManager, fv);

                transactionManager.Commit();

            }
            catch (Exception ex)
            {
                if ((transactionManager != null) && (transactionManager.IsOpen))
                    transactionManager.Rollback();
                throw ex;
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            ListControlCollections();
        }

        protected void ListControlCollections()
        {
            ArrayList controlList = new ArrayList();
            AddControls(Page.Controls, controlList);

            foreach (string str in controlList)
                Response.Write(str + "<br />");
            Response.Write("Total Controls:" + controlList.Count);
        }

        private void AddControls(ControlCollection page, ArrayList controlList)
        {
            foreach (Control c in page)
            {
                if (c.ID != null)
                    controlList.Add(c.ID);

                if (c.HasControls())
                    AddControls(c.Controls, controlList);
            }
        }
    }
}