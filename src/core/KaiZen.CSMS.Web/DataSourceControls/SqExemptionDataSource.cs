﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SqExemptionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SqExemptionDataSourceDesigner))]
	public class SqExemptionDataSource : ProviderDataSource<SqExemption, SqExemptionKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionDataSource class.
		/// </summary>
		public SqExemptionDataSource() : base(new SqExemptionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SqExemptionDataSourceView used by the SqExemptionDataSource.
		/// </summary>
		protected SqExemptionDataSourceView SqExemptionView
		{
			get { return ( View as SqExemptionDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SqExemptionDataSource control invokes to retrieve data.
		/// </summary>
		public SqExemptionSelectMethod SelectMethod
		{
			get
			{
				SqExemptionSelectMethod selectMethod = SqExemptionSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SqExemptionSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SqExemptionDataSourceView class that is to be
		/// used by the SqExemptionDataSource.
		/// </summary>
		/// <returns>An instance of the SqExemptionDataSourceView class.</returns>
		protected override BaseDataSourceView<SqExemption, SqExemptionKey> GetNewDataSourceView()
		{
			return new SqExemptionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SqExemptionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SqExemptionDataSourceView : ProviderDataSourceView<SqExemption, SqExemptionKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SqExemptionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SqExemptionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SqExemptionDataSourceView(SqExemptionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SqExemptionDataSource SqExemptionOwner
		{
			get { return Owner as SqExemptionDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SqExemptionSelectMethod SelectMethod
		{
			get { return SqExemptionOwner.SelectMethod; }
			set { SqExemptionOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SqExemptionService SqExemptionProvider
		{
			get { return Provider as SqExemptionService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SqExemption> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SqExemption> results = null;
			SqExemption item;
			count = 0;
			
			System.Int32 _sqExemptionId;
			System.Int32 _companyId;
			System.Int32 _requestedByCompanyId;
			System.Int32 _siteId;
			System.Int32 _companyStatus2Id;
			System.Int32 _modifiedByUserId;
			System.Int32 _fileVaultId;

			switch ( SelectMethod )
			{
				case SqExemptionSelectMethod.Get:
					SqExemptionKey entityKey  = new SqExemptionKey();
					entityKey.Load(values);
					item = SqExemptionProvider.Get(entityKey);
					results = new TList<SqExemption>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SqExemptionSelectMethod.GetAll:
                    results = SqExemptionProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SqExemptionSelectMethod.GetPaged:
					results = SqExemptionProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SqExemptionSelectMethod.Find:
					if ( FilterParameters != null )
						results = SqExemptionProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SqExemptionProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SqExemptionSelectMethod.GetBySqExemptionId:
					_sqExemptionId = ( values["SqExemptionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SqExemptionId"], typeof(System.Int32)) : (int)0;
					item = SqExemptionProvider.GetBySqExemptionId(_sqExemptionId);
					results = new TList<SqExemption>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				case SqExemptionSelectMethod.GetByCompanyId:
					_companyId = ( values["CompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyId"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetByCompanyId(_companyId, this.StartIndex, this.PageSize, out count);
					break;
				case SqExemptionSelectMethod.GetByRequestedByCompanyId:
					_requestedByCompanyId = ( values["RequestedByCompanyId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["RequestedByCompanyId"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetByRequestedByCompanyId(_requestedByCompanyId, this.StartIndex, this.PageSize, out count);
					break;
				case SqExemptionSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				case SqExemptionSelectMethod.GetByCompanyStatus2Id:
					_companyStatus2Id = ( values["CompanyStatus2Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CompanyStatus2Id"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetByCompanyStatus2Id(_companyStatus2Id, this.StartIndex, this.PageSize, out count);
					break;
				case SqExemptionSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case SqExemptionSelectMethod.GetByFileVaultId:
					_fileVaultId = ( values["FileVaultId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultId"], typeof(System.Int32)) : (int)0;
					results = SqExemptionProvider.GetByFileVaultId(_fileVaultId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SqExemptionSelectMethod.Get || SelectMethod == SqExemptionSelectMethod.GetBySqExemptionId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SqExemption entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SqExemptionProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SqExemption> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SqExemptionProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SqExemptionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SqExemptionDataSource class.
	/// </summary>
	public class SqExemptionDataSourceDesigner : ProviderDataSourceDesigner<SqExemption, SqExemptionKey>
	{
		/// <summary>
		/// Initializes a new instance of the SqExemptionDataSourceDesigner class.
		/// </summary>
		public SqExemptionDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SqExemptionSelectMethod SelectMethod
		{
			get { return ((SqExemptionDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SqExemptionDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SqExemptionDataSourceActionList

	/// <summary>
	/// Supports the SqExemptionDataSourceDesigner class.
	/// </summary>
	internal class SqExemptionDataSourceActionList : DesignerActionList
	{
		private SqExemptionDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SqExemptionDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SqExemptionDataSourceActionList(SqExemptionDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SqExemptionSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SqExemptionDataSourceActionList
	
	#endregion SqExemptionDataSourceDesigner
	
	#region SqExemptionSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SqExemptionDataSource.SelectMethod property.
	/// </summary>
	public enum SqExemptionSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetBySqExemptionId method.
		/// </summary>
		GetBySqExemptionId,
		/// <summary>
		/// Represents the GetByCompanyId method.
		/// </summary>
		GetByCompanyId,
		/// <summary>
		/// Represents the GetByRequestedByCompanyId method.
		/// </summary>
		GetByRequestedByCompanyId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetByCompanyStatus2Id method.
		/// </summary>
		GetByCompanyStatus2Id,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByFileVaultId method.
		/// </summary>
		GetByFileVaultId
	}
	
	#endregion SqExemptionSelectMethod

	#region SqExemptionFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionFilter : SqlFilter<SqExemptionColumn>
	{
	}
	
	#endregion SqExemptionFilter

	#region SqExemptionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionExpressionBuilder : SqlExpressionBuilder<SqExemptionColumn>
	{
	}
	
	#endregion SqExemptionExpressionBuilder	

	#region SqExemptionProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SqExemptionChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SqExemptionProperty : ChildEntityProperty<SqExemptionChildEntityTypes>
	{
	}
	
	#endregion SqExemptionProperty
}

