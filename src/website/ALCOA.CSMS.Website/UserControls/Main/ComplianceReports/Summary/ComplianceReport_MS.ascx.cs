using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Web;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.XtraPrinting;
using System.IO;
using System.Collections.Generic;

public partial class UserControls_Tiny_ComplianceReports_ComplianceReport_MS : System.Web.UI.UserControl
{
    Auth auth = new Auth();
    int green = 0;
    int expected = 0;
    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load

            switch (auth.RoleId)
            {
                case ((int)RoleList.Reader):
                    break;
                case ((int)RoleList.Contractor):
                    break;
                case ((int)RoleList.Administrator):
                    break;
                    // do something
                default:
                    Response.Redirect(String.Format("AccessDenied.aspx?error={0}", Server.UrlEncode("Unrecognised User.")));
                    break;
            }
        }
            if (SessionHandler.spVar_Page == "ComplianceReport")
            {
                const string Type = "MS";
                grid.SettingsBehavior.AllowSort = false;
                grid.SettingsBehavior.AllowGroup = false;

                grid.Settings.ShowFilterRow = false;
                grid.Settings.ShowGroupPanel = false;
                //residentialstatus = SessionHandler.spVar_ResidentialStatus2;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();
                if (Convert.ToInt32(SessionHandler.spVar_CompanyId) > 0) //specific company
                {
                    ds = DataRepository.FileDbMedicalTrainingProvider.ComplianceReport_ByCompany(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Type,
                            Convert.ToInt32(SessionHandler.spVar_CompanyId),
                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                            SessionHandler.spVar_CategoryId2);

                    grid.Columns["Company Name"].Visible = false;
                }
                else
                {
                    ds = DataRepository.FileDbMedicalTrainingProvider.ComplianceReport(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Type);
                    grid.Columns["Company Name"].Visible = true;
                }

                grid.DataSource = ds.Tables[0];
				//Added By Bishwajit for Item#31
                Session["CompanyReportMS"] = ds.Tables[0];
				//End Added By Bishwajit for Item#31
                grid.DataBind();
                ds.Dispose();

                if (green >= expected)
                {
                    int a = Convert.ToInt32(SessionHandler.spVar_ComplianceCount);
                    a++;
                    SessionHandler.spVar_ComplianceCount = a.ToString();
                }
            }
            else if (SessionHandler.spVar_Page == "ComplianceReportBySite")
            {
                const string Type = "MS";
                //grid.SettingsBehavior.AllowSort = false;
                //grid.SettingsBehavior.AllowGroup = false;

                //grid.Settings.ShowFilterRow = false;
                //grid.Settings.ShowGroupPanel = false;

                grid.DataSourceID = null;

                DataSet ds = new DataSet();
                if (Convert.ToInt32(SessionHandler.spVar_SiteId) > 0) //Site
                {
                    ds = DataRepository.FileDbMedicalTrainingProvider.ComplianceReport_BySite(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Type,
                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                            SessionHandler.spVar_CategoryId2);

                    grid.Columns["Site"].Visible = false;
                }
                else //Region
                {
                    ds = DataRepository.FileDbMedicalTrainingProvider.ComplianceReport_BySite(
                            Convert.ToInt32(SessionHandler.spVar_Year),
                            Type,
                            Convert.ToInt32(SessionHandler.spVar_SiteId),
                            SessionHandler.spVar_CategoryId2);
                    grid.Columns["Site"].Visible = true;
                }

                grid.DataSource = ds.Tables[0];
                grid.DataBind();
                ds.Dispose();
            }
    }

    protected void grid_RowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        int month;
        int qtr = 4;
        string selectedYear = SessionHandler.spVar_Year;

        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
            if (month <= 12) qtr = 4;
            if (month <= 9) qtr = 3;
            if (month <= 6) qtr = 2;
            if (month <= 3) qtr = 1;
            expected = qtr;
        }
        else
        {
            month = 12;
            qtr = 4;
        }

        if (e.RowType != GridViewRowType.Data) return;
        Label lblYear = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted13") as Label;
        int yearCount = (int)grid.GetRowValues(e.VisibleIndex, "Year");
        if (yearCount > 0)
        {
            if (lblYear != null) lblYear.ForeColor = Color.Green;
            if (lblYear != null) lblYear.Text = yearCount.ToString();
        }
        else
        {
            if (yearCount == -1)
            {
                if (lblYear != null) lblYear.Text = "-";
                if (lblYear != null) lblYear.ForeColor = Color.Black;
            }
            else
            {
                if (lblYear != null) lblYear.ForeColor = Color.Red;
                if (lblYear != null) lblYear.Text = yearCount.ToString();
            }
        }


        if (qtr >= 1)
        {
            #region Qtr1
            if (e.RowType != GridViewRowType.Data) return;
            Label label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted1") as Label;
            int change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr1");
            if (label != null) label.Text = "";
            System.Web.UI.WebControls.Image img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage1")
                                        as System.Web.UI.WebControls.Image;
            if (img != null) img.Visible = false;
            if (change != 0)
            {
                if (img != null) img.Visible = true;
                if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                if (label != null) label.ForeColor = Color.Green;
                //label.Text = "(" + change + ")";
                green++;
            }
            else
            {
                if (img != null) img.Visible = true;
                if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                if (label != null) label.ForeColor = Color.Red;
            }
            #endregion
            if (qtr >= 2)
            {
                #region Qtr2
                label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted2") as Label;
                change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr2");
                if (label != null) label.Text = "";
                img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage2")
                                            as System.Web.UI.WebControls.Image;
                if (img != null) img.Visible = false;
                if (change != 0)
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                    if (label != null) label.ForeColor = Color.Green;
                    //label.Text = "(" + change + ")";
                    green++;
                }
                else
                {
                    if (img != null) img.Visible = true;
                    if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                    if (label != null) label.ForeColor = Color.Red;
                }
                #endregion
                if (qtr >= 3)
                {
                    #region Qtr3
                    label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted3") as Label;
                    change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr3");
                    if (label != null) label.Text = "";
                    img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage3")
                                                as System.Web.UI.WebControls.Image;
                    if (img != null) img.Visible = false;
                    if (change != 0)
                    {
                        if (img != null) img.Visible = true;
                        if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                        if (label != null) label.ForeColor = Color.Green;
                        //label.Text = "(" + change + ")";
                        green++;
                    }
                    else
                    {
                        if (img != null) img.Visible = true;
                        if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                        if (label != null) label.ForeColor = Color.Red;
                    }
                    #endregion
                    if (qtr >= 4)
                    {
                        #region Qtr4
                        label = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "noKPISubmitted4") as Label;
                        change = (int)grid.GetRowValues(e.VisibleIndex, "Qtr4");
                        if (label != null) label.Text = "";
                        img = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "changeImage4")
                                                    as System.Web.UI.WebControls.Image;
                        if (img != null) img.Visible = false;
                        if (change != 0)
                        {
                            if (img != null) img.Visible = true;
                            if (img != null) img.ImageUrl = "~/Images/greentick.gif";
                            if (label != null) label.ForeColor = Color.Green;
                            //label.Text = "(" + change + ")";
                            green++;
                        }
                        else
                        {
                            if (img != null) img.Visible = true;
                            if (img != null) img.ImageUrl = "~/Images/redcross.gif";
                            if (label != null) label.ForeColor = Color.Red;
                        }
                        #endregion
                    }
                }
            }
        }
    }


    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName == "CompanyId")
        {
            ASPxComboBox comboBox = e.Editor as ASPxComboBox;
            //comboBox.Items.Clear();
            comboBox.ClientSideEvents.Init = "function(s, e) {s.InsertItem(0, '(ALL)', ''); s.RemoveItem(-1);}";
        }
    }

    public int getExpected()
    {
        if (green >= expected)
        { return 1; }
        else { return 0; }
    }

    public PrintableComponentLink ExportToPdf()
    {
        PrintableComponentLink pcl = new PrintableComponentLink(new PrintingSystem());
        pcl.Component = gridExport;
        pcl.CreateReportHeaderArea += new CreateAreaEventHandler(pcl_CreateReportHeaderArea);
        pcl.CreateDocument(false);
        return pcl;
    }

    public void pcl_CreateReportHeaderArea(object sender, CreateAreaEventArgs e)
    {
        TextBrick brick1 = e.Graph.DrawString("Medical Schedules", System.Drawing.ColorTranslator.FromHtml("#000099"), new RectangleF(0, 0, 620, 20), DevExpress.XtraPrinting.BorderSide.None);
        brick1.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
        brick1.Font = new Font(FontFamily.GenericSansSerif, 12, FontStyle.Regular);
    }

    public string ExportToHtml()
    {
        var sb = new StringBuilder();

        sb.Append("<table width='100%' border='1' style='font-size:9px; color:Black;'>");
        sb.Append("<tbody>");
        sb.Append("<tr>");
        sb.Append("<td width='25%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Residential Category</td>");
        sb.Append("<td width='50%' bgcolor='#94b6e8' border='1' align='left' valign='top'>Company Name</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 1</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 2</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 3</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Qtr 4</td>");
        sb.Append("<td width='5%' bgcolor='#94b6e8' border='1' align='center' valign='top'>Year</td>");
        sb.Append("</tr>");

        var dt = (DataTable)grid.DataSource;
        if (dt != null && dt.Rows.Count > 0)
        {
            dt.DefaultView.Sort = "CategoryDesc asc, CompanyName asc";
            dt = dt.DefaultView.ToTable();

            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<tr>");
                sb.Append(string.Format("<td width='25%' border='1' align='left' valign='top'>{0}</td>", dr["CategoryDesc"]));
                sb.Append(string.Format("<td width='50%' border='1' align='left' valign='top'>{0}</td>", dr["CompanyName"].ToString()));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr1"].ToString(), 1)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr2"].ToString(), 2)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr3"].ToString(), 3)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", QtrHtml(dr["Qtr4"].ToString(), 4)));
                sb.Append(string.Format("<td width='5%' border='1' align='center' valign='top'>{0}</td>", YearHtml(dr["Year"])));
            }
        }
        else
        {
            sb.Append("<tr>");
            sb.Append("<td colspan='4'>No Medical Schedules submitted</td>");
            sb.Append("</tr>");
        }

        sb.Append("</tbody>");
        sb.Append("</table>");

        return sb.ToString();
    }

    private string QtrHtml(string qtrData, int qtrNumber)
    {
        string html = "";

        int month;
        int qtr = 4;
        string selectedYear = SessionHandler.spVar_Year;

        if (String.IsNullOrEmpty(selectedYear)) { selectedYear = DateTime.Now.Year.ToString(); };
        if (selectedYear == DateTime.Now.Year.ToString())
        {
            month = DateTime.Now.Month;
            if (month <= 12) qtr = 4;
            if (month <= 9) qtr = 3;
            if (month <= 6) qtr = 2;
            if (month <= 3) qtr = 1;
            expected = qtr;
        }
        else
        {
            month = 12;
            qtr = 4;
        }

        if (qtr >= 1)
        {
            if (qtrNumber == 1)
            {
                if (qtrData != "0")
                {
                    html = "<img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='center' valign='middle' />";
                }
                else
                {
                    html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                }
            }
            else
            {
                if (qtr >= 2)
                {
                    if (qtrNumber == 2)
                    {
                        if (qtrData != "0")
                        {
                            html = "<img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='center' valign='middle' />";
                        }
                        else
                        {
                            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                        }
                    }
                    else
                    {
                        if (qtr >= 3)
                        {
                            if (qtrNumber == 3)
                            {
                                if (qtrData != "0")
                                {
                                    html = "<img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='center' valign='middle' />";
                                }
                                else
                                {
                                    html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                                }
                            }
                            else
                            {
                                if (qtr >= 4)
                                {
                                    if (qtrNumber == 4)
                                    {
                                        if (qtrData != "0")
                                        {
                                            html = "<img width='16px' height='16px' src='[UrlPath]/images/greentick.gif' align='center' valign='middle' />";
                                        }
                                        else
                                        {
                                            html = "<img width='16px' height='16px' src='[UrlPath]/images/redcross.gif' align='center' valign='middle' />";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return html;
    }

    private string YearHtml(object yearData)
    {
        string html = "";

        int yearCount = (int)yearData;

        if (yearCount > 0)
        {
            html = string.Format("<span style='color:#00ff00;' valign='middle'>{0}</span>", yearCount.ToString());
        }
        else
        {
            if (yearCount == -1)
            {
                html = string.Format("<span style='color:#000000;' valign='middle'>{0}</span>", "-");
            }
            else
            {
                html = string.Format("<span style='color:#ff0000;' valign='middle'>{0}</span>", yearCount.ToString());
            }
        }

        return html;
    }
}
