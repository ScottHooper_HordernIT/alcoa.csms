﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------



public partial class PopUps_KpiProjectsList {
    
    /// <summary>
    /// ASPxButton1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxEditors.ASPxButton ASPxButton1;
    
    /// <summary>
    /// ASPxPageControl1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxTabControl.ASPxPageControl ASPxPageControl1;
    
    /// <summary>
    /// gridPurchaseOrders0 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridPurchaseOrders0;
    
    /// <summary>
    /// gridPurchaseOrders control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::DevExpress.Web.ASPxGridView.ASPxGridView gridPurchaseOrders;
    
    /// <summary>
    /// KpiPurchaseOrderListOpenProjectsAscDataSource1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::KaiZen.CSMS.Web.Data.KpiPurchaseOrderListOpenProjectsAscDataSource KpiPurchaseOrderListOpenProjectsAscDataSource1;
    
    /// <summary>
    /// KpiPurchaseOrderListOpenProjectsDistinctAscDataSource1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::KaiZen.CSMS.Web.Data.KpiPurchaseOrderListOpenProjectsDistinctAscDataSource KpiPurchaseOrderListOpenProjectsDistinctAscDataSource1;
}
