﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainRationaleProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainRationaleProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainRationale, KaiZen.CSMS.Entities.QuestionnaireMainRationaleKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainRationaleKey key)
		{
			return Delete(transactionManager, key.QuestionnaireMainRationaleId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireMainRationaleId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireMainRationaleId)
		{
			return Delete(null, _questionnaireMainRationaleId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireMainRationaleId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireMainRationaleId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainRationale Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainRationaleKey key, int start, int pageLength)
		{
			return GetByQuestionnaireMainRationaleId(transactionManager, key.QuestionnaireMainRationaleId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(System.Int32 _questionnaireMainRationaleId)
		{
			int count = -1;
			return GetByQuestionnaireMainRationaleId(null,_questionnaireMainRationaleId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(System.Int32 _questionnaireMainRationaleId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireMainRationaleId(null, _questionnaireMainRationaleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(TransactionManager transactionManager, System.Int32 _questionnaireMainRationaleId)
		{
			int count = -1;
			return GetByQuestionnaireMainRationaleId(transactionManager, _questionnaireMainRationaleId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(TransactionManager transactionManager, System.Int32 _questionnaireMainRationaleId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireMainRationaleId(transactionManager, _questionnaireMainRationaleId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(System.Int32 _questionnaireMainRationaleId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireMainRationaleId(null, _questionnaireMainRationaleId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireMainRationaleId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionnaireMainRationaleId(TransactionManager transactionManager, System.Int32 _questionnaireMainRationaleId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(null,_questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNo(null, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionNo(transactionManager, _questionNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(System.String _questionNo, int start, int pageLength, out int count)
		{
			return GetByQuestionNo(null, _questionNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireMainRationale index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainRationale GetByQuestionNo(TransactionManager transactionManager, System.String _questionNo, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainRationale&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainRationale&gt;"/></returns>
		public static TList<QuestionnaireMainRationale> Fill(IDataReader reader, TList<QuestionnaireMainRationale> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainRationale c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainRationale")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireMainRationaleColumn.QuestionnaireMainRationaleId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainRationale>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainRationale",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainRationale();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireMainRationaleId = (System.Int32)reader[((int)QuestionnaireMainRationaleColumn.QuestionnaireMainRationaleId - 1)];
					c.QuestionNo = (System.String)reader[((int)QuestionnaireMainRationaleColumn.QuestionNo - 1)];
					c.Rationale = (reader.IsDBNull(((int)QuestionnaireMainRationaleColumn.Rationale - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainRationaleColumn.Rationale - 1)];
					c.Assistance = (reader.IsDBNull(((int)QuestionnaireMainRationaleColumn.Assistance - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainRationaleColumn.Assistance - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainRationale entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireMainRationaleId = (System.Int32)reader[((int)QuestionnaireMainRationaleColumn.QuestionnaireMainRationaleId - 1)];
			entity.QuestionNo = (System.String)reader[((int)QuestionnaireMainRationaleColumn.QuestionNo - 1)];
			entity.Rationale = (reader.IsDBNull(((int)QuestionnaireMainRationaleColumn.Rationale - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainRationaleColumn.Rationale - 1)];
			entity.Assistance = (reader.IsDBNull(((int)QuestionnaireMainRationaleColumn.Assistance - 1)))?null:(System.Byte[])reader[((int)QuestionnaireMainRationaleColumn.Assistance - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainRationale entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireMainRationaleId = (System.Int32)dataRow["QuestionnaireMainRationaleId"];
			entity.QuestionNo = (System.String)dataRow["QuestionNo"];
			entity.Rationale = Convert.IsDBNull(dataRow["Rationale"]) ? null : (System.Byte[])dataRow["Rationale"];
			entity.Assistance = Convert.IsDBNull(dataRow["Assistance"]) ? null : (System.Byte[])dataRow["Assistance"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainRationale"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainRationale Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainRationale entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainRationale object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainRationale instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainRationale Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainRationale entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainRationaleChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainRationale</c>
	///</summary>
	public enum QuestionnaireMainRationaleChildEntityTypes
	{
	}
	
	#endregion QuestionnaireMainRationaleChildEntityTypes
	
	#region QuestionnaireMainRationaleFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleFilterBuilder : SqlFilterBuilder<QuestionnaireMainRationaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilterBuilder class.
		/// </summary>
		public QuestionnaireMainRationaleFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainRationaleFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainRationaleFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainRationaleFilterBuilder
	
	#region QuestionnaireMainRationaleParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainRationaleParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainRationaleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleParameterBuilder class.
		/// </summary>
		public QuestionnaireMainRationaleParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainRationaleParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainRationaleParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainRationaleParameterBuilder
	
	#region QuestionnaireMainRationaleSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainRationaleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainRationale"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainRationaleSortBuilder : SqlSortBuilder<QuestionnaireMainRationaleColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainRationaleSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainRationaleSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainRationaleSortBuilder
	
} // end namespace
