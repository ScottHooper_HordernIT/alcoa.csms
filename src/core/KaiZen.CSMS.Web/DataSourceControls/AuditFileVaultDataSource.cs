﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AuditFileVaultProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AuditFileVaultDataSourceDesigner))]
	public class AuditFileVaultDataSource : ProviderDataSource<AuditFileVault, AuditFileVaultKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultDataSource class.
		/// </summary>
		public AuditFileVaultDataSource() : base(new AuditFileVaultService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AuditFileVaultDataSourceView used by the AuditFileVaultDataSource.
		/// </summary>
		protected AuditFileVaultDataSourceView AuditFileVaultView
		{
			get { return ( View as AuditFileVaultDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AuditFileVaultDataSource control invokes to retrieve data.
		/// </summary>
		public AuditFileVaultSelectMethod SelectMethod
		{
			get
			{
				AuditFileVaultSelectMethod selectMethod = AuditFileVaultSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AuditFileVaultSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AuditFileVaultDataSourceView class that is to be
		/// used by the AuditFileVaultDataSource.
		/// </summary>
		/// <returns>An instance of the AuditFileVaultDataSourceView class.</returns>
		protected override BaseDataSourceView<AuditFileVault, AuditFileVaultKey> GetNewDataSourceView()
		{
			return new AuditFileVaultDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AuditFileVaultDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AuditFileVaultDataSourceView : ProviderDataSourceView<AuditFileVault, AuditFileVaultKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AuditFileVaultDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AuditFileVaultDataSourceView(AuditFileVaultDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AuditFileVaultDataSource AuditFileVaultOwner
		{
			get { return Owner as AuditFileVaultDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AuditFileVaultSelectMethod SelectMethod
		{
			get { return AuditFileVaultOwner.SelectMethod; }
			set { AuditFileVaultOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AuditFileVaultService AuditFileVaultProvider
		{
			get { return Provider as AuditFileVaultService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AuditFileVault> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AuditFileVault> results = null;
			AuditFileVault item;
			count = 0;
			
			System.Int32 _auditId;
			System.Int32? _fileVaultId_nullable;
			System.Int32? _modifiedByUserId_nullable;

			switch ( SelectMethod )
			{
				case AuditFileVaultSelectMethod.Get:
					AuditFileVaultKey entityKey  = new AuditFileVaultKey();
					entityKey.Load(values);
					item = AuditFileVaultProvider.Get(entityKey);
					results = new TList<AuditFileVault>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AuditFileVaultSelectMethod.GetAll:
                    results = AuditFileVaultProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AuditFileVaultSelectMethod.GetPaged:
					results = AuditFileVaultProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AuditFileVaultSelectMethod.Find:
					if ( FilterParameters != null )
						results = AuditFileVaultProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AuditFileVaultProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AuditFileVaultSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = AuditFileVaultProvider.GetByAuditId(_auditId);
					results = new TList<AuditFileVault>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AuditFileVaultSelectMethod.GetByFileVaultId:
					_fileVaultId_nullable = (System.Int32?) EntityUtil.ChangeType(values["FileVaultId"], typeof(System.Int32?));
					results = AuditFileVaultProvider.GetByFileVaultId(_fileVaultId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case AuditFileVaultSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32?));
					results = AuditFileVaultProvider.GetByModifiedByUserId(_modifiedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AuditFileVaultSelectMethod.Get || SelectMethod == AuditFileVaultSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AuditFileVault entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AuditFileVaultProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AuditFileVault> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AuditFileVaultProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AuditFileVaultDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AuditFileVaultDataSource class.
	/// </summary>
	public class AuditFileVaultDataSourceDesigner : ProviderDataSourceDesigner<AuditFileVault, AuditFileVaultKey>
	{
		/// <summary>
		/// Initializes a new instance of the AuditFileVaultDataSourceDesigner class.
		/// </summary>
		public AuditFileVaultDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AuditFileVaultSelectMethod SelectMethod
		{
			get { return ((AuditFileVaultDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AuditFileVaultDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AuditFileVaultDataSourceActionList

	/// <summary>
	/// Supports the AuditFileVaultDataSourceDesigner class.
	/// </summary>
	internal class AuditFileVaultDataSourceActionList : DesignerActionList
	{
		private AuditFileVaultDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AuditFileVaultDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AuditFileVaultDataSourceActionList(AuditFileVaultDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AuditFileVaultSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AuditFileVaultDataSourceActionList
	
	#endregion AuditFileVaultDataSourceDesigner
	
	#region AuditFileVaultSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AuditFileVaultDataSource.SelectMethod property.
	/// </summary>
	public enum AuditFileVaultSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId,
		/// <summary>
		/// Represents the GetByFileVaultId method.
		/// </summary>
		GetByFileVaultId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion AuditFileVaultSelectMethod

	#region AuditFileVaultFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AuditFileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultFilter : SqlFilter<AuditFileVaultColumn>
	{
	}
	
	#endregion AuditFileVaultFilter

	#region AuditFileVaultExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AuditFileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultExpressionBuilder : SqlExpressionBuilder<AuditFileVaultColumn>
	{
	}
	
	#endregion AuditFileVaultExpressionBuilder	

	#region AuditFileVaultProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AuditFileVaultChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AuditFileVault"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AuditFileVaultProperty : ChildEntityProperty<AuditFileVaultChildEntityTypes>
	{
	}
	
	#endregion AuditFileVaultProperty
}

