﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireMainResponseProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireMainResponseProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireMainResponse, KaiZen.CSMS.Entities.QuestionnaireMainResponseKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainResponseKey key)
		{
			return Delete(transactionManager, key.ResponseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_responseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _responseId)
		{
			return Delete(null, _responseId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _responseId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		FK_QuestionnaireMainResponse_Questionnaire Description: 
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(_questionnaireId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		FK_QuestionnaireMainResponse_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainResponse> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		FK_QuestionnaireMainResponse_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		fkQuestionnaireMainResponseQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count =  -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		fkQuestionnaireMainResponseQuestionnaire Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength,out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Questionnaire key.
		///		FK_QuestionnaireMainResponse_Questionnaire Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public abstract TList<QuestionnaireMainResponse> GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		FK_QuestionnaireMainResponse_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		FK_QuestionnaireMainResponse_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainResponse> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		FK_QuestionnaireMainResponse_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		fkQuestionnaireMainResponseUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		fkQuestionnaireMainResponseUsersModifiedBy Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_Users_ModifiedBy key.
		///		FK_QuestionnaireMainResponse_Users_ModifiedBy Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public abstract TList<QuestionnaireMainResponse> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainResponse_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByAnswerId(System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(_answerId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainResponse_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireMainResponse> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainResponse_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByAnswerId(transactionManager, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		fkQuestionnaireMainResponseQuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByAnswerId(System.Int32 _answerId, int start, int pageLength)
		{
			int count =  -1;
			return GetByAnswerId(null, _answerId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		fkQuestionnaireMainResponseQuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_answerId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public TList<QuestionnaireMainResponse> GetByAnswerId(System.Int32 _answerId, int start, int pageLength,out int count)
		{
			return GetByAnswerId(null, _answerId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireMainResponse_QuestionnaireAnswer key.
		///		FK_QuestionnaireMainResponse_QuestionnaireAnswer Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireMainResponse objects.</returns>
		public abstract TList<QuestionnaireMainResponse> GetByAnswerId(TransactionManager transactionManager, System.Int32 _answerId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireMainResponse Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainResponseKey key, int start, int pageLength)
		{
			return GetByResponseId(transactionManager, key.ResponseId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(null,_responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength)
		{
			int count = -1;
			return GetByResponseId(transactionManager, _responseId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(System.Int32 _responseId, int start, int pageLength, out int count)
		{
			return GetByResponseId(null, _responseId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_responseId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByResponseId(TransactionManager transactionManager, System.Int32 _responseId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null,_questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireIdAnswerId(transactionManager, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireIdAnswerId(null, _questionnaireId, _answerId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireResponse index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="_answerId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireMainResponse GetByQuestionnaireIdAnswerId(TransactionManager transactionManager, System.Int32 _questionnaireId, System.Int32 _answerId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireMainResponse&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireMainResponse&gt;"/></returns>
		public static TList<QuestionnaireMainResponse> Fill(IDataReader reader, TList<QuestionnaireMainResponse> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireMainResponse c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireMainResponse")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireMainResponseColumn.ResponseId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireMainResponse>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireMainResponse",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireMainResponse();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ResponseId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.ResponseId - 1)];
					c.AnswerId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.AnswerId - 1)];
					c.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainResponseColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireMainResponseColumn.AnswerText - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.QuestionnaireId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireMainResponseColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireMainResponse entity)
		{
			if (!reader.Read()) return;
			
			entity.ResponseId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.ResponseId - 1)];
			entity.AnswerId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.AnswerId - 1)];
			entity.AnswerText = (reader.IsDBNull(((int)QuestionnaireMainResponseColumn.AnswerText - 1)))?null:(System.String)reader[((int)QuestionnaireMainResponseColumn.AnswerText - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.QuestionnaireId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireMainResponseColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireMainResponseColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireMainResponse entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ResponseId = (System.Int32)dataRow["ResponseId"];
			entity.AnswerId = (System.Int32)dataRow["AnswerId"];
			entity.AnswerText = Convert.IsDBNull(dataRow["AnswerText"]) ? null : (System.String)dataRow["AnswerText"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireMainResponse"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainResponse Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainResponse entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region AnswerIdSource	
			if (CanDeepLoad(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepLoadType, innerList) 
				&& entity.AnswerIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AnswerId;
				QuestionnaireMainAnswer tmpEntity = EntityManager.LocateEntity<QuestionnaireMainAnswer>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireMainAnswer), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AnswerIdSource = tmpEntity;
				else
					entity.AnswerIdSource = DataRepository.QuestionnaireMainAnswerProvider.GetByAnswerId(transactionManager, entity.AnswerId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnswerIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AnswerIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireMainAnswerProvider.DeepLoad(transactionManager, entity.AnswerIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AnswerIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireMainResponse object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireMainResponse instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireMainResponse Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireMainResponse entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region AnswerIdSource
			if (CanDeepSave(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepSaveType, innerList) 
				&& entity.AnswerIdSource != null)
			{
				DataRepository.QuestionnaireMainAnswerProvider.Save(transactionManager, entity.AnswerIdSource);
				entity.AnswerId = entity.AnswerIdSource.AnswerId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireMainResponseChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireMainResponse</c>
	///</summary>
	public enum QuestionnaireMainResponseChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>Users</c> at ModifiedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireMainAnswer</c> at AnswerIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireMainAnswer))]
		QuestionnaireMainAnswer,
		}
	
	#endregion QuestionnaireMainResponseChildEntityTypes
	
	#region QuestionnaireMainResponseFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseFilterBuilder : SqlFilterBuilder<QuestionnaireMainResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilterBuilder class.
		/// </summary>
		public QuestionnaireMainResponseFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseFilterBuilder
	
	#region QuestionnaireMainResponseParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponse"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireMainResponseParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireMainResponseColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseParameterBuilder class.
		/// </summary>
		public QuestionnaireMainResponseParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireMainResponseParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireMainResponseParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireMainResponseParameterBuilder
	
	#region QuestionnaireMainResponseSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainResponseColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireMainResponse"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireMainResponseSortBuilder : SqlSortBuilder<QuestionnaireMainResponseColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireMainResponseSqlSortBuilder class.
		/// </summary>
		public QuestionnaireMainResponseSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireMainResponseSortBuilder
	
} // end namespace
