//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Repo.CSMS.DAL.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Document
    {
        public int DocumentId { get; set; }
        public Nullable<int> RegionId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentFileName { get; set; }
        public string DocumentFileDescription { get; set; }
        public string DocumentType { get; set; }
        public string RootNode { get; set; }
        public string ParentNode { get; set; }
        public string ChildNode { get; set; }
        public byte[] UploadedFile { get; set; }
        public byte[] UploadedFile2 { get; set; }
        public byte[] UploadedFile3 { get; set; }
        public string UploadedFileName { get; set; }
        public string UploadedFileName2 { get; set; }
        public string UploadedFileName3 { get; set; }
    
        public virtual Region Region { get; set; }
    }
}
