
/****** Object:  StoredProcedure [dbo].[_SMP_HS_Assessor_DashBoard]    Script Date: 3/08/2015 1:45:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_SMP_HS_Assessor_DashBoard]                              
(                              
 @SiteId int,                              
 @Month int,                              
 @Year int                              
)                              
AS            
          
DECLARE @HSAssessorsRequiredCompanies INT, @HSAssessorsBeingAssessedCompanies INT          
, @HSAssessorsAwaitingAssignment INT,@HSAssessorsApproved INT,@HSAssessorsApprovedPer INT               
                 
IF (@SiteId > 0)                           
 BEGIN                  
  SELECT @HSAssessorsRequiredCompanies = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A                  
  WHERE (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)      
  AND A.SiteId = @SiteId                    
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month  
          
  -----                  
  SELECT @HSAssessorsBeingAssessedCompanies = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A     
  WHERE [dbo].[GetSMPStatusIdByCompanyIdYear](A.CompanyId,@Year) = 'Being Assessed'     
  AND (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)      
  AND A.SiteId = @SiteId      
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month                   
  -----                  
        
  Select @HSAssessorsAwaitingAssignment = dbo.GetHSAssessorsAwaitingAssignmentDB(@SiteId, @Month, @Year)                 
        
  ------                  
        
  SELECT @HSAssessorsApproved = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A         
  WHERE [dbo].[GetSMPStatusIdByCompanyIdYear](A.CompanyId,@Year) = 'Approved'       
  AND A.SiteId = @SiteId       
  AND (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)          
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month                
  -------                  
  Select @HSAssessorsApprovedPer = dbo.GetHSAssessorsApprovedPercentage(@SiteId, @Month, @Year)          
  --------                  
        
  SELECT @HSAssessorsRequiredCompanies AS [HS Assessors � Required (# Companies)],           
  @HSAssessorsBeingAssessedCompanies AS [HS Assessors � Being Assessed (# Companies)],          
  @HSAssessorsAwaitingAssignment AS [HS Assessors � Awaiting Assignment],                   
  @HSAssessorsApproved AS [HS Assessors � Approved],                  
  @HSAssessorsApprovedPer AS [HS Assessors � Approved %]                   
 END                    
ELSE     
 BEGIN                  
  SELECT @HSAssessorsRequiredCompanies = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A                  
  WHERE (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)      
  AND ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = A.SiteId) > 0)        
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month      
        
  -----                  
  SELECT @HSAssessorsBeingAssessedCompanies = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A     
  WHERE [dbo].[GetSMPStatusIdByCompanyIdYear](A.CompanyId,@Year) = 'Being Assessed'     
  AND (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)      
  AND ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = A.SiteId) > 0)        
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month                  
  -----                  
        
  Select @HSAssessorsAwaitingAssignment = dbo.GetHSAssessorsAwaitingAssignmentDB(@SiteId, @Month, @Year)                 
        
  ------                  
        
  SELECT @HSAssessorsApproved = COUNT(DISTINCT(A.CompanyId)) FROM CompanySiteCategoryStandard A         
  WHERE [dbo].[GetSMPStatusIdByCompanyIdYear](A.CompanyId,@Year) = 'Approved'       
  AND ((Select COUNT(*) FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId and dbo.RegionsSites.SiteId = A.SiteId) > 0)        
  AND (A.CompanySiteCategoryId = 1 OR A.CompanySiteCategoryId = 2)          
  AND YEAR(GETDATE()) = @Year                   
  AND MONTH(GETDATE()) = @Month                
  -------                  
  Select @HSAssessorsApprovedPer = dbo.GetHSAssessorsApprovedPercentage(@SiteId, @Month, @Year)          
  --------                  
        
  SELECT @HSAssessorsRequiredCompanies AS [HS Assessors � Required (# Companies)],           
  @HSAssessorsBeingAssessedCompanies AS [HS Assessors � Being Assessed (# Companies)],          
  @HSAssessorsAwaitingAssignment AS [HS Assessors � Awaiting Assignment],                   
  @HSAssessorsApproved AS [HS Assessors � Approved],                  
  @HSAssessorsApprovedPer AS [HS Assessors � Approved %]                   
 END 
----------------------------------------

GO


