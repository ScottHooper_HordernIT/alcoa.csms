﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UsersProcurementProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UsersProcurementProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.UsersProcurement, KaiZen.CSMS.Entities.UsersProcurementKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementKey key)
		{
			return Delete(transactionManager, key.UsersProcurementId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_usersProcurementId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _usersProcurementId)
		{
			return Delete(null, _usersProcurementId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_usersProcurementId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _usersProcurementId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		FK_UsersProcurement_UsersProcurement_Supervisor Description: 
		/// </summary>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		public TList<UsersProcurement> GetBySupervisorUserId(System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(_supervisorUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		FK_UsersProcurement_UsersProcurement_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		/// <remarks></remarks>
		public TList<UsersProcurement> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		FK_UsersProcurement_UsersProcurement_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		public TList<UsersProcurement> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count = -1;
			return GetBySupervisorUserId(transactionManager, _supervisorUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		fkUsersProcurementUsersProcurementSupervisor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		public TList<UsersProcurement> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		fkUsersProcurementUsersProcurementSupervisor Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		public TList<UsersProcurement> GetBySupervisorUserId(System.Int32? _supervisorUserId, int start, int pageLength,out int count)
		{
			return GetBySupervisorUserId(null, _supervisorUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UsersProcurement_UsersProcurement_Supervisor key.
		///		FK_UsersProcurement_UsersProcurement_Supervisor Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supervisorUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UsersProcurement objects.</returns>
		public abstract TList<UsersProcurement> GetBySupervisorUserId(TransactionManager transactionManager, System.Int32? _supervisorUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.UsersProcurement Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurementKey key, int start, int pageLength)
		{
			return GetByUsersProcurementId(transactionManager, key.UsersProcurementId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_UsersProcurement index.
		/// </summary>
		/// <param name="_usersProcurementId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(System.Int32 _usersProcurementId)
		{
			int count = -1;
			return GetByUsersProcurementId(null,_usersProcurementId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurement index.
		/// </summary>
		/// <param name="_usersProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(System.Int32 _usersProcurementId, int start, int pageLength)
		{
			int count = -1;
			return GetByUsersProcurementId(null, _usersProcurementId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_usersProcurementId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(TransactionManager transactionManager, System.Int32 _usersProcurementId)
		{
			int count = -1;
			return GetByUsersProcurementId(transactionManager, _usersProcurementId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_usersProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(TransactionManager transactionManager, System.Int32 _usersProcurementId, int start, int pageLength)
		{
			int count = -1;
			return GetByUsersProcurementId(transactionManager, _usersProcurementId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurement index.
		/// </summary>
		/// <param name="_usersProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(System.Int32 _usersProcurementId, int start, int pageLength, out int count)
		{
			return GetByUsersProcurementId(null, _usersProcurementId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_usersProcurementId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UsersProcurement GetByUsersProcurementId(TransactionManager transactionManager, System.Int32 _usersProcurementId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_UsersProcurement index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(null,_userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UsersProcurement index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UsersProcurement index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public KaiZen.CSMS.Entities.UsersProcurement GetByUserId(System.Int32 _userId, int start, int pageLength, out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UsersProcurement index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UsersProcurement GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UsersProcurement&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UsersProcurement&gt;"/></returns>
		public static TList<UsersProcurement> Fill(IDataReader reader, TList<UsersProcurement> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.UsersProcurement c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UsersProcurement")
					.Append("|").Append((System.Int32)reader[((int)UsersProcurementColumn.UsersProcurementId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UsersProcurement>(
					key.ToString(), // EntityTrackingKey
					"UsersProcurement",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.UsersProcurement();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementColumn.UsersProcurementId - 1)];
					c.UserId = (System.Int32)reader[((int)UsersProcurementColumn.UserId - 1)];
					c.Enabled = (System.Boolean)reader[((int)UsersProcurementColumn.Enabled - 1)];
					c.SiteId = (reader.IsDBNull(((int)UsersProcurementColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.SiteId - 1)];
					c.RegionId = (reader.IsDBNull(((int)UsersProcurementColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.RegionId - 1)];
					c.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.SupervisorUserId - 1)];
					c.Level3UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level3UserId - 1)];
					c.Level4UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level4UserId - 1)];
					c.Level5UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level5UserId - 1)];
					c.Level6UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level6UserId - 1)];
					c.Level7UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level7UserId - 1)];
					c.Level8UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level8UserId - 1)];
					c.Level9UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level9UserId - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)UsersProcurementColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)UsersProcurementColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.UsersProcurement entity)
		{
			if (!reader.Read()) return;
			
			entity.UsersProcurementId = (System.Int32)reader[((int)UsersProcurementColumn.UsersProcurementId - 1)];
			entity.UserId = (System.Int32)reader[((int)UsersProcurementColumn.UserId - 1)];
			entity.Enabled = (System.Boolean)reader[((int)UsersProcurementColumn.Enabled - 1)];
			entity.SiteId = (reader.IsDBNull(((int)UsersProcurementColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.SiteId - 1)];
			entity.RegionId = (reader.IsDBNull(((int)UsersProcurementColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.RegionId - 1)];
			entity.SupervisorUserId = (reader.IsDBNull(((int)UsersProcurementColumn.SupervisorUserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.SupervisorUserId - 1)];
			entity.Level3UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level3UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level3UserId - 1)];
			entity.Level4UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level4UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level4UserId - 1)];
			entity.Level5UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level5UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level5UserId - 1)];
			entity.Level6UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level6UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level6UserId - 1)];
			entity.Level7UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level7UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level7UserId - 1)];
			entity.Level8UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level8UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level8UserId - 1)];
			entity.Level9UserId = (reader.IsDBNull(((int)UsersProcurementColumn.Level9UserId - 1)))?null:(System.Int32?)reader[((int)UsersProcurementColumn.Level9UserId - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)UsersProcurementColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)UsersProcurementColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.UsersProcurement entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UsersProcurementId = (System.Int32)dataRow["UsersProcurementId"];
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.Enabled = (System.Boolean)dataRow["Enabled"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.SupervisorUserId = Convert.IsDBNull(dataRow["SupervisorUserId"]) ? null : (System.Int32?)dataRow["SupervisorUserId"];
			entity.Level3UserId = Convert.IsDBNull(dataRow["Level3UserId"]) ? null : (System.Int32?)dataRow["Level3UserId"];
			entity.Level4UserId = Convert.IsDBNull(dataRow["Level4UserId"]) ? null : (System.Int32?)dataRow["Level4UserId"];
			entity.Level5UserId = Convert.IsDBNull(dataRow["Level5UserId"]) ? null : (System.Int32?)dataRow["Level5UserId"];
			entity.Level6UserId = Convert.IsDBNull(dataRow["Level6UserId"]) ? null : (System.Int32?)dataRow["Level6UserId"];
			entity.Level7UserId = Convert.IsDBNull(dataRow["Level7UserId"]) ? null : (System.Int32?)dataRow["Level7UserId"];
			entity.Level8UserId = Convert.IsDBNull(dataRow["Level8UserId"]) ? null : (System.Int32?)dataRow["Level8UserId"];
			entity.Level9UserId = Convert.IsDBNull(dataRow["Level9UserId"]) ? null : (System.Int32?)dataRow["Level9UserId"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UsersProcurement"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersProcurement Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurement entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SupervisorUserIdSource	
			if (CanDeepLoad(entity, "Users|SupervisorUserIdSource", deepLoadType, innerList) 
				&& entity.SupervisorUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupervisorUserId ?? (int)0);
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupervisorUserIdSource = tmpEntity;
				else
					entity.SupervisorUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, (entity.SupervisorUserId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupervisorUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupervisorUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.SupervisorUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupervisorUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.UsersProcurement object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.UsersProcurement instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UsersProcurement Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.UsersProcurement entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SupervisorUserIdSource
			if (CanDeepSave(entity, "Users|SupervisorUserIdSource", deepSaveType, innerList) 
				&& entity.SupervisorUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.SupervisorUserIdSource);
				entity.SupervisorUserId = entity.SupervisorUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UsersProcurementChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.UsersProcurement</c>
	///</summary>
	public enum UsersProcurementChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at SupervisorUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
		}
	
	#endregion UsersProcurementChildEntityTypes
	
	#region UsersProcurementFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UsersProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementFilterBuilder : SqlFilterBuilder<UsersProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilterBuilder class.
		/// </summary>
		public UsersProcurementFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementFilterBuilder
	
	#region UsersProcurementParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UsersProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurement"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersProcurementParameterBuilder : ParameterizedSqlFilterBuilder<UsersProcurementColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementParameterBuilder class.
		/// </summary>
		public UsersProcurementParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UsersProcurementParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UsersProcurementParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UsersProcurementParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UsersProcurementParameterBuilder
	
	#region UsersProcurementSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UsersProcurementColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersProcurement"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UsersProcurementSortBuilder : SqlSortBuilder<UsersProcurementColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersProcurementSqlSortBuilder class.
		/// </summary>
		public UsersProcurementSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UsersProcurementSortBuilder
	
} // end namespace
