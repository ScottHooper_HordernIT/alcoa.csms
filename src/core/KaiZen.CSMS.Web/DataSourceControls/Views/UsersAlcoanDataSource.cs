﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.UsersAlcoanProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(UsersAlcoanDataSourceDesigner))]
	public class UsersAlcoanDataSource : ReadOnlyDataSource<UsersAlcoan>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanDataSource class.
		/// </summary>
		public UsersAlcoanDataSource() : base(new UsersAlcoanService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the UsersAlcoanDataSourceView used by the UsersAlcoanDataSource.
		/// </summary>
		protected UsersAlcoanDataSourceView UsersAlcoanView
		{
			get { return ( View as UsersAlcoanDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the UsersAlcoanDataSourceView class that is to be
		/// used by the UsersAlcoanDataSource.
		/// </summary>
		/// <returns>An instance of the UsersAlcoanDataSourceView class.</returns>
		protected override BaseDataSourceView<UsersAlcoan, Object> GetNewDataSourceView()
		{
			return new UsersAlcoanDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the UsersAlcoanDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class UsersAlcoanDataSourceView : ReadOnlyDataSourceView<UsersAlcoan>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UsersAlcoanDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the UsersAlcoanDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public UsersAlcoanDataSourceView(UsersAlcoanDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal UsersAlcoanDataSource UsersAlcoanOwner
		{
			get { return Owner as UsersAlcoanDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal UsersAlcoanService UsersAlcoanProvider
		{
			get { return Provider as UsersAlcoanService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region UsersAlcoanDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the UsersAlcoanDataSource class.
	/// </summary>
	public class UsersAlcoanDataSourceDesigner : ReadOnlyDataSourceDesigner<UsersAlcoan>
	{
	}

	#endregion UsersAlcoanDataSourceDesigner

	#region UsersAlcoanFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAlcoan"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAlcoanFilter : SqlFilter<UsersAlcoanColumn>
	{
	}

	#endregion UsersAlcoanFilter

	#region UsersAlcoanExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UsersAlcoan"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UsersAlcoanExpressionBuilder : SqlExpressionBuilder<UsersAlcoanColumn>
	{
	}
	
	#endregion UsersAlcoanExpressionBuilder		
}

