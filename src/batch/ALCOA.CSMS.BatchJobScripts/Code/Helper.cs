using System;
using System.Net.Mail;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.Library;

using System.Configuration;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using ALCOA.CSMS.Batch.Common.Log;
using System.Web.Mvc;
using System.Collections.Generic;


namespace ALCOA.CSMS.BatchJobScripts
{
    public class Helper
    {
        /// <summary>
        /// Summary description for Configuration
        /// </summary>
        public class Configuration
        {
            public static string GetValue(ConfigList Key)
            {
                ConfigService configService = new ConfigService();
                TList<Config> configList = configService.GetAll(); //All KPI
                TList<Config> configList2 = configList.FindAll( //Find KPI We Need
                    delegate(Config config)
                    {
                        return
                            config.Key == Key.ToString();
                    }
                );

                return configList2[0].Value.ToString();
            }
        }

        /// <summary>
        /// Summary description for EmailAgent
        /// http://weblogs.asp.net/dfindley/archive/2006/04/23/Migrating-from-System.Web.Mail-to-System.Net.Mail.aspx
        /// </summary>
        public class EmailAgent
        {
            //Cindi Thornton 8/9/2015 - Ash Goldstraw with replace the calling code to use LogEmail.
            //public static bool notifyEHSConsultant(int CompanyId, string[] cc, string[] bcc, string subject, string body, string attachmentPath)
            //{
            //    try
            //    {
            //        string[] to = { getEHSConsultantEmail(CompanyId) };
            //        return sendEmail(to, cc, bcc, subject, body, false, attachmentPath);
            //    }
            //    catch (Exception)
            //    {
            //        return false;
            //    }
            //}

            //Cindi Thornton 8/9/2015 - replace with LogEmail calls. Emails now sent via a batch script.
            //public static bool sendEmail(string[] to, string[] cc, string[] bcc, string subject, string body, bool IsBodyHtml, string attachmentPath)
            //{
            //    try
            //    {
            //        string fromEmail = Configuration.GetValue(ConfigList.ContactEmail);
            //        string fromName = "AUA Alcoa Contractor Services Team";
            //        string mailServer = Configuration.GetValue(ConfigList.MailServer);

            //        using (MailMessage mail = new MailMessage())
            //        {
            //            mail.From = new MailAddress(fromEmail, fromName);
            //            if (to != null)
            //                foreach (string s in to)
            //                    if (!String.IsNullOrEmpty(s)) mail.To.Add(s);
            //            if (cc != null)
            //                foreach (string s in cc)
            //                    if(!String.IsNullOrEmpty(s)) mail.CC.Add(s);
            //            if (bcc != null)
            //                foreach (string s in bcc)
            //                    if (!String.IsNullOrEmpty(s)) mail.Bcc.Add(s);
            //            if (!String.IsNullOrEmpty(attachmentPath))
            //                if (System.IO.File.Exists(attachmentPath))
            //                    mail.Attachments.Add(new Attachment(attachmentPath));
            //            mail.Subject = subject;
            //            mail.Body = body;
            //            mail.IsBodyHtml = IsBodyHtml;
            //            SmtpClient smtp = new SmtpClient(mailServer);
            //            //SmtpClient smtp = new SmtpClient();
            //            smtp.Send(mail);
            //        }
            //        return true;
            //    }
            //    catch (Exception)
            //    {
            //        return false;
            //    }
            //}

            private static string getEHSConsultantEmail(int CompanyId)
            {
                Configuration configuration = new Configuration();
                string defaultEmail = Configuration.GetValue(ConfigList.DefaultEhsConsultant_WAO);
                //TODO: Vic Ops
                try
                {
                    CompaniesService companiesService = new CompaniesService();
                    Companies c = companiesService.GetByCompanyId(CompanyId);

                    EhsConsultantService ehsconsultantService = new EhsConsultantService();
                    int ehsConsultantId = -1;
                    ehsConsultantId = Convert.ToInt32(c.EhsConsultantId);
                    EhsConsultant ehsconsultant = ehsconsultantService.GetByEhsConsultantId(ehsConsultantId);

                    UsersService usersService = new UsersService();
                    Users u = usersService.GetByUserId(ehsconsultant.UserId);
                    if (!String.IsNullOrEmpty(u.Email))
                    {
                        return u.Email;
                    }
                    else
                    {
                        return defaultEmail;
                    }
                }
                catch (Exception)
                {
                    return defaultEmail;
                }
            }

            //Cindi Thornton - use the EntityFramework Insert for EmailLog instead of this.
            ////Cindi Thornton 4/9/2015 - modified call to logEmail for new columns IsBodyHtml & attachmentPath.
            //public static bool logEmail(string[] to, string[] cc, string[] bcc, string subject, string body, int emailLogTypeId, bool IsBodyHtml, string attachmentPath)
            //{
            //    try
            //    {
            //        var emailLogService = DependencyResolver.Current.GetService<Repo.CSMS.Service.Database.IEmailLogService>();
            //        Repo.CSMS.DAL.EntityModels.EmailLog el = new Repo.CSMS.DAL.EntityModels.EmailLog();

            //        el.EmailLogTypeId = emailLogTypeId;
            //        el.EmailDateTime = DateTime.Now;

            //        el.EmailTo = Helper.General.ConvertStringArrayToString(to);
            //        if (!string.IsNullOrEmpty(Helper.General.ConvertStringArrayToString(cc)))
            //            el.EmailCc = Helper.General.ConvertStringArrayToString(cc);
            //        if (!string.IsNullOrEmpty(Helper.General.ConvertStringArrayToString(bcc)))
            //            el.EmailBcc = Helper.General.ConvertStringArrayToString(bcc);

            //        el.EmailLogMessageSubject = subject;

            //        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //        el.EmailLogMessageBody = (byte[])encoding.GetBytes(body);

            //        el.IsHTML = IsBodyHtml;
            //        el.Handled = false;
            //        el.Attachments = attachmentPath;

            //        emailLogService.Insert(el);



            //        //EmailLogService elService = new EmailLogService();
            //        //EmailLog el = new EmailLog();
            //        //el.EmailLogTypeId = (int)eltl;
            //        //el.EmailDateTime = DateTime.Now;


            //        //el.EmailTo = Helper.General.ConvertStringArrayToString(to);
            //        //if (!string.IsNullOrEmpty(Helper.General.ConvertStringArrayToString(cc)))
            //        //    el.EmailCc = Helper.General.ConvertStringArrayToString(cc);
            //        //if (!string.IsNullOrEmpty(Helper.General.ConvertStringArrayToString(bcc)))
            //        //    el.EmailBcc = Helper.General.ConvertStringArrayToString(bcc);

            //        //el.EmailLogMessageSubject = subject;

            //        //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //        //el.EmailLogMessageBody = (byte[])encoding.GetBytes(body);

            //        //elService.Save(el);

            //        return true;
            //    }
            //    catch(Exception)
            //    {
            //        return false;
            //    }
            //}
        }

        public class General
        {
            //Cindi Thornton 11/9/15 - this has been moved to the Repo
            //public static string ConvertStringArrayToString(string[] array)
            //{
            //    //
            //    // Concatenate all the elements into a StringBuilder.
            //    //
            //    if (array == null) return "";

            //    int count = 0;
            //    foreach (string c in array)
            //    {
            //        count++;
            //    }
                

            //    StringBuilder builder = new StringBuilder();
            //    foreach (string value in array)
            //    {
            //        builder.Append(value);
            //        if (count > 1) builder.Append(' ');
            //    }
            //    return builder.ToString();
            //}

            /// <summary>
            /// Trims the string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static string TrimString(string str)
            {
                try
                {
                    string pattern = @"^[ \t]+|[ \t]+$";
                    Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
                    str = reg.Replace(str, "");
                    return str;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public class Radar
        {
            public static DataTable CreateDataTable(int _CompanyId, int _SiteId, int No, int? _CategoryId)
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("KpiMeasure", typeof(string));
                dt.Columns.Add("PlanYtd", typeof(string));

                if (_SiteId < 0)
                {
                    CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                    RegionsSitesService rService = new RegionsSitesService();
                    SitesService sService = new SitesService();

                    TList<RegionsSites> rsTlist = rService.GetByRegionId(-1 * _SiteId);
                    TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_CompanyId);

                    foreach (RegionsSites rs in rsTlist)
                    {
                        foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                        {
                            if (cscs.SiteId == rs.SiteId)
                            {
                                bool cont = true;
                                if (_CategoryId != null)
                                {
                                    if (cscs.CompanySiteCategoryId != _CategoryId) cont = false;
                                }

                                if (cont)
                                {
                                    Sites s = sService.GetBySiteId(rs.SiteId);
                                    if (s == null) throw new Exception("Error Occurred whilst Loading Site Columns");
                                    string SiteName = s.SiteName;
                                    if (SiteName.Length > 10) SiteName = s.SiteAbbrev;
                                    dt.Columns.Add(String.Format("PlanYtd{0}", SiteName), typeof(string));
                                    dt.Columns.Add(String.Format("KpiScoreYtd{0}", SiteName), typeof(string));
                                }
                            }
                        }
                    }
                }
                else
                {
                    dt.Columns.Add("KpiScoreYtd", typeof(string));
                    dt.Columns.Add("KpiScoreYtdPercentage", typeof(string));

                    if (No == 2)
                    {
                        dt.Columns.Add("PlanMonth", typeof(string));
                        for (int i = 1; i <= 12; i++) dt.Columns.Add(String.Format("KpiScoreMonth{0}", i), typeof(string));
                        for (int i = 1; i <= 12; i++) dt.Columns.Add(String.Format("KpiScoreMonth{0}Percentage", i), typeof(string));

                    }
                }

                return dt;
            }
            public static DataRow CreateDataRow_DataSet1_Summary(DataTable dt, string KpiMeasure, int _CompanyId, int _SiteId, int _MonthId, int _Year, int _CategoryId)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                DataRow dr = dt.NewRow();

                string KpiScoreYtd = String.Empty;


                if (KpiMeasure == "Residential Category")
                {
                    CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(_CategoryId);
                    KpiScoreYtd = csc.CategoryDesc;
                }
                else
                {
                    KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
                    DataSet ds;
                    if (_MonthId > 0)
                    {
                        ds = kcscService.Compliance_Table_SS_Month(_CompanyId, _SiteId, _MonthId, _Year, _CategoryId.ToString()); //Month
                    }
                    else
                    {
                        ds = kcscService.Compliance_Table_SS(_CompanyId, _SiteId, _Year, _CategoryId.ToString()); //Year
                    }
                    if (ds.Tables[0] != null)
                    {
                        foreach (DataRow drKpi in ds.Tables[0].Rows)
                        {
                            if (KpiMeasure == "Average No. Of People On Site" || KpiMeasure == "Total Man Hours")
                            {
                                int num = 0;
                                Int32.TryParse(drKpi[KpiMeasure].ToString(), out num);
                                KpiScoreYtd = num.ToString("#,##0");
                            }
                            else if (KpiMeasure == "LWDFR" || KpiMeasure == "TRIFR" || KpiMeasure == "AIFR")
                            {
                                KpiScoreYtd = drKpi[KpiMeasure].ToString();
                            }
                        }
                    }
                    else
                    {
                        KpiScoreYtd = "n/a";
                    }
                }

                dr["KpiMeasure"] = KpiMeasure;
                dr["KpiScoreYtd"] = KpiScoreYtd;

                return dr;
            }
            public static DataRow CreateDataRow_DataSet2_Ytd(DataTable dt, string KpiMeasure, int _CompanyId, int _SiteId, int _MonthId, int _Year, int _CategoryId, List<Repo.CSMS.DAL.EntityModels.Kpi> kpi)
            {
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
                VList<KpiCompanySiteCategory> kcscVlist = kcscService.GetByCompanyIdSiteIdYear(_CompanyId, _SiteId, _Year);

                SitesService sService = new SitesService();//30/11/2015 moved to improve performance, only get it once
                Sites s = sService.GetBySiteId(_SiteId);//30/11/2015 moved to improve performance, only get it once

                DataRow dr = dt.NewRow();

                string PlanYtd = "-";
                string PlanMonth = "-";
                string KpiScoreYtd = "-";
                string KpiScoreYtdPercentage = "-";
                string[] KpiScoreMonth = new string[13];
                string[] KpiScoreMonthPercentage = new string[13];

                for (int i = 1; i <= 12; i++) KpiScoreMonth[i] = "-";
                for (int i = 1; i <= 12; i++) KpiScoreMonthPercentage[i] = "-";

                AnnualTargetsService atService = new AnnualTargetsService();
                //Sayani
                AnnualTargets at = atService.GetByYearCompanySiteCategoryId(_Year, _CategoryId);

                CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(_CategoryId);
                //If Can't Find Annual Targets. Try to Find and Use a Previous Year's one...
                if (at == null)
                {
                    //find latest
                    int count = 0;
                    AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                    atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, _CategoryId.ToString());
                    //Changes done for DT#2681
                    atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, _Year.ToString());

                    TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                    atFilters.ToString(), "Year DESC", 0, 100, out count);
                    if (count > 0)
                    {
                        at = atList[0].Copy();
                    }
                    else
                    {
                        throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                    }
                }

                DateTime dtNowMinusOneMonth = DateTime.Now;
                if (_MonthId == 0)
                {
                    if (DateTime.Now.Month != 1)
                    {
                        dtNowMinusOneMonth = dtNowMinusOneMonth.AddMonths(-1);
                    }

                    if (_Year < DateTime.Now.Year) dtNowMinusOneMonth = new DateTime(_Year, 12, 1);
                }
                else
                {
                    dtNowMinusOneMonth = new DateTime(_Year, _MonthId, 1);
                }


                int upto = 12;
                if (_Year == DateTime.Now.Year) upto = dtNowMinusOneMonth.Month;
                int NoMonths = GetCountMonthsExpectedOnSite(_CompanyId, _SiteId, upto, _Year, kpi);
                if (KpiMeasure != "On site")
                {
                    for (int i = 1; i <= upto; i++)
                    {
                        //switch (ExpectedOnSite(_CompanyId, _SiteId, i, _Year))
                        switch (ExpectedOnSite(_CompanyId, _SiteId, i, _Year, kpi, s.SiteNameEbi)) //30/11/2015 changed to improve performance, less hits on the database
                        {
                            case true: //On Site
                                KpiScoreMonthPercentage[i] = "0";
                                KpiScoreMonth[i] = "0";
                                break;
                            case false: //Not On Site
                                KpiScoreMonthPercentage[i] = "-";
                                KpiScoreMonth[i] = "-";
                                break;
                            default: //???
                                KpiScoreMonthPercentage[i] = "-";
                                KpiScoreMonth[i] = "-";
                                break;
                        }
                    }
                }
                switch (KpiMeasure)
                {
                    #region "On site"
                    case "On site":
                        PlanYtd = "-";
                        PlanMonth = "-";

                        //SitesService sService = new SitesService();
                        //Sites s = sService.GetBySiteId(_SiteId);

                        for (int i = 1; i <= upto; i++) KpiScoreMonth[i] = "-";

                        if (!String.IsNullOrEmpty(s.SiteNameEbi))
                        {
                            for (int i = 1; i <= upto; i++) KpiScoreMonth[i] = "No";
                        }

                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "Yes";
                        }

                        break;
                    #endregion
                    #region "CSA"
                    case "Alcoa Annual Audit Score (%)":
                        PlanYtd = "0";
                        PlanMonth = "0";
                        //KpiScoreYtd = "0";
                        KpiScoreYtdPercentage = "-"; //Not Calculated in Compliance %
                        for (int i = 1; i <= upto; i++) KpiScoreMonthPercentage[i] = "-"; //Not Calculated in Compliance %

                        if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                        PlanMonth = PlanYtd;

                        if (NoMonths > 0)
                        {
                            KpiScoreYtd = GetCsaScore(_CompanyId, _SiteId, 5, _Year);

                            for (int i = 1; i <= 12; i++)
                            {
                                KpiScoreMonth[i] = KpiScoreYtd;
                            }
                        }

                        break;

                    case "CSA - Self Audit (% Score) - Current Qtr":
                        PlanYtd = "0";
                        if (NoMonths > 0) KpiScoreYtd = "0";
                        KpiScoreYtdPercentage = "-"; //Not Calculated in Compliance %
                        for (int i = 1; i <= upto; i++) KpiScoreMonthPercentage[i] = "-"; //Not Calculated in Compliance %

                        if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                        PlanMonth = PlanYtd;

                        bool cont = false;
                        int QtrId = 0;
                        if (dtNowMinusOneMonth.Month >= 1)
                        {
                            QtrId = 1;
                            if (ExpectedOnSite(_CompanyId, _SiteId, 1, _Year, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 2, _Year, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 3, _Year, kpi, s.SiteNameEbi) == true)
                            {
                                cont = true;
                            }
                            if (dtNowMinusOneMonth.Month >= 4)
                            {
                                QtrId = 2;
                                if (ExpectedOnSite(_CompanyId, _SiteId, 4, _Year, kpi, s.SiteNameEbi) == true ||
                                    ExpectedOnSite(_CompanyId, _SiteId, 5, _Year, kpi, s.SiteNameEbi) == true ||
                                    ExpectedOnSite(_CompanyId, _SiteId, 6, _Year, kpi, s.SiteNameEbi) == true)
                                {
                                    cont = true;
                                }
                                if (dtNowMinusOneMonth.Month >= 7)
                                {
                                    QtrId = 3;
                                    if (ExpectedOnSite(_CompanyId, _SiteId, 7, _Year, kpi, s.SiteNameEbi) == true ||
                                        ExpectedOnSite(_CompanyId, _SiteId, 8, _Year, kpi, s.SiteNameEbi) == true ||
                                        ExpectedOnSite(_CompanyId, _SiteId, 9, _Year, kpi, s.SiteNameEbi) == true)
                                    {
                                        cont = true;
                                    }
                                    if (dtNowMinusOneMonth.Month >= 10)
                                    {
                                        QtrId = 4;
                                        if (ExpectedOnSite(_CompanyId, _SiteId, 10, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 11, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 12, _Year, kpi, s.SiteNameEbi) == true)
                                        {
                                            cont = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (cont)
                        {
                            KpiScoreYtd = GetCsaScore(_CompanyId, _SiteId, QtrId, _Year);

                            int MonthStart = 0;
                            for (int i = 1; i <= upto; i = i + 3)
                            {
                                if (i >= 1)
                                {
                                    if (ExpectedOnSite(_CompanyId, _SiteId, 1, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 2, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 3, _Year, kpi, s.SiteNameEbi) == true)
                                    {
                                        MonthStart = 1;
                                        KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _SiteId, 1, _Year);
                                        if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-") KpiScoreMonthPercentage[MonthStart] = "100";
                                        KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                        KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                        KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                        KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                    }
                                    if (i >= 4)
                                    {
                                        if (ExpectedOnSite(_CompanyId, _SiteId, 4, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 5, _Year, kpi, s.SiteNameEbi) == true ||
                                            ExpectedOnSite(_CompanyId, _SiteId, 6, _Year, kpi, s.SiteNameEbi) == true)
                                        {
                                            MonthStart = 4;
                                            KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _SiteId, 2, _Year);
                                            if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-") KpiScoreMonthPercentage[MonthStart] = "100";
                                            KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                            KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                            KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                        }
                                        if (i >= 7)
                                        {
                                            if (ExpectedOnSite(_CompanyId, _SiteId, 7, _Year, kpi, s.SiteNameEbi) == true ||
                                                ExpectedOnSite(_CompanyId, _SiteId, 8, _Year, kpi, s.SiteNameEbi) == true ||
                                                ExpectedOnSite(_CompanyId, _SiteId, 9, _Year, kpi, s.SiteNameEbi) == true)
                                            {
                                                MonthStart = 7;
                                                KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _SiteId, 3, _Year);
                                                if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-") KpiScoreMonthPercentage[MonthStart] = "100";
                                                KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                                KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                                KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                                KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                            }
                                            if (i >= 10)
                                            {
                                                if (ExpectedOnSite(_CompanyId, _SiteId, 10, _Year, kpi, s.SiteNameEbi) == true ||
                                                    ExpectedOnSite(_CompanyId, _SiteId, 11, _Year, kpi, s.SiteNameEbi) == true ||
                                                    ExpectedOnSite(_CompanyId, _SiteId, 12, _Year, kpi, s.SiteNameEbi) == true)
                                                {
                                                    MonthStart = 10;
                                                    KpiScoreMonth[MonthStart] = GetCsaScore(_CompanyId, _SiteId, 4, _Year);
                                                    if (KpiScoreMonth[MonthStart] != "0" && KpiScoreMonth[MonthStart] != "-") KpiScoreMonthPercentage[MonthStart] = "100";
                                                    KpiScoreMonth[MonthStart + 1] = KpiScoreMonth[MonthStart];
                                                    KpiScoreMonthPercentage[MonthStart + 1] = KpiScoreMonthPercentage[MonthStart];
                                                    KpiScoreMonth[MonthStart + 2] = KpiScoreMonth[MonthStart];
                                                    KpiScoreMonthPercentage[MonthStart + 2] = KpiScoreMonthPercentage[MonthStart];
                                                }
                                            }
                                        }
                                    }
                                }


                            }
                        }

                        break;
                    case "CSA - Self Audit (% Score) - Previous Qtr":
                        PlanYtd = "0";
                        //KpiScoreYtd = "0";
                        //KpiScoreYtdPercentage = "0";

                        if (at.QuarterlyAuditScore > 0) PlanYtd = at.QuarterlyAuditScore.ToString();
                        PlanMonth = PlanYtd;

                        int _QtrId = 0;
                        if (dtNowMinusOneMonth.Month >= 1)
                        {
                            _QtrId = 1;
                            if (dtNowMinusOneMonth.Month >= 4)
                            {
                                _QtrId = 2;
                                if (dtNowMinusOneMonth.Month >= 7)
                                {
                                    _QtrId = 3;
                                    if (dtNowMinusOneMonth.Month >= 10)
                                    {
                                        _QtrId = 4;
                                    }
                                }
                            }
                        }
                        int SearchYear = _Year;
                        if (_QtrId == 1)
                        {
                            _QtrId = 4;
                            SearchYear = _Year - 1;
                        }
                        else if (_QtrId == 4 && SearchYear != DateTime.Now.Year)
                        {
                            //do nothing.
                        }
                        else
                        {
                            _QtrId = _QtrId - 1;
                        }

                        int Month1 = 10;
                        int Month2 = 11;
                        int Month3 = 12;
                        switch (_QtrId)
                        {
                            case 1:
                                Month1 = 1;
                                Month2 = 2;
                                Month3 = 3;
                                break;
                            case 2:
                                Month1 = 4;
                                Month2 = 5;
                                Month3 = 6;
                                break;
                            case 3:
                                Month1 = 7;
                                Month2 = 8;
                                Month3 = 9;
                                break;
                            case 4:
                                Month1 = 10;
                                Month2 = 11;
                                Month3 = 12;
                                break;
                            default:
                                break;
                        }

                        if (ExpectedOnSite(_CompanyId, _SiteId, Month1, SearchYear, kpi, s.SiteNameEbi) == true ||
                            ExpectedOnSite(_CompanyId, _SiteId, Month2, SearchYear, kpi, s.SiteNameEbi) == true ||
                            ExpectedOnSite(_CompanyId, _SiteId, Month3, SearchYear, kpi, s.SiteNameEbi) == true)
                        {
                            KpiScoreYtdPercentage = "0";
                            KpiScoreYtd = GetCsaScore(_CompanyId, _SiteId, _QtrId, SearchYear);
                            if (KpiScoreYtd != "0") KpiScoreYtdPercentage = "100";

                            for (int i = 1; i <= upto; i++)
                            {
                                int _QtrId2 = 0;
                                int _SearchYear = _Year;
                                if (i >= 1)
                                {
                                    _QtrId2 = 4;
                                    _SearchYear = _Year - 1;
                                    if (i >= 4)
                                    {
                                        _QtrId2 = 1;
                                        if (i >= 7)
                                        {
                                            _QtrId2 = 2;
                                            if (i >= 10)
                                            {
                                                _QtrId2 = 3;
                                            }
                                        }
                                    }
                                }

                                KpiScoreMonth[i] = GetCsaScore(_CompanyId, _SiteId, _QtrId2, _SearchYear);
                                KpiScoreMonth[i] = GetCsaScore(_CompanyId, _SiteId, _QtrId2, _SearchYear);
                                KpiScoreMonthPercentage[i] = "0";
                                if (KpiScoreMonth[i] != "0" && KpiScoreMonth[i] != "-") KpiScoreMonthPercentage[i] = "100";
                            }

                        }
                        break;

                    #endregion
                    #region "IFE : Injury Ratio"
                    case "IFE : Injury Ratio":
                        //Sayani
                        ALCOA.CSMS.BatchJobScripts.Configuration config = new ALCOA.CSMS.BatchJobScripts.Configuration();

                        //Change By Sayani for task# 1
                        PlanYtd = at.IFEInjuryRatioTargetYTD.ToString();
                        PlanMonth = at.IFEInjuryRatioTargetMon.ToString();
                        //PlanYtd = config.GetValue(ConfigList.IfeInjuryRatioYtd);
                        //PlanMonth = config.GetValue(ConfigList.IfeInjuryRatioMonth);

                        //End

                        if (NoMonths > 0) KpiScoreYtd = "0";

                        int YTDipFATI = 0;
                        int YTDipMTI = 0;
                        int YTDipRDI = 0;
                        int YTDipLTI = 0;
                        int YTDipIFE = 0;
                        int[] ipFATI = new int[13];
                        int[] ipMTI = new int[13];
                        int[] ipRDI = new int[13];
                        int[] ipLTI = new int[13];
                        int[] ipIFE = new int[13];

                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.KpiDateTime.Month <= upto)
                            {
                                if (kcsc.IpFati != null)
                                {
                                    YTDipFATI += Convert.ToInt32(kcsc.IpFati);
                                    ipFATI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                                }
                                if (kcsc.IpMti != null)
                                {
                                    YTDipMTI += Convert.ToInt32(kcsc.IpMti);
                                    ipMTI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                                }
                                if (kcsc.IpRdi != null)
                                {
                                    YTDipRDI += Convert.ToInt32(kcsc.IpRdi);
                                    ipRDI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                                }
                                if (kcsc.IpLti != null)
                                {
                                    YTDipLTI += Convert.ToInt32(kcsc.IpLti);
                                    ipLTI[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpFati);
                                }
                                if (kcsc.IpIfe != null)
                                {
                                    YTDipIFE += Convert.ToInt32(kcsc.IpIfe);
                                    ipIFE[kcsc.KpiDateTime.Month] = Convert.ToInt32(kcsc.IpIfe);
                                }
                            }
                        }

                        for (int i = 1; i <= 12; i++)
                        {
                            if (ExpectedOnSite(_CompanyId, _SiteId, i, _Year, kpi, s.SiteNameEbi) == true)
                            {
                                KpiScoreMonth[i] = "0";
                                if ((ipFATI[i] + ipMTI[i] + ipRDI[i] + ipLTI[i]) == 0)
                                {
                                    KpiScoreMonth[i] = Convert.ToDecimal(ipIFE[i]).ToString();
                                }
                                else
                                {
                                    KpiScoreMonth[i] = Decimal.Round(Convert.ToDecimal(ipIFE[i]) /
                                                                        Convert.ToDecimal(ipFATI[i] + ipMTI[i] + ipRDI[i] + ipLTI[i]), 2).ToString();
                                }
                            }
                        }
                        if (YTDipIFE > 0)
                        {

                            if ((YTDipFATI + YTDipMTI + YTDipRDI + YTDipLTI) == 0)
                            {
                                KpiScoreYtd = Convert.ToDecimal(YTDipIFE).ToString();
                            }
                            else
                            {
                                Decimal Score = 0;
                                for (int i = 1; i <= upto; i++)
                                {
                                    Decimal _Score = 0;
                                    Decimal.TryParse(KpiScoreMonth[i], out _Score);
                                    Score += _Score;
                                }
                                if (Score > 0)
                                {
                                    KpiScoreYtd = Decimal.Round(Convert.ToDecimal(Score / Convert.ToDecimal(NoMonths)), 2).ToString();
                                }
                            }
                        }

                        break;
                    #endregion
                    #region "Workplace Safety & Compliance"
                    case "Workplace Safety & Compliance":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.WorkplaceSafetyCompliance))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.WorkplaceSafetyCompliance))).ToString();

                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.IWsc.ToString();
                        }

                        break;
                    #endregion
                    #region "Management Health Safety Work Contacts"
                    case "Management Health Safety Work Contacts":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.HealthSafetyWorkContacts))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.HealthSafetyWorkContacts))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.ONoHswc.ToString();

                            int Score = 0;
                            int.TryParse(KpiScoreYtd, out Score);

                            int _Score = 0;
                            int.TryParse(KpiScoreMonth[kcsc.KpiDateTime.Month], out _Score);

                            KpiScoreYtd = (Score + _Score).ToString();
                        }

                        break;
                    #endregion
                    #region "Behavioural Observations (%)"
                    case "Behavioural Observations (%)":
                        PlanYtd = "100";
                        PlanMonth = "100";

                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.ONoBsp != null)
                            {
                                Decimal Divider = Convert.ToDecimal(at.BehaviouralSafetyProgram) * Convert.ToDecimal(kcsc.AheaAvgNopplSiteMonth);
                                if (Divider != 0)
                                {
                                    if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0)
                                        KpiScoreMonth[kcsc.KpiDateTime.Month] = Decimal.Round((Convert.ToDecimal(kcsc.ONoBsp / Divider) * 100)).ToString();
                                }
                                else
                                {
                                    if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = "";
                                }
                            }
                        }

                        break;
                    #endregion
                    #region "Fatality Prevention Program"
                    case "Fatality Prevention Program":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.FatalityPrevention))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.FatalityPrevention))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MFatality.ToString();
                        }

                        break;
                    #endregion
                    #region "JSA Field Audit Verifications"
                    case "JSA Field Audit Verifications":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.JsaScore))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.JsaScore))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.JsaAudits.ToString();
                        }

                        break;
                    #endregion
                    #region "Toolbox Meetings"
                    case "Toolbox Meetings":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.ToolboxMeetingsPerMonth))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.ToolboxMeetingsPerMonth))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MTbmpm.ToString();
                        }

                        break;
                    #endregion
                    #region "Alcoa Weekly Contractors Meeting"
                    case "Alcoa Weekly Contractors Meeting":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.AlcoaWeeklyContractorsMeeting))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.AlcoaWeeklyContractorsMeeting))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MAwcm.ToString();
                        }

                        break;
                    #endregion
                    #region "Alcoa Monthly Contractors Meeting"
                    case "Alcoa Monthly Contractors Meeting":
                        PlanYtd = Decimal.Round(((NoMonths / 12.0m) * Convert.ToDecimal(at.AlcoaMonthlyContractorsMeeting))).ToString();
                        PlanMonth = Decimal.Round(((1 / 12.0m) * Convert.ToDecimal(at.AlcoaMonthlyContractorsMeeting))).ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {
                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.MAmcm.ToString();
                        }

                        break;
                    #endregion
                    #region "Safety Plan(s) Submitted"
                    case "Safety Plan(s) Submitted":
                        PlanYtd = at.SafetyPlan.ToString();
                        PlanMonth = PlanYtd;
                        //Count # Safety Plans submitted for year
                        FileDbService fService = new FileDbService();
                        DataSet dsSafetyPlans = fService.ComplianceReport_GetByByCompanyIdYear(_CompanyId, dtNowMinusOneMonth.Year);
                        if (dsSafetyPlans.Tables[0].Rows.Count == 1)
                        {
                            if (dsSafetyPlans.Tables[0].Rows[0].ItemArray[0] != null)
                            {
                                KpiScoreYtd = dsSafetyPlans.Tables[0].Rows[0].ItemArray[0].ToString();
                            }
                        }

                        for (int i = 1; i <= 12; i++) KpiScoreMonth[i] = KpiScoreYtd;

                        break;
                    #endregion
                    #region "Mandated Training"
                    case "% Mandated Training":
                        PlanYtd = at.Training.ToString();
                        PlanMonth = at.Training.ToString();
                        foreach (KpiCompanySiteCategory kcsc in kcscVlist)
                        {

                            if (kcsc.EbiOnSite == true || kcsc.AheaTotalManHours > 0) KpiScoreMonth[kcsc.KpiDateTime.Month] = kcsc.Training.ToString();
                        }
                        break;
                    #endregion

                    default:
                        #region "Training Schedules" / "Medical Schedules"
                        if (KpiMeasure == "Training Schedules" || KpiMeasure == "Medical Schedules")
                        {
                            PlanYtd = "1";
                            PlanMonth = PlanYtd;

                            int _SearchYear_TS = _Year;

                            for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}", i)] = "0";
                            for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}Percentage", i)] = "0";

                            FileDbMedicalTrainingService fdbmtService = new FileDbMedicalTrainingService();
                            string MSTS = "";
                            if (KpiMeasure == "Training Schedules") MSTS = "TS";
                            if (KpiMeasure == "Medical Schedules") MSTS = "MS";

                            DataSet dsFdbmt = fdbmtService.ComplianceReport_ByCompany(_SearchYear_TS, MSTS, _CompanyId, _SiteId, "%");

                            if (dsFdbmt.Tables[0] != null)
                            {
                                if (dsFdbmt.Tables[0].Rows.Count == 1)
                                {
                                    foreach (DataRow drFdbmt in dsFdbmt.Tables[0].Rows)
                                    {
                                        KpiScoreMonth[1] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonth[2] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonth[3] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonthPercentage[1] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonthPercentage[2] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonthPercentage[3] = drFdbmt["Qtr1"].ToString();
                                        KpiScoreMonth[4] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonth[5] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonth[6] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonth[7] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonth[8] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonth[9] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonth[10] = drFdbmt["Qtr4"].ToString();
                                        KpiScoreMonth[11] = drFdbmt["Qtr4"].ToString();
                                        KpiScoreMonth[12] = drFdbmt["Qtr4"].ToString();


                                        KpiScoreMonthPercentage[4] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonthPercentage[5] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonthPercentage[6] = drFdbmt["Qtr2"].ToString();
                                        KpiScoreMonthPercentage[7] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonthPercentage[8] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonthPercentage[9] = drFdbmt["Qtr3"].ToString();
                                        KpiScoreMonthPercentage[10] = drFdbmt["Qtr4"].ToString();
                                        KpiScoreMonthPercentage[11] = drFdbmt["Qtr4"].ToString();
                                        KpiScoreMonthPercentage[12] = drFdbmt["Qtr4"].ToString();
                                    }
                                }
                            }

                            if (ExpectedOnSite(_CompanyId, _SiteId, 1, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 2, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 3, _SearchYear_TS, kpi, s.SiteNameEbi) == true)
                            {

                            }
                            else
                            {
                                if (KpiScoreMonth[1].ToString() == "0") KpiScoreMonth[1] = "-";
                                if (KpiScoreMonth[2].ToString() == "0") KpiScoreMonth[2] = "-";
                                if (KpiScoreMonth[3].ToString() == "0") KpiScoreMonth[3] = "-";
                                if (KpiScoreMonthPercentage[1].ToString() == "0") KpiScoreMonthPercentage[1] = "-";
                                if (KpiScoreMonthPercentage[2].ToString() == "0") KpiScoreMonthPercentage[2] = "-";
                                if (KpiScoreMonthPercentage[3].ToString() == "0") KpiScoreMonthPercentage[3] = "-";
                            }
                            if (ExpectedOnSite(_CompanyId, _SiteId, 4, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 5, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 6, _SearchYear_TS, kpi, s.SiteNameEbi) == true)
                            {

                            }
                            else
                            {
                                if (KpiScoreMonth[4].ToString() == "0") KpiScoreMonth[4] = "-";
                                if (KpiScoreMonth[5].ToString() == "0") KpiScoreMonth[5] = "-";
                                if (KpiScoreMonth[6].ToString() == "0") KpiScoreMonth[6] = "-";
                                if (KpiScoreMonthPercentage[4].ToString() == "0") KpiScoreMonthPercentage[4] = "-";
                                if (KpiScoreMonthPercentage[5].ToString() == "0") KpiScoreMonthPercentage[5] = "-";
                                if (KpiScoreMonthPercentage[6].ToString() == "0") KpiScoreMonthPercentage[6] = "-";
                            }
                            if (ExpectedOnSite(_CompanyId, _SiteId, 7, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 8, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 9, _SearchYear_TS, kpi, s.SiteNameEbi) == true)
                            {
                                if (KpiScoreMonth[7].ToString() == "0") KpiScoreMonth[7] = "-";
                                if (KpiScoreMonth[8].ToString() == "0") KpiScoreMonth[8] = "-";
                                if (KpiScoreMonth[9].ToString() == "0") KpiScoreMonth[9] = "-";
                                if (KpiScoreMonthPercentage[7].ToString() == "0") KpiScoreMonthPercentage[7] = "-";
                                if (KpiScoreMonthPercentage[8].ToString() == "0") KpiScoreMonthPercentage[8] = "-";
                                if (KpiScoreMonthPercentage[9].ToString() == "0") KpiScoreMonthPercentage[9] = "-";
                            }
                            else
                            {
                            }
                            if (ExpectedOnSite(_CompanyId, _SiteId, 10, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 11, _SearchYear_TS, kpi, s.SiteNameEbi) == true ||
                                ExpectedOnSite(_CompanyId, _SiteId, 12, _SearchYear_TS, kpi, s.SiteNameEbi) == true)
                            {

                            }
                            else
                            {
                                if (KpiScoreMonth[10].ToString() == "0") KpiScoreMonth[10] = "-";
                                if (KpiScoreMonth[11].ToString() == "0") KpiScoreMonth[11] = "-";
                                if (KpiScoreMonth[12].ToString() == "0") KpiScoreMonth[12] = "-";
                                if (KpiScoreMonthPercentage[10].ToString() == "0") KpiScoreMonthPercentage[10] = "-";
                                if (KpiScoreMonthPercentage[11].ToString() == "0") KpiScoreMonthPercentage[11] = "-";
                                if (KpiScoreMonthPercentage[12].ToString() == "0") KpiScoreMonthPercentage[12] = "-";
                            }

                            if (dtNowMinusOneMonth.Month <= 9)
                            {
                                KpiScoreMonth[10] = "-";
                                KpiScoreMonth[11] = "-";
                                KpiScoreMonth[12] = "-";
                                KpiScoreMonthPercentage[10] = "-";
                                KpiScoreMonthPercentage[11] = "-";
                                KpiScoreMonthPercentage[12] = "-";
                                if (dtNowMinusOneMonth.Month <= 6)
                                {
                                    KpiScoreMonth[7] = "-";
                                    KpiScoreMonth[8] = "-";
                                    KpiScoreMonth[9] = "-";
                                    KpiScoreMonthPercentage[7] = "-";
                                    KpiScoreMonthPercentage[8] = "-";
                                    KpiScoreMonthPercentage[9] = "-";
                                    if (dtNowMinusOneMonth.Month <= 3)
                                    {
                                        KpiScoreMonth[4] = "-";
                                        KpiScoreMonth[5] = "-";
                                        KpiScoreMonth[6] = "-";
                                        KpiScoreMonthPercentage[4] = "-";
                                        KpiScoreMonthPercentage[5] = "-";
                                        KpiScoreMonthPercentage[6] = "-";
                                    }
                                }
                            }
                        }
                        #endregion
                        break;
                }

                #region "Sum/Avg YTD Score & Calculate Percentage"
                if (KpiMeasure != "On site" &&
                    KpiMeasure != "Alcoa Annual Audit Score (%)" &&
                    !KpiMeasure.Contains("CSA - Self Audit (% Score)") &&
                    KpiMeasure != "Management Health Safety Work Contacts" &&
                    KpiMeasure != "Safety Plan(s) Submitted" &&
                    KpiMeasure != "IFE : Injury Ratio")
                {
                    if (NoMonths > 0)
                    {

                        for (int i = 1; i <= upto; i++)
                        {
                            if (ExpectedOnSite(_CompanyId, _SiteId, i, _Year, kpi, s.SiteNameEbi) == true) //30/11/2015 changed to improve performance
                            {
                                Decimal Score = 0;
                                Decimal.TryParse(KpiScoreYtd, out Score);
                                Decimal _Score = 0;
                                Decimal.TryParse(KpiScoreMonth[i], out _Score);
                                KpiScoreYtd = (Score + _Score).ToString(); //SUM
                            }
                        }

                        if (KpiMeasure == "Behavioural Observations (%)" ||
                            KpiMeasure == "% Mandated Training") //AVERAGE
                        {
                            Decimal Score = 0;
                            Decimal.TryParse(KpiScoreYtd, out Score);
                            if (Score != 0)
                            {
                                KpiScoreYtd = Decimal.Round((Score / NoMonths)).ToString();
                            }
                            else
                            {
                                if (PlanYtd == "0") KpiScoreYtdPercentage = "100";
                            }
                        }
                    }
                }

                if (KpiMeasure != "On site" &&
                    KpiMeasure != "Alcoa Annual Audit Score (%)" &&
                    !KpiMeasure.Contains("CSA - Self Audit (% Score)"))
                {
                    if (PlanYtd != "0")
                    {
                        Decimal Top = 0;
                        Decimal Bottom = 0;
                        Decimal.TryParse(PlanYtd, out Bottom);
                        Decimal.TryParse(KpiScoreYtd, out Top);

                        if (Bottom != 0 && KpiScoreYtd != "-")
                        {
                            Decimal Score = (Top / Bottom) * 100;
                            if (Score > 100) Score = 100;
                            KpiScoreYtdPercentage = Decimal.Round(Score).ToString();
                        }
                    }
                    if (PlanMonth != "0")
                    {
                        for (int i = 1; i <= upto; i++)
                        {
                            Decimal Top = 0;
                            Decimal Bottom = 0;
                            Decimal.TryParse(PlanMonth, out Bottom);
                            Decimal.TryParse(KpiScoreMonth[i], out Top);

                            if (Bottom != 0 && KpiScoreMonth[i] != "-")
                            {
                                Decimal Score = (Top / Bottom) * 100;
                                if (Score > 100) Score = 100;
                                KpiScoreMonthPercentage[i] = Decimal.Round(Score).ToString();
                            }
                        }
                    }
                }
                #endregion


                dr["KpiMeasure"] = KpiMeasure;
                dr["PlanYtd"] = PlanYtd;
                dr["PlanMonth"] = PlanMonth;
                if (_MonthId == 0)
                {
                    dr["KpiScoreYtd"] = KpiScoreYtd;
                    dr["KpiScoreYtdPercentage"] = KpiScoreYtdPercentage;
                }
                else
                {
                    dr["PlanYtd"] = dr["PlanMonth"];
                    dr["KpiScoreYtd"] = KpiScoreMonth[_MonthId];
                    dr["KpiScoreYtdPercentage"] = KpiScoreMonthPercentage[_MonthId];
                }
                for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}", i)] = KpiScoreMonth[i];
                for (int i = 1; i <= 12; i++) dr[String.Format("KpiScoreMonth{0}Percentage", i)] = KpiScoreMonthPercentage[i];

                return dr;
            }


            public static string GetCsaScore(int _CompanyId, int _SiteId, int _QtrId, int _Year)
            {
                string TotalRating = "0";
                CsaService csaService = new CsaService();
                Csa csa = csaService.GetByCompanyIdSiteIdQtrIdYear(_CompanyId, _SiteId, _QtrId, _Year);
                if (csa != null)
                {
                    if (csa.TotalRating != null)
                    {
                        if (csa.TotalRating > 0) TotalRating = csa.TotalRating.ToString();
                    }
                }
                return TotalRating;
            }
            public static string GetRateScore(string rate, int _CompanyId, int _SiteId, int _CategoryId, int _MonthId, int _Year)
            {
                string RateScore = "";

                KpiCompanySiteCategoryService kcscService = new KpiCompanySiteCategoryService();
                DataSet dsKpiData;

                if (_MonthId == 0)
                {
                    dsKpiData = kcscService.Compliance_Table_SS(_CompanyId, _SiteId, _Year, _CategoryId.ToString());
                }
                else
                {
                    dsKpiData = kcscService.Compliance_Table_SS_Month(_CompanyId, _SiteId, _MonthId, _Year, _CategoryId.ToString());
                }

                foreach (DataRow dr in dsKpiData.Tables[0].Rows)
                {
                    RateScore = dr[rate].ToString();
                }

                return RateScore;
            }

            //30/11/15 Cindi Thornton - re-written to improve performance, added Kpi and siteNameEbi to improve performance, perfor it was going to the the database for these each time.
            // and this is call A LOT.
            public static bool? ExpectedOnSite(int _CompanyId, int _SiteId, int _MonthId, int _Year, List<Repo.CSMS.DAL.EntityModels.Kpi> _Kpi, string _SiteNameEbi) 
            {
                bool? _ExpectedOnSite = null;
                DateTime dtSearchMonthYear = new DateTime(_Year, _MonthId, 1);

                if (!String.IsNullOrEmpty(_SiteNameEbi))
                {
                    _ExpectedOnSite = false;
                }

                _Kpi = _Kpi.FindAll(x => x.kpiDateTime == dtSearchMonthYear && (x.EbiOnSite == true || x.aheaTotalManHours > 0));
                if (_Kpi.Count > 0)
                 {
                     _ExpectedOnSite = true;
                 }

                return _ExpectedOnSite;
            }

            public static int GetCountMonthsExpectedOnSite(int _CompanyId, int _SiteId, int upto, int _Year, List<Repo.CSMS.DAL.EntityModels.Kpi> _Kpi)
            {
                //bool? _ExpectedOnSite = null;

                int ExpectedCount = 0;

                //DT384 Cindi Thornton 30/11/15 replaced with code that doesn't hit the database. Data required now passed in to speed up execution.
                //SitesService sService = new SitesService();
                //Sites s = sService.GetBySiteId(_SiteId);

                //KpiService kService = new KpiService();

                //for (int i = 1; i <= upto; i++)
                //{
                //    DateTime dtSearchMonthYear = new DateTime(_Year, i, 1);
                //    Kpi k = kService.GetByCompanyIdSiteIdKpiDateTime(_CompanyId, _SiteId, dtSearchMonthYear);
                //    //if (!String.IsNullOrEmpty(s.SiteNameEbi))
                //    //{
                //    if (k != null)
                //    {
                //        if (k.EbiOnSite == true || k.AheaTotalManHours > 0)
                //        {
                //            ExpectedCount++;
                //        }
                //    }

                //    //}
                //}

                // get the records for all the months in one hit to the database to improve performance.
                DateTime dtStartMonthYear = new DateTime(_Year, 1, 1); 
                DateTime dtEndMonthYear = new DateTime(_Year, upto, 1);

                _Kpi = _Kpi.FindAll(x => x.kpiDateTime >= dtStartMonthYear && x.kpiDateTime <= dtEndMonthYear && (x.EbiOnSite == true || x.aheaTotalManHours > 0));

                ExpectedCount = _Kpi.Count;
                return ExpectedCount;
            }

            public static int[] GetResidentialCategories(int CompanyId, int SiteId)
            {
                int[] i = new int[1];
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                CompanySiteCategoryStandard cscs = cscsService.GetByCompanyIdSiteId(CompanyId, SiteId);

                bool error = false;

                if (cscs == null) error = true;
                else if (cscs.CompanySiteCategoryId == null) error = true;

                if (error)
                    throw new Exception("Residential Category not assigned for selected Company and Site.");
                else
                {
                    i[0] = Convert.ToInt32(cscs.CompanySiteCategoryId);
                }
                return i;
            }
            public static int[] GetResidentialCategories()
            {
                int[] i = null;
                CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                TList<CompanySiteCategory> cscList = cscService.GetAll();

                bool error = false;

                if (cscList == null) error = true;
                else if (cscList.Count == 0) error = true;

                if (error)
                    throw new Exception("Residential Categories not available.");
                {
                    i = new int[cscList.Count];
                    int _i = 0;
                    i[0] = 0;
                    foreach (CompanySiteCategory csc in cscList)
                    {
                        i[_i] = csc.CompanySiteCategoryId;
                        _i++;
                    }
                }
                return i;
            }
            public static void CheckAnnualTargets(int[] CategoryList, int Year)
            {
                foreach (int CategoryId in CategoryList)
                {
                    AnnualTargetsService atService = new AnnualTargetsService();
                    AnnualTargets at = atService.GetByYearCompanySiteCategoryId(Year, CategoryId);
                    if (at == null)
                    {
                        int count = 0;
                        AnnualTargetsFilters atFilters = new AnnualTargetsFilters();
                        atFilters.Append(AnnualTargetsColumn.CompanySiteCategoryId, CategoryId.ToString());
                        //Changes done for DT#2681
                        atFilters.AppendLessThanOrEqual(AnnualTargetsColumn.Year, Year.ToString());
                        TList<AnnualTargets> atList = DataRepository.AnnualTargetsProvider.GetPaged(
                                                        atFilters.ToString(), "Year DESC", 0, 100, out count);
                        if (count > 0)
                        { //Using a Previous Year's Annual Targets.
                            at = atList[0].Copy();
                        }
                        else
                        {
                            CompanySiteCategoryService cscService = new CompanySiteCategoryService();
                            CompanySiteCategory csc = cscService.GetByCompanySiteCategoryId(CategoryId);
                            throw new Exception("Cannot find Annual Targets for '" + csc.CategoryDesc + "'");
                        }
                    }
                    else
                    {
                        if (CategoryId != (int)CompanySiteCategoryList.NE3)
                        { //Embedded, Non-Embedded 1, Non-Embedded 2
                            if (at.QuarterlyAuditScore == null) throw new Exception("Annual Targets Not Set (EHS Audits)");
                            if (at.JsaScore == null) throw new Exception("Annual Targets Not Set (JSA Field Audit Verifications)");
                            if (at.WorkplaceSafetyCompliance == null) throw new Exception("Annual Targets Not Set (Workplace Safety & Compliance Inspection)");
                            if (at.HealthSafetyWorkContacts == null) throw new Exception("Annual Targets Not Set (Management Safety Work Contacts)");
                            if (at.BehaviouralSafetyProgram == null) throw new Exception("Annual Targets Not Set (Behavioural Observations)");
                            if (at.ToolboxMeetingsPerMonth == null) throw new Exception("Annual Targets Not Set (Toolbox Meetings)");
                            if (at.AlcoaMonthlyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Monthly Contractors Meeting)");
                            if (at.AlcoaWeeklyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Weekly Contractors Meeting)");
                            if (at.Training == null) throw new Exception("Annual Targets Not Set (Training)");

                            if (at.Aifr == null) throw new Exception("Annual Targets Not Set (AIFR)");
                            if (at.Trifr == null) throw new Exception("Annual Targets Not Set (TRIFR)");
                            if (at.SafetyPlan == null) throw new Exception("Annual Targets Not Set (Safety Plan)");
                            if (at.FatalityPrevention == null) throw new Exception("Annual Targets Not Set (Fatality Prevention)");

                            if (at.MedicalSchedule == null) throw new Exception("Annual Targets Not Set (Medical Schedule)");
                            if (at.TrainingSchedule == null) throw new Exception("Annual Targets Not Set (Training Schedule)");

                            if (CategoryId != (int)CompanySiteCategoryList.NE2)
                            { //Not Required for Non-Embedded 2
                                if (at.EhsAudits == null) throw new Exception("Annual Targets Not Set (EHS Audits)");
                            }
                        }
                        else // Non-Embedded 3
                        {
                            if (at.JsaScore == null) throw new Exception("Annual Targets Not Set (JSA Field Audit Verifications)");
                            if (at.AlcoaMonthlyContractorsMeeting == null) throw new Exception("Annual Targets Not Set (Alcoa Monthly Contractors Meeting)");
                            if (at.Training == null) throw new Exception("Annual Targets Not Set (Training)");
                        }
                    }
                }
            }
        }

        public class IO
        {
            public static string ReadFile(string FileFullPath)
            {
                Log4NetService.LogInfo("Reading file: " + FileFullPath);
                return FileUtilities.Read.File(FileFullPath);
                //string myFile = "";
                //try
                //{
                //    using (FileStream logFileStream = new FileStream(FileFullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                //    {
                //        StreamReader logFileReader = new StreamReader(logFileStream);
                //        while (!logFileReader.EndOfStream)
                //        {
                //            string line = logFileReader.ReadLine();
                //            myFile += line + "\n";
                //        }
                //        logFileReader.Close();
                //        logFileStream.Close();
                //    }
                //    return myFile;
                //}
                //catch (Exception ex)
                //{
                //    //Elmah.ErrorSignal.FromCurrentContext();
                //    Log4NetService.LogInfo("[ERROR] " + ex.Message);
                //}
                //return "";
            }
        }
    }
}