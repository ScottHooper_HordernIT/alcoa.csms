﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_TwentyOnePointAudit" Codebehind="TwentyOnePointAudit.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxp" %>
<%--<%@ Register Assembly="Fluent.MultiLineTextBoxValidator" Namespace="Fluent" TagPrefix="cc1" %>--%>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dxcb" %>
    <%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
    
<script language="JavaScript">
  var needToConfirm = false;
  
  window.onbeforeunload = confirmExit;
  function confirmExit()
  {
    if (needToConfirm)
      return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
  }
</script>

<table border="0" cellpadding="2" cellspacing="0" width="900">
    <tr>
        <td class="pageName" colspan="8" style="height: 44px">
            <span class="title">Safety</span><br />
            <span class="date">Contractor Services Audit</span><br />
            <img height="1" src="images/grfc_dottedline.gif" width="24" /><br /></td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
            <table border="0" cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td class="bodyText" style="height: 20px; text-align: left" valign="top">
                        <span style="font-size: 12pt; color: navy"><strong>1. Select Company/Timeframe</strong></span></td>
                </tr>
                <tr>
                    <td class="bodyText" style="height: 17px; text-align: left" valign="top">
                        &nbsp;<dxcp:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="200px"><PanelCollection>
<dxp:PanelContent runat="server"><TABLE border=0><TBODY><TR><TD style="WIDTH: 110px; TEXT-ALIGN: right"><STRONG>Company Name:</STRONG></TD><TD style="WIDTH: 350px; TEXT-ALIGN: left">
    <dxe:ASPxComboBox runat="server" EnableSynchronization="False" 
        IncrementalFilteringMode="StartsWith" ValueType="System.Int32" 
        CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        ClientInstanceName="ddlCompanies" Width="350px" ID="ddlCompanies" 
        OnSelectedIndexChanged="ddlCompanies_SelectedIndexChanged" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<ClientSideEvents SelectedIndexChanged="function(s, e) {
                                        ddlSites.PerformCallback(s.GetValue());
                                    }"></ClientSideEvents>

        <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </loadingpanelimage>

<ButtonStyle Width="13px"></ButtonStyle>
</dxe:ASPxComboBox>
 </TD><TD style="WIDTH: 150px; TEXT-ALIGN: right" colSpan=2 rowSpan=3>
        <dxe:ASPxButton runat="server" Text="View/Enter/Update" 
            CssPostfix="Office2003Blue" 
            CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" Width="140px" 
            Height="63px" Font-Bold="True" ID="btnSearchGo" OnClick="Button2_Click1" 
            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<ClientSideEvents Click="function(s, e) {
                                            needToConfirm = true;
                                        }"></ClientSideEvents>

</dxe:ASPxButton>
 </TD><TD style="WIDTH: 150px; TEXT-ALIGN: right" colSpan=1 rowSpan=3><BUTTON style="FONT-WEIGHT: bold; WIDTH: 140px; HEIGHT: 63px" onclick="javascript:print();" name="btnPrint" type=button>Print</BUTTON> </TD></TR><TR><TD style="WIDTH: 110px; HEIGHT: 15px; TEXT-ALIGN: right"><SPAN style="FONT-SIZE: 10pt"><STRONG>Site:</STRONG></SPAN></TD><TD style="FONT-SIZE: 10pt; WIDTH: 200px; HEIGHT: 15px; TEXT-ALIGN: left">
    <dxe:ASPxComboBox runat="server" EnableSynchronization="False" 
        IncrementalFilteringMode="StartsWith" ValueType="System.Int32" 
        TextField="SiteName" 
        ValueField="SiteId" CssPostfix="Office2003Blue" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        ClientInstanceName="ddlSites" ID="ddlSites" OnCallback="ddlSites_Callback" 
        SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
        </loadingpanelimage>
<ButtonStyle Width="13px"></ButtonStyle>
</dxe:ASPxComboBox>
 </TD></TR><TR><TD style="WIDTH: 110px; HEIGHT: 15px; TEXT-ALIGN: right"><STRONG>Qtr:</STRONG></TD><TD style="WIDTH: 333px; HEIGHT: 15px; TEXT-ALIGN: left"><asp:DropDownList runat="server" Width="130px" ID="ddlMonth"></asp:DropDownList>
<asp:DropDownList runat="server" Width="60px" ID="ddlYear"><asp:ListItem Value="2007"></asp:ListItem>
</asp:DropDownList>
 </TD></TR></TBODY></TABLE></dxp:PanelContent>
</PanelCollection>
</dxcp:ASPxCallbackPanel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
            <strong><span style="font-size: 12pt; color: #000080">2. Fill in Details</span></strong></td>
    </tr>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" Visible="False">
                <ContentTemplate>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
                
                <!----------------------------------- Section 01 ---------------------------------->
        <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                                    <td style="width: 617px; height: 15px;">
                                        <strong>1. Administrative Controls</strong></td>
                                    <td style="width: 49px; height: 15px;">
                                        <strong>Weight</strong></td>
                                    <td style="width: 63px; height: 15px;">
                                        <strong>Achieved</strong></td>
                                    <td style="height: 15px; width: 41px;">
                                        <strong>Score</strong></td>
                                    <td style="width: 253px; height: 15px">
                                        <strong>Observation</strong></td>
                                </tr>
            <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) EH&amp;S activities matrix in place (or 2 week look ahead)</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight1a" runat="server" Text="8"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved1a" runat="server" TabIndex=1>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center">
                        <asp:Label ID="pScore1a" runat="server"></asp:Label>
                    </td>
                    <td width="253px" style="font-size: 8pt">
                        <span><asp:TextBox ID="pObservation1a" runat="server" TextMode="MultiLine" Width="244px" TabIndex=0></asp:TextBox></span>
                        

                    </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff; font-size: 8pt;">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Company and or Alcoa EH&amp;S policies or statements
                            displayed in offices/cribrooms.</span></td>
                    <td style="width: 49px; text-align: center; font-size: 8pt;">
                        <asp:Label ID="pWeight1b" runat="server" Text="5"></asp:Label>
                    </td>
                    <td style="width: 63px; font-size: 8pt;">
                        <asp:DropDownList ID="pAchieved1b" runat="server" TabIndex=2>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center; font-size: 8pt;">
                        <asp:Label ID="pScore1b" runat="server"></asp:Label>
                    </td>
                    <td width="253px" style="font-size: 8pt">
                        <span><asp:TextBox ID="pObservation1b" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></span>
                    </td>
            </tr>
            <tr style="height: 13px; background-color: White; font-size: 8pt;">
                <td style="width: 617px">c) Registers - Scaffold</td>
                <td style="width: 49px; text-align: center; font-size: 8pt;">
                    <span><asp:Label ID="pWeight1c" runat="server" Text="8"></asp:Label></span>
                </td>
                <td style="width: 63px; font-size: 8pt;">
                    <asp:DropDownList ID="pAchieved1c" runat="server" TabIndex=3>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center; font-size: 8pt;">
                    <asp:Label ID="pScore1c" runat="server"></asp:Label>
                </td>
                <td width="253" style="font-size: 8pt">
                    <asp:TextBox ID="pObservation1c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Rigging</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c1" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c1" runat="server" TabIndex=4>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c1" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c1" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Fall Protection Equipment</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c2" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c2" runat="server" TabIndex=5>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c2" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c2" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Electrical</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c3" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c3" runat="server" TabIndex=6>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c3" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c3" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px; height: 13px">
                    &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Certificates of Competency</td>
                <td style="width: 49px; height: 13px; text-align: center">
                    <asp:Label ID="pWeight1c4" runat="server" Text="8"></asp:Label>
                </td>
                <td style="height: 13px; width: 63px;">
                    <asp:DropDownList ID="pAchieved1c4" runat="server" TabIndex=7>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; height: 13px; text-align: center">
                    <asp:Label ID="pScore1c4" runat="server"></asp:Label>
                </td>
                <td style="height: 13px" width="253">
                    <asp:TextBox ID="pObservation1c4" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - JSA</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c5" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c5" runat="server" TabIndex=8>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c5" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c5" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Plant and Equipment</td>
                <td style="width: 49px; text-align: center">
                    <span style="font-size: 8pt">
                        <asp:Label ID="pWeight1c6" runat="server" Text="8"></asp:Label></span>
                </td>
                <td style="width: 63px; font-size: 8pt;">
                    <asp:DropDownList ID="pAchieved1c6" runat="server" TabIndex=9>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center; font-size: 8pt;">
                    <asp:Label ID="pScore1c6" runat="server"></asp:Label>
                </td>
                <td width="253" style="font-size: 8pt">
                    <asp:TextBox ID="pObservation1c6" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - First Aid</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c7" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c7" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c7" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c7" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Fire Extinguisher (included in vehicles)</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c8" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c8" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c8" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c8" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - MSDS & Dangerous Goods</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c9" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c9" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c9" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c9" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Job description of staff include EH&S</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c10" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c10" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c10" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c10" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Comprehensive induction program in place</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c11" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c11" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c11" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c11" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 13px; background-color: white; font-size: 8pt;">
                <td style="width: 617px">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - Sub contractor Permit Register and is maintained and Current</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight1c12" runat="server" Text="8"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved1c12" runat="server" TabIndex=10>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1c12" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation1c12" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff; font-size: 8pt;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span>d) Notification Whiteboards used in Operating Centres.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1d" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1d" runat="server" TabIndex=11>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1d" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">e) Safety Meetings and Toolbox Meeting minutes demonstrate
                            that items are closed out and that any safety suggestions are recorded. Special
                            toolbox topics are held at least monthly. Those present are recorded including management.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1e" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1e" runat="server" TabIndex=12>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1e" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">f) EH&amp;S Plan published and progress maintained.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1f" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1f" runat="server" TabIndex=13>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1f" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">g) Job Observation Program - analysis of observations.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1g" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1g" runat="server" TabIndex=14>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1g" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">h) Adequate professional consultants available (EH&amp;S
                            / OCC Hygiene).</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1h" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1h" runat="server" TabIndex=15>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1h" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">i) Alcoa newsletter and EH&amp;S alerts and Incident reports
                            are on display and up to date.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1i" runat="server" Text="8"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1i" runat="server" TabIndex=16>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1i" runat="server"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">j) Currently Pre-Qualified (including Sub-contractors)
                            as an Alcoa Contractor.<span style="mso-spacerun: yes">&nbsp;</span></span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1j" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1j" runat="server" TabIndex=17>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1j" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">k) Recognition of good employee behaviour is recorded.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1k" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1k" runat="server" TabIndex=17>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1k" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1k" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">
                    l<span>) Recognition awards have been for individual, crew and
                            site level.</span></span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1l" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1l" runat="server" TabIndex=18>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1l" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1l" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">m) Safety reps are elected and names are posted.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1m" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1m" runat="server" TabIndex=19>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1m" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1m" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">n) Examples where safety issues have been elevated from
                            the toolbox meeting.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1n" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1n" runat="server" TabIndex=20>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1n" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1n" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">o) Site workgroup is achieving greater than 5:1 IFE's/
                            IFO's to injuries.</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1o" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1o" runat="server" TabIndex=21>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1o" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1o" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">p) All incidents are investigated within 24 hours and data
                            input to the Alcoa system.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1p" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved1p" runat="server" TabIndex=22>
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem Value="0"></asp:ListItem>
                        <asp:ListItem Value="1"></asp:ListItem>
                        <asp:ListItem Value="2"></asp:ListItem>
                        <asp:ListItem Value="3"></asp:ListItem>
                        <asp:ListItem Value="4"></asp:ListItem>
                        <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1p" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                        <asp:TextBox ID="pObservation1p" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">q) Evacuation procedures are known by work group and location
                            of nearest muster point is known.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1q" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1q" runat="server" TabIndex=23>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1q" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                        <asp:TextBox ID="pObservation1q" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">r) Contractor employees have undertaken Mines Medical.</span>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                        <asp:Label ID="pWeight1r" runat="server" Text="8"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1r" runat="server" TabIndex=24>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1r" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1r" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
               </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">s) Contractor employees have Mobile Equipment Medicals</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1s" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1s" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1s" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1s" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">t) Metrics are developed for tracking EH&amp;S performance.
                            These are distributed to employees HS9.1.</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1t" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1t" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1t" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1t" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">u) Camera Approvals in Place</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1u" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1u" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1u" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1u" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">v) Mobile phone approvals in place (doc 61791)</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1v" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1v" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1v" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1v" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">w) Fatigue Management procedure available. evidence that it is being used to reduce risk. How?</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1w" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1w" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1w" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1w" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">x) Fatality Prevention, Has company nominated top 5 high risks, Are action plans or projects being addressed</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1x" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1x" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1x" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1x" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">y) Pre-Shift Briefings Held, Evidence that briefs address EH&S and documented</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1y" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1y" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1y" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1y" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff;">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt;">z) Close out Briefings Held, Evidence that briefs address EH&S and any incidents from shift and are documented. </span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight1z" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                        <asp:DropDownList ID="pAchieved1z" runat="server" TabIndex=25>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore1z" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation1z" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong>
                </td>
                <td style="font-size: 8pt; width: 49px; text-align: center">
                    <span style="color: #ff0000">1715</span>
                </td>
                <td style="font-size: 8pt; width: 63px;"></td>
                <td style="font-size: 8pt; width: 41px;"></td>
                <td style="font-size: 8pt; width: 253px;"></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong>
                </td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore1a" runat="server" Text="0" Width="40px"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;"></td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore1b" runat="server" Text="0"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore1c" runat="server" Text="0%"></asp:Label>
                </td>
            </tr>
      </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                <!----------------------------------- Section 02 ---------------------------------->
      <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;"><strong>2. Yard / Amenities / Offices</strong></td>
                    <td style="width: 49px; height: 15px;"><strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;"><strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;"><strong>Score</strong></td>
                    <td style="width: 253px; height: 15px"><strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px"><span style="font-size: 8pt">a) Adequate facilities for employees.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2a" runat="server" Text="5"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2a" runat="server" TabIndex=26>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2a" runat="server"></asp:Label></td>
                    <td width="253px">
                        <span style="font-size: 7pt">
                            <asp:TextBox ID="pObservation2a" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Notice board - adequate size.</span>
                    </td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2b" runat="server" Text="2"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2b" runat="server" TabIndex=27>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2b" runat="server"></asp:Label></td>
                    <td width="253px">
                        <span style="font-size: 7pt">
                            <asp:TextBox ID="pObservation2b" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox>
                        </span>
                    </td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Fire extinguishers available.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2c" runat="server" Text="5"></asp:Label>
                     </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2c" runat="server" TabIndex=28>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2c" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Access / Egress clearly marked / emergency.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2d" runat="server" Text="5"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2d" runat="server" TabIndex=29>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2d" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Bins provided / emptied. (recycle compliance)</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2e" runat="server" Text="5"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2e" runat="server" TabIndex=30>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2e" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Dangerous goods not stored in or adjacent to amenities.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2f" runat="server" Text="10"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2f" runat="server" TabIndex=31>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2f" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Offices, Yard and Crib Facilities clean and housekeeping schedules met.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight2g" runat="server" Text="8"></asp:Label>
                    </td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved2g" runat="server" TabIndex=32>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2g" runat="server"></asp:Label></td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation2g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) Correct storage of materials, food and office equipment.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2h" runat="server" Text="8"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2h" runat="server" TabIndex=33>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2h" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        i) Organisation chart nominating supervisors, EH&amp;S Adviser, Safety Reps, First
                        Aiders.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight2i" runat="server" Text="2"></asp:Label>
                    </td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved2i" runat="server" TabIndex=34>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore2i" runat="server"></asp:Label></td>
                    <td width="253">
                        <asp:TextBox ID="pObservation2i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong>
                    </td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px; text-align: center">
                        <span style="color: #ff0000">250</span>
                    </td>
                    <td style="font-size: 8pt; width: 63px;"></td>
                    <td style="font-size: 8pt; width: 41px;"></td>
                    <td style="font-size: 8pt; width: 253px;"></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong>
                    </td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore2a" runat="server" Text="0" Width="40px"></asp:Label>
                    </td>
                    <td style="font-size: 8pt; width: 63px;"></td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore2b" runat="server" Text="0"></asp:Label>
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore2c" runat="server" Text="0%"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>3. First Aid &amp; Employee Welfare</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span><span style="font-size: 8pt">a) Stock list in cabinet.</span></span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3a" runat="server" Text="5"></asp:Label>
                </td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3a" runat="server" TabIndex=35>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation3a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Emergency procedures / phone numbers displayed. 222 or 97338222</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3b" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3b" runat="server" TabIndex=36>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation3b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Treatment register at facility.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3c" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3c" runat="server" TabIndex=37>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) List of qualified First Aiders displayed at facility.<span style="mso-spacerun: yes">&nbsp;</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3d" runat="server" TabIndex=38>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Facility easily identified.<span style="mso-spacerun: yes">&nbsp;</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3e" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3e" runat="server">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Green Cross displayed on hard hats.<span style="mso-spacerun: yes">&nbsp;</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3f" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3f" runat="server" TabIndex=39>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px; height: 13px">
                    g) Return to work program exists.<span style="mso-spacerun: yes">&nbsp;</span></td>
                <td style="width: 49px; height: 13px; text-align: center">
                    <asp:Label ID="pWeight3g" runat="server" Text="5"></asp:Label></td>
                <td style="height: 13px; width: 63px;">
                    <asp:DropDownList ID="pAchieved3g" runat="server" TabIndex=40>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore3g" runat="server"></asp:Label>
                </td>
                <td style="height: 13px" width="253">
                    <asp:TextBox ID="pObservation3g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    h) Off the Job Injuries are tracked and recorded.<span style="mso-spacerun: yes">&nbsp;</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3h" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3h" runat="server" TabIndex=41>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3h" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    i) Off the Job Safety Program is in place.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight3i" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved3i" runat="server" TabIndex=42>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore3i" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation3i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">250</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore3a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore3b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore3c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>4. Site Conditions</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) Correct stacking of materials.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4a" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4a" runat="server" TabIndex=43>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation4a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Barricades erected / maintained / blue info tagging
                            maintained</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4b" runat="server">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation4b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">c) Signs/blue info tagging maintained</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4c" runat="server">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4c" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <asp:TextBox ID="pObservation4c" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        d) Sharp objects capped ie: star pickets, reo bar.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4d" runat="server" Text="5"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4d" runat="server" TabIndex=44>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        e) Area delineated for vehicle parking.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4e" runat="server" Text="3"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4e" runat="server" TabIndex=45>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        f) Other Hazards, noise, dust, spills, &amp; waste.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4f" runat="server" Text="5"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4f" runat="server" TabIndex=46>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        g) Signs complied with.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4g" runat="server" Text="7"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4g" runat="server" TabIndex=47>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4g" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        h) Laydown areas defined.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight4h" runat="server" Text="5"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved4h" runat="server" TabIndex=48>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore4h" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation4h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        i) Equipment being used complies to noise standards.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4i" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4i" runat="server" TabIndex=49>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 617px">
                        j) Combustion engines not exhausting dark smoke.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4j" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4j" runat="server" TabIndex=50>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4j" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        k) Bin locations prominent, adequate, emptied regularly, recycling compliance</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4k" runat="server" Text="5"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4k" runat="server">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4k" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4k" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        l) Pre-start inspections show hazard identification and housekeeping as primary
                        issues.
                    </td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight4l" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved4l" runat="server" TabIndex=50>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore4l" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation4l" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">440</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore4a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore4b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore4c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>5. Access / Egress</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) Ladders secured, 1.4 metre lean, project 1 metre
                            above platform.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5a" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5a" runat="server" TabIndex=51>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation5a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Clear access egress points to scaffolds, excavations,
                            mobile equipment etc.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5b" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5b" runat="server" TabIndex=52>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation5b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Handrails installed ie: ramps, stairs.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5c" runat="server" TabIndex=53>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation5c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Sharp edges and scaffold clips covered on walkways.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5d" runat="server" Text="7"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5d" runat="server" TabIndex=54>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation5d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Traffic management - roads closure permits, in place spotters, delineation.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5e" runat="server" TabIndex=55>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation5e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Gridmesh &amp; Chequerplate removal permit.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight5f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved5f" runat="server" TabIndex=56>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore5f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation5f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Access Egress permits in place.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight5g" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved5g" runat="server" TabIndex=57>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore5g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation5g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px; height: 13px">
                        h) Roof Access permits in place. Tank Roofs boared out etc</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight5h" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved5h" runat="server" TabIndex=57>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore5h" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation5h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">365</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore5a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore5b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore5c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>6. Mobile Plant</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) Daily inspection checklist completed by operator.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6a" runat="server" TabIndex=58>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation6a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Operators Manual.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6b" runat="server" TabIndex=59>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation6b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Faults corrected immediately, no leaks or spills from oil or fuel etc (including
                        service trucks).</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6c" runat="server" TabIndex=60>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Classified Equipment log books up to date.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6d" runat="server" Text="5"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6d" runat="server" TabIndex=61>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Safety devices fitted - seat belts / fire extinguishers / reverse beeper (smart
                        type) / machine guards.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6e" runat="server"  TabIndex=62>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Safe working loads marked.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6f" runat="server" TabIndex=63>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Employee operator holds relevant ticket or competency.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight6g" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved6g" runat="server" TabIndex=64>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore6g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation6g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) Dust suppression adequate for construction considering potential for wind.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6h" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6h" runat="server" TabIndex=65>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 617px">
                        i) Road closure permit in place and complied with.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6i" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6i" runat="server" TabIndex=66>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        j) Permit to dig in place and complied with.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6j" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6j" runat="server" TabIndex=67>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6j" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        k) Spotters in place.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6k" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6k" runat="server" TabIndex=68>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6k" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6k" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        l) Mobile plant is not exhausting dark smoke</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight6l" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved6l" runat="server" TabIndex=68>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore6l" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation6l" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">525</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore6a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore6b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore6c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>7. Rigging / Lifting Gear</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) All lifting gear marked with current colour coding.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7a" runat="server" TabIndex=69>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation7a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Safe working loads marked on gear.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7b" runat="server" TabIndex=70>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation7b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Tag lines used as required [16mm fibre rope].</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7c" runat="server" TabIndex=71>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation7c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Gear inspected regular / good condition.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7d" runat="server" TabIndex=72>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation7d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Adequate storage facility [off ground level].</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7e" runat="server" TabIndex=73>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation7e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Significant Lifts studies completed were required</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight7f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved7f" runat="server" TabIndex=73>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore7f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation7f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">250</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore7a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore7b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore7c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>8. Welding / Cutting / Grinding / Hot Work</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) Hot work permit required / displayed with JSA.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8a" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8a" runat="server" TabIndex=74>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation8a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Fire extinguisher - prominent position.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8b" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8b" runat="server" TabIndex=75>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation8b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Sparks encapsulated from fellow workers or hazards.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8c" runat="server" Text="8"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8c" runat="server" TabIndex=76>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Adequate PPE for task.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8d" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8d" runat="server" TabIndex=77>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Cylinders secured, gauges operative, flashback arresters fitted at regulator
                        outlet and handpiece.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8e" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8e" runat="server" TabIndex=78>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Earth lead close proximity to weld.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8f" runat="server" Text="7"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8f" runat="server" TabIndex=79>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Welding screens, fire blankets.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight8g" runat="server" Text="10"></asp:Label>
</td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved8g" runat="server" TabIndex=80>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore8g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation8g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) Automatic voltage reducers installed on diesel welding machines.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8h" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8h" runat="server" TabIndex=81>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td style="width: 617px">
                        i) Grinders guards in place, correct RPM Disk rating.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight8i" runat="server" Text="10"></asp:Label>
</td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved8i" runat="server" TabIndex=82>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore8i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation8i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">425</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore8a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore8b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore8c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>9. Cranage</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span style="font-size: 8pt">a) Clear of unnecessary personnel / barricading.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9a" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9a" runat="server" TabIndex=83>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation9a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Controlled crane work area set up.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9b" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9b" runat="server" TabIndex=84>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation9b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Adequate ground support / level surface clear of excavations.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9c" runat="server" Text="8"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9c" runat="server" TabIndex=85>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation9c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Daily checklist completed / safety devices. eg. smart type reversing beepers etc.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9d" runat="server" TabIndex=86>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation9d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Operators manual.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9e" runat="server" Text="8"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9e" runat="server" TabIndex=87>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation9e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Crane Operator / Riggers communication methods clear.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9f" runat="server" TabIndex=88>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation9f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px; height: 13px">
                    g) Significant lift study completed (if required).</td>
                <td style="width: 49px; height: 13px; text-align: center">
                    <asp:Label ID="pWeight9g" runat="server" Text="10"></asp:Label></td>
                <td style="height: 13px; width: 63px;">
                    <asp:DropDownList ID="pAchieved9g" runat="server" TabIndex=89>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore9g" runat="server"></asp:Label>
                </td>
                <td style="height: 13px" width="253">
                    <asp:TextBox ID="pObservation9g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    h) Crane log book up to date, repairs signed off etc</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight9h" runat="server" Text="7"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved9h" runat="server" TabIndex=90>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore9h" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation9h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">315</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore9a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore9b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore9c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>10. Scaffolding</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span style="font-size: 8pt">a) Frequency of inspections (minimum monthly).</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10a" runat="server" Text="7"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10a" runat="server" TabIndex=91>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation10a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Hand / mid rails, toe boards fitted.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10b" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10b" runat="server" TabIndex= 92>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation10b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Adequate access [maximum of 4 metres between landings].</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10c" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10c" runat="server" TabIndex=93>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation10c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Secured - vertically, horizontally and bracing.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10d" runat="server" TabIndex=94>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation10d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) All platforms fully planked.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10e" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10e" runat="server" TabIndex=95>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation10e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Scafftag fitted in correct position.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10f" runat="server" TabIndex=96>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation10f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px; height: 13px">
                    g) Internal ladder access or shoulder rail fitted.</td>
                <td style="width: 49px; height: 13px; text-align: center">
                    <asp:Label ID="pWeight10g" runat="server" Text="10"></asp:Label></td>
                <td style="height: 13px; width: 63px;">
                    <asp:DropDownList ID="pAchieved10g" runat="server" TabIndex=97>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore10g" runat="server"></asp:Label>
                </td>
                <td style="height: 13px" width="253">
                    <asp:TextBox ID="pObservation10g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    h) Correct stacking &amp; storing of scaffolding.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight10h" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved10h" runat="server" TabIndex=98>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore10h" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation10h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">360</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore10a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore10b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore10c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>11. Electrical</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span><span style="font-size: 8pt">a) Earth leakage used on all tools / equipment.</span></span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11a" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11a" runat="server" TabIndex=99>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation11a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Cords elevated above ground level or protected from
                        potential damage.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11b" runat="server" Text="7"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11b" runat="server" TabIndex=100>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation11b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Identification tag fitted with correct quarterly information and current colour
                    coding.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11c" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11c" runat="server" TabIndex=101>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation11c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Temporary power boxes location clear of hazards.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11d" runat="server" Text="7"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11d" runat="server" TabIndex=102>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation11d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Leads not obstructing walkways.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11e" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11e" runat="server" TabIndex=103>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation11e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Employees are aware of Alcoa rules 32.60. eg. 3m &amp; 10m rules, High Voltage
                    permit, not walking on cable in trays etc.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight11f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved11f" runat="server" TabIndex=104>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore11f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation11f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">245</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore11a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore11b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore11c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>12. Compressed Air</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span><span style="font-size: 8pt">a) Correct hoses for task, ie: pressure rating.</span></span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight12a" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved12a" runat="server" TabIndex=105>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore12a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation12a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Retaining devices used on hose couplings, ie: safety
                        clips, wire ropes, chains.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight12b" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved12b" runat="server" TabIndex=106>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore12b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation12b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Ear defenders and mono goggles worn by operator.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight12c" runat="server" Text="8"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved12c" runat="server" TabIndex=107>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore12c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation12c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Air hoses never used for housekeeping or cleaning.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight12d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved12d" runat="server" TabIndex=108>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore12d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation12d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Hoses not obstructing walkways</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight12e" runat="server" Text="5"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved12e" runat="server" TabIndex=109>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore12e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation12e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">215</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore12a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore12b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore12c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>13. Hazardous Substances &amp; Dangerous Goods</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span><span style="font-size: 8pt">a) Access to register &amp; MSDS sheets by employees.</span></span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13a" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13a" runat="server" TabIndex=110>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation13a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Segregation from other materials, warning signs displayed
                        as per regulations.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13b" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13b" runat="server" TabIndex=111>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation13b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Compliance with MSDS / JSA ie relevant MSDS information is included in the JSA
                    or SWI.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13c" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13c" runat="server" TabIndex=112>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation13c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Emergency equipment accessible, training in spill response has been conducted.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13d" runat="server" TabIndex=113>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation13d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Dangerous goods are segregated and bunded to standard (drums labelled and managed).</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13e" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13e" runat="server" TabIndex=114>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation13e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Procedures clearly visible in case of fire, spills, etc</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight13f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved13f" runat="server" TabIndex=115>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore13f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation13f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">300</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore13a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore13b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore13c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
         </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>14. Employee Pro-Active Approach</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) Manual handling practices, lifting technique etc.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14a" runat="server" TabIndex=116>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation14a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Exposure to falling objects.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14b" runat="server" TabIndex=117>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation14b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Work area set up ergonomically.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14c" runat="server" TabIndex=118>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation14c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Aware of the "Take Two" principle.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14d" runat="server" TabIndex=119>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation14d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Appears confident in his/her duties and is complying to site rules.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14e" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14e" runat="server" TabIndex=120>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation14e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) There is a process for employees to be involved in the selection of PPE and for
                        discussion of improving PPE.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14f" runat="server" TabIndex=121>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation14f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Employees have or know where they can access and Alcoa EH&amp;S directory (rules).</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight14g" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved14g" runat="server" TabIndex=122>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore14g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation14g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) Defensive work practices displayed. (including appropriate PPE worn, operating
                        at safe speed etc).</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight14h" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved14h" runat="server" TabIndex=123>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore14h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation14h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">390</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore14a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore14b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore14c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>15. Employee Compliance</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) Qualified for the operation of equipment, competency
                            based training in place.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15a" runat="server" TabIndex=124>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation15a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Work at heights practices. Fall protected, pro-active
                            fall prevention.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15b" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15b" runat="server" TabIndex=125>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation15b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Participation in job observation program.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15c" runat="server" Text="8"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15c" runat="server" TabIndex=126>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Tools being used are right for the job, used correctly.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15d" runat="server" TabIndex=127>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Adjusting or correcting PPE on observation. Seat belts, goggles, hearing protection
                        etc.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15e" runat="server" Text="7"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15e" runat="server" TabIndex=128>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Inadequate PPE for task. Double eye protection grinding, P2 respirators etc.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15f" runat="server" TabIndex=129>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        g) Appears confident in his/her duties and is complying to site rules.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15g" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15g" runat="server" TabIndex=129>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15g" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) There is a process for employees to be involved in the selection of PPE and for discussion of improving PPE</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15h" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15h" runat="server" TabIndex=129>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        i) Employees have or know where they can access and Alcoa EH&S directory (rules).</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight15i" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved15i" runat="server" TabIndex=129>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore15i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation15i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">415</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore15a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore15b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore15c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px" id="TABLE1">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>16. Permit to Work / JSA's</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) Participation in JSA development.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16a" runat="server" TabIndex=130>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation16a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Approved JSA in place.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16b" runat="server" TabIndex=131>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation16b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) JSA reviewed regularly.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16c" runat="server" TabIndex=132>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) JSA reflects the task in progress.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16d" runat="server" TabIndex=133>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) MSDS attached to JSA and specific risks taken from MSDS and noted in JSA.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16e" runat="server" TabIndex=134>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Danger tag/lockout procedures complied with.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16f" runat="server" TabIndex=135>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) All Permit conditions adhered to.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight16g" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved16g" runat="server" TabIndex=136>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore16g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation16g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) Permit understood by relevant persons.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16h" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16h" runat="server" TabIndex=137>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        i) Displayed at location of work.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16i" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16i" runat="server" TabIndex=138>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        j) Within expiry date.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight16j" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved16j" runat="server" TabIndex=139>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore16j" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation16j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; text-align: center">
                        <span style="color: #ff0000">500</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore16a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore16b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore16c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>17. Confined Space</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span style="font-size: 8pt">a) Permit valid.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17a" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17a" runat="server" TabIndex=140>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation17a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Permit requirements complied with - access, gas monitoring,
                        isolation, housekeeping, ventilation etc.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17b" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17b" runat="server" TabIndex=141>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation17b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Confined space entry procedure is attached to permit with any additional JSA's.
                    MSDS's attached to permit and complied with.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17c" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17c" runat="server" TabIndex=142>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Scaffolding - tagged, checked, not overloaded.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17d" runat="server" TabIndex=143>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Communications in place, suitable and understood by employees.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17e" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17e" runat="server" TabIndex=144>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Access - internal and external.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17f" runat="server" TabIndex=145>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px; height: 13px">
                    g) Pre-entry briefing was conducted.</td>
                <td style="width: 49px; height: 13px; text-align: center">
                    <asp:Label ID="pWeight17g" runat="server" Text="10"></asp:Label></td>
                <td style="height: 13px; width: 63px;">
                    <asp:DropDownList ID="pAchieved17g" runat="server" TabIndex=146>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore17g" runat="server"></asp:Label>
                </td>
                <td style="height: 13px" width="253">
                    <asp:TextBox ID="pObservation17g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    h) Emergency evacuation in place.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17h" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17h" runat="server" TabIndex=147>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17h" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    i) Stand-by person identified, in position, understands his responsibilities.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17i" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17i" runat="server" TabIndex=148>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17i" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    j) Entry Leader identified and understands responsibilities.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight17j" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved17j" runat="server" TabIndex=149>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore17j" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation17j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">k) Correct signage in place "No Unauthorised Access", "Confined
                        Space No Unauthorised Entry".</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight17k" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved17k" runat="server" TabIndex=150>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore17k" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation17k" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: #edf5ff">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">l) Personal locks and Danger tags attached to hasp at permit.</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight17l" runat="server" Text="10"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved17l" runat="server" TabIndex=151>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore17l" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation17l" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: white">
                <td style="width: 617px; height: 13px; text-align: left">
                    <span style="font-size: 8pt">m) Emergency lighting installed and functional</span></td>
                <td style="font-size: 8pt; width: 49px; text-align: center;">
                    <asp:Label ID="pWeight17m" runat="server" Text="10"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 63px;">
                    <asp:DropDownList ID="pAchieved17m" runat="server" TabIndex=152>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center">
                    <asp:Label ID="pScore17m" runat="server"></asp:Label>
                </td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:TextBox ID="pObservation17m" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; text-align: center">
                    <span style="color: #ff0000">650</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore17a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore17b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore17c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
                    <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
            <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                <td style="width: 617px; height: 15px;">
                    <strong>18. Lighting / Nightshift</strong></td>
                <td style="width: 49px; height: 15px;">
                    <strong>Weight</strong></td>
                <td style="width: 63px; height: 15px;">
                    <strong>Achieved</strong></td>
                <td style="height: 15px; width: 41px;">
                    <strong>Score</strong></td>
                <td style="width: 253px; height: 15px">
                    <strong>Observation</strong></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    <span><span style="font-size: 8pt">a) Designated work areas well lit.</span></span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18a" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18a" runat="server" TabIndex=153>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18a" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation18a" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    <span style="font-size: 8pt">b) Access / Egress to designated work areas well lit.</span></td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18b" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18b" runat="server" TabIndex=154>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18b" runat="server"></asp:Label>
                </td>
                <td width="253px">
                    <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation18b" runat="server" TextMode="MultiLine"
                        Width="244px"></asp:TextBox></span></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    c) Access / Egress to distribution boards in case of power failure (torch is available
                    to employees).</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18c" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18c" runat="server" TabIndex=155>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18c" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    d) Access / Egress to ablution blocks well lit.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18d" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18d" runat="server" TabIndex=156>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18d" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: white">
                <td style="width: 617px">
                    e) Access / Egress to Crib Rooms well lit.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18e" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18e" runat="server" TabIndex=157>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18e" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    f) Stairways emergency lighting is on separate circuit.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18f" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18f" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18f" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    g) Distribution Boards [ above lighting]</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18g" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18g" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18g" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    h) Excavations.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18h" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18h" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18h" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    i) On or near ELEVATED WORK PLATFORM in operation</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18i" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18i" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18i" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    j) Near fire extinguishers</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18j" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18j" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18j" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18j" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    k) On roadways / where applicable.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18k" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18k" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18k" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18k" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
                        <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    l) In laydown areas / where applicable.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18l" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18l" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18l" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18l" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    m) In storage yards / where applicable.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18m" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18m" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18m" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18m" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    n) Over Alimak entrances / where applicable.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18n" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18n" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18n" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18n" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: White">
                <td style="width: 617px">
                    o) For scaffolding work.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18o" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18o" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18o" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18o" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            <tr style="height: 13px; background-color: #edf5ff">
                <td style="width: 617px">
                    p) Onto scaffolding platform in use.</td>
                <td style="width: 49px; text-align: center">
                    <asp:Label ID="pWeight18p" runat="server" Text="10"></asp:Label></td>
                <td style="width: 63px">
                    <asp:DropDownList ID="pAchieved18p" runat="server" TabIndex=158>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                <td style="width: 41px; text-align: center"><asp:Label ID="pScore18p" runat="server"></asp:Label>
                </td>
                <td width="253">
                    <asp:TextBox ID="pObservation18p" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
            </tr>
            
            
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Total Possible Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <span style="color: #ff0000">800</span></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px;">
                </td>
                <td style="font-size: 8pt; width: 253px;">
                </td>
            </tr>
            <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                <td style="width: 617px; height: 13px; text-align: right">
                    <strong>Actual Applicable Score:</strong></td>
                <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore18a" runat="server" Text="0" Width="40px"></asp:Label></td>
                <td style="font-size: 8pt; width: 63px;">
                </td>
                <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                    <asp:Label ID="pAAScore18b" runat="server" Text="0"></asp:Label></td>
                <td style="font-size: 8pt; width: 253px;">
                    <asp:Label ID="pAAScore18c" runat="server" Text="0%"></asp:Label></td>
            </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>19. Tagging</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">a) Employees have completed competency based tagging
                            training.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19a" runat="server" TabIndex=159>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation19a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Supervisor has completed pre-work tagging checklist.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19b" runat="server" TabIndex=160>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation19b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Tie-In permit is in place.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19c" runat="server" TabIndex=161>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation19c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) MWR has been signed on before work commenced.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19d" runat="server" TabIndex=162>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation19d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Personal locks and tags are attached to correct hasp or lockbox.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19e" runat="server" TabIndex=163>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation19e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Scope of work on Tie-In , Scope of work in the MWR &amp; the contractor's job
                        scope are one in the same.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight19f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved19f" runat="server" TabIndex=164>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore19f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation19f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">300</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore19a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore19b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore19c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
         </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>20. Waste Management</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) Employees have completed Environmental and Waste
                            Management information and training package.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight20a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved20a" runat="server" TabIndex=165>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore20a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation20a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) There is a "waste management" program.</span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight20b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved20b" runat="server" TabIndex=166>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore20b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation20b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Re-cycling efficiency is high.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight20c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved20c" runat="server" TabIndex=167>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore20c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation20c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) No evidence of waste in re-cycling bins.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight20d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved20d" runat="server" TabIndex=168>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore20d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation20d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Re-useable items are not evident in the waste bins.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight20e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved20e" runat="server" TabIndex=169>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore20e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation20e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">250</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore20a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore20b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore20c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="width: 617px; height: 15px;">
                        <strong>21. Employee Training</strong></td>
                    <td style="width: 49px; height: 15px;">
                        <strong>Weight</strong></td>
                    <td style="width: 63px; height: 15px;">
                        <strong>Achieved</strong></td>
                    <td style="height: 15px; width: 41px;">
                        <strong>Score</strong></td>
                    <td style="width: 253px; height: 15px">
                        <strong>Observation</strong></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        <span><span style="font-size: 8pt">a) Employee Competency matrix is up to date.</span></span></td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21a" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21a" runat="server" TabIndex=170>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21a" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation21a" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        <span style="font-size: 8pt">b) Mandatory Environmental training is recorded. (spill
                            response, noise reduction, dust reduction, recycling) </span>
                    </td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21b" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21b" runat="server" TabIndex=171>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21b" runat="server"></asp:Label>
                    </td>
                    <td width="253px">
                        <span style="font-size: 7pt">&nbsp;<asp:TextBox ID="pObservation21b" runat="server" TextMode="MultiLine"
                            Width="244px"></asp:TextBox></span></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        c) Mandatory Hygiene / Health training is recorded (Hearing, Respirator fit testing,
                        Hazardous Materials).</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21c" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21c" runat="server" TabIndex=172>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21c" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21c" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        d) Mandatory Safety training is recorded (Tagging, Mobile Equip, CSE, Falls, Electrical).</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21d" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21d" runat="server" TabIndex=173>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21d" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21d" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px">
                        e) Toolbox minutes demonstrate ongoing commitment to training</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21e" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21e" runat="server" TabIndex=174>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21e" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21e" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        f) Relevant employees are trained in investigation and recording techniques.</td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21f" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21f" runat="server" TabIndex=175>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21f" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21f" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: white">
                    <td style="width: 617px; height: 13px">
                        g) Incident Prevention Fundamentals Training is documented for all employees.</td>
                    <td style="width: 49px; height: 13px; text-align: center">
                        <asp:Label ID="pWeight21g" runat="server" Text="10"></asp:Label></td>
                    <td style="height: 13px; width: 63px;">
                        <asp:DropDownList ID="pAchieved21g" runat="server" TabIndex=176>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; height: 13px; text-align: center"><asp:Label ID="pScore21g" runat="server"></asp:Label>
                    </td>
                    <td style="height: 13px" width="253">
                        <asp:TextBox ID="pObservation21g" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 617px">
                        h) There is a documented induction process for all re-assigned or transferred employees.
                    </td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21h" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21h" runat="server" TabIndex=177>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21h" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21h" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="height: 13px; background-color: White">
                    <td style="width: 617px">
                        i) Is the companies Contractor Responsible Person.(CRP) known. Have they completed the Alcoa training?
                    </td>
                    <td style="width: 49px; text-align: center">
                        <asp:Label ID="pWeight21i" runat="server" Text="10"></asp:Label></td>
                    <td style="width: 63px">
                        <asp:DropDownList ID="pAchieved21i" runat="server" TabIndex=177>
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="0"></asp:ListItem>
                            <asp:ListItem Value="1"></asp:ListItem>
                            <asp:ListItem Value="2"></asp:ListItem>
                            <asp:ListItem Value="3"></asp:ListItem>
                            <asp:ListItem Value="4"></asp:ListItem>
                            <asp:ListItem Value="5"></asp:ListItem>
                    </asp:DropDownList></td>
                    <td style="width: 41px; text-align: center"><asp:Label ID="pScore21i" runat="server"></asp:Label>
                    </td>
                    <td width="253">
                        <asp:TextBox ID="pObservation21i" runat="server" TextMode="MultiLine" Width="244px"></asp:TextBox></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Total Possible Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <span style="color: #ff0000">450</span></td>
                    <td style="font-size: 8pt; width: 63px;">
                    </td>
                    <td style="font-size: 8pt; width: 41px;">
                    </td>
                    <td style="font-size: 8pt; width: 253px;">
                    </td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 617px; height: 13px; text-align: right">
                        <strong>Actual Applicable Score:</strong></td>
                    <td style="font-size: 8pt; width: 49px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore21a" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt">
                    </td>
                    <td style="font-size: 8pt; width: 41px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore21b" runat="server" Text="0"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px;">
                        <asp:Label ID="pAAScore21c" runat="server" Text="0%"></asp:Label></td>
                </tr>
            </table>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: right;">
            <a href="#save">Scroll Down to bottom and Save</a>
        </td>
      </tr>
      <tr>
        <td class="pageName" colspan="8" style="text-align: left;">
<br />
<a name="save"></a>
            <table style="background-color:Transparent; overflow:hidden; font: 11px Tahoma;color: Black; border-right: #4f93e3 1px solid; table-layout: auto; border-top: #4f93e3 1px solid; border-left: #4f93e3 1px solid; border-bottom: #4f93e3 1px solid;" width="900px">
                <tr height="16px" style="background: url(./Images/gvGradient.gif) repeat-x center top #94b6e8; color: Black; padding: 8px 4px 8px 4px; border-bottom: solid 1px #4f93e3;">
                    <td style="height: 15px; text-align: center;" colspan="5">
                        <strong>SUMMARY</strong></td>
                </tr>
                <tr style="height: 13px; background-color: #edf5ff">
                    <td style="width: 479px">
                        <strong>Item / Area</strong></td>
                    <td style="width: 50px; text-align: center">
                    </td>
                    <td style="width: 63px; text-align: center;">
                        <strong>Possible Score</strong></td>
                    <td style="width: 44px; text-align: center">
                        <strong>Actual Score</strong></td>
                    <td style="width: 253px; text-align: center">
                        <strong>Rating %</strong></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: PaleGoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        1. Administrative Controls</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px;
                    text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center;">
                        <asp:Label ID="pAAScore1a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px;
                    text-align: center">
                        <asp:Label ID="pAAScore1b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center;">
                        <asp:Label ID="pAAScore1c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        2. Amenities / Offices</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore2a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore2b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore2c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        3. First Aid facilities</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore3a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore3b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore3c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        4. Site Conditions</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore4a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore4b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore4c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        5. Access / Egress</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore5a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore5b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore5c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        6. Mobile Plant</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore6a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center;">
                        <asp:Label ID="pAAScore6b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore6c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        7. Lifting Gear</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore7a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore7b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore7c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        8. Welding, Cutting, Grinding / Hot work</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore8a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore8b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore8c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        9. Cranage</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore9a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore9b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore9c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        10. Scaffolding</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore10a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore10b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore10c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        11. Electrical</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore11a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore11b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore11c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        12. Compressed Air</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore12a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore12b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore12c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        13. Hazardous Substances</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore13a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore13b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore13c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        14. Employee Pro-Active Approach</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore14a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore14b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore14c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        15. Employee Compliance</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore15a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore15b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore15c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        16. Permit to work / JSA</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore16a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore16b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore16c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        17. Confined Space</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore17a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore17b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore17c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        18. Lighting</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore18a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore18b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore18c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        19. Tagging</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore19a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore19b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore19c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        20. Waste Management</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore20a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore20b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore20c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                        21. EH&amp;S Training</td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScore21a_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScore21b_" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScore21c_" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: left">
                    </td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                        <strong>Total:</strong></td>
                    <td style="font-size: 8pt; width: 63px; text-align: center">
                        <asp:Label ID="pAAScoreTotala" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                        <asp:Label ID="pAAScoreTotalb" runat="server" Text="0" Width="40px"></asp:Label></td>
                    <td style="font-size: 8pt; width: 253px; text-align: center">
                        <asp:Label ID="pAAScoreTotalc" runat="server" Text="0%"></asp:Label></td>
                </tr>
                <tr style="color: #000000; height: 13px; background-color: palegoldenrod">
                    <td style="width: 479px; height: 13px; text-align: right">
                        </td>
                    <td style="font-size: 8pt; width: 50px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 63px; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 44px; color: #000000; height: 13px; text-align: center">
                    </td>
                    <td style="font-size: 8pt; width: 253px; height: 13px; text-align: center">
                    </td>
                </tr>
            </table>

            </td>
    </tr>
    
                <tr>
                <td style="text-align: center;">
                    <asp:Label ID="lblSave" runat="server" Font-Bold="True" ForeColor="Red" Text="" Font-Size="16pt" />
                </td>
                <td style="text-align: right;">
                
                  <dxe:ASPxButton id="btnSave" runat="server" Width="167px" ForeColor="Navy" Font-Bold="True" Height="77px"
                                    Text="Save KPI Record" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                                    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                                    CssPostfix="Office2003Blue" Wrap="True" OnClick="btnSave_Click">
                    <Paddings Padding="0px" PaddingLeft="0px" PaddingTop="0px" PaddingRight="0px" PaddingBottom="0px"></Paddings>

                    <ClientSideEvents Click="function(s, e) {
                        needToConfirm = false;
	                    e.processOnServer = confirm('Are you sure you wish to save your changes?');
                    }"></ClientSideEvents>
                    </dxe:ASPxButton>

                </td>
            </tr>
    
    </ContentTemplate>
            </asp:UpdatePanel>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="height: 17px; text-align: left">
            <strong><span style="font-size: 12pt; color: #000080">3. Ratings Information</span></strong></td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
            <br />
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 140px; height: 13px">
                        0 - Unacceptable</td>
                    <td style="width: 718px; height: 13px">
                        <strong>Cease work for whole crew / shut down equipment. </strong><font class="font5">
                            <br />
                        </font><span style="text-decoration:underline"><font class="font6"><strong>Based On</strong></font><font
                            class="font7">:</font></span><font class="font5">
                                <br />
                                Legislative major non-compliance. Potential for serious injury / major damage to
                                equipment or environment.
                                <br />
                                Stop work and call Inspector.<br />
                                <br />
                            </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        1 - Poor</td>
                    <td style="width: 718px">
                        <strong>Immediate action required / major non compliance.</strong><font class="font5">
                            <br />
                        </font><span style="text-decoration: underline"><font class="font6"><strong>Based On:</strong></font><font
                            class="font7"> </font></span><font class="font5">
                                <br />
                                Personnel, environment<span style="mso-spacerun: yes">&nbsp; </span>or equipment
                                exposed to significant risk.
                                <br />
                                Stop work and remediate, if required call an Inspector.<br />
                                <br />
                            </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        2 - Fair</td>
                    <td style="width: 718px">
                        <strong>Attention required before the end of the shift.</strong><font class="font5">
                            <br />
                        </font><span style="text-decoration: underline"><font class="font6"><strong>Based On:</strong></font><font
                            class="font7"> </font></span><font class="font5">
                                <br />
                                Likelihood of a high risk event occurring.
                                <br />
                                If there is no plan to regain control, stop work until plan is implemented to allow
                                re-start of work.<br />
                                <br />
                            </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        3 - Acceptable</td>
                    <td style="width: 718px">
                        <strong>Attention required before the end of the shift.</strong><font class="font5">
                            <br />
                        </font><span style="text-decoration: underline"><font class="font6"><strong>Based On:</strong></font><font
                            class="font7"> </font></span><font class="font5">
                                <br />
                                Likelihood of a high risk event occurring.
                                <br />
                                Work can continue, providing a plan is being implemented to manage improvement.<br />
                                <br />
                            </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px; height: 52px;">
                        4 - Good</td>
                    <td style="width: 718px; height: 52px;">
                        <strong>Acceptable, minimal rectification required to own standards, if any.</strong><font
                            class="font5">
                            <br />
                        </font><font class="font6"><span style="text-decoration: underline"><strong>Based On:</strong></span></font><font
                            class="font5">
                            <br />
                            Meets all Alcoa and Legislative requirements.<br />
                            <br />
                        </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        5 - Excellent</td>
                    <td style="width: 718px">
                        <strong>No action required, best practice achieved.</strong><font
                            class="font5">
                            <br />
                        </font><font class="font6"><span style="text-decoration: underline"><strong>Based On:</strong></span></font><font
                            class="font5">
                            <br />
                            Clearly demonstrates best practice.<br />
                            Record the best practice and evaluate potential to transfer to standard practice.<br />
                            <br />
                        </font>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px; height: 13px;">
                        *</td>
                    <td style="width: 718px; height: 13px;">
                        Not Applicable N/A</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
            <br />
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
            nb. All elements of this audit are weighted using a scale from 1 - 10. The purpose
            of this weighting is to expose areas of vulnerability. This audit scrutinises both
            behavioural aspects of employees and the more traditional areas of environmental,
            health and safety compliance.</td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
        

                 
            
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>

    <data:EntityDataSource ID="SitesDataSource2" runat="server"
    ProviderName="SitesProvider"
    EntityTypeName="KaiZen.CSMS.Sites, KaiZen.CSMS"
    EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll"
    Sort="SiteName ASC" Filter="IsVisible = True"
/> 

            <data:SitesDataSource ID="SitesDataSource" runat="server" EnableDeepLoad="False"
                EnableSorting="true" SelectMethod="GetAll">
            </data:SitesDataSource>
            <data:TwentyOnePointAuditQtrDataSource ID="topaQtr" runat="server" EnableDeepLoad="False"
                EnableSorting="true" SelectMethod="GetAll"></data:TwentyOnePointAuditQtrDataSource>
                
               
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
        <br />
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
        </td>
    </tr>
    <tr>
        <td class="pageName" colspan="8" style="text-align: left">
        </td>
    </tr>
</table>

<dxpc:aspxpopupcontrol id="ASPxPopupControl1" runat="server" cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
    csspostfix="Office2003Blue" enablehottrack="False" headertext="Warning" 
    height="111px" modal="True" popuphorizontalalign="WindowCenter"
    popupverticalalign="WindowCenter" width="439px" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
<ContentCollection>
<dxpc:PopupControlContentControl runat="server"><dxe:ASPxLabel runat="server" Text="You can not enter monthly KPI information for a future month." Height="30px" Font-Size="14px" ID="ASPxLabel1">
<Border BorderColor="White" BorderWidth="10px"></Border>
</dxe:ASPxLabel>
</dxpc:PopupControlContentControl>
</ContentCollection>

<HeaderStyle>
<Paddings PaddingRight="6px"></Paddings>
</HeaderStyle>

</dxpc:aspxpopupcontrol>
<dxcb:ASPxCallback ID="ASPxCallback1" runat="server">
</dxcb:ASPxCallback>
