<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Other_ExportButtonsPivot" Codebehind="ExportButtonsPivot.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPivotGrid.Export" TagPrefix="dx" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
    <contenttemplate>
    <div style="padding-top: 2px">
        <TABLE style="HEIGHT: 20px" cellSpacing=0 cellPadding=0 border=0>
        <TBODY><TR>
        <TD align="right" style="VERTICAL-ALIGN: middle; padding-right: 3px; text-align:-moz-right; text-align:right;" vAlign=middle><STRONG>Export:</STRONG></TD>
        <TD align="right" style="VERTICAL-ALIGN: middle; padding-right: 2px; text-align:-moz-right; text-align:right;" vAlign=middle>
            <asp:ImageButton id="imgBtnExcel" onclick="imgBtnExcel_Click" runat="server" ToolTip="Export table to Microsoft Excel (XLS)" ImageUrl="~/Images/ExportLogos/excel.gif"></asp:ImageButton>
            <asp:ImageButton id="imgBtnWord" onclick="imgBtnWord_Click" runat="server" ToolTip="Export table to Microsoft Word Document (RTF)" ImageUrl="~/Images/ExportLogos/word.gif"></asp:ImageButton>
            <asp:ImageButton id="imgBtnPdf" onclick="imgBtnPdf_Click" runat="server" ToolTip="Export table to Adobe Acrobat (PDF)" ImageUrl="~/Images/ExportLogos/pdf.gif"></asp:ImageButton>
            </TD>
        </TR></TBODY>
        </TABLE></div>
</contenttemplate>
<Triggers>
    <asp:PostBackTrigger ControlID="imgBtnExcel"></asp:PostBackTrigger>
    <asp:PostBackTrigger ControlID="imgBtnWord"></asp:PostBackTrigger>
    <asp:PostBackTrigger ControlID="imgBtnPdf"></asp:PostBackTrigger>
</Triggers>
</asp:UpdatePanel>

<dx:aspxpivotgridexporter ID="gridPivotExporter" runat="server" ASPxPivotGridID="gridPivot" runat="server"></dx:aspxpivotgridexporter>
