﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.Data.SqlClient;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;


public class ValidityData
{

    private static String _connectionString;
    private static Boolean _initialized;

    public static void Initialize()
    {
        // Initialize data source. Use "Northwind" connection string from configuration.

        if (ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"] == null ||
            ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString.Trim() == "")
        {
            throw new Exception("A connection string named 'AdventureWorksConnectionString' with a valid connection string " +
                                "must exist in the <connectionStrings> configuration section for the application.");
        }

        _connectionString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

        _initialized = true;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public static DataTable GetValidity(Int32 startRecord, Int32 maxRecords, String sortColumns)
    {
        VerifySortColumns(sortColumns);

        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;
       // maxRecords = 10;
        int filterCount = 0;


        if (HttpContext.Current.Session["CompanyFilter"] != null)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["CompanyFilter"]) != -1)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + " CompanyId = " + Convert.ToString(HttpContext.Current.Session["CompanyFilter"]);
                filterCount++;
            }
        }

        if (HttpContext.Current.Session["SiteFilter"] != null)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["SiteFilter"]) > 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + " SiteId = " + Convert.ToString(HttpContext.Current.Session["SiteFilter"]);
                filterCount++;
            }
            else if (Convert.ToInt32(HttpContext.Current.Session["SiteFilter"]) < 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + "SiteId IN (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*"+ Convert.ToString(HttpContext.Current.Session["SiteFilter"]) +"))   " ;
                filterCount++;
            }
            
        }


        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            //whereCondition =" where "+ field + " = " + value;
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["SiteIdVal"] != null)
        {
            field = "SiteId";
            value = Convert.ToString(HttpContext.Current.Session["SiteIdVal"]);
            if (Convert.ToInt32(value) > 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else if (Convert.ToInt32(value) < 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + value + ")) ";
                filterCount++;
                // (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*@SiteId)) 
            }
        }
        if (HttpContext.Current.Session["COURSE_NAMEVal"] != null)
        {
            field = "COURSE_NAME";
            value = "'" + Convert.ToString(HttpContext.Current.Session["COURSE_NAMEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["FULL_NAMEVal"] != null)
        {
            field = "FULL_NAME";
            value = Convert.ToString(HttpContext.Current.Session["FULL_NAMEVal"]);
            if (value != "")
            {
                value = "'%" + value + "%'";
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " like " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["FULL_NAMEVal"] = null;
            }
        }
        if (HttpContext.Current.Session["COURSE_END_DATEVal"] != null)
        {
            field = "COURSE_END_DATE";
            value = "'" + Convert.ToString(HttpContext.Current.Session["COURSE_END_DATEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["EXPIRATION_DATEVal"] != null)
        {
            field = "EXPIRATION_DATE";
            value = "'" + Convert.ToString(HttpContext.Current.Session["EXPIRATION_DATEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["ExpiresInVal"] != null)
        {
            if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "n/a")
            {
                field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                           " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                           " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + "=''";
                filterCount++;
            }
            else
            {
                if (HttpContext.Current.Session["ExpiresInVal"].ToString() == ">60")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    whereCondition = whereCondition + field + value;
                    filterCount++;
                }
                else if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "<=60")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    whereCondition = whereCondition + field + value + " and " + field + "<>''";
                    filterCount++;
                }
                if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "Expired")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    
                    string extraCondition = " and (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'  " +
                                            " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'   " +
                                            " ELSE 'Yes' END)='Yes'";
                    whereCondition = whereCondition + field + "<=60" + " and " + field + "<>''" + extraCondition;

                    filterCount++;
                }

            }
        }
        if (HttpContext.Current.Session["ExpiredVal"] != null)
        {
          
            field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'  " +
                    " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'   " +
                    " ELSE 'Yes' END)";
            value = "'" + Convert.ToString(HttpContext.Current.Session["ExpiredVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
           
        }



        string sqlSortColumn = "ExpiresIn DESC";

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;

        if (sqlSortColumn.Contains("CompanyId"))
        {
            sqlSortColumn = sqlSortColumn.Replace("CompanyId", "CompanyName");
        }
        else if (sqlSortColumn.Contains("SiteId"))
        {
            sqlSortColumn = sqlSortColumn.Replace("SiteId", "SiteName");
        }
        else if (sqlSortColumn.Contains("Expired"))
        {
            string strExpired=" (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-' "+
                                " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No' "+
                                "ELSE 'Yes' END) ";

            sqlSortColumn = sqlSortColumn.Replace("Expired", strExpired);
        }
        else if (sqlSortColumn.Contains("ExpiresIn"))
        {
            string strExpired = " (CASE WHEN ((DATEDIFF(day, Getdate(),"+
                                   " EXPIRATION_DATE) IS NULL)) THEN '-' "+
                                   "  WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) "+
                                   " THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  "+
                                       " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) "+
                                  "END) ";

            sqlSortColumn = sqlSortColumn.Replace("ExpiresIn", strExpired);
        }
        //else
        //{
        //    //sqlSortColumn = "B." + sqlSortColumn;
        //}

        

        String sqlColumns = "[ContractId], [CompanyId], [SiteId], [Year], [January], [February], [March],[April], [May], [June],"+
                                    "[July], [August], [September], [October], [November], [December], "+
                                    "[YearCount],[LocationSponsorUserId], [LocationSpaUserId], [ModifiedDate],[ModifiedBy]";
        String sqlTable = "[ContractReviewsRegister]";        

        String sqlQuery = "SELECT ROW_NUMBER() OVER (ORDER BY " + sqlSortColumn + ") AS rownumber," +
                            " PERSON_ID, FULL_NAME, EMPL_ID, CompanyName,SiteName,"+
                            " COURSE_NAME, COURSE_END_DATE,CompanyId,SiteId," +
                            "convert(varchar, EXPIRATION_DATE, 103) as EXPIRATION_DATE,"+
                           " (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'   "+       
                            " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)   "+       
                             "    ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END) ExpiresIn, "+         
                   
                            "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'   "+       
                            " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'     "+     
                             "    ELSE 'Yes' END) Expired, "+
        
                                " VENDOR_NO "+
                               "  FROM Training_Management_Validity " + whereCondition;

        if (!String.IsNullOrEmpty(sortColumns))
            sqlSortColumn = sortColumns;

        String sqlCommand = String.Empty;


        if (HttpContext.Current.Session["ShowAll"] != null)
        {
            if (Convert.ToBoolean(HttpContext.Current.Session["ShowAll"]) == true)
            {
                sqlCommand = String.Format("SELECT * FROM({0}) AS foo ", sqlQuery);

            }
            else
            {
                sqlCommand = String.Format(
               "SELECT * FROM({0}) AS foo " +
               "WHERE rownumber >= {1} AND rownumber <= {2}",
               sqlQuery,
               startRecord + 1, startRecord + maxRecords
                );
            }

        }
        else
        {
            sqlCommand = String.Format(
               "SELECT * FROM({0}) AS foo " +
               "WHERE rownumber >= {1} AND rownumber <= {2}",
               sqlQuery,
               startRecord + 1, startRecord + maxRecords
            );
        }




        SqlConnection conn = new SqlConnection(_connectionString);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        DataSet ds = new DataSet();

        try
        {
            conn.Open();
            da.Fill(ds, "ValidityData");
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        if (ds.Tables["ValidityData"] != null)
            return ds.Tables["ValidityData"];

        return null;
    }


    // columns are specified in the sort expression to avoid a SQL Injection attack.

    private static void VerifySortColumns(string sortColumns)
    {
        sortColumns = sortColumns.ToLowerInvariant().Replace(" asc", String.Empty).Replace(" desc", String.Empty);
        String[] columnNames = sortColumns.Split(',');

        foreach (String columnName in columnNames)
        {

            switch (columnName.Trim().ToLowerInvariant())
            {
                case "siteid":
                case "companyname":
                case "companyid":
                case "course_name":
                case "full_name":               
                case "course_end_date":
                case "expiration_date":
                case "expiresin":
                case "expired":
                case "":
                    break;
                default:
                    throw new ArgumentException("SortColumns contains an invalid column name.");
            }
        }
    }

    public static Int32 GetValidityCount()
    {
        if (!_initialized) { Initialize(); }

        string field = string.Empty;
        string value = string.Empty;
        string whereCondition = string.Empty;
        // maxRecords = 10;
        int filterCount = 0;

        if (HttpContext.Current.Session["CompanyFilter"] != null)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["CompanyFilter"]) != -1)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + " CompanyId = " + Convert.ToString(HttpContext.Current.Session["CompanyFilter"]);
                filterCount++;
            }
        }

        if (HttpContext.Current.Session["SiteFilter"] != null)
        {
            if (Convert.ToInt32(HttpContext.Current.Session["SiteFilter"]) > 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + " SiteId = " + Convert.ToString(HttpContext.Current.Session["SiteFilter"]);
                filterCount++;
            }
            else if (Convert.ToInt32(HttpContext.Current.Session["SiteFilter"]) < 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + "SiteId IN (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + Convert.ToString(HttpContext.Current.Session["SiteFilter"]) + "))   ";
                filterCount++;
            }

        }

        if (HttpContext.Current.Session["CompanyIdVal"] != null)
        {
            field = "CompanyId";
            value = Convert.ToString(HttpContext.Current.Session["CompanyIdVal"]);
            //whereCondition =" where "+ field + " = " + value;
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["SiteIdVal"] != null)
        {
            field = "SiteId";
            value = Convert.ToString(HttpContext.Current.Session["SiteIdVal"]);
            if (Convert.ToInt32(value) > 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " = " + value;
                filterCount++;
            }
            else if (Convert.ToInt32(value) < 0)
            {
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " in  (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*" + value + ")) ";
                filterCount++;
                // (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*@SiteId)) 
            }
        }
        if (HttpContext.Current.Session["COURSE_NAMEVal"] != null)
        {
            field = "COURSE_NAME";
            value = "'" + Convert.ToString(HttpContext.Current.Session["COURSE_NAMEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["FULL_NAMEVal"] != null)
        {
            field = "FULL_NAME";
            value = Convert.ToString(HttpContext.Current.Session["FULL_NAMEVal"]);
            if (value != "")
            {
                value = "'%" + value + "%'";
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field + " like " + value;
                filterCount++;
            }
            else
            {
                HttpContext.Current.Session["FULL_NAMEVal"] = null;
            }
        }
        if (HttpContext.Current.Session["COURSE_END_DATEVal"] != null)
        {
            field = "COURSE_END_DATE";
            value = "'" + Convert.ToString(HttpContext.Current.Session["COURSE_END_DATEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["EXPIRATION_DATEVal"] != null)
        {
            field = "EXPIRATION_DATE";
            value = "'" + Convert.ToString(HttpContext.Current.Session["EXPIRATION_DATEVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }
        if (HttpContext.Current.Session["ExpiresInVal"] != null)
        {
            if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "n/a")
            {
                field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                           " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                           " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                else { whereCondition = whereCondition + " where "; }
                whereCondition = whereCondition + field  + "=''";
                filterCount++;
            }
            else 
            {
                if (HttpContext.Current.Session["ExpiresInVal"].ToString() == ">60")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    whereCondition = whereCondition + field + value;
                    filterCount++;
                }
                else if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "<=60")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    whereCondition = whereCondition + field + value + " and " + field + "<>''";
                    filterCount++;
                }
                if (HttpContext.Current.Session["ExpiresInVal"].ToString() == "Expired")
                {
                    field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'        " +
                               " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)  " +
                               " ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END)";
                    value = Convert.ToString(HttpContext.Current.Session["ExpiresInVal"]);
                    if (filterCount > 0) { whereCondition = whereCondition + " and "; }
                    else { whereCondition = whereCondition + " where "; }
                    string extraCondition = " and (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'  " +
                                            " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'   " +
                                            " ELSE 'Yes' END)='Yes'";
                    whereCondition = whereCondition + field + "<=60" + " and " + field + "<>''" + extraCondition;

                    filterCount++;
                }
               
            }
        }
        if (HttpContext.Current.Session["ExpiredVal"] != null)
        {
            field = "(CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'  " +
                    " WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'   " +
                    " ELSE 'Yes' END)";
            value = "'" + Convert.ToString(HttpContext.Current.Session["ExpiredVal"]) + "'";
            if (filterCount > 0) { whereCondition = whereCondition + " and "; }
            else { whereCondition = whereCondition + " where "; }
            whereCondition = whereCondition + field + " = " + value;
            filterCount++;
        }

        string sqlCommand = "SELECT COUNT (*) FROM [Training_Management_Validity]" + whereCondition;

        SqlConnection conn = new SqlConnection(_connectionString);
        SqlCommand command = new SqlCommand(sqlCommand, conn);
        SqlDataAdapter da = new SqlDataAdapter(sqlCommand, conn);

        Int32 result = 0;

        try
        {
            conn.Open();
            result = (Int32)command.ExecuteScalar();
            
        }
        catch (SqlException e)
        {
            // Handle exception.
        }
        finally
        {
            conn.Close();
        }

        return result;
    }

   
 

}







//String sqlColumns = "[ContractId], [CompanyId], [SiteId], [Year], [January], [February], [March],[April], [May], [June],"+
//                                    "[July], [August], [September], [October], [November], [December], "+
//                                    "[YearCount],[LocationSponsorUserId], [LocationSpaUserId], [ModifiedDate],[ModifiedBy]";
//        String sqlTable = "[ContractReviewsRegister]";
//        String sqlSortColumn = "B.CompanyName";

//        String sqlQuery = "BEGIN TRAN     " +     
//        "CREATE TABLE #Training_Management_Validity " +         
//        "( "+          
//        " PERSON_ID INT,    "+      
//        " FULL_NAME VARCHAR(MAX),   "+       
//         " CWK_NBR VARCHAR(MAX), "+          
//         " COURSE_NAME NVARCHAR(80),  "+        
//        " COURSE_END_DATE DATETIME,          "+
//         " EXPIRATION_DATE DATETIME,          "+
//         "  VENDOR_NO VARCHAR(MAX),  "+        
//        " CompanyId INT,  "+        
//        " SiteId INT   "+       
//       " )     "+     
//       " COMMIT   "+       
          
//       " IF(@CompanyId=-1)  "+        
//       " BEGIN     "+     
          
//       " IF(@SITEID=0)   "+       
          
//       " BEGIN        "+  
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME, "+         
//            "     COURSE_END_DATE,EXPIRATION_DATE,      "+    
//             "    VENDOR_NO,        "+  
//              "   CompanyId,SiteId)  "+        
          
                             
//       " SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,    "+      
//        "  COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId   "+       
//      "  FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A   "+        
//       " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B   "+       
//       " ON A.PERSON_ID = B.PERSON_ID        "+  
//       " Left Outer Join CompanyVendor C   "+       
//      "  ON B.Vendor_No = C.Vendor_Number    "+       
//       " Left Outer Join Companies D          "+
//        "On C.Tax_Registration_Number=D.CompanyABN "+          
//        "Left Outer Join Sites E           "+
//        "on B.Location_Code=E.SiteNameHr     "+     
//        "WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE) "+         
//         " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]      "+     
//        " WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME) "+         
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)   "+       
//      "  AND C.Vendor_Number Is Not Null          "+
//      "  AND E.SiteId Is Not Null          "+
//      "  AND D.CompanyId Is Not Null         "+ 
          
//       " END   "+       
          
//       " ELSE IF(@SiteId>0)  "+        
          
//       " BEGIN          "+
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,          "+
//        "         COURSE_END_DATE,EXPIRATION_DATE,          "+
//         "        VENDOR_NO,          "+
//          "       CompanyId,SiteId)          "+
          
                             
//      "  SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,          "+
//       "   COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId          "+
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A           "+
//       " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B          "+
//       " ON A.PERSON_ID = B.PERSON_ID          "+
//       " Left Outer Join CompanyVendor C          "+
//       " ON B.Vendor_No = C.Vendor_Number       "+    
//       " Left Outer Join Companies D          "+
//       " On C.Tax_Registration_Number=D.CompanyABN          "+
//       " Left Outer Join Sites E           "+
//       " on B.Location_Code=E.SiteNameHr          "+
//       " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE)         "+ 
//       "  FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]           "+
//       "  WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)          "+
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)          "+
//       " AND C.Vendor_Number Is Not Null "+         
//       " AND E.SiteId Is Not Null      "+    
//       " AND D.CompanyId Is Not Null "+         
//       " AND E.SiteId=@SiteId     "+     
          
//       " END          "+
          
        
//       " ELSE IF(@SiteId<0)   "+       
         
//       " BEGIN          "+
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,          "+
//       "          COURSE_END_DATE,EXPIRATION_DATE,          "+
//        "         VENDOR_NO,          "+
//         "        CompanyId,SiteId)   "+
          
                             
//        " SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,          "+
//         " COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId          "+
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A   "+        
//        " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B          "+
//        " ON A.PERSON_ID = B.PERSON_ID          "+
//        " Left Outer Join CompanyVendor C          "+
//        " ON B.Vendor_No = C.Vendor_Number       "+    
//        " Left Outer Join Companies D          "+
//        " On C.Tax_Registration_Number=D.CompanyABN          "+
//        " Left Outer Join Sites E           "+
//        " on B.Location_Code=E.SiteNameHr          "+
//        " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE)       "+   
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]           "+
//       "  WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)          "+
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)          "+
//       " AND C.Vendor_Number Is Not Null     "+     
//       " AND E.SiteId Is Not Null          "+
//       " AND D.CompanyId Is Not Null           "+
//       " AND E.SiteId IN (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*@SiteId))  "+        
          
//       " END          "+
          
//       " END          "+
          
//       " ELSE          "+
          
//       " BEGIN          "+
          
//       " IF(@SITEID=0)    "+      
          
//       " BEGIN          "+
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,          "+
//        "         COURSE_END_DATE,EXPIRATION_DATE,          "+
//         "        VENDOR_NO,          "+
//          "       CompanyId,SiteId)          "+
          
                             
//       " SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,          "+
//        "  COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId          "+
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A           "+
//       " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B          "+
//       " ON A.PERSON_ID = B.PERSON_ID          "+
//       " Left Outer Join CompanyVendor C          "+
//       " ON B.Vendor_No = C.Vendor_Number       "+    
//       " Left Outer Join Companies D          "+
//       " On C.Tax_Registration_Number=D.CompanyABN          "+
//       " Left Outer Join Sites E           "+
//       " on B.Location_Code=E.SiteNameHr          "+
//       " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE)         "+ 
//       "  FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]           "+
//       "  WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)     "+     
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)          "+
//       " AND C.Vendor_Number Is Not Null     "+     
//       " AND E.SiteId Is Not Null          "+
//       " AND D.CompanyId Is Not Null           "+
//       " AND D.CompanyId=@CompanyId          "+
          
//        " END          "+
          
//       " ELSE IF(@SiteId>0)          "+
          
//       " BEGIN          "+
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,          "+
//        "         COURSE_END_DATE,EXPIRATION_DATE,          "+
//         "        VENDOR_NO,          "+
//          "       CompanyId,SiteId)    "+      
          
                             
//        " SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,    "+      
//        "  COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId "+         
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A           "+
//       " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B          "+
//       " ON A.PERSON_ID = B.PERSON_ID          "+
//       " Left Outer Join CompanyVendor C          "+
//       " ON B.Vendor_No = C.Vendor_Number       "+    
//       " Left Outer Join Companies D          "+
//       " On C.Tax_Registration_Number=D.CompanyABN          "+
//       " Left Outer Join Sites E           "+
//       " on B.Location_Code=E.SiteNameHr          "+
//       " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE)   "+      
//       "  FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]           "+
//       "  WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)          "+
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)          "+
//       " AND C.Vendor_Number Is Not Null     "+     
//       " AND E.SiteId Is Not Null          "+
//       " AND D.CompanyId Is Not Null      "+    
//       " AND E.SiteId=@SiteId           "+
//       " AND D.CompanyId=@CompanyId          "+
          
//      "  END          "+
          
//       " ELSE IF(@SiteId<0)    "+      
          
//       " BEGIN          "+
          
//       " INSERT #Training_Management_Validity(PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,          "+
//        "         COURSE_END_DATE,EXPIRATION_DATE,          "+
//        "         VENDOR_NO,          "+
//         "        CompanyId,SiteId)          "+
          
                             
//       " SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.CWK_NBR, COURSE_NAME,          "+
//        "  COURSE_END_DATE,EXPIRATION_DATE, B.VENDOR_NO,D.CompanyId,E.SiteId          "+
//       " FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE] A           "+
//       " LEFT OUTER JOIN dbo.XXHR_HR_TO_CSMS_CWK_TEMPTABLE B          "+
//       " ON A.PERSON_ID = B.PERSON_ID          "+
//       " Left Outer Join CompanyVendor C          "+
//       " ON B.Vendor_No = C.Vendor_Number       "+    
//       " Left Outer Join Companies D          "+
//       " On C.Tax_Registration_Number=D.CompanyABN          "+
//       " Left Outer Join Sites E           "+
//       " on B.Location_Code=E.SiteNameHr          "+
//       " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE)  "+        
//       "  FROM dbo.[XXHR_HR_TO_CSMS_CWK_ENR_TRNG_TEMPTABLE]    "+       

//       "  WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)          "+
//       " AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)          "+
//       " AND C.Vendor_Number Is Not Null     "+     
//       " AND E.SiteId Is Not Null          "+
//       " AND D.CompanyId Is Not Null           "+
//       " AND E.SiteId IN (SELECT SITEID FROM REGIONSSITES WHERE REGIONID=(-1*@SiteId))           "+
//       " AND D.CompanyId=@CompanyId    "+      
          
//       " END          "+
          
//       " END          "+
          
          
          
//       " SELECT PERSON_ID,FULL_NAME,CWK_NBR,COURSE_NAME,convert(varchar, COURSE_END_DATE, 103) as COURSE_END_DATE,          "+
//        "          convert(varchar, EXPIRATION_DATE, 103) as EXPIRATION_DATE,        "+
          
//       " (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'          "+
//       "  WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int)          "+
//       "      ELSE CAST(DATEDIFF(day, Getdate(), EXPIRATION_DATE) as int) END) ExpiresIn,          "+
                   
//       " (CASE WHEN ((DATEDIFF(day, Getdate(), EXPIRATION_DATE) IS NULL)) THEN '-'  "+        
//       "  WHEN (DATEDIFF(day, Getdate(), EXPIRATION_DATE) > 0) THEN 'No'          "+
//       "      ELSE 'Yes' END) Expired,  "+         
          
//        "         VENDOR_NO,          "+
//        "         CompanyId,SiteId           "+
          
//        " FROM #Training_Management_Validity    "+      
//       " DROP TABLE #Training_Management_Validity " ;
