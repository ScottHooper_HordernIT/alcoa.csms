﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IUsersProcurementService
    {
        UsersProcurement Get(Expression<Func<UsersProcurement, bool>> where, List<Expression<Func<UsersProcurement, object>>> includes);
        List<UsersProcurement> GetMany(int? top, Expression<Func<UsersProcurement, bool>> where, Func<IQueryable<UsersProcurement>, IOrderedQueryable<UsersProcurement>> orderBy, List<Expression<Func<UsersProcurement, object>>> includes);
        List<UsersProcurement> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersProcurement, bool>> where, Func<IQueryable<UsersProcurement>, IOrderedQueryable<UsersProcurement>> orderBy, List<Expression<Func<UsersProcurement, object>>> includes, out int pageCount, out int totalRecords);
        List<UsersProcurement> GetAllReaderAdmin();
        int? GetCount(Expression<Func<UsersProcurement, bool>> where, List<Expression<Func<UsersProcurement, object>>> includes);
        void Insert(UsersProcurement item, bool saveChanges = true);
        void Insert(UsersProcurement item);
        void Update(UsersProcurement item, bool saveChanges = true);
        void Update(UsersProcurement item);
        void Delete(UsersProcurement item, bool saveChanges = true);
        void Delete(UsersProcurement item);
        void Delete(int UsersProcurementId, bool saveChanges);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class UsersProcurementService : IUsersProcurementService
    {
        private readonly IRepository<UsersProcurement> _repository;

        public UsersProcurementService(IRepository<UsersProcurement> repository)
        {
            this._repository = repository;
        }

        public UsersProcurement Get(Expression<Func<UsersProcurement, bool>> where, List<Expression<Func<UsersProcurement, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<UsersProcurement> GetMany(int? top, Expression<Func<UsersProcurement, bool>> where, Func<IQueryable<UsersProcurement>, IOrderedQueryable<UsersProcurement>> orderBy, List<Expression<Func<UsersProcurement, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<UsersProcurement> GetManyPaged(int pageNumber, int pageSize, Expression<Func<UsersProcurement, bool>> where, Func<IQueryable<UsersProcurement>, IOrderedQueryable<UsersProcurement>> orderBy, List<Expression<Func<UsersProcurement, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<UsersProcurement, bool>> where, List<Expression<Func<UsersProcurement, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(UsersProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(UsersProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(UsersProcurement item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }


        public List<UsersProcurement> GetAllReaderAdmin()
        {
            //List<UsersProcurement> lst = _repository.GetMany(i=>i.SupervisorUserId == null || i.SupervisorUserId != null, null, null, null).ToList();
            //return _repository.GetMany(i=>i.User.RoleId == 1 || i.User.RoleId == 3, null, null, null).ToList();
            return _repository.GetMany(i => i.SupervisorUserId == null || i.SupervisorUserId != null, null, null, null).ToList();
        }
        public void Delete(UsersProcurement item)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");
            Delete(item.UsersProcurementId, true);
        }
        public void Delete(int usersProcurementId, bool saveChanges = true)
        {
            _repository.Delete(e => e.UsersProcurementId == usersProcurementId);

            if (saveChanges)
                _repository.SaveChanges();
        }
        public void Update(UsersProcurement item)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");
            item.ModifiedDate = DateTime.Now;
            _repository.Update(item);
            _repository.SaveChanges();
        }


        public void Insert(UsersProcurement item)
        {
            if (item == null)
                throw new ArgumentNullException("UsersProcurement");
            item.ModifiedDate = DateTime.Now;
            _repository.Insert(item);
            _repository.SaveChanges();
        }
    }

    #endregion
}
