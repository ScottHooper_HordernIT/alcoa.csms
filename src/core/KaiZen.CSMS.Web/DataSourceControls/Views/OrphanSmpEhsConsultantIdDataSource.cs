﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.OrphanSmpEhsConsultantIdProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(OrphanSmpEhsConsultantIdDataSourceDesigner))]
	public class OrphanSmpEhsConsultantIdDataSource : ReadOnlyDataSource<OrphanSmpEhsConsultantId>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdDataSource class.
		/// </summary>
		public OrphanSmpEhsConsultantIdDataSource() : base(new OrphanSmpEhsConsultantIdService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the OrphanSmpEhsConsultantIdDataSourceView used by the OrphanSmpEhsConsultantIdDataSource.
		/// </summary>
		protected OrphanSmpEhsConsultantIdDataSourceView OrphanSmpEhsConsultantIdView
		{
			get { return ( View as OrphanSmpEhsConsultantIdDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the OrphanSmpEhsConsultantIdDataSourceView class that is to be
		/// used by the OrphanSmpEhsConsultantIdDataSource.
		/// </summary>
		/// <returns>An instance of the OrphanSmpEhsConsultantIdDataSourceView class.</returns>
		protected override BaseDataSourceView<OrphanSmpEhsConsultantId, Object> GetNewDataSourceView()
		{
			return new OrphanSmpEhsConsultantIdDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the OrphanSmpEhsConsultantIdDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class OrphanSmpEhsConsultantIdDataSourceView : ReadOnlyDataSourceView<OrphanSmpEhsConsultantId>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanSmpEhsConsultantIdDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the OrphanSmpEhsConsultantIdDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public OrphanSmpEhsConsultantIdDataSourceView(OrphanSmpEhsConsultantIdDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal OrphanSmpEhsConsultantIdDataSource OrphanSmpEhsConsultantIdOwner
		{
			get { return Owner as OrphanSmpEhsConsultantIdDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal OrphanSmpEhsConsultantIdService OrphanSmpEhsConsultantIdProvider
		{
			get { return Provider as OrphanSmpEhsConsultantIdService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region OrphanSmpEhsConsultantIdDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the OrphanSmpEhsConsultantIdDataSource class.
	/// </summary>
	public class OrphanSmpEhsConsultantIdDataSourceDesigner : ReadOnlyDataSourceDesigner<OrphanSmpEhsConsultantId>
	{
	}

	#endregion OrphanSmpEhsConsultantIdDataSourceDesigner

	#region OrphanSmpEhsConsultantIdFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdFilter : SqlFilter<OrphanSmpEhsConsultantIdColumn>
	{
	}

	#endregion OrphanSmpEhsConsultantIdFilter

	#region OrphanSmpEhsConsultantIdExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanSmpEhsConsultantId"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanSmpEhsConsultantIdExpressionBuilder : SqlExpressionBuilder<OrphanSmpEhsConsultantIdColumn>
	{
	}
	
	#endregion OrphanSmpEhsConsultantIdExpressionBuilder		
}

