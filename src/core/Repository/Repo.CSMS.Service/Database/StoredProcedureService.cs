﻿using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.DAL.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IStoredProcedureService
    {
        List<f_FileDb_GetStatusFromResponseId_Result> SP_GetSafetyPlanStatuses(int responseId);
        List<f_DocumentsDownloadLog_MustReadDownloaded_APSS_Result> SP_GetDocumentsDownloadLogMustReadDownloadedAPSS(int companyId, int regionId);
        List<f_FileDb_VisibilitySafetyManagementPlan_Result> SP_GetCompanySiteCategoryStandardDetails(int companyId, string description);
        List<f_Sites_CompanySiteCategoryStandard_ByRegionId_Result> SP_CompanySiteCategoryStandard_ByRegionId(int regionId);
        List<f_Kpi_GetReport_Compliance_SQ_Result> SP_GetReport_Compliance_SQ(int regionId);
        List<f_KpiCompanySiteCategory_Compliance_Table_AA_Result> SP_Compliance_Table_AA(int regionId, int year, string companySiteCategoryId);
        List<f_KpiCompanySiteCategory_Compliance_Table_AS_Result> SP_Compliance_Table_AS(int siteId, int year, string companySiteCategoryId);
        List<f_KpiCompanySiteCategory_Compliance_Table_SS_Result> SP_Compliance_Table_SS(int companyId, int siteId, int year, string companySiteCategoryId);
        void SP_Compliance_Table_Batch(string yearIn, int monthIn); 

        List<Kpi_Result> SP_GetByCompanyIdSiteIdKpiDateTime(int companyId, int siteId, DateTime kpiDateTime);
        List<Kpi_Result> SP_GetByKpiId(int kpiId);
        int? SP_XXHR_PEOPLE_DETAILS_SelectMaxCwkNbr();
    }

    #endregion

    #region Interface Implementation

    public class StoredProcedureService : IStoredProcedureService
    {
        private readonly IRepository<User> _repository;

        public StoredProcedureService(IRepository<User> repository)
        {
            this._repository = repository;
        }

        public List<f_FileDb_GetStatusFromResponseId_Result> SP_GetSafetyPlanStatuses(int responseId)
        {
            return _repository.ExecuteCustomFunction<f_FileDb_GetStatusFromResponseId_Result>("f_FileDb_GetStatusFromResponseId", new ObjectParameter("ResponseId", responseId)).ToList();
        }

        public List<f_DocumentsDownloadLog_MustReadDownloaded_APSS_Result> SP_GetDocumentsDownloadLogMustReadDownloadedAPSS(int companyId, int regionId)
        {
            return _repository.ExecuteCustomFunction<f_DocumentsDownloadLog_MustReadDownloaded_APSS_Result>("f_DocumentsDownloadLog_MustReadDownloaded_APSS", new ObjectParameter("CompanyId", companyId), new ObjectParameter("RegionId", regionId)).ToList();
        }

        public List<f_FileDb_VisibilitySafetyManagementPlan_Result> SP_GetCompanySiteCategoryStandardDetails(int companyId, string description)
        {
            return _repository.ExecuteCustomFunction<f_FileDb_VisibilitySafetyManagementPlan_Result>("f_FileDb_VisibilitySafetyManagementPlan", new ObjectParameter("CompanyId", companyId), new ObjectParameter("Description", description)).ToList();
        }

        public List<f_Sites_CompanySiteCategoryStandard_ByRegionId_Result> SP_CompanySiteCategoryStandard_ByRegionId(int regionId)
        {
            return _repository.ExecuteCustomFunction<f_Sites_CompanySiteCategoryStandard_ByRegionId_Result>("f_Sites_CompanySiteCategoryStandard_ByRegionId", new ObjectParameter("RegionId", regionId)).ToList();
        }

        public List<f_Kpi_GetReport_Compliance_SQ_Result> SP_GetReport_Compliance_SQ(int regionId)
        {
            return _repository.ExecuteCustomFunction<f_Kpi_GetReport_Compliance_SQ_Result>("f_Kpi_GetReport_Compliance_SQ", new ObjectParameter("RegionId", regionId)).ToList();
        }

        public List<Kpi_Result> SP_GetByCompanyIdSiteIdKpiDateTime(int companyId, int siteId, DateTime kpiDateTime)
        {
            return _repository.ExecuteCustomFunction<Kpi_Result>("Kpi_GetByCompanyIdSiteIdKpiDateTime", new ObjectParameter("CompanyId", companyId), new ObjectParameter("SiteId", siteId), new ObjectParameter("KpiDateTime", kpiDateTime)).ToList();
        }

        public List<f_KpiCompanySiteCategory_Compliance_Table_AA_Result> SP_Compliance_Table_AA(int regionId, int year, string companySiteCategoryId)
        {
            return _repository.ExecuteCustomFunction<f_KpiCompanySiteCategory_Compliance_Table_AA_Result>("f_KpiCompanySiteCategory_Compliance_Table_AA", new ObjectParameter("RegionId", regionId), new ObjectParameter("Year", year), new ObjectParameter("CompanySiteCategoryId", companySiteCategoryId)).ToList();
        }

        public List<f_KpiCompanySiteCategory_Compliance_Table_AS_Result> SP_Compliance_Table_AS(int siteId, int year, string companySiteCategoryId)
        {
            return _repository.ExecuteCustomFunction<f_KpiCompanySiteCategory_Compliance_Table_AS_Result>("f_KpiCompanySiteCategory_Compliance_Table_AS", new ObjectParameter("SiteId", siteId), new ObjectParameter("Year", year), new ObjectParameter("CompanySiteCategoryId", companySiteCategoryId)).ToList();
        }

        public List<f_KpiCompanySiteCategory_Compliance_Table_SS_Result> SP_Compliance_Table_SS(int companyId, int siteId, int year, string companySiteCategoryId)
        {
            return _repository.ExecuteCustomFunction<f_KpiCompanySiteCategory_Compliance_Table_SS_Result>("f_KpiCompanySiteCategory_Compliance_Table_SS", new ObjectParameter("CompanyId", companyId), new ObjectParameter("SiteId", siteId), new ObjectParameter("Year", year), new ObjectParameter("CompanySiteCategoryId", companySiteCategoryId)).ToList();
        }

        public void SP_Compliance_Table_Batch(string year, int month)
        {
            _repository.ExecuteCustomFunction("f_KpiCompanySiteCategory_Compliance_Table_Batch", new ObjectParameter("YearIn", year), new ObjectParameter("MonthIn", month));
        } 

        public List<Kpi_Result> SP_GetByKpiId(int kpiId)
        {
            return _repository.ExecuteCustomFunction<Kpi_Result>("Kpi_GetByKpiId", new ObjectParameter("KpiId", kpiId)).ToList();
        }

        public int? SP_XXHR_PEOPLE_DETAILS_SelectMaxCwkNbr()
        {
            return _repository.ExecuteCustomFunction<int>("XXHR_PEOPLE_DETAILS_SelectMaxCwkNbr").FirstOrDefault();
        }
    }

    #endregion
}
