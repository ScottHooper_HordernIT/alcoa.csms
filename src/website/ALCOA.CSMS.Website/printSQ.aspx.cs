﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Data;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Services;

public partial class printSQ : System.Web.UI.Page
{
    Auth auth = new Auth();
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        {
            try
            {
                if (Request.QueryString["q"] == null) throw new Exception("No Questionnaire requested.");
                string _q = Request.QueryString["q"].ToString();
                int __q = Convert.ToInt32(_q);
                QuestionnaireService qService = new QuestionnaireService();
                Questionnaire q = qService.GetByQuestionnaireId(__q);
                if (q == null) throw new Exception("Questionnaire does not exist.");
                hlGoBack.Visible = true;
                hlGoBack.NavigateUrl = "SafetyPQ_QuestionnaireModify.aspx?q=" + _q;

                //always display questionnaire main & procurement questionnaire, even if incomplete...
                Control cQuestionnaire = LoadControl("~/UserControls/Main/SqQuestionnaire/QuestionnaireModify.ascx");
                phQuestionnaire.Controls.Add(cQuestionnaire);
                Control cProcurement = LoadControl("~/UserControls/Main/SqQuestionnaire/Questionnaire1Procurement.ascx");
                phProcurement.Controls.Add(cProcurement);
                Control cSupplier = LoadControl("~/UserControls/Main/SqQuestionnaire/Questionnaire2Supplier.ascx");
                phSupplier.Controls.Add(cSupplier);

                //show verification if it's required.
                bool MainCompleted = false;
                if (q.MainStatus == (int)QuestionnaireStatusList.BeingAssessed || q.MainStatus == (int)QuestionnaireStatusList.AssessmentComplete)
                {
                    MainCompleted = true;
                }
                if (MainCompleted || q.IsVerificationRequired)
                {
                    LoadVerification();
                }

                //if (q.InitialStatus == (int)QuestionnaireStatusList.AssessmentComplete)
                //{
                //    if (q.IsMainRequired == true)
                //    {
                //        Control cSupplier = LoadControl("~/UserControls/SafetyPQ_QuestionnaireMain.ascx");
                //        phSupplier.Controls.Add(cSupplier);
                //        if (q.MainStatus != (int)QuestionnaireStatusList.Incomplete)
                //        {

                //        }
                //        QuestionnaireVerificationResponseService qvrService = new QuestionnaireVerificationResponseService();
                //        TList<QuestionnaireVerificationResponse> qvrList = qvrService.GetByQuestionnaireId(__q);
                //        if (qvrList.Count > 0)
                //        {
                //            LoadVerification();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                lblError.Text = "Error Occured: " + ex.Message.ToString();
            }
        }
    }
    private void LoadVerification()
    {
        Control cVerification = LoadControl("~/UserControls/Main/SqQuestionnaire/Questionnaire3Verification.ascx");
        phVerification.Controls.Add(cVerification);
        Control cVerification1 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_1.ascx");
        phVerification1.Controls.Add(cVerification1);
        Control cVerification2 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_2.ascx");
        phVerification2.Controls.Add(cVerification2);
        Control cVerification4 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_4.ascx");
        phVerification4.Controls.Add(cVerification4);
        Control cVerification5 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_5.ascx");
        phVerification5.Controls.Add(cVerification5);
        Control cVerification6 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_6.ascx");
        phVerification6.Controls.Add(cVerification6);
        Control cVerification7 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_7.ascx");
        phVerification7.Controls.Add(cVerification7);
        Control cVerification8 = LoadControl("~/UserControls/Main/SqQuestionnaire/Verification/QuestionnaireVerification_8.ascx");
        phVerification8.Controls.Add(cVerification8);
    }
}
