﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISqExemptionService
    {
        SqExemption Get(Expression<Func<SqExemption, bool>> where, List<Expression<Func<SqExemption, object>>> includes);
        List<SqExemption> GetMany(int? top, Expression<Func<SqExemption, bool>> where, Func<IQueryable<SqExemption>, IOrderedQueryable<SqExemption>> orderBy, List<Expression<Func<SqExemption, object>>> includes);
        List<SqExemption> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SqExemption, bool>> where, Func<IQueryable<SqExemption>, IOrderedQueryable<SqExemption>> orderBy, List<Expression<Func<SqExemption, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SqExemption, bool>> where, List<Expression<Func<SqExemption, object>>> includes);
        void Insert(SqExemption item, bool saveChanges = true);
        void Update(SqExemption item, bool saveChanges = true);
        void Delete(SqExemption item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SqExemptionService : ISqExemptionService
    {
        private readonly IRepository<SqExemption> _repository;

        public SqExemptionService(IRepository<SqExemption> repository)
        {
            this._repository = repository;
        }

        public SqExemption Get(Expression<Func<SqExemption, bool>> where, List<Expression<Func<SqExemption, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SqExemption> GetMany(int? top, Expression<Func<SqExemption, bool>> where, Func<IQueryable<SqExemption>, IOrderedQueryable<SqExemption>> orderBy, List<Expression<Func<SqExemption, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SqExemption> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SqExemption, bool>> where, Func<IQueryable<SqExemption>, IOrderedQueryable<SqExemption>> orderBy, List<Expression<Func<SqExemption, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SqExemption, bool>> where, List<Expression<Func<SqExemption, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(SqExemption item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemption");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(SqExemption item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemption");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(SqExemption item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SqExemption");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
