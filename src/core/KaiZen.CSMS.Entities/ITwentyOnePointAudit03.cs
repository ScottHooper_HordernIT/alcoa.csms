﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'TwentyOnePointAudit_03' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ITwentyOnePointAudit03 
	{
		/// <summary>			
		/// ID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "TwentyOnePointAudit_03"</remarks>
		System.Int32 Id { get; set; }
				
		
		
		/// <summary>
		/// Achieved3a : 
		/// </summary>
		System.Int32?  Achieved3a  { get; set; }
		
		/// <summary>
		/// Achieved3b : 
		/// </summary>
		System.Int32?  Achieved3b  { get; set; }
		
		/// <summary>
		/// Achieved3c : 
		/// </summary>
		System.Int32?  Achieved3c  { get; set; }
		
		/// <summary>
		/// Achieved3d : 
		/// </summary>
		System.Int32?  Achieved3d  { get; set; }
		
		/// <summary>
		/// Achieved3e : 
		/// </summary>
		System.Int32?  Achieved3e  { get; set; }
		
		/// <summary>
		/// Achieved3f : 
		/// </summary>
		System.Int32?  Achieved3f  { get; set; }
		
		/// <summary>
		/// Achieved3g : 
		/// </summary>
		System.Int32?  Achieved3g  { get; set; }
		
		/// <summary>
		/// Achieved3h : 
		/// </summary>
		System.Int32?  Achieved3h  { get; set; }
		
		/// <summary>
		/// Achieved3i : 
		/// </summary>
		System.Int32?  Achieved3i  { get; set; }
		
		/// <summary>
		/// Observation3a : 
		/// </summary>
		System.String  Observation3a  { get; set; }
		
		/// <summary>
		/// Observation3b : 
		/// </summary>
		System.String  Observation3b  { get; set; }
		
		/// <summary>
		/// Observation3c : 
		/// </summary>
		System.String  Observation3c  { get; set; }
		
		/// <summary>
		/// Observation3d : 
		/// </summary>
		System.String  Observation3d  { get; set; }
		
		/// <summary>
		/// Observation3e : 
		/// </summary>
		System.String  Observation3e  { get; set; }
		
		/// <summary>
		/// Observation3f : 
		/// </summary>
		System.String  Observation3f  { get; set; }
		
		/// <summary>
		/// Observation3g : 
		/// </summary>
		System.String  Observation3g  { get; set; }
		
		/// <summary>
		/// Observation3h : 
		/// </summary>
		System.String  Observation3h  { get; set; }
		
		/// <summary>
		/// Observation3i : 
		/// </summary>
		System.String  Observation3i  { get; set; }
		
		/// <summary>
		/// TotalScore : 
		/// </summary>
		System.Int32?  TotalScore  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _twentyOnePointAuditPoint03Id
		/// </summary>	
		TList<TwentyOnePointAudit> TwentyOnePointAuditCollection {  get;  set;}	

		#endregion Data Properties

	}
}


