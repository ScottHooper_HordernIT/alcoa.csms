﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrphanQuestionnaireProcurementFunctionalManagerProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class OrphanQuestionnaireProcurementFunctionalManagerProviderBaseCore : EntityViewProviderBase<OrphanQuestionnaireProcurementFunctionalManager>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt;"/></returns>
		protected static VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt; Fill(DataSet dataSet, VList<OrphanQuestionnaireProcurementFunctionalManager> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<OrphanQuestionnaireProcurementFunctionalManager>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<OrphanQuestionnaireProcurementFunctionalManager>"/></returns>
		protected static VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt; Fill(DataTable dataTable, VList<OrphanQuestionnaireProcurementFunctionalManager> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					OrphanQuestionnaireProcurementFunctionalManager c = new OrphanQuestionnaireProcurementFunctionalManager();
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;OrphanQuestionnaireProcurementFunctionalManager&gt;"/></returns>
		protected VList<OrphanQuestionnaireProcurementFunctionalManager> Fill(IDataReader reader, VList<OrphanQuestionnaireProcurementFunctionalManager> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					OrphanQuestionnaireProcurementFunctionalManager entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<OrphanQuestionnaireProcurementFunctionalManager>("OrphanQuestionnaireProcurementFunctionalManager",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new OrphanQuestionnaireProcurementFunctionalManager();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AnswerText = (System.String)reader[((int)OrphanQuestionnaireProcurementFunctionalManagerColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, OrphanQuestionnaireProcurementFunctionalManager entity)
		{
			reader.Read();
			entity.AnswerText = (System.String)reader[((int)OrphanQuestionnaireProcurementFunctionalManagerColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, OrphanQuestionnaireProcurementFunctionalManager entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder : SqlFilterBuilder<OrphanQuestionnaireProcurementFunctionalManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerFilterBuilder

	#region OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder : ParameterizedSqlFilterBuilder<OrphanQuestionnaireProcurementFunctionalManagerColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerParameterBuilder
	
	#region OrphanQuestionnaireProcurementFunctionalManagerSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrphanQuestionnaireProcurementFunctionalManagerSortBuilder : SqlSortBuilder<OrphanQuestionnaireProcurementFunctionalManagerColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerSqlSortBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrphanQuestionnaireProcurementFunctionalManagerSortBuilder

} // end namespace
