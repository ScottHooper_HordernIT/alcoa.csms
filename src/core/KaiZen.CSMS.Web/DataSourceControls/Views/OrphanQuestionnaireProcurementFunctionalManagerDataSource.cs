﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.OrphanQuestionnaireProcurementFunctionalManagerProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(OrphanQuestionnaireProcurementFunctionalManagerDataSourceDesigner))]
	public class OrphanQuestionnaireProcurementFunctionalManagerDataSource : ReadOnlyDataSource<OrphanQuestionnaireProcurementFunctionalManager>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerDataSource class.
		/// </summary>
		public OrphanQuestionnaireProcurementFunctionalManagerDataSource() : base(new OrphanQuestionnaireProcurementFunctionalManagerService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the OrphanQuestionnaireProcurementFunctionalManagerDataSourceView used by the OrphanQuestionnaireProcurementFunctionalManagerDataSource.
		/// </summary>
		protected OrphanQuestionnaireProcurementFunctionalManagerDataSourceView OrphanQuestionnaireProcurementFunctionalManagerView
		{
			get { return ( View as OrphanQuestionnaireProcurementFunctionalManagerDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the OrphanQuestionnaireProcurementFunctionalManagerDataSourceView class that is to be
		/// used by the OrphanQuestionnaireProcurementFunctionalManagerDataSource.
		/// </summary>
		/// <returns>An instance of the OrphanQuestionnaireProcurementFunctionalManagerDataSourceView class.</returns>
		protected override BaseDataSourceView<OrphanQuestionnaireProcurementFunctionalManager, Object> GetNewDataSourceView()
		{
			return new OrphanQuestionnaireProcurementFunctionalManagerDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the OrphanQuestionnaireProcurementFunctionalManagerDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class OrphanQuestionnaireProcurementFunctionalManagerDataSourceView : ReadOnlyDataSourceView<OrphanQuestionnaireProcurementFunctionalManager>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementFunctionalManagerDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the OrphanQuestionnaireProcurementFunctionalManagerDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public OrphanQuestionnaireProcurementFunctionalManagerDataSourceView(OrphanQuestionnaireProcurementFunctionalManagerDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal OrphanQuestionnaireProcurementFunctionalManagerDataSource OrphanQuestionnaireProcurementFunctionalManagerOwner
		{
			get { return Owner as OrphanQuestionnaireProcurementFunctionalManagerDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal OrphanQuestionnaireProcurementFunctionalManagerService OrphanQuestionnaireProcurementFunctionalManagerProvider
		{
			get { return Provider as OrphanQuestionnaireProcurementFunctionalManagerService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region OrphanQuestionnaireProcurementFunctionalManagerDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the OrphanQuestionnaireProcurementFunctionalManagerDataSource class.
	/// </summary>
	public class OrphanQuestionnaireProcurementFunctionalManagerDataSourceDesigner : ReadOnlyDataSourceDesigner<OrphanQuestionnaireProcurementFunctionalManager>
	{
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerDataSourceDesigner

	#region OrphanQuestionnaireProcurementFunctionalManagerFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerFilter : SqlFilter<OrphanQuestionnaireProcurementFunctionalManagerColumn>
	{
	}

	#endregion OrphanQuestionnaireProcurementFunctionalManagerFilter

	#region OrphanQuestionnaireProcurementFunctionalManagerExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementFunctionalManager"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementFunctionalManagerExpressionBuilder : SqlExpressionBuilder<OrphanQuestionnaireProcurementFunctionalManagerColumn>
	{
	}
	
	#endregion OrphanQuestionnaireProcurementFunctionalManagerExpressionBuilder		
}

