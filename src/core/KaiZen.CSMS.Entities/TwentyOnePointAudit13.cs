﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_13' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit13 : TwentyOnePointAudit13Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAudit13"/> instance.
		///</summary>
		public TwentyOnePointAudit13():base(){}	
		
		#endregion
	}
}
