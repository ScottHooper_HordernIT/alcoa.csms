﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CompaniesEhsConsultantsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(CompaniesEhsConsultantsDataSourceDesigner))]
	public class CompaniesEhsConsultantsDataSource : ReadOnlyDataSource<CompaniesEhsConsultants>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsDataSource class.
		/// </summary>
		public CompaniesEhsConsultantsDataSource() : base(new CompaniesEhsConsultantsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CompaniesEhsConsultantsDataSourceView used by the CompaniesEhsConsultantsDataSource.
		/// </summary>
		protected CompaniesEhsConsultantsDataSourceView CompaniesEhsConsultantsView
		{
			get { return ( View as CompaniesEhsConsultantsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CompaniesEhsConsultantsDataSourceView class that is to be
		/// used by the CompaniesEhsConsultantsDataSource.
		/// </summary>
		/// <returns>An instance of the CompaniesEhsConsultantsDataSourceView class.</returns>
		protected override BaseDataSourceView<CompaniesEhsConsultants, Object> GetNewDataSourceView()
		{
			return new CompaniesEhsConsultantsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CompaniesEhsConsultantsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CompaniesEhsConsultantsDataSourceView : ReadOnlyDataSourceView<CompaniesEhsConsultants>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesEhsConsultantsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CompaniesEhsConsultantsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CompaniesEhsConsultantsDataSourceView(CompaniesEhsConsultantsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CompaniesEhsConsultantsDataSource CompaniesEhsConsultantsOwner
		{
			get { return Owner as CompaniesEhsConsultantsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CompaniesEhsConsultantsService CompaniesEhsConsultantsProvider
		{
			get { return Provider as CompaniesEhsConsultantsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region CompaniesEhsConsultantsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CompaniesEhsConsultantsDataSource class.
	/// </summary>
	public class CompaniesEhsConsultantsDataSourceDesigner : ReadOnlyDataSourceDesigner<CompaniesEhsConsultants>
	{
	}

	#endregion CompaniesEhsConsultantsDataSourceDesigner

	#region CompaniesEhsConsultantsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsFilter : SqlFilter<CompaniesEhsConsultantsColumn>
	{
	}

	#endregion CompaniesEhsConsultantsFilter

	#region CompaniesEhsConsultantsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesEhsConsultants"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesEhsConsultantsExpressionBuilder : SqlExpressionBuilder<CompaniesEhsConsultantsColumn>
	{
	}
	
	#endregion CompaniesEhsConsultantsExpressionBuilder		
}

