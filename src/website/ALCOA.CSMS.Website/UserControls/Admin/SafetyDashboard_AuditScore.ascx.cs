﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using System.Collections.Generic;



 public partial class SafetyDashboard_AuditScore : System.Web.UI.UserControl
    {
       
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e)
        {
            Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack);
        }

        private void moduleInit(bool postBack)
        {

            if (!postBack)
            {
                int _i = 0;

                int Current_year = DateTime.Now.Year;
                while (Current_year >= 2012)
                {
                    ddlYear.Items.Add(Current_year.ToString(), Convert.ToInt32(Current_year));
                    Current_year--;
                } //End
                ddlYear.Value = DateTime.Today.Year;
               // ddlMonth.Value = DateTime.Today.Month;

                grid.SettingsBehavior.AllowDragDrop = false;
            }

        }

        protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
           // e.NewValues["Key"] = e.OldValues["Key"];
            int SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            int Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            //DataSet dsAuditScore = DataRepository.KpiProvider.CheckEntry(SiteId, Year);
            int auditScoreId = Convert.ToInt32(e.Keys[0]);
            DataSet dsgetSet = DataRepository.KpiProvider.getSet(Convert.ToInt32(e.Keys[0]));

            try
            {
                if (Convert.ToInt32(e.NewValues["SiteId"]) == Convert.ToInt32(e.OldValues["SiteId"])
                        && Year == Convert.ToInt32(dsgetSet.Tables[0].Rows[0]["Year"]))
                {
                    int val = DataRepository.KpiProvider.UpdateGrid(SiteId, Year
                        , Convert.ToInt32(e.Keys[0]), Convert.ToString(e.NewValues["Description"]));
                    e.Cancel = true;
                    grid.CancelEdit();
                }
                
                else if(e.NewValues["Description"] != e.OldValues["Description"])
                {
                    
                    //int Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
                    DataSet dsAuditScore = DataRepository.KpiProvider.CheckEntry(SiteId, Year);
                    if (dsAuditScore.Tables[0].Rows.Count > 0)
                    {
                        throw new Exception("This Is A Duplicate Entry!");

                    }
                    else
                    {
                        int val = DataRepository.KpiProvider.UpdateGrid(SiteId, Year
                        , Convert.ToInt32(e.Keys[0]), Convert.ToString(e.NewValues["Description"]));
                        e.Cancel = true;
                        grid.CancelEdit();
                    }
                }


            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }

        }

        protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {
            int SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
            int Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
            DataSet dsAuditScore = DataRepository.KpiProvider.CheckEntry(SiteId, Year);

            try
            {
                if (dsAuditScore.Tables[0].Rows.Count > 0)
                {
                    throw new Exception("This Is A Duplicate Entry!");

                }
                else
                {
                    int val = DataRepository.KpiProvider.InsertGrid(SiteId, Year,
                      Convert.ToString(e.NewValues["Description"]));
                    e.Cancel = true;
                    grid.CancelEdit();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }

        //protected void grid_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
        //{
        //    int SiteId = Convert.ToInt32(e.NewValues["SiteId"]);
        //    int Year = Convert.ToInt32(ddlYear.SelectedItem.Value);
        //    DataSet dsAuditScore = DataRepository.KpiProvider.CheckEntry(SiteId, Year);

        //    try
        //    {
        //        if (dsAuditScore.Tables[0].Rows.Count > 0)
        //        {
        //            throw new Exception("This Is A Duplicate Entry!");

        //        }
        //        else
        //        {
        ////                <asp:Parameter Name="SiteId" Type="Int32" />
        
        ////<asp:ControlParameter ControlID="ddlYear" Name="Year" Type="Int32" />
        ////    <asp:Parameter Name="Description" Type="String" />
        //            int val = DataRepository.KpiProvider.InsertGrid(SiteId, Year,
        //              Convert.ToString(e.NewValues["Description"]));
        //            e.Cancel = true;
        //            grid.CancelEdit();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);

        //    }
        //}

    

    }
