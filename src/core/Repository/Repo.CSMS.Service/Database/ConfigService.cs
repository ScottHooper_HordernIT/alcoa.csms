﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IConfigService
    {
        Config Get(Expression<Func<Config, bool>> where, List<Expression<Func<Config, object>>> includes);
        List<Config> GetMany(int? top, Expression<Func<Config, bool>> where, Func<IQueryable<Config>, IOrderedQueryable<Config>> orderBy, List<Expression<Func<Config, object>>> includes);
        List<Config> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Config, bool>> where, Func<IQueryable<Config>, IOrderedQueryable<Config>> orderBy, List<Expression<Func<Config, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Config, bool>> where, List<Expression<Func<Config, object>>> includes);
        void Insert(Config item, bool saveChanges = true);
        void Update(Config item, bool saveChanges = true);
        void Delete(Config item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ConfigService : IConfigService
    {
        private readonly IRepository<Config> _repository;

        public ConfigService(IRepository<Config> repository)
        {
            this._repository = repository;
        }

        public Config Get(Expression<Func<Config, bool>> where, List<Expression<Func<Config, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Config> GetMany(int? top, Expression<Func<Config, bool>> where, Func<IQueryable<Config>, IOrderedQueryable<Config>> orderBy, List<Expression<Func<Config, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Config> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Config, bool>> where, Func<IQueryable<Config>, IOrderedQueryable<Config>> orderBy, List<Expression<Func<Config, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Config, bool>> where, List<Expression<Func<Config, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Config item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Config");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Config item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Config");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Config item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Config");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
