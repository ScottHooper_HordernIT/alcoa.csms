﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>EmailLogAutoRepeater</c>
    /// </summary>
	public class EmailLogAutoRepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:EmailLogAutoRepeaterDesigner"/> class.
        /// </summary>
		public EmailLogAutoRepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is EmailLogAutoRepeater))
			{ 
				throw new ArgumentException("Component is not a EmailLogAutoRepeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			EmailLogAutoRepeater z = (EmailLogAutoRepeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();
		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="EmailLogAutoRepeater"/> Type.
    /// </summary>
	[Designer(typeof(EmailLogAutoRepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:EmailLogAutoRepeater runat=\"server\"></{0}:EmailLogAutoRepeater>")]
	public class EmailLogAutoRepeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:EmailLogAutoRepeater"/> class.
        /// </summary>
		public EmailLogAutoRepeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(EmailLogAutoItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(EmailLogAutoItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
		/// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(EmailLogAutoItem))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }

		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(EmailLogAutoItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(EmailLogAutoItem))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}
		
		
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//		{
//			if (ChildControlsCreated)
//			{
//				return;
//			}
//			Controls.Clear();
//
//			if (m_headerTemplate != null)
//			{
//				Control headerItem = new Control();
//				m_headerTemplate.InstantiateIn(headerItem);
//				Controls.Add(headerItem);
//			}
//
//			
//			if (m_footerTemplate != null)
//			{
//				Control footerItem = new Control();
//				m_footerTemplate.InstantiateIn(footerItem);
//				Controls.Add(footerItem);
//			}
//			ChildControlsCreated = true;
//		}
		
		/// <summary>
      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      /// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      {
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.EmailLogAuto entity = o as KaiZen.CSMS.Entities.EmailLogAuto;
						EmailLogAutoItem container = new EmailLogAutoItem(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}

							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}            
			//Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }				
		 }
			
			return pos;
		}
		
      /// <summary>
      /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
      /// </summary>
      /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class EmailLogAutoItem : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.EmailLogAuto _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EmailLogAutoItem"/> class.
        /// </summary>
		public EmailLogAutoItem()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EmailLogAutoItem"/> class.
        /// </summary>
		public EmailLogAutoItem(KaiZen.CSMS.Entities.EmailLogAuto entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the EmailLogId
        /// </summary>
        /// <value>The EmailLogId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 EmailLogId
		{
			get { return _entity.EmailLogId; }
		}
        /// <summary>
        /// Gets the EmailLogTypeId
        /// </summary>
        /// <value>The EmailLogTypeId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 EmailLogTypeId
		{
			get { return _entity.EmailLogTypeId; }
		}
        /// <summary>
        /// Gets the EmailLogTypeDesc
        /// </summary>
        /// <value>The EmailLogTypeDesc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailLogTypeDesc
		{
			get { return _entity.EmailLogTypeDesc; }
		}
        /// <summary>
        /// Gets the EmailDateTime
        /// </summary>
        /// <value>The EmailDateTime.</value>
		[System.ComponentModel.Bindable(true)]
		public System.DateTime EmailDateTime
		{
			get { return _entity.EmailDateTime; }
		}
        /// <summary>
        /// Gets the EmailFrom
        /// </summary>
        /// <value>The EmailFrom.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailFrom
		{
			get { return _entity.EmailFrom; }
		}
        /// <summary>
        /// Gets the EmailTo
        /// </summary>
        /// <value>The EmailTo.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailTo
		{
			get { return _entity.EmailTo; }
		}
        /// <summary>
        /// Gets the EmailCc
        /// </summary>
        /// <value>The EmailCc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailCc
		{
			get { return _entity.EmailCc; }
		}
        /// <summary>
        /// Gets the EmailBcc
        /// </summary>
        /// <value>The EmailBcc.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailBcc
		{
			get { return _entity.EmailBcc; }
		}
        /// <summary>
        /// Gets the EmailLogMessageSubject
        /// </summary>
        /// <value>The EmailLogMessageSubject.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String EmailLogMessageSubject
		{
			get { return _entity.EmailLogMessageSubject; }
		}
        /// <summary>
        /// Gets the EmailLogMessageBody
        /// </summary>
        /// <value>The EmailLogMessageBody.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Byte[] EmailLogMessageBody
		{
			get { return _entity.EmailLogMessageBody; }
		}
        /// <summary>
        /// Gets the SentByUserId
        /// </summary>
        /// <value>The SentByUserId.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? SentByUserId
		{
			get { return _entity.SentByUserId; }
		}
        /// <summary>
        /// Gets the SentByUserFullName
        /// </summary>
        /// <value>The SentByUserFullName.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String SentByUserFullName
		{
			get { return _entity.SentByUserFullName; }
		}

	}
}
