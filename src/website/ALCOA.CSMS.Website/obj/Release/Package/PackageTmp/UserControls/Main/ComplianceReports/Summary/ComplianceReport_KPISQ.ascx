﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_KPISQ" Codebehind="ComplianceReport_KPISQ.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<table border="0" cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td style="height: 28px; width: 1170px;" colspan="3">
            <dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
                CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
                 Width="900px" OnHtmlRowCreated="grid_RowCreated"
                KeyFieldName="CompanyId" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
                <Columns>
                                    <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CompanySiteCategoryId" SortIndex="0" SortOrder="Descending" UnboundType="String"
            VisibleIndex="0" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
                        SortIndex="2" SortOrder="Ascending" VisibleIndex="0" Visible="false">
                        <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                            ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        </PropertiesComboBox>
                        <EditFormSettings Visible="True" />
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company Name" FieldName="CompanyName2" SortIndex="0" SortOrder="Ascending" VisibleIndex="1"
                        Visible="True">
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Last Time On Site (EBI)" 
                        FieldName="LastEbiSwipe" VisibleIndex="3">
                        <PropertiesDateEdit DisplayFormatString="yyyy/MM/dd">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataComboBoxColumn FieldName="SiteId"
                        Visible="False">
                        <PropertiesComboBox ValueType="System.String">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Site Name" FieldName="SiteName2"
                        VisibleIndex="2" SortIndex="1" SortOrder="Ascending">
                        <Settings SortMode="DisplayText" />
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="Last KPI Entered" 
                        FieldName="LastKpiEntered" VisibleIndex="3">
                        <PropertiesDateEdit DisplayFormatString="yyyy/MM">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn FieldName="Type" VisibleIndex="4" />
                    <dxwgv:GridViewDataTextColumn Caption="Valid For Site" FieldName="ValidForSite" VisibleIndex="5">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Approved For Site" FieldName="ApprovedForSite" Width="55px" VisibleIndex="6">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataTextColumn Caption="SQ Validity" FieldName="SqValidity" VisibleIndex="7">
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                    <dxwgv:GridViewDataDateColumn Caption="SQ Valid To" 
                        FieldName="SqValidTo" VisibleIndex="9">
                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy">
                        </PropertiesDateEdit>
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataDateColumn>
                    <dxwgv:GridViewDataTextColumn Caption="Company SQ Status" FieldName="CompanySqStatus"
                        VisibleIndex="9">
                        <HeaderStyle Wrap="True" />
                    </dxwgv:GridViewDataTextColumn>
                </Columns>
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <Settings ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                <SettingsPager PageSize="20" AlwaysShowPager="True">
                    <AllButton Visible="True">
                    </AllButton>
                    <NextPageButton>
                    </NextPageButton>
                    <PrevPageButton>
                    </PrevPageButton>
                </SettingsPager>
                <SettingsCustomizationWindow Enabled="True" />
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                </Styles>
                <SettingsBehavior ConfirmDelete="True" ColumnResizeMode="NextColumn" />
                <SettingsText CommandClearFilter="Clear" />
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dxwgv:ASPxGridView>

            <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dxwgv:ASPxGridViewExporter>
        </td>
</table>
<%--<asp:SqlDataSource ID="sqldsComplianceKPI" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Kpi_GetReport_Compliance_SQ" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter DefaultValue="1" Name="RegionId" SessionField="spVar_RegionId"
            Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>--%>
<asp:SqlDataSource ID="sqldsKPIYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(YEAR(kpiDateTime)) As [Year] FROM dbo.KPI ORDER BY [Year] ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True" Sort="SiteName ASC"></data:SitesDataSource>     
<data:RegionsDataSource ID="dsRegionsFilter" runat="server" Filter="IsVisible = True"
    Sort="Ordinal ASC">
</data:RegionsDataSource>

<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>