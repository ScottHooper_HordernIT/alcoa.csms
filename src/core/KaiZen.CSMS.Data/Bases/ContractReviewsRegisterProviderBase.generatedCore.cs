﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{
    ///<summary>
    /// This class is the base class for any <see cref="QuestionnaireMainAttachmentProviderBase"/> implementation.
    /// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
    ///</summary>
    public abstract partial class ContractReviewsRegisterProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.ContractReviewsRegister, KaiZen.CSMS.Entities.ContractReviewsRegisterKey>
    {
        #region Get from Many To Many Relationship Functions
        #endregion

        #region Delete Methods

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
        /// <param name="key">The unique identifier of the row to delete.</param>
        /// <returns>Returns true if operation suceeded.</returns>
        public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContractReviewsRegisterKey key)
        {
            return Delete(transactionManager, key.ContractId);
        }

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="_attachmentId">. Primary Key.</param>
        /// <remarks>Deletes based on primary key(s).</remarks>
        /// <returns>Returns true if operation suceeded.</returns>
        public bool Delete(System.Int32 _contractId)
        {
            return Delete(null, _contractId);
        }

        /// <summary>
        /// 	Deletes a row from the DataSource.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId">. Primary Key.</param>
        /// <remarks>Deletes based on primary key(s).</remarks>
        /// <returns>Returns true if operation suceeded.</returns>
        public abstract bool Delete(TransactionManager transactionManager, System.Int32 _contractId);

        #endregion Delete Methods

        #region Get By Foreign Key Functions

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="_modifiedByUserId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<ContractReviewsRegister> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
        {
            int count = -1;
            return GetByModifiedByUserId(_modifiedByUserId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        /// <remarks></remarks>
        public TList<ContractReviewsRegister> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
        {
            int count = -1;
            return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        ///  <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<ContractReviewsRegister> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
        {
            int count = -1;
            return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_modifiedByUserId"></param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<ContractReviewsRegister> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
        {
            int count = -1;
            return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		fkQuestionnaireMainAttachmentUsers Description: 
        /// </summary>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public TList<ContractReviewsRegister> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength, out int count)
        {
            return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the FK_QuestionnaireMainAttachment_Users key.
        ///		FK_QuestionnaireMainAttachment_Users Description: 
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_modifiedByUserId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns a typed collection of KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment objects.</returns>
        public abstract TList<ContractReviewsRegister> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);


        #endregion

        #region Get By Index Functions

        /// <summary>
        /// 	Gets a row from the DataSource based on its primary key.
        /// </summary>
        /// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
        /// <param name="key">The unique identifier of the row to retrieve.</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <returns>Returns an instance of the Entity class.</returns>
        public override KaiZen.CSMS.Entities.ContractReviewsRegister Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContractReviewsRegisterKey key, int start, int pageLength)
        {
            return GetByContractId(transactionManager, key.ContractId, start, pageLength);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the primary key PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(System.Int32 _contractId)
        {
            int count = -1;
            return GetByContractId(null, _contractId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(System.Int32 _contractId, int start, int pageLength)
        {
            int count = -1;
            return GetByContractId(null, _contractId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(TransactionManager transactionManager, System.Int32 _contractId)
        {
            int count = -1;
            return GetByContractId(transactionManager, _contractId, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(TransactionManager transactionManager, System.Int32 _contractId, int start, int pageLength)
        {
            int count = -1;
            return GetByContractId(transactionManager, _contractId, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(System.Int32 _contractId, int start, int pageLength, out int count)
        {
            return GetByContractId(null, _contractId, start, pageLength, out count);
        }


        /// <summary>
        /// 	Gets rows from the datasource based on the PK_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_attachmentId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public abstract KaiZen.CSMS.Entities.ContractReviewsRegister GetByContractId(TransactionManager transactionManager, System.Int32 _contractId, int start, int pageLength, out int count);

        /// <summary>
        /// 	Gets rows from the datasource based on the primary key IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year)
        {
            int count = -1;
            return GetByCompanyIdSiteIdYear(null, _companyId, _siteId, _year, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyIdSiteIdYear(null, _companyId, _siteId, _year, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year)
        {
            int count = -1;
            return GetByCompanyIdSiteIdYear(transactionManager, _companyId, _siteId, _year, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year, int start, int pageLength)
        {
            int count = -1;
            return GetByCompanyIdSiteIdYear(transactionManager, _companyId, _siteId, _year, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year, int start, int pageLength, out int count)
        {
            return GetByCompanyIdSiteIdYear(null, _companyId, _siteId, _year, start, pageLength, out count);
        }


        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <param name="_answerId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> class.</returns>
        public abstract KaiZen.CSMS.Entities.ContractReviewsRegister GetByCompanyIdSiteIdYear(TransactionManager transactionManager, System.Int32 _companyId, System.Int32 _siteId, System.Int32 _year, int start, int pageLength, out int count);

        /// <summary>
        /// 	Gets rows from the datasource based on the primary key IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public TList<ContractReviewsRegister> GetByYear(System.Int32 _year)
        {
            int count = -1;
            return GetByYear(null, _year, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public TList<ContractReviewsRegister> GetByYear(System.Int32 _year, int start, int pageLength)
        {
            int count = -1;
            return GetByYear(null, _year, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public TList<ContractReviewsRegister> GetByYear(TransactionManager transactionManager, System.Int32 _year)
        {
            int count = -1;
            return GetByYear(transactionManager, _year, 0, int.MaxValue, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public TList<ContractReviewsRegister> GetByYear(TransactionManager transactionManager, System.Int32 _year, int start, int pageLength)
        {
            int count = -1;
            return GetByYear(transactionManager, _year, start, pageLength, out count);
        }

        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="_questionnaireId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">out parameter to get total records for query</param>
        /// <remarks></remarks>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public TList<ContractReviewsRegister> GetByYear(System.Int32 _year, int start, int pageLength, out int count)
        {
            return GetByYear(null, _year, start, pageLength, out count);
        }


        /// <summary>
        /// 	Gets rows from the datasource based on the IX_QuestionnaireMainAttachment index.
        /// </summary>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="_questionnaireId"></param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">Number of rows to return.</param>
        /// <param name="count">The total number of records.</param>
        /// <returns>Returns an instance of the <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/> class.</returns>
        public abstract TList<ContractReviewsRegister> GetByYear(TransactionManager transactionManager, System.Int32 _year, int start, int pageLength, out int count);



       #endregion "Get By Index Functions"

        #region Custom Methods


        #endregion

        #region Helper Functions

        /// <summary>
        /// Fill a TList&lt;SafetyPlansSEResponsesAttachment&gt; From a DataReader.
        /// </summary>
        /// <param name="reader">Datareader</param>
        /// <param name="rows">The collection to fill</param>
        /// <param name="start">Row number at which to start reading, the first row is 0.</param>
        /// <param name="pageLength">number of rows.</param>
        /// <returns>a <see cref="TList&lt;QuestionnaireMainAttachment&gt;"/></returns>
        public static TList<ContractReviewsRegister> Fill(IDataReader reader, TList<ContractReviewsRegister> rows, int start, int pageLength)
        {
            NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
            Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;

            // advance to the starting row
            for (int i = 0; i < start; i++)
            {
                if (!reader.Read())
                    return rows; // not enough rows, just return
            }
            for (int i = 0; i < pageLength; i++)
            {
                if (!reader.Read())
                    break; // we are done

                string key = null;

                KaiZen.CSMS.Entities.ContractReviewsRegister c = null;
                if (useEntityFactory)
                {
                    key = new System.Text.StringBuilder("ContractReviewsRegister")
                    .Append("|").Append((System.Int32)reader[((int)ContractReviewsRegisterColumn.ContractId - 1)]).ToString();
                    c = EntityManager.LocateOrCreate<ContractReviewsRegister>(
                    key.ToString(), // EntityTrackingKey
                    "ContractReviewsRegister",  //Creational Type
                    entityCreationFactoryType,  //Factory used to create entity
                    enableEntityTracking); // Track this entity?
                }
                else
                {
                    c = new KaiZen.CSMS.Entities.ContractReviewsRegister();
                }

                if (!enableEntityTracking ||
                    c.EntityState == EntityState.Added ||
                    (enableEntityTracking &&

                        (
                            (currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
                            (currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
                        )
                    ))
                {
                    c.SuppressEntityEvents = true;
                    c.ContractId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.ContractId - 1)];
                    c.CompanyId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.CompanyId - 1)];
                    c.SiteId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.SiteId - 1)];
                    c.Year = (System.Int32)reader[((int)ContractReviewsRegisterColumn.Year - 1)];
                    if ((byte)reader[((int)ContractReviewsRegisterColumn.January - 1)] == 1)
                    {
                        c.January = true;
                    }
                    else 
                    {
                        c.January = false;
                    }


                    if ((byte)reader[((int)ContractReviewsRegisterColumn.February - 1)] == 1)
                    {
                        c.February = true;
                    }
                    else
                    {
                        c.February = false;
                    }


                    if ((byte)reader[((int)ContractReviewsRegisterColumn.March - 1)] == 1)
                    {
                        c.March = true;
                    }
                    else
                    {
                        c.March = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.April - 1)] == 1)
                    {
                        c.April = true;
                    }
                    else
                    {
                        c.April = false;
                    }


                    if ((byte)reader[((int)ContractReviewsRegisterColumn.May - 1)] == 1)
                    {
                        c.May = true;
                    }
                    else
                    {
                        c.May = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.June - 1)] == 1)
                    {
                        c.June = true;
                    }
                    else
                    {
                        c.June = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.July - 1)] == 1)
                    {
                        c.July = true;
                    }
                    else
                    {
                        c.July = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.August - 1)] == 1)
                    {
                        c.August = true;
                    }
                    else
                    {
                        c.August = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.September - 1)] == 1)
                    {
                        c.September = true;
                    }
                    else
                    {
                        c.September = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.October - 1)] == 1)
                    {
                        c.October = true;
                    }
                    else
                    {
                        c.October = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.November - 1)] == 1)
                    {
                        c.November = true;
                    }
                    else
                    {
                        c.November = false;
                    }

                    if ((byte)reader[((int)ContractReviewsRegisterColumn.December - 1)] == 1)
                    {
                        c.December = true;
                    }
                    else
                    {
                        c.December = false;
                    }

                    c.YearCount = (System.Int32)reader[((int)ContractReviewsRegisterColumn.YearCount - 1)];
                   // c.LocationSponsorUserId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1)];
                   // c.LocationSpaUserId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1)];

                    c.LocationSponsorUserId = (reader.IsDBNull(((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1))) ? null : (System.Int32?)reader[((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1)];
                    c.LocationSpaUserId = (reader.IsDBNull(((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1))) ? null : (System.Int32?)reader[((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1)];

                    c.ModifiedDate = (System.DateTime)reader[((int)ContractReviewsRegisterColumn.ModifiedDate - 1)];
                    c.ModifiedBy = (System.Int32)reader[((int)ContractReviewsRegisterColumn.ModifiedBy - 1)];
                    
                    c.EntityTrackingKey = key;
                    c.AcceptChanges();
                    c.SuppressEntityEvents = false;
                }
                rows.Add(c);
            }
            return rows;
        }
        /// <summary>
        /// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object from the <see cref="IDataReader"/>.
        /// </summary>
        /// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object to refresh.</param>
        public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.ContractReviewsRegister entity)
        {
            if (!reader.Read()) return;

           // entity.AttachmentId = (System.Int32)reader[((int)SafetyPlansSEResponsesAttachmentColumn.AttachmentId - 1)];
            entity.ContractId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.ContractId - 1)];
            entity.CompanyId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.CompanyId - 1)];
           entity.SiteId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.SiteId - 1)];
           entity.Year = (System.Int32)reader[((int)ContractReviewsRegisterColumn.Year - 1)];

           if ((byte)reader[((int)ContractReviewsRegisterColumn.January - 1)] == 1)
           {
               entity.January = true;
           }
           else
           {
               entity.January = false;
           }


           if ((byte)reader[((int)ContractReviewsRegisterColumn.February - 1)] == 1)
           {
               entity.February = true;
           }
           else
           {
               entity.February = false;
           }


           if ((byte)reader[((int)ContractReviewsRegisterColumn.March - 1)] == 1)
           {
               entity.March = true;
           }
           else
           {
               entity.March = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.April - 1)] == 1)
           {
               entity.April = true;
           }
           else
           {
               entity.April = false;
           }


           if ((byte)reader[((int)ContractReviewsRegisterColumn.May - 1)] == 1)
           {
               entity.May = true;
           }
           else
           {
               entity.May = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.June - 1)] == 1)
           {
               entity.June = true;
           }
           else
           {
               entity.June = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.July - 1)] == 1)
           {
               entity.July = true;
           }
           else
           {
               entity.July = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.August - 1)] == 1)
           {
               entity.August = true;
           }
           else
           {
               entity.August = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.September - 1)] == 1)
           {
               entity.September = true;
           }
           else
           {
               entity.September = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.October - 1)] == 1)
           {
               entity.October = true;
           }
           else
           {
               entity.October = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.November - 1)] == 1)
           {
               entity.November = true;
           }
           else
           {
               entity.November = false;
           }

           if ((byte)reader[((int)ContractReviewsRegisterColumn.December - 1)] == 1)
           {
               entity.December = true;
           }
           else
           {
               entity.December = false;
           }
           entity.YearCount = (System.Int32)reader[((int)ContractReviewsRegisterColumn.YearCount - 1)];
           //entity.LocationSponsorUserId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1)];
          // entity.LocationSpaUserId = (System.Int32)reader[((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1)];


           entity.LocationSponsorUserId = (reader.IsDBNull(((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1))) ? null : (System.Int32?)reader[((int)ContractReviewsRegisterColumn.LocationSponsorUserId - 1)];
           entity.LocationSpaUserId = (reader.IsDBNull(((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1))) ? null : (System.Int32?)reader[((int)ContractReviewsRegisterColumn.LocationSpaUserId - 1)];


           entity.ModifiedDate = (System.DateTime)reader[((int)ContractReviewsRegisterColumn.ModifiedDate - 1)];
           entity.ModifiedBy = (System.Int32)reader[((int)ContractReviewsRegisterColumn.ModifiedBy - 1)];
          
            entity.AcceptChanges();
        }

        /// <summary>
        /// Refreshes the <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object from the <see cref="DataSet"/>.
        /// </summary>
        /// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object.</param>
        public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.ContractReviewsRegister entity)
        {
            DataRow dataRow = dataSet.Tables[0].Rows[0];

            entity.ContractId = (System.Int32)dataRow["ContractId"];
            entity.CompanyId = (System.Int32)dataRow["CompanyId"]; 
            entity.SiteId = (System.Int32)dataRow["SiteId"];           
            entity.Year = (System.Int32)dataRow["Year"];

            if ((byte)dataRow["January"] == 1)
            {
                entity.January = true;
            }
            else
            {
                entity.January = false;
            }


            if ((byte)dataRow["February"] == 1)
            {
                entity.February = true;
            }
            else
            {
                entity.February = false;
            }


            if ((byte)dataRow["March"] == 1)
            {
                entity.March = true;
            }
            else
            {
                entity.March = false;
            }

            if ((byte)dataRow["April"] == 1)
            {
                entity.April = true;
            }
            else
            {
                entity.April = false;
            }


            if ((byte)dataRow["May"] == 1)
            {
                entity.May = true;
            }
            else
            {
                entity.May = false;
            }

            if ((byte)dataRow["June"] == 1)
            {
                entity.June = true;
            }
            else
            {
                entity.June = false;
            }

            if ((byte)dataRow["July"] == 1)
            {
                entity.July = true;
            }
            else
            {
                entity.July = false;
            }

            if ((byte)dataRow["August"] == 1)
            {
                entity.August = true;
            }
            else
            {
                entity.August = false;
            }

            if ((byte)dataRow["September"] == 1)
            {
                entity.September = true;
            }
            else
            {
                entity.September = false;
            }

            if ((byte)dataRow["October"] == 1)
            {
                entity.October = true;
            }
            else
            {
                entity.October = false;
            }

            if ((byte)dataRow["November"] == 1)
            {
                entity.November = true;
            }
            else
            {
                entity.November = false;
            }

            if ((byte)dataRow["December"] == 1)
            {
                entity.December = true;
            }
            else
            {
                entity.December = false;
            }

            entity.YearCount = (System.Int32)dataRow["YearCount"];
           // entity.LocationSponsorUserId = (System.Int32)dataRow["LocationSponsorUserId"];
            //entity.LocationSpaUserId = (System.Int32)dataRow["LocationSpaUserId"];

            entity.LocationSponsorUserId = Convert.IsDBNull(dataRow["LocationSponsorUserId"]) ? null : (System.Int32?)dataRow["LocationSponsorUserId"];
            entity.LocationSpaUserId = Convert.IsDBNull(dataRow["LocationSpaUserId"]) ? null : (System.Int32?)dataRow["LocationSpaUserId"];


            entity.ModifiedBy = (System.Int32)dataRow["ModifiedBy"];
            entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
            entity.AcceptChanges();
        }
        #endregion

        #region DeepLoad Methods
        /// <summary>
        /// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
        /// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
        /// </summary>
        /// <remarks>
        /// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
        /// </remarks>
        /// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
        /// <param name="entity">The <see cref="KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment"/> object to load.</param>
        /// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
        /// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
        /// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment Property Collection Type Array To Include or Exclude from Load</param>
        /// <param name="innerList">A collection of child types for easy access.</param>
        /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
        /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
        public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContractReviewsRegister entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
        {
            if (entity == null)
                return;

            #region ModifiedByUserIdSource
            if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList)
                && entity.ModifiedByUserIdSource == null)
            {
                object[] pkItems = new object[1];
                pkItems[0] = entity.ModifiedBy;
                Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
                if (tmpEntity != null)
                    entity.ModifiedByUserIdSource = tmpEntity;
                else
                    entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedBy);

#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
#endif

                if (deep && entity.ModifiedByUserIdSource != null)
                {
                    innerList.SkipChildren = true;
                    DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
                    innerList.SkipChildren = false;
                }

            }
            #endregion ModifiedByUserIdSource

            #region AnswerIdSource
            if (CanDeepLoad(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepLoadType, innerList)
                && entity.AnswerIdSource == null)
            {
                object[] pkItems = new object[1];
                pkItems[0] = entity.ContractId;
                QuestionnaireMainAnswer tmpEntity = EntityManager.LocateEntity<QuestionnaireMainAnswer>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireMainAnswer), pkItems), DataRepository.Provider.EnableEntityTracking);
                if (tmpEntity != null)
                    entity.AnswerIdSource = tmpEntity;
                else
                    entity.AnswerIdSource = DataRepository.QuestionnaireMainAnswerProvider.GetByAnswerId(transactionManager, entity.ContractId);

#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AnswerIdSource' loaded. key " + entity.EntityTrackingKey);
#endif

                if (deep && entity.AnswerIdSource != null)
                {
                    innerList.SkipChildren = true;
                    DataRepository.QuestionnaireMainAnswerProvider.DeepLoad(transactionManager, entity.AnswerIdSource, deep, deepLoadType, childTypes, innerList);
                    innerList.SkipChildren = false;
                }

            }
            #endregion AnswerIdSource

            //used to hold DeepLoad method delegates and fire after all the local children have been loaded.
            Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

            //Fire all DeepLoad Items
            foreach (KeyValuePair<Delegate, object> pair in deepHandles.Values)
            {
                pair.Key.DynamicInvoke((object[])pair.Value);
            }
            deepHandles = null;
        }

        #endregion

        #region DeepSave Methods

        /// <summary>
        /// Deep Save the entire object graph of the KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment object with criteria based of the child 
        /// Type property array and DeepSaveType.
        /// </summary>
        /// <param name="transactionManager">The transaction manager.</param>
        /// <param name="entity">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment instance</param>
        /// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
        /// <param name="childTypes">KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment Property Collection Type Array To Include or Exclude from Save</param>
        /// <param name="innerList">A Hashtable of child types for easy access.</param>
        public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.ContractReviewsRegister entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
        {
            if (entity == null)
                return false;

            #region Composite Parent Properties
            //Save Source Composite Properties, however, don't call deep save on them.  
            //So they only get saved a single level deep.

            #region ModifiedByUserIdSource
            if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList)
                && entity.ModifiedByUserIdSource != null)
            {
                DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
                entity.ModifiedBy = entity.ModifiedByUserIdSource.UserId;
            }
            #endregion

            #region AnswerIdSource
            if (CanDeepSave(entity, "QuestionnaireMainAnswer|AnswerIdSource", deepSaveType, innerList)
                && entity.AnswerIdSource != null)
            {
                DataRepository.QuestionnaireMainAnswerProvider.Save(transactionManager, entity.AnswerIdSource);
                entity.ContractId = entity.ContractId;
            }
            #endregion
            #endregion Composite Parent Properties

            // Save Root Entity through Provider
            if (!entity.IsDeleted)
                this.Save(transactionManager, entity);

            //used to hold DeepSave method delegates and fire after all the local children have been saved.
            Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
            //Fire all DeepSave Items
            foreach (KeyValuePair<Delegate, object> pair in deepHandles.Values)
            {
                pair.Key.DynamicInvoke((object[])pair.Value);
            }

            // Save Root Entity through Provider, if not already saved in delete mode
            if (entity.IsDeleted)
                this.Save(transactionManager, entity);


            deepHandles = null;

            return true;
        }
        #endregion
    } // end class

    #region ContractReviewsRegisterChildEntityTypes

    ///<summary>
    /// Enumeration used to expose the different child entity types 
    /// for child properties in <c>KaiZen.CSMS.Entities.SafetyPlansSEResponsesAttachment</c>
    ///</summary>
    public enum ContractReviewsRegisterChildEntityTypes
    {

        ///<summary>
        /// Composite Property for <c>Users</c> at ModifiedByUserIdSource
        ///</summary>
        [ChildEntityType(typeof(Users))]
        Users,

        ///<summary>
        /// Composite Property for <c>QuestionnaireMainAnswer</c> at AnswerIdSource
        ///</summary>
        [ChildEntityType(typeof(ContractReviewsRegister))]
        ContractReviewsRegister,
    }

    #endregion QuestionnaireMainAttachmentChildEntityTypes

    #region ContractReviewsRegisterFilterBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContractReviewsRegisterFilterBuilder : SqlFilterBuilder<ContractReviewsRegisterColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        public ContractReviewsRegisterFilterBuilder() : base() { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public ContractReviewsRegisterFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentFilterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public ContractReviewsRegisterFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion QuestionnaireMainAttachmentFilterBuilder

    #region ContractReviewsRegisterParameterBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContractReviewsRegisterParameterBuilder : ParameterizedSqlFilterBuilder<ContractReviewsRegisterColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        public ContractReviewsRegisterParameterBuilder() : base() { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        public ContractReviewsRegisterParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentParameterBuilder class.
        /// </summary>
        /// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
        /// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
        public ContractReviewsRegisterParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

        #endregion Constructors
    }

    #endregion QuestionnaireMainAttachmentParameterBuilder

    #region ContractReviewsRegisterSortBuilder

    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireMainAttachmentColumn&gt;"/> class
    /// that is used exclusively with a <see cref="QuestionnaireMainAttachment"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContractReviewsRegisterSortBuilder : SqlSortBuilder<ContractReviewsRegisterColumn>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentSqlSortBuilder class.
        /// </summary>
        public ContractReviewsRegisterSortBuilder() : base() { }

        #endregion Constructors

    }
    #endregion QuestionnaireMainAttachmentSortBuilder

} // end namespace
