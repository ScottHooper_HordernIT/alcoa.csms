﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_EhsimsCompare" CodeBehind="EhsimsCompare.ascx.cs" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<table cellpadding="0" cellspacing="0" class="style1">
    <tr>
    <table cellpadding="0" cellspacing="0" class="style1">
    <tr align="right">
        <td align="left"><strong>From:</strong>
            <dx:aspxcombobox id="ddlMonth" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                csspostfix="Office2003Blue" incrementalfilteringmode="StartsWith" selectedindex="0"
                spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                valuetype="System.Int32" width="90px">
                    <Items>
                       <%-- <dx:listedititem text="All" value="0">
                        </dx:listedititem>--%>
                        <dx:listedititem text="January" value="1">
                        </dx:listedititem>
                        <dx:listedititem text="February" value="2">
                        </dx:listedititem>
                        <dx:listedititem text="March" value="3">
                        </dx:listedititem>
                        <dx:listedititem text="April" value="4">
                        </dx:listedititem>
                        <dx:listedititem text="May" value="5">
                        </dx:listedititem>
                        <dx:listedititem text="June" value="6">
                        </dx:listedititem>
                        <dx:listedititem text="July" value="7">
                        </dx:listedititem>
                        <dx:listedititem text="August" value="8">
                        </dx:listedititem>
                        <dx:listedititem text="September" value="9">
                        </dx:listedititem>
                        <dx:listedititem text="October" value="10">
                        </dx:listedititem>
                        <dx:listedititem text="November" value="11">
                        </dx:listedititem>
                        <dx:listedititem text="December" value="12">
                        </dx:listedititem>
                    </Items>
                    <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </loadingpanelimage>
                    <buttonstyle width="13px">
                    </buttonstyle>
                </dx:aspxcombobox>
            <dx:aspxcombobox id="ddlYear" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                csspostfix="Office2003Blue" incrementalfilteringmode="StartsWith"
                spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                valuetype="System.Int32" width="60px">
                    <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </loadingpanelimage>
                    <buttonstyle width="13px">
                    </buttonstyle>
                </dx:aspxcombobox>
        </td>
        <td align="left"><strong>To:</strong>
            <dx:aspxcombobox id="ddlMonthTo" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                csspostfix="Office2003Blue" incrementalfilteringmode="StartsWith" selectedindex="0"
                spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                valuetype="System.Int32" width="90px">
                    <Items>
                        <%--<dx:listedititem text="All" value="0">
                        </dx:listedititem>--%>
                        <dx:listedititem text="January" value="1">
                        </dx:listedititem>
                        <dx:listedititem text="February" value="2">
                        </dx:listedititem>
                        <dx:listedititem text="March" value="3">
                        </dx:listedititem>
                        <dx:listedititem text="April" value="4">
                        </dx:listedititem>
                        <dx:listedititem text="May" value="5">
                        </dx:listedititem>
                        <dx:listedititem text="June" value="6">
                        </dx:listedititem>
                        <dx:listedititem text="July" value="7">
                        </dx:listedititem>
                        <dx:listedititem text="August" value="8">
                        </dx:listedititem>
                        <dx:listedititem text="September" value="9">
                        </dx:listedititem>
                        <dx:listedititem text="October" value="10">
                        </dx:listedititem>
                        <dx:listedititem text="November" value="11">
                        </dx:listedititem>
                        <dx:listedititem text="December" value="12">
                        </dx:listedititem>
                    </Items>
                    <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </loadingpanelimage>
                    <buttonstyle width="13px">
                    </buttonstyle>
                </dx:aspxcombobox>
            <dx:aspxcombobox id="ddlYearTo" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                csspostfix="Office2003Blue" incrementalfilteringmode="StartsWith"
                spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                valuetype="System.Int32" width="60px">
                    <loadingpanelimage url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                    </loadingpanelimage>
                    <buttonstyle width="13px">
                    </buttonstyle>
                </dx:aspxcombobox>
        </td>
    </tr>
        </table>
        </tr>
    <tr>
        <td>

            <dx:aspxbutton id="btnFilter" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                csspostfix="Office2003Blue"
                spritecssfilepath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                text="Filter" onclick="btnFilter_Click">
        </dx:aspxbutton>
        <asp:Label ID="lblErrorMessage" runat="server" Text="From date must be before To date" ForeColor="Red" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr align="right">
        <td align="right">
            
            <div align="right"><a style="COLOR: #0000ff; TEXT-ALIGN: right; TEXT-DECORATION: none" href="javascript:ShowHideCustomizationWindow()">Customize</a> </div>
        </td>
    </tr>
    <tr align="left">
        <td>
            <dx:aspxgridview id="grid" runat="server"
                cssfilepath="~/App_Themes/Office2003Blue/{0}/styles.css"
                onhtmlrowcreated="grid_RowCreated" clientinstancename="grid"
                csspostfix="Office2003Blue" width="900px" autogeneratecolumns="False">
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Month" FieldName="Year-Month" 
                        VisibleIndex="0" Width="76px" FixedStyle="Left" SortIndex="0" 
                        SortOrder="Descending">
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataComboBoxColumn FieldName="Company" VisibleIndex="1" Caption="Company" SortIndex="2" Width="200px" FixedStyle="Left" SortOrder="Ascending">
                                                <PropertiesComboBox DataSourceID="CompaniesDataSource1" TextField="CompanyName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                            </dx:GridViewDataComboBoxColumn>
                   <%-- <dx:GridViewDataTextColumn Caption="Region" FieldName="RegionName" VisibleIndex="2" 
                        FixedStyle="Left" SortIndex="1" SortOrder="Ascending">
                    </dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataComboBoxColumn FieldName="RegionName" VisibleIndex="2" Caption="Region" SortIndex="1"  FixedStyle="Left" SortOrder="Ascending">
                                                <PropertiesComboBox DataSourceID="RegionsDS" TextField="RegionNameAbbrev"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                            </dx:GridViewDataComboBoxColumn>
                    <%--<dx:GridViewDataTextColumn Caption="Site" FieldName="Site" VisibleIndex="2" 
                        FixedStyle="Left" SortIndex="1" SortOrder="Ascending">
                    </dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataComboBoxColumn FieldName="Site" VisibleIndex="2" Caption="Site" SortIndex="1"  FixedStyle="Left" SortOrder="Ascending">
                                                <PropertiesComboBox DataSourceID="SitesDataSource2" TextField="SiteFullName"
                                                     IncrementalFilteringMode="StartsWith">
                                                </PropertiesComboBox>
                                                <Settings SortMode="DisplayText"></Settings>
                            </dx:GridViewDataComboBoxColumn>
                     
                    <dx:GridViewDataComboBoxColumn FieldName="Status" VisibleIndex="3"
                                                 UnboundType="Integer" Caption="Status"  Width="55px" FixedStyle="Left">
                                                <PropertiesComboBox  ValueType="System.Int32">
                                                   <items>
                                                    <dx:ListEditItem Text="Yes" Value="1" />
                                                    <dx:ListEditItem Text="No" Value="0" />
                                                   
                                                    </items>
                                                </PropertiesComboBox>
                                                <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td align="center" width="52px"><asp:Image ID="changeImage" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     </tr></table>
                     </DataItemTemplate>
                                            </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn FieldName="Csms-Hours" VisibleIndex="5">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-Hours" ShowInCustomizationForm="True" VisibleIndex="5">
                    <CellStyle HorizontalAlign="Center"></CellStyle>
                    </dx:GridViewDataTextColumn>
                   
                    <dx:GridViewDataComboBoxColumn FieldName="Status-Hours" VisibleIndex="6"
                                                 UnboundType="Integer" >
                                                 <CellStyle HorizontalAlign="Center"></CellStyle>
                                                <PropertiesComboBox  ValueType="System.Int32">
                                                   <items>
                                                    <dx:ListEditItem Text="True" Value="1" />
                                                    <dx:ListEditItem Text="False" Value="0" />
                                                   
                                                    </items>
                                                </PropertiesComboBox>
                                                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td align="center" width="52px"><asp:Image ID="changeImageHours" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     </tr></table>
                     </DataItemTemplate>
                                            </dx:GridViewDataComboBoxColumn>
                   
                    <dx:GridViewDataComboBoxColumn FieldName="Status-Counts" VisibleIndex="7"
                                                 UnboundType="Integer" >
                                                 <CellStyle HorizontalAlign="Center"></CellStyle>
                                                <PropertiesComboBox  ValueType="System.Int32">
                                                   <items>
                                                    <dx:ListEditItem Text="True" Value="1" />
                                                    <dx:ListEditItem Text="False" Value="0" />
                                                   
                                                    </items>
                                                </PropertiesComboBox>
                                                 <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td align="center" width="52px"><asp:Image ID="changeImageCounts" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true" GenerateEmptyAlternateText="True" /></td>
                     </tr></table>
                     </DataItemTemplate>
                                            </dx:GridViewDataComboBoxColumn>
<dx:GridViewDataTextColumn FieldName="Csms-IFE" ShowInCustomizationForm="True" VisibleIndex="8">
<CellStyle HorizontalAlign="Center"></CellStyle>
</dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-IFE" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Csms-FA" VisibleIndex="10">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-FA" VisibleIndex="11">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Csms-RW" VisibleIndex="12">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-RW" VisibleIndex="13">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Csms-MT" VisibleIndex="14">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-MT" VisibleIndex="15">
                    <CellStyle HorizontalAlign="Center" />
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="Csms-LWD" VisibleIndex="16">
                     <CellStyle HorizontalAlign="Center" />
                         <CellStyle HorizontalAlign="Center">
                         </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="EhsimsIhs-LWD" VisibleIndex="17">
                    <CellStyle HorizontalAlign="Center" />
                        <CellStyle HorizontalAlign="Center">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <%--<dx:GridViewDataTextColumn FieldName="StatusDesc" VisibleIndex="18" 
                        Width="300px">
                    </dx:GridViewDataTextColumn>--%>
                    <dx:GridViewDataComboBoxColumn FieldName="StatusDesc" VisibleIndex="18" Width="300px">
                                                 
                                                <PropertiesComboBox  ValueType="System.String" IncrementalFilteringMode="StartsWith">
                                                   <items>
                                                    <dx:ListEditItem Text="Cannot find in CSMS raw data." Value="Cannot find in CSMS raw data." />
                                                    <dx:ListEditItem Text="Cannot find in EHSIMS raw data." Value="Cannot find in EHSIMS raw data." />
                                                    <dx:ListEditItem Text="Cannot find in IHS raw data." Value="Cannot find in IHS raw data." />  
                                                    <dx:ListEditItem Text="Does not exist within IHS Companies Map." Value="Does not exist within IHS Companies Map." /> <%--DT 3385--%>                                                  
                                                    <dx:ListEditItem Text="Data does not match." Value="Data does not match." />
                                                    <dx:ListEditItem Text="Match." Value="Match." />
                                                    </items>
                                                </PropertiesComboBox>
                                            </dx:GridViewDataComboBoxColumn>
                </Columns>
                <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True" ></SettingsBehavior>
                <SettingsPager>
                    <AllButton Visible="True">
                    </AllButton>
                </SettingsPager>
                <Settings  ShowHorizontalScrollBar="True" 
                    ShowVerticalScrollBar="True" VerticalScrollableHeight="310" 
                    ShowGroupPanel="True" ShowFilterBar="Visible" ShowFilterRow="true" />
                <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                    <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                    </LoadingPanelOnStatusBar>
                    <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                    </LoadingPanel>
                </Images>
                <ImagesFilterControl>
                    <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                    </LoadingPanel>
                </ImagesFilterControl>
                <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                    CssPostfix="Office2003Blue">
                    <LoadingPanel ImageSpacing="10px">
                    </LoadingPanel>
                    <Header ImageSpacing="5px" SortingImageSpacing="5px">
                    </Header>
                    <AlternatingRow Enabled="True">
                    </AlternatingRow>
                </Styles>
                <StylesEditors>
                    <ProgressBar Height="25px">
                    </ProgressBar>
                </StylesEditors>
            </dx:aspxgridview>
        </td>
    </tr>
    <tr align="right">
        <td>
            <uc1:exportbuttons id="ucExportButtons" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <br />
            Errors:<br />
            <asp:textbox id="TextBox1" runat="server" height="74px" textmode="MultiLine"
                width="890px"></asp:textbox>
        </td>
    </tr>
</table>
<data:companiesdatasource id="CompaniesDataSource1" runat="server" sort="CompanyName ASC"></data:companiesdatasource>
<data:regionsdatasource id="RegionsDataSource1" runat="server" sort="RegionNameAbbrev ASC"></data:regionsdatasource>
<data:sitesdatasource id="SitesDataSource1" runat="server"></data:sitesdatasource>
<asp:sqldatasource
    id="SitesDataSource2"
    runat="server"
    connectionstring="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    selectcommand="SELECT (SiteName +' - '+LocCode+' - '+SiteNameIhs) SiteFullName 
FROM sites WHERE (SiteName +' - '+LocCode+' - '+SiteNameIhs) IS NOT NULL ORDER BY SiteFullName">
</asp:sqldatasource>
<asp:sqldatasource
    id="RegionsDS"
    runat="server"
    connectionstring="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    selectcommand="select RegionNameAbbrev from Regions Union select top 1 '?' RegionNameAbbrev from Regions">
</asp:sqldatasource>

