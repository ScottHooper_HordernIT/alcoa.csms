﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireReportOverviewSubContractorsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireReportOverviewSubContractorsDataSourceDesigner))]
	public class QuestionnaireReportOverviewSubContractorsDataSource : ReadOnlyDataSource<QuestionnaireReportOverviewSubContractors>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsDataSource class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsDataSource() : base(new QuestionnaireReportOverviewSubContractorsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireReportOverviewSubContractorsDataSourceView used by the QuestionnaireReportOverviewSubContractorsDataSource.
		/// </summary>
		protected QuestionnaireReportOverviewSubContractorsDataSourceView QuestionnaireReportOverviewSubContractorsView
		{
			get { return ( View as QuestionnaireReportOverviewSubContractorsDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireReportOverviewSubContractorsDataSourceView class that is to be
		/// used by the QuestionnaireReportOverviewSubContractorsDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireReportOverviewSubContractorsDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireReportOverviewSubContractors, Object> GetNewDataSourceView()
		{
			return new QuestionnaireReportOverviewSubContractorsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireReportOverviewSubContractorsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireReportOverviewSubContractorsDataSourceView : ReadOnlyDataSourceView<QuestionnaireReportOverviewSubContractors>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireReportOverviewSubContractorsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireReportOverviewSubContractorsDataSourceView(QuestionnaireReportOverviewSubContractorsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireReportOverviewSubContractorsDataSource QuestionnaireReportOverviewSubContractorsOwner
		{
			get { return Owner as QuestionnaireReportOverviewSubContractorsDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireReportOverviewSubContractorsService QuestionnaireReportOverviewSubContractorsProvider
		{
			get { return Provider as QuestionnaireReportOverviewSubContractorsService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireReportOverviewSubContractorsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireReportOverviewSubContractorsDataSource class.
	/// </summary>
	public class QuestionnaireReportOverviewSubContractorsDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireReportOverviewSubContractors>
	{
	}

	#endregion QuestionnaireReportOverviewSubContractorsDataSourceDesigner

	#region QuestionnaireReportOverviewSubContractorsFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsFilter : SqlFilter<QuestionnaireReportOverviewSubContractorsColumn>
	{
	}

	#endregion QuestionnaireReportOverviewSubContractorsFilter

	#region QuestionnaireReportOverviewSubContractorsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsExpressionBuilder : SqlExpressionBuilder<QuestionnaireReportOverviewSubContractorsColumn>
	{
	}
	
	#endregion QuestionnaireReportOverviewSubContractorsExpressionBuilder		
}

