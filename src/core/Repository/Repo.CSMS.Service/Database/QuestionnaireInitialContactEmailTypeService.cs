﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialContactEmailTypeService
    {
        QuestionnaireInitialContactEmailType Get(Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes);
        List<QuestionnaireInitialContactEmailType> GetMany(int? top, Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmailType>, IOrderedQueryable<QuestionnaireInitialContactEmailType>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes);
        List<QuestionnaireInitialContactEmailType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmailType>, IOrderedQueryable<QuestionnaireInitialContactEmailType>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes);
        void Insert(QuestionnaireInitialContactEmailType item, bool saveChanges = true);
        void Update(QuestionnaireInitialContactEmailType item, bool saveChanges = true);
        void Delete(QuestionnaireInitialContactEmailType item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialContactEmailTypeService : IQuestionnaireInitialContactEmailTypeService
    {
        private readonly IRepository<QuestionnaireInitialContactEmailType> _repository;

        public QuestionnaireInitialContactEmailTypeService(IRepository<QuestionnaireInitialContactEmailType> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialContactEmailType Get(Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialContactEmailType> GetMany(int? top, Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmailType>, IOrderedQueryable<QuestionnaireInitialContactEmailType>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialContactEmailType> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, Func<IQueryable<QuestionnaireInitialContactEmailType>, IOrderedQueryable<QuestionnaireInitialContactEmailType>> orderBy, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialContactEmailType, bool>> where, List<Expression<Func<QuestionnaireInitialContactEmailType, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialContactEmailType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmailType");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialContactEmailType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmailType");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialContactEmailType item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialContactEmailType");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
