﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrphanQuestionnaireProcurementContactProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class OrphanQuestionnaireProcurementContactProviderBaseCore : EntityViewProviderBase<OrphanQuestionnaireProcurementContact>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;OrphanQuestionnaireProcurementContact&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;OrphanQuestionnaireProcurementContact&gt;"/></returns>
		protected static VList&lt;OrphanQuestionnaireProcurementContact&gt; Fill(DataSet dataSet, VList<OrphanQuestionnaireProcurementContact> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<OrphanQuestionnaireProcurementContact>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;OrphanQuestionnaireProcurementContact&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<OrphanQuestionnaireProcurementContact>"/></returns>
		protected static VList&lt;OrphanQuestionnaireProcurementContact&gt; Fill(DataTable dataTable, VList<OrphanQuestionnaireProcurementContact> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					OrphanQuestionnaireProcurementContact c = new OrphanQuestionnaireProcurementContact();
					c.AnswerText = (Convert.IsDBNull(row["AnswerText"]))?string.Empty:(System.String)row["AnswerText"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;OrphanQuestionnaireProcurementContact&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;OrphanQuestionnaireProcurementContact&gt;"/></returns>
		protected VList<OrphanQuestionnaireProcurementContact> Fill(IDataReader reader, VList<OrphanQuestionnaireProcurementContact> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					OrphanQuestionnaireProcurementContact entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<OrphanQuestionnaireProcurementContact>("OrphanQuestionnaireProcurementContact",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new OrphanQuestionnaireProcurementContact();
					}
					
					entity.SuppressEntityEvents = true;

					entity.AnswerText = (System.String)reader[((int)OrphanQuestionnaireProcurementContactColumn.AnswerText)];
					//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="OrphanQuestionnaireProcurementContact"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanQuestionnaireProcurementContact"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, OrphanQuestionnaireProcurementContact entity)
		{
			reader.Read();
			entity.AnswerText = (System.String)reader[((int)OrphanQuestionnaireProcurementContactColumn.AnswerText)];
			//entity.AnswerText = (Convert.IsDBNull(reader["AnswerText"]))?string.Empty:(System.String)reader["AnswerText"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="OrphanQuestionnaireProcurementContact"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="OrphanQuestionnaireProcurementContact"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, OrphanQuestionnaireProcurementContact entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AnswerText = (Convert.IsDBNull(dataRow["AnswerText"]))?string.Empty:(System.String)dataRow["AnswerText"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region OrphanQuestionnaireProcurementContactFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactFilterBuilder : SqlFilterBuilder<OrphanQuestionnaireProcurementContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementContactFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementContactFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementContactFilterBuilder

	#region OrphanQuestionnaireProcurementContactParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrphanQuestionnaireProcurementContactParameterBuilder : ParameterizedSqlFilterBuilder<OrphanQuestionnaireProcurementContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrphanQuestionnaireProcurementContactParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrphanQuestionnaireProcurementContactParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrphanQuestionnaireProcurementContactParameterBuilder
	
	#region OrphanQuestionnaireProcurementContactSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrphanQuestionnaireProcurementContact"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrphanQuestionnaireProcurementContactSortBuilder : SqlSortBuilder<OrphanQuestionnaireProcurementContactColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrphanQuestionnaireProcurementContactSqlSortBuilder class.
		/// </summary>
		public OrphanQuestionnaireProcurementContactSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrphanQuestionnaireProcurementContactSortBuilder

} // end namespace
