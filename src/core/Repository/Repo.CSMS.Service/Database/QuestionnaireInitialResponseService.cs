﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireInitialResponseService
    {
        QuestionnaireInitialResponse Get(Expression<Func<QuestionnaireInitialResponse, bool>> where, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes);
        List<QuestionnaireInitialResponse> GetMany(int? top, Expression<Func<QuestionnaireInitialResponse, bool>> where, Func<IQueryable<QuestionnaireInitialResponse>, IOrderedQueryable<QuestionnaireInitialResponse>> orderBy, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes);
        List<QuestionnaireInitialResponse> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialResponse, bool>> where, Func<IQueryable<QuestionnaireInitialResponse>, IOrderedQueryable<QuestionnaireInitialResponse>> orderBy, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireInitialResponse, bool>> where, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes);
        void Insert(QuestionnaireInitialResponse item, bool saveChanges = true);
        void Update(QuestionnaireInitialResponse item, bool saveChanges = true);
        void Delete(QuestionnaireInitialResponse item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireInitialResponseService : IQuestionnaireInitialResponseService
    {
        private readonly IRepository<QuestionnaireInitialResponse> _repository;

        public QuestionnaireInitialResponseService(IRepository<QuestionnaireInitialResponse> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireInitialResponse Get(Expression<Func<QuestionnaireInitialResponse, bool>> where, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireInitialResponse> GetMany(int? top, Expression<Func<QuestionnaireInitialResponse, bool>> where, Func<IQueryable<QuestionnaireInitialResponse>, IOrderedQueryable<QuestionnaireInitialResponse>> orderBy, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireInitialResponse> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireInitialResponse, bool>> where, Func<IQueryable<QuestionnaireInitialResponse>, IOrderedQueryable<QuestionnaireInitialResponse>> orderBy, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireInitialResponse, bool>> where, List<Expression<Func<QuestionnaireInitialResponse, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireInitialResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponse");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireInitialResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponse");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireInitialResponse item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireInitialResponse");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
