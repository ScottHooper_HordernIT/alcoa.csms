USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[Procurement_CurrentlyWithHSAssessor]    Script Date: 31/07/2015 12:37:41 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Procurement_CurrentlyWithHSAssessor]                                      
(                                      
 @SiteId int,                                      
 @Month int,                                      
 @Year int                                      
)                                      
AS                            
DECLARE @Companies VARCHAR(MAX)          
                        
IF (@SiteId > 0)                                     
BEGIN                   
                  
    SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM            
    (SELECT DISTINCT F.CompanyName             
FROM dbo.Questionnaire AS A                     
  LEFT OUTER JOIN QuestionnaireReportExpiry B              
  ON A.QuestionnaireId = B.QuestionnaireId               
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D              
  ON D.CompanyId = A.CompanyId             
LEFT OUTER JOIN dbo.Companies F                          
  ON F.CompanyId = A.CompanyId               
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'H&S Assessor'                 
  AND D.SiteId = @SiteId                  
  --AND YEAR(B.MainAssessmentValidTo) = @Year                  
  --AND MONTH(B.MainAssessmentValidTo) = @Month          
--  AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0      
  AND B.MainAssessmentValidTo < GETDATE()   
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X                
END                              
ELSE                              
BEGIN                          
   SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM             
   (SELECT DISTINCT F.CompanyName             
FROM dbo.Questionnaire AS A                     
  LEFT OUTER JOIN QuestionnaireReportExpiry B              
  ON A.QuestionnaireId = B.QuestionnaireId               
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D               
  ON D.CompanyId = A.CompanyId              
LEFT OUTER JOIN dbo.Companies F                          
  ON F.CompanyId = A.CompanyId             
  WHERE B.QuestionnairePresentlyWIthUserDesc = 'H&S Assessor'                 
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)               
  --AND YEAR(B.MainAssessmentValidTo) = @Year                  
  --AND MONTH(B.MainAssessmentValidTo) = @Month        
--  AND DATEDIFF(DAY, B.MainAssessmentValidTo, CONVERT(DATETIME, GETDATE(), 103))>0      
  AND B.MainAssessmentValidTo < GETDATE()   
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X          
END                       
         
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year         
 AND [Month] = @Month AND [ItemName] = 'Expired - Currently With H&S Assessor')        
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]        
 SET [NonCompliantCompanies] = @Companies        
 WHERE [SiteId] = @SiteId        
   AND [Month] = @Month        
   AND [Year] = @Year        
   AND [ItemName] = 'Expired - Currently With H&S Assessor'        
ELSE        
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]        
           ([SiteId]        
           ,[Month]        
           ,[Year]        
           ,[ItemName]        
           ,[NonCompliantCompanies])        
     VALUES        
           (@SiteId        
           ,@Month        
           ,@Year        
           ,'Expired - Currently With H&S Assessor'        
           ,@Companies)    
------------------------------------------------------------------------------------------------ 


GO


