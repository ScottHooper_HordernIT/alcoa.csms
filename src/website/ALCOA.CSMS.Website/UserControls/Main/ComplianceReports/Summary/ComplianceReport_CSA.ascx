﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Tiny_ComplianceReports_ComplianceReport_CSA" Codebehind="ComplianceReport_CSA.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>


 <strong id="lblEmbeded" runat="server" visible="false">
            <br />
            <span style="font-size: 10pt">
            Contractor Services Audit - Embedded</span></strong><br />
            </span>

<dxwgv:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    Width="900px" OnHtmlRowCreated="grid_RowCreated" KeyFieldName="CompanyID" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize">
    <Columns>
 <%--   below field name change from CompanySiteCategoryId to CategoryDesc--%>
        <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CategoryDesc" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="0" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
            VisibleIndex="1">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site Name" FieldName="SiteId" Name="SiteBox"
            VisibleIndex="2">
            <PropertiesComboBox DataSourceID="sqldsResidentialSites" DropDownHeight="150px" TextField="SiteName"
                ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 1" FieldName="Qtr 1" ReadOnly="True" VisibleIndex="3">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 2" FieldName="Qtr 2" ReadOnly="True" VisibleIndex="4">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 3" FieldName="Qtr 3" ReadOnly="True" VisibleIndex="5">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted3" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 4" FieldName="Qtr 4" ReadOnly="True" VisibleIndex="6">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted4" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Alcoa Annual Audit" FieldName="Alcoa Audit" ReadOnly="True"
            VisibleIndex="7">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage5" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted5" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
    </Columns>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Settings ShowGroupedColumns="True" ShowFilterRow="True" ShowGroupPanel="True" />
    <SettingsPager Visible="False" PageSize="999" Mode="ShowAllRecords">
        <AllButton Text="All">
        </AllButton>
        <NextPageButton Text="Next &gt;">
        </NextPageButton>
        <PrevPageButton Text="&lt; Prev">
        </PrevPageButton>
    </SettingsPager>
    <SettingsCustomizationWindow Enabled="True" />
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <AlternatingRow Enabled="True">
        </AlternatingRow>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsBehavior ConfirmDelete="True" />
    <SettingsCookies CookiesID="NonComplianceCSA" Version="0.1" />
    <SettingsText EmptyDataRow="No Contractor Services Audit(s) submitted" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>



    <strong id="lblNonEmbeded1" runat="server" visible="false">
            <br />
            <span style="font-size: 10pt">
            <span style="color: #000099">
            Contractor Services Audit - Non Embedded 1 </span></strong><br />
            </span>

<dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dxwgv:ASPxGridViewExporter>

<dxwgv:ASPxGridView ID="grid1" ClientInstanceName="grid" runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue"
    Width="900px" OnHtmlRowCreated="grid1_RowCreated" KeyFieldName="CompanyID" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
    visible="false" >
    <Columns>
        <dxwgv:GridViewDataComboBoxColumn Name="gcbResidentialCategory" Caption="Residential Category"
            FieldName="CategoryDesc" SortIndex="0" SortOrder="Ascending" UnboundType="String"
            VisibleIndex="0" FixedStyle="Left" Width="145px">
            <PropertiesComboBox DataSourceID="CompanySiteCategoryDataSource" DropDownHeight="150px"
                TextField="CategoryDesc" ValueField="CompanySiteCategoryId" ValueType="System.Int32"
                IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
       <%-- <dxwgv:GridViewDataComboBoxColumn Caption="Company Name" FieldName="CompanyId" Name="CompanyBox"
            VisibleIndex="1">
            <PropertiesComboBox DataSourceID="sqldsCompaniesList" DropDownHeight="150px" TextField="CompanyName"
                ValueField="CompanyId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>--%>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site Name" FieldName="SiteId" Name="SiteBox"
            VisibleIndex="1">
            <PropertiesComboBox DataSourceID="sqldsResidentialSites" DropDownHeight="150px" TextField="SiteName"
                ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
            </PropertiesComboBox>
            <EditFormSettings Visible="True" />
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>

         <dxwgv:GridViewDataTextColumn Name="Qtr 1" FieldName="Qtr 1" ReadOnly="True" VisibleIndex="2">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage1" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 2" FieldName="Qtr 2" ReadOnly="True" VisibleIndex="3">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage2" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 3" FieldName="Qtr 3" ReadOnly="True" VisibleIndex="4">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage3" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted3" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Qtr 4" FieldName="Qtr 4" ReadOnly="True" VisibleIndex="5">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage4" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted4" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Half 1" FieldName="Half 1" ReadOnly="True" VisibleIndex="6">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage6" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmittedHalf1" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataTextColumn Name="Half 2" FieldName="Half 2" ReadOnly="True" VisibleIndex="7">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage7" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmittedHalf2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>
        
        <dxwgv:GridViewDataTextColumn Name="Alcoa Annual Audit" FieldName="Alcoa Audit" ReadOnly="True"
            VisibleIndex="8">
            <DataItemTemplate>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:Image ID="changeImage5" runat="server" ImageUrl="~/Images/spacer.gif" Visible="true"
                                GenerateEmptyAlternateText="True" />
                        </td>
                        <td style="width: 35px">
                            <asp:Label ID="noKPISubmitted5" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </DataItemTemplate>
            <HeaderStyle HorizontalAlign="Center" />
            <CellStyle HorizontalAlign="Center">
            </CellStyle>
        </dxwgv:GridViewDataTextColumn>



    </Columns>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <SettingsPager Visible="False" PageSize="999" Mode="ShowAllRecords">
        <AllButton Text="All">
        </AllButton>
        <NextPageButton Text="Next &gt;">
        </NextPageButton>
        <PrevPageButton Text="&lt; Prev">
        </PrevPageButton>
    </SettingsPager>
    <SettingsCustomizationWindow Enabled="True" />
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px">
        </Header>
        <AlternatingRow Enabled="True">
        </AlternatingRow>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <SettingsBehavior ConfirmDelete="True" />
    <SettingsCookies CookiesID="NonComplianceCSA" Version="0.1" />
    <SettingsText EmptyDataRow="No Contractor Services Audit(s) submitted" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
</dxwgv:ASPxGridView>

<dxwgv:ASPxGridViewExporter ID="grid1Export" runat="server" GridViewID="grid1"></dxwgv:ASPxGridViewExporter>




<asp:SqlDataSource ID="sqldsCompaniesList" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="_Companies_GetCompanyNameList" SelectCommandType="StoredProcedure">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsResidentialSites" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT(dbo.CompanySiteCategoryStandard.SiteId) As [SiteId], dbo.Sites.SiteName As [SiteName] FROM dbo.CompanySiteCategoryStandard INNER JOIN dbo.Sites on dbo.CompanySiteCategoryStandard.SiteId = dbo.Sites.SiteId WHERE dbo.CompanySiteCategoryStandard.CompanySiteCategoryId <= 2 ORDER BY [SiteName] ASC">
</asp:SqlDataSource>
<asp:SqlDataSource ID="sqldsCSAYear" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    ProviderName="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString.ProviderName %>"
    SelectCommand="SELECT DISTINCT([Year]) As [Year] FROM dbo.CSA ORDER BY [Year] ASC">
</asp:SqlDataSource>
<data:CompanySiteCategoryDataSource ID="CompanySiteCategoryDataSource" runat="server"
    SelectMethod="GetPaged" EnablePaging="True" EnableSorting="True">
</data:CompanySiteCategoryDataSource>