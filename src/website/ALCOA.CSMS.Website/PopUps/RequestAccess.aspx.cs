﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Web.UI;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

namespace ALCOA.CSMS.Website.PopUps
{
    public partial class RequestAccess : System.Web.UI.Page
    {
        Auth auth = new Auth();
        protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

        private void moduleInit(bool postBack)
        {
            if (!postBack)
            {
                //check MaxContractorAccessRequestsPerUser
                if (auth.RoleId == (int)RoleList.Contractor)
                {
                    Configuration config = new Configuration();
                    int MaxAllowed = 5; //default
                    Int32.TryParse(config.GetValue(ConfigList.MaxAllowedOpenContractorRequestsByUser), out MaxAllowed);

                    int count = 0;
                    AdminTaskFilters atFilters = new AdminTaskFilters();
                    atFilters.Append(AdminTaskColumn.OpenedByUserId, auth.UserId.ToString());
                    atFilters.Append(AdminTaskColumn.AdminTaskStatusId, ((int)AdminTaskStatusList.Open).ToString());

                    DataRepository.AdminTaskProvider.GetPaged(atFilters.ToString(), "", 0, 10, out count);
                    if (count >= MaxAllowed)
                    {
                        btnRequestAccess.Enabled = false;
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode(String.Format("Unfortunately You have {0} outstanding contractor requests. Please wait for the Admin to grant access to these {0} users before requesting more.", MaxAllowed)));
                    }
                    else
                    {
                        btnRequestAccess.Enabled = true;
                    }

                    cbCompany.Text = auth.CompanyName;
                }
                else
                {
                    if (auth.RoleId == (int)RoleList.Administrator || auth.RoleId == (int)RoleList.Reader)
                    {
                        lblError.Text = "This form is only to be used by Contractors. Alcoan Requests please use ARF.";
                        tbFirstName.Enabled = false;
                        tbLastName.Enabled = false;
                        tbJobTitle.Enabled = false;
                        cbTitle.Enabled = false;
                        cbCompany.Enabled = false;
                        tbTelephoneNo.Enabled = false;
                        tbFaxNo.Enabled = false;
                        tbEmail.Enabled = false;
                        tbMobileNumber.Enabled = false;
                        cbAccess.Enabled = false;
                        tbLogin.Enabled = false;
                        btnRequestAccess.Enabled = false;
                    }
                    else
                    {
                        Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                    }
                }
            }
        }

        protected void btnRequestAccess_Click(object sender, EventArgs e)
        {
            //create task
            //email user saying task received and will be actioned shortly.
            bool success = false;

            TransactionManager transactionManager = null;
            try
            {
                transactionManager = ConnectionScope.ValidateOrCreateTransaction(true);
                AdminTask adminTask = new AdminTask();
                adminTask.FirstName = tbFirstName.Text;
                adminTask.LastName = tbLastName.Text;
                adminTask.JobRole = cbTitle.SelectedItem.Text; //Changed by Ashley Goldstraw to make job title show correctly DT312
                adminTask.JobTitle = tbJobTitle.Text;//Changed by Ashley Goldstraw to make job title show correctly DT312  Switched job role and job title around.  The field names dont match the db column names anyway.
                adminTask.CompanyId = auth.CompanyId;
                adminTask.TelephoneNo = tbTelephoneNo.Text;
                adminTask.MobileNo = tbMobileNumber.Text;
                adminTask.FaxNo = tbFaxNo.Text;
                adminTask.EmailAddress = tbEmail.Text;
                adminTask.Login = tbLogin.Text;
                adminTask.AdminTaskTypeId = (int)AdminTaskTypeList.Contractor_Request_for_Access;
                adminTask.AdminTaskSourceId = (int)AdminTaskSourceList.Manual;
                adminTask.AdminTaskStatusId = (int)AdminTaskStatusList.Open;
                adminTask.DateOpened = DateTime.Now;
                adminTask.OpenedByUserId = auth.UserId;

                switch ((int)cbAccess.Value)
                {
                    case 1:
                        adminTask.CsmsAccessId = (int)CsmsAccessList.AlcoaDirect;
                        break;
                    case 0:
                        adminTask.CsmsAccessId = (int)CsmsAccessList.AlcoaLan;
                        break;
                    default:
                        throw new Exception("Access Type must be selected.");
                }

                adminTask.AdminTaskComments = String.Format("Request created by: {0} - {1} {2} ({3}) at {4}\n", auth.UserLogon, auth.FirstName, auth.LastName, auth.CompanyName, DateTime.Now);
                if (!string.IsNullOrEmpty(mTaskComments.Text)) adminTask.AdminTaskComments += "\n" + mTaskComments.Text;


                DataRepository.AdminTaskProvider.Insert(transactionManager, adminTask);
                transactionManager.Commit();
                success = true;
            }
            catch (Exception ex)
            {
                lblError.Text = "Error Occured: " + ex.Message;
                throw ex;
            }
            finally
            {
                if (success) Response.Redirect("RequestAccessSuccessful.aspx", true);
            }
        }
    }
}