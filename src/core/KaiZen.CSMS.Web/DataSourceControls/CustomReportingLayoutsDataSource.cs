﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CustomReportingLayoutsProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CustomReportingLayoutsDataSourceDesigner))]
	public class CustomReportingLayoutsDataSource : ProviderDataSource<CustomReportingLayouts, CustomReportingLayoutsKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsDataSource class.
		/// </summary>
		public CustomReportingLayoutsDataSource() : base(new CustomReportingLayoutsService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CustomReportingLayoutsDataSourceView used by the CustomReportingLayoutsDataSource.
		/// </summary>
		protected CustomReportingLayoutsDataSourceView CustomReportingLayoutsView
		{
			get { return ( View as CustomReportingLayoutsDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CustomReportingLayoutsDataSource control invokes to retrieve data.
		/// </summary>
		public CustomReportingLayoutsSelectMethod SelectMethod
		{
			get
			{
				CustomReportingLayoutsSelectMethod selectMethod = CustomReportingLayoutsSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CustomReportingLayoutsSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CustomReportingLayoutsDataSourceView class that is to be
		/// used by the CustomReportingLayoutsDataSource.
		/// </summary>
		/// <returns>An instance of the CustomReportingLayoutsDataSourceView class.</returns>
		protected override BaseDataSourceView<CustomReportingLayouts, CustomReportingLayoutsKey> GetNewDataSourceView()
		{
			return new CustomReportingLayoutsDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CustomReportingLayoutsDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CustomReportingLayoutsDataSourceView : ProviderDataSourceView<CustomReportingLayouts, CustomReportingLayoutsKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CustomReportingLayoutsDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CustomReportingLayoutsDataSourceView(CustomReportingLayoutsDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CustomReportingLayoutsDataSource CustomReportingLayoutsOwner
		{
			get { return Owner as CustomReportingLayoutsDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CustomReportingLayoutsSelectMethod SelectMethod
		{
			get { return CustomReportingLayoutsOwner.SelectMethod; }
			set { CustomReportingLayoutsOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CustomReportingLayoutsService CustomReportingLayoutsProvider
		{
			get { return Provider as CustomReportingLayoutsService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CustomReportingLayouts> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CustomReportingLayouts> results = null;
			CustomReportingLayouts item;
			count = 0;
			
			System.Int32 _reportId;
			System.String _area;
			System.Int32 _modifiedByUserId;
			System.String _reportName;

			switch ( SelectMethod )
			{
				case CustomReportingLayoutsSelectMethod.Get:
					CustomReportingLayoutsKey entityKey  = new CustomReportingLayoutsKey();
					entityKey.Load(values);
					item = CustomReportingLayoutsProvider.Get(entityKey);
					results = new TList<CustomReportingLayouts>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CustomReportingLayoutsSelectMethod.GetAll:
                    results = CustomReportingLayoutsProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CustomReportingLayoutsSelectMethod.GetPaged:
					results = CustomReportingLayoutsProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CustomReportingLayoutsSelectMethod.Find:
					if ( FilterParameters != null )
						results = CustomReportingLayoutsProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CustomReportingLayoutsProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CustomReportingLayoutsSelectMethod.GetByReportId:
					_reportId = ( values["ReportId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ReportId"], typeof(System.Int32)) : (int)0;
					item = CustomReportingLayoutsProvider.GetByReportId(_reportId);
					results = new TList<CustomReportingLayouts>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CustomReportingLayoutsSelectMethod.GetByAreaModifiedByUserIdReportName:
					_area = ( values["Area"] != null ) ? (System.String) EntityUtil.ChangeType(values["Area"], typeof(System.String)) : string.Empty;
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					_reportName = ( values["ReportName"] != null ) ? (System.String) EntityUtil.ChangeType(values["ReportName"], typeof(System.String)) : string.Empty;
					item = CustomReportingLayoutsProvider.GetByAreaModifiedByUserIdReportName(_area, _modifiedByUserId, _reportName);
					results = new TList<CustomReportingLayouts>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CustomReportingLayoutsSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = CustomReportingLayoutsProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CustomReportingLayoutsSelectMethod.Get || SelectMethod == CustomReportingLayoutsSelectMethod.GetByReportId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CustomReportingLayouts entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CustomReportingLayoutsProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CustomReportingLayouts> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CustomReportingLayoutsProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CustomReportingLayoutsDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CustomReportingLayoutsDataSource class.
	/// </summary>
	public class CustomReportingLayoutsDataSourceDesigner : ProviderDataSourceDesigner<CustomReportingLayouts, CustomReportingLayoutsKey>
	{
		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsDataSourceDesigner class.
		/// </summary>
		public CustomReportingLayoutsDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CustomReportingLayoutsSelectMethod SelectMethod
		{
			get { return ((CustomReportingLayoutsDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CustomReportingLayoutsDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CustomReportingLayoutsDataSourceActionList

	/// <summary>
	/// Supports the CustomReportingLayoutsDataSourceDesigner class.
	/// </summary>
	internal class CustomReportingLayoutsDataSourceActionList : DesignerActionList
	{
		private CustomReportingLayoutsDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CustomReportingLayoutsDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CustomReportingLayoutsDataSourceActionList(CustomReportingLayoutsDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CustomReportingLayoutsSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CustomReportingLayoutsDataSourceActionList
	
	#endregion CustomReportingLayoutsDataSourceDesigner
	
	#region CustomReportingLayoutsSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CustomReportingLayoutsDataSource.SelectMethod property.
	/// </summary>
	public enum CustomReportingLayoutsSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByReportId method.
		/// </summary>
		GetByReportId,
		/// <summary>
		/// Represents the GetByAreaModifiedByUserIdReportName method.
		/// </summary>
		GetByAreaModifiedByUserIdReportName,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId
	}
	
	#endregion CustomReportingLayoutsSelectMethod

	#region CustomReportingLayoutsFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsFilter : SqlFilter<CustomReportingLayoutsColumn>
	{
	}
	
	#endregion CustomReportingLayoutsFilter

	#region CustomReportingLayoutsExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsExpressionBuilder : SqlExpressionBuilder<CustomReportingLayoutsColumn>
	{
	}
	
	#endregion CustomReportingLayoutsExpressionBuilder	

	#region CustomReportingLayoutsProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CustomReportingLayoutsChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CustomReportingLayouts"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CustomReportingLayoutsProperty : ChildEntityProperty<CustomReportingLayoutsChildEntityTypes>
	{
	}
	
	#endregion CustomReportingLayoutsProperty
}

