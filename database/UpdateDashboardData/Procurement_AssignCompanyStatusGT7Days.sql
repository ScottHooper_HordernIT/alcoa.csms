USE [AuA_CSMS_Dev]
GO

/****** Object:  StoredProcedure [dbo].[Procurement_AssignCompanyStatusGT7Days]    Script Date: 31/07/2015 12:40:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Procurement_AssignCompanyStatusGT7Days]                                  
(                                  
 @SiteId int,                                  
 @Month int,                                  
 @Year int                                  
)                                  
AS            
        
DECLARE @Companies VARCHAR(MAX)                     
                      
IF (@SiteId > 0)                                 
BEGIN               
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM        
  (SELECT DISTINCT F.CompanyName         
  FROM dbo.Questionnaire AS A                 
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId         
  LEFT OUTER JOIN dbo.Companies F                      
  ON F.CompanyId = A.CompanyId          
  WHERE C.ActionName = 'ProcurementAssignCompanyStatus'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId = @SiteId            
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7                
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7  
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X    
END                          
ELSE                          
BEGIN                      
  SELECT @Companies = COALESCE(@Companies + ',', '') + X.CompanyName FROM     
  (SELECT DISTINCT F.CompanyName         
  FROM dbo.Questionnaire AS A               
  LEFT OUTER JOIN dbo.QuestionnairePresentlyWithAction AS C          
  ON A.QuestionnairePresentlyWithActionId = C.QuestionnairePresentlyWithActionId             
  LEFT OUTER JOIN dbo.CompanySiteCategoryStandard D           
  ON D.CompanyId = A.CompanyId           
  LEFT OUTER JOIN dbo.Companies F                      
  ON F.CompanyId = A.CompanyId        
  WHERE C.ActionName = 'ProcurementAssignCompanyStatus'          
  AND A.QuestionnaireId = (SELECT MAX(QuestionnaireId) FROM dbo.Questionnaire WHERE CompanyId = A.CompanyId           
  AND QuestionnairePresentlyWithSince IS NOT NULL)               
  AND D.SiteId IN (SELECT SiteId FROM RegionsSites WHERE (-1 * dbo.RegionsSites.RegionId) = @SiteId)           
  --AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, (@cDay+'/'+@cMonth+'/'+@cYear), 103))>7       
  AND DATEDIFF(DAY, A.QuestionnairePresentlyWithSince, CONVERT(DATETIME, GETDATE(), 103))>7  
  AND YEAR(GETDATE()) = @Year AND MONTH(GETDATE()) = @Month ) X    
END                   
     
IF EXISTS(SELECT * FROM dbo.Dashboard_Companies_NonCompliant WHERE SiteId = @SiteId AND [Year] = @Year     
 AND [Month] = @Month AND [ItemName] = 'Assign Company Status > 7 days')    
 UPDATE [dbo].[Dashboard_Companies_NonCompliant]    
 SET [NonCompliantCompanies] = @Companies    
 WHERE [SiteId] = @SiteId    
   AND [Month] = @Month    
   AND [Year] = @Year    
   AND [ItemName] = 'Assign Company Status > 7 days'    
ELSE    
 INSERT INTO [dbo].[Dashboard_Companies_NonCompliant]    
           ([SiteId]    
           ,[Month]    
           ,[Year]    
           ,[ItemName]    
           ,[NonCompliantCompanies])    
     VALUES    
           (@SiteId    
           ,@Month    
           ,@Year    
           ,'Assign Company Status > 7 days'    
           ,@Companies)
----------------------------------------

GO


