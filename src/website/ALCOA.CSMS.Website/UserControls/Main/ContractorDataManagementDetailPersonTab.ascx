﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="ALCOA.CSMS.Website.UserControls.Main.ContractorDataManagementDetailPersonTab" Codebehind="ContractorDataManagementDetailPersonTab.ascx.cs" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxClasses" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxRoundPanel" tagprefix="dx" %>

<style type="text/css">
    .td-label {
        text-align:right;
    }
        .td-label label {
            padding-right:10px;
        }

    h3 {
        color:#000;
        margin-bottom:0;
    }

    fieldset {
        margin-bottom:20px;
    }
</style>

<script type="text/javascript">
    function onAlcoaSponsorValidation(s, e) {
        //console.log("AlcoaPrivileges.GetSelectedItem().text", AlcoaPrivileges.GetSelectedItem().text);
        //console.log("AlcoaSponsor.GetValue()", AlcoaSponsor.GetValue());

        e.isValid = (!AlcoaPrivileges.GetSelectedItem() || AlcoaPrivileges.GetSelectedItem().text === "No" || (AlcoaPrivileges.GetSelectedItem().text === "Yes" && AlcoaSponsor.GetValue()));
    }
    function onAlcoaPrivilegesChanged(s, e) {
        validateSponsor();
    }
    function validateSponsor() {
        AlcoaSponsor.Validate();
    }
</script>

<fieldset>
    <legend>Profile</legend>
    
    <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
        <thead>
            <tr>
                <th width="150px"></th>
                <th></th>
                <th width="200px"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Title" AssociatedControlID="cbTitle"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbTitle" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px" TabIndex="1">
                        <Items>
                            <dx:ListEditItem Text="--please select--" Value="" />
                            <dx:ListEditItem Text="Mr" Value="Mr" />
                            <dx:ListEditItem Text="Mrs" Value="Mrs" />
                            <dx:ListEditItem Text="Miss" Value="Miss" />
                            <dx:ListEditItem Text="Ms" Value="Ms" />
                            <dx:ListEditItem Text="Sir" Value="Sir" />
                        </Items>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="First Name" AssociatedControlID="tbFirstName"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbFirstName" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="2">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblAddressType" runat="server" Text="Address Type" AssociatedControlID="cbAddressType"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbAddressType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px" TabIndex="20">
                        <Items>
                            <dx:ListEditItem Text="--please select--" Value="" />
                            <dx:ListEditItem Text="Home" Value="Home" />
                            <dx:ListEditItem Text="Work" Value="Work" />
                        </Items>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Middle Name" AssociatedControlID="tbMiddleName"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbMiddleName" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="3">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblAddress1" runat="server" Text="Address" AssociatedControlID="tbAddress1"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAddress1" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="21">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Last Name" AssociatedControlID="tbLastName"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbLastName" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="4">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAddress2" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="22">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblDateOfBirth" runat="server" Text="Date Of Birth" AssociatedControlID="dtDateOfBirth"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtDateOfBirth" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="5">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
                <td class="td-label">
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAddress3" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="23">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Home Phone" AssociatedControlID="tbHomePhone"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbHomePhone" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="6">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblCity" runat="server" Text="City" AssociatedControlID="tbCity"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbCity" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="24">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Mobile Phone" AssociatedControlID="tbMobilePhone"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbMobilePhone" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="7">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblState" runat="server" Text="State" AssociatedControlID="tbState"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbState" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="25">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
               <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel15" runat="server" Text="Alcoa Email Address" AssociatedControlID="tbEmailAddress"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbEmailAddress" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="8">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                            <RegularExpression ValidationExpression="^\w+([-+.'%]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" ErrorText="Invalid e-mail"/>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblPostalCode" runat="server" Text="Postal Code" AssociatedControlID="tbPostalCode"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbPostalCode" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="26">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="Original Hire Date" AssociatedControlID="dtOriginalHireDate"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtOriginalHireDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="9">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>--%>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="lblCountry" runat="server" Text="Country" AssociatedControlID="cbCountry"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbCountry" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px"
                        DataSourceID="CountryDataSource" TextField="Text" ValueField="Value" TabIndex="27">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Login ID" AssociatedControlID="tbLoginID"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbLoginID" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="10">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel42" runat="server" Text="Gender" AssociatedControlID="rbGender"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxRadioButtonList ID="rbGender" runat="server" ValueType="System.String" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" RepeatDirection="Horizontal" 
                        Border-BorderStyle="None" Paddings-PaddingLeft="0" TabIndex="28">
                        <Items>
                            <dx:ListEditItem Text="Male" Value="M" />
                            <dx:ListEditItem Text="Female" Value="F" />
                        </Items>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxRadioButtonList>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Assignment</legend>

    <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
        <thead>
            <tr>
                <th width="150px"></th>
                <th></th>
                <th width="200px"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Contractor Number" AssociatedControlID="tbContractorNumber"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbContractorNumber" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Enabled="false" TabIndex="30">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Person Type" AssociatedControlID="cbPersonType"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbPersonType" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px" Enabled="false" TabIndex="31">
                        <Items>
                            <dx:ListEditItem Text="Contingent Worker" Value="C" Selected="true" />
                        </Items>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                   <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Assignment" AssociatedControlID="cbAssignment"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbAssignment" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px" TabIndex="32">
                        <%--DataSourceID="CompaniesDataSource1" TextField="CompanyName" ValueField="CompanyId">--%>
                        <Items>
                            <dx:ListEditItem Text="--please select--" Value="" />
                            <dx:ListEditItem Text="CONTRACTOR.GENERAL" Value="CONTRACTOR.GENERAL" />
                        </Items>
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Assignment Number" AssociatedControlID="tbAssignmentNumber"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAssignmentNumber" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="33">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Placement Start Date" AssociatedControlID="dtPlacementStartDate"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtPlacementStartDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="34">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Assignment Effective Start Date" AssociatedControlID="dtAssignmentEffectiveStartDate"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtAssignmentEffectiveStartDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="35">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                </td>
                <td>

                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel18" runat="server" Text="Assignment Effective End Date" AssociatedControlID="dtAssignmentEffectiveEndDate"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtAssignmentEffectiveEndDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="36">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Assignment Status" AssociatedControlID="tbAssignmentStatus"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAssignmentStatus" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="37">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel16" runat="server" Text="Assignment Category" AssociatedControlID="tbAssignmentCategory"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbAssignmentCategory" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="38">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="Alcoa Privileges" AssociatedControlID="rbAlcoaPrivileges"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxRadioButtonList ID="rbAlcoaPrivileges" runat="server" ValueType="System.String" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" RepeatDirection="Horizontal" 
                        Border-BorderStyle="None" Paddings-PaddingLeft="0" ClientInstanceName="AlcoaPrivileges" TabIndex="39">
                        <Items>
                            <dx:ListEditItem Text="Yes" Value="Yes" />
                            <dx:ListEditItem Text="No" Value="No" />
                        </Items>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                        <ClientSideEvents SelectedIndexChanged="onAlcoaPrivilegesChanged"></ClientSideEvents>
                    </dx:ASPxRadioButtonList>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="Old Contractor Number" AssociatedControlID="tbOldContractorNumber"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbOldContractorNumber" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="40">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text="Location" AssociatedControlID="cbLocation"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbLocation" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" Width="225px"
                        DataSourceID="SitesDataSource" TextField="Text" ValueField="Value" TabIndex="41">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="Organization" AssociatedControlID="tbOrganization"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbOrganization" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="42">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel26" runat="server" Text="Alcoa Job" AssociatedControlID="cbAlcoaJob"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbAlcoaJob" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="AlcoaJobDataSource" TextField="Text" ValueField="Value" TabIndex="43">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel28" runat="server" Text="Alcoa Supervisor" AssociatedControlID="cbAlcoaSupervisor"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbAlcoaSupervisor" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="AlcoaPersonDataSource" TextField="Text" ValueField="Value" TabIndex="44">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel31" runat="server" Text="Labour Classification" AssociatedControlID="cbLabourClassification"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbLabourClassification" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="LabourClassesDataSource" TextField="Text" ValueField="Value" TabIndex="45">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel33" runat="server" Text="Alcoa Sponsor" AssociatedControlID="tbAlcoaSponsor"></dx:ASPxLabel>
                </td>
                <td onload="validateSponsor();">
                    <dx:ASPxTextBox ID="tbAlcoaSponsor" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" ClientInstanceName="AlcoaSponsor" TabIndex="46">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" EnableCustomValidation="true" ErrorText="Required field" />
                        <ClientSideEvents Validation="onAlcoaSponsorValidation" Init="function(s, e) { AlcoaSponsor.Validate(); }" />
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel39" runat="server" Text="Labour Classification 2" AssociatedControlID="cbLabourClassification2"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbLabourClassification2" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="LabourClassesDataSource" TextField="Text" ValueField="Value" TabIndex="47">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel41" runat="server" Text="Name??" AssociatedControlID="cbName"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbName" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="NameDataSource" TextField="Text" ValueField="Value" TabIndex="48">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel40" runat="server" Text="Labour Classification 3" AssociatedControlID="cbLabourClassification3"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbLabourClassification3" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="LabourClassesDataSource" TextField="Text" ValueField="Value" TabIndex="49">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
                <td class="td-label">
                </td>
                <td>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Supplier</legend>

    <table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
        <thead>
            <tr>
                <th width="150px"></th>
                <th></th>
                <th width="200px"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel34" runat="server" Text="Supplier" AssociatedControlID="tbSupplier"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbSupplier" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="50">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel35" runat="server" Text="Projected Contract End Date" AssociatedControlID="dtProjectedContractEndDate"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxDateEdit ID="dtProjectedContractEndDate" CssPostfix="Office2003Blue" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        runat="server" Width="225px" EditFormatString="dd/MM/yyyy"
                        EditFormat="Custom" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="51">
                        <ButtonStyle Width="13px">
                        </ButtonStyle>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip" SetFocusOnError="True">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field"></RequiredField>--%>
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel36" runat="server" Text="Non-Pref. Supplier" AssociatedControlID="tbNonPrefSupplier"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbNonPrefSupplier" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="52">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel37" runat="server" Text="Supplier Supervisor on Site" AssociatedControlID="cbSupplierSupervisorOnSite"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxComboBox ID="cbSupplierSupervisorOnSite" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css"
                        Width="225px" DataSourceID="AlcoaPersonDataSource" TextField="Text" ValueField="Value" TabIndex="53">
                        <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                        </LoadingPanelImage>
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <%--<RequiredField IsRequired="True" ErrorText="Required field" />--%>
                        </ValidationSettings>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td class="td-label">
                    <dx:ASPxLabel ID="ASPxLabel38" runat="server" Text="People Group" AssociatedControlID="tbPeopleGroup"></dx:ASPxLabel>
                </td>
                <td>
                    <dx:ASPxTextBox ID="tbPeopleGroup" runat="server" Width="225px" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                        CssPostfix="Office2003Blue" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" TabIndex="54">
                        <ValidationSettings ValidationGroup="PersonGroup" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                            <RequiredField IsRequired="True" ErrorText="Required field" />
                        </ValidationSettings>
                    </dx:ASPxTextBox>
                </td>
                <td class="td-label">
                </td>
                <td>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<table border="0" cellpadding="2" cellspacing="0" style="width: 100%">
    <thead>
        <tr>
            <th width="100%"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="right">
                <dx:ASPxButton ID="btnSubmit" runat="server" Text="Submit" ValidationGroup="PersonGroup" OnClick="btnSubmit_Click" TabIndex="55"></dx:ASPxButton>
                <%--<dx:ASPxButton ID="btnTestData" runat="server" Text="Test Data" CausesValidation="false" OnClick="btnTestData_Click"></dx:ASPxButton>--%>
            </td>
        </tr>
    </tbody>
</table>

<% if (Request.QueryString["CWK_NBR"] != "new") { %>
    <table >
        <tbody>
            <tr>
                <td colspan="4">
                    <h3>History</h3>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <dxwgv:ASPxGridView ID="grid" 
                        runat="server" 
                        AutoGenerateColumns="False" 
                        ClientInstanceName="grid"
                        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
                        CssPostfix="Office2003Blue" 
                        DataSourceID="ContractorDataSource"
                        KeyFieldName="UserId" 
                        Width="100%">
                        <Columns>
                            <dxwgv:GridViewDataTextColumn 
                                Caption="Contractor Number" 
                                FieldName="CWK_NBR" 
                                ReadOnly="True"  
                                VisibleIndex="1"
                                Width="150px">
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn 
                                Caption="Full Name (Last, First)" 
                                FieldName="FULL_NAME" 
                                SortIndex="0" 
                                VisibleIndex="2">
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataTextColumn 
                                Caption="Company" 
                                FieldName="CWK_VENDOR_NAME" 
                                VisibleIndex="3">
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                            <dxwgv:GridViewDataDateColumn 
                                Caption="From Date" 
                                FieldName="EFFECTIVE_START_DATE" 
                                VisibleIndex="4" 
                                SortOrder="Descending"
                                Width="100px">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit> 
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataDateColumn 
                                Caption="To Date" 
                                FieldName="EFFECTIVE_END_DATE" 
                                VisibleIndex="5" 
                                Width="100px">
                                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy"></PropertiesDateEdit> 
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataDateColumn>
                            <dxwgv:GridViewDataTextColumn 
                                Caption="Updated Date" 
                                FieldName="LAST_UPDATE_DATE" 
                                VisibleIndex="6" 
                                Width="100px">
                                <Settings AutoFilterCondition="Contains" />
                                <CellStyle HorizontalAlign="Left"></CellStyle>
                            </dxwgv:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior ColumnResizeMode="Control" ConfirmDelete="True" />
                        <SettingsPager>
                            <AllButton Visible="True">
                            </AllButton>
                        </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" ShowHeaderFilterButton="True" />
                        <SettingsCustomizationWindow Enabled="True" />
                        <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
                            </LoadingPanelOnStatusBar>
                            <CollapsedButton Height="12px" Width="11px">
                            </CollapsedButton>
                            <ExpandedButton Height="12px" Width="11px">
                            </ExpandedButton>
                            <DetailCollapsedButton Height="12px" Width="11px">
                            </DetailCollapsedButton>
                            <DetailExpandedButton Height="12px" Width="11px">
                            </DetailExpandedButton>
                            <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
                            </LoadingPanel>
                        </Images>
                        <ImagesFilterControl>
                            <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
                            </LoadingPanel>
                        </ImagesFilterControl>
                        <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" CssPostfix="Office2003Blue">
                            <Header ImageSpacing="5px" SortingImageSpacing="5px">
                            </Header>
                            <LoadingPanel ImageSpacing="10px">
                            </LoadingPanel>
                        </Styles>
                    </dxwgv:ASPxGridView>
                </td>
            </tr>
        </tbody>
    </table>

    <asp:SqlDataSource  
        ID="ContractorDataSource" 
        runat="server" 
        ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
        SelectCommand="[HR].[XXHR_CWK_History_SelectByCwkNbr]" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter Name="CWK_NBR" QueryStringField="CWK_NBR" />
        </SelectParameters>
    </asp:SqlDataSource>

<% } %>

<dxpc:ASPxPopupControl ID="ASPxPopupControl1" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue" EnableHotTrack="False" HeaderText="Warning" Height="111px"
    Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
    Width="439px" SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
    <SizeGripImage Height="16px" Width="16px">
    </SizeGripImage>
    <HeaderStyle>
        <Paddings PaddingRight="6px"></Paddings>
    </HeaderStyle>
    <CloseButtonImage Height="12px" Width="13px">
    </CloseButtonImage>
    <ContentCollection>
        <dxpc:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
            <dx:ASPxLabel runat="server" Height="30px" Font-Size="14px" ID="ASPxLabel9">
                <Border BorderColor="White" BorderWidth="10px"></Border>
            </dx:ASPxLabel>
        </dxpc:PopupControlContentControl>
    </ContentCollection>
</dxpc:ASPxPopupControl>

<asp:SqlDataSource  
    ID="SitesDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT '--please select--' as [Text],
	                      null as [Value],
	                      -1 [Ordinal]
                   UNION
                   SELECT [SiteNameHr] as [Text],
	                      [SiteNameHr] as [Value],
	                      [Ordinal]
                     FROM [dbo].[Sites]
                     WHERE [IsVisible] = 1 AND [SiteNameHr] IS NOT NULL
                   ORDER BY [Ordinal]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="AlcoaJobDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM (
	                    SELECT	'--please select--' as [Text],
			                    null as [Value],
			                    -1 as [Ordinal]
	                    UNION
	                    SELECT distinct([POS_DESCR]) [Text],
		                       ([POS_DESCR]) [Value],
		                       1 as [Ordinal]
	                      FROM [HR].[XXHR_CWK_HISTORY]
	                      WHERE [POS_DESCR] IS NOT NULL
                    ) AlcoaJob 
                    ORDER BY [Ordinal], [Text]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="AlcoaPersonDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM (
	                    SELECT	'--please select--' as [Text],
			                    null as [Value],
			                    -1 as [Ordinal]
	                    UNION
	                    SELECT [FULL_NAME] as [Text],
			                    [EMPLID] as [Value],
			                    1 [Ordinal]
		                    FROM [HR].[XXHR_PEOPLE_DETAILS]
		                    WHERE PER_TYPE_ID = 'E'
                    ) AlcoaJob 
                    ORDER BY [Ordinal], [Text]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="CountryDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM (
	                    SELECT	'--please select--' as [Text],
			                    null as [Value],
			                    -1 as [Ordinal]
	                    UNION
	                    SELECT [Country] as [Text],
			                    [Country] as [Value],
			                    1 as [Ordinal]
	                    FROM [dbo].[ISOCountryCodes]
                    ) Countries 
                    ORDER BY [Ordinal], [Text]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="LabourClassesDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM (
	                    SELECT	'--please select--' as [Text],
			                    null as [Value],
			                    -1 as [Ordinal]
	                    UNION
	                    SELECT [LabourClassTypeDesc] as [Text],
			                    [LabourClassTypeDesc] as [Value],
			                    1 as [Ordinal]
	                    FROM [dbo].[LabourClassType]
						WHERE [LabourClassTypeIsActive] = 1
                    ) LabourClasses 
                    ORDER BY [Ordinal], [Text]"
    SelectCommandType="Text">
</asp:SqlDataSource>

<asp:SqlDataSource  
    ID="NameDataSource" 
    runat="server" 
    ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT * FROM (
	                    SELECT	'--please select--' as [Text],
			                    null as [Value],
			                    -1 as [Ordinal]
	                    UNION
	                    SELECT distinct([NAME]) [Text],
		                        ([NAME]) [Value],
		                        1 as [Ordinal]
	                        FROM [HR].[XXHR_CWK_HISTORY]
	                        WHERE [POS_DESCR] IS NOT NULL
                    ) Name 
                    ORDER BY [Ordinal], [Text]"
    SelectCommandType="Text">
</asp:SqlDataSource>