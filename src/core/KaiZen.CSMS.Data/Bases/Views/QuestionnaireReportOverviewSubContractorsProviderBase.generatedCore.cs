﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireReportOverviewSubContractorsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class QuestionnaireReportOverviewSubContractorsProviderBaseCore : EntityViewProviderBase<QuestionnaireReportOverviewSubContractors>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubContractors&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;QuestionnaireReportOverviewSubContractors&gt;"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubContractors&gt; Fill(DataSet dataSet, VList<QuestionnaireReportOverviewSubContractors> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<QuestionnaireReportOverviewSubContractors>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;QuestionnaireReportOverviewSubContractors&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<QuestionnaireReportOverviewSubContractors>"/></returns>
		protected static VList&lt;QuestionnaireReportOverviewSubContractors&gt; Fill(DataTable dataTable, VList<QuestionnaireReportOverviewSubContractors> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					QuestionnaireReportOverviewSubContractors c = new QuestionnaireReportOverviewSubContractors();
					c.CompanyId = (Convert.IsDBNull(row["CompanyId"]))?(int)0:(System.Int32)row["CompanyId"];
					c.SqExpiryDate = (Convert.IsDBNull(row["SqExpiryDate"]))?DateTime.MinValue:(System.DateTime?)row["SqExpiryDate"];
					c.SupervisoryLevel = (Convert.IsDBNull(row["SupervisoryLevel"]))?(int)0:(System.Int32?)row["SupervisoryLevel"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;QuestionnaireReportOverviewSubContractors&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;QuestionnaireReportOverviewSubContractors&gt;"/></returns>
		protected VList<QuestionnaireReportOverviewSubContractors> Fill(IDataReader reader, VList<QuestionnaireReportOverviewSubContractors> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					QuestionnaireReportOverviewSubContractors entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<QuestionnaireReportOverviewSubContractors>("QuestionnaireReportOverviewSubContractors",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new QuestionnaireReportOverviewSubContractors();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.CompanyId)];
					//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
					entity.SqExpiryDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubContractorsColumn.SqExpiryDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.SqExpiryDate)];
					//entity.SqExpiryDate = (Convert.IsDBNull(reader["SqExpiryDate"]))?DateTime.MinValue:(System.DateTime?)reader["SqExpiryDate"];
					entity.SupervisoryLevel = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubContractorsColumn.SupervisoryLevel)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.SupervisoryLevel)];
					//entity.SupervisoryLevel = (Convert.IsDBNull(reader["SupervisoryLevel"]))?(int)0:(System.Int32?)reader["SupervisoryLevel"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubContractors"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubContractors"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, QuestionnaireReportOverviewSubContractors entity)
		{
			reader.Read();
			entity.CompanyId = (System.Int32)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.CompanyId)];
			//entity.CompanyId = (Convert.IsDBNull(reader["CompanyId"]))?(int)0:(System.Int32)reader["CompanyId"];
			entity.SqExpiryDate = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubContractorsColumn.SqExpiryDate)))?null:(System.DateTime?)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.SqExpiryDate)];
			//entity.SqExpiryDate = (Convert.IsDBNull(reader["SqExpiryDate"]))?DateTime.MinValue:(System.DateTime?)reader["SqExpiryDate"];
			entity.SupervisoryLevel = (reader.IsDBNull(((int)QuestionnaireReportOverviewSubContractorsColumn.SupervisoryLevel)))?null:(System.Int32?)reader[((int)QuestionnaireReportOverviewSubContractorsColumn.SupervisoryLevel)];
			//entity.SupervisoryLevel = (Convert.IsDBNull(reader["SupervisoryLevel"]))?(int)0:(System.Int32?)reader["SupervisoryLevel"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="QuestionnaireReportOverviewSubContractors"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="QuestionnaireReportOverviewSubContractors"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, QuestionnaireReportOverviewSubContractors entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompanyId = (Convert.IsDBNull(dataRow["CompanyId"]))?(int)0:(System.Int32)dataRow["CompanyId"];
			entity.SqExpiryDate = (Convert.IsDBNull(dataRow["SqExpiryDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["SqExpiryDate"];
			entity.SupervisoryLevel = (Convert.IsDBNull(dataRow["SupervisoryLevel"]))?(int)0:(System.Int32?)dataRow["SupervisoryLevel"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region QuestionnaireReportOverviewSubContractorsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsFilterBuilder : SqlFilterBuilder<QuestionnaireReportOverviewSubContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubContractorsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubContractorsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubContractorsFilterBuilder

	#region QuestionnaireReportOverviewSubContractorsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireReportOverviewSubContractorsParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireReportOverviewSubContractorsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsParameterBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireReportOverviewSubContractorsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireReportOverviewSubContractorsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireReportOverviewSubContractorsParameterBuilder
	
	#region QuestionnaireReportOverviewSubContractorsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireReportOverviewSubContractors"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireReportOverviewSubContractorsSortBuilder : SqlSortBuilder<QuestionnaireReportOverviewSubContractorsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireReportOverviewSubContractorsSqlSortBuilder class.
		/// </summary>
		public QuestionnaireReportOverviewSubContractorsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireReportOverviewSubContractorsSortBuilder

} // end namespace
