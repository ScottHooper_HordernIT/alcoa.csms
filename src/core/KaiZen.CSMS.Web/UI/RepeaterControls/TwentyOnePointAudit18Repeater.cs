﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.Design.WebControls;
using System.Web.UI.Design;
using System.Web.UI.WebControls;

namespace KaiZen.CSMS.Web.UI
{
    /// <summary>
    /// A designer class for a strongly typed repeater <c>TwentyOnePointAudit18Repeater</c>
    /// </summary>
	public class TwentyOnePointAudit18RepeaterDesigner : System.Web.UI.Design.ControlDesigner
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit18RepeaterDesigner"/> class.
        /// </summary>
		public TwentyOnePointAudit18RepeaterDesigner()
		{
		}

        /// <summary>
        /// Initializes the control designer and loads the specified component.
        /// </summary>
        /// <param name="component">The control being designed.</param>
		public override void Initialize(IComponent component)
		{
			if (!(component is TwentyOnePointAudit18Repeater))
			{ 
				throw new ArgumentException("Component is not a TwentyOnePointAudit18Repeater."); 
			} 
			base.Initialize(component); 
			base.SetViewFlags(ViewFlags.TemplateEditing, true); 
		}


		/// <summary>
		/// Generate HTML for the designer
		/// </summary>
		/// <returns>a string of design time HTML</returns>
		public override string GetDesignTimeHtml()
		{

			// Get the instance this designer applies to
			//
			TwentyOnePointAudit18Repeater z = (TwentyOnePointAudit18Repeater)Component;
			z.DataBind();

			return base.GetDesignTimeHtml();

			//return z.RenderAtDesignTime();

			//	ControlCollection c = z.Controls;
			//Totem z = (Totem) Component;
			//Totem z = (Totem) Component;
			//return ("<div style='border: 1px gray dotted; background-color: lightgray'><b>TagStat :</b> zae |  qsdds</div>");

		}
	}

    /// <summary>
    /// A strongly typed repeater control for the <see cref="TwentyOnePointAudit18Repeater"/> Type.
    /// </summary>
	[Designer(typeof(TwentyOnePointAudit18RepeaterDesigner))]
	[ParseChildren(true)]
	[ToolboxData("<{0}:TwentyOnePointAudit18Repeater runat=\"server\"></{0}:TwentyOnePointAudit18Repeater>")]
	public class TwentyOnePointAudit18Repeater : CompositeDataBoundControl, System.Web.UI.INamingContainer
	{
	    /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit18Repeater"/> class.
        /// </summary>
		public TwentyOnePointAudit18Repeater()
		{
		}

		/// <summary>
        /// Gets a <see cref="T:System.Web.UI.ControlCollection"></see> object that represents the child controls for a specified server control in the UI hierarchy.
        /// </summary>
        /// <value></value>
        /// <returns>The collection of child controls for the specified server control.</returns>
		public override ControlCollection Controls
		{
			get
			{
				this.EnsureChildControls();
				return base.Controls;
			}
		}

		private ITemplate m_headerTemplate;
		/// <summary>
        /// Gets or sets the header template.
        /// </summary>
        /// <value>The header template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit18Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate HeaderTemplate
		{
			get { return m_headerTemplate; }
			set { m_headerTemplate = value; }
		}

		private ITemplate m_itemTemplate;
		/// <summary>
        /// Gets or sets the item template.
        /// </summary>
        /// <value>The item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit18Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate ItemTemplate
		{
			get { return m_itemTemplate; }
			set { m_itemTemplate = value; }
		}

		private ITemplate m_seperatorTemplate;
        /// <summary>
        /// Gets or sets the Seperator Template
        /// </summary>
        [Browsable(false)]
        [TemplateContainer(typeof(TwentyOnePointAudit18Item))]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public ITemplate SeperatorTemplate
        {
            get { return m_seperatorTemplate; }
            set { m_seperatorTemplate = value; }
        }
			
		private ITemplate m_altenateItemTemplate;
        /// <summary>
        /// Gets or sets the alternating item template.
        /// </summary>
        /// <value>The alternating item template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit18Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate AlternatingItemTemplate
		{
			get { return m_altenateItemTemplate; }
			set { m_altenateItemTemplate = value; }
		}

		private ITemplate m_footerTemplate;
        /// <summary>
        /// Gets or sets the footer template.
        /// </summary>
        /// <value>The footer template.</value>
		[Browsable(false)]
		[TemplateContainer(typeof(TwentyOnePointAudit18Item))]
		[PersistenceMode(PersistenceMode.InnerDefaultProperty)]
		public ITemplate FooterTemplate
		{
			get { return m_footerTemplate; }
			set { m_footerTemplate = value; }
		}

//      /// <summary>
//      /// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
//      /// </summary>
//		protected override void CreateChildControls()
//      {
//         if (ChildControlsCreated)
//         {
//            return;
//         }

//         Controls.Clear();

//         //Instantiate the Header template (if exists)
//         if (m_headerTemplate != null)
//         {
//            Control headerItem = new Control();
//            m_headerTemplate.InstantiateIn(headerItem);
//            Controls.Add(headerItem);
//         }

//         //Instantiate the Footer template (if exists)
//         if (m_footerTemplate != null)
//         {
//            Control footerItem = new Control();
//            m_footerTemplate.InstantiateIn(footerItem);
//            Controls.Add(footerItem);
//         }
//
//         ChildControlsCreated = true;
//      }
	
		/// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            
        }

        /// <summary>
        /// Overridden and Empty so that span tags are not written
        /// </summary>
        /// <param name="writer"></param>
        public override void RenderEndTag(HtmlTextWriter writer)
        {
                
        }		
		
		/// <summary>
      	/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
      	/// </summary>
		protected override int CreateChildControls(System.Collections.IEnumerable dataSource, bool dataBinding)
      	{
         int pos = 0;

         if (dataBinding)
         {
            //Instantiate the Header template (if exists)
            if (m_headerTemplate != null)
            {
                Control headerItem = new Control();
                m_headerTemplate.InstantiateIn(headerItem);
                Controls.Add(headerItem);
            }
			if (dataSource != null)
			{
				foreach (object o in dataSource)
				{
						KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity = o as KaiZen.CSMS.Entities.TwentyOnePointAudit18;
						TwentyOnePointAudit18Item container = new TwentyOnePointAudit18Item(entity);
	
						if (m_itemTemplate != null && (pos % 2) == 0)
						{
							m_itemTemplate.InstantiateIn(container);
							
							if (m_seperatorTemplate != null)
							{
								m_seperatorTemplate.InstantiateIn(container);
							}
						}
						else
						{
							if (m_altenateItemTemplate != null)
							{
								m_altenateItemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
								
							}
							else if (m_itemTemplate != null)
							{
								m_itemTemplate.InstantiateIn(container);
								
								if (m_seperatorTemplate != null)
								{
									m_seperatorTemplate.InstantiateIn(container);
								}
							}
							else
							{
								// no template !!!
							}
						}
						Controls.Add(container);
						
						container.DataBind();
						
						pos++;
				}
			}
            //Instantiate the Footer template (if exists)
            if (m_footerTemplate != null)
            {
                Control footerItem = new Control();
                m_footerTemplate.InstantiateIn(footerItem);
                Controls.Add(footerItem);
            }

		}
			
			return pos;
		}

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.PreRender"></see> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"></see> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			base.DataBind();
		}

		#region Design time
        /// <summary>
        /// Renders at design time.
        /// </summary>
        /// <returns>a  string of the Designed HTML</returns>
		internal string RenderAtDesignTime()
		{			
			return "Designer currently not implemented"; 
		}

		#endregion
	}

    /// <summary>
    /// A wrapper type for the entity
    /// </summary>
	[System.ComponentModel.ToolboxItem(false)]
	public class TwentyOnePointAudit18Item : System.Web.UI.Control, System.Web.UI.INamingContainer
	{
		private KaiZen.CSMS.Entities.TwentyOnePointAudit18 _entity;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit18Item"/> class.
        /// </summary>
		public TwentyOnePointAudit18Item()
			: base()
		{ }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:TwentyOnePointAudit18Item"/> class.
        /// </summary>
		public TwentyOnePointAudit18Item(KaiZen.CSMS.Entities.TwentyOnePointAudit18 entity)
			: base()
		{
			_entity = entity;
		}
		
        /// <summary>
        /// Gets the Id
        /// </summary>
        /// <value>The Id.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32 Id
		{
			get { return _entity.Id; }
		}
        /// <summary>
        /// Gets the Achieved18a
        /// </summary>
        /// <value>The Achieved18a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18a
		{
			get { return _entity.Achieved18a; }
		}
        /// <summary>
        /// Gets the Achieved18b
        /// </summary>
        /// <value>The Achieved18b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18b
		{
			get { return _entity.Achieved18b; }
		}
        /// <summary>
        /// Gets the Achieved18c
        /// </summary>
        /// <value>The Achieved18c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18c
		{
			get { return _entity.Achieved18c; }
		}
        /// <summary>
        /// Gets the Achieved18d
        /// </summary>
        /// <value>The Achieved18d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18d
		{
			get { return _entity.Achieved18d; }
		}
        /// <summary>
        /// Gets the Achieved18e
        /// </summary>
        /// <value>The Achieved18e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18e
		{
			get { return _entity.Achieved18e; }
		}
        /// <summary>
        /// Gets the Achieved18f
        /// </summary>
        /// <value>The Achieved18f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18f
		{
			get { return _entity.Achieved18f; }
		}
        /// <summary>
        /// Gets the Achieved18g
        /// </summary>
        /// <value>The Achieved18g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18g
		{
			get { return _entity.Achieved18g; }
		}
        /// <summary>
        /// Gets the Achieved18h
        /// </summary>
        /// <value>The Achieved18h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18h
		{
			get { return _entity.Achieved18h; }
		}
        /// <summary>
        /// Gets the Achieved18i
        /// </summary>
        /// <value>The Achieved18i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18i
		{
			get { return _entity.Achieved18i; }
		}
        /// <summary>
        /// Gets the Achieved18j
        /// </summary>
        /// <value>The Achieved18j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18j
		{
			get { return _entity.Achieved18j; }
		}
        /// <summary>
        /// Gets the Achieved18k
        /// </summary>
        /// <value>The Achieved18k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18k
		{
			get { return _entity.Achieved18k; }
		}
        /// <summary>
        /// Gets the Achieved18l
        /// </summary>
        /// <value>The Achieved18l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18l
		{
			get { return _entity.Achieved18l; }
		}
        /// <summary>
        /// Gets the Achieved18m
        /// </summary>
        /// <value>The Achieved18m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18m
		{
			get { return _entity.Achieved18m; }
		}
        /// <summary>
        /// Gets the Achieved18n
        /// </summary>
        /// <value>The Achieved18n.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18n
		{
			get { return _entity.Achieved18n; }
		}
        /// <summary>
        /// Gets the Achieved18o
        /// </summary>
        /// <value>The Achieved18o.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18o
		{
			get { return _entity.Achieved18o; }
		}
        /// <summary>
        /// Gets the Achieved18p
        /// </summary>
        /// <value>The Achieved18p.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? Achieved18p
		{
			get { return _entity.Achieved18p; }
		}
        /// <summary>
        /// Gets the Observation18a
        /// </summary>
        /// <value>The Observation18a.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18a
		{
			get { return _entity.Observation18a; }
		}
        /// <summary>
        /// Gets the Observation18b
        /// </summary>
        /// <value>The Observation18b.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18b
		{
			get { return _entity.Observation18b; }
		}
        /// <summary>
        /// Gets the Observation18c
        /// </summary>
        /// <value>The Observation18c.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18c
		{
			get { return _entity.Observation18c; }
		}
        /// <summary>
        /// Gets the Observation18d
        /// </summary>
        /// <value>The Observation18d.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18d
		{
			get { return _entity.Observation18d; }
		}
        /// <summary>
        /// Gets the Observation18e
        /// </summary>
        /// <value>The Observation18e.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18e
		{
			get { return _entity.Observation18e; }
		}
        /// <summary>
        /// Gets the Observation18f
        /// </summary>
        /// <value>The Observation18f.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18f
		{
			get { return _entity.Observation18f; }
		}
        /// <summary>
        /// Gets the Observation18g
        /// </summary>
        /// <value>The Observation18g.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18g
		{
			get { return _entity.Observation18g; }
		}
        /// <summary>
        /// Gets the Observation18h
        /// </summary>
        /// <value>The Observation18h.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18h
		{
			get { return _entity.Observation18h; }
		}
        /// <summary>
        /// Gets the Observation18i
        /// </summary>
        /// <value>The Observation18i.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18i
		{
			get { return _entity.Observation18i; }
		}
        /// <summary>
        /// Gets the Observation18j
        /// </summary>
        /// <value>The Observation18j.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18j
		{
			get { return _entity.Observation18j; }
		}
        /// <summary>
        /// Gets the Observation18k
        /// </summary>
        /// <value>The Observation18k.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18k
		{
			get { return _entity.Observation18k; }
		}
        /// <summary>
        /// Gets the Observation18l
        /// </summary>
        /// <value>The Observation18l.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18l
		{
			get { return _entity.Observation18l; }
		}
        /// <summary>
        /// Gets the Observation18m
        /// </summary>
        /// <value>The Observation18m.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18m
		{
			get { return _entity.Observation18m; }
		}
        /// <summary>
        /// Gets the Observation18n
        /// </summary>
        /// <value>The Observation18n.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18n
		{
			get { return _entity.Observation18n; }
		}
        /// <summary>
        /// Gets the Observation18o
        /// </summary>
        /// <value>The Observation18o.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18o
		{
			get { return _entity.Observation18o; }
		}
        /// <summary>
        /// Gets the Observation18p
        /// </summary>
        /// <value>The Observation18p.</value>
		[System.ComponentModel.Bindable(true)]
		public System.String Observation18p
		{
			get { return _entity.Observation18p; }
		}
        /// <summary>
        /// Gets the TotalScore
        /// </summary>
        /// <value>The TotalScore.</value>
		[System.ComponentModel.Bindable(true)]
		public System.Int32? TotalScore
		{
			get { return _entity.TotalScore; }
		}

        /// <summary>
        /// Gets a <see cref="T:KaiZen.CSMS.Entities.TwentyOnePointAudit18"></see> object
        /// </summary>
        /// <value></value>
        [System.ComponentModel.Bindable(true)]
        public KaiZen.CSMS.Entities.TwentyOnePointAudit18 Entity
        {
            get { return _entity; }
        }
	}
}
