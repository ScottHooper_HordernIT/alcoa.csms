﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationAssessmentAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationAssessmentAuditDataSourceDesigner))]
	public class QuestionnaireVerificationAssessmentAuditDataSource : ProviderDataSource<QuestionnaireVerificationAssessmentAudit, QuestionnaireVerificationAssessmentAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditDataSource class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditDataSource() : base(new QuestionnaireVerificationAssessmentAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationAssessmentAuditDataSourceView used by the QuestionnaireVerificationAssessmentAuditDataSource.
		/// </summary>
		protected QuestionnaireVerificationAssessmentAuditDataSourceView QuestionnaireVerificationAssessmentAuditView
		{
			get { return ( View as QuestionnaireVerificationAssessmentAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationAssessmentAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationAssessmentAuditSelectMethod selectMethod = QuestionnaireVerificationAssessmentAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationAssessmentAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationAssessmentAuditDataSourceView class that is to be
		/// used by the QuestionnaireVerificationAssessmentAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationAssessmentAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationAssessmentAudit, QuestionnaireVerificationAssessmentAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationAssessmentAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationAssessmentAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationAssessmentAuditDataSourceView : ProviderDataSourceView<QuestionnaireVerificationAssessmentAudit, QuestionnaireVerificationAssessmentAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationAssessmentAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationAssessmentAuditDataSourceView(QuestionnaireVerificationAssessmentAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationAssessmentAuditDataSource QuestionnaireVerificationAssessmentAuditOwner
		{
			get { return Owner as QuestionnaireVerificationAssessmentAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationAssessmentAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationAssessmentAuditOwner.SelectMethod; }
			set { QuestionnaireVerificationAssessmentAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationAssessmentAuditService QuestionnaireVerificationAssessmentAuditProvider
		{
			get { return Provider as QuestionnaireVerificationAssessmentAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationAssessmentAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationAssessmentAudit> results = null;
			QuestionnaireVerificationAssessmentAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationAssessmentAuditSelectMethod.Get:
					QuestionnaireVerificationAssessmentAuditKey entityKey  = new QuestionnaireVerificationAssessmentAuditKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationAssessmentAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationAssessmentAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationAssessmentAuditSelectMethod.GetAll:
                    results = QuestionnaireVerificationAssessmentAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationAssessmentAuditSelectMethod.GetPaged:
					results = QuestionnaireVerificationAssessmentAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationAssessmentAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationAssessmentAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationAssessmentAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationAssessmentAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationAssessmentAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireVerificationAssessmentAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationAssessmentAuditSelectMethod.Get || SelectMethod == QuestionnaireVerificationAssessmentAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationAssessmentAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationAssessmentAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationAssessmentAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationAssessmentAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationAssessmentAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationAssessmentAuditDataSource class.
	/// </summary>
	public class QuestionnaireVerificationAssessmentAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationAssessmentAudit, QuestionnaireVerificationAssessmentAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationAssessmentAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationAssessmentAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationAssessmentAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationAssessmentAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationAssessmentAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationAssessmentAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationAssessmentAuditDataSourceActionList(QuestionnaireVerificationAssessmentAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAssessmentAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationAssessmentAuditDataSourceActionList
	
	#endregion QuestionnaireVerificationAssessmentAuditDataSourceDesigner
	
	#region QuestionnaireVerificationAssessmentAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationAssessmentAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationAssessmentAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion QuestionnaireVerificationAssessmentAuditSelectMethod

	#region QuestionnaireVerificationAssessmentAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditFilter : SqlFilter<QuestionnaireVerificationAssessmentAuditColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentAuditFilter

	#region QuestionnaireVerificationAssessmentAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationAssessmentAuditColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentAuditExpressionBuilder	

	#region QuestionnaireVerificationAssessmentAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationAssessmentAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessmentAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentAuditProperty : ChildEntityProperty<QuestionnaireVerificationAssessmentAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentAuditProperty
}

