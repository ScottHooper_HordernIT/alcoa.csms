﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UserPrivilegeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UserPrivilegeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.UserPrivilege, KaiZen.CSMS.Entities.UserPrivilegeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserPrivilegeKey key)
		{
			return Delete(transactionManager, key.UserPrivilegeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_userPrivilegeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _userPrivilegeId)
		{
			return Delete(null, _userPrivilegeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userPrivilegeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _userPrivilegeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		FK_UserPrivilege_CreatedByUserId Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		FK_UserPrivilege_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		/// <remarks></remarks>
		public TList<UserPrivilege> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		FK_UserPrivilege_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		fkUserPrivilegeCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		fkUserPrivilegeCreatedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_CreatedByUserId key.
		///		FK_UserPrivilege_CreatedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public abstract TList<UserPrivilege> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		FK_UserPrivilege_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		FK_UserPrivilege_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		/// <remarks></remarks>
		public TList<UserPrivilege> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		FK_UserPrivilege_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		fkUserPrivilegeModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		fkUserPrivilegeModifiedByUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_ModifiedByUserId key.
		///		FK_UserPrivilege_ModifiedByUserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public abstract TList<UserPrivilege> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		FK_UserPrivilege_PrivilegeId Description: 
		/// </summary>
		/// <param name="_privilegeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByPrivilegeId(System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByPrivilegeId(_privilegeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		FK_UserPrivilege_PrivilegeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		/// <remarks></remarks>
		public TList<UserPrivilege> GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByPrivilegeId(transactionManager, _privilegeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		FK_UserPrivilege_PrivilegeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByPrivilegeId(transactionManager, _privilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		fkUserPrivilegePrivilegeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_privilegeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByPrivilegeId(System.Int32 _privilegeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPrivilegeId(null, _privilegeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		fkUserPrivilegePrivilegeId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_privilegeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByPrivilegeId(System.Int32 _privilegeId, int start, int pageLength,out int count)
		{
			return GetByPrivilegeId(null, _privilegeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_PrivilegeId key.
		///		FK_UserPrivilege_PrivilegeId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public abstract TList<UserPrivilege> GetByPrivilegeId(TransactionManager transactionManager, System.Int32 _privilegeId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		FK_UserPrivilege_UserId Description: 
		/// </summary>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByUserId(System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(_userId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		FK_UserPrivilege_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		/// <remarks></remarks>
		public TList<UserPrivilege> GetByUserId(TransactionManager transactionManager, System.Int32 _userId)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		FK_UserPrivilege_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserId(transactionManager, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		fkUserPrivilegeUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByUserId(System.Int32 _userId, int start, int pageLength)
		{
			int count =  -1;
			return GetByUserId(null, _userId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		fkUserPrivilegeUserId Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_userId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public TList<UserPrivilege> GetByUserId(System.Int32 _userId, int start, int pageLength,out int count)
		{
			return GetByUserId(null, _userId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_UserPrivilege_UserId key.
		///		FK_UserPrivilege_UserId Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.UserPrivilege objects.</returns>
		public abstract TList<UserPrivilege> GetByUserId(TransactionManager transactionManager, System.Int32 _userId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.UserPrivilege Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserPrivilegeKey key, int start, int pageLength)
		{
			return GetByUserPrivilegeId(transactionManager, key.UserPrivilegeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_UserPrivilege index.
		/// </summary>
		/// <param name="_userPrivilegeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(System.Int32 _userPrivilegeId)
		{
			int count = -1;
			return GetByUserPrivilegeId(null,_userPrivilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserPrivilege index.
		/// </summary>
		/// <param name="_userPrivilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(System.Int32 _userPrivilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserPrivilegeId(null, _userPrivilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userPrivilegeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(TransactionManager transactionManager, System.Int32 _userPrivilegeId)
		{
			int count = -1;
			return GetByUserPrivilegeId(transactionManager, _userPrivilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userPrivilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(TransactionManager transactionManager, System.Int32 _userPrivilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserPrivilegeId(transactionManager, _userPrivilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserPrivilege index.
		/// </summary>
		/// <param name="_userPrivilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(System.Int32 _userPrivilegeId, int start, int pageLength, out int count)
		{
			return GetByUserPrivilegeId(null, _userPrivilegeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userPrivilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UserPrivilege GetByUserPrivilegeId(TransactionManager transactionManager, System.Int32 _userPrivilegeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_UserPrivilege index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(System.Int32 _userId, System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByUserIdPrivilegeId(null,_userId, _privilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserPrivilege index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(System.Int32 _userId, System.Int32 _privilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserIdPrivilegeId(null, _userId, _privilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(TransactionManager transactionManager, System.Int32 _userId, System.Int32 _privilegeId)
		{
			int count = -1;
			return GetByUserIdPrivilegeId(transactionManager, _userId, _privilegeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(TransactionManager transactionManager, System.Int32 _userId, System.Int32 _privilegeId, int start, int pageLength)
		{
			int count = -1;
			return GetByUserIdPrivilegeId(transactionManager, _userId, _privilegeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserPrivilege index.
		/// </summary>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(System.Int32 _userId, System.Int32 _privilegeId, int start, int pageLength, out int count)
		{
			return GetByUserIdPrivilegeId(null, _userId, _privilegeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_UserPrivilege index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_userId"></param>
		/// <param name="_privilegeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.UserPrivilege GetByUserIdPrivilegeId(TransactionManager transactionManager, System.Int32 _userId, System.Int32 _privilegeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies 
		
		/// <summary>
		///	This method wrap the '_UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="TList&lt;UserPrivilege&gt;"/> instance.</returns>
		public TList<UserPrivilege> GetAll_ProjectHoursVisibleForAllCompanies()
		{
			return GetAll_ProjectHoursVisibleForAllCompanies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="TList&lt;UserPrivilege&gt;"/> instance.</returns>
		public TList<UserPrivilege> GetAll_ProjectHoursVisibleForAllCompanies(int start, int pageLength)
		{
			return GetAll_ProjectHoursVisibleForAllCompanies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="TList&lt;UserPrivilege&gt;"/> instance.</returns>
		public TList<UserPrivilege> GetAll_ProjectHoursVisibleForAllCompanies(TransactionManager transactionManager)
		{
			return GetAll_ProjectHoursVisibleForAllCompanies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_UserPrivilege_GetAll_ProjectHoursVisibleForAllCompanies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="TList&lt;UserPrivilege&gt;"/> instance.</returns>
		public abstract TList<UserPrivilege> GetAll_ProjectHoursVisibleForAllCompanies(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UserPrivilege&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UserPrivilege&gt;"/></returns>
		public static TList<UserPrivilege> Fill(IDataReader reader, TList<UserPrivilege> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.UserPrivilege c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UserPrivilege")
					.Append("|").Append((System.Int32)reader[((int)UserPrivilegeColumn.UserPrivilegeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UserPrivilege>(
					key.ToString(), // EntityTrackingKey
					"UserPrivilege",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.UserPrivilege();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.UserPrivilegeId = (System.Int32)reader[((int)UserPrivilegeColumn.UserPrivilegeId - 1)];
					c.UserId = (System.Int32)reader[((int)UserPrivilegeColumn.UserId - 1)];
					c.PrivilegeId = (System.Int32)reader[((int)UserPrivilegeColumn.PrivilegeId - 1)];
					c.Active = (System.Boolean)reader[((int)UserPrivilegeColumn.Active - 1)];
					c.RegionId = (reader.IsDBNull(((int)UserPrivilegeColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UserPrivilegeColumn.RegionId - 1)];
					c.SiteId = (reader.IsDBNull(((int)UserPrivilegeColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UserPrivilegeColumn.SiteId - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)UserPrivilegeColumn.CreatedByUserId - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)UserPrivilegeColumn.CreatedDate - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)UserPrivilegeColumn.ModifiedByUserId - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)UserPrivilegeColumn.ModifiedDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.UserPrivilege entity)
		{
			if (!reader.Read()) return;
			
			entity.UserPrivilegeId = (System.Int32)reader[((int)UserPrivilegeColumn.UserPrivilegeId - 1)];
			entity.UserId = (System.Int32)reader[((int)UserPrivilegeColumn.UserId - 1)];
			entity.PrivilegeId = (System.Int32)reader[((int)UserPrivilegeColumn.PrivilegeId - 1)];
			entity.Active = (System.Boolean)reader[((int)UserPrivilegeColumn.Active - 1)];
			entity.RegionId = (reader.IsDBNull(((int)UserPrivilegeColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)UserPrivilegeColumn.RegionId - 1)];
			entity.SiteId = (reader.IsDBNull(((int)UserPrivilegeColumn.SiteId - 1)))?null:(System.Int32?)reader[((int)UserPrivilegeColumn.SiteId - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)UserPrivilegeColumn.CreatedByUserId - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)UserPrivilegeColumn.CreatedDate - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)UserPrivilegeColumn.ModifiedByUserId - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)UserPrivilegeColumn.ModifiedDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.UserPrivilege entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UserPrivilegeId = (System.Int32)dataRow["UserPrivilegeId"];
			entity.UserId = (System.Int32)dataRow["UserId"];
			entity.PrivilegeId = (System.Int32)dataRow["PrivilegeId"];
			entity.Active = (System.Boolean)dataRow["Active"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.SiteId = Convert.IsDBNull(dataRow["SiteId"]) ? null : (System.Int32?)dataRow["SiteId"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.UserPrivilege"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UserPrivilege Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserPrivilege entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource

			#region PrivilegeIdSource	
			if (CanDeepLoad(entity, "Privilege|PrivilegeIdSource", deepLoadType, innerList) 
				&& entity.PrivilegeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PrivilegeId;
				Privilege tmpEntity = EntityManager.LocateEntity<Privilege>(EntityLocator.ConstructKeyFromPkItems(typeof(Privilege), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PrivilegeIdSource = tmpEntity;
				else
					entity.PrivilegeIdSource = DataRepository.PrivilegeProvider.GetByPrivilegeId(transactionManager, entity.PrivilegeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PrivilegeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PrivilegeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PrivilegeProvider.DeepLoad(transactionManager, entity.PrivilegeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PrivilegeIdSource

			#region UserIdSource	
			if (CanDeepLoad(entity, "Users|UserIdSource", deepLoadType, innerList) 
				&& entity.UserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.UserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.UserIdSource = tmpEntity;
				else
					entity.UserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.UserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'UserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.UserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.UserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion UserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.UserPrivilege object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.UserPrivilege instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.UserPrivilege Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.UserPrivilege entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			
			#region PrivilegeIdSource
			if (CanDeepSave(entity, "Privilege|PrivilegeIdSource", deepSaveType, innerList) 
				&& entity.PrivilegeIdSource != null)
			{
				DataRepository.PrivilegeProvider.Save(transactionManager, entity.PrivilegeIdSource);
				entity.PrivilegeId = entity.PrivilegeIdSource.PrivilegeId;
			}
			#endregion 
			
			#region UserIdSource
			if (CanDeepSave(entity, "Users|UserIdSource", deepSaveType, innerList) 
				&& entity.UserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.UserIdSource);
				entity.UserId = entity.UserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UserPrivilegeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.UserPrivilege</c>
	///</summary>
	public enum UserPrivilegeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
			
		///<summary>
		/// Composite Property for <c>Privilege</c> at PrivilegeIdSource
		///</summary>
		[ChildEntityType(typeof(Privilege))]
		Privilege,
		}
	
	#endregion UserPrivilegeChildEntityTypes
	
	#region UserPrivilegeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UserPrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeFilterBuilder : SqlFilterBuilder<UserPrivilegeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilterBuilder class.
		/// </summary>
		public UserPrivilegeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserPrivilegeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserPrivilegeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserPrivilegeFilterBuilder
	
	#region UserPrivilegeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UserPrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UserPrivilegeParameterBuilder : ParameterizedSqlFilterBuilder<UserPrivilegeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeParameterBuilder class.
		/// </summary>
		public UserPrivilegeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UserPrivilegeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UserPrivilegeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UserPrivilegeParameterBuilder
	
	#region UserPrivilegeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UserPrivilegeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UserPrivilege"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UserPrivilegeSortBuilder : SqlSortBuilder<UserPrivilegeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UserPrivilegeSqlSortBuilder class.
		/// </summary>
		public UserPrivilegeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UserPrivilegeSortBuilder
	
} // end namespace
