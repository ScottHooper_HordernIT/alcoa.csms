﻿
#region using directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;

using KaiZen.CSMS.Entities.Validation;
#endregion

namespace KaiZen.CSMS.Entities
{
    ///<summary>
    /// An object representation of the 'UserCompany' table. [No description found the database]	
    ///</summary>
    [Serializable]
    [DataObject, CLSCompliant(true)]
    public abstract partial class UserCompanyBase : EntityBase, IUserCompany, IEntityId<UserCompanyKey>, System.IComparable, System.ICloneable, ICloneableEx, IEditableObject, IComponent, INotifyPropertyChanged
    {
        #region Variable Declarations

        /// <summary>
        ///  Hold the inner data of the entity.
        /// </summary>
        private UserCompanyEntityData entityData;

        /// <summary>
        /// 	Hold the original data of the entity, as loaded from the repository.
        /// </summary>
        private UserCompanyEntityData _originalData;

        /// <summary>
        /// 	Hold a backup of the inner data of the entity.
        /// </summary>
        private UserCompanyEntityData backupData;

        /// <summary>
        /// 	Key used if Tracking is Enabled for the <see cref="EntityLocator" />.
        /// </summary>
        private string entityTrackingKey;

        /// <summary>
        /// 	Hold the parent TList&lt;entity&gt; in which this entity maybe contained.
        /// </summary>
        /// <remark>Mostly used for databinding</remark>
        [NonSerialized]
        private TList<UserCompany> parentCollection;

        private bool inTxn = false;

        /// <summary>
        /// Occurs when a value is being changed for the specified column.
        /// </summary>
        [field: NonSerialized]
        public event UserCompanyEventHandler ColumnChanging;

        /// <summary>
        /// Occurs after a value has been changed for the specified column.
        /// </summary>
        [field: NonSerialized]
        public event UserCompanyEventHandler ColumnChanged;

        #endregion Variable Declarations

        #region Constructors
        ///<summary>
        /// Creates a new <see cref="UserCompanyBase"/> instance.
        ///</summary>
        public UserCompanyBase()
        {
            this.entityData = new UserCompanyEntityData();
            this.backupData = null;
        }

        ///<summary>
        /// Creates a new <see cref="UserCompanyBase"/> instance.
        ///</summary>
        ///<param name="_questionnaireId"></param>
        ///<param name="_answerId"></param>
        ///<param name="_fileName"></param>
        ///<param name="_fileHash"></param>
        ///<param name="_contentLength"></param>
        ///<param name="_content"></param>
        ///<param name="_modifiedByUserId"></param>
        ///<param name="_modifiedDate"></param>
        public UserCompanyBase(System.Int32 _userCompanyId, System.Int32 _userId, System.Int32 _companyId, System.Boolean _active)
        {
            this.entityData = new UserCompanyEntityData();
            this.backupData = null;
            this.UserCompanyId = _userCompanyId;
            this.UserId = _userId;
            this.CompanyId = _companyId;
            this.Active = _active;
            
        }

        ///<summary>
        /// A simple factory method to create a new <see cref="UserCompany"/> instance.
        ///</summary>
        ///<param name="_questionnaireId"></param>
        ///<param name="_answerId"></param>
        ///<param name="_fileName"></param>
        ///<param name="_fileHash"></param>
        ///<param name="_contentLength"></param>
        ///<param name="_content"></param>
        ///<param name="_modifiedByUserId"></param>
        ///<param name="_modifiedDate"></param>
        public static UserCompany CreateUserCompany(System.Int32 _userCompanyId, System.Int32 _userId, System.Int32 _companyId, System.Boolean _active)
        {
            UserCompany newUserCompany = new UserCompany();
            newUserCompany.UserCompanyId = _userCompanyId;
            newUserCompany.UserId = _userId;
            newUserCompany.CompanyId = _companyId;
            newUserCompany.Active = _active;

            return newUserCompany;
        }

        #endregion Constructors

        #region Properties

        #region Data Properties


        /// <summary>
        /// 	Gets or sets the UserCompanyId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 UserCompanyId
        {
            get
            {
                return this.entityData.UserCompanyId;
            }

            set
            {
                if (this.entityData.UserCompanyId == value)
                    return;

                OnPropertyChanging("UserCompanyId");
                OnColumnChanging(UserCompanyColumn.UserCompanyId, this.entityData.UserCompanyId);
                this.entityData.UserCompanyId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(UserCompanyColumn.UserCompanyId, this.entityData.UserCompanyId);
                OnPropertyChanged("UserCompanyId");
            }
        }



        /// <summary>
        /// 	Gets or sets the UserCompanyId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 UserId
        {
            get
            {
                return this.entityData.UserId;
            }

            set
            {
                if (this.entityData.UserId == value)
                    return;

                OnPropertyChanging("UserId");
                OnColumnChanging(UserCompanyColumn.UserId, this.entityData.UserId);
                this.entityData.UserId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(UserCompanyColumn.UserId, this.entityData.UserId);
                OnPropertyChanged("UserId");
            }
        }



        /// <summary>
        /// 	Gets or sets the CompanyId property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Int32 CompanyId
        {
            get
            {
                return this.entityData.CompanyId;
            }

            set
            {
                if (this.entityData.CompanyId == value)
                    return;

                OnPropertyChanging("CompanyId");
                OnColumnChanging(UserCompanyColumn.CompanyId, this.entityData.CompanyId);
                this.entityData.CompanyId = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(UserCompanyColumn.CompanyId, this.entityData.CompanyId);
                OnPropertyChanged("CompanyId");
            }
        }



        /// <summary>
        /// 	Gets or sets the Active property. 
        ///		
        /// </summary>
        /// <value>This type is int.</value>
        /// <remarks>
        /// This property can not be set to null. 
        /// </remarks>


        [DescriptionAttribute(@""), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        [DataObjectField(false, false, false)]
        public virtual System.Boolean Active
        {
            get
            {
                return this.entityData.Active;
            }

            set
            {
                if (this.entityData.Active == value)
                    return;

                OnPropertyChanging("Active");
                OnColumnChanging(UserCompanyColumn.Active, this.entityData.Active);
                this.entityData.Active = value;
                if (this.EntityState == EntityState.Unchanged)
                    this.EntityState = EntityState.Changed;
                OnColumnChanged(UserCompanyColumn.Active, this.entityData.Active);
                OnPropertyChanged("Active");
            }
        }

        #endregion Data Properties

        #region Source Foreign Key Property

        /// <summary>
        /// Gets or sets the source <see cref="Users"/>.
        /// </summary>
        /// <value>The source Users for ModifiedByUserId.</value>
        [XmlIgnore()]
        [Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        public virtual Users UserIdSource
        {
            get { return entityData.UserIdSource; }
            set { entityData.UserIdSource = value; }
        }
        /// <summary>
        /// Gets or sets the source <see cref="QuestionnaireMainAnswer"/>.
        /// </summary>
        /// <value>The source QuestionnaireMainAnswer for AnswerId.</value>
        [XmlIgnore()]
        [Browsable(false), System.ComponentModel.Bindable(System.ComponentModel.BindableSupport.Yes)]
        public virtual Companies CompanyIdSource
        {
            get { return entityData.CompanyIdSource; }
            set { entityData.CompanyIdSource = value; }
        }
        #endregion

        #region Children Collections
        #endregion Children Collections

        #endregion
        #region Validation

        /// <summary>
        /// Assigns validation rules to this object based on model definition.
        /// </summary>
        /// <remarks>This method overrides the base class to add schema related validation.</remarks>
        protected override void AddValidationRules()
        {
            //Validation rules based on database schema.
            //ValidationRules.AddRule(CommonRules.NotNull,
            //    new ValidationRuleArgs("FileName", "File Name"));
            //ValidationRules.AddRule(CommonRules.StringMaxLength,
            //    new CommonRules.MaxLengthRuleArgs("FileName", "File Name", 255));
            //ValidationRules.AddRule(CommonRules.NotNull,
            //    new ValidationRuleArgs("FileHash", "File Hash"));
            //ValidationRules.AddRule(CommonRules.NotNull,
            //    new ValidationRuleArgs("Content", "Content"));
        }
        #endregion

        #region Table Meta Data
        /// <summary>
        ///		The name of the underlying database table.
        /// </summary>
        [BrowsableAttribute(false), XmlIgnoreAttribute()]
        public override string TableName
        {
            get { return "UserCompany"; }
        }

        /// <summary>
        ///		The name of the underlying database table's columns.
        /// </summary>
        [BrowsableAttribute(false), XmlIgnoreAttribute()]
        public override string[] TableColumns
        {
            get
            {
                return new string[] { "UserCompanyId","SiteId", "CompanyId", "Active" };
            }
        }
        #endregion

        #region IEditableObject

        #region  CancelAddNew Event
        /// <summary>
        /// The delegate for the CancelAddNew event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void CancelAddNewEventHandler(object sender, EventArgs e);

        /// <summary>
        /// The CancelAddNew event.
        /// </summary>
        [field: NonSerialized]
        public event CancelAddNewEventHandler CancelAddNew;

        /// <summary>
        /// Called when [cancel add new].
        /// </summary>
        public void OnCancelAddNew()
        {
            if (!SuppressEntityEvents)
            {
                CancelAddNewEventHandler handler = CancelAddNew;
                if (handler != null)
                {
                    handler(this, EventArgs.Empty);
                }
            }
        }
        #endregion

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        void IEditableObject.BeginEdit()
        {
            //Console.WriteLine("Start BeginEdit");
            if (!inTxn)
            {
                this.backupData = this.entityData.Clone() as UserCompanyEntityData;
                inTxn = true;
                //Console.WriteLine("BeginEdit");
            }
            //Console.WriteLine("End BeginEdit");
        }

        /// <summary>
        /// Discards changes since the last <c>BeginEdit</c> call.
        /// </summary>
        void IEditableObject.CancelEdit()
        {
            //Console.WriteLine("Start CancelEdit");
            if (this.inTxn)
            {
                this.entityData = this.backupData;
                this.backupData = null;
                this.inTxn = false;

                if (this.bindingIsNew)
                //if (this.EntityState == EntityState.Added)
                {
                    if (this.parentCollection != null)
                        this.parentCollection.Remove((UserCompany)this);
                }
            }
            //Console.WriteLine("End CancelEdit");
        }

        /// <summary>
        /// Pushes changes since the last <c>BeginEdit</c> or <c>IBindingList.AddNew</c> call into the underlying object.
        /// </summary>
        void IEditableObject.EndEdit()
        {
            //Console.WriteLine("Start EndEdit" + this.custData.id + this.custData.lastName);
            if (this.inTxn)
            {
                this.backupData = null;
                if (this.IsDirty)
                {
                    if (this.bindingIsNew)
                    {
                        this.EntityState = EntityState.Added;
                        this.bindingIsNew = false;
                    }
                    else
                        if (this.EntityState == EntityState.Unchanged)
                            this.EntityState = EntityState.Changed;
                }

                this.bindingIsNew = false;
                this.inTxn = false;
            }
            //Console.WriteLine("End EndEdit");
        }

        /// <summary>
        /// Gets or sets the parent collection of this current entity, if available.
        /// </summary>
        /// <value>The parent collection.</value>
        [XmlIgnore]
        [Browsable(false)]
        public override object ParentCollection
        {
            get
            {
                return this.parentCollection;
            }
            set
            {
                this.parentCollection = value as TList<UserCompany>;
            }
        }

        /// <summary>
        /// Called when the entity is changed.
        /// </summary>
        private void OnEntityChanged()
        {
            if (!SuppressEntityEvents && !inTxn && this.parentCollection != null)
            {
                this.parentCollection.EntityChanged(this as UserCompany);
            }
        }


        #endregion

        #region ICloneable Members
        ///<summary>
        ///  Returns a Typed QuestionnaireMainAttachment Entity 
        ///</summary>
        protected virtual UserCompany Copy(IDictionary existingCopies)
        {
            if (existingCopies == null)
            {
                // This is the root of the tree to be copied!
                existingCopies = new Hashtable();
            }

            //shallow copy entity
            UserCompany copy = new UserCompany();
            existingCopies.Add(this, copy);
            copy.SuppressEntityEvents = true;
            copy.UserCompanyId = this.UserCompanyId;
            copy.UserId = this.UserId;
            copy.CompanyId = this.CompanyId;
            copy.Active = this.Active;


            if (this.UserIdSource != null && existingCopies.Contains(this.UserIdSource))
                copy.UserIdSource = existingCopies[this.UserIdSource] as Users;
            else
                copy.UserIdSource = MakeCopyOf(this.UserIdSource, existingCopies) as Users;
            if (this.CompanyIdSource != null && existingCopies.Contains(this.CompanyIdSource))
                copy.CompanyIdSource = existingCopies[this.CompanyIdSource] as Companies;
            else
                copy.CompanyIdSource = MakeCopyOf(this.CompanyIdSource, existingCopies) as Companies;

            copy.EntityState = this.EntityState;
            copy.SuppressEntityEvents = false;
            return copy;
        }



        ///<summary>
        ///  Returns a Typed QuestionnaireMainAttachment Entity 
        ///</summary>
        public virtual UserCompany Copy()
        {
            return this.Copy(null);
        }

        ///<summary>
        /// ICloneable.Clone() Member, returns the Shallow Copy of this entity.
        ///</summary>
        public object Clone()
        {
            return this.Copy(null);
        }

        ///<summary>
        /// ICloneableEx.Clone() Member, returns the Shallow Copy of this entity.
        ///</summary>
        public object Clone(IDictionary existingCopies)
        {
            return this.Copy(existingCopies);
        }

        ///<summary>
        /// Returns a deep copy of the child collection object passed in.
        ///</summary>
        public static object MakeCopyOf(object x)
        {
            if (x == null)
                return null;

            if (x is ICloneable)
            {
                // Return a deep copy of the object
                return ((ICloneable)x).Clone();
            }
            else
                throw new System.NotSupportedException("Object Does Not Implement the ICloneable Interface.");
        }

        ///<summary>
        /// Returns a deep copy of the child collection object passed in.
        ///</summary>
        public static object MakeCopyOf(object x, IDictionary existingCopies)
        {
            if (x == null)
                return null;

            if (x is ICloneableEx)
            {
                // Return a deep copy of the object
                return ((ICloneableEx)x).Clone(existingCopies);
            }
            else if (x is ICloneable)
            {
                // Return a deep copy of the object
                return ((ICloneable)x).Clone();
            }
            else
                throw new System.NotSupportedException("Object Does Not Implement the ICloneable or IClonableEx Interface.");
        }


        ///<summary>
        ///  Returns a Typed QuestionnaireMainAttachment Entity which is a deep copy of the current entity.
        ///</summary>
        public virtual UserCompany DeepCopy()
        {
            return EntityHelper.Clone<UserCompany>(this as UserCompany);
        }
        #endregion

        #region Methods

        ///<summary>
        /// Revert all changes and restore original values.
        ///</summary>
        public override void CancelChanges()
        {
            IEditableObject obj = (IEditableObject)this;
            obj.CancelEdit();

            this.entityData = null;
            if (this._originalData != null)
            {
                this.entityData = this._originalData.Clone() as UserCompanyEntityData;
            }
            else
            {
                //Since this had no _originalData, then just reset the entityData with a new one.  entityData cannot be null.
                this.entityData = new UserCompanyEntityData();
            }
        }

        /// <summary>
        /// Accepts the changes made to this object.
        /// </summary>
        /// <remarks>
        /// After calling this method, properties: IsDirty, IsNew are false. IsDeleted flag remains unchanged as it is handled by the parent List.
        /// </remarks>
        public override void AcceptChanges()
        {
            base.AcceptChanges();

            // we keep of the original version of the data
            this._originalData = null;
            this._originalData = this.entityData.Clone() as UserCompanyEntityData;
        }

        #region Comparision with original data

        /// <summary>
        /// Determines whether the property value has changed from the original data.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>
        /// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
        /// </returns>
        public bool IsPropertyChanged(UserCompanyColumn column)
        {
            switch (column)
            {
                case UserCompanyColumn.UserCompanyId:
                    return entityData.UserCompanyId != _originalData.UserCompanyId;
                case UserCompanyColumn.UserId:
                    return entityData.UserId != _originalData.UserId;
                case UserCompanyColumn.CompanyId:
                    return entityData.CompanyId != _originalData.CompanyId;
                case UserCompanyColumn.Active:
                    return entityData.Active != _originalData.Active;   


                default:
                    return false;
            }
        }

        /// <summary>
        /// Determines whether the property value has changed from the original data.
        /// </summary>
        /// <param name="columnName">The column name.</param>
        /// <returns>
        /// 	<c>true</c> if the property value has changed; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsPropertyChanged(string columnName)
        {
            return IsPropertyChanged(EntityHelper.GetEnumValue<UserCompanyColumn>(columnName));
        }

        /// <summary>
        /// Determines whether the data has changed from original.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if data has changed; otherwise, <c>false</c>.
        /// </returns>
        public bool HasDataChanged()
        {
            bool result = false;
            result = result || entityData.UserCompanyId != _originalData.UserCompanyId;
            result = result || entityData.UserId != _originalData.UserId;
            result = result || entityData.CompanyId != _originalData.CompanyId;
            result = result || entityData.Active != _originalData.Active;
            
            return result;
        }

        ///<summary>
        ///  Returns a UserCompany Entity with the original data.
        ///</summary>
        public UserCompany GetOriginalEntity()
        {
            if (_originalData != null)
                return CreateUserCompany(
                _originalData.UserCompanyId,
                _originalData.UserId,
                _originalData.CompanyId,
                _originalData.Active
                );

            return (UserCompany)this.Clone();
        }
        #endregion

        #region Value Semantics Instance Equality
        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="Object1">An object to compare to this instance.</param>
        ///<returns>true if Object1 is a <see cref="UserCompanyBase"/> and has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object Object1)
        {
            // Cast exception if Object1 is null or DbNull
            if (Object1 != null && Object1 != DBNull.Value && Object1 is UserCompanyBase)
                return ValueEquals(this, (UserCompanyBase)Object1);
            else
                return false;
        }

        /// <summary>
        /// Serves as a hash function for a particular type, suitable for use in hashing algorithms and data structures like a hash table.
        /// Provides a hash function that is appropriate for <see cref="QuestionnaireMainAttachmentBase"/> class 
        /// and that ensures a better distribution in the hash table
        /// </summary>
        /// <returns>number (hash code) that corresponds to the value of an object</returns>
        public override int GetHashCode()
        {
            return
                    this.UserCompanyId.GetHashCode() ^
                    this.UserId.GetHashCode() ^
                this.CompanyId.GetHashCode() ^ this.Active.GetHashCode();
        }

        ///<summary>
        /// Returns a value indicating whether this instance is equal to a specified object using value semantics.
        ///</summary>
        ///<param name="toObject">An object to compare to this instance.</param>
        ///<returns>true if toObject is a <see cref="QuestionnaireMainAttachmentBase"/> and has the same value as this instance; otherwise, false.</returns>
        public virtual bool Equals(UserCompanyBase toObject)
        {
            if (toObject == null)
                return false;
            return ValueEquals(this, toObject);
        }
        #endregion

        ///<summary>
        /// Determines whether the specified <see cref="QuestionnaireMainAttachmentBase"/> instances are considered equal using value semantics.
        ///</summary>
        ///<param name="Object1">The first <see cref="QuestionnaireMainAttachmentBase"/> to compare.</param>
        ///<param name="Object2">The second <see cref="QuestionnaireMainAttachmentBase"/> to compare. </param>
        ///<returns>true if Object1 is the same instance as Object2 or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public static bool ValueEquals(UserCompanyBase Object1, UserCompanyBase Object2)
        {
            // both are null
            if (Object1 == null && Object2 == null)
                return true;

            // one or the other is null, but not both
            if (Object1 == null ^ Object2 == null)
                return false;

            bool equal = true;
            if (Object1.UserCompanyId != Object2.UserCompanyId)
                equal = false;
            if (Object1.UserId != Object2.UserId)
                equal = false;
            if (Object1.CompanyId != Object2.CompanyId)
                equal = false;
            if (Object1.Active != Object2.Active)
                equal = false;
        
            return equal;
        }

        #endregion

        #region IComparable Members
        ///<summary>
        /// Compares this instance to a specified object and returns an indication of their relative values.
        ///<param name="obj">An object to compare to this instance, or a null reference (Nothing in Visual Basic).</param>
        ///</summary>
        ///<returns>A signed integer that indicates the relative order of this instance and obj.</returns>
        public virtual int CompareTo(object obj)
        {
            throw new NotImplementedException();
            //return this. GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]) .CompareTo(((QuestionnaireMainAttachmentBase)obj).GetPropertyName(SourceTable.PrimaryKey.MemberColumns[0]));
        }

        /*
        // static method to get a Comparer object
        public static QuestionnaireMainAttachmentComparer GetComparer()
        {
            return new QuestionnaireMainAttachmentComparer();
        }
        */

        // Comparer delegates back to QuestionnaireMainAttachment
        // Employee uses the integer's default
        // CompareTo method
        /*
        public int CompareTo(Item rhs)
        {
            return this.Id.CompareTo(rhs.Id);
        }
        */

        /*
                // Special implementation to be called by custom comparer
                public int CompareTo(QuestionnaireMainAttachment rhs, QuestionnaireMainAttachmentColumn which)
                {
                    switch (which)
                    {
            	
            	
                        case QuestionnaireMainAttachmentColumn.AttachmentId:
                            return this.AttachmentId.CompareTo(rhs.AttachmentId);
            		
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.QuestionnaireId:
                            return this.QuestionnaireId.CompareTo(rhs.QuestionnaireId);
            		
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.AnswerId:
                            return this.AnswerId.CompareTo(rhs.AnswerId);
            		
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.FileName:
                            return this.FileName.CompareTo(rhs.FileName);
            		
            		                 
            	
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.ContentLength:
                            return this.ContentLength.CompareTo(rhs.ContentLength);
            		
            		                 
            	
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.ModifiedByUserId:
                            return this.ModifiedByUserId.CompareTo(rhs.ModifiedByUserId);
            		
            		                 
            	
            	
                        case QuestionnaireMainAttachmentColumn.ModifiedDate:
                            return this.ModifiedDate.CompareTo(rhs.ModifiedDate);
            		
            		                 
                    }
                    return 0;
                }
                */

        #endregion

        #region IComponent Members

        private ISite _site = null;

        /// <summary>
        /// Gets or Sets the site where this data is located.
        /// </summary>
        [XmlIgnore]
        [SoapIgnore]
        [Browsable(false)]
        public ISite Site
        {
            get { return this._site; }
            set { this._site = value; }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Notify those that care when we dispose.
        /// </summary>
        [field: NonSerialized]
        public event System.EventHandler Disposed;

        /// <summary>
        /// Clean up. Nothing here though.
        /// </summary>
        public virtual void Dispose()
        {
            this.parentCollection = null;
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Clean up.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                EventHandler handler = Disposed;
                if (handler != null)
                    handler(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IEntityKey<UserCompanyKey> Members

        // member variable for the EntityId property
        private UserCompanyKey _entityId;

        /// <summary>
        /// Gets or sets the EntityId property.
        /// </summary>
        [XmlIgnore]
        public virtual UserCompanyKey EntityId
        {
            get
            {
                if (_entityId == null)
                {
                    _entityId = new UserCompanyKey(this);
                }

                return _entityId;
            }
            set
            {
                if (value != null)
                {
                    value.Entity = this;
                }

                _entityId = value;
            }
        }

        #endregion

        #region EntityState
        /// <summary>
        ///		Indicates state of object
        /// </summary>
        /// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
        [BrowsableAttribute(false), XmlIgnoreAttribute()]
        public override EntityState EntityState
        {
            get { return entityData.EntityState; }
            set { entityData.EntityState = value; }
        }
        #endregion

        #region EntityTrackingKey
        ///<summary>
        /// Provides the tracking key for the <see cref="EntityLocator"/>
        ///</summary>
        [XmlIgnore]
        public override string EntityTrackingKey
        {
            get
            {
                if (entityTrackingKey == null)
                    entityTrackingKey = new System.Text.StringBuilder("UserCompany")
                    .Append("|").Append(this.UserCompanyId.ToString()).ToString();
                return entityTrackingKey;
            }
            set
            {
                if (value != null)
                    entityTrackingKey = value;
            }
        }
        #endregion

        #region ToString Method

        ///<summary>
        /// Returns a String that represents the current object.
        ///</summary>
        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "{5}{4}- UserCompanyId: {0}{3}- UserId: {1}{3}- CompanyId: {2}{4}- Active: {3}{4}{5}",
                this.UserCompanyId,
                this.UserId,
                this.CompanyId,  
                this.Active,
                System.Environment.NewLine,
                this.GetType(),
                this.Error.Length == 0 ? string.Empty : string.Format("- Error: {0}\n", this.Error));
        }

        #endregion ToString Method

        #region Inner data class

        /// <summary>
        ///		The data structure representation of the 'UserCompany' table.
        /// </summary>
        /// <remarks>
        /// 	This struct is generated by a tool and should never be modified.
        /// </remarks>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Serializable]
        internal protected class UserCompanyEntityData : ICloneable, ICloneableEx
        {
            #region Variable Declarations
            private EntityState currentEntityState = EntityState.Added;

            #region Primary key(s)
            /// <summary>			
            /// UserCompanyId : 
            /// </summary>
            /// <remarks>Member of the primary key of the underlying table "UserCompany"</remarks>
            public System.Int32 UserCompanyId;

            #endregion

            #region Non Primary key(s)


            /// <summary>
            /// SiteId : 
            /// </summary>
            public System.Int32 UserId = (int)0;

            /// <summary>
            /// CompanyId : 
            /// </summary>
            public System.Int32 CompanyId = (int)0;

            /// <summary>
            /// Active : 
            /// </summary>
            public System.Boolean Active = (bool)false;


            #endregion

            #region Source Foreign Key Property

            private Users _userIdSource = null;

            /// <summary>
            /// Gets or sets the source <see cref="Users"/>.
            /// </summary>
            /// <value>The source Users for ModifiedByUserId.</value>
            [XmlIgnore()]
            [Browsable(false)]
            public virtual Users UserIdSource
            {
                get { return this._userIdSource; }
                set { this._userIdSource = value; }
            }
            private Companies _companiesIdSource = null;

            /// <summary>
            /// Gets or sets the source <see cref="QuestionnaireMainAnswer"/>.
            /// </summary>
            /// <value>The source QuestionnaireMainAnswer for AnswerId.</value>
            [XmlIgnore()]
            [Browsable(false)]
            public virtual Companies CompanyIdSource
            {
                get { return this._companiesIdSource; }
                set { this._companiesIdSource = value; }
            }
            #endregion

            #endregion Variable Declarations

            #region Data Properties

            #endregion Data Properties
            #region Clone Method

            /// <summary>
            /// Creates a new object that is a copy of the current instance.
            /// </summary>
            /// <returns>A new object that is a copy of this instance.</returns>
            public Object Clone()
            {
                UserCompanyEntityData _tmp = new UserCompanyEntityData();

                _tmp.UserCompanyId = this.UserCompanyId;
                _tmp.UserId = this.UserId;
                _tmp.CompanyId = this.CompanyId;
                _tmp.Active = this.Active;
                
                

                #region Source Parent Composite Entities
                if (this.UserIdSource != null)
                    _tmp.UserIdSource = MakeCopyOf(this.UserIdSource) as Users;
                if (this.CompanyIdSource != null)
                    _tmp.CompanyIdSource = MakeCopyOf(this.CompanyIdSource) as Companies;
                #endregion

                #region Child Collections
                #endregion Child Collections

                //EntityState
                _tmp.EntityState = this.EntityState;

                return _tmp;
            }

            /// <summary>
            /// Creates a new object that is a copy of the current instance.
            /// </summary>
            /// <returns>A new object that is a copy of this instance.</returns>
            public object Clone(IDictionary existingCopies)
            {
                if (existingCopies == null)
                    existingCopies = new Hashtable();

                UserCompanyEntityData _tmp = new UserCompanyEntityData();

                _tmp.UserCompanyId = this.UserCompanyId;
                _tmp.UserId = this.UserId;
                _tmp.CompanyId = this.CompanyId;
                _tmp.Active = this.Active;



                #region Source Parent Composite Entities
                if (this.UserIdSource != null)
                    _tmp.UserIdSource = MakeCopyOf(this.UserIdSource) as Users;
                if (this.CompanyIdSource != null)
                    _tmp.CompanyIdSource = MakeCopyOf(this.CompanyIdSource) as Companies;
                #endregion

                #region Child Collections
                #endregion Child Collections

                //EntityState
                _tmp.EntityState = this.EntityState;

                return _tmp;
            }

            #endregion Clone Method

            /// <summary>
            ///		Indicates state of object
            /// </summary>
            /// <remarks>0=Unchanged, 1=Added, 2=Changed</remarks>
            [BrowsableAttribute(false), XmlIgnoreAttribute()]
            public EntityState EntityState
            {
                get { return currentEntityState; }
                set { currentEntityState = value; }
            }

        }//End struct

        #endregion



        #region Events trigger
        /// <summary>
        /// Raises the <see cref="ColumnChanging" /> event.
        /// </summary>
        /// <param name="column">The <see cref="UserCompanyColumn"/> which has raised the event.</param>
        public virtual void OnColumnChanging(UserCompanyColumn column)
        {
            OnColumnChanging(column, null);
            return;
        }

        /// <summary>
        /// Raises the <see cref="ColumnChanged" /> event.
        /// </summary>
        /// <param name="column">The <see cref="UserCompanyColumn"/> which has raised the event.</param>
        public virtual void OnColumnChanged(UserCompanyColumn column)
        {
            OnColumnChanged(column, null);
            return;
        }


        /// <summary>
        /// Raises the <see cref="ColumnChanging" /> event.
        /// </summary>
        /// <param name="column">The <see cref="UserCompanyColumn"/> which has raised the event.</param>
        /// <param name="value">The changed value.</param>
        public virtual void OnColumnChanging(UserCompanyColumn column, object value)
        {
            if (IsEntityTracked && EntityState != EntityState.Added && !EntityManager.TrackChangedEntities)
                EntityManager.StopTracking(entityTrackingKey);

            if (!SuppressEntityEvents)
            {
                UserCompanyEventHandler handler = ColumnChanging;
                if (handler != null)
                {
                    handler(this, new UserCompanyEventArgs(column, value));
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="ColumnChanged" /> event.
        /// </summary>
        /// <param name="column">The <see cref="QuestionnaireMainAttachmentColumn"/> which has raised the event.</param>
        /// <param name="value">The changed value.</param>
        public virtual void OnColumnChanged(UserCompanyColumn column, object value)
        {
            if (!SuppressEntityEvents)
            {
                UserCompanyEventHandler handler = ColumnChanged;
                if (handler != null)
                {
                    handler(this, new UserCompanyEventArgs(column, value));
                }

                // warn the parent list that i have changed
                OnEntityChanged();
            }
        }
        #endregion

    } // End Class


    #region UserCompanyEventArgs class
    /// <summary>
    /// Provides data for the ColumnChanging and ColumnChanged events.
    /// </summary>
    /// <remarks>
    /// The ColumnChanging and ColumnChanged events occur when a change is made to the value 
    /// of a property of a <see cref="UserCompany"/> object.
    /// </remarks>
    public class UserCompanyEventArgs : System.EventArgs
    {
        private UserCompanyColumn column;
        private object value;

        ///<summary>
        /// Initalizes a new Instance of the QuestionnaireMainAttachmentEventArgs class.
        ///</summary>
        public UserCompanyEventArgs(UserCompanyColumn column)
        {
            this.column = column;
        }

        ///<summary>
        /// Initalizes a new Instance of the QuestionnaireMainAttachmentEventArgs class.
        ///</summary>
        public UserCompanyEventArgs(UserCompanyColumn column, object value)
        {
            this.column = column;
            this.value = value;
        }

        ///<summary>
        /// The UserCompanyColumn that was modified, which has raised the event.
        ///</summary>
        ///<value cref="QuestionnaireMainAttachmentColumn" />
        public UserCompanyColumn Column { get { return this.column; } }

        /// <summary>
        /// Gets the current value of the column.
        /// </summary>
        /// <value>The current value of the column.</value>
        public object Value { get { return this.value; } }

    }
    #endregion

    ///<summary>
    /// Define a delegate for all QuestionnaireMainAttachment related events.
    ///</summary>
    public delegate void UserCompanyEventHandler(object sender, UserCompanyEventArgs e);

    #region ContractReviewsRegisterComparer

    /// <summary>
    ///	Strongly Typed IComparer
    /// </summary>
    public class UserCompanyComparer : System.Collections.Generic.IComparer<UserCompany>
    {
        UserCompanyColumn whichComparison;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:ContractReviewsRegisterComparer"/> class.
        /// </summary>
        public UserCompanyComparer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:ContractReviewsRegisterComparer"/> class.
        /// </summary>
        /// <param name="column">The column to sort on.</param>
        public UserCompanyComparer(UserCompanyColumn column)
        {
            this.whichComparison = column;
        }

        /// <summary>
        /// Determines whether the specified <see cref="UserCompany"/> instances are considered equal.
        /// </summary>
        /// <param name="a">The first <see cref="UserCompany"/> to compare.</param>
        /// <param name="b">The second <c>QuestionnaireMainAttachment</c> to compare.</param>
        /// <returns>true if objA is the same instance as objB or if both are null references or if objA.Equals(objB) returns true; otherwise, false.</returns>
        public bool Equals(UserCompany a, UserCompany b)
        {
            return this.Compare(a, b) == 0;
        }

        /// <summary>
        /// Gets the hash code of the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        public int GetHashCode(UserCompany entity)
        {
            return entity.GetHashCode();
        }

        /// <summary>
        /// Performs a case-insensitive comparison of two objects of the same type and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        /// <returns></returns>
        public int Compare(UserCompany a, UserCompany b)
        {
            EntityPropertyComparer entityPropertyComparer = new EntityPropertyComparer(this.whichComparison.ToString());
            return entityPropertyComparer.Compare(a, b);
        }

        /// <summary>
        /// Gets or sets the column that will be used for comparison.
        /// </summary>
        /// <value>The comparison column.</value>
        public UserCompanyColumn WhichComparison
        {
            get { return this.whichComparison; }
            set { this.whichComparison = value; }
        }
    }

    #endregion

    #region SafetyPlansSEResponsesAttachmentKey Class

    /// <summary>
    /// Wraps the unique identifier values for the <see cref="UserCompany"/> object.
    /// </summary>
    [Serializable]
    [CLSCompliant(true)]
    public class UserCompanyKey : EntityKeyBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the UserCompanyKey class.
        /// </summary>
        public UserCompanyKey()
        {
        }

        /// <summary>
        /// Initializes a new instance of the QuestionnaireMainAttachmentKey class.
        /// </summary>
        public UserCompanyKey(UserCompanyBase entity)
        {
            this.Entity = entity;

            #region Init Properties

            if (entity != null)
            {
                this.UserCompanyId = entity.UserCompanyId;
            }

            #endregion
        }

        /// <summary>
        /// Initializes a new instance of the UserCompanyKey class.
        /// </summary>
        public UserCompanyKey(System.Int32 _contractId)
        {
            #region Init Properties

            this.UserCompanyId = _contractId;

            #endregion
        }

        #endregion Constructors

        #region Properties

        // member variable for the Entity property
        private UserCompanyBase _entity;

        /// <summary>
        /// Gets or sets the Entity property.
        /// </summary>
        public UserCompanyBase Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        // member variable for the AttachmentId property
        private System.Int32 _contractId;

        /// <summary>
        /// Gets or sets the AttachmentId property.
        /// </summary>
        public System.Int32 UserCompanyId
        {
            get { return _contractId; }
            set
            {
                if (this.Entity != null)
                    this.Entity.UserCompanyId = value;

                _contractId = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reads values from the supplied <see cref="IDictionary"/> object into
        /// properties of the current object.
        /// </summary>
        /// <param name="values">An <see cref="IDictionary"/> instance that contains
        /// the key/value pairs to be used as property values.</param>
        public override void Load(IDictionary values)
        {
            #region Init Properties

            if (values != null)
            {
                UserCompanyId = (values["UserCompanyId"] != null) ? (System.Int32)EntityUtil.ChangeType(values["UserCompanyId"], typeof(System.Int32)) : (int)0;
            }

            #endregion
        }

        /// <summary>
        /// Creates a new <see cref="IDictionary"/> object and populates it
        /// with the property values of the current object.
        /// </summary>
        /// <returns>A collection of name/value pairs.</returns>
        public override IDictionary ToDictionary()
        {
            IDictionary values = new Hashtable();

            #region Init Dictionary

            values.Add("UserCompanyId", UserCompanyId);

            #endregion Init Dictionary

            return values;
        }

        ///<summary>
        /// Returns a String that represents the current object.
        ///</summary>
        public override string ToString()
        {
            return String.Format("UserCompanyId: {0}{1}",
                                UserCompanyId,
                                System.Environment.NewLine);
        }

        #endregion Methods
    }

    #endregion

    #region UserCompanyColumn Enum

    /// <summary>
    /// Enumerate the UserCompany columns.
    /// </summary>
    [Serializable]
    public enum UserCompanyColumn : int
    {
        /// <summary>
        /// UserCompanyId : 
        /// </summary>
        [EnumTextValue("UserCompanyId")]
        [ColumnEnum("UserCompanyId", typeof(System.Int32), System.Data.DbType.Int32, true, true, false)]
        UserCompanyId = 1,

        /// <summary>
        /// UserId : 
        /// </summary>
        [EnumTextValue("UserId")]
        [ColumnEnum("UserId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        UserId = 2,

        /// <summary>
        /// QuestionId : 
        /// </summary>
        [EnumTextValue("CompanyId")]
        [ColumnEnum("CompanyId", typeof(System.Int32), System.Data.DbType.Int32, false, false, false)]
        CompanyId = 3,

        /// <summary>
        /// Active : 
        /// </summary>
        [EnumTextValue("Active")]
        [ColumnEnum("Active", typeof(System.Boolean), System.Data.DbType.Boolean, false, false, false)]
        Active = 4

        

    }//End enum

    #endregion SafetyPlansSEResponsesAttachmentColumn Enum

} // end namespace
