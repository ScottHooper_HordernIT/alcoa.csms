﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultListSqExemptionProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(FileVaultListSqExemptionDataSourceDesigner))]
	public class FileVaultListSqExemptionDataSource : ReadOnlyDataSource<FileVaultListSqExemption>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionDataSource class.
		/// </summary>
		public FileVaultListSqExemptionDataSource() : base(new FileVaultListSqExemptionService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultListSqExemptionDataSourceView used by the FileVaultListSqExemptionDataSource.
		/// </summary>
		protected FileVaultListSqExemptionDataSourceView FileVaultListSqExemptionView
		{
			get { return ( View as FileVaultListSqExemptionDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultListSqExemptionDataSourceView class that is to be
		/// used by the FileVaultListSqExemptionDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultListSqExemptionDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultListSqExemption, Object> GetNewDataSourceView()
		{
			return new FileVaultListSqExemptionDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultListSqExemptionDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultListSqExemptionDataSourceView : ReadOnlyDataSourceView<FileVaultListSqExemption>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultListSqExemptionDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultListSqExemptionDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultListSqExemptionDataSourceView(FileVaultListSqExemptionDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultListSqExemptionDataSource FileVaultListSqExemptionOwner
		{
			get { return Owner as FileVaultListSqExemptionDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultListSqExemptionService FileVaultListSqExemptionProvider
		{
			get { return Provider as FileVaultListSqExemptionService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region FileVaultListSqExemptionDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultListSqExemptionDataSource class.
	/// </summary>
	public class FileVaultListSqExemptionDataSourceDesigner : ReadOnlyDataSourceDesigner<FileVaultListSqExemption>
	{
	}

	#endregion FileVaultListSqExemptionDataSourceDesigner

	#region FileVaultListSqExemptionFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListSqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListSqExemptionFilter : SqlFilter<FileVaultListSqExemptionColumn>
	{
	}

	#endregion FileVaultListSqExemptionFilter

	#region FileVaultListSqExemptionExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultListSqExemption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultListSqExemptionExpressionBuilder : SqlExpressionBuilder<FileVaultListSqExemptionColumn>
	{
	}
	
	#endregion FileVaultListSqExemptionExpressionBuilder		
}

