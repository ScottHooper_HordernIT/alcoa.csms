﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskEmailRecipientProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskEmailRecipientDataSourceDesigner))]
	public class AdminTaskEmailRecipientDataSource : ProviderDataSource<AdminTaskEmailRecipient, AdminTaskEmailRecipientKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientDataSource class.
		/// </summary>
		public AdminTaskEmailRecipientDataSource() : base(new AdminTaskEmailRecipientService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskEmailRecipientDataSourceView used by the AdminTaskEmailRecipientDataSource.
		/// </summary>
		protected AdminTaskEmailRecipientDataSourceView AdminTaskEmailRecipientView
		{
			get { return ( View as AdminTaskEmailRecipientDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskEmailRecipientDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskEmailRecipientSelectMethod SelectMethod
		{
			get
			{
				AdminTaskEmailRecipientSelectMethod selectMethod = AdminTaskEmailRecipientSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskEmailRecipientSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskEmailRecipientDataSourceView class that is to be
		/// used by the AdminTaskEmailRecipientDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskEmailRecipientDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskEmailRecipient, AdminTaskEmailRecipientKey> GetNewDataSourceView()
		{
			return new AdminTaskEmailRecipientDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskEmailRecipientDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskEmailRecipientDataSourceView : ProviderDataSourceView<AdminTaskEmailRecipient, AdminTaskEmailRecipientKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskEmailRecipientDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskEmailRecipientDataSourceView(AdminTaskEmailRecipientDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskEmailRecipientDataSource AdminTaskEmailRecipientOwner
		{
			get { return Owner as AdminTaskEmailRecipientDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskEmailRecipientSelectMethod SelectMethod
		{
			get { return AdminTaskEmailRecipientOwner.SelectMethod; }
			set { AdminTaskEmailRecipientOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskEmailRecipientService AdminTaskEmailRecipientProvider
		{
			get { return Provider as AdminTaskEmailRecipientService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskEmailRecipient> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskEmailRecipient> results = null;
			AdminTaskEmailRecipient item;
			count = 0;
			
			System.Int32 _adminTaskEmailRecipientId;
			System.String _recipientName;

			switch ( SelectMethod )
			{
				case AdminTaskEmailRecipientSelectMethod.Get:
					AdminTaskEmailRecipientKey entityKey  = new AdminTaskEmailRecipientKey();
					entityKey.Load(values);
					item = AdminTaskEmailRecipientProvider.Get(entityKey);
					results = new TList<AdminTaskEmailRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskEmailRecipientSelectMethod.GetAll:
                    results = AdminTaskEmailRecipientProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskEmailRecipientSelectMethod.GetPaged:
					results = AdminTaskEmailRecipientProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskEmailRecipientSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskEmailRecipientProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskEmailRecipientProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskEmailRecipientSelectMethod.GetByAdminTaskEmailRecipientId:
					_adminTaskEmailRecipientId = ( values["AdminTaskEmailRecipientId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskEmailRecipientId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskEmailRecipientProvider.GetByAdminTaskEmailRecipientId(_adminTaskEmailRecipientId);
					results = new TList<AdminTaskEmailRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskEmailRecipientSelectMethod.GetByRecipientName:
					_recipientName = ( values["RecipientName"] != null ) ? (System.String) EntityUtil.ChangeType(values["RecipientName"], typeof(System.String)) : string.Empty;
					item = AdminTaskEmailRecipientProvider.GetByRecipientName(_recipientName);
					results = new TList<AdminTaskEmailRecipient>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskEmailRecipientSelectMethod.Get || SelectMethod == AdminTaskEmailRecipientSelectMethod.GetByAdminTaskEmailRecipientId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskEmailRecipient entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskEmailRecipientProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskEmailRecipient> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskEmailRecipientProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskEmailRecipientDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskEmailRecipientDataSource class.
	/// </summary>
	public class AdminTaskEmailRecipientDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskEmailRecipient, AdminTaskEmailRecipientKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientDataSourceDesigner class.
		/// </summary>
		public AdminTaskEmailRecipientDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailRecipientSelectMethod SelectMethod
		{
			get { return ((AdminTaskEmailRecipientDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskEmailRecipientDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskEmailRecipientDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskEmailRecipientDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskEmailRecipientDataSourceActionList : DesignerActionList
	{
		private AdminTaskEmailRecipientDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskEmailRecipientDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskEmailRecipientDataSourceActionList(AdminTaskEmailRecipientDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskEmailRecipientSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskEmailRecipientDataSourceActionList
	
	#endregion AdminTaskEmailRecipientDataSourceDesigner
	
	#region AdminTaskEmailRecipientSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskEmailRecipientDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskEmailRecipientSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskEmailRecipientId method.
		/// </summary>
		GetByAdminTaskEmailRecipientId,
		/// <summary>
		/// Represents the GetByRecipientName method.
		/// </summary>
		GetByRecipientName
	}
	
	#endregion AdminTaskEmailRecipientSelectMethod

	#region AdminTaskEmailRecipientFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientFilter : SqlFilter<AdminTaskEmailRecipientColumn>
	{
	}
	
	#endregion AdminTaskEmailRecipientFilter

	#region AdminTaskEmailRecipientExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientExpressionBuilder : SqlExpressionBuilder<AdminTaskEmailRecipientColumn>
	{
	}
	
	#endregion AdminTaskEmailRecipientExpressionBuilder	

	#region AdminTaskEmailRecipientProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskEmailRecipientChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskEmailRecipient"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskEmailRecipientProperty : ChildEntityProperty<AdminTaskEmailRecipientChildEntityTypes>
	{
	}
	
	#endregion AdminTaskEmailRecipientProperty
}

