﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/popup.master" AutoEventWireup="true" CodeBehind="EditKpiHelpFile.aspx.cs" Inherits="ALCOA.CSMS.Website.PopUps.EditKpiHelpFile" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxUploadControl" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script language="javascript" type="text/javascript">
    function closePopup() { window.close(); }

    window.onunload = function () {
        if (window.opener && !window.opener.closed) {
            window.opener.popUpClosed();
        }
    };
    </script>
<dx:ASPxLabel ID="lblFileUploadMsg" runat="server" ForeColor="Red">
    </dx:ASPxLabel>
    <br />
    <br />
<dx:ASPxUploadControl ID="ucNewFile" runat="server" ShowProgressPanel="True" 
        CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
            <ValidationSettings  
                AllowedFileExtensions=".txt, .pdf, .xls, .xlsx, .doc, .docx, .ppt, .pptx, .zip, .jpg, .jpeg, .png"
                GeneralErrorText="File Upload Failed."
                MaxFileSize="26252928" 
                MaxFileSizeErrorText="File size exceeds the maximum allowed size of 25Mb (maybe consider ZIPing the file?)"
                NotAllowedFileExtensionErrorText="File NOT uploaded. Please upload only files of the following file type. All Other file types will not be uploaded: Text (TXT), Excel (XLS, XLSX), Word Document (DOC, DOCX), PowerPoint (PPT, PPTX), Adobe PDF (PDF), ZIP Compressed File (ZIP), Image (JPG/PNG)">
            </ValidationSettings>
        </dx:ASPxUploadControl>
        <dx:ASPxLabel ID="lblCurrentFile" runat="server" ForeColor="Red" Text="Uploading another file will overwrite the existing one" Visible="false">
        </dx:ASPxLabel>
        <dx:ASPxHyperLink ID="hlCurrentFile" runat="server" Text="-" Visible="false">
        </dx:ASPxHyperLink>
       <br />
       <br />
    
   
    
    <table cellpadding="0" cellspacing="0">
       
       <dx:ASPxComboBox ID="cbKpiHelpCaption" runat="server" CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
                            CssPostfix="Office2003Blue" IncrementalFilteringMode="StartsWith"
                            ValueType="System.Int32" Width="250px" Visible="false"
                            SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
                            <Items>
                                <dx:ListEditItem Text="Default" Value="0" Selected="true" />
                                <dx:ListEditItem Text="JSA Field Audit Verifications" Value="1" />
                                <dx:ListEditItem Text="Workplace Safety & Compliance" Value="2" />
                                <dx:ListEditItem Text="Management Health Safety work contacts" Value="3" />
                                <dx:ListEditItem Text="Behavioural Observations" Value="4" />
                                <dx:ListEditItem Text="Tool box meetings per month" Value="5" />
                                <dx:ListEditItem Text="Alcoa weekly contractors meeting" Value="6" />
                                <dx:ListEditItem Text="Alcoa monthly contractors meeting" Value="7" />
                                <dx:ListEditItem Text="Fatality Prevention discussion and presentation" Value="8" />
                            </Items>
                            <LoadingPanelImage Url="~/App_Themes/Office2003Blue/Web/Loading.gif">
                            </LoadingPanelImage>
                            <ButtonStyle Width="13px">
                            </ButtonStyle>
                        </dx:ASPxComboBox>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    
<dx:ASPxButton ID="btnUpload" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" onclick="btnUpload_Click" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Add / Upload">
</dx:ASPxButton>
<dx:ASPxButton ID="btnDelete" runat="server" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" onclick="btnDelete_Click" 
    SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css" 
        Text="Delete" Visible="false">
</dx:ASPxButton>
    </asp:Content>
