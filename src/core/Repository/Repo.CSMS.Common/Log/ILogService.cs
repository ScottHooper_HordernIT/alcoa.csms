﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace Repo.CSMS.Common.Log
{
    public interface ILogService
    {
        ILog Logger();

        void LogError(string message);
        void LogError(Exception ex);
        void LogError(string message, string user);
        void LogError(string message, Exception ex);
        void LogError(string message, Exception ex, string user);

        void LogWarning(string message);
        void LogWarning(Exception ex);
        void LogWarning(string message, string user);
        void LogWarning(string message, Exception ex);
        void LogWarning(string message, Exception ex, string user);

        void LogInfo(string message);
        void LogInfo(Exception ex);
        void LogInfo(string message, string user);
        void LogInfo(string message, Exception ex);
        void LogInfo(string message, Exception ex, string user);

        void LogDebug(string message);
        void LogDebug(Exception ex);
        void LogDebug(string message, string user);
        void LogDebug(string message, Exception ex);
        void LogDebug(string message, Exception ex, string user);
    }
}
