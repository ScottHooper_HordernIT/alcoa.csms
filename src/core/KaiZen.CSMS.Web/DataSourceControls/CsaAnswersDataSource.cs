﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.CsaAnswersProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(CsaAnswersDataSourceDesigner))]
	public class CsaAnswersDataSource : ProviderDataSource<CsaAnswers, CsaAnswersKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersDataSource class.
		/// </summary>
		public CsaAnswersDataSource() : base(new CsaAnswersService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the CsaAnswersDataSourceView used by the CsaAnswersDataSource.
		/// </summary>
		protected CsaAnswersDataSourceView CsaAnswersView
		{
			get { return ( View as CsaAnswersDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the CsaAnswersDataSource control invokes to retrieve data.
		/// </summary>
		public CsaAnswersSelectMethod SelectMethod
		{
			get
			{
				CsaAnswersSelectMethod selectMethod = CsaAnswersSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (CsaAnswersSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the CsaAnswersDataSourceView class that is to be
		/// used by the CsaAnswersDataSource.
		/// </summary>
		/// <returns>An instance of the CsaAnswersDataSourceView class.</returns>
		protected override BaseDataSourceView<CsaAnswers, CsaAnswersKey> GetNewDataSourceView()
		{
			return new CsaAnswersDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the CsaAnswersDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class CsaAnswersDataSourceView : ProviderDataSourceView<CsaAnswers, CsaAnswersKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CsaAnswersDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the CsaAnswersDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public CsaAnswersDataSourceView(CsaAnswersDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal CsaAnswersDataSource CsaAnswersOwner
		{
			get { return Owner as CsaAnswersDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal CsaAnswersSelectMethod SelectMethod
		{
			get { return CsaAnswersOwner.SelectMethod; }
			set { CsaAnswersOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal CsaAnswersService CsaAnswersProvider
		{
			get { return Provider as CsaAnswersService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<CsaAnswers> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<CsaAnswers> results = null;
			CsaAnswers item;
			count = 0;
			
			System.Int32 _csaAnswerId;
			System.Int32 _csaId;
			System.String _questionNo;

			switch ( SelectMethod )
			{
				case CsaAnswersSelectMethod.Get:
					CsaAnswersKey entityKey  = new CsaAnswersKey();
					entityKey.Load(values);
					item = CsaAnswersProvider.Get(entityKey);
					results = new TList<CsaAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case CsaAnswersSelectMethod.GetAll:
                    results = CsaAnswersProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case CsaAnswersSelectMethod.GetPaged:
					results = CsaAnswersProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case CsaAnswersSelectMethod.Find:
					if ( FilterParameters != null )
						results = CsaAnswersProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = CsaAnswersProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case CsaAnswersSelectMethod.GetByCsaAnswerId:
					_csaAnswerId = ( values["CsaAnswerId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsaAnswerId"], typeof(System.Int32)) : (int)0;
					item = CsaAnswersProvider.GetByCsaAnswerId(_csaAnswerId);
					results = new TList<CsaAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case CsaAnswersSelectMethod.GetByCsaIdQuestionNo:
					_csaId = ( values["CsaId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsaId"], typeof(System.Int32)) : (int)0;
					_questionNo = ( values["QuestionNo"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionNo"], typeof(System.String)) : string.Empty;
					item = CsaAnswersProvider.GetByCsaIdQuestionNo(_csaId, _questionNo);
					results = new TList<CsaAnswers>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case CsaAnswersSelectMethod.GetByCsaId:
					_csaId = ( values["CsaId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["CsaId"], typeof(System.Int32)) : (int)0;
					results = CsaAnswersProvider.GetByCsaId(_csaId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == CsaAnswersSelectMethod.Get || SelectMethod == CsaAnswersSelectMethod.GetByCsaAnswerId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				CsaAnswers entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					CsaAnswersProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<CsaAnswers> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			CsaAnswersProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region CsaAnswersDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the CsaAnswersDataSource class.
	/// </summary>
	public class CsaAnswersDataSourceDesigner : ProviderDataSourceDesigner<CsaAnswers, CsaAnswersKey>
	{
		/// <summary>
		/// Initializes a new instance of the CsaAnswersDataSourceDesigner class.
		/// </summary>
		public CsaAnswersDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsaAnswersSelectMethod SelectMethod
		{
			get { return ((CsaAnswersDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new CsaAnswersDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region CsaAnswersDataSourceActionList

	/// <summary>
	/// Supports the CsaAnswersDataSourceDesigner class.
	/// </summary>
	internal class CsaAnswersDataSourceActionList : DesignerActionList
	{
		private CsaAnswersDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the CsaAnswersDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public CsaAnswersDataSourceActionList(CsaAnswersDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public CsaAnswersSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion CsaAnswersDataSourceActionList
	
	#endregion CsaAnswersDataSourceDesigner
	
	#region CsaAnswersSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the CsaAnswersDataSource.SelectMethod property.
	/// </summary>
	public enum CsaAnswersSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByCsaAnswerId method.
		/// </summary>
		GetByCsaAnswerId,
		/// <summary>
		/// Represents the GetByCsaIdQuestionNo method.
		/// </summary>
		GetByCsaIdQuestionNo,
		/// <summary>
		/// Represents the GetByCsaId method.
		/// </summary>
		GetByCsaId
	}
	
	#endregion CsaAnswersSelectMethod

	#region CsaAnswersFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersFilter : SqlFilter<CsaAnswersColumn>
	{
	}
	
	#endregion CsaAnswersFilter

	#region CsaAnswersExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersExpressionBuilder : SqlExpressionBuilder<CsaAnswersColumn>
	{
	}
	
	#endregion CsaAnswersExpressionBuilder	

	#region CsaAnswersProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;CsaAnswersChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="CsaAnswers"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CsaAnswersProperty : ChildEntityProperty<CsaAnswersChildEntityTypes>
	{
	}
	
	#endregion CsaAnswersProperty
}

