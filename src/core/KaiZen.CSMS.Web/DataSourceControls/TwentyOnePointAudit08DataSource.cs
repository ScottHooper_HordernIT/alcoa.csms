﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.TwentyOnePointAudit08Provider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(TwentyOnePointAudit08DataSourceDesigner))]
	public class TwentyOnePointAudit08DataSource : ProviderDataSource<TwentyOnePointAudit08, TwentyOnePointAudit08Key>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08DataSource class.
		/// </summary>
		public TwentyOnePointAudit08DataSource() : base(new TwentyOnePointAudit08Service())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the TwentyOnePointAudit08DataSourceView used by the TwentyOnePointAudit08DataSource.
		/// </summary>
		protected TwentyOnePointAudit08DataSourceView TwentyOnePointAudit08View
		{
			get { return ( View as TwentyOnePointAudit08DataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the TwentyOnePointAudit08DataSource control invokes to retrieve data.
		/// </summary>
		public TwentyOnePointAudit08SelectMethod SelectMethod
		{
			get
			{
				TwentyOnePointAudit08SelectMethod selectMethod = TwentyOnePointAudit08SelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (TwentyOnePointAudit08SelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the TwentyOnePointAudit08DataSourceView class that is to be
		/// used by the TwentyOnePointAudit08DataSource.
		/// </summary>
		/// <returns>An instance of the TwentyOnePointAudit08DataSourceView class.</returns>
		protected override BaseDataSourceView<TwentyOnePointAudit08, TwentyOnePointAudit08Key> GetNewDataSourceView()
		{
			return new TwentyOnePointAudit08DataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the TwentyOnePointAudit08DataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class TwentyOnePointAudit08DataSourceView : ProviderDataSourceView<TwentyOnePointAudit08, TwentyOnePointAudit08Key>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08DataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the TwentyOnePointAudit08DataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public TwentyOnePointAudit08DataSourceView(TwentyOnePointAudit08DataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal TwentyOnePointAudit08DataSource TwentyOnePointAudit08Owner
		{
			get { return Owner as TwentyOnePointAudit08DataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal TwentyOnePointAudit08SelectMethod SelectMethod
		{
			get { return TwentyOnePointAudit08Owner.SelectMethod; }
			set { TwentyOnePointAudit08Owner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal TwentyOnePointAudit08Service TwentyOnePointAudit08Provider
		{
			get { return Provider as TwentyOnePointAudit08Service; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<TwentyOnePointAudit08> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<TwentyOnePointAudit08> results = null;
			TwentyOnePointAudit08 item;
			count = 0;
			
			System.Int32 _id;

			switch ( SelectMethod )
			{
				case TwentyOnePointAudit08SelectMethod.Get:
					TwentyOnePointAudit08Key entityKey  = new TwentyOnePointAudit08Key();
					entityKey.Load(values);
					item = TwentyOnePointAudit08Provider.Get(entityKey);
					results = new TList<TwentyOnePointAudit08>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case TwentyOnePointAudit08SelectMethod.GetAll:
                    results = TwentyOnePointAudit08Provider.GetAll(StartIndex, PageSize, out count);
                    break;
				case TwentyOnePointAudit08SelectMethod.GetPaged:
					results = TwentyOnePointAudit08Provider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case TwentyOnePointAudit08SelectMethod.Find:
					if ( FilterParameters != null )
						results = TwentyOnePointAudit08Provider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = TwentyOnePointAudit08Provider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case TwentyOnePointAudit08SelectMethod.GetById:
					_id = ( values["Id"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["Id"], typeof(System.Int32)) : (int)0;
					item = TwentyOnePointAudit08Provider.GetById(_id);
					results = new TList<TwentyOnePointAudit08>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == TwentyOnePointAudit08SelectMethod.Get || SelectMethod == TwentyOnePointAudit08SelectMethod.GetById )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				TwentyOnePointAudit08 entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					TwentyOnePointAudit08Provider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<TwentyOnePointAudit08> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			TwentyOnePointAudit08Provider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region TwentyOnePointAudit08DataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the TwentyOnePointAudit08DataSource class.
	/// </summary>
	public class TwentyOnePointAudit08DataSourceDesigner : ProviderDataSourceDesigner<TwentyOnePointAudit08, TwentyOnePointAudit08Key>
	{
		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08DataSourceDesigner class.
		/// </summary>
		public TwentyOnePointAudit08DataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit08SelectMethod SelectMethod
		{
			get { return ((TwentyOnePointAudit08DataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new TwentyOnePointAudit08DataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region TwentyOnePointAudit08DataSourceActionList

	/// <summary>
	/// Supports the TwentyOnePointAudit08DataSourceDesigner class.
	/// </summary>
	internal class TwentyOnePointAudit08DataSourceActionList : DesignerActionList
	{
		private TwentyOnePointAudit08DataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the TwentyOnePointAudit08DataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public TwentyOnePointAudit08DataSourceActionList(TwentyOnePointAudit08DataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public TwentyOnePointAudit08SelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion TwentyOnePointAudit08DataSourceActionList
	
	#endregion TwentyOnePointAudit08DataSourceDesigner
	
	#region TwentyOnePointAudit08SelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the TwentyOnePointAudit08DataSource.SelectMethod property.
	/// </summary>
	public enum TwentyOnePointAudit08SelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetById method.
		/// </summary>
		GetById
	}
	
	#endregion TwentyOnePointAudit08SelectMethod

	#region TwentyOnePointAudit08Filter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit08"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit08Filter : SqlFilter<TwentyOnePointAudit08Column>
	{
	}
	
	#endregion TwentyOnePointAudit08Filter

	#region TwentyOnePointAudit08ExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit08"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit08ExpressionBuilder : SqlExpressionBuilder<TwentyOnePointAudit08Column>
	{
	}
	
	#endregion TwentyOnePointAudit08ExpressionBuilder	

	#region TwentyOnePointAudit08Property
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;TwentyOnePointAudit08ChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="TwentyOnePointAudit08"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TwentyOnePointAudit08Property : ChildEntityProperty<TwentyOnePointAudit08ChildEntityTypes>
	{
	}
	
	#endregion TwentyOnePointAudit08Property
}

