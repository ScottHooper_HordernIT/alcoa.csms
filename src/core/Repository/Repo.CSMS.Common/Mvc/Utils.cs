﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web;
using System.Web.Routing;
using System.Security.Principal;

namespace Repo.CSMS.Common.Mvc
{
    public class Utils
    {
        #region Url Helper

        public static UrlHelper GetUrlHelper()  
        {  
            var httpContext = new HttpContextWrapper(HttpContext.Current);  
            return new UrlHelper( new RequestContext(httpContext, CurrentRoute(httpContext)));  
        }  

        public static RouteData CurrentRoute(HttpContextWrapper httpContext)  
        {  
            return RouteTable.Routes.GetRouteData(httpContext);
        }

        public static string BaseUrl
        {
            get
            {
                string baseUrl = "";

                try
                {
                    if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    {
                        var request = HttpContext.Current.Request;
                        var appUrl = HttpRuntime.AppDomainAppVirtualPath.TrimEnd('/').TrimStart('/');
                        if (!string.IsNullOrWhiteSpace(appUrl)) appUrl = "/" + appUrl;
                        baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);
                    }
                }
                catch { }

                return baseUrl.TrimEnd('/');
            }
        }

        public static string MvcBaseUrl
        {
            get
            {
                string baseUrl = "";

                try
                {
                    if (HttpContext.Current != null && HttpContext.Current.Request != null)
                    {
                        baseUrl = GetUrlHelper().Action("Index", "Home", null, null, HttpContext.Current.Request.Url.Host);
                    }
                }
                catch { }

                return baseUrl.TrimEnd('/');
            }
        }

        #endregion

        #region Security

        public static string NameWithoutDomain(string identityName)
        {
            string nameWithoutDomain = "";

            try
            {
                if (identityName.Contains('\\'))
                {
                    string[] parts = identityName.Split(new char[] { '\\' });

                    if (parts.Count() > 1)
                        nameWithoutDomain = parts[1];
                    else
                        nameWithoutDomain = parts[0];
                }
                else
                {
                    nameWithoutDomain = identityName;
                }
            }
            catch 
            {
                nameWithoutDomain = identityName;
            }

            return (nameWithoutDomain);
        }

        #endregion
    }
}
