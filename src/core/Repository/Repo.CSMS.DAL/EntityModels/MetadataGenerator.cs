﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects.DataClasses;
using Repo.CSMS.DAL.Infrastructure;

namespace Repo.CSMS.DAL.EntityModels
{
	[MetadataType(typeof(AccessLogMetadata))]
	public partial class AccessLog
	{
		public sealed class AccessLogMetadata //: IEntity
		{
		
			[DisplayName("Log")]
			[Required(ErrorMessage="Log is required")]
    		public Int32 LogId { get; set; }

			[DisplayName("Log Date Time")]
			[Required(ErrorMessage="Log Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LogDateTime { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(AdHoc_RadarMetadata))]
	public partial class AdHoc_Radar
	{
		public sealed class AdHoc_RadarMetadata //: IEntity
		{
		
			[DisplayName("Ad Hoc_ Radar")]
			[Required(ErrorMessage="Ad Hoc_ Radar is required")]
    		public Int32 AdHoc_RadarId { get; set; }

			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Kpi Ordinal")]
			[Required(ErrorMessage="Kpi Ordinal is required")]
    		public Int32 KpiOrdinal { get; set; }

			[DisplayName("Kpi Name")]
			[Required(ErrorMessage="Kpi Name is required")]
			[StringLength(255)]
    		public String KpiName { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

    		public EntityCollection<AdHoc_Radar_Items> AdHoc_Radar_Items { get; set; }

    		public EntityCollection<AdHoc_Radar_Items2> AdHoc_Radar_Items2 { get; set; }

		}
	}
	
	[MetadataType(typeof(AdHoc_Radar_ItemsMetadata))]
	public partial class AdHoc_Radar_Items
	{
		public sealed class AdHoc_Radar_ItemsMetadata //: IEntity
		{
		
			[DisplayName("Ad Hoc_ Radar_ Item")]
			[Required(ErrorMessage="Ad Hoc_ Radar_ Item is required")]
    		public Int32 AdHoc_Radar_ItemId { get; set; }

			[DisplayName("Ad Hoc_ Radar")]
			[Required(ErrorMessage="Ad Hoc_ Radar is required")]
    		public Int32 AdHoc_RadarId { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Kpi Score Ytd")]
			[Required(ErrorMessage="Kpi Score Ytd is required")]
			[StringLength(50)]
    		public String KpiScoreYtd { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Companies Not Compliant")]
    		public String CompaniesNotCompliant { get; set; }

			[DisplayName("Kpi Score Jan")]
			[StringLength(50)]
    		public String KpiScoreJan { get; set; }

			[DisplayName("Kpi Score Feb")]
			[StringLength(50)]
    		public String KpiScoreFeb { get; set; }

			[DisplayName("Kpi Score Mar")]
			[StringLength(50)]
    		public String KpiScoreMar { get; set; }

			[DisplayName("Kpi Score Apr")]
			[StringLength(50)]
    		public String KpiScoreApr { get; set; }

			[DisplayName("Kpi Score May")]
			[StringLength(50)]
    		public String KpiScoreMay { get; set; }

			[DisplayName("Kpi Score Jun")]
			[StringLength(50)]
    		public String KpiScoreJun { get; set; }

			[DisplayName("Kpi Score Jul")]
			[StringLength(50)]
    		public String KpiScoreJul { get; set; }

			[DisplayName("Kpi Score Aug")]
			[StringLength(50)]
    		public String KpiScoreAug { get; set; }

			[DisplayName("Kpi Score Sep")]
			[StringLength(50)]
    		public String KpiScoreSep { get; set; }

			[DisplayName("Kpi Score Oct")]
			[StringLength(50)]
    		public String KpiScoreOct { get; set; }

			[DisplayName("Kpi Score Nov")]
			[StringLength(50)]
    		public String KpiScoreNov { get; set; }

			[DisplayName("Kpi Score Dec")]
			[StringLength(50)]
    		public String KpiScoreDec { get; set; }

			[DisplayName("Companies Not Compliant Jan")]
    		public String CompaniesNotCompliantJan { get; set; }

			[DisplayName("Companies Not Compliant Feb")]
    		public String CompaniesNotCompliantFeb { get; set; }

			[DisplayName("Companies Not Compliant Mar")]
    		public String CompaniesNotCompliantMar { get; set; }

			[DisplayName("Companies Not Compliant Apr")]
    		public String CompaniesNotCompliantApr { get; set; }

			[DisplayName("Companies Not Compliant May")]
    		public String CompaniesNotCompliantMay { get; set; }

			[DisplayName("Companies Not Compliant Jun")]
    		public String CompaniesNotCompliantJun { get; set; }

			[DisplayName("Companies Not Compliant Jul")]
    		public String CompaniesNotCompliantJul { get; set; }

			[DisplayName("Companies Not Compliant Aug")]
    		public String CompaniesNotCompliantAug { get; set; }

			[DisplayName("Companies Not Compliant Sep")]
    		public String CompaniesNotCompliantSep { get; set; }

			[DisplayName("Companies Not Compliant Oct")]
    		public String CompaniesNotCompliantOct { get; set; }

			[DisplayName("Companies Not Compliant Nov")]
    		public String CompaniesNotCompliantNov { get; set; }

			[DisplayName("Companies Not Compliant Dec")]
    		public String CompaniesNotCompliantDec { get; set; }

    		public EntityCollection<AdHoc_Radar> AdHoc_Radar { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(AdHoc_Radar_Items2Metadata))]
	public partial class AdHoc_Radar_Items2
	{
		public sealed class AdHoc_Radar_Items2Metadata //: IEntity
		{
		
			[DisplayName("Ad Hoc_ Radar_ Item2")]
			[Required(ErrorMessage="Ad Hoc_ Radar_ Item2 is required")]
    		public Int32 AdHoc_Radar_Item2Id { get; set; }

			[DisplayName("Ad Hoc_ Radar")]
			[Required(ErrorMessage="Ad Hoc_ Radar is required")]
    		public Int32 AdHoc_RadarId { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Kpi Compliance Score")]
    		public Int32 KpiComplianceScore { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<AdHoc_Radar> AdHoc_Radar { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminAuditScoreMetadata))]
	public partial class AdminAuditScore
	{
		public sealed class AdminAuditScoreMetadata //: IEntity
		{
		
			[DisplayName("Audit Score")]
			[Required(ErrorMessage="Audit Score is required")]
    		public Int32 AuditScoreId { get; set; }

			[DisplayName("Year")]
    		public Int32 Year { get; set; }

			[DisplayName("Month")]
    		public Int32 Month { get; set; }

			[DisplayName("Description")]
			[StringLength(50)]
    		public String Description { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminDashboardMetadata))]
	public partial class AdminDashboard
	{
		public sealed class AdminDashboardMetadata //: IEntity
		{
		
			[DisplayName("Dashboard")]
			[Required(ErrorMessage="Dashboard is required")]
    		public Int32 DashboardId { get; set; }

			[DisplayName("Field Name")]
			[StringLength(300)]
    		public String FieldName { get; set; }

			[DisplayName("Parent Dashboard")]
    		public Int32 ParentDashboardId { get; set; }

    		public EntityCollection<AdminPlan> AdminPlans { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminPlanMetadata))]
	public partial class AdminPlan
	{
		public sealed class AdminPlanMetadata //: IEntity
		{
		
			[DisplayName("Dashboard Plan")]
			[Required(ErrorMessage="Dashboard Plan is required")]
    		public Int32 DashboardPlanId { get; set; }

			[DisplayName("Dashboard")]
			[Required(ErrorMessage="Dashboard is required")]
    		public Int32 DashboardId { get; set; }

			[DisplayName("Plan Value")]
			[StringLength(50)]
    		public String PlanValue { get; set; }

			[DisplayName("Plan Text")]
			[StringLength(50)]
    		public String PlanText { get; set; }

			[DisplayName("Year")]
    		public Int32 Year { get; set; }

			[DisplayName("Month")]
    		public Int32 Month { get; set; }

			[DisplayName("Operator")]
			[StringLength(20)]
    		public String Operator { get; set; }

    		public EntityCollection<AdminDashboard> AdminDashboard { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminPlanOperatorMetadata))]
	public partial class AdminPlanOperator
	{
		public sealed class AdminPlanOperatorMetadata //: IEntity
		{
		
			[DisplayName("Operator")]
			[Required(ErrorMessage="Operator is required")]
    		public Int32 OperatorId { get; set; }

			[DisplayName("Operator")]
			[StringLength(10)]
    		public String Operator { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskMetadata))]
	public partial class AdminTask
	{
		public sealed class AdminTaskMetadata //: IEntity
		{
		
			[DisplayName("Admin Task")]
			[Required(ErrorMessage="Admin Task is required")]
    		public Int32 AdminTaskId { get; set; }

			[DisplayName("Admin Task Source")]
			[Required(ErrorMessage="Admin Task Source is required")]
    		public Int32 AdminTaskSourceId { get; set; }

			[DisplayName("Admin Task Type")]
			[Required(ErrorMessage="Admin Task Type is required")]
    		public Int32 AdminTaskTypeId { get; set; }

			[DisplayName("Admin Task Status")]
			[Required(ErrorMessage="Admin Task Status is required")]
    		public Int32 AdminTaskStatusId { get; set; }

			[DisplayName("Admin Task Comments")]
    		public String AdminTaskComments { get; set; }

			[DisplayName("Date Opened")]
			[Required(ErrorMessage="Date Opened is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateOpened { get; set; }

			[DisplayName("Opened By User")]
			[Required(ErrorMessage="Opened By User is required")]
    		public Int32 OpenedByUserId { get; set; }

			[DisplayName("Date Closed")]
			[DataType(DataType.DateTime)]
    		public DateTime DateClosed { get; set; }

			[DisplayName("Closed By User")]
    		public Int32 ClosedByUserId { get; set; }

			[DisplayName("Csms Access")]
			[Required(ErrorMessage="Csms Access is required")]
    		public Int32 CsmsAccessId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Login")]
			[Required(ErrorMessage="Login is required")]
			[StringLength(255)]
    		public String Login { get; set; }

			[DisplayName("Email Address")]
			[Required(ErrorMessage="Email Address is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailAddress { get; set; }

			[DisplayName("First Name")]
			[Required(ErrorMessage="First Name is required")]
			[StringLength(255)]
    		public String FirstName { get; set; }

			[DisplayName("Last Name")]
			[Required(ErrorMessage="Last Name is required")]
			[StringLength(255)]
    		public String LastName { get; set; }

			[DisplayName("Job Role")]
			[Required(ErrorMessage="Job Role is required")]
			[StringLength(255)]
    		public String JobRole { get; set; }

			[DisplayName("Job Title")]
			[Required(ErrorMessage="Job Title is required")]
			[StringLength(255)]
    		public String JobTitle { get; set; }

			[DisplayName("Mobile No")]
			[StringLength(255)]
    		public String MobileNo { get; set; }

			[DisplayName("Telephone No")]
			[StringLength(255)]
			[DataType(DataType.PhoneNumber)]
    		public String TelephoneNo { get; set; }

			[DisplayName("Fax No")]
			[StringLength(255)]
    		public String FaxNo { get; set; }

    		public EntityCollection<AdminTaskSource> AdminTaskSource { get; set; }

    		public EntityCollection<AdminTaskStatus> AdminTaskStatu { get; set; }

    		public EntityCollection<AdminTaskType> AdminTaskType { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<CsmsAccess> CsmsAccess { get; set; }

    		public EntityCollection<AdminTaskEmailLog> AdminTaskEmailLogs { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskEmailLogMetadata))]
	public partial class AdminTaskEmailLog
	{
		public sealed class AdminTaskEmailLogMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Email")]
			[Required(ErrorMessage="Admin Task Email is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailId { get; set; }

			[DisplayName("Admin Task")]
			[Required(ErrorMessage="Admin Task is required")]
    		public Int32 AdminTaskId { get; set; }

			[DisplayName("Csms Email Log")]
			[Required(ErrorMessage="Csms Email Log is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailLogId { get; set; }

    		public EntityCollection<AdminTask> AdminTask { get; set; }

    		public EntityCollection<CsmsEmailLog> CsmsEmailLog { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskEmailRecipientMetadata))]
	public partial class AdminTaskEmailRecipient
	{
		public sealed class AdminTaskEmailRecipientMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Email Recipient")]
			[Required(ErrorMessage="Admin Task Email Recipient is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailRecipientId { get; set; }

			[DisplayName("Recipient Name")]
			[Required(ErrorMessage="Recipient Name is required")]
			[StringLength(100)]
    		public String RecipientName { get; set; }

			[DisplayName("Recipient Desc")]
			[Required(ErrorMessage="Recipient Desc is required")]
			[StringLength(255)]
    		public String RecipientDesc { get; set; }

    		public EntityCollection<AdminTaskEmailTemplateRecipient> AdminTaskEmailTemplateRecipients { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskEmailTemplateMetadata))]
	public partial class AdminTaskEmailTemplate
	{
		public sealed class AdminTaskEmailTemplateMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Email Template")]
			[Required(ErrorMessage="Admin Task Email Template is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailTemplateId { get; set; }

			[DisplayName("Admin Task Type")]
			[Required(ErrorMessage="Admin Task Type is required")]
    		public Int32 AdminTaskTypeId { get; set; }

			[DisplayName("Email Subject")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailSubject { get; set; }

			[DisplayName("Email Body Alcoa Direct Existing User")]
			[DataType(DataType.EmailAddress)]
    		public Byte[] EmailBodyAlcoaDirectExistingUser { get; set; }

			[DisplayName("Email Body Alcoa Lan")]
			[DataType(DataType.EmailAddress)]
    		public Byte[] EmailBodyAlcoaLan { get; set; }

			[DisplayName("Email Body Alcoa Direct New User")]
			[DataType(DataType.EmailAddress)]
    		public Byte[] EmailBodyAlcoaDirectNewUser { get; set; }

    		public EntityCollection<AdminTaskType> AdminTaskType { get; set; }

    		public EntityCollection<AdminTaskEmailTemplateAttachment> AdminTaskEmailTemplateAttachments { get; set; }

    		public EntityCollection<AdminTaskEmailTemplateRecipient> AdminTaskEmailTemplateRecipients { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskEmailTemplateAttachmentMetadata))]
	public partial class AdminTaskEmailTemplateAttachment
	{
		public sealed class AdminTaskEmailTemplateAttachmentMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Email Template Attachment")]
			[Required(ErrorMessage="Admin Task Email Template Attachment is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailTemplateAttachmentId { get; set; }

			[DisplayName("Admin Task Email Template")]
			[Required(ErrorMessage="Admin Task Email Template is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailTemplateId { get; set; }

			[DisplayName("Csms File")]
			[Required(ErrorMessage="Csms File is required")]
    		public Int32 CsmsFileId { get; set; }

			[DisplayName("Csms Access")]
			[Required(ErrorMessage="Csms Access is required")]
    		public Int32 CsmsAccessId { get; set; }

    		public EntityCollection<AdminTaskEmailTemplate> AdminTaskEmailTemplate { get; set; }

    		public EntityCollection<CsmsFile> CsmsFile { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskEmailTemplateRecipientMetadata))]
	public partial class AdminTaskEmailTemplateRecipient
	{
		public sealed class AdminTaskEmailTemplateRecipientMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Email Template Recipient")]
			[Required(ErrorMessage="Admin Task Email Template Recipient is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailTemplateRecipientId { get; set; }

			[DisplayName("Admin Task Email Template")]
			[Required(ErrorMessage="Admin Task Email Template is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailTemplateId { get; set; }

			[DisplayName("Admin Task Email Recipient")]
			[Required(ErrorMessage="Admin Task Email Recipient is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 AdminTaskEmailRecipientId { get; set; }

			[DisplayName("Csms Email Recipient Type")]
			[Required(ErrorMessage="Csms Email Recipient Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailRecipientTypeId { get; set; }

    		public EntityCollection<AdminTaskEmailRecipient> AdminTaskEmailRecipient { get; set; }

    		public EntityCollection<AdminTaskEmailTemplate> AdminTaskEmailTemplate { get; set; }

    		public EntityCollection<CsmsEmailRecipientType> CsmsEmailRecipientType { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskSourceMetadata))]
	public partial class AdminTaskSource
	{
		public sealed class AdminTaskSourceMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Source")]
			[Required(ErrorMessage="Admin Task Source is required")]
    		public Int32 AdminTaskSourceId { get; set; }

			[DisplayName("Admin Task Source Name")]
			[Required(ErrorMessage="Admin Task Source Name is required")]
			[StringLength(50)]
    		public String AdminTaskSourceName { get; set; }

			[DisplayName("Admin Task Source Desc")]
			[Required(ErrorMessage="Admin Task Source Desc is required")]
			[StringLength(255)]
    		public String AdminTaskSourceDesc { get; set; }

    		public EntityCollection<AdminTask> AdminTasks { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskStatusMetadata))]
	public partial class AdminTaskStatus
	{
		public sealed class AdminTaskStatusMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Status")]
			[Required(ErrorMessage="Admin Task Status is required")]
    		public Int32 AdminTaskStatusId { get; set; }

			[DisplayName("Status Name")]
			[Required(ErrorMessage="Status Name is required")]
			[StringLength(50)]
    		public String StatusName { get; set; }

			[DisplayName("Status Desc")]
			[Required(ErrorMessage="Status Desc is required")]
			[StringLength(255)]
    		public String StatusDesc { get; set; }

    		public EntityCollection<AdminTask> AdminTasks { get; set; }

		}
	}
	
	[MetadataType(typeof(AdminTaskTypeMetadata))]
	public partial class AdminTaskType
	{
		public sealed class AdminTaskTypeMetadata //: IEntity
		{
		
			[DisplayName("Admin Task Type")]
			[Required(ErrorMessage="Admin Task Type is required")]
    		public Int32 AdminTaskTypeId { get; set; }

			[DisplayName("Admin Task Type Name")]
			[Required(ErrorMessage="Admin Task Type Name is required")]
			[StringLength(255)]
    		public String AdminTaskTypeName { get; set; }

			[DisplayName("Admin Task Type Desc")]
			[StringLength(255)]
    		public String AdminTaskTypeDesc { get; set; }

    		public EntityCollection<AdminTask> AdminTasks { get; set; }

    		public EntityCollection<AdminTaskEmailTemplate> AdminTaskEmailTemplates { get; set; }

		}
	}
	
	[MetadataType(typeof(AnnualTargetMetadata))]
	public partial class AnnualTarget
	{
		public sealed class AnnualTargetMetadata //: IEntity
		{
		
			[DisplayName("Annual Targets")]
			[Required(ErrorMessage="Annual Targets is required")]
    		public Int32 AnnualTargetsId { get; set; }

			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("EHS Audits")]
    		public Int32 EHSAudits { get; set; }

			[DisplayName("Workplace Safety Compliance")]
    		public Int32 WorkplaceSafetyCompliance { get; set; }

			[DisplayName("Health Safety Work Contacts")]
    		public Int32 HealthSafetyWorkContacts { get; set; }

			[DisplayName("Behavioural Safety Program")]
    		public Decimal BehaviouralSafetyProgram { get; set; }

			[DisplayName("Quarterly Audit Score")]
    		public Int32 QuarterlyAuditScore { get; set; }

			[DisplayName("Toolbox Meetings Per Month")]
    		public Int32 ToolboxMeetingsPerMonth { get; set; }

			[DisplayName("Alcoa Weekly Contractors Meeting")]
    		public Int32 AlcoaWeeklyContractorsMeeting { get; set; }

			[DisplayName("Alcoa Monthly Contractors Meeting")]
    		public Int32 AlcoaMonthlyContractorsMeeting { get; set; }

			[DisplayName("Safety Plan")]
    		public Int32 SafetyPlan { get; set; }

			[DisplayName("JSA Score")]
    		public Int32 JSAScore { get; set; }

			[DisplayName("Fatality Prevention")]
    		public Int32 FatalityPrevention { get; set; }

			[DisplayName("TRIFR")]
    		public Decimal TRIFR { get; set; }

			[DisplayName("AIFR")]
    		public Decimal AIFR { get; set; }

			[DisplayName("Training")]
    		public Int32 Training { get; set; }

			[DisplayName("Medical Schedule")]
    		public Int32 MedicalSchedule { get; set; }

			[DisplayName("Training Schedule")]
    		public Int32 TrainingSchedule { get; set; }

			[DisplayName("IFE Injury Ratio Target YTD")]
    		public Decimal IFEInjuryRatioTargetYTD { get; set; }

			[DisplayName("IFE Injury Ratio Target Mon")]
    		public Decimal IFEInjuryRatioTargetMon { get; set; }

			[DisplayName("RN Injury Target YTD")]
    		public Decimal RNInjuryTargetYTD { get; set; }

			[DisplayName("RN Injury Ratio Target Mon")]
    		public Decimal RNInjuryRatioTargetMon { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

		}
	}
	
	[MetadataType(typeof(ApssLogMetadata))]
	public partial class ApssLog
	{
		public sealed class ApssLogMetadata //: IEntity
		{
		
			[DisplayName("Entry")]
			[Required(ErrorMessage="Entry is required")]
    		public Int32 EntryId { get; set; }

			[DisplayName("Time Stamp")]
			[Required(ErrorMessage="Time Stamp is required")]
			[DataType(DataType.DateTime)]
    		public DateTime TimeStamp { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Tag")]
			[Required(ErrorMessage="File Tag is required")]
			[StringLength(255)]
    		public String FileTag { get; set; }

			[DisplayName("File Type")]
			[Required(ErrorMessage="File Type is required")]
			[StringLength(20)]
    		public String FileType { get; set; }

		}
	}
	
	[MetadataType(typeof(ARPNameMasterMetadata))]
	public partial class ARPNameMaster
	{
		public sealed class ARPNameMasterMetadata //: IEntity
		{
		
			[DisplayName("ARP User")]
			[Required(ErrorMessage="ARP User is required")]
    		public Int32 ARPUserId { get; set; }

			[DisplayName("ARP Name")]
    		public String ARPName { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("ARP Person")]
    		public String ARPPersonId { get; set; }

		}
	}
	
	[MetadataType(typeof(AuditFileVaultMetadata))]
	public partial class AuditFileVault
	{
		public sealed class AuditFileVaultMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("File Vault")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

		}
	}
	
	[MetadataType(typeof(AuditFileVaultTableMetadata))]
	public partial class AuditFileVaultTable
	{
		public sealed class AuditFileVaultTableMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("File Vault Table")]
    		public Int32 FileVaultTableId { get; set; }

			[DisplayName("File Vault")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("File Vault Category")]
    		public Int32 FileVaultCategoryId { get; set; }

			[DisplayName("File Vault Sub Category")]
    		public Int32 FileVaultSubCategoryId { get; set; }

			[DisplayName("File Name Custom")]
			[StringLength(255)]
    		public String FileNameCustom { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(CompaniesAuditMetadata))]
	public partial class CompaniesAudit
	{
		public sealed class CompaniesAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Company Name")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Company Name Ebi")]
			[StringLength(255)]
    		public String CompanyNameEbi { get; set; }

			[DisplayName("Company Abn")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("Phone No")]
			[StringLength(20)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[DisplayName("Fax No")]
			[StringLength(20)]
    		public String FaxNo { get; set; }

			[DisplayName("Mob No")]
			[StringLength(20)]
    		public String MobNo { get; set; }

			[DisplayName("Address Business")]
			[StringLength(200)]
    		public String AddressBusiness { get; set; }

			[DisplayName("Address Postal")]
			[StringLength(200)]
    		public String AddressPostal { get; set; }

			[DisplayName("Procurement Supplier No")]
			[StringLength(100)]
    		public String ProcurementSupplierNo { get; set; }

			[DisplayName("EHS Consultant")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("Modifiedby User")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Iproc Supplier No")]
			[StringLength(10)]
    		public String IprocSupplierNo { get; set; }

			[DisplayName("Company Status")]
    		public Int32 CompanyStatusId { get; set; }

			[DisplayName("Start Date")]
			[DataType(DataType.DateTime)]
    		public DateTime StartDate { get; set; }

			[DisplayName("End Date")]
			[DataType(DataType.DateTime)]
    		public DateTime EndDate { get; set; }

			[DisplayName("Company Status2")]
    		public Int32 CompanyStatus2Id { get; set; }

			[DisplayName("Contacts Last Updated")]
			[DataType(DataType.DateTime)]
    		public DateTime ContactsLastUpdated { get; set; }

			[DisplayName("Deactivated")]
    		public Boolean Deactivated { get; set; }

		}
	}
	
	[MetadataType(typeof(CompaniesEhsimsMapMetadata))]
	public partial class CompaniesEhsimsMap
	{
		public sealed class CompaniesEhsimsMapMetadata //: IEntity
		{
		
			[DisplayName("Ehssims Map")]
			[Required(ErrorMessage="Ehssims Map is required")]
    		public Int32 EhssimsMapId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Cont Company Code")]
			[Required(ErrorMessage="Cont Company Code is required")]
			[StringLength(3)]
    		public String ContCompanyCode { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("ABN")]
    		public String ABN { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(CompaniesHrCrpDataMetadata))]
	public partial class CompaniesHrCrpData
	{
		public sealed class CompaniesHrCrpDataMetadata //: IEntity
		{
		
			[DisplayName("Companies Hr Crp Data")]
			[Required(ErrorMessage="Companies Hr Crp Data is required")]
    		public Int32 CompaniesHrCrpDataId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Crp Name")]
			[Required(ErrorMessage="Crp Name is required")]
			[StringLength(255)]
    		public String CrpName { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(CompaniesHrMapMetadata))]
	public partial class CompaniesHrMap
	{
		public sealed class CompaniesHrMapMetadata //: IEntity
		{
		
			[DisplayName("Companies Hr Map")]
			[Required(ErrorMessage="Companies Hr Map is required")]
    		public Int32 CompaniesHrMapId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Company Name Hr")]
			[Required(ErrorMessage="Company Name Hr is required")]
    		public Int32 CompanyNameHr { get; set; }

		}
	}
	
	[MetadataType(typeof(CompaniesRelationshipMetadata))]
	public partial class CompaniesRelationship
	{
		public sealed class CompaniesRelationshipMetadata //: IEntity
		{
		
			[DisplayName("Parent")]
			[Required(ErrorMessage="Parent is required")]
    		public Int32 ParentId { get; set; }

			[DisplayName("Child")]
			[Required(ErrorMessage="Child is required")]
    		public Int32 ChildId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Company> Company1 { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyMetadata))]
	public partial class Company
	{
		public sealed class CompanyMetadata //: IEntity
		{
		
			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Company Name Ebi")]
			[StringLength(255)]
    		public String CompanyNameEbi { get; set; }

			[DisplayName("Company Abn")]
			[Required(ErrorMessage="Company Abn is required")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("Phone No")]
			[StringLength(20)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[DisplayName("Fax No")]
			[StringLength(20)]
    		public String FaxNo { get; set; }

			[DisplayName("Mob No")]
			[StringLength(20)]
    		public String MobNo { get; set; }

			[DisplayName("Address Business")]
			[StringLength(200)]
    		public String AddressBusiness { get; set; }

			[DisplayName("Address Postal")]
			[StringLength(200)]
    		public String AddressPostal { get; set; }

			[DisplayName("Procurement Supplier No")]
			[StringLength(100)]
    		public String ProcurementSupplierNo { get; set; }

			[DisplayName("EHS Consultant")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("Modifiedby User")]
			[Required(ErrorMessage="Modifiedby User is required")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Iproc Supplier No")]
			[StringLength(10)]
    		public String IprocSupplierNo { get; set; }

			[DisplayName("Company Status")]
			[Required(ErrorMessage="Company Status is required")]
    		public Int32 CompanyStatusId { get; set; }

			[DisplayName("Start Date")]
			[DataType(DataType.DateTime)]
    		public DateTime StartDate { get; set; }

			[DisplayName("End Date")]
			[DataType(DataType.DateTime)]
    		public DateTime EndDate { get; set; }

			[DisplayName("Company Status2")]
			[Required(ErrorMessage="Company Status2 is required")]
    		public Int32 CompanyStatus2Id { get; set; }

			[DisplayName("Contacts Last Updated")]
			[DataType(DataType.DateTime)]
    		public DateTime ContactsLastUpdated { get; set; }

			[DisplayName("Deactivated")]
    		public Boolean Deactivated { get; set; }

			[DisplayName("Ihs Cont Company Code")]
    		public Int32 IhsContCompanyCode { get; set; }

			[DisplayName("Ihs Cont Company Field")]
			[StringLength(500)]
    		public String IhsContCompanyField { get; set; }

			[DisplayName("Ihs Loc Code")]
			[StringLength(50)]
    		public String IhsLocCode { get; set; }

    		public EntityCollection<AdHoc_Radar_Items2> AdHoc_Radar_Items2 { get; set; }

    		public EntityCollection<CompanyStatus> CompanyStatus { get; set; }

    		public EntityCollection<CompanyStatus2> CompanyStatus2 { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<CompaniesEhsimsMap> CompaniesEhsimsMaps { get; set; }

    		public EntityCollection<CompaniesRelationship> CompaniesRelationships { get; set; }

    		public EntityCollection<CompaniesRelationship> CompaniesRelationships1 { get; set; }

    		public EntityCollection<CompanyNote> CompanyNotes { get; set; }

    		public EntityCollection<CompanySiteCategoryException> CompanySiteCategoryExceptions { get; set; }

    		public EntityCollection<CompanySiteCategoryException2> CompanySiteCategoryException2 { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals { get; set; }

    		public EntityCollection<ContactsContractor> ContactsContractors { get; set; }

    		public EntityCollection<CSA> CSAs { get; set; }

    		public EntityCollection<FileDb> FileDbs { get; set; }

    		public EntityCollection<FileDbMedicalTraining> FileDbMedicalTrainings { get; set; }

    		public EntityCollection<Kpi> Kpis { get; set; }

    		public EntityCollection<QuestionnaireContractorRs> QuestionnaireContractorRs { get; set; }

    		public EntityCollection<QuestionnaireContractorRs> QuestionnaireContractorRs1 { get; set; }

    		public EntityCollection<Residential> Residentials { get; set; }

    		public EntityCollection<SafetyPlans_SE_Responses> SafetyPlans_SE_Responses { get; set; }

    		public EntityCollection<SqExemption> SqExemptions { get; set; }

    		public EntityCollection<SqExemption> SqExemptions1 { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

    		public EntityCollection<User> Users { get; set; }

    		public EntityCollection<CompanySiteCategoryStandardHistory> CompanySiteCategoryStandardHistories { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyContractorReviewMetadata))]
	public partial class CompanyContractorReview
	{
		public sealed class CompanyContractorReviewMetadata //: IEntity
		{
		
			[DisplayName("Company Contractor Review")]
			[Required(ErrorMessage="Company Contractor Review is required")]
    		public Int32 CompanyContractorReviewId { get; set; }

			[DisplayName("Company Abn")]
			[Required(ErrorMessage="Company Abn is required")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("CWK Number")]
			[Required(ErrorMessage="CWK Number is required")]
    		public Decimal CWKNumber { get; set; }

			[DisplayName("Review Date")]
			[Required(ErrorMessage="Review Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ReviewDate { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Labour Class Type")]
			[Required(ErrorMessage="Labour Class Type is required")]
    		public Int32 LabourClassTypeId { get; set; }

			[DisplayName("Alcoa Record Active")]
			[Required(ErrorMessage="Alcoa Record Active is required")]
    		public Boolean AlcoaRecordActive { get; set; }

			[DisplayName("Active Induction")]
			[Required(ErrorMessage="Active Induction is required")]
    		public Boolean ActiveInduction { get; set; }

			[DisplayName("Still At Company")]
			[Required(ErrorMessage="Still At Company is required")]
    		public Boolean StillAtCompany { get; set; }

			[DisplayName("Active At Alcoa")]
			[Required(ErrorMessage="Active At Alcoa is required")]
    		public Boolean ActiveAtAlcoa { get; set; }

			[DisplayName("Comments")]
    		public String Comments { get; set; }

			[DisplayName("Changed")]
			[Required(ErrorMessage="Changed is required")]
    		public Boolean Changed { get; set; }

			[DisplayName("Changed By User")]
    		public Int32 ChangedByUserId { get; set; }

			[DisplayName("Changed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ChangedDate { get; set; }

			[DisplayName("Processed")]
			[Required(ErrorMessage="Processed is required")]
    		public Boolean Processed { get; set; }

			[DisplayName("Processed By User")]
    		public Int32 ProcessedByUserId { get; set; }

			[DisplayName("Processed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ProcessedDate { get; set; }

			[DisplayName("Last Update By User")]
    		public Int32 LastUpdateByUserId { get; set; }

			[DisplayName("Last Update Date")]
			[Required(ErrorMessage="Last Update Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LastUpdateDate { get; set; }

			[DisplayName("Last Update Reason Type")]
			[Required(ErrorMessage="Last Update Reason Type is required")]
    		public Int32 LastUpdateReasonTypeId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<UpdateReasonType> UpdateReasonType { get; set; }

    		public EntityCollection<User> User2 { get; set; }

    		public EntityCollection<LabourClassType> LabourClassType { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyContractorReviewAuditMetadata))]
	public partial class CompanyContractorReviewAudit
	{
		public sealed class CompanyContractorReviewAuditMetadata //: IEntity
		{
		
			[DisplayName("Company Contractor Review Audit")]
			[Required(ErrorMessage="Company Contractor Review Audit is required")]
    		public Int32 CompanyContractorReviewAuditId { get; set; }

			[DisplayName("Company Abn")]
			[Required(ErrorMessage="Company Abn is required")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("CWK Number")]
			[Required(ErrorMessage="CWK Number is required")]
    		public Decimal CWKNumber { get; set; }

			[DisplayName("Review Date")]
			[Required(ErrorMessage="Review Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ReviewDate { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Labour Class Type")]
			[Required(ErrorMessage="Labour Class Type is required")]
    		public Int32 LabourClassTypeId { get; set; }

			[DisplayName("Alcoa Record Active")]
			[Required(ErrorMessage="Alcoa Record Active is required")]
    		public Boolean AlcoaRecordActive { get; set; }

			[DisplayName("Active Induction")]
			[Required(ErrorMessage="Active Induction is required")]
    		public Boolean ActiveInduction { get; set; }

			[DisplayName("Still At Company")]
			[Required(ErrorMessage="Still At Company is required")]
    		public Boolean StillAtCompany { get; set; }

			[DisplayName("Active At Alcoa")]
			[Required(ErrorMessage="Active At Alcoa is required")]
    		public Boolean ActiveAtAlcoa { get; set; }

			[DisplayName("Comments")]
    		public String Comments { get; set; }

			[DisplayName("Changed")]
			[Required(ErrorMessage="Changed is required")]
    		public Boolean Changed { get; set; }

			[DisplayName("Changed By User")]
    		public Int32 ChangedByUserId { get; set; }

			[DisplayName("Changed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ChangedDate { get; set; }

			[DisplayName("Processed")]
			[Required(ErrorMessage="Processed is required")]
    		public Boolean Processed { get; set; }

			[DisplayName("Processed By User")]
    		public Int32 ProcessedByUserId { get; set; }

			[DisplayName("Processed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ProcessedDate { get; set; }

			[DisplayName("Last Update By User")]
    		public Int32 LastUpdateByUserId { get; set; }

			[DisplayName("Last Update Date")]
			[Required(ErrorMessage="Last Update Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LastUpdateDate { get; set; }

			[DisplayName("Last Update Reason Type")]
			[Required(ErrorMessage="Last Update Reason Type is required")]
    		public Int32 LastUpdateReasonTypeId { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyCRPMapMetadata))]
	public partial class CompanyCRPMap
	{
		public sealed class CompanyCRPMapMetadata //: IEntity
		{
		
			[DisplayName("CRP_ Company")]
			[Required(ErrorMessage="CRP_ Company is required")]
    		public Int32 CRP_CompanyId { get; set; }

			[DisplayName("CRP_ Names")]
    		public String CRP_Names { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyNoteMetadata))]
	public partial class CompanyNote
	{
		public sealed class CompanyNoteMetadata //: IEntity
		{
		
			[DisplayName("Company Note")]
			[Required(ErrorMessage="Company Note is required")]
    		public Int32 CompanyNoteId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created By Role")]
			[Required(ErrorMessage="Created By Role is required")]
    		public Int32 CreatedByRoleId { get; set; }

			[DisplayName("Created By Company")]
			[Required(ErrorMessage="Created By Company is required")]
    		public Int32 CreatedByCompanyId { get; set; }

			[DisplayName("Created By Privledged Role")]
			[StringLength(100)]
    		public String CreatedByPrivledgedRole { get; set; }

			[DisplayName("Visibility")]
			[StringLength(100)]
    		public String Visibility { get; set; }

			[DisplayName("Action Note")]
			[Required(ErrorMessage="Action Note is required")]
			[StringLength(8000)]
    		public String ActionNote { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Role> Role { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryMetadata))]
	public partial class CompanySiteCategory
	{
		public sealed class CompanySiteCategoryMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Category Name")]
			[Required(ErrorMessage="Category Name is required")]
			[StringLength(100)]
    		public String CategoryName { get; set; }

			[DisplayName("Category Desc")]
			[Required(ErrorMessage="Category Desc is required")]
			[StringLength(255)]
    		public String CategoryDesc { get; set; }

			[DisplayName("Ordinal")]
			[Required(ErrorMessage="Ordinal is required")]
    		public Int32 Ordinal { get; set; }

    		public EntityCollection<AdHoc_Radar> AdHoc_Radar { get; set; }

    		public EntityCollection<AnnualTarget> AnnualTargets { get; set; }

    		public EntityCollection<CompanySiteCategoryException> CompanySiteCategoryExceptions { get; set; }

    		public EntityCollection<CompanySiteCategoryException2> CompanySiteCategoryException2 { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards { get; set; }

    		public EntityCollection<KpiMeasure> KpiMeasures { get; set; }

    		public EntityCollection<CompanySiteCategoryStandardHistory> CompanySiteCategoryStandardHistories { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryExceptionMetadata))]
	public partial class CompanySiteCategoryException
	{
		public sealed class CompanySiteCategoryExceptionMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category Exception")]
			[Required(ErrorMessage="Company Site Category Exception is required")]
    		public Int32 CompanySiteCategoryExceptionId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Month Year From")]
			[Required(ErrorMessage="Month Year From is required")]
			[DataType(DataType.DateTime)]
    		public DateTime MonthYearFrom { get; set; }

			[DisplayName("Month Year To")]
			[Required(ErrorMessage="Month Year To is required")]
			[DataType(DataType.DateTime)]
    		public DateTime MonthYearTo { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryException2Metadata))]
	public partial class CompanySiteCategoryException2
	{
		public sealed class CompanySiteCategoryException2Metadata //: IEntity
		{
		
			[DisplayName("Company Site Category Exception2")]
			[Required(ErrorMessage="Company Site Category Exception2 is required")]
    		public Int32 CompanySiteCategoryException2Id { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Qtr")]
			[Required(ErrorMessage="Qtr is required")]
    		public Int32 Qtr { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryStandardMetadata))]
	public partial class CompanySiteCategoryStandard
	{
		public sealed class CompanySiteCategoryStandardMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category Standard")]
			[Required(ErrorMessage="Company Site Category Standard is required")]
    		public Int32 CompanySiteCategoryStandardId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Location Sponsor User")]
    		public Int32 LocationSponsorUserId { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Area")]
			[StringLength(100)]
    		public String Area { get; set; }

			[DisplayName("Arp User")]
    		public Int32 ArpUserId { get; set; }

			[DisplayName("Location Spa User")]
    		public Int32 LocationSpaUserId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<User> User2 { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryStandardAuditMetadata))]
	public partial class CompanySiteCategoryStandardAudit
	{
		public sealed class CompanySiteCategoryStandardAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Company Site Category Standard")]
    		public Int32 CompanySiteCategoryStandardId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Location Spa User")]
    		public Int32 LocationSpaUserId { get; set; }

			[DisplayName("Location Sponsor User")]
    		public Int32 LocationSponsorUserId { get; set; }

			[DisplayName("Arp User")]
    		public Int32 ArpUserId { get; set; }

			[DisplayName("Area")]
			[StringLength(100)]
    		public String Area { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryStandardDetailMetadata))]
	public partial class CompanySiteCategoryStandardDetail
	{
		public sealed class CompanySiteCategoryStandardDetailMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category Standard")]
			[Required(ErrorMessage="Company Site Category Standard is required")]
    		public Int32 CompanySiteCategoryStandardId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Location Spa User")]
    		public Int32 LocationSpaUserId { get; set; }

			[DisplayName("Location Sponsor User")]
    		public Int32 LocationSponsorUserId { get; set; }

			[DisplayName("Arp User")]
    		public Int32 ArpUserId { get; set; }

			[DisplayName("Area")]
			[StringLength(100)]
    		public String Area { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Site Name")]
			[Required(ErrorMessage="Site Name is required")]
			[StringLength(50)]
    		public String SiteName { get; set; }

			[DisplayName("Site Abbrev")]
			[StringLength(5)]
    		public String SiteAbbrev { get; set; }

			[DisplayName("Category Name")]
			[StringLength(100)]
    		public String CategoryName { get; set; }

			[DisplayName("Category Desc")]
			[StringLength(255)]
    		public String CategoryDesc { get; set; }

			[DisplayName("Approval Text")]
			[Required(ErrorMessage="Approval Text is required")]
			[StringLength(10)]
    		public String ApprovalText { get; set; }

			[DisplayName("Location Spa User Full Name")]
			[StringLength(202)]
    		public String LocationSpaUserFullName { get; set; }

			[DisplayName("Location Sponsor User Full Name")]
			[StringLength(202)]
    		public String LocationSponsorUserFullName { get; set; }

			[DisplayName("Arp User Full Name")]
    		public String ArpUserFullName { get; set; }

			[DisplayName("CRP Names")]
    		public String CRPNames { get; set; }

			[DisplayName("Crp User Full Names")]
    		public Int32 CrpUserFullNames { get; set; }

			[DisplayName("Approved By User Full Name")]
			[StringLength(202)]
    		public String ApprovedByUserFullName { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryStandardHistoryMetadata))]
	public partial class CompanySiteCategoryStandardHistory
	{
		public sealed class CompanySiteCategoryStandardHistoryMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category Standard History")]
			[Required(ErrorMessage="Company Site Category Standard History is required")]
    		public Int32 CompanySiteCategoryStandardHistoryId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Record Date")]
			[Required(ErrorMessage="Record Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime RecordDate { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanySiteCategoryStandardRegionMetadata))]
	public partial class CompanySiteCategoryStandardRegion
	{
		public sealed class CompanySiteCategoryStandardRegionMetadata //: IEntity
		{
		
			[DisplayName("Company Site Category Standard")]
			[Required(ErrorMessage="Company Site Category Standard is required")]
    		public Int32 CompanySiteCategoryStandardId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Location Sponsor User")]
    		public Int32 LocationSponsorUserId { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Region")]
			[Required(ErrorMessage="Region is required")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site Name")]
			[Required(ErrorMessage="Site Name is required")]
			[StringLength(50)]
    		public String SiteName { get; set; }

			[DisplayName("Site Abbrev")]
			[StringLength(5)]
    		public String SiteAbbrev { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyStatusMetadata))]
	public partial class CompanyStatus
	{
		public sealed class CompanyStatusMetadata //: IEntity
		{
		
			[DisplayName("Company Status")]
			[Required(ErrorMessage="Company Status is required")]
    		public Int32 CompanyStatusId { get; set; }

			[DisplayName("Company Status Name")]
			[Required(ErrorMessage="Company Status Name is required")]
			[StringLength(100)]
    		public String CompanyStatusName { get; set; }

			[DisplayName("Company Status Desc")]
			[StringLength(255)]
    		public String CompanyStatusDesc { get; set; }

			[DisplayName("Ordinal")]
    		public Int32 Ordinal { get; set; }

			[DisplayName("Change Requires Approval")]
			[Required(ErrorMessage="Change Requires Approval is required")]
    		public Boolean ChangeRequiresApproval { get; set; }

    		public EntityCollection<Company> Companies { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals1 { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyStatus2Metadata))]
	public partial class CompanyStatus2
	{
		public sealed class CompanyStatus2Metadata //: IEntity
		{
		
			[DisplayName("Company Status")]
			[Required(ErrorMessage="Company Status is required")]
    		public Int32 CompanyStatusId { get; set; }

			[DisplayName("Company Status Name")]
			[Required(ErrorMessage="Company Status Name is required")]
			[StringLength(50)]
    		public String CompanyStatusName { get; set; }

			[DisplayName("Company Status Desc")]
			[StringLength(255)]
    		public String CompanyStatusDesc { get; set; }

    		public EntityCollection<Company> Companies { get; set; }

    		public EntityCollection<SqExemption> SqExemptions { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyStatusChangeApprovalMetadata))]
	public partial class CompanyStatusChangeApproval
	{
		public sealed class CompanyStatusChangeApprovalMetadata //: IEntity
		{
		
			[DisplayName("Company Status Change Approval")]
			[Required(ErrorMessage="Company Status Change Approval is required")]
    		public Int32 CompanyStatusChangeApprovalId { get; set; }

			[DisplayName("Requested Date")]
			[Required(ErrorMessage="Requested Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime RequestedDate { get; set; }

			[DisplayName("Requested By User")]
			[Required(ErrorMessage="Requested By User is required")]
    		public Int32 RequestedByUserId { get; set; }

			[DisplayName("Comments")]
			[Required(ErrorMessage="Comments is required")]
			[StringLength(1000)]
    		public String Comments { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Current Company Status")]
			[Required(ErrorMessage="Current Company Status is required")]
    		public Int32 CurrentCompanyStatusId { get; set; }

			[DisplayName("Requested Company Status")]
			[Required(ErrorMessage="Requested Company Status is required")]
    		public Int32 RequestedCompanyStatusId { get; set; }

			[DisplayName("Assessed By User")]
    		public Int32 AssessedByUserId { get; set; }

			[DisplayName("Assessed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime AssessedDate { get; set; }

			[DisplayName("Assessor Comments")]
			[StringLength(1000)]
    		public String AssessorComments { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompanyStatus> CompanyStatu { get; set; }

    		public EntityCollection<CompanyStatus> CompanyStatu1 { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(CompanyVendorMetadata))]
	public partial class CompanyVendor
	{
		public sealed class CompanyVendorMetadata //: IEntity
		{
		
			[DisplayName("VENDOR_")]
			[Required(ErrorMessage="VENDOR_ is required")]
    		public Int32 VENDOR_ID { get; set; }

			[DisplayName("VENDOR_NUMBER")]
			[StringLength(50)]
    		public String VENDOR_NUMBER { get; set; }

			[DisplayName("VENDOR_NAME")]
			[Required(ErrorMessage="VENDOR_NAME is required")]
			[StringLength(200)]
    		public String VENDOR_NAME { get; set; }

			[DisplayName("VENDOR_NAME_ALT")]
    		public String VENDOR_NAME_ALT { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[Required(ErrorMessage="LAST_UPDATE_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

			[DisplayName("START_DATE_ACTIVE")]
			[DataType(DataType.DateTime)]
    		public DateTime START_DATE_ACTIVE { get; set; }

			[DisplayName("END_DATE_ACTIVE")]
			[DataType(DataType.DateTime)]
    		public DateTime END_DATE_ACTIVE { get; set; }

			[DisplayName("TAX_REGISTRATION_NUMBER")]
			[StringLength(50)]
    		public String TAX_REGISTRATION_NUMBER { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigMetadata))]
	public partial class Config
	{
		public sealed class ConfigMetadata //: IEntity
		{
		
			[DisplayName("Id")]
			[Required(ErrorMessage="Id is required")]
    		public Int32 Id { get; set; }

			[DisplayName("Key")]
			[Required(ErrorMessage="Key is required")]
			[StringLength(255)]
    		public String Key { get; set; }

			[DisplayName("Value")]
			[StringLength(255)]
    		public String Value { get; set; }

			[DisplayName("Description")]
			[StringLength(255)]
    		public String Description { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigNavigationMetadata))]
	public partial class ConfigNavigation
	{
		public sealed class ConfigNavigationMetadata //: IEntity
		{
		
			[DisplayName("Config Navigation")]
			[Required(ErrorMessage="Config Navigation is required")]
    		public Int32 ConfigNavigationId { get; set; }

			[DisplayName("Parent")]
			[Required(ErrorMessage="Parent is required")]
    		public Int32 ParentId { get; set; }

			[DisplayName("Text")]
			[StringLength(255)]
    		public String Text { get; set; }

			[DisplayName("Text_ Contractor")]
			[StringLength(255)]
    		public String Text_Contractor { get; set; }

			[DisplayName("Navigate URL")]
			[StringLength(255)]
			[DataType(DataType.Url)]
    		public String NavigateURL { get; set; }

			[DisplayName("Navigate URL_ Contractor")]
			[StringLength(255)]
			[DataType(DataType.Url)]
    		public String NavigateURL_Contractor { get; set; }

			[DisplayName("Visible By")]
			[StringLength(255)]
    		public String VisibleBy { get; set; }

			[DisplayName("Display Order")]
    		public Int32 DisplayOrderId { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigSa812Metadata))]
	public partial class ConfigSa812
	{
		public sealed class ConfigSa812Metadata //: IEntity
		{
		
			[DisplayName("Sa812")]
			[Required(ErrorMessage="Sa812 is required")]
    		public Int32 Sa812Id { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Date Training Completed")]
			[Required(ErrorMessage="Date Training Completed is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateTrainingCompleted { get; set; }

			[DisplayName("Date Training Completed2")]
			[DataType(DataType.DateTime)]
    		public DateTime DateTrainingCompleted2 { get; set; }

			[DisplayName("Supervisor User")]
    		public Int32 SupervisorUserId { get; set; }

			[DisplayName("Ehs Consultant")]
    		public Int32 EhsConsultantId { get; set; }

    		public EntityCollection<EHSConsultant> EHSConsultant { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigTextMetadata))]
	public partial class ConfigText
	{
		public sealed class ConfigTextMetadata //: IEntity
		{
		
			[DisplayName("Config Text")]
			[Required(ErrorMessage="Config Text is required")]
    		public Int32 ConfigTextId { get; set; }

			[DisplayName("Config Text Type")]
			[Required(ErrorMessage="Config Text Type is required")]
    		public Int32 ConfigTextTypeId { get; set; }

			[DisplayName("Config Text1")]
			[Required(ErrorMessage="Config Text1 is required")]
			[StringLength(255)]
    		public String ConfigText1 { get; set; }

    		public EntityCollection<ConfigTextType> ConfigTextType { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigText2Metadata))]
	public partial class ConfigText2
	{
		public sealed class ConfigText2Metadata //: IEntity
		{
		
			[DisplayName("Config Text2")]
			[Required(ErrorMessage="Config Text2 is required")]
    		public Int32 ConfigText2Id { get; set; }

			[DisplayName("Config Text Type")]
			[Required(ErrorMessage="Config Text Type is required")]
    		public Int32 ConfigTextTypeId { get; set; }

			[DisplayName("Config Text")]
    		public Byte[] ConfigText { get; set; }

    		public EntityCollection<ConfigTextType> ConfigTextType { get; set; }

		}
	}
	
	[MetadataType(typeof(ConfigTextTypeMetadata))]
	public partial class ConfigTextType
	{
		public sealed class ConfigTextTypeMetadata //: IEntity
		{
		
			[DisplayName("Config Text Type")]
			[Required(ErrorMessage="Config Text Type is required")]
    		public Int32 ConfigTextTypeId { get; set; }

			[DisplayName("Config Text Type1")]
			[Required(ErrorMessage="Config Text Type1 is required")]
			[StringLength(50)]
    		public String ConfigTextType1 { get; set; }

			[DisplayName("Config Text Type Desc")]
			[StringLength(255)]
    		public String ConfigTextTypeDesc { get; set; }

    		public EntityCollection<ConfigText> ConfigTexts { get; set; }

    		public EntityCollection<ConfigText2> ConfigText2 { get; set; }

		}
	}
	
	[MetadataType(typeof(ContactsAlcoaMetadata))]
	public partial class ContactsAlcoa
	{
		public sealed class ContactsAlcoaMetadata //: IEntity
		{
		
			[DisplayName("Contact")]
			[Required(ErrorMessage="Contact is required")]
    		public Int32 ContactId { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Contact First Name")]
			[Required(ErrorMessage="Contact First Name is required")]
			[StringLength(50)]
    		public String ContactFirstName { get; set; }

			[DisplayName("Contact Last Name")]
			[Required(ErrorMessage="Contact Last Name is required")]
			[StringLength(50)]
    		public String ContactLastName { get; set; }

			[DisplayName("Contact Title Position")]
			[StringLength(50)]
    		public String ContactTitlePosition { get; set; }

			[DisplayName("Contact Email")]
			[StringLength(100)]
			[DataType(DataType.EmailAddress)]
    		public String ContactEmail { get; set; }

			[DisplayName("Contact Main No")]
			[StringLength(50)]
    		public String ContactMainNo { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(ContactsContractorMetadata))]
	public partial class ContactsContractor
	{
		public sealed class ContactsContractorMetadata //: IEntity
		{
		
			[DisplayName("Contact")]
			[Required(ErrorMessage="Contact is required")]
    		public Int32 ContactId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Contact First Name")]
			[Required(ErrorMessage="Contact First Name is required")]
			[StringLength(50)]
    		public String ContactFirstName { get; set; }

			[DisplayName("Contact Last Name")]
			[StringLength(50)]
    		public String ContactLastName { get; set; }

			[DisplayName("Contact Role")]
			[StringLength(50)]
    		public String ContactRole { get; set; }

			[DisplayName("Contact Title")]
			[StringLength(50)]
    		public String ContactTitle { get; set; }

			[DisplayName("Contact Email")]
			[StringLength(50)]
			[DataType(DataType.EmailAddress)]
    		public String ContactEmail { get; set; }

			[DisplayName("Contact Mobile")]
			[StringLength(50)]
    		public String ContactMobile { get; set; }

			[DisplayName("Contact Phone")]
			[StringLength(50)]
			[DataType(DataType.PhoneNumber)]
    		public String ContactPhone { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Region> Region { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(ContractReviewsRegisterMetadata))]
	public partial class ContractReviewsRegister
	{
		public sealed class ContractReviewsRegisterMetadata //: IEntity
		{
		
			[DisplayName("Contract")]
			[Required(ErrorMessage="Contract is required")]
    		public Int32 ContractId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Year")]
    		public Int32 Year { get; set; }

			[DisplayName("January")]
    		public Byte January { get; set; }

			[DisplayName("February")]
    		public Byte February { get; set; }

			[DisplayName("March")]
    		public Byte March { get; set; }

			[DisplayName("April")]
    		public Byte April { get; set; }

			[DisplayName("May")]
    		public Byte May { get; set; }

			[DisplayName("June")]
    		public Byte June { get; set; }

			[DisplayName("July")]
    		public Byte July { get; set; }

			[DisplayName("August")]
    		public Byte August { get; set; }

			[DisplayName("September")]
    		public Byte September { get; set; }

			[DisplayName("October")]
    		public Byte October { get; set; }

			[DisplayName("November")]
    		public Byte November { get; set; }

			[DisplayName("December")]
    		public Byte December { get; set; }

			[DisplayName("Year Count")]
    		public Int32 YearCount { get; set; }

			[DisplayName("Location Sponsor User")]
    		public Int32 LocationSponsorUserId { get; set; }

			[DisplayName("Location Spa User")]
    		public Int32 LocationSpaUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By")]
    		public Int32 ModifiedBy { get; set; }

		}
	}
	
	[MetadataType(typeof(cprs_UserMetadata))]
	public partial class cprs_User
	{
		public sealed class cprs_UserMetadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Username")]
			[Required(ErrorMessage="Username is required")]
			[StringLength(200)]
    		public String Username { get; set; }

			[DisplayName("Password")]
			[Required(ErrorMessage="Password is required")]
    		public String Password { get; set; }

			[DisplayName("Disabled")]
			[Required(ErrorMessage="Disabled is required")]
    		public Boolean Disabled { get; set; }

			[DisplayName("Created_ By_")]
			[Required(ErrorMessage="Created_ By_ is required")]
    		public Int32 Created_By_ID { get; set; }

			[DisplayName("Contractor_")]
			[Required(ErrorMessage="Contractor_ is required")]
    		public Int32 Contractor_ID { get; set; }

			[DisplayName("Display Name")]
			[Required(ErrorMessage="Display Name is required")]
    		public String DisplayName { get; set; }

    		public EntityCollection<cprs_User> User11 { get; set; }

    		public EntityCollection<cprs_User> User2 { get; set; }

		}
	}
	
	[MetadataType(typeof(CSAMetadata))]
	public partial class CSA
	{
		public sealed class CSAMetadata //: IEntity
		{
		
			[DisplayName("CSA")]
			[Required(ErrorMessage="CSA is required")]
    		public Int32 CSAId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Date Added")]
			[Required(ErrorMessage="Date Added is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateAdded { get; set; }

			[DisplayName("Date Modified")]
			[Required(ErrorMessage="Date Modified is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateModified { get; set; }

			[DisplayName("Qtr")]
			[Required(ErrorMessage="Qtr is required")]
    		public Int32 QtrId { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int64 Year { get; set; }

			[DisplayName("Total Possible Score")]
    		public Int32 TotalPossibleScore { get; set; }

			[DisplayName("Total Actual Score")]
    		public Int32 TotalActualScore { get; set; }

			[DisplayName("Total Rating")]
    		public Int32 TotalRating { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CSA_Answers> CSA_Answers { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(CSA_AnswersMetadata))]
	public partial class CSA_Answers
	{
		public sealed class CSA_AnswersMetadata //: IEntity
		{
		
			[DisplayName("CSA Answer")]
			[Required(ErrorMessage="CSA Answer is required")]
    		public Int32 CSAAnswerId { get; set; }

			[DisplayName("CSA")]
			[Required(ErrorMessage="CSA is required")]
    		public Int32 CSAId { get; set; }

			[DisplayName("Question No")]
			[Required(ErrorMessage="Question No is required")]
			[StringLength(5)]
    		public String QuestionNo { get; set; }

			[DisplayName("Answer Value")]
    		public Int32 AnswerValue { get; set; }

			[DisplayName("Comment")]
    		public Byte[] Comment { get; set; }

    		public EntityCollection<CSA> CSA { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsAccessMetadata))]
	public partial class CsmsAccess
	{
		public sealed class CsmsAccessMetadata //: IEntity
		{
		
			[DisplayName("Csms Access")]
			[Required(ErrorMessage="Csms Access is required")]
    		public Int32 CsmsAccessId { get; set; }

			[DisplayName("Access Name")]
			[Required(ErrorMessage="Access Name is required")]
			[StringLength(50)]
    		public String AccessName { get; set; }

			[DisplayName("Access Desc")]
			[Required(ErrorMessage="Access Desc is required")]
			[StringLength(255)]
    		public String AccessDesc { get; set; }

    		public EntityCollection<AdminTask> AdminTasks { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsEmailLogMetadata))]
	public partial class CsmsEmailLog
	{
		public sealed class CsmsEmailLogMetadata //: IEntity
		{
		
			[DisplayName("Csms Email Log")]
			[Required(ErrorMessage="Csms Email Log is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailLogId { get; set; }

			[DisplayName("Sent By User")]
			[Required(ErrorMessage="Sent By User is required")]
    		public Int32 SentByUserId { get; set; }

			[DisplayName("Email Sent Date Time")]
			[Required(ErrorMessage="Email Sent Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EmailSentDateTime { get; set; }

			[DisplayName("Subject")]
			[Required(ErrorMessage="Subject is required")]
    		public String Subject { get; set; }

			[DisplayName("Body")]
			[Required(ErrorMessage="Body is required")]
    		public Byte[] Body { get; set; }

    		public EntityCollection<AdminTaskEmailLog> AdminTaskEmailLogs { get; set; }

    		public EntityCollection<CsmsEmailLogRecipient> CsmsEmailLogRecipients { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsEmailLogRecipientMetadata))]
	public partial class CsmsEmailLogRecipient
	{
		public sealed class CsmsEmailLogRecipientMetadata //: IEntity
		{
		
			[DisplayName("Csms Email Recipient")]
			[Required(ErrorMessage="Csms Email Recipient is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailRecipientId { get; set; }

			[DisplayName("Csms Email Log")]
			[Required(ErrorMessage="Csms Email Log is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailLogId { get; set; }

			[DisplayName("Csms Email Recipient Type")]
			[Required(ErrorMessage="Csms Email Recipient Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailRecipientTypeId { get; set; }

			[DisplayName("Recipient Address")]
			[Required(ErrorMessage="Recipient Address is required")]
			[StringLength(255)]
    		public String RecipientAddress { get; set; }

    		public EntityCollection<CsmsEmailLog> CsmsEmailLog { get; set; }

    		public EntityCollection<CsmsEmailRecipientType> CsmsEmailRecipientType { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsEmailRecipientTypeMetadata))]
	public partial class CsmsEmailRecipientType
	{
		public sealed class CsmsEmailRecipientTypeMetadata //: IEntity
		{
		
			[DisplayName("Csms Email Recipient Email Type")]
			[Required(ErrorMessage="Csms Email Recipient Email Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 CsmsEmailRecipientEmailTypeId { get; set; }

			[DisplayName("Type Name")]
			[Required(ErrorMessage="Type Name is required")]
			[StringLength(100)]
    		public String TypeName { get; set; }

			[DisplayName("Type Desc")]
			[Required(ErrorMessage="Type Desc is required")]
			[StringLength(255)]
    		public String TypeDesc { get; set; }

    		public EntityCollection<AdminTaskEmailTemplateRecipient> AdminTaskEmailTemplateRecipients { get; set; }

    		public EntityCollection<CsmsEmailLogRecipient> CsmsEmailLogRecipients { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsFileMetadata))]
	public partial class CsmsFile
	{
		public sealed class CsmsFileMetadata //: IEntity
		{
		
			[DisplayName("Csms File")]
			[Required(ErrorMessage="Csms File is required")]
    		public Int32 CsmsFileId { get; set; }

			[DisplayName("Csms File Type")]
			[Required(ErrorMessage="Csms File Type is required")]
    		public Int32 CsmsFileTypeId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(255)]
    		public String Description { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Is Visible")]
			[Required(ErrorMessage="Is Visible is required")]
    		public Boolean IsVisible { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<AdminTaskEmailTemplateAttachment> AdminTaskEmailTemplateAttachments { get; set; }

    		public EntityCollection<CsmsFileType> CsmsFileType { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(CsmsFileTypeMetadata))]
	public partial class CsmsFileType
	{
		public sealed class CsmsFileTypeMetadata //: IEntity
		{
		
			[DisplayName("Csms File Type")]
			[Required(ErrorMessage="Csms File Type is required")]
    		public Int32 CsmsFileTypeId { get; set; }

			[DisplayName("Csms File Type Name")]
			[Required(ErrorMessage="Csms File Type Name is required")]
			[StringLength(100)]
    		public String CsmsFileTypeName { get; set; }

			[DisplayName("Csms File Type Desc")]
			[Required(ErrorMessage="Csms File Type Desc is required")]
			[StringLength(255)]
    		public String CsmsFileTypeDesc { get; set; }

    		public EntityCollection<CsmsFile> CsmsFiles { get; set; }

		}
	}
	
	[MetadataType(typeof(CustomReportingLayoutMetadata))]
	public partial class CustomReportingLayout
	{
		public sealed class CustomReportingLayoutMetadata //: IEntity
		{
		
			[DisplayName("Report")]
			[Required(ErrorMessage="Report is required")]
    		public Int32 ReportId { get; set; }

			[DisplayName("Area")]
			[Required(ErrorMessage="Area is required")]
			[StringLength(100)]
    		public String Area { get; set; }

			[DisplayName("Report Name")]
			[Required(ErrorMessage="Report Name is required")]
			[StringLength(100)]
    		public String ReportName { get; set; }

			[DisplayName("Report Description")]
			[StringLength(255)]
    		public String ReportDescription { get; set; }

			[DisplayName("Report Layout")]
			[Required(ErrorMessage="Report Layout is required")]
    		public String ReportLayout { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(Dashboard_Companies_NonCompliantMetadata))]
	public partial class Dashboard_Companies_NonCompliant
	{
		public sealed class Dashboard_Companies_NonCompliantMetadata //: IEntity
		{
		
			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Month")]
			[Required(ErrorMessage="Month is required")]
    		public Int32 Month { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("Item Name")]
			[Required(ErrorMessage="Item Name is required")]
			[StringLength(300)]
    		public String ItemName { get; set; }

			[DisplayName("Non Compliant Companies")]
    		public String NonCompliantCompanies { get; set; }

		}
	}
	
	[MetadataType(typeof(DocumentMetadata))]
	public partial class Document
	{
		public sealed class DocumentMetadata //: IEntity
		{
		
			[DisplayName("Document")]
			[Required(ErrorMessage="Document is required")]
    		public Int32 DocumentId { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Document Name")]
			[StringLength(255)]
    		public String DocumentName { get; set; }

			[DisplayName("Document File Name")]
			[Required(ErrorMessage="Document File Name is required")]
			[StringLength(255)]
    		public String DocumentFileName { get; set; }

			[DisplayName("Document File Description")]
			[StringLength(255)]
    		public String DocumentFileDescription { get; set; }

			[DisplayName("Document Type")]
			[Required(ErrorMessage="Document Type is required")]
			[StringLength(255)]
    		public String DocumentType { get; set; }

			[DisplayName("Root Node")]
			[StringLength(255)]
    		public String RootNode { get; set; }

			[DisplayName("Parent Node")]
			[StringLength(255)]
    		public String ParentNode { get; set; }

			[DisplayName("Child Node")]
			[StringLength(255)]
    		public String ChildNode { get; set; }

			[DisplayName("Uploaded File")]
    		public Byte[] UploadedFile { get; set; }

			[DisplayName("Uploaded File2")]
    		public Byte[] UploadedFile2 { get; set; }

			[DisplayName("Uploaded File3")]
    		public Byte[] UploadedFile3 { get; set; }

			[DisplayName("Uploaded File Name")]
			[StringLength(255)]
    		public String UploadedFileName { get; set; }

			[DisplayName("Uploaded File Name2")]
			[StringLength(255)]
    		public String UploadedFileName2 { get; set; }

			[DisplayName("Uploaded File Name3")]
			[StringLength(255)]
    		public String UploadedFileName3 { get; set; }

    		public EntityCollection<Region> Region { get; set; }

		}
	}
	
	[MetadataType(typeof(DocumentsDownloadLogMetadata))]
	public partial class DocumentsDownloadLog
	{
		public sealed class DocumentsDownloadLogMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Document File Name")]
			[StringLength(255)]
    		public String DocumentFileName { get; set; }

			[DisplayName("Downloaded By User")]
			[Required(ErrorMessage="Downloaded By User is required")]
    		public Int32 DownloadedByUserId { get; set; }

			[DisplayName("Downloaded From")]
			[StringLength(50)]
    		public String DownloadedFrom { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiMetadata))]
	public partial class Ebi
	{
		public sealed class EbiMetadata //: IEntity
		{
		
			[DisplayName("Ebi")]
			[Required(ErrorMessage="Ebi is required")]
    		public Int32 EbiId { get; set; }

			[DisplayName("Company Name")]
			[StringLength(255)]
    		public String CompanyName { get; set; }

			[DisplayName("Clock")]
    		public Int32 ClockId { get; set; }

			[DisplayName("Full Name")]
			[StringLength(255)]
    		public String FullName { get; set; }

			[DisplayName("Access Card No")]
    		public Int32 AccessCardNo { get; set; }

			[DisplayName("Swipe Site")]
			[Required(ErrorMessage="Swipe Site is required")]
			[StringLength(255)]
    		public String SwipeSite { get; set; }

			[DisplayName("Data Checked")]
    		public Boolean DataChecked { get; set; }

			[DisplayName("Swipe Date Time")]
			[Required(ErrorMessage="Swipe Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime SwipeDateTime { get; set; }

			[DisplayName("Swipe Year")]
			[Required(ErrorMessage="Swipe Year is required")]
    		public Int32 SwipeYear { get; set; }

			[DisplayName("Swipe Month")]
			[Required(ErrorMessage="Swipe Month is required")]
    		public Int32 SwipeMonth { get; set; }

			[DisplayName("Swipe Day")]
			[Required(ErrorMessage="Swipe Day is required")]
    		public Int32 SwipeDay { get; set; }

			[DisplayName("Vendor_ Number")]
			[StringLength(50)]
    		public String Vendor_Number { get; set; }

			[DisplayName("Source")]
    		public String Source { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiDailyReportMetadata))]
	public partial class EbiDailyReport
	{
		public sealed class EbiDailyReportMetadata //: IEntity
		{
		
			[DisplayName("Ebi Daily Report")]
			[Required(ErrorMessage="Ebi Daily Report is required")]
    		public Int32 EbiDailyReportId { get; set; }

			[DisplayName("Date Time")]
			[Required(ErrorMessage="Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateTime { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(255)]
    		public String CompanyName { get; set; }

			[DisplayName("Company Name Csms")]
			[Required(ErrorMessage="Company Name Csms is required")]
			[StringLength(255)]
    		public String CompanyNameCsms { get; set; }

			[DisplayName("Site Name")]
			[Required(ErrorMessage="Site Name is required")]
			[StringLength(255)]
    		public String SiteName { get; set; }

			[DisplayName("Approved On Site")]
			[Required(ErrorMessage="Approved On Site is required")]
			[StringLength(255)]
    		public String ApprovedOnSite { get; set; }

			[DisplayName("Ebi Issue")]
			[Required(ErrorMessage="Ebi Issue is required")]
    		public Int32 EbiIssueId { get; set; }

			[DisplayName("Sq Valid Till")]
			[StringLength(255)]
    		public String SqValidTill { get; set; }

			[DisplayName("Company Sq Status")]
			[StringLength(255)]
    		public String CompanySqStatus { get; set; }

			[DisplayName("Questionnaire")]
			[StringLength(255)]
    		public String QuestionnaireId { get; set; }

			[DisplayName("Validity")]
			[StringLength(255)]
    		public String Validity { get; set; }

			[DisplayName("Type")]
			[StringLength(255)]
    		public String Type { get; set; }

			[DisplayName("Procurement Contact")]
			[StringLength(255)]
    		public String ProcurementContact { get; set; }

			[DisplayName("HS Assessor")]
			[StringLength(255)]
    		public String HSAssessor { get; set; }

    		public EntityCollection<EbiIssue> EbiIssue { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiGateDetailMetadata))]
	public partial class EbiGateDetail
	{
		public sealed class EbiGateDetailMetadata //: IEntity
		{
		
			[DisplayName("Source")]
			[Required(ErrorMessage="Source is required")]
			[StringLength(200)]
    		public String Source { get; set; }

			[DisplayName("Description")]
			[StringLength(2000)]
    		public String Description { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
			[StringLength(50)]
    		public String Site { get; set; }

			[DisplayName("Is Entry")]
    		public Boolean IsEntry { get; set; }

			[DisplayName("Is Exit")]
    		public Boolean IsExit { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiHoursWorkedMetadata))]
	public partial class EbiHoursWorked
	{
		public sealed class EbiHoursWorkedMetadata //: IEntity
		{
		
			[DisplayName("Ebi HW")]
			[Required(ErrorMessage="Ebi HW is required")]
    		public Int32 EbiHWId { get; set; }

			[DisplayName("Clock")]
    		public Int32 ClockId { get; set; }

			[DisplayName("Entry Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime EntryDateTime { get; set; }

			[DisplayName("Exit Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime ExitDateTime { get; set; }

			[DisplayName("Total Time")]
    		public Decimal TotalTime { get; set; }

			[DisplayName("Year")]
    		public Int32 Year { get; set; }

			[DisplayName("Month")]
    		public Int32 Month { get; set; }

			[DisplayName("Doubt")]
    		public Boolean Doubt { get; set; }

			[DisplayName("Site")]
			[StringLength(255)]
    		public String Site { get; set; }

			[DisplayName("Full Name")]
			[StringLength(255)]
    		public String FullName { get; set; }

			[DisplayName("Vendor_ Number")]
			[StringLength(50)]
    		public String Vendor_Number { get; set; }

			[DisplayName("Access Card No")]
    		public Int32 AccessCardNo { get; set; }

			[DisplayName("Company Name Ebi")]
			[StringLength(255)]
    		public String CompanyNameEbi { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiIssueMetadata))]
	public partial class EbiIssue
	{
		public sealed class EbiIssueMetadata //: IEntity
		{
		
			[DisplayName("Ebi Issue")]
			[Required(ErrorMessage="Ebi Issue is required")]
    		public Int32 EbiIssueId { get; set; }

			[DisplayName("Issue Name")]
			[Required(ErrorMessage="Issue Name is required")]
			[StringLength(100)]
    		public String IssueName { get; set; }

			[DisplayName("Issue Desc")]
			[Required(ErrorMessage="Issue Desc is required")]
			[StringLength(255)]
    		public String IssueDesc { get; set; }

    		public EntityCollection<EbiDailyReport> EbiDailyReports { get; set; }

		}
	}
	
	[MetadataType(typeof(EbiMetricMetadata))]
	public partial class EbiMetric
	{
		public sealed class EbiMetricMetadata //: IEntity
		{
		
			[DisplayName("Ebi Metric")]
			[Required(ErrorMessage="Ebi Metric is required")]
    		public Int32 EbiMetricId { get; set; }

			[DisplayName("Date")]
			[Required(ErrorMessage="Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Date { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Metric1")]
			[Required(ErrorMessage="Metric1 is required")]
    		public Int32 Metric1 { get; set; }

			[DisplayName("Metric2")]
			[Required(ErrorMessage="Metric2 is required")]
    		public Int32 Metric2 { get; set; }

			[DisplayName("Metric3")]
			[Required(ErrorMessage="Metric3 is required")]
    		public Int32 Metric3 { get; set; }

			[DisplayName("Metric4")]
			[Required(ErrorMessage="Metric4 is required")]
    		public Int32 Metric4 { get; set; }

			[DisplayName("Metric5")]
			[Required(ErrorMessage="Metric5 is required")]
    		public Int32 Metric5 { get; set; }

			[DisplayName("Metric6")]
			[Required(ErrorMessage="Metric6 is required")]
    		public Int32 Metric6 { get; set; }

			[DisplayName("Metric7")]
			[Required(ErrorMessage="Metric7 is required")]
    		public Int32 Metric7 { get; set; }

			[DisplayName("Metric8")]
			[Required(ErrorMessage="Metric8 is required")]
    		public Int32 Metric8 { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(EHSConsultantMetadata))]
	public partial class EHSConsultant
	{
		public sealed class EHSConsultantMetadata //: IEntity
		{
		
			[DisplayName("EHS Consultant")]
			[Required(ErrorMessage="EHS Consultant is required")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("Enabled")]
			[Required(ErrorMessage="Enabled is required")]
    		public Boolean Enabled { get; set; }

			[DisplayName("Default")]
    		public Boolean Default { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Level3 User")]
    		public Int32 Level3UserId { get; set; }

			[DisplayName("Level4 User")]
    		public Int32 Level4UserId { get; set; }

			[DisplayName("Level5 User")]
    		public Int32 Level5UserId { get; set; }

			[DisplayName("Level6 User")]
    		public Int32 Level6UserId { get; set; }

			[DisplayName("Level7 User")]
    		public Int32 Level7UserId { get; set; }

			[DisplayName("Level8 User")]
    		public Int32 Level8UserId { get; set; }

			[DisplayName("Level9 User")]
    		public Int32 Level9UserId { get; set; }

			[DisplayName("Supervisor User")]
    		public Int32 SupervisorUserId { get; set; }

    		public EntityCollection<ConfigSa812> ConfigSa812 { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<Site> Sites { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(EhsimsExceptionMetadata))]
	public partial class EhsimsException
	{
		public sealed class EhsimsExceptionMetadata //: IEntity
		{
		
			[DisplayName("Ehsims Exception")]
			[Required(ErrorMessage="Ehsims Exception is required")]
    		public Int32 EhsimsExceptionId { get; set; }

			[DisplayName("Report Date Time")]
			[Required(ErrorMessage="Report Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ReportDateTime { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("kpi Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime kpiDateTime { get; set; }

			[DisplayName("Error Msg")]
			[StringLength(255)]
    		public String ErrorMsg { get; set; }

		}
	}
	
	[MetadataType(typeof(ELMAH_ErrorMetadata))]
	public partial class ELMAH_Error
	{
		public sealed class ELMAH_ErrorMetadata //: IEntity
		{
		
			[DisplayName("Error")]
			[Required(ErrorMessage="Error is required")]
    		public Guid ErrorId { get; set; }

			[DisplayName("Application")]
			[Required(ErrorMessage="Application is required")]
			[StringLength(60)]
    		public String Application { get; set; }

			[DisplayName("Host")]
			[Required(ErrorMessage="Host is required")]
			[StringLength(50)]
    		public String Host { get; set; }

			[DisplayName("Type")]
			[Required(ErrorMessage="Type is required")]
			[StringLength(100)]
    		public String Type { get; set; }

			[DisplayName("Source")]
			[Required(ErrorMessage="Source is required")]
			[StringLength(60)]
    		public String Source { get; set; }

			[DisplayName("Message")]
			[Required(ErrorMessage="Message is required")]
			[StringLength(500)]
    		public String Message { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
			[StringLength(50)]
    		public String User { get; set; }

			[DisplayName("Status Code")]
			[Required(ErrorMessage="Status Code is required")]
    		public Int32 StatusCode { get; set; }

			[DisplayName("Time Utc")]
			[Required(ErrorMessage="Time Utc is required")]
			[DataType(DataType.DateTime)]
    		public DateTime TimeUtc { get; set; }

			[DisplayName("Sequence")]
			[Required(ErrorMessage="Sequence is required")]
    		public Int32 Sequence { get; set; }

			[DisplayName("All Xml")]
			[Required(ErrorMessage="All Xml is required")]
    		public String AllXml { get; set; }

		}
	}
	
	[MetadataType(typeof(EmailLogMetadata))]
	public partial class EmailLog
	{
		public sealed class EmailLogMetadata //: IEntity
		{
		
			[DisplayName("Email Log")]
			[Required(ErrorMessage="Email Log is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailLogId { get; set; }

			[DisplayName("Email Log Type")]
			[Required(ErrorMessage="Email Log Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailLogTypeId { get; set; }

			[DisplayName("Email Date Time")]
			[Required(ErrorMessage="Email Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EmailDateTime { get; set; }

			[DisplayName("Email From")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailFrom { get; set; }

			[DisplayName("Email To")]
			[Required(ErrorMessage="Email To is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailTo { get; set; }

			[DisplayName("Email Cc")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailCc { get; set; }

			[DisplayName("Email Bcc")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailBcc { get; set; }

			[DisplayName("Email Log Message Subject")]
			[Required(ErrorMessage="Email Log Message Subject is required")]
			[DataType(DataType.EmailAddress)]
    		public String EmailLogMessageSubject { get; set; }

			[DisplayName("Email Log Message Body")]
			[DataType(DataType.EmailAddress)]
    		public Byte[] EmailLogMessageBody { get; set; }

			[DisplayName("Sent By User")]
    		public Int32 SentByUserId { get; set; }

			[DisplayName("Config Key")]
			[StringLength(255)]
    		public String ConfigKey { get; set; }

			[DisplayName("Is HTML")]
			[Required(ErrorMessage="Is HTML is required")]
			[DataType(DataType.Html)]
    		public Boolean IsHTML { get; set; }

			[DisplayName("Attachment Path")]
			[StringLength(500)]
    		public String AttachmentPath { get; set; }

			[DisplayName("Attachment File Filter")]
			[StringLength(500)]
    		public String AttachmentFileFilter { get; set; }

			[DisplayName("Handled")]
    		public Boolean Handled { get; set; }

			[DisplayName("Handled Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime HandledDateTime { get; set; }

			[DisplayName("Sent")]
    		public Boolean Sent { get; set; }

    		public EntityCollection<EmailLogType> EmailLogType { get; set; }

		}
	}
	
	[MetadataType(typeof(EmailLogTypeMetadata))]
	public partial class EmailLogType
	{
		public sealed class EmailLogTypeMetadata //: IEntity
		{
		
			[DisplayName("Email Log Type")]
			[Required(ErrorMessage="Email Log Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailLogTypeId { get; set; }

			[DisplayName("Email Log Type Name")]
			[Required(ErrorMessage="Email Log Type Name is required")]
			[StringLength(50)]
			[DataType(DataType.EmailAddress)]
    		public String EmailLogTypeName { get; set; }

			[DisplayName("Email Log Type Desc")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailLogTypeDesc { get; set; }

    		public EntityCollection<EmailLog> EmailLogs { get; set; }

		}
	}
	
	[MetadataType(typeof(EmailTemplateMetadata))]
	public partial class EmailTemplate
	{
		public sealed class EmailTemplateMetadata //: IEntity
		{
		
			[DisplayName("Email Template")]
			[Required(ErrorMessage="Email Template is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailTemplateId { get; set; }

			[DisplayName("Email Template Name")]
			[Required(ErrorMessage="Email Template Name is required")]
			[StringLength(100)]
			[DataType(DataType.EmailAddress)]
    		public String EmailTemplateName { get; set; }

			[DisplayName("Email Template Desc")]
			[Required(ErrorMessage="Email Template Desc is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailTemplateDesc { get; set; }

			[DisplayName("Email Template Subject")]
			[Required(ErrorMessage="Email Template Subject is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String EmailTemplateSubject { get; set; }

			[DisplayName("Email Body")]
			[DataType(DataType.EmailAddress)]
    		public Byte[] EmailBody { get; set; }

    		public EntityCollection<EmailTemplateVariable> EmailTemplateVariables { get; set; }

		}
	}
	
	[MetadataType(typeof(EmailTemplateVariableMetadata))]
	public partial class EmailTemplateVariable
	{
		public sealed class EmailTemplateVariableMetadata //: IEntity
		{
		
			[DisplayName("Email Template Variable")]
			[Required(ErrorMessage="Email Template Variable is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailTemplateVariableId { get; set; }

			[DisplayName("Email Template")]
			[Required(ErrorMessage="Email Template is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailTemplateId { get; set; }

			[DisplayName("Variable Name")]
			[Required(ErrorMessage="Variable Name is required")]
			[StringLength(100)]
    		public String VariableName { get; set; }

			[DisplayName("Variable Description")]
			[Required(ErrorMessage="Variable Description is required")]
			[StringLength(200)]
    		public String VariableDescription { get; set; }

    		public EntityCollection<EmailTemplate> EmailTemplate { get; set; }

		}
	}
	
	[MetadataType(typeof(EnumApprovalMetadata))]
	public partial class EnumApproval
	{
		public sealed class EnumApprovalMetadata //: IEntity
		{
		
			[DisplayName("Enum Approval")]
			[Required(ErrorMessage="Enum Approval is required")]
    		public Int32 EnumApprovalId { get; set; }

			[DisplayName("Approval")]
    		public Boolean Approval { get; set; }

			[DisplayName("Approval Text")]
			[Required(ErrorMessage="Approval Text is required")]
			[StringLength(10)]
    		public String ApprovalText { get; set; }

		}
	}
	
	[MetadataType(typeof(EnumQuestionnaireRiskRatingMetadata))]
	public partial class EnumQuestionnaireRiskRating
	{
		public sealed class EnumQuestionnaireRiskRatingMetadata //: IEntity
		{
		
			[DisplayName("Enum Questionnaire Risk Rating")]
			[Required(ErrorMessage="Enum Questionnaire Risk Rating is required")]
    		public Int32 EnumQuestionnaireRiskRatingId { get; set; }

			[DisplayName("Ordinal")]
			[Required(ErrorMessage="Ordinal is required")]
    		public Int32 Ordinal { get; set; }

			[DisplayName("Rating")]
			[Required(ErrorMessage="Rating is required")]
			[StringLength(255)]
    		public String Rating { get; set; }

		}
	}
	
	[MetadataType(typeof(EscalationChainProcurementMetadata))]
	public partial class EscalationChainProcurement
	{
		public sealed class EscalationChainProcurementMetadata //: IEntity
		{
		
			[DisplayName("Escalation Chain Procurement")]
			[Required(ErrorMessage="Escalation Chain Procurement is required")]
    		public Int32 EscalationChainProcurementId { get; set; }

			[DisplayName("Level")]
			[Required(ErrorMessage="Level is required")]
    		public Int32 Level { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(EscalationChainProcurementRoleMetadata))]
	public partial class EscalationChainProcurementRole
	{
		public sealed class EscalationChainProcurementRoleMetadata //: IEntity
		{
		
			[DisplayName("Escalation Chain Procurement Role")]
			[Required(ErrorMessage="Escalation Chain Procurement Role is required")]
    		public Int32 EscalationChainProcurementRoleId { get; set; }

			[DisplayName("Level")]
			[Required(ErrorMessage="Level is required")]
    		public Int32 Level { get; set; }

			[DisplayName("Role Title")]
			[Required(ErrorMessage="Role Title is required")]
			[StringLength(100)]
    		public String RoleTitle { get; set; }

			[DisplayName("Enabled")]
			[Required(ErrorMessage="Enabled is required")]
    		public Boolean Enabled { get; set; }

			[DisplayName("Contact After Days With")]
    		public Int32 ContactAfterDaysWith { get; set; }

		}
	}
	
	[MetadataType(typeof(EscalationChainSafetyMetadata))]
	public partial class EscalationChainSafety
	{
		public sealed class EscalationChainSafetyMetadata //: IEntity
		{
		
			[DisplayName("Escalation Chain Safety")]
			[Required(ErrorMessage="Escalation Chain Safety is required")]
    		public Int32 EscalationChainSafetyId { get; set; }

			[DisplayName("Level")]
			[Required(ErrorMessage="Level is required")]
    		public Int32 Level { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(EscalationChainSafetyRoleMetadata))]
	public partial class EscalationChainSafetyRole
	{
		public sealed class EscalationChainSafetyRoleMetadata //: IEntity
		{
		
			[DisplayName("Escalation Chain Safety Role")]
			[Required(ErrorMessage="Escalation Chain Safety Role is required")]
    		public Int32 EscalationChainSafetyRoleId { get; set; }

			[DisplayName("Level")]
			[Required(ErrorMessage="Level is required")]
    		public Int32 Level { get; set; }

			[DisplayName("Role Title")]
			[Required(ErrorMessage="Role Title is required")]
			[StringLength(255)]
    		public String RoleTitle { get; set; }

			[DisplayName("Enabled")]
			[Required(ErrorMessage="Enabled is required")]
    		public Boolean Enabled { get; set; }

			[DisplayName("Contact After Days With")]
    		public Int32 ContactAfterDaysWith { get; set; }

		}
	}
	
	[MetadataType(typeof(FatigueManagementDetailMetadata))]
	public partial class FatigueManagementDetail
	{
		public sealed class FatigueManagementDetailMetadata //: IEntity
		{
		
			[DisplayName("Clock")]
			[Required(ErrorMessage="Clock is required")]
    		public Int32 ClockId { get; set; }

			[DisplayName("Total Time")]
    		public Int32 TotalTime { get; set; }

			[DisplayName("Entry Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime EntryDateTime { get; set; }

			[DisplayName("Full Name")]
			[StringLength(255)]
    		public String FullName { get; set; }

			[DisplayName("Vendor_ Number")]
			[StringLength(50)]
    		public String Vendor_Number { get; set; }

			[DisplayName("Rolling Week Hours")]
    		public Int32 RollingWeekHours { get; set; }

			[DisplayName("Comments")]
    		public String Comments { get; set; }

			[DisplayName("Fatigue Entry")]
			[Required(ErrorMessage="Fatigue Entry is required")]
    		public Int32 FatigueEntryId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

		}
	}
	
	[MetadataType(typeof(FileDbMetadata))]
	public partial class FileDb
	{
		public sealed class FileDbMetadata //: IEntity
		{
		
			[DisplayName("File")]
			[Required(ErrorMessage="File is required")]
    		public Int32 FileId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(100)]
    		public String Description { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(100)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Status")]
			[Required(ErrorMessage="Status is required")]
    		public Int32 StatusId { get; set; }

			[DisplayName("Status Comments")]
			[StringLength(100)]
    		public String StatusComments { get; set; }

			[DisplayName("Status Modified By User")]
    		public Int32 StatusModifiedByUserId { get; set; }

			[DisplayName("EHS Consultant")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("Self Assessment_ Response")]
    		public Int32 SelfAssessment_ResponseId { get; set; }

			[DisplayName("Status Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime StatusModifiedDate { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(FileDbAdminMetadata))]
	public partial class FileDbAdmin
	{
		public sealed class FileDbAdminMetadata //: IEntity
		{
		
			[DisplayName("File")]
			[Required(ErrorMessage="File is required")]
    		public Int32 FileId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(100)]
    		public String FileName { get; set; }

			[DisplayName("Description")]
			[StringLength(255)]
    		public String Description { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

		}
	}
	
	[MetadataType(typeof(FileDbAuditMetadata))]
	public partial class FileDbAudit
	{
		public sealed class FileDbAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("File")]
    		public Int32 FileId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Description")]
			[StringLength(100)]
    		public String Description { get; set; }

			[DisplayName("File Name")]
			[StringLength(100)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Status Modified By User")]
    		public Int32 StatusModifiedByUserId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("EHS Consultant")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("Self Assessment_ Response")]
    		public Int32 SelfAssessment_ResponseId { get; set; }

			[DisplayName("Status Comments")]
			[StringLength(100)]
    		public String StatusComments { get; set; }

			[DisplayName("Status")]
    		public Int32 StatusId { get; set; }

			[DisplayName("Status Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime StatusModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(FileDbMedicalTrainingMetadata))]
	public partial class FileDbMedicalTraining
	{
		public sealed class FileDbMedicalTrainingMetadata //: IEntity
		{
		
			[DisplayName("File")]
			[Required(ErrorMessage="File is required")]
    		public Int32 FileId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(100)]
    		public String FileName { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(100)]
    		public String Description { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Type")]
			[Required(ErrorMessage="Type is required")]
			[StringLength(50)]
    		public String Type { get; set; }

			[DisplayName("Comments")]
			[StringLength(255)]
    		public String Comments { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Region")]
			[Required(ErrorMessage="Region is required")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Region> Region { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(FileVaultMetadata))]
	public partial class FileVault
	{
		public sealed class FileVaultMetadata //: IEntity
		{
		
			[DisplayName("File Vault")]
			[Required(ErrorMessage="File Vault is required")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("File Vault Category")]
			[Required(ErrorMessage="File Vault Category is required")]
    		public Int32 FileVaultCategoryId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<FileVaultCategory> FileVaultCategory { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<FileVaultTable> FileVaultTables { get; set; }

    		public EntityCollection<SqExemption> SqExemptions { get; set; }

		}
	}
	
	[MetadataType(typeof(FileVaultAuditMetadata))]
	public partial class FileVaultAudit
	{
		public sealed class FileVaultAuditMetadata //: IEntity
		{
		
			[DisplayName("File Vault Audit")]
			[Required(ErrorMessage="File Vault Audit is required")]
    		public Int32 FileVaultAuditId { get; set; }

			[DisplayName("File Vault")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("File Vault Category")]
    		public Int32 FileVaultCategoryId { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

		}
	}
	
	[MetadataType(typeof(FileVaultCategoryMetadata))]
	public partial class FileVaultCategory
	{
		public sealed class FileVaultCategoryMetadata //: IEntity
		{
		
			[DisplayName("File Vault Category")]
			[Required(ErrorMessage="File Vault Category is required")]
    		public Int32 FileVaultCategoryId { get; set; }

			[DisplayName("Category Name")]
			[Required(ErrorMessage="Category Name is required")]
			[StringLength(100)]
    		public String CategoryName { get; set; }

			[DisplayName("Category Desc")]
			[Required(ErrorMessage="Category Desc is required")]
			[StringLength(255)]
    		public String CategoryDesc { get; set; }

    		public EntityCollection<FileVault> FileVaults { get; set; }

		}
	}
	
	[MetadataType(typeof(FileVaultSubCategoryMetadata))]
	public partial class FileVaultSubCategory
	{
		public sealed class FileVaultSubCategoryMetadata //: IEntity
		{
		
			[DisplayName("File Vault Sub Category")]
			[Required(ErrorMessage="File Vault Sub Category is required")]
    		public Int32 FileVaultSubCategoryId { get; set; }

			[DisplayName("Sub Category Name")]
			[Required(ErrorMessage="Sub Category Name is required")]
			[StringLength(100)]
    		public String SubCategoryName { get; set; }

			[DisplayName("Sub Category Desc")]
			[Required(ErrorMessage="Sub Category Desc is required")]
			[StringLength(100)]
    		public String SubCategoryDesc { get; set; }

    		public EntityCollection<FileVaultTable> FileVaultTables { get; set; }

		}
	}
	
	[MetadataType(typeof(FileVaultTableMetadata))]
	public partial class FileVaultTable
	{
		public sealed class FileVaultTableMetadata //: IEntity
		{
		
			[DisplayName("File Vault Table")]
			[Required(ErrorMessage="File Vault Table is required")]
    		public Int32 FileVaultTableId { get; set; }

			[DisplayName("File Vault")]
			[Required(ErrorMessage="File Vault is required")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("File Vault Sub Category")]
			[Required(ErrorMessage="File Vault Sub Category is required")]
    		public Int32 FileVaultSubCategoryId { get; set; }

			[DisplayName("File Name Custom")]
			[StringLength(255)]
    		public String FileNameCustom { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<FileVault> FileVault { get; set; }

    		public EntityCollection<FileVaultSubCategory> FileVaultSubCategory { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(JobScheduleMetadata))]
	public partial class JobSchedule
	{
		public sealed class JobScheduleMetadata //: IEntity
		{
		
			[DisplayName("Id")]
			[Required(ErrorMessage="Id is required")]
    		public Int32 Id { get; set; }

			[DisplayName("Name")]
			[Required(ErrorMessage="Name is required")]
			[StringLength(200)]
    		public String Name { get; set; }

			[DisplayName("SLA Hours")]
			[Required(ErrorMessage="SLA Hours is required")]
    		public Decimal SLAHours { get; set; }

			[DisplayName("Last Started Date Time")]
			[Required(ErrorMessage="Last Started Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime LastStartedDateTime { get; set; }

			[DisplayName("Is Overdue")]
			[Required(ErrorMessage="Is Overdue is required")]
    		public Int32 IsOverdue { get; set; }

			[DisplayName("Email Sent")]
			[Required(ErrorMessage="Email Sent is required")]
			[DataType(DataType.EmailAddress)]
    		public Boolean EmailSent { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiMetadata))]
	public partial class Kpi
	{
		public sealed class KpiMetadata //: IEntity
		{
		
			[DisplayName("Kpi")]
			[Required(ErrorMessage="Kpi is required")]
    		public Int32 KpiId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Createdby User")]
			[Required(ErrorMessage="Createdby User is required")]
    		public Int32 CreatedbyUserId { get; set; }

			[DisplayName("Modifiedby User")]
			[Required(ErrorMessage="Modifiedby User is required")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Date Added")]
			[Required(ErrorMessage="Date Added is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateAdded { get; set; }

			[DisplayName("Datemodified")]
			[Required(ErrorMessage="Datemodified is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Datemodified { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("kpi Date Time")]
			[Required(ErrorMessage="kpi Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime kpiDateTime { get; set; }

			[DisplayName("kpi General")]
			[Required(ErrorMessage="kpi General is required")]
    		public Decimal kpiGeneral { get; set; }

			[DisplayName("kpi Calciner Expense")]
			[Required(ErrorMessage="kpi Calciner Expense is required")]
    		public Decimal kpiCalcinerExpense { get; set; }

			[DisplayName("kpi Calciner Capital")]
			[Required(ErrorMessage="kpi Calciner Capital is required")]
    		public Decimal kpiCalcinerCapital { get; set; }

			[DisplayName("kpi Residue")]
			[Required(ErrorMessage="kpi Residue is required")]
    		public Decimal kpiResidue { get; set; }

			[DisplayName("project Capital1 Title")]
			[StringLength(100)]
    		public String projectCapital1Title { get; set; }

			[DisplayName("project Capital1 Hours")]
    		public Decimal projectCapital1Hours { get; set; }

			[DisplayName("project Capital2 Title")]
			[StringLength(100)]
    		public String projectCapital2Title { get; set; }

			[DisplayName("project Capital2 Hours")]
    		public Decimal projectCapital2Hours { get; set; }

			[DisplayName("project Capital3 Title")]
			[StringLength(100)]
    		public String projectCapital3Title { get; set; }

			[DisplayName("project Capital3 Hours")]
    		public Decimal projectCapital3Hours { get; set; }

			[DisplayName("project Capital4 Title")]
			[StringLength(100)]
    		public String projectCapital4Title { get; set; }

			[DisplayName("project Capital4 Hours")]
    		public Decimal projectCapital4Hours { get; set; }

			[DisplayName("project Capital5 Title")]
			[StringLength(100)]
    		public String projectCapital5Title { get; set; }

			[DisplayName("project Capital5 Hours")]
    		public Decimal projectCapital5Hours { get; set; }

			[DisplayName("ahea Refinery Work")]
			[Required(ErrorMessage="ahea Refinery Work is required")]
    		public Decimal aheaRefineryWork { get; set; }

			[DisplayName("ahea Residue Work")]
			[Required(ErrorMessage="ahea Residue Work is required")]
    		public Decimal aheaResidueWork { get; set; }

			[DisplayName("ahea Smelting Work")]
    		public Decimal aheaSmeltingWork { get; set; }

			[DisplayName("ahea Power Generation Work")]
    		public Decimal aheaPowerGenerationWork { get; set; }

			[DisplayName("ahea Total Man Hours")]
			[Required(ErrorMessage="ahea Total Man Hours is required")]
    		public Decimal aheaTotalManHours { get; set; }

			[DisplayName("ahea Peak Noppl Site Week")]
			[Required(ErrorMessage="ahea Peak Noppl Site Week is required")]
    		public Int32 aheaPeakNopplSiteWeek { get; set; }

			[DisplayName("ahea Avg Noppl Site Month")]
			[Required(ErrorMessage="ahea Avg Noppl Site Month is required")]
    		public Int32 aheaAvgNopplSiteMonth { get; set; }

			[DisplayName("ahea No Emp Excess Month")]
			[Required(ErrorMessage="ahea No Emp Excess Month is required")]
    		public Int32 aheaNoEmpExcessMonth { get; set; }

			[DisplayName("ip FATI")]
    		public Int32 ipFATI { get; set; }

			[DisplayName("ip MTI")]
    		public Int32 ipMTI { get; set; }

			[DisplayName("ip RDI")]
    		public Int32 ipRDI { get; set; }

			[DisplayName("ip LTI")]
    		public Int32 ipLTI { get; set; }

			[DisplayName("ip IFE")]
    		public Int32 ipIFE { get; set; }

			[DisplayName("ehsp No LWD")]
    		public Decimal ehspNoLWD { get; set; }

			[DisplayName("ehsp No RD")]
    		public Decimal ehspNoRD { get; set; }

			[DisplayName("ehs Corrective")]
    		public Int32 ehsCorrective { get; set; }

			[DisplayName("JSA Audits")]
    		public Int32 JSAAudits { get; set; }

			[DisplayName("i WSC")]
    		public Int32 iWSC { get; set; }

			[DisplayName("o No HSWC")]
    		public Int32 oNoHSWC { get; set; }

			[DisplayName("o No BSP")]
    		public Int32 oNoBSP { get; set; }

			[DisplayName("q QAS")]
    		public Decimal qQAS { get; set; }

			[DisplayName("q No NCI")]
    		public Int32 qNoNCI { get; set; }

			[DisplayName("m Tbmpm")]
    		public Int32 mTbmpm { get; set; }

			[DisplayName("m Awcm")]
    		public Int32 mAwcm { get; set; }

			[DisplayName("m Amcm")]
    		public Int32 mAmcm { get; set; }

			[DisplayName("m Fatality")]
    		public Int32 mFatality { get; set; }

			[DisplayName("Training")]
    		public Int32 Training { get; set; }

			[DisplayName("mt Tolo")]
    		public Int32 mtTolo { get; set; }

			[DisplayName("mt Fp")]
    		public Int32 mtFp { get; set; }

			[DisplayName("mt Elec")]
    		public Int32 mtElec { get; set; }

			[DisplayName("mt Me")]
    		public Int32 mtMe { get; set; }

			[DisplayName("mt Cs")]
    		public Int32 mtCs { get; set; }

			[DisplayName("mt Cb")]
    		public Int32 mtCb { get; set; }

			[DisplayName("mt Ergo")]
    		public Int32 mtErgo { get; set; }

			[DisplayName("mt Ra")]
    		public Int32 mtRa { get; set; }

			[DisplayName("mt Hs")]
    		public Int32 mtHs { get; set; }

			[DisplayName("mt Sp")]
    		public Int32 mtSp { get; set; }

			[DisplayName("mt If")]
    		public Int32 mtIf { get; set; }

			[DisplayName("mt Hp")]
    		public Int32 mtHp { get; set; }

			[DisplayName("mt Rp")]
    		public Int32 mtRp { get; set; }

			[DisplayName("mt ENGINFO81t")]
    		public Int32 mtENGINFO81t { get; set; }

			[DisplayName("mt Others")]
			[StringLength(1000)]
    		public String mtOthers { get; set; }

			[DisplayName("Safety Plans Submitted")]
			[Required(ErrorMessage="Safety Plans Submitted is required")]
    		public Boolean SafetyPlansSubmitted { get; set; }

			[DisplayName("Ebi On Site")]
    		public Boolean EbiOnSite { get; set; }

			[DisplayName("project Capital10 Hours")]
    		public Decimal projectCapital10Hours { get; set; }

			[DisplayName("project Capital10 Title")]
			[StringLength(100)]
    		public String projectCapital10Title { get; set; }

			[DisplayName("project Capital11 Hours")]
    		public Decimal projectCapital11Hours { get; set; }

			[DisplayName("project Capital11 Title")]
			[StringLength(100)]
    		public String projectCapital11Title { get; set; }

			[DisplayName("project Capital12 Hours")]
    		public Decimal projectCapital12Hours { get; set; }

			[DisplayName("project Capital12 Title")]
			[StringLength(100)]
    		public String projectCapital12Title { get; set; }

			[DisplayName("project Capital13 Hours")]
    		public Decimal projectCapital13Hours { get; set; }

			[DisplayName("project Capital13 Title")]
			[StringLength(100)]
    		public String projectCapital13Title { get; set; }

			[DisplayName("project Capital14 Hours")]
    		public Decimal projectCapital14Hours { get; set; }

			[DisplayName("project Capital14 Title")]
			[StringLength(100)]
    		public String projectCapital14Title { get; set; }

			[DisplayName("project Capital15 Hours")]
    		public Decimal projectCapital15Hours { get; set; }

			[DisplayName("project Capital15 Title")]
			[StringLength(100)]
    		public String projectCapital15Title { get; set; }

			[DisplayName("project Capital6 Hours")]
    		public Decimal projectCapital6Hours { get; set; }

			[DisplayName("project Capital6 Title")]
			[StringLength(100)]
    		public String projectCapital6Title { get; set; }

			[DisplayName("project Capital7 Hours")]
    		public Decimal projectCapital7Hours { get; set; }

			[DisplayName("project Capital7 Title")]
			[StringLength(100)]
    		public String projectCapital7Title { get; set; }

			[DisplayName("project Capital8 Hours")]
    		public Decimal projectCapital8Hours { get; set; }

			[DisplayName("project Capital8 Title")]
			[StringLength(100)]
    		public String projectCapital8Title { get; set; }

			[DisplayName("project Capital9 Hours")]
    		public Decimal projectCapital9Hours { get; set; }

			[DisplayName("project Capital9 Title")]
			[StringLength(100)]
    		public String projectCapital9Title { get; set; }

			[DisplayName("Is System Edit")]
    		public Boolean IsSystemEdit { get; set; }

			[DisplayName("project Capital10 Line")]
			[StringLength(5)]
    		public String projectCapital10Line { get; set; }

			[DisplayName("project Capital10 Po")]
			[StringLength(100)]
    		public String projectCapital10Po { get; set; }

			[DisplayName("project Capital11 Line")]
			[StringLength(5)]
    		public String projectCapital11Line { get; set; }

			[DisplayName("project Capital11 Po")]
			[StringLength(100)]
    		public String projectCapital11Po { get; set; }

			[DisplayName("project Capital12 Line")]
			[StringLength(5)]
    		public String projectCapital12Line { get; set; }

			[DisplayName("project Capital12 Po")]
			[StringLength(100)]
    		public String projectCapital12Po { get; set; }

			[DisplayName("project Capital13 Line")]
			[StringLength(5)]
    		public String projectCapital13Line { get; set; }

			[DisplayName("project Capital13 Po")]
			[StringLength(100)]
    		public String projectCapital13Po { get; set; }

			[DisplayName("project Capital14 Line")]
			[StringLength(5)]
    		public String projectCapital14Line { get; set; }

			[DisplayName("project Capital14 Po")]
			[StringLength(100)]
    		public String projectCapital14Po { get; set; }

			[DisplayName("project Capital15 Line")]
			[StringLength(5)]
    		public String projectCapital15Line { get; set; }

			[DisplayName("project Capital15 Po")]
			[StringLength(100)]
    		public String projectCapital15Po { get; set; }

			[DisplayName("project Capital1 Line")]
			[StringLength(5)]
    		public String projectCapital1Line { get; set; }

			[DisplayName("project Capital1 Po")]
			[StringLength(100)]
    		public String projectCapital1Po { get; set; }

			[DisplayName("project Capital2 Line")]
			[StringLength(5)]
    		public String projectCapital2Line { get; set; }

			[DisplayName("project Capital2 Po")]
			[StringLength(100)]
    		public String projectCapital2Po { get; set; }

			[DisplayName("project Capital3 Line")]
			[StringLength(5)]
    		public String projectCapital3Line { get; set; }

			[DisplayName("project Capital3 Po")]
			[StringLength(100)]
    		public String projectCapital3Po { get; set; }

			[DisplayName("project Capital4 Line")]
			[StringLength(5)]
    		public String projectCapital4Line { get; set; }

			[DisplayName("project Capital4 Po")]
			[StringLength(100)]
    		public String projectCapital4Po { get; set; }

			[DisplayName("project Capital5 Line")]
			[StringLength(5)]
    		public String projectCapital5Line { get; set; }

			[DisplayName("project Capital5 Po")]
			[StringLength(100)]
    		public String projectCapital5Po { get; set; }

			[DisplayName("project Capital6 Line")]
			[StringLength(5)]
    		public String projectCapital6Line { get; set; }

			[DisplayName("project Capital6 Po")]
			[StringLength(100)]
    		public String projectCapital6Po { get; set; }

			[DisplayName("project Capital7 Line")]
			[StringLength(5)]
    		public String projectCapital7Line { get; set; }

			[DisplayName("project Capital7 Po")]
			[StringLength(100)]
    		public String projectCapital7Po { get; set; }

			[DisplayName("project Capital8 Line")]
			[StringLength(5)]
    		public String projectCapital8Line { get; set; }

			[DisplayName("project Capital8 Po")]
			[StringLength(100)]
    		public String projectCapital8Po { get; set; }

			[DisplayName("project Capital9 Line")]
			[StringLength(5)]
    		public String projectCapital9Line { get; set; }

			[DisplayName("project Capital9 Po")]
			[StringLength(100)]
    		public String projectCapital9Po { get; set; }

			[DisplayName("ip RN")]
    		public Int32 ipRN { get; set; }

			[DisplayName("ffw Blood Alcohol Tests")]
    		public Int32 ffwBloodAlcoholTests { get; set; }

			[DisplayName("ffw Positive BAC Results")]
    		public Int32 ffwPositiveBACResults { get; set; }

			[DisplayName("ffw Drug Tests")]
    		public Int32 ffwDrugTests { get; set; }

			[DisplayName("ffw Non Negative Results")]
    		public Int32 ffwNonNegativeResults { get; set; }

			[DisplayName("ffw Positive Results Med Declared")]
    		public Int32 ffwPositiveResultsMedDeclared { get; set; }

			[DisplayName("ffw Positive Results Med Not Declared")]
    		public Int32 ffwPositiveResultsMedNotDeclared { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<KpiProject> KpiProjects { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiAuditMetadata))]
	public partial class KpiAudit
	{
		public sealed class KpiAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Kpi")]
    		public Int32 KpiId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Createdby User")]
    		public Int32 CreatedbyUserId { get; set; }

			[DisplayName("Modifiedby User")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Date Added")]
			[DataType(DataType.DateTime)]
    		public DateTime DateAdded { get; set; }

			[DisplayName("Datemodified")]
			[DataType(DataType.DateTime)]
    		public DateTime Datemodified { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("kpi Date Time")]
			[DataType(DataType.DateTime)]
    		public DateTime kpiDateTime { get; set; }

			[DisplayName("kpi General")]
    		public Decimal kpiGeneral { get; set; }

			[DisplayName("kpi Calciner Expense")]
    		public Decimal kpiCalcinerExpense { get; set; }

			[DisplayName("kpi Calciner Capital")]
    		public Decimal kpiCalcinerCapital { get; set; }

			[DisplayName("kpi Residue")]
    		public Decimal kpiResidue { get; set; }

			[DisplayName("project Capital1 Title")]
			[StringLength(100)]
    		public String projectCapital1Title { get; set; }

			[DisplayName("project Capital1 Hours")]
    		public Decimal projectCapital1Hours { get; set; }

			[DisplayName("project Capital2 Title")]
			[StringLength(100)]
    		public String projectCapital2Title { get; set; }

			[DisplayName("project Capital2 Hours")]
    		public Decimal projectCapital2Hours { get; set; }

			[DisplayName("project Capital3 Title")]
			[StringLength(100)]
    		public String projectCapital3Title { get; set; }

			[DisplayName("project Capital3 Hours")]
    		public Decimal projectCapital3Hours { get; set; }

			[DisplayName("project Capital4 Title")]
			[StringLength(100)]
    		public String projectCapital4Title { get; set; }

			[DisplayName("project Capital4 Hours")]
    		public Decimal projectCapital4Hours { get; set; }

			[DisplayName("project Capital5 Title")]
			[StringLength(100)]
    		public String projectCapital5Title { get; set; }

			[DisplayName("project Capital5 Hours")]
    		public Decimal projectCapital5Hours { get; set; }

			[DisplayName("ahea Refinery Work")]
    		public Decimal aheaRefineryWork { get; set; }

			[DisplayName("ahea Residue Work")]
    		public Decimal aheaResidueWork { get; set; }

			[DisplayName("ahea Smelting Work")]
    		public Decimal aheaSmeltingWork { get; set; }

			[DisplayName("ahea Power Generation Work")]
    		public Decimal aheaPowerGenerationWork { get; set; }

			[DisplayName("ahea Total Man Hours")]
    		public Decimal aheaTotalManHours { get; set; }

			[DisplayName("ahea Peak Noppl Site Week")]
    		public Int32 aheaPeakNopplSiteWeek { get; set; }

			[DisplayName("ahea Avg Noppl Site Month")]
    		public Int32 aheaAvgNopplSiteMonth { get; set; }

			[DisplayName("ahea No Emp Excess Month")]
    		public Int32 aheaNoEmpExcessMonth { get; set; }

			[DisplayName("ip FATI")]
    		public Int32 ipFATI { get; set; }

			[DisplayName("ip MTI")]
    		public Int32 ipMTI { get; set; }

			[DisplayName("ip RDI")]
    		public Int32 ipRDI { get; set; }

			[DisplayName("ip LTI")]
    		public Int32 ipLTI { get; set; }

			[DisplayName("ip IFE")]
    		public Int32 ipIFE { get; set; }

			[DisplayName("ehsp No LWD")]
    		public Decimal ehspNoLWD { get; set; }

			[DisplayName("ehsp No RD")]
    		public Decimal ehspNoRD { get; set; }

			[DisplayName("ehs Audits")]
    		public Int32 ehsAudits { get; set; }

			[DisplayName("ehs Corrective")]
    		public Int32 ehsCorrective { get; set; }

			[DisplayName("JSA Audits")]
    		public Int32 JSAAudits { get; set; }

			[DisplayName("i WSC")]
    		public Int32 iWSC { get; set; }

			[DisplayName("o No HSWC")]
    		public Int32 oNoHSWC { get; set; }

			[DisplayName("o No BSP")]
    		public Int32 oNoBSP { get; set; }

			[DisplayName("q QAS")]
    		public Decimal qQAS { get; set; }

			[DisplayName("q No NCI")]
    		public Int32 qNoNCI { get; set; }

			[DisplayName("m Tbmpm")]
    		public Int32 mTbmpm { get; set; }

			[DisplayName("m Awcm")]
    		public Int32 mAwcm { get; set; }

			[DisplayName("m Amcm")]
    		public Int32 mAmcm { get; set; }

			[DisplayName("m Fatality")]
    		public Int32 mFatality { get; set; }

			[DisplayName("Training")]
    		public Int32 Training { get; set; }

			[DisplayName("mt Tolo")]
    		public Int32 mtTolo { get; set; }

			[DisplayName("mt Fp")]
    		public Int32 mtFp { get; set; }

			[DisplayName("mt Elec")]
    		public Int32 mtElec { get; set; }

			[DisplayName("mt Me")]
    		public Int32 mtMe { get; set; }

			[DisplayName("mt Cs")]
    		public Int32 mtCs { get; set; }

			[DisplayName("mt Cb")]
    		public Int32 mtCb { get; set; }

			[DisplayName("mt Ergo")]
    		public Int32 mtErgo { get; set; }

			[DisplayName("mt Ra")]
    		public Int32 mtRa { get; set; }

			[DisplayName("mt Hs")]
    		public Int32 mtHs { get; set; }

			[DisplayName("mt Sp")]
    		public Int32 mtSp { get; set; }

			[DisplayName("mt If")]
    		public Int32 mtIf { get; set; }

			[DisplayName("mt Hp")]
    		public Int32 mtHp { get; set; }

			[DisplayName("mt Rp")]
    		public Int32 mtRp { get; set; }

			[DisplayName("mt ENGINFO81t")]
    		public Int32 mtENGINFO81t { get; set; }

			[DisplayName("mt Others")]
			[StringLength(1000)]
    		public String mtOthers { get; set; }

			[DisplayName("Safety Plans Submitted")]
    		public Boolean SafetyPlansSubmitted { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("project Capital10 Hours")]
    		public Decimal projectCapital10Hours { get; set; }

			[DisplayName("project Capital10 Title")]
			[StringLength(100)]
    		public String projectCapital10Title { get; set; }

			[DisplayName("project Capital11 Hours")]
    		public Decimal projectCapital11Hours { get; set; }

			[DisplayName("project Capital11 Title")]
			[StringLength(100)]
    		public String projectCapital11Title { get; set; }

			[DisplayName("project Capital12 Hours")]
    		public Decimal projectCapital12Hours { get; set; }

			[DisplayName("project Capital12 Title")]
			[StringLength(100)]
    		public String projectCapital12Title { get; set; }

			[DisplayName("project Capital13 Hours")]
    		public Decimal projectCapital13Hours { get; set; }

			[DisplayName("project Capital13 Title")]
			[StringLength(100)]
    		public String projectCapital13Title { get; set; }

			[DisplayName("project Capital14 Hours")]
    		public Decimal projectCapital14Hours { get; set; }

			[DisplayName("project Capital14 Title")]
			[StringLength(100)]
    		public String projectCapital14Title { get; set; }

			[DisplayName("project Capital15 Hours")]
    		public Decimal projectCapital15Hours { get; set; }

			[DisplayName("project Capital15 Title")]
			[StringLength(100)]
    		public String projectCapital15Title { get; set; }

			[DisplayName("project Capital6 Hours")]
    		public Decimal projectCapital6Hours { get; set; }

			[DisplayName("project Capital6 Title")]
			[StringLength(100)]
    		public String projectCapital6Title { get; set; }

			[DisplayName("project Capital7 Hours")]
    		public Decimal projectCapital7Hours { get; set; }

			[DisplayName("project Capital7 Title")]
			[StringLength(100)]
    		public String projectCapital7Title { get; set; }

			[DisplayName("project Capital8 Hours")]
    		public Decimal projectCapital8Hours { get; set; }

			[DisplayName("project Capital8 Title")]
			[StringLength(100)]
    		public String projectCapital8Title { get; set; }

			[DisplayName("project Capital9 Hours")]
    		public Decimal projectCapital9Hours { get; set; }

			[DisplayName("project Capital9 Title")]
			[StringLength(100)]
    		public String projectCapital9Title { get; set; }

			[DisplayName("Ebi On Site")]
    		public Boolean EbiOnSite { get; set; }

			[DisplayName("Is System Edit")]
    		public Boolean IsSystemEdit { get; set; }

			[DisplayName("project Capital10 Line")]
			[StringLength(10)]
    		public String projectCapital10Line { get; set; }

			[DisplayName("project Capital10 Po")]
			[StringLength(100)]
    		public String projectCapital10Po { get; set; }

			[DisplayName("project Capital11 Line")]
			[StringLength(10)]
    		public String projectCapital11Line { get; set; }

			[DisplayName("project Capital11 Po")]
			[StringLength(100)]
    		public String projectCapital11Po { get; set; }

			[DisplayName("project Capital12 Line")]
			[StringLength(10)]
    		public String projectCapital12Line { get; set; }

			[DisplayName("project Capital12 Po")]
			[StringLength(100)]
    		public String projectCapital12Po { get; set; }

			[DisplayName("project Capital13 Line")]
			[StringLength(10)]
    		public String projectCapital13Line { get; set; }

			[DisplayName("project Capital13 Po")]
			[StringLength(100)]
    		public String projectCapital13Po { get; set; }

			[DisplayName("project Capital14 Line")]
			[StringLength(10)]
    		public String projectCapital14Line { get; set; }

			[DisplayName("project Capital14 Po")]
			[StringLength(100)]
    		public String projectCapital14Po { get; set; }

			[DisplayName("project Capital15 Line")]
			[StringLength(10)]
    		public String projectCapital15Line { get; set; }

			[DisplayName("project Capital15 Po")]
			[StringLength(100)]
    		public String projectCapital15Po { get; set; }

			[DisplayName("project Capital1 Line")]
			[StringLength(10)]
    		public String projectCapital1Line { get; set; }

			[DisplayName("project Capital1 Po")]
			[StringLength(100)]
    		public String projectCapital1Po { get; set; }

			[DisplayName("project Capital2 Line")]
			[StringLength(10)]
    		public String projectCapital2Line { get; set; }

			[DisplayName("project Capital2 Po")]
			[StringLength(100)]
    		public String projectCapital2Po { get; set; }

			[DisplayName("project Capital3 Line")]
			[StringLength(10)]
    		public String projectCapital3Line { get; set; }

			[DisplayName("project Capital3 Po")]
			[StringLength(100)]
    		public String projectCapital3Po { get; set; }

			[DisplayName("project Capital4 Line")]
			[StringLength(10)]
    		public String projectCapital4Line { get; set; }

			[DisplayName("project Capital4 Po")]
			[StringLength(100)]
    		public String projectCapital4Po { get; set; }

			[DisplayName("project Capital5 Line")]
			[StringLength(10)]
    		public String projectCapital5Line { get; set; }

			[DisplayName("project Capital5 Po")]
			[StringLength(100)]
    		public String projectCapital5Po { get; set; }

			[DisplayName("project Capital6 Line")]
			[StringLength(10)]
    		public String projectCapital6Line { get; set; }

			[DisplayName("project Capital6 Po")]
			[StringLength(100)]
    		public String projectCapital6Po { get; set; }

			[DisplayName("project Capital7 Line")]
			[StringLength(10)]
    		public String projectCapital7Line { get; set; }

			[DisplayName("project Capital7 Po")]
			[StringLength(100)]
    		public String projectCapital7Po { get; set; }

			[DisplayName("project Capital8 Line")]
			[StringLength(10)]
    		public String projectCapital8Line { get; set; }

			[DisplayName("project Capital8 Po")]
			[StringLength(100)]
    		public String projectCapital8Po { get; set; }

			[DisplayName("project Capital9 Line")]
			[StringLength(10)]
    		public String projectCapital9Line { get; set; }

			[DisplayName("project Capital9 Po")]
			[StringLength(100)]
    		public String projectCapital9Po { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiCompanySiteCategoryMetadata))]
	public partial class KpiCompanySiteCategory
	{
		public sealed class KpiCompanySiteCategoryMetadata //: IEntity
		{
		
			[DisplayName("Kpi")]
			[Required(ErrorMessage="Kpi is required")]
    		public Int32 KpiId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Createdby User")]
			[Required(ErrorMessage="Createdby User is required")]
    		public Int32 CreatedbyUserId { get; set; }

			[DisplayName("Modifiedby User")]
			[Required(ErrorMessage="Modifiedby User is required")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Date Added")]
			[Required(ErrorMessage="Date Added is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateAdded { get; set; }

			[DisplayName("Datemodified")]
			[Required(ErrorMessage="Datemodified is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Datemodified { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("kpi Date Time")]
			[Required(ErrorMessage="kpi Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime kpiDateTime { get; set; }

			[DisplayName("kpi General")]
			[Required(ErrorMessage="kpi General is required")]
    		public Decimal kpiGeneral { get; set; }

			[DisplayName("kpi Calciner Expense")]
			[Required(ErrorMessage="kpi Calciner Expense is required")]
    		public Decimal kpiCalcinerExpense { get; set; }

			[DisplayName("kpi Calciner Capital")]
			[Required(ErrorMessage="kpi Calciner Capital is required")]
    		public Decimal kpiCalcinerCapital { get; set; }

			[DisplayName("kpi Residue")]
			[Required(ErrorMessage="kpi Residue is required")]
    		public Decimal kpiResidue { get; set; }

			[DisplayName("project Capital1 Title")]
			[StringLength(100)]
    		public String projectCapital1Title { get; set; }

			[DisplayName("project Capital1 Hours")]
    		public Decimal projectCapital1Hours { get; set; }

			[DisplayName("project Capital1 Line")]
			[StringLength(5)]
    		public String projectCapital1Line { get; set; }

			[DisplayName("project Capital1 Po")]
			[StringLength(100)]
    		public String projectCapital1Po { get; set; }

			[DisplayName("project Capital2 Title")]
			[StringLength(100)]
    		public String projectCapital2Title { get; set; }

			[DisplayName("project Capital2 Hours")]
    		public Decimal projectCapital2Hours { get; set; }

			[DisplayName("project Capital2 Line")]
			[StringLength(5)]
    		public String projectCapital2Line { get; set; }

			[DisplayName("project Capital2 Po")]
			[StringLength(100)]
    		public String projectCapital2Po { get; set; }

			[DisplayName("project Capital3 Title")]
			[StringLength(100)]
    		public String projectCapital3Title { get; set; }

			[DisplayName("project Capital3 Hours")]
    		public Decimal projectCapital3Hours { get; set; }

			[DisplayName("project Capital3 Line")]
			[StringLength(5)]
    		public String projectCapital3Line { get; set; }

			[DisplayName("project Capital3 Po")]
			[StringLength(100)]
    		public String projectCapital3Po { get; set; }

			[DisplayName("project Capital4 Title")]
			[StringLength(100)]
    		public String projectCapital4Title { get; set; }

			[DisplayName("project Capital4 Hours")]
    		public Decimal projectCapital4Hours { get; set; }

			[DisplayName("project Capital4 Line")]
			[StringLength(5)]
    		public String projectCapital4Line { get; set; }

			[DisplayName("project Capital4 Po")]
			[StringLength(100)]
    		public String projectCapital4Po { get; set; }

			[DisplayName("project Capital5 Title")]
			[StringLength(100)]
    		public String projectCapital5Title { get; set; }

			[DisplayName("project Capital5 Hours")]
    		public Decimal projectCapital5Hours { get; set; }

			[DisplayName("project Capital5 Line")]
			[StringLength(5)]
    		public String projectCapital5Line { get; set; }

			[DisplayName("project Capital5 Po")]
			[StringLength(100)]
    		public String projectCapital5Po { get; set; }

			[DisplayName("project Capital6 Title")]
			[StringLength(100)]
    		public String projectCapital6Title { get; set; }

			[DisplayName("project Capital6 Hours")]
    		public Decimal projectCapital6Hours { get; set; }

			[DisplayName("project Capital6 Line")]
			[StringLength(5)]
    		public String projectCapital6Line { get; set; }

			[DisplayName("project Capital6 Po")]
			[StringLength(100)]
    		public String projectCapital6Po { get; set; }

			[DisplayName("project Capital7 Title")]
			[StringLength(100)]
    		public String projectCapital7Title { get; set; }

			[DisplayName("project Capital7 Hours")]
    		public Decimal projectCapital7Hours { get; set; }

			[DisplayName("project Capital7 Line")]
			[StringLength(5)]
    		public String projectCapital7Line { get; set; }

			[DisplayName("project Capital7 Po")]
			[StringLength(100)]
    		public String projectCapital7Po { get; set; }

			[DisplayName("project Capital8 Title")]
			[StringLength(100)]
    		public String projectCapital8Title { get; set; }

			[DisplayName("project Capital8 Hours")]
    		public Decimal projectCapital8Hours { get; set; }

			[DisplayName("project Capital8 Line")]
			[StringLength(5)]
    		public String projectCapital8Line { get; set; }

			[DisplayName("project Capital8 Po")]
			[StringLength(100)]
    		public String projectCapital8Po { get; set; }

			[DisplayName("project Capital9 Title")]
			[StringLength(100)]
    		public String projectCapital9Title { get; set; }

			[DisplayName("project Capital9 Hours")]
    		public Decimal projectCapital9Hours { get; set; }

			[DisplayName("project Capital9 Line")]
			[StringLength(5)]
    		public String projectCapital9Line { get; set; }

			[DisplayName("project Capital9 Po")]
			[StringLength(100)]
    		public String projectCapital9Po { get; set; }

			[DisplayName("project Capital10 Title")]
			[StringLength(100)]
    		public String projectCapital10Title { get; set; }

			[DisplayName("project Capital10 Hours")]
    		public Decimal projectCapital10Hours { get; set; }

			[DisplayName("project Capital10 Line")]
			[StringLength(5)]
    		public String projectCapital10Line { get; set; }

			[DisplayName("project Capital10 Po")]
			[StringLength(100)]
    		public String projectCapital10Po { get; set; }

			[DisplayName("project Capital11 Title")]
			[StringLength(100)]
    		public String projectCapital11Title { get; set; }

			[DisplayName("project Capital11 Hours")]
    		public Decimal projectCapital11Hours { get; set; }

			[DisplayName("project Capital11 Line")]
			[StringLength(5)]
    		public String projectCapital11Line { get; set; }

			[DisplayName("project Capital11 Po")]
			[StringLength(100)]
    		public String projectCapital11Po { get; set; }

			[DisplayName("project Capital12 Title")]
			[StringLength(100)]
    		public String projectCapital12Title { get; set; }

			[DisplayName("project Capital12 Hours")]
    		public Decimal projectCapital12Hours { get; set; }

			[DisplayName("project Capital12 Line")]
			[StringLength(5)]
    		public String projectCapital12Line { get; set; }

			[DisplayName("project Capital12 Po")]
			[StringLength(100)]
    		public String projectCapital12Po { get; set; }

			[DisplayName("project Capital13 Title")]
			[StringLength(100)]
    		public String projectCapital13Title { get; set; }

			[DisplayName("project Capital13 Hours")]
    		public Decimal projectCapital13Hours { get; set; }

			[DisplayName("project Capital13 Line")]
			[StringLength(5)]
    		public String projectCapital13Line { get; set; }

			[DisplayName("project Capital13 Po")]
			[StringLength(100)]
    		public String projectCapital13Po { get; set; }

			[DisplayName("project Capital14 Title")]
			[StringLength(100)]
    		public String projectCapital14Title { get; set; }

			[DisplayName("project Capital14 Hours")]
    		public Decimal projectCapital14Hours { get; set; }

			[DisplayName("project Capital14 Line")]
			[StringLength(5)]
    		public String projectCapital14Line { get; set; }

			[DisplayName("project Capital14 Po")]
			[StringLength(100)]
    		public String projectCapital14Po { get; set; }

			[DisplayName("project Capital15 Title")]
			[StringLength(100)]
    		public String projectCapital15Title { get; set; }

			[DisplayName("project Capital15 Hours")]
    		public Decimal projectCapital15Hours { get; set; }

			[DisplayName("project Capital15 Line")]
			[StringLength(5)]
    		public String projectCapital15Line { get; set; }

			[DisplayName("project Capital15 Po")]
			[StringLength(100)]
    		public String projectCapital15Po { get; set; }

			[DisplayName("ahea Refinery Work")]
			[Required(ErrorMessage="ahea Refinery Work is required")]
    		public Decimal aheaRefineryWork { get; set; }

			[DisplayName("ahea Residue Work")]
			[Required(ErrorMessage="ahea Residue Work is required")]
    		public Decimal aheaResidueWork { get; set; }

			[DisplayName("ahea Smelting Work")]
    		public Decimal aheaSmeltingWork { get; set; }

			[DisplayName("ahea Power Generation Work")]
    		public Decimal aheaPowerGenerationWork { get; set; }

			[DisplayName("ahea Total Man Hours")]
			[Required(ErrorMessage="ahea Total Man Hours is required")]
    		public Decimal aheaTotalManHours { get; set; }

			[DisplayName("ahea Peak Noppl Site Week")]
			[Required(ErrorMessage="ahea Peak Noppl Site Week is required")]
    		public Int32 aheaPeakNopplSiteWeek { get; set; }

			[DisplayName("ahea Avg Noppl Site Month")]
			[Required(ErrorMessage="ahea Avg Noppl Site Month is required")]
    		public Int32 aheaAvgNopplSiteMonth { get; set; }

			[DisplayName("ahea No Emp Excess Month")]
			[Required(ErrorMessage="ahea No Emp Excess Month is required")]
    		public Int32 aheaNoEmpExcessMonth { get; set; }

			[DisplayName("ip FATI")]
    		public Int32 ipFATI { get; set; }

			[DisplayName("ip MTI")]
    		public Int32 ipMTI { get; set; }

			[DisplayName("ip RDI")]
    		public Int32 ipRDI { get; set; }

			[DisplayName("ip LTI")]
    		public Int32 ipLTI { get; set; }

			[DisplayName("ip IFE")]
    		public Int32 ipIFE { get; set; }

			[DisplayName("ehsp No LWD")]
    		public Decimal ehspNoLWD { get; set; }

			[DisplayName("ehsp No RD")]
    		public Decimal ehspNoRD { get; set; }

			[DisplayName("ehs Corrective")]
    		public Int32 ehsCorrective { get; set; }

			[DisplayName("JSA Audits")]
    		public Int32 JSAAudits { get; set; }

			[DisplayName("i WSC")]
    		public Int32 iWSC { get; set; }

			[DisplayName("o No HSWC")]
    		public Int32 oNoHSWC { get; set; }

			[DisplayName("o No BSP")]
    		public Int32 oNoBSP { get; set; }

			[DisplayName("q QAS")]
    		public Decimal qQAS { get; set; }

			[DisplayName("q No NCI")]
    		public Int32 qNoNCI { get; set; }

			[DisplayName("m Tbmpm")]
    		public Int32 mTbmpm { get; set; }

			[DisplayName("m Awcm")]
    		public Int32 mAwcm { get; set; }

			[DisplayName("m Amcm")]
    		public Int32 mAmcm { get; set; }

			[DisplayName("m Fatality")]
    		public Int32 mFatality { get; set; }

			[DisplayName("Training")]
    		public Int32 Training { get; set; }

			[DisplayName("mt Tolo")]
    		public Int32 mtTolo { get; set; }

			[DisplayName("mt Fp")]
    		public Int32 mtFp { get; set; }

			[DisplayName("mt Elec")]
    		public Int32 mtElec { get; set; }

			[DisplayName("mt Me")]
    		public Int32 mtMe { get; set; }

			[DisplayName("mt Cs")]
    		public Int32 mtCs { get; set; }

			[DisplayName("mt Cb")]
    		public Int32 mtCb { get; set; }

			[DisplayName("mt Ergo")]
    		public Int32 mtErgo { get; set; }

			[DisplayName("mt Ra")]
    		public Int32 mtRa { get; set; }

			[DisplayName("mt Hs")]
    		public Int32 mtHs { get; set; }

			[DisplayName("mt Sp")]
    		public Int32 mtSp { get; set; }

			[DisplayName("mt If")]
    		public Int32 mtIf { get; set; }

			[DisplayName("mt Hp")]
    		public Int32 mtHp { get; set; }

			[DisplayName("mt Rp")]
    		public Int32 mtRp { get; set; }

			[DisplayName("mt ENGINFO81t")]
    		public Int32 mtENGINFO81t { get; set; }

			[DisplayName("mt Others")]
			[StringLength(1000)]
    		public String mtOthers { get; set; }

			[DisplayName("Safety Plans Submitted")]
			[Required(ErrorMessage="Safety Plans Submitted is required")]
    		public Boolean SafetyPlansSubmitted { get; set; }

			[DisplayName("Ebi On Site")]
    		public Boolean EbiOnSite { get; set; }

			[DisplayName("ip RN")]
    		public Int32 ipRN { get; set; }

			[DisplayName("Company Site Category")]
    		public Int32 CompanySiteCategoryId { get; set; }

			[DisplayName("Company Site Category Name")]
			[Required(ErrorMessage="Company Site Category Name is required")]
			[StringLength(50)]
    		public String CompanySiteCategoryName { get; set; }

			[DisplayName("Company Site Category Desc")]
			[Required(ErrorMessage="Company Site Category Desc is required")]
			[StringLength(50)]
    		public String CompanySiteCategoryDesc { get; set; }

			[DisplayName("ffw Blood Alcohol Tests")]
    		public Int32 ffwBloodAlcoholTests { get; set; }

			[DisplayName("ffw Positive BAC Results")]
    		public Int32 ffwPositiveBACResults { get; set; }

			[DisplayName("ffw Drug Tests")]
    		public Int32 ffwDrugTests { get; set; }

			[DisplayName("ffw Non Negative Results")]
    		public Int32 ffwNonNegativeResults { get; set; }

			[DisplayName("ffw Positive Results Med Declared")]
    		public Int32 ffwPositiveResultsMedDeclared { get; set; }

			[DisplayName("ffw Positive Results Med Not Declared")]
    		public Int32 ffwPositiveResultsMedNotDeclared { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiHelpFileMetadata))]
	public partial class KpiHelpFile
	{
		public sealed class KpiHelpFileMetadata //: IEntity
		{
		
			[DisplayName("Kpi Help File")]
			[Required(ErrorMessage="Kpi Help File is required")]
    		public Int32 KpiHelpFileId { get; set; }

			[DisplayName("Kpi Help Caption")]
			[Required(ErrorMessage="Kpi Help Caption is required")]
			[StringLength(100)]
    		public String KpiHelpCaption { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiMeasureMetadata))]
	public partial class KpiMeasure
	{
		public sealed class KpiMeasureMetadata //: IEntity
		{
		
			[DisplayName("Kpi Measure")]
			[Required(ErrorMessage="Kpi Measure is required")]
    		public Int32 KpiMeasureId { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(255)]
    		public String Description { get; set; }

			[DisplayName("Company Site Category")]
			[Required(ErrorMessage="Company Site Category is required")]
    		public Int32 CompanySiteCategoryId { get; set; }

    		public EntityCollection<CompanySiteCategory> CompanySiteCategory { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiProjectMetadata))]
	public partial class KpiProject
	{
		public sealed class KpiProjectMetadata //: IEntity
		{
		
			[DisplayName("Kpi Project")]
			[Required(ErrorMessage="Kpi Project is required")]
    		public Int32 KpiProjectId { get; set; }

			[DisplayName("Kpi")]
			[Required(ErrorMessage="Kpi is required")]
    		public Int32 KpiId { get; set; }

			[DisplayName("Project Title")]
			[Required(ErrorMessage="Project Title is required")]
			[StringLength(255)]
    		public String ProjectTitle { get; set; }

			[DisplayName("Project Hours")]
			[Required(ErrorMessage="Project Hours is required")]
    		public Int32 ProjectHours { get; set; }

    		public EntityCollection<Kpi> Kpi { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiProjectListMetadata))]
	public partial class KpiProjectList
	{
		public sealed class KpiProjectListMetadata //: IEntity
		{
		
			[DisplayName("Kpi Project List")]
			[Required(ErrorMessage="Kpi Project List is required")]
    		public Int32 KpiProjectListId { get; set; }

			[DisplayName("Project Number")]
			[Required(ErrorMessage="Project Number is required")]
			[StringLength(50)]
    		public String ProjectNumber { get; set; }

			[DisplayName("Project Description")]
			[Required(ErrorMessage="Project Description is required")]
			[StringLength(255)]
    		public String ProjectDescription { get; set; }

			[DisplayName("Project Code")]
			[Required(ErrorMessage="Project Code is required")]
			[StringLength(50)]
    		public String ProjectCode { get; set; }

			[DisplayName("Area")]
			[Required(ErrorMessage="Area is required")]
			[StringLength(50)]
    		public String Area { get; set; }

			[DisplayName("Project Status")]
			[Required(ErrorMessage="Project Status is required")]
			[StringLength(50)]
    		public String ProjectStatus { get; set; }

			[DisplayName("Current Project Manager")]
			[Required(ErrorMessage="Current Project Manager is required")]
			[StringLength(50)]
    		public String CurrentProjectManager { get; set; }

			[DisplayName("Phase")]
			[Required(ErrorMessage="Phase is required")]
			[StringLength(50)]
    		public String Phase { get; set; }

			[DisplayName("Original Completion Date")]
			[DataType(DataType.DateTime)]
    		public DateTime OriginalCompletionDate { get; set; }

			[DisplayName("Authorised Date")]
			[DataType(DataType.DateTime)]
    		public DateTime AuthorisedDate { get; set; }

			[DisplayName("Concept Approval Number")]
			[StringLength(50)]
    		public String ConceptApprovalNumber { get; set; }

		}
	}
	
	[MetadataType(typeof(KpiPurchaseOrderListMetadata))]
	public partial class KpiPurchaseOrderList
	{
		public sealed class KpiPurchaseOrderListMetadata //: IEntity
		{
		
			[DisplayName("Kpi Purchase Order List")]
			[Required(ErrorMessage="Kpi Purchase Order List is required")]
    		public Int32 KpiPurchaseOrderListId { get; set; }

			[DisplayName("Purchase Order Number")]
			[Required(ErrorMessage="Purchase Order Number is required")]
			[StringLength(50)]
    		public String PurchaseOrderNumber { get; set; }

			[DisplayName("Purchase Order Line Number")]
			[Required(ErrorMessage="Purchase Order Line Number is required")]
			[StringLength(10)]
    		public String PurchaseOrderLineNumber { get; set; }

			[DisplayName("Project Number")]
			[Required(ErrorMessage="Project Number is required")]
			[StringLength(255)]
    		public String ProjectNumber { get; set; }

			[DisplayName("Project Desc")]
			[StringLength(255)]
    		public String ProjectDesc { get; set; }

			[DisplayName("Area")]
			[Required(ErrorMessage="Area is required")]
			[StringLength(255)]
    		public String Area { get; set; }

			[DisplayName("Project Status")]
			[Required(ErrorMessage="Project Status is required")]
			[StringLength(255)]
    		public String ProjectStatus { get; set; }

			[DisplayName("Task Number")]
			[Required(ErrorMessage="Task Number is required")]
			[StringLength(255)]
    		public String TaskNumber { get; set; }

		}
	}
	
	[MetadataType(typeof(LabourClassTypeMetadata))]
	public partial class LabourClassType
	{
		public sealed class LabourClassTypeMetadata //: IEntity
		{
		
			[DisplayName("Labour Class Type")]
			[Required(ErrorMessage="Labour Class Type is required")]
    		public Int32 LabourClassTypeId { get; set; }

			[DisplayName("Labour Class Type Desc")]
			[Required(ErrorMessage="Labour Class Type Desc is required")]
			[StringLength(30)]
    		public String LabourClassTypeDesc { get; set; }

			[DisplayName("Labour Class Type Is Active")]
			[Required(ErrorMessage="Labour Class Type Is Active is required")]
    		public Boolean LabourClassTypeIsActive { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews { get; set; }

		}
	}
	
	[MetadataType(typeof(LogDetailMetadata))]
	public partial class LogDetail
	{
		public sealed class LogDetailMetadata //: IEntity
		{
		
			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(50)]
    		public String Description { get; set; }

			[DisplayName("Polling Date Time")]
			[Required(ErrorMessage="Polling Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime PollingDateTime { get; set; }

			[DisplayName("Timestamp")]
			[Required(ErrorMessage="Timestamp is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Timestamp { get; set; }

		}
	}
	
	[MetadataType(typeof(MonthMetadata))]
	public partial class Month
	{
		public sealed class MonthMetadata //: IEntity
		{
		
			[DisplayName("Month")]
			[Required(ErrorMessage="Month is required")]
    		public Int32 MonthId { get; set; }

			[DisplayName("Month Name")]
			[Required(ErrorMessage="Month Name is required")]
			[StringLength(50)]
    		public String MonthName { get; set; }

			[DisplayName("Month Abbrev")]
			[StringLength(50)]
    		public String MonthAbbrev { get; set; }

		}
	}
	
	[MetadataType(typeof(NewsMetadata))]
	public partial class News
	{
		public sealed class NewsMetadata //: IEntity
		{
		
			[DisplayName("News")]
			[Required(ErrorMessage="News is required")]
    		public Int32 NewsId { get; set; }

			[DisplayName("Posted On")]
			[Required(ErrorMessage="Posted On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime PostedOn { get; set; }

			[DisplayName("Header")]
			[Required(ErrorMessage="Header is required")]
			[StringLength(100)]
    		public String Header { get; set; }

			[DisplayName("Contents")]
			[StringLength(255)]
    		public String Contents { get; set; }

		}
	}
	
	[MetadataType(typeof(News2Metadata))]
	public partial class News2
	{
		public sealed class News2Metadata //: IEntity
		{
		
			[DisplayName("News2")]
			[Required(ErrorMessage="News2 is required")]
    		public Int32 News2Id { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

		}
	}
	
	[MetadataType(typeof(PermissionMetadata))]
	public partial class Permission
	{
		public sealed class PermissionMetadata //: IEntity
		{
		
			[DisplayName("Permission")]
			[Required(ErrorMessage="Permission is required")]
    		public Int32 PermissionId { get; set; }

			[DisplayName("Permission1")]
			[Required(ErrorMessage="Permission1 is required")]
			[StringLength(100)]
    		public String Permission1 { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(255)]
    		public String Description { get; set; }

		}
	}
	
	[MetadataType(typeof(PollingTableMetadata))]
	public partial class PollingTable
	{
		public sealed class PollingTableMetadata //: IEntity
		{
		
			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(50)]
    		public String Description { get; set; }

			[DisplayName("Polling Date Time")]
			[Required(ErrorMessage="Polling Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime PollingDateTime { get; set; }

		}
	}
	
	[MetadataType(typeof(PrivilegeMetadata))]
	public partial class Privilege
	{
		public sealed class PrivilegeMetadata //: IEntity
		{
		
			[DisplayName("Privilege")]
			[Required(ErrorMessage="Privilege is required")]
    		public Int32 PrivilegeId { get; set; }

			[DisplayName("Privilege Name")]
			[Required(ErrorMessage="Privilege Name is required")]
			[StringLength(50)]
    		public String PrivilegeName { get; set; }

			[DisplayName("Privilege Description")]
			[Required(ErrorMessage="Privilege Description is required")]
			[StringLength(100)]
    		public String PrivilegeDescription { get; set; }

    		public EntityCollection<UserPrivilege> UserPrivileges { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMetadata))]
	public partial class Questionnaire
	{
		public sealed class QuestionnaireMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Status")]
			[Required(ErrorMessage="Status is required")]
    		public Int32 Status { get; set; }

			[DisplayName("Is Re Qualification")]
			[Required(ErrorMessage="Is Re Qualification is required")]
    		public Boolean IsReQualification { get; set; }

			[DisplayName("Is Main Required")]
			[Required(ErrorMessage="Is Main Required is required")]
    		public Boolean IsMainRequired { get; set; }

			[DisplayName("Is Verification Required")]
			[Required(ErrorMessage="Is Verification Required is required")]
    		public Boolean IsVerificationRequired { get; set; }

			[DisplayName("Initial Category High")]
    		public Boolean InitialCategoryHigh { get; set; }

			[DisplayName("Initial Created By User")]
    		public Int32 InitialCreatedByUserId { get; set; }

			[DisplayName("Initial Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialCreatedDate { get; set; }

			[DisplayName("Initial Submitted By User")]
    		public Int32 InitialSubmittedByUserId { get; set; }

			[DisplayName("Initial Submitted Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialSubmittedDate { get; set; }

			[DisplayName("Initial Modified By User")]
    		public Int32 InitialModifiedByUserId { get; set; }

			[DisplayName("Initial Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialModifiedDate { get; set; }

			[DisplayName("Initial Status")]
			[Required(ErrorMessage="Initial Status is required")]
    		public Int32 InitialStatus { get; set; }

			[DisplayName("Initial Risk Assessment")]
			[StringLength(50)]
    		public String InitialRiskAssessment { get; set; }

			[DisplayName("Main Created By User")]
    		public Int32 MainCreatedByUserId { get; set; }

			[DisplayName("Main Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainCreatedDate { get; set; }

			[DisplayName("Main Modified By User")]
    		public Int32 MainModifiedByUserId { get; set; }

			[DisplayName("Main Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainModifiedDate { get; set; }

			[DisplayName("Main Score Expectations")]
    		public Int32 MainScoreExpectations { get; set; }

			[DisplayName("Main Score Overall")]
    		public Int32 MainScoreOverall { get; set; }

			[DisplayName("Main Score Results")]
    		public Int32 MainScoreResults { get; set; }

			[DisplayName("Main Score Staffing")]
    		public Int32 MainScoreStaffing { get; set; }

			[DisplayName("Main Score Systems")]
    		public Int32 MainScoreSystems { get; set; }

			[DisplayName("Main Status")]
			[Required(ErrorMessage="Main Status is required")]
    		public Int32 MainStatus { get; set; }

			[DisplayName("Main Assessment By User")]
    		public Int32 MainAssessmentByUserId { get; set; }

			[DisplayName("Main Assessment Comments")]
    		public String MainAssessmentComments { get; set; }

			[DisplayName("Main Assessment Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentDate { get; set; }

			[DisplayName("Main Assessment Risk Rating")]
			[StringLength(50)]
    		public String MainAssessmentRiskRating { get; set; }

			[DisplayName("Main Assessment Status")]
			[StringLength(50)]
    		public String MainAssessmentStatus { get; set; }

			[DisplayName("Main Assessment Valid To")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentValidTo { get; set; }

			[DisplayName("Main Score P Expectations")]
    		public Int32 MainScorePExpectations { get; set; }

			[DisplayName("Main Score P Overall")]
    		public Int32 MainScorePOverall { get; set; }

			[DisplayName("Main Score P Results")]
    		public Int32 MainScorePResults { get; set; }

			[DisplayName("Main Score P Staffing")]
    		public Int32 MainScorePStaffing { get; set; }

			[DisplayName("Main Score P Systems")]
    		public Int32 MainScorePSystems { get; set; }

			[DisplayName("Verification Created By User")]
    		public Int32 VerificationCreatedByUserId { get; set; }

			[DisplayName("Verification Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationCreatedDate { get; set; }

			[DisplayName("Verification Modified By User")]
    		public Int32 VerificationModifiedByUserId { get; set; }

			[DisplayName("Verification Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationModifiedDate { get; set; }

			[DisplayName("Verification Risk Rating")]
			[StringLength(50)]
    		public String VerificationRiskRating { get; set; }

			[DisplayName("Verification Status")]
			[Required(ErrorMessage="Verification Status is required")]
    		public Int32 VerificationStatus { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Recommended")]
    		public Boolean Recommended { get; set; }

			[DisplayName("Recommended Comments")]
    		public String RecommendedComments { get; set; }

			[DisplayName("Level Of Supervision")]
			[StringLength(15)]
    		public String LevelOfSupervision { get; set; }

			[DisplayName("Procurement Modified")]
			[Required(ErrorMessage="Procurement Modified is required")]
    		public Boolean ProcurementModified { get; set; }

			[DisplayName("Sub Contractor")]
    		public Boolean SubContractor { get; set; }

			[DisplayName("Assessed By User")]
    		public Int32 AssessedByUserId { get; set; }

			[DisplayName("Assessed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime AssessedDate { get; set; }

			[DisplayName("Last Reminder Email Sent On")]
			[DataType(DataType.DateTime)]
    		public DateTime LastReminderEmailSentOn { get; set; }

			[DisplayName("No Reminder Emails Sent")]
			[DataType(DataType.EmailAddress)]
    		public Int32 NoReminderEmailsSent { get; set; }

			[DisplayName("Supplier Contact Verified On")]
			[DataType(DataType.DateTime)]
    		public DateTime SupplierContactVerifiedOn { get; set; }

			[DisplayName("Questionnaire Presently With Action")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Questionnaire Presently With Since")]
			[DataType(DataType.DateTime)]
    		public DateTime QuestionnairePresentlyWithSince { get; set; }

			[DisplayName("Requires Additional Approval")]
    		public Boolean RequiresAdditionalApproval { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<User> User2 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire1 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire2 { get; set; }

    		public EntityCollection<QuestionnaireStatus> QuestionnaireStatu { get; set; }

    		public EntityCollection<QuestionnaireStatus> QuestionnaireStatu1 { get; set; }

    		public EntityCollection<QuestionnaireStatus> QuestionnaireStatu2 { get; set; }

    		public EntityCollection<QuestionnaireStatus> QuestionnaireStatu3 { get; set; }

    		public EntityCollection<User> User3 { get; set; }

    		public EntityCollection<User> User4 { get; set; }

    		public EntityCollection<User> User5 { get; set; }

    		public EntityCollection<User> User6 { get; set; }

    		public EntityCollection<QuestionnaireComment> QuestionnaireComments { get; set; }

    		public EntityCollection<QuestionnaireActionLog> QuestionnaireActionLogs { get; set; }

    		public EntityCollection<QuestionnaireInitialContact> QuestionnaireInitialContacts { get; set; }

    		public EntityCollection<QuestionnaireInitialContactEmail> QuestionnaireInitialContactEmails { get; set; }

    		public EntityCollection<QuestionnaireInitialLocation> QuestionnaireInitialLocations { get; set; }

    		public EntityCollection<QuestionnaireInitialResponse> QuestionnaireInitialResponses { get; set; }

    		public EntityCollection<QuestionnaireMainAssessment> QuestionnaireMainAssessments { get; set; }

    		public EntityCollection<QuestionnaireMainResponse> QuestionnaireMainResponses { get; set; }

    		public EntityCollection<QuestionnaireServicesSelected> QuestionnaireServicesSelecteds { get; set; }

    		public EntityCollection<QuestionnaireVerificationAssessment> QuestionnaireVerificationAssessments { get; set; }

    		public EntityCollection<QuestionnaireVerificationAttachment> QuestionnaireVerificationAttachments { get; set; }

    		public EntityCollection<QuestionnaireVerificationResponse> QuestionnaireVerificationResponses { get; set; }

    		public EntityCollection<QuestionnaireVerificationSection> QuestionnaireVerificationSections { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireActionMetadata))]
	public partial class QuestionnaireAction
	{
		public sealed class QuestionnaireActionMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Action")]
			[Required(ErrorMessage="Questionnaire Action is required")]
    		public Int32 QuestionnaireActionId { get; set; }

			[DisplayName("Action Name")]
			[Required(ErrorMessage="Action Name is required")]
			[StringLength(100)]
    		public String ActionName { get; set; }

			[DisplayName("Action Description")]
			[Required(ErrorMessage="Action Description is required")]
			[StringLength(255)]
    		public String ActionDescription { get; set; }

			[DisplayName("History Log Format")]
			[Required(ErrorMessage="History Log Format is required")]
			[StringLength(255)]
    		public String HistoryLogFormat { get; set; }

			[DisplayName("Log Action")]
			[Required(ErrorMessage="Log Action is required")]
    		public Boolean LogAction { get; set; }

    		public EntityCollection<QuestionnaireActionLog> QuestionnaireActionLogs { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireActionLogMetadata))]
	public partial class QuestionnaireActionLog
	{
		public sealed class QuestionnaireActionLogMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Action Log")]
			[Required(ErrorMessage="Questionnaire Action Log is required")]
    		public Int32 QuestionnaireActionLogId { get; set; }

			[DisplayName("Questionnaire Action")]
			[Required(ErrorMessage="Questionnaire Action is required")]
    		public Int32 QuestionnaireActionId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created By Role")]
			[Required(ErrorMessage="Created By Role is required")]
    		public Int32 CreatedByRoleId { get; set; }

			[DisplayName("Created By Company")]
			[Required(ErrorMessage="Created By Company is required")]
    		public Int32 CreatedByCompanyId { get; set; }

			[DisplayName("Created By Privledged Role")]
			[StringLength(100)]
    		public String CreatedByPrivledgedRole { get; set; }

			[DisplayName("History Log")]
			[Required(ErrorMessage="History Log is required")]
			[StringLength(8000)]
    		public String HistoryLog { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<QuestionnaireAction> QuestionnaireAction { get; set; }

    		public EntityCollection<Role> Role { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireAuditMetadata))]
	public partial class QuestionnaireAudit
	{
		public sealed class QuestionnaireAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Created By User")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Status")]
    		public Int32 Status { get; set; }

			[DisplayName("Is Re Qualification")]
    		public Boolean IsReQualification { get; set; }

			[DisplayName("Is Main Required")]
    		public Boolean IsMainRequired { get; set; }

			[DisplayName("Is Verification Required")]
    		public Boolean IsVerificationRequired { get; set; }

			[DisplayName("Initial Category High")]
    		public Boolean InitialCategoryHigh { get; set; }

			[DisplayName("Initial Created By User")]
    		public Int32 InitialCreatedByUserId { get; set; }

			[DisplayName("Initial Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialCreatedDate { get; set; }

			[DisplayName("Initial Submitted Byuser")]
    		public Int32 InitialSubmittedByuserId { get; set; }

			[DisplayName("Initial Submitted Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialSubmittedDate { get; set; }

			[DisplayName("Initial Modified By User")]
    		public Int32 InitialModifiedByUserId { get; set; }

			[DisplayName("Initial Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialModifiedDate { get; set; }

			[DisplayName("Initial Status")]
    		public Int32 InitialStatus { get; set; }

			[DisplayName("Initial Risk Assessment")]
			[StringLength(50)]
    		public String InitialRiskAssessment { get; set; }

			[DisplayName("Main Created By User")]
    		public Int32 MainCreatedByUserId { get; set; }

			[DisplayName("Main Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainCreatedDate { get; set; }

			[DisplayName("Main Modified By User")]
    		public Int32 MainModifiedByUserId { get; set; }

			[DisplayName("Main Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainModifiedDate { get; set; }

			[DisplayName("Main Score Expectations")]
    		public Int32 MainScoreExpectations { get; set; }

			[DisplayName("Main Score Overall")]
    		public Int32 MainScoreOverall { get; set; }

			[DisplayName("Main Score Results")]
    		public Int32 MainScoreResults { get; set; }

			[DisplayName("Main Score Staffing")]
    		public Int32 MainScoreStaffing { get; set; }

			[DisplayName("Main Score Systems")]
    		public Int32 MainScoreSystems { get; set; }

			[DisplayName("Main Status")]
    		public Int32 MainStatus { get; set; }

			[DisplayName("Main Assessment By User")]
    		public Int32 MainAssessmentByUserId { get; set; }

			[DisplayName("Main Assessment Comments")]
    		public String MainAssessmentComments { get; set; }

			[DisplayName("Main Assessment Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentDate { get; set; }

			[DisplayName("Main Assessment Risk Rating")]
			[StringLength(50)]
    		public String MainAssessmentRiskRating { get; set; }

			[DisplayName("Main Assessment Status")]
			[StringLength(50)]
    		public String MainAssessmentStatus { get; set; }

			[DisplayName("Main Assessment Valid To")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentValidTo { get; set; }

			[DisplayName("Main Score P Expectations")]
    		public Int32 MainScorePExpectations { get; set; }

			[DisplayName("Main Score P Overall")]
    		public Int32 MainScorePOverall { get; set; }

			[DisplayName("Main Score P Results")]
    		public Int32 MainScorePResults { get; set; }

			[DisplayName("Main Score P Staffing")]
    		public Int32 MainScorePStaffing { get; set; }

			[DisplayName("Main Score P Systems")]
    		public Int32 MainScorePSystems { get; set; }

			[DisplayName("Verification Created By User")]
    		public Int32 VerificationCreatedByUserId { get; set; }

			[DisplayName("Verification Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationCreatedDate { get; set; }

			[DisplayName("Verification Modified By User")]
    		public Int32 VerificationModifiedByUserId { get; set; }

			[DisplayName("Verification Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationModifiedDate { get; set; }

			[DisplayName("Verification Risk Rating")]
			[StringLength(50)]
    		public String VerificationRiskRating { get; set; }

			[DisplayName("Verification Status")]
    		public Int32 VerificationStatus { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Recommended")]
    		public Boolean Recommended { get; set; }

			[DisplayName("Recommended Comments")]
    		public String RecommendedComments { get; set; }

			[DisplayName("Level Of Supervision")]
			[StringLength(15)]
    		public String LevelOfSupervision { get; set; }

			[DisplayName("Procurement Modified")]
    		public Boolean ProcurementModified { get; set; }

			[DisplayName("Sub Contractor")]
    		public Boolean SubContractor { get; set; }

			[DisplayName("Assessed By User")]
    		public Int32 AssessedByUserId { get; set; }

			[DisplayName("Assessed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime AssessedDate { get; set; }

			[DisplayName("Last Reminder Email Sent On")]
			[DataType(DataType.DateTime)]
    		public DateTime LastReminderEmailSentOn { get; set; }

			[DisplayName("No Reminder Emails Sent")]
			[DataType(DataType.EmailAddress)]
    		public Int32 NoReminderEmailsSent { get; set; }

			[DisplayName("Supplier Contact Verified On")]
			[DataType(DataType.DateTime)]
    		public DateTime SupplierContactVerifiedOn { get; set; }

			[DisplayName("Questionnaire Presently With Action")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Questionnaire Presently With Since")]
			[DataType(DataType.DateTime)]
    		public DateTime QuestionnairePresentlyWithSince { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireCommentMetadata))]
	public partial class QuestionnaireComment
	{
		public sealed class QuestionnaireCommentMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Comment")]
			[Required(ErrorMessage="Questionnaire Comment is required")]
    		public Int32 QuestionnaireCommentId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Comment")]
			[Required(ErrorMessage="Comment is required")]
			[StringLength(8000)]
    		public String Comment { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireCommentsAuditMetadata))]
	public partial class QuestionnaireCommentsAudit
	{
		public sealed class QuestionnaireCommentsAuditMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Commen Auditt")]
			[Required(ErrorMessage="Questionnaire Commen Auditt is required")]
    		public Int32 QuestionnaireCommenAudittId { get; set; }

			[DisplayName("Questionnaire Comment")]
    		public Int32 QuestionnaireCommentId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Comment")]
			[StringLength(8000)]
    		public String Comment { get; set; }

			[DisplayName("Created By User")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireContractorRsMetadata))]
	public partial class QuestionnaireContractorRs
	{
		public sealed class QuestionnaireContractorRsMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Contractor Rs")]
			[Required(ErrorMessage="Questionnaire Contractor Rs is required")]
    		public Int32 QuestionnaireContractorRsId { get; set; }

			[DisplayName("Contractor Company")]
			[Required(ErrorMessage="Contractor Company is required")]
    		public Int32 ContractorCompanyId { get; set; }

			[DisplayName("Sub Contractor Company")]
			[Required(ErrorMessage="Sub Contractor Company is required")]
    		public Int32 SubContractorCompanyId { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Company> Company1 { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialContactMetadata))]
	public partial class QuestionnaireInitialContact
	{
		public sealed class QuestionnaireInitialContactMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Initial Contact")]
			[Required(ErrorMessage="Questionnaire Initial Contact is required")]
    		public Int32 QuestionnaireInitialContactId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Contact First Name")]
			[Required(ErrorMessage="Contact First Name is required")]
			[StringLength(255)]
    		public String ContactFirstName { get; set; }

			[DisplayName("Contact Last Name")]
			[Required(ErrorMessage="Contact Last Name is required")]
			[StringLength(255)]
    		public String ContactLastName { get; set; }

			[DisplayName("Contact Email")]
			[Required(ErrorMessage="Contact Email is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String ContactEmail { get; set; }

			[DisplayName("Contact Title")]
			[Required(ErrorMessage="Contact Title is required")]
			[StringLength(255)]
    		public String ContactTitle { get; set; }

			[DisplayName("Contact Phone")]
			[Required(ErrorMessage="Contact Phone is required")]
			[StringLength(255)]
			[DataType(DataType.PhoneNumber)]
    		public String ContactPhone { get; set; }

			[DisplayName("Contact Status")]
			[Required(ErrorMessage="Contact Status is required")]
    		public Int32 ContactStatusId { get; set; }

			[DisplayName("Verified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerifiedDate { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<QuestionnaireInitialContactStatus> QuestionnaireInitialContactStatu { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<QuestionnaireInitialContactEmail> QuestionnaireInitialContactEmails { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialContactAuditMetadata))]
	public partial class QuestionnaireInitialContactAudit
	{
		public sealed class QuestionnaireInitialContactAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Questionnaire Initial Contact")]
    		public Int32 QuestionnaireInitialContactId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Contact First Name")]
			[StringLength(255)]
    		public String ContactFirstName { get; set; }

			[DisplayName("Contact Last Name")]
			[StringLength(255)]
    		public String ContactLastName { get; set; }

			[DisplayName("Contact Email")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String ContactEmail { get; set; }

			[DisplayName("Contact Title")]
			[StringLength(255)]
    		public String ContactTitle { get; set; }

			[DisplayName("Contact Phone")]
			[StringLength(255)]
			[DataType(DataType.PhoneNumber)]
    		public String ContactPhone { get; set; }

			[DisplayName("Contact Status")]
    		public Int32 ContactStatusId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Created By User")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Verified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialContactEmailMetadata))]
	public partial class QuestionnaireInitialContactEmail
	{
		public sealed class QuestionnaireInitialContactEmailMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Initial Contact Email")]
			[Required(ErrorMessage="Questionnaire Initial Contact Email is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 QuestionnaireInitialContactEmailId { get; set; }

			[DisplayName("Email Type")]
			[Required(ErrorMessage="Email Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 EmailTypeId { get; set; }

			[DisplayName("Email Sent Date Time")]
			[Required(ErrorMessage="Email Sent Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EmailSentDateTime { get; set; }

			[DisplayName("Contact")]
			[Required(ErrorMessage="Contact is required")]
    		public Int32 ContactId { get; set; }

			[DisplayName("Contact Email")]
			[Required(ErrorMessage="Contact Email is required")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String ContactEmail { get; set; }

			[DisplayName("Subject")]
			[Required(ErrorMessage="Subject is required")]
			[StringLength(255)]
    		public String Subject { get; set; }

			[DisplayName("Message")]
			[Required(ErrorMessage="Message is required")]
    		public String Message { get; set; }

			[DisplayName("Sent By User")]
			[Required(ErrorMessage="Sent By User is required")]
    		public Int32 SentByUserId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<QuestionnaireInitialContact> QuestionnaireInitialContact { get; set; }

    		public EntityCollection<QuestionnaireInitialContactEmailType> QuestionnaireInitialContactEmailType { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialContactEmailTypeMetadata))]
	public partial class QuestionnaireInitialContactEmailType
	{
		public sealed class QuestionnaireInitialContactEmailTypeMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Initial Contact Email Type")]
			[Required(ErrorMessage="Questionnaire Initial Contact Email Type is required")]
			[DataType(DataType.EmailAddress)]
    		public Int32 QuestionnaireInitialContactEmailTypeId { get; set; }

			[DisplayName("Type Name")]
			[Required(ErrorMessage="Type Name is required")]
			[StringLength(255)]
    		public String TypeName { get; set; }

			[DisplayName("Type Desc")]
			[Required(ErrorMessage="Type Desc is required")]
			[StringLength(255)]
    		public String TypeDesc { get; set; }

    		public EntityCollection<QuestionnaireInitialContactEmail> QuestionnaireInitialContactEmails { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialContactStatusMetadata))]
	public partial class QuestionnaireInitialContactStatus
	{
		public sealed class QuestionnaireInitialContactStatusMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Initial Contact Status")]
			[Required(ErrorMessage="Questionnaire Initial Contact Status is required")]
    		public Int32 QuestionnaireInitialContactStatusId { get; set; }

			[DisplayName("Status Name")]
			[Required(ErrorMessage="Status Name is required")]
			[StringLength(255)]
    		public String StatusName { get; set; }

			[DisplayName("Status Desc")]
			[Required(ErrorMessage="Status Desc is required")]
			[StringLength(255)]
    		public String StatusDesc { get; set; }

    		public EntityCollection<QuestionnaireInitialContact> QuestionnaireInitialContacts { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialLocationMetadata))]
	public partial class QuestionnaireInitialLocation
	{
		public sealed class QuestionnaireInitialLocationMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Location")]
			[Required(ErrorMessage="Questionnaire Location is required")]
    		public Int32 QuestionnaireLocationId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("SPA")]
    		public Int32 SPA { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<User> User2 { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialLocationAuditMetadata))]
	public partial class QuestionnaireInitialLocationAudit
	{
		public sealed class QuestionnaireInitialLocationAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Questionnaire Location")]
    		public Int32 QuestionnaireLocationId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("SPA")]
    		public Int32 SPA { get; set; }

			[DisplayName("Approved")]
    		public Boolean Approved { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialResponseMetadata))]
	public partial class QuestionnaireInitialResponse
	{
		public sealed class QuestionnaireInitialResponseMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
			[StringLength(50)]
    		public String QuestionId { get; set; }

			[DisplayName("Answer Text")]
			[Required(ErrorMessage="Answer Text is required")]
    		public String AnswerText { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireInitialResponseAuditMetadata))]
	public partial class QuestionnaireInitialResponseAudit
	{
		public sealed class QuestionnaireInitialResponseAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Question")]
			[StringLength(50)]
    		public String QuestionId { get; set; }

			[DisplayName("Answer Text")]
    		public String AnswerText { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainAnswerMetadata))]
	public partial class QuestionnaireMainAnswer
	{
		public sealed class QuestionnaireMainAnswerMetadata //: IEntity
		{
		
			[DisplayName("Answer")]
			[Required(ErrorMessage="Answer is required")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("Question No")]
			[Required(ErrorMessage="Question No is required")]
			[StringLength(10)]
    		public String QuestionNo { get; set; }

			[DisplayName("Answer No")]
			[StringLength(10)]
    		public String AnswerNo { get; set; }

			[DisplayName("Answer Text")]
    		public String AnswerText { get; set; }

    		public EntityCollection<QuestionnaireMainAttachment> QuestionnaireMainAttachments { get; set; }

    		public EntityCollection<QuestionnaireMainResponse> QuestionnaireMainResponses { get; set; }

    		public EntityCollection<QuestionnaireMainQuestion> QuestionnaireMainQuestion { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainAssessmentMetadata))]
	public partial class QuestionnaireMainAssessment
	{
		public sealed class QuestionnaireMainAssessmentMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Question No")]
			[Required(ErrorMessage="Question No is required")]
			[StringLength(10)]
    		public String QuestionNo { get; set; }

			[DisplayName("Assessor Approval")]
    		public Boolean AssessorApproval { get; set; }

			[DisplayName("Assessor Comment")]
    		public String AssessorComment { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainAssessmentAuditMetadata))]
	public partial class QuestionnaireMainAssessmentAudit
	{
		public sealed class QuestionnaireMainAssessmentAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Question No")]
			[StringLength(10)]
    		public String QuestionNo { get; set; }

			[DisplayName("Assessor Approval")]
    		public Boolean AssessorApproval { get; set; }

			[DisplayName("Assessor Comment")]
    		public String AssessorComment { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainAttachmentMetadata))]
	public partial class QuestionnaireMainAttachment
	{
		public sealed class QuestionnaireMainAttachmentMetadata //: IEntity
		{
		
			[DisplayName("Attachment")]
			[Required(ErrorMessage="Attachment is required")]
    		public Int32 AttachmentId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Answer")]
			[Required(ErrorMessage="Answer is required")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<QuestionnaireMainAnswer> QuestionnaireMainAnswer { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainAttachmentAuditMetadata))]
	public partial class QuestionnaireMainAttachmentAudit
	{
		public sealed class QuestionnaireMainAttachmentAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Attachment")]
    		public Int32 AttachmentId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Answer")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainQuestionMetadata))]
	public partial class QuestionnaireMainQuestion
	{
		public sealed class QuestionnaireMainQuestionMetadata //: IEntity
		{
		
			[DisplayName("Question No")]
			[Required(ErrorMessage="Question No is required")]
			[StringLength(10)]
    		public String QuestionNo { get; set; }

			[DisplayName("Question Text")]
			[Required(ErrorMessage="Question Text is required")]
    		public String QuestionText { get; set; }

    		public EntityCollection<QuestionnaireMainAnswer> QuestionnaireMainAnswers { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainRationaleMetadata))]
	public partial class QuestionnaireMainRationale
	{
		public sealed class QuestionnaireMainRationaleMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Main Rationale")]
			[Required(ErrorMessage="Questionnaire Main Rationale is required")]
    		public Int32 QuestionnaireMainRationaleId { get; set; }

			[DisplayName("Question No")]
			[Required(ErrorMessage="Question No is required")]
			[StringLength(50)]
    		public String QuestionNo { get; set; }

			[DisplayName("Rationale")]
    		public Byte[] Rationale { get; set; }

			[DisplayName("Assistance")]
    		public Byte[] Assistance { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainResponseMetadata))]
	public partial class QuestionnaireMainResponse
	{
		public sealed class QuestionnaireMainResponseMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Answer")]
			[Required(ErrorMessage="Answer is required")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("Answer Text")]
    		public String AnswerText { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<QuestionnaireMainAnswer> QuestionnaireMainAnswer { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireMainResponseAuditMetadata))]
	public partial class QuestionnaireMainResponseAudit
	{
		public sealed class QuestionnaireMainResponseAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Answer")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("Answer Text")]
    		public String AnswerText { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnairePresentlyWithActionMetadata))]
	public partial class QuestionnairePresentlyWithAction
	{
		public sealed class QuestionnairePresentlyWithActionMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Presently With Action")]
			[Required(ErrorMessage="Questionnaire Presently With Action is required")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Action Name")]
			[Required(ErrorMessage="Action Name is required")]
			[StringLength(100)]
    		public String ActionName { get; set; }

			[DisplayName("Action Description")]
			[StringLength(255)]
    		public String ActionDescription { get; set; }

			[DisplayName("Questionnaire Presently With User")]
			[Required(ErrorMessage="Questionnaire Presently With User is required")]
    		public Int32 QuestionnairePresentlyWithUserId { get; set; }

			[DisplayName("Process No")]
    		public Int32 ProcessNo { get; set; }

    		public EntityCollection<QuestionnairePresentlyWithMetric> QuestionnairePresentlyWithMetrics { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnairePresentlyWithMetricMetadata))]
	public partial class QuestionnairePresentlyWithMetric
	{
		public sealed class QuestionnairePresentlyWithMetricMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Presently With Metric")]
			[Required(ErrorMessage="Questionnaire Presently With Metric is required")]
    		public Int32 QuestionnairePresentlyWithMetricId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Questionnaire Presently With Action")]
			[Required(ErrorMessage="Questionnaire Presently With Action is required")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Questionnaire Presently With User")]
			[Required(ErrorMessage="Questionnaire Presently With User is required")]
    		public Int32 QuestionnairePresentlyWithUserId { get; set; }

			[DisplayName("Date")]
			[Required(ErrorMessage="Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Date { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("Week No")]
			[Required(ErrorMessage="Week No is required")]
    		public Int32 WeekNo { get; set; }

			[DisplayName("Metric")]
			[Required(ErrorMessage="Metric is required")]
    		public Int32 Metric { get; set; }

    		public EntityCollection<QuestionnairePresentlyWithAction> QuestionnairePresentlyWithAction { get; set; }

    		public EntityCollection<QuestionnairePresentlyWithUser> QuestionnairePresentlyWithUser { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnairePresentlyWithUserMetadata))]
	public partial class QuestionnairePresentlyWithUser
	{
		public sealed class QuestionnairePresentlyWithUserMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Presently With User")]
			[Required(ErrorMessage="Questionnaire Presently With User is required")]
    		public Int32 QuestionnairePresentlyWithUserId { get; set; }

			[DisplayName("User Name")]
			[Required(ErrorMessage="User Name is required")]
			[StringLength(50)]
    		public String UserName { get; set; }

			[DisplayName("User Description")]
			[StringLength(255)]
    		public String UserDescription { get; set; }

    		public EntityCollection<QuestionnairePresentlyWithMetric> QuestionnairePresentlyWithMetrics { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireReportOverviewMetadata))]
	public partial class QuestionnaireReportOverview
	{
		public sealed class QuestionnaireReportOverviewMetadata //: IEntity
		{
		
			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Company Abn")]
			[Required(ErrorMessage="Company Abn is required")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("Recommended")]
    		public Boolean Recommended { get; set; }

			[DisplayName("Initial Risk Assessment")]
			[StringLength(50)]
    		public String InitialRiskAssessment { get; set; }

			[DisplayName("Main Assessment Risk Rating")]
			[StringLength(50)]
    		public String MainAssessmentRiskRating { get; set; }

			[DisplayName("Final Risk Rating")]
			[StringLength(50)]
    		public String FinalRiskRating { get; set; }

			[DisplayName("Status")]
			[Required(ErrorMessage="Status is required")]
    		public Int32 Status { get; set; }

			[DisplayName("Type")]
			[Required(ErrorMessage="Type is required")]
			[StringLength(14)]
    		public String Type { get; set; }

			[DisplayName("Description Of Work")]
			[StringLength(8000)]
    		public String DescriptionOfWork { get; set; }

			[DisplayName("Level Of Supervision")]
			[StringLength(15)]
    		public String LevelOfSupervision { get; set; }

			[DisplayName("Primary Contractor")]
			[StringLength(10)]
    		public String PrimaryContractor { get; set; }

			[DisplayName("Procurement Contact User")]
    		public Int32 ProcurementContactUserId { get; set; }

			[DisplayName("Contract Manager User")]
    		public Int32 ContractManagerUserId { get; set; }

			[DisplayName("Procurement Contact User")]
			[StringLength(202)]
    		public String ProcurementContactUser { get; set; }

			[DisplayName("Contract Manager User")]
			[StringLength(202)]
    		public String ContractManagerUser { get; set; }

			[DisplayName("Type Of Service")]
    		public Int32 TypeOfService { get; set; }

			[DisplayName("Main Assessment Valid To")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentValidTo { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Approved By User")]
			[StringLength(202)]
    		public String ApprovedByUser { get; set; }

			[DisplayName("Is Main Required")]
			[Required(ErrorMessage="Is Main Required is required")]
    		public Boolean IsMainRequired { get; set; }

			[DisplayName("Is Verification Required")]
			[Required(ErrorMessage="Is Verification Required is required")]
    		public Boolean IsVerificationRequired { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Number Of People")]
			[StringLength(10)]
    		public String NumberOfPeople { get; set; }

			[DisplayName("Last SPQ Date")]
			[DataType(DataType.DateTime)]
    		public DateTime LastSPQDate { get; set; }

			[DisplayName("Initial SPQ Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialSPQDate { get; set; }

			[DisplayName("Active")]
    		public Boolean Active { get; set; }

			[DisplayName("Main Score P Overall")]
    		public Int32 MainScorePOverall { get; set; }

			[DisplayName("Company Status Desc")]
			[StringLength(255)]
    		public String CompanyStatusDesc { get; set; }

			[DisplayName("EHS Consultant")]
    		public Int32 EHSConsultantId { get; set; }

			[DisplayName("Safety Assessor")]
			[StringLength(202)]
    		public String SafetyAssessor { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created By User")]
			[StringLength(202)]
    		public String CreatedByUser { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified By User")]
			[StringLength(202)]
    		public String ModifiedByUser { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Sub Contractor")]
    		public Boolean SubContractor { get; set; }

			[DisplayName("Validity")]
			[StringLength(47)]
    		public String Validity { get; set; }

			[DisplayName("Questionnaire Presently With Action")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Questionnaire Presently With Since")]
			[DataType(DataType.DateTime)]
    		public DateTime QuestionnairePresentlyWithSince { get; set; }

			[DisplayName("Process No")]
    		public Int32 ProcessNo { get; set; }

			[DisplayName("User Name")]
			[StringLength(50)]
    		public String UserName { get; set; }

			[DisplayName("Action Name")]
			[StringLength(100)]
    		public String ActionName { get; set; }

			[DisplayName("User Description")]
			[StringLength(255)]
    		public String UserDescription { get; set; }

			[DisplayName("Action Description")]
			[StringLength(255)]
    		public String ActionDescription { get; set; }

			[DisplayName("Questionnaire Presently With Since Days Count")]
    		public Int32 QuestionnairePresentlyWithSinceDaysCount { get; set; }

			[DisplayName("Days Till Expiry")]
    		public Int32 DaysTillExpiry { get; set; }

			[DisplayName("Traffic Light")]
			[Required(ErrorMessage="Traffic Light is required")]
			[StringLength(6)]
    		public String TrafficLight { get; set; }

			[DisplayName("Company Id_1")]
			[Required(ErrorMessage="Company Id_1 is required")]
    		public Int32 CompanyId_1 { get; set; }

			[DisplayName("KWI")]
			[StringLength(10)]
    		public String KWI { get; set; }

			[DisplayName("KWI_ Spa_ Name")]
			[StringLength(202)]
    		public String KWI_Spa_Name { get; set; }

			[DisplayName("KWI_ Spa")]
    		public Int32 KWI_Spa { get; set; }

			[DisplayName("KWI_ Sponsor_ Name")]
			[StringLength(202)]
    		public String KWI_Sponsor_Name { get; set; }

			[DisplayName("KWI_ Sponsor")]
    		public Int32 KWI_Sponsor { get; set; }

			[DisplayName("KWI_ Company Site Category")]
			[StringLength(255)]
    		public String KWI_CompanySiteCategory { get; set; }

			[DisplayName("PIN")]
			[StringLength(10)]
    		public String PIN { get; set; }

			[DisplayName("PIN_ Spa_ Name")]
			[StringLength(202)]
    		public String PIN_Spa_Name { get; set; }

			[DisplayName("PIN_ Spa")]
    		public Int32 PIN_Spa { get; set; }

			[DisplayName("PIN_ Sponsor_ Name")]
			[StringLength(202)]
    		public String PIN_Sponsor_Name { get; set; }

			[DisplayName("PIN_ Sponsor")]
    		public Int32 PIN_Sponsor { get; set; }

			[DisplayName("PIN_ Company Site Category")]
			[StringLength(255)]
    		public String PIN_CompanySiteCategory { get; set; }

			[DisplayName("WGP")]
			[StringLength(10)]
    		public String WGP { get; set; }

			[DisplayName("WGP_ Spa_ Name")]
			[StringLength(202)]
    		public String WGP_Spa_Name { get; set; }

			[DisplayName("WGP_ Spa")]
    		public Int32 WGP_Spa { get; set; }

			[DisplayName("WGP_ Sponsor_ Name")]
			[StringLength(202)]
    		public String WGP_Sponsor_Name { get; set; }

			[DisplayName("WGP_ Sponsor")]
    		public Int32 WGP_Sponsor { get; set; }

			[DisplayName("WGP_ Company Site Category")]
			[StringLength(255)]
    		public String WGP_CompanySiteCategory { get; set; }

			[DisplayName("HUN")]
			[StringLength(10)]
    		public String HUN { get; set; }

			[DisplayName("HUN_ Spa_ Name")]
			[StringLength(202)]
    		public String HUN_Spa_Name { get; set; }

			[DisplayName("HUN_ Spa")]
    		public Int32 HUN_Spa { get; set; }

			[DisplayName("HUN_ Sponsor_ Name")]
			[StringLength(202)]
    		public String HUN_Sponsor_Name { get; set; }

			[DisplayName("HUN_ Sponsor")]
    		public Int32 HUN_Sponsor { get; set; }

			[DisplayName("HUN_ Company Site Category")]
			[StringLength(255)]
    		public String HUN_CompanySiteCategory { get; set; }

			[DisplayName("WDL")]
			[StringLength(10)]
    		public String WDL { get; set; }

			[DisplayName("WDL_ Spa_ Name")]
			[StringLength(202)]
    		public String WDL_Spa_Name { get; set; }

			[DisplayName("WDL_ Spa")]
    		public Int32 WDL_Spa { get; set; }

			[DisplayName("WDL_ Sponsor_ Name")]
			[StringLength(202)]
    		public String WDL_Sponsor_Name { get; set; }

			[DisplayName("WDL_ Sponsor")]
    		public Int32 WDL_Sponsor { get; set; }

			[DisplayName("WDL_ Company Site Category")]
			[StringLength(255)]
    		public String WDL_CompanySiteCategory { get; set; }

			[DisplayName("BUN")]
			[StringLength(10)]
    		public String BUN { get; set; }

			[DisplayName("BUN_ Spa_ Name")]
			[StringLength(202)]
    		public String BUN_Spa_Name { get; set; }

			[DisplayName("BUN_ Spa")]
    		public Int32 BUN_Spa { get; set; }

			[DisplayName("BUN_ Sponsor_ Name")]
			[StringLength(202)]
    		public String BUN_Sponsor_Name { get; set; }

			[DisplayName("BUN_ Sponsor")]
    		public Int32 BUN_Sponsor { get; set; }

			[DisplayName("BUN_ Company Site Category")]
			[StringLength(255)]
    		public String BUN_CompanySiteCategory { get; set; }

			[DisplayName("FML")]
			[StringLength(10)]
    		public String FML { get; set; }

			[DisplayName("FML_ Spa_ Name")]
			[StringLength(202)]
    		public String FML_Spa_Name { get; set; }

			[DisplayName("FML_ Spa")]
    		public Int32 FML_Spa { get; set; }

			[DisplayName("FML_ Sponsor_ Name")]
			[StringLength(202)]
    		public String FML_Sponsor_Name { get; set; }

			[DisplayName("FML_ Sponsor")]
    		public Int32 FML_Sponsor { get; set; }

			[DisplayName("FML_ Company Site Category")]
			[StringLength(255)]
    		public String FML_CompanySiteCategory { get; set; }

			[DisplayName("BGN")]
			[StringLength(10)]
    		public String BGN { get; set; }

			[DisplayName("BGN_ Spa_ Name")]
			[StringLength(202)]
    		public String BGN_Spa_Name { get; set; }

			[DisplayName("BGN_ Spa")]
    		public Int32 BGN_Spa { get; set; }

			[DisplayName("BGN_ Sponsor_ Name")]
			[StringLength(202)]
    		public String BGN_Sponsor_Name { get; set; }

			[DisplayName("BGN_ Sponsor")]
    		public Int32 BGN_Sponsor { get; set; }

			[DisplayName("BGN_ Company Site Category")]
			[StringLength(255)]
    		public String BGN_CompanySiteCategory { get; set; }

			[DisplayName("CE")]
			[StringLength(10)]
    		public String CE { get; set; }

			[DisplayName("CE_ Spa_ Name")]
			[StringLength(202)]
    		public String CE_Spa_Name { get; set; }

			[DisplayName("CE_ Spa")]
    		public Int32 CE_Spa { get; set; }

			[DisplayName("CE_ Sponsor_ Name")]
			[StringLength(202)]
    		public String CE_Sponsor_Name { get; set; }

			[DisplayName("CE_ Sponsor")]
    		public Int32 CE_Sponsor { get; set; }

			[DisplayName("CE_ Company Site Category")]
			[StringLength(255)]
    		public String CE_CompanySiteCategory { get; set; }

			[DisplayName("ANG")]
			[StringLength(10)]
    		public String ANG { get; set; }

			[DisplayName("ANG_ Spa_ Name")]
			[StringLength(202)]
    		public String ANG_Spa_Name { get; set; }

			[DisplayName("ANG_ Spa")]
    		public Int32 ANG_Spa { get; set; }

			[DisplayName("ANG_ Sponsor_ Name")]
			[StringLength(202)]
    		public String ANG_Sponsor_Name { get; set; }

			[DisplayName("ANG_ Sponsor")]
    		public Int32 ANG_Sponsor { get; set; }

			[DisplayName("ANG_ Company Site Category")]
			[StringLength(255)]
    		public String ANG_CompanySiteCategory { get; set; }

			[DisplayName("PTD")]
			[StringLength(10)]
    		public String PTD { get; set; }

			[DisplayName("PTD_ Spa_ Name")]
			[StringLength(202)]
    		public String PTD_Spa_Name { get; set; }

			[DisplayName("PTD_ Spa")]
    		public Int32 PTD_Spa { get; set; }

			[DisplayName("PTD_ Sponsor_ Name")]
			[StringLength(202)]
    		public String PTD_Sponsor_Name { get; set; }

			[DisplayName("PTD_ Sponsor")]
    		public Int32 PTD_Sponsor { get; set; }

			[DisplayName("PTD_ Company Site Category")]
			[StringLength(255)]
    		public String PTD_CompanySiteCategory { get; set; }

			[DisplayName("PTH")]
			[StringLength(10)]
    		public String PTH { get; set; }

			[DisplayName("PTH_ Spa_ Name")]
			[StringLength(202)]
    		public String PTH_Spa_Name { get; set; }

			[DisplayName("PTH_ Spa")]
    		public Int32 PTH_Spa { get; set; }

			[DisplayName("PTH_ Sponsor_ Name")]
			[StringLength(202)]
    		public String PTH_Sponsor_Name { get; set; }

			[DisplayName("PTH_ Sponsor")]
    		public Int32 PTH_Sponsor { get; set; }

			[DisplayName("PTH_ Company Site Category")]
			[StringLength(255)]
    		public String PTH_CompanySiteCategory { get; set; }

			[DisplayName("PNJ")]
			[StringLength(10)]
    		public String PNJ { get; set; }

			[DisplayName("PNJ_ Spa_ Name")]
			[StringLength(202)]
    		public String PNJ_Spa_Name { get; set; }

			[DisplayName("PNJ_ Spa")]
    		public Int32 PNJ_Spa { get; set; }

			[DisplayName("PNJ_ Sponsor_ Name")]
			[StringLength(202)]
    		public String PNJ_Sponsor_Name { get; set; }

			[DisplayName("PNJ_ Sponsor")]
    		public Int32 PNJ_Sponsor { get; set; }

			[DisplayName("PNJ_ Company Site Category")]
			[StringLength(255)]
    		public String PNJ_CompanySiteCategory { get; set; }

			[DisplayName("KPH")]
			[StringLength(10)]
    		public String KPH { get; set; }

			[DisplayName("KPH_ Spa_ Name")]
			[StringLength(202)]
    		public String KPH_Spa_Name { get; set; }

			[DisplayName("KPH_ Spa")]
    		public Int32 KPH_Spa { get; set; }

			[DisplayName("KPH_ Sponsor_ Name")]
			[StringLength(202)]
    		public String KPH_Sponsor_Name { get; set; }

			[DisplayName("KPH_ Sponsor")]
    		public Int32 KPH_Sponsor { get; set; }

			[DisplayName("KPH_ Company Site Category")]
			[StringLength(255)]
    		public String KPH_CompanySiteCategory { get; set; }

			[DisplayName("YEN")]
			[StringLength(10)]
    		public String YEN { get; set; }

			[DisplayName("YEN_ Spa_ Name")]
			[StringLength(202)]
    		public String YEN_Spa_Name { get; set; }

			[DisplayName("YEN_ Spa")]
    		public Int32 YEN_Spa { get; set; }

			[DisplayName("YEN_ Sponsor_ Name")]
			[StringLength(202)]
    		public String YEN_Sponsor_Name { get; set; }

			[DisplayName("YEN_ Sponsor")]
    		public Int32 YEN_Sponsor { get; set; }

			[DisplayName("YEN_ Company Site Category")]
			[StringLength(255)]
    		public String YEN_CompanySiteCategory { get; set; }

			[DisplayName("KWF")]
			[StringLength(10)]
    		public String KWF { get; set; }

			[DisplayName("KWF_ Spa_ Name")]
			[StringLength(202)]
    		public String KWF_Spa_Name { get; set; }

			[DisplayName("KWF_ Spa")]
    		public Int32 KWF_Spa { get; set; }

			[DisplayName("KWF_ Sponsor_ Name")]
			[StringLength(202)]
    		public String KWF_Sponsor_Name { get; set; }

			[DisplayName("KWF_ Sponsor")]
    		public Int32 KWF_Sponsor { get; set; }

			[DisplayName("KWF_ Company Site Category")]
			[StringLength(255)]
    		public String KWF_CompanySiteCategory { get; set; }

			[DisplayName("Company Id_2")]
			[Required(ErrorMessage="Company Id_2 is required")]
    		public Int32 CompanyId_2 { get; set; }

			[DisplayName("KWI_ARP_ Name")]
    		public String KWI_ARP_Name { get; set; }

			[DisplayName("KWI_CRP_ Name")]
    		public String KWI_CRP_Name { get; set; }

			[DisplayName("PIN_ARP_ Name")]
    		public String PIN_ARP_Name { get; set; }

			[DisplayName("PIN_CRP_ Name")]
    		public Int32 PIN_CRP_Name { get; set; }

			[DisplayName("WGP_ARP_ Name")]
    		public String WGP_ARP_Name { get; set; }

			[DisplayName("WGP_CRP_ Name")]
    		public Int32 WGP_CRP_Name { get; set; }

			[DisplayName("HUN_ARP_ Name")]
    		public String HUN_ARP_Name { get; set; }

			[DisplayName("HUN_CRP_ Name")]
    		public Int32 HUN_CRP_Name { get; set; }

			[DisplayName("WDL_ARP_ Name")]
    		public String WDL_ARP_Name { get; set; }

			[DisplayName("WDL_CRP_ Name")]
    		public Int32 WDL_CRP_Name { get; set; }

			[DisplayName("BUN_ARP_ Name")]
    		public String BUN_ARP_Name { get; set; }

			[DisplayName("BUN_CRP_ Name")]
    		public Int32 BUN_CRP_Name { get; set; }

			[DisplayName("FML_ARP_ Name")]
    		public String FML_ARP_Name { get; set; }

			[DisplayName("FML_CRP_ Name")]
    		public Int32 FML_CRP_Name { get; set; }

			[DisplayName("BGN_ARP_ Name")]
    		public String BGN_ARP_Name { get; set; }

			[DisplayName("BGN_CRP_ Name")]
    		public Int32 BGN_CRP_Name { get; set; }

			[DisplayName("CE_ARP_ Name")]
    		public String CE_ARP_Name { get; set; }

			[DisplayName("CE_CRP_ Name")]
    		public Int32 CE_CRP_Name { get; set; }

			[DisplayName("ANG_ARP_ Name")]
    		public String ANG_ARP_Name { get; set; }

			[DisplayName("ANG_CRP_ Name")]
    		public Int32 ANG_CRP_Name { get; set; }

			[DisplayName("PTD_ARP_ Name")]
    		public String PTD_ARP_Name { get; set; }

			[DisplayName("PTD_CRP_ Name")]
    		public Int32 PTD_CRP_Name { get; set; }

			[DisplayName("PTH_ARP_ Name")]
    		public String PTH_ARP_Name { get; set; }

			[DisplayName("PTH_CRP_ Name")]
    		public Int32 PTH_CRP_Name { get; set; }

			[DisplayName("PNJ_ARP_ Name")]
    		public String PNJ_ARP_Name { get; set; }

			[DisplayName("PNJ_CRP_ Name")]
    		public Int32 PNJ_CRP_Name { get; set; }

			[DisplayName("KPH_ARP_ Name")]
    		public String KPH_ARP_Name { get; set; }

			[DisplayName("KPH_CRP_ Name")]
    		public Int32 KPH_CRP_Name { get; set; }

			[DisplayName("YEN_ARP_ Name")]
    		public String YEN_ARP_Name { get; set; }

			[DisplayName("YEN_CRP_ Name")]
    		public Int32 YEN_CRP_Name { get; set; }

			[DisplayName("Requesting Company")]
    		public Int32 RequestingCompanyId { get; set; }

			[DisplayName("Requesting Company Name")]
			[StringLength(200)]
    		public String RequestingCompanyName { get; set; }

			[DisplayName("Reason Contractor")]
			[StringLength(30)]
    		public String ReasonContractor { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireServicesCategoryMetadata))]
	public partial class QuestionnaireServicesCategory
	{
		public sealed class QuestionnaireServicesCategoryMetadata //: IEntity
		{
		
			[DisplayName("Category")]
			[Required(ErrorMessage="Category is required")]
    		public Int32 CategoryId { get; set; }

			[DisplayName("Category Text")]
			[Required(ErrorMessage="Category Text is required")]
			[StringLength(255)]
    		public String CategoryText { get; set; }

			[DisplayName("Category Desc")]
			[StringLength(255)]
    		public String CategoryDesc { get; set; }

			[DisplayName("Visible")]
			[Required(ErrorMessage="Visible is required")]
    		public Boolean Visible { get; set; }

			[DisplayName("High Risk")]
    		public Boolean HighRisk { get; set; }

    		public EntityCollection<QuestionnaireServicesSelected> QuestionnaireServicesSelecteds { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireServicesSelectedMetadata))]
	public partial class QuestionnaireServicesSelected
	{
		public sealed class QuestionnaireServicesSelectedMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Service")]
			[Required(ErrorMessage="Questionnaire Service is required")]
    		public Int32 QuestionnaireServiceId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Category")]
			[Required(ErrorMessage="Category is required")]
    		public Int32 CategoryId { get; set; }

			[DisplayName("Questionnaire Type")]
			[Required(ErrorMessage="Questionnaire Type is required")]
    		public Int32 QuestionnaireTypeId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<QuestionnaireServicesCategory> QuestionnaireServicesCategory { get; set; }

    		public EntityCollection<QuestionnaireType> QuestionnaireType { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireServicesSelectedAuditMetadata))]
	public partial class QuestionnaireServicesSelectedAudit
	{
		public sealed class QuestionnaireServicesSelectedAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Questionnaire Service")]
    		public Int32 QuestionnaireServiceId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Category")]
    		public Int32 CategoryId { get; set; }

			[DisplayName("Questionnaire Type")]
    		public Int32 QuestionnaireTypeId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireStatusMetadata))]
	public partial class QuestionnaireStatus
	{
		public sealed class QuestionnaireStatusMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Status")]
			[Required(ErrorMessage="Questionnaire Status is required")]
    		public Int32 QuestionnaireStatusId { get; set; }

			[DisplayName("Questionnaire Status Name")]
			[Required(ErrorMessage="Questionnaire Status Name is required")]
			[StringLength(50)]
    		public String QuestionnaireStatusName { get; set; }

			[DisplayName("Questionnaire Status Desc")]
			[Required(ErrorMessage="Questionnaire Status Desc is required")]
			[StringLength(255)]
    		public String QuestionnaireStatusDesc { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires1 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires2 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires3 { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireTypeMetadata))]
	public partial class QuestionnaireType
	{
		public sealed class QuestionnaireTypeMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Type")]
			[Required(ErrorMessage="Questionnaire Type is required")]
    		public Int32 QuestionnaireTypeId { get; set; }

			[DisplayName("Questionnaire Type Name")]
			[Required(ErrorMessage="Questionnaire Type Name is required")]
			[StringLength(255)]
    		public String QuestionnaireTypeName { get; set; }

			[DisplayName("Questionnaire Type Desc")]
			[Required(ErrorMessage="Questionnaire Type Desc is required")]
			[StringLength(255)]
    		public String QuestionnaireTypeDesc { get; set; }

    		public EntityCollection<QuestionnaireServicesSelected> QuestionnaireServicesSelecteds { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationAssessmentMetadata))]
	public partial class QuestionnaireVerificationAssessment
	{
		public sealed class QuestionnaireVerificationAssessmentMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
			[Required(ErrorMessage="Section is required")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Assessor Approval")]
    		public Boolean AssessorApproval { get; set; }

			[DisplayName("Assessor Comment")]
    		public String AssessorComment { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationAssessmentAuditMetadata))]
	public partial class QuestionnaireVerificationAssessmentAudit
	{
		public sealed class QuestionnaireVerificationAssessmentAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Assessor Approval")]
    		public Boolean AssessorApproval { get; set; }

			[DisplayName("Assessor Comment")]
    		public String AssessorComment { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationAttachmentMetadata))]
	public partial class QuestionnaireVerificationAttachment
	{
		public sealed class QuestionnaireVerificationAttachmentMetadata //: IEntity
		{
		
			[DisplayName("Attachment")]
			[Required(ErrorMessage="Attachment is required")]
    		public Int32 AttachmentId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
			[Required(ErrorMessage="Section is required")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[Required(ErrorMessage="File Hash is required")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
			[Required(ErrorMessage="Content Length is required")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationAttachmentAuditMetadata))]
	public partial class QuestionnaireVerificationAttachmentAudit
	{
		public sealed class QuestionnaireVerificationAttachmentAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Attachment")]
    		public Int32 AttachmentId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationRationaleMetadata))]
	public partial class QuestionnaireVerificationRationale
	{
		public sealed class QuestionnaireVerificationRationaleMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire Verification Rationale1")]
			[Required(ErrorMessage="Questionnaire Verification Rationale1 is required")]
    		public Int32 QuestionnaireVerificationRationale1 { get; set; }

			[DisplayName("Section")]
			[Required(ErrorMessage="Section is required")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
			[StringLength(50)]
    		public String QuestionId { get; set; }

			[DisplayName("Rationale")]
    		public Byte[] Rationale { get; set; }

			[DisplayName("Assistance")]
    		public Byte[] Assistance { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationResponseMetadata))]
	public partial class QuestionnaireVerificationResponse
	{
		public sealed class QuestionnaireVerificationResponseMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
			[Required(ErrorMessage="Section is required")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Answer Text")]
			[StringLength(255)]
    		public String AnswerText { get; set; }

			[DisplayName("Additional Evidence")]
    		public Byte[] AdditionalEvidence { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationResponseAuditMetadata))]
	public partial class QuestionnaireVerificationResponseAudit
	{
		public sealed class QuestionnaireVerificationResponseAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Questionnaire")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section")]
    		public Int32 SectionId { get; set; }

			[DisplayName("Question")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Answer Text")]
			[StringLength(255)]
    		public String AnswerText { get; set; }

			[DisplayName("Additional Evidence")]
    		public Byte[] AdditionalEvidence { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireVerificationSectionMetadata))]
	public partial class QuestionnaireVerificationSection
	{
		public sealed class QuestionnaireVerificationSectionMetadata //: IEntity
		{
		
			[DisplayName("Section Response")]
			[Required(ErrorMessage="Section Response is required")]
    		public Int32 SectionResponseId { get; set; }

			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Section1_ Complete")]
    		public Boolean Section1_Complete { get; set; }

			[DisplayName("Section2_ Complete")]
    		public Boolean Section2_Complete { get; set; }

			[DisplayName("Section3_ Complete")]
    		public Boolean Section3_Complete { get; set; }

			[DisplayName("Section4_ Complete")]
    		public Boolean Section4_Complete { get; set; }

			[DisplayName("Section5_ Complete")]
    		public Boolean Section5_Complete { get; set; }

			[DisplayName("Section6_ Complete")]
    		public Boolean Section6_Complete { get; set; }

			[DisplayName("Section7_ Complete")]
    		public Boolean Section7_Complete { get; set; }

			[DisplayName("Section8_ Complete")]
    		public Boolean Section8_Complete { get; set; }

    		public EntityCollection<Questionnaire> Questionnaire { get; set; }

		}
	}
	
	[MetadataType(typeof(QuestionnaireWithLocationApprovalViewMetadata))]
	public partial class QuestionnaireWithLocationApprovalView
	{
		public sealed class QuestionnaireWithLocationApprovalViewMetadata //: IEntity
		{
		
			[DisplayName("Questionnaire")]
			[Required(ErrorMessage="Questionnaire is required")]
    		public Int32 QuestionnaireId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created By User")]
			[StringLength(202)]
    		public String CreatedByUser { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified By User")]
			[StringLength(202)]
    		public String ModifiedByUser { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Approved Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ApprovedDate { get; set; }

			[DisplayName("Days Since Last Activity")]
    		public Int32 DaysSinceLastActivity { get; set; }

			[DisplayName("QMA Last Modified")]
			[DataType(DataType.DateTime)]
    		public DateTime QMALastModified { get; set; }

			[DisplayName("QVA Last Modified")]
			[DataType(DataType.DateTime)]
    		public DateTime QVALastModified { get; set; }

			[DisplayName("Assessed Date")]
			[DataType(DataType.DateTime)]
    		public DateTime AssessedDate { get; set; }

			[DisplayName("Assessed By User")]
    		public Int32 AssessedByUserId { get; set; }

			[DisplayName("Status")]
			[Required(ErrorMessage="Status is required")]
    		public Int32 Status { get; set; }

			[DisplayName("Recommended")]
    		public Boolean Recommended { get; set; }

			[DisplayName("Initial Created By User")]
    		public Int32 InitialCreatedByUserId { get; set; }

			[DisplayName("Initial Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialCreatedDate { get; set; }

			[DisplayName("Initial Submitted By User")]
    		public Int32 InitialSubmittedByUserId { get; set; }

			[DisplayName("Initial Submitted Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialSubmittedDate { get; set; }

			[DisplayName("Main Created By User")]
    		public Int32 MainCreatedByUserId { get; set; }

			[DisplayName("Main Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainCreatedDate { get; set; }

			[DisplayName("Verification Created By User")]
    		public Int32 VerificationCreatedByUserId { get; set; }

			[DisplayName("Verification Created Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationCreatedDate { get; set; }

			[DisplayName("Is Main Required")]
			[Required(ErrorMessage="Is Main Required is required")]
    		public Boolean IsMainRequired { get; set; }

			[DisplayName("Is Verification Required")]
			[Required(ErrorMessage="Is Verification Required is required")]
    		public Boolean IsVerificationRequired { get; set; }

			[DisplayName("Initial Category High")]
    		public Boolean InitialCategoryHigh { get; set; }

			[DisplayName("Initial Modified By User")]
    		public Int32 InitialModifiedByUserId { get; set; }

			[DisplayName("Initial Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime InitialModifiedDate { get; set; }

			[DisplayName("Initial Status")]
			[Required(ErrorMessage="Initial Status is required")]
    		public Int32 InitialStatus { get; set; }

			[DisplayName("Initial Risk Assessment")]
			[StringLength(50)]
    		public String InitialRiskAssessment { get; set; }

			[DisplayName("Main Modified By User")]
    		public Int32 MainModifiedByUserId { get; set; }

			[DisplayName("Main Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainModifiedDate { get; set; }

			[DisplayName("Main Score Expectations")]
    		public Int32 MainScoreExpectations { get; set; }

			[DisplayName("Main Score Overall")]
    		public Int32 MainScoreOverall { get; set; }

			[DisplayName("Main Score Results")]
    		public Int32 MainScoreResults { get; set; }

			[DisplayName("Main Score Staffing")]
    		public Int32 MainScoreStaffing { get; set; }

			[DisplayName("Main Score Systems")]
    		public Int32 MainScoreSystems { get; set; }

			[DisplayName("Main Status")]
			[Required(ErrorMessage="Main Status is required")]
    		public Int32 MainStatus { get; set; }

			[DisplayName("Main Assessment By User")]
    		public Int32 MainAssessmentByUserId { get; set; }

			[DisplayName("Main Assessment Comments")]
    		public String MainAssessmentComments { get; set; }

			[DisplayName("Main Assessment Date")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentDate { get; set; }

			[DisplayName("Main Assessment Risk Rating")]
			[StringLength(50)]
    		public String MainAssessmentRiskRating { get; set; }

			[DisplayName("Main Assessment Status")]
			[StringLength(50)]
    		public String MainAssessmentStatus { get; set; }

			[DisplayName("Main Assessment Valid To")]
			[DataType(DataType.DateTime)]
    		public DateTime MainAssessmentValidTo { get; set; }

			[DisplayName("Main Score P Expectations")]
    		public Int32 MainScorePExpectations { get; set; }

			[DisplayName("Main Score P Overall")]
    		public Int32 MainScorePOverall { get; set; }

			[DisplayName("Main Score P Results")]
    		public Int32 MainScorePResults { get; set; }

			[DisplayName("Main Score P Staffing")]
    		public Int32 MainScorePStaffing { get; set; }

			[DisplayName("Main Score P Systems")]
    		public Int32 MainScorePSystems { get; set; }

			[DisplayName("Verification Modified By User")]
    		public Int32 VerificationModifiedByUserId { get; set; }

			[DisplayName("Verification Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime VerificationModifiedDate { get; set; }

			[DisplayName("Verification Risk Rating")]
			[StringLength(50)]
    		public String VerificationRiskRating { get; set; }

			[DisplayName("Verification Status")]
			[Required(ErrorMessage="Verification Status is required")]
    		public Int32 VerificationStatus { get; set; }

			[DisplayName("Supplier Contact First Name")]
			[StringLength(255)]
    		public String SupplierContactFirstName { get; set; }

			[DisplayName("Supplier Contact Last Name")]
			[StringLength(255)]
    		public String SupplierContactLastName { get; set; }

			[DisplayName("Supplier Contact Email")]
			[StringLength(255)]
			[DataType(DataType.EmailAddress)]
    		public String SupplierContactEmail { get; set; }

			[DisplayName("Supplier Contact Title")]
			[StringLength(255)]
    		public String SupplierContactTitle { get; set; }

			[DisplayName("Supplier Contact Phone")]
			[StringLength(255)]
			[DataType(DataType.PhoneNumber)]
    		public String SupplierContactPhone { get; set; }

			[DisplayName("Procurement Contact User")]
    		public Int32 ProcurementContactUserId { get; set; }

			[DisplayName("Contract Manager User")]
    		public Int32 ContractManagerUserId { get; set; }

			[DisplayName("KWI")]
			[StringLength(1)]
    		public String KWI { get; set; }

			[DisplayName("PIN")]
			[StringLength(1)]
    		public String PIN { get; set; }

			[DisplayName("WGP")]
			[StringLength(1)]
    		public String WGP { get; set; }

			[DisplayName("HUN")]
			[StringLength(1)]
    		public String HUN { get; set; }

			[DisplayName("WDL")]
			[StringLength(1)]
    		public String WDL { get; set; }

			[DisplayName("BUN")]
			[StringLength(1)]
    		public String BUN { get; set; }

			[DisplayName("FML")]
			[StringLength(1)]
    		public String FML { get; set; }

			[DisplayName("BGN")]
			[StringLength(1)]
    		public String BGN { get; set; }

			[DisplayName("ANG")]
			[StringLength(1)]
    		public String ANG { get; set; }

			[DisplayName("PTL")]
			[StringLength(1)]
    		public String PTL { get; set; }

			[DisplayName("PTH")]
			[StringLength(1)]
    		public String PTH { get; set; }

			[DisplayName("PEL")]
			[StringLength(1)]
    		public String PEL { get; set; }

			[DisplayName("ARP")]
			[StringLength(1)]
    		public String ARP { get; set; }

			[DisplayName("YEN")]
			[StringLength(1)]
    		public String YEN { get; set; }

			[DisplayName("Procurement Modified")]
			[Required(ErrorMessage="Procurement Modified is required")]
    		public Boolean ProcurementModified { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Safety Assessor")]
			[StringLength(202)]
    		public String SafetyAssessor { get; set; }

			[DisplayName("Safety Assessor User")]
    		public Int32 SafetyAssessorUserId { get; set; }

			[DisplayName("Sub Contractor")]
    		public Boolean SubContractor { get; set; }

			[DisplayName("Company Status")]
			[Required(ErrorMessage="Company Status is required")]
    		public Int32 CompanyStatusId { get; set; }

			[DisplayName("Company Status Desc")]
			[StringLength(255)]
    		public String CompanyStatusDesc { get; set; }

			[DisplayName("Deactivated")]
    		public Boolean Deactivated { get; set; }

			[DisplayName("Procurement Contact User")]
			[StringLength(202)]
    		public String ProcurementContactUser { get; set; }

			[DisplayName("Contract Manager User")]
			[StringLength(202)]
    		public String ContractManagerUser { get; set; }

			[DisplayName("No Sq Expiry Emails Sent")]
			[StringLength(81)]
			[DataType(DataType.EmailAddress)]
    		public String NoSqExpiryEmailsSent { get; set; }

			[DisplayName("Questionnaire Presently With Action")]
    		public Int32 QuestionnairePresentlyWithActionId { get; set; }

			[DisplayName("Questionnaire Presently With User")]
    		public Int32 QuestionnairePresentlyWithUserId { get; set; }

			[DisplayName("Process No")]
    		public Int32 ProcessNo { get; set; }

			[DisplayName("User Name")]
			[StringLength(50)]
    		public String UserName { get; set; }

			[DisplayName("Action Name")]
			[StringLength(100)]
    		public String ActionName { get; set; }

			[DisplayName("User Description")]
			[StringLength(255)]
    		public String UserDescription { get; set; }

			[DisplayName("Action Description")]
			[StringLength(255)]
    		public String ActionDescription { get; set; }

			[DisplayName("Questionnaire Presently With Since")]
			[DataType(DataType.DateTime)]
    		public DateTime QuestionnairePresentlyWithSince { get; set; }

			[DisplayName("Questionnaire Presently With Since Days Count")]
    		public Int32 QuestionnairePresentlyWithSinceDaysCount { get; set; }

			[DisplayName("Expr1")]
    		public Int32 Expr1 { get; set; }

			[DisplayName("No Reminder Emails Sent")]
			[DataType(DataType.EmailAddress)]
    		public Int32 NoReminderEmailsSent { get; set; }

			[DisplayName("Last Reminder Email Sent On")]
			[DataType(DataType.DateTime)]
    		public DateTime LastReminderEmailSentOn { get; set; }

			[DisplayName("Supplier Contact Verified On")]
			[DataType(DataType.DateTime)]
    		public DateTime SupplierContactVerifiedOn { get; set; }

			[DisplayName("Level Of Supervision")]
			[StringLength(15)]
    		public String LevelOfSupervision { get; set; }

			[DisplayName("Recommended Comments")]
    		public String RecommendedComments { get; set; }

			[DisplayName("Approved By User")]
    		public Int32 ApprovedByUserId { get; set; }

			[DisplayName("Is Re Qualification")]
			[Required(ErrorMessage="Is Re Qualification is required")]
    		public Boolean IsReQualification { get; set; }

		}
	}
	
	[MetadataType(typeof(RegionMetadata))]
	public partial class Region
	{
		public sealed class RegionMetadata //: IEntity
		{
		
			[DisplayName("Region")]
			[Required(ErrorMessage="Region is required")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Region Name")]
			[Required(ErrorMessage="Region Name is required")]
			[StringLength(100)]
    		public String RegionName { get; set; }

			[DisplayName("Region Internal Name")]
			[StringLength(50)]
    		public String RegionInternalName { get; set; }

			[DisplayName("Region Name Abbrev")]
			[StringLength(50)]
    		public String RegionNameAbbrev { get; set; }

			[DisplayName("Region Description")]
			[StringLength(255)]
    		public String RegionDescription { get; set; }

			[DisplayName("Region Value")]
			[Required(ErrorMessage="Region Value is required")]
    		public Int32 RegionValue { get; set; }

			[DisplayName("Iproc Code")]
    		public Int32 IprocCode { get; set; }

			[DisplayName("Is Visible")]
			[Required(ErrorMessage="Is Visible is required")]
    		public Boolean IsVisible { get; set; }

			[DisplayName("Ordinal")]
			[Required(ErrorMessage="Ordinal is required")]
    		public Int32 Ordinal { get; set; }

			[DisplayName("Level")]
			[Required(ErrorMessage="Level is required")]
    		public Int32 Level { get; set; }

			[DisplayName("Region Site")]
    		public Int32 RegionSiteId { get; set; }

    		public EntityCollection<ContactsContractor> ContactsContractors { get; set; }

    		public EntityCollection<Document> Documents { get; set; }

    		public EntityCollection<FileDbMedicalTraining> FileDbMedicalTrainings { get; set; }

    		public EntityCollection<RegionsSite> RegionsSites { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(RegionsSiteMetadata))]
	public partial class RegionsSite
	{
		public sealed class RegionsSiteMetadata //: IEntity
		{
		
			[DisplayName("Region Site")]
			[Required(ErrorMessage="Region Site is required")]
    		public Int32 RegionSiteId { get; set; }

			[DisplayName("Region")]
			[Required(ErrorMessage="Region is required")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

    		public EntityCollection<Region> Region { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(ResidentialMetadata))]
	public partial class Residential
	{
		public sealed class ResidentialMetadata //: IEntity
		{
		
			[DisplayName("Residential")]
			[Required(ErrorMessage="Residential is required")]
    		public Int32 ResidentialId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Residential Status")]
    		public Boolean ResidentialStatus { get; set; }

			[DisplayName("Cont Company Code")]
			[StringLength(3)]
    		public String ContCompanyCode { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Site> Site { get; set; }

		}
	}
	
	[MetadataType(typeof(RoleMetadata))]
	public partial class Role
	{
		public sealed class RoleMetadata //: IEntity
		{
		
			[DisplayName("Role")]
			[Required(ErrorMessage="Role is required")]
    		public Int32 RoleId { get; set; }

			[DisplayName("Role1")]
			[Required(ErrorMessage="Role1 is required")]
			[StringLength(50)]
    		public String Role1 { get; set; }

			[DisplayName("Role Description")]
			[StringLength(100)]
    		public String RoleDescription { get; set; }

    		public EntityCollection<CompanyNote> CompanyNotes { get; set; }

    		public EntityCollection<QuestionnaireActionLog> QuestionnaireActionLogs { get; set; }

    		public EntityCollection<User> Users { get; set; }

		}
	}
	
	[MetadataType(typeof(S812EvidenceFileMetadata))]
	public partial class S812EvidenceFile
	{
		public sealed class S812EvidenceFileMetadata //: IEntity
		{
		
			[DisplayName("S812 Evidence File")]
			[Required(ErrorMessage="S812 Evidence File is required")]
    		public Int32 S812EvidenceFileId { get; set; }

			[DisplayName("File Name")]
			[Required(ErrorMessage="File Name is required")]
			[StringLength(200)]
    		public String FileName { get; set; }

			[DisplayName("Upload Date")]
			[Required(ErrorMessage="Upload Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime UploadDate { get; set; }

			[DisplayName("Uploaded By")]
			[Required(ErrorMessage="Uploaded By is required")]
    		public Int32 UploadedById { get; set; }

			[DisplayName("File Size")]
			[Required(ErrorMessage="File Size is required")]
    		public Double FileSize { get; set; }

			[DisplayName("Content")]
			[Required(ErrorMessage="Content is required")]
    		public Byte[] Content { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlans_SE_AnswersMetadata))]
	public partial class SafetyPlans_SE_Answers
	{
		public sealed class SafetyPlans_SE_AnswersMetadata //: IEntity
		{
		
			[DisplayName("Answer")]
			[Required(ErrorMessage="Answer is required")]
    		public Int32 AnswerId { get; set; }

			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Answer")]
			[Required(ErrorMessage="Answer is required")]
    		public Boolean Answer { get; set; }

			[DisplayName("Comment")]
    		public Byte[] Comment { get; set; }

			[DisplayName("Assessor Approval")]
    		public Boolean AssessorApproval { get; set; }

			[DisplayName("Assessor Comment")]
    		public Byte[] AssessorComment { get; set; }

    		public EntityCollection<SafetyPlans_SE_Questions> SafetyPlans_SE_Questions { get; set; }

    		public EntityCollection<SafetyPlans_SE_Responses> SafetyPlans_SE_Responses { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlans_SE_QuestionsMetadata))]
	public partial class SafetyPlans_SE_Questions
	{
		public sealed class SafetyPlans_SE_QuestionsMetadata //: IEntity
		{
		
			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
    		public Int32 QuestionId { get; set; }

			[DisplayName("Question")]
			[Required(ErrorMessage="Question is required")]
			[StringLength(255)]
    		public String Question { get; set; }

			[DisplayName("Question Text")]
    		public Byte[] QuestionText { get; set; }

    		public EntityCollection<SafetyPlans_SE_Answers> SafetyPlans_SE_Answers { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlans_SE_ResponsesMetadata))]
	public partial class SafetyPlans_SE_Responses
	{
		public sealed class SafetyPlans_SE_ResponsesMetadata //: IEntity
		{
		
			[DisplayName("Response")]
			[Required(ErrorMessage="Response is required")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("Submitted")]
			[Required(ErrorMessage="Submitted is required")]
    		public Boolean Submitted { get; set; }

			[DisplayName("Asessor Comment")]
    		public Byte[] AsessorComment { get; set; }

			[DisplayName("Is Copy")]
    		public Boolean IsCopy { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<SafetyPlans_SE_Answers> SafetyPlans_SE_Answers { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlans_SE_ResponsesAttachmentMetadata))]
	public partial class SafetyPlans_SE_ResponsesAttachment
	{
		public sealed class SafetyPlans_SE_ResponsesAttachmentMetadata //: IEntity
		{
		
			[DisplayName("Attachment")]
			[Required(ErrorMessage="Attachment is required")]
    		public Int32 AttachmentId { get; set; }

			[DisplayName("Question")]
    		public Int32 QuestionID { get; set; }

			[DisplayName("Response")]
    		public Int32 ResponseId { get; set; }

			[DisplayName("File Name")]
			[StringLength(255)]
    		public String FileName { get; set; }

			[DisplayName("File Hash")]
			[StringLength(16)]
    		public Byte[] FileHash { get; set; }

			[DisplayName("Content Length")]
    		public Int32 ContentLength { get; set; }

			[DisplayName("Content")]
    		public Byte[] Content { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlanStatusMetadata))]
	public partial class SafetyPlanStatus
	{
		public sealed class SafetyPlanStatusMetadata //: IEntity
		{
		
			[DisplayName("Status")]
			[Required(ErrorMessage="Status is required")]
    		public Int32 StatusId { get; set; }

			[DisplayName("Status Name")]
			[Required(ErrorMessage="Status Name is required")]
			[StringLength(50)]
    		public String StatusName { get; set; }

			[DisplayName("Status Description")]
			[Required(ErrorMessage="Status Description is required")]
			[StringLength(100)]
    		public String StatusDescription { get; set; }

		}
	}
	
	[MetadataType(typeof(SafetyPlansWithoutFileHashMetadata))]
	public partial class SafetyPlansWithoutFileHash
	{
		public sealed class SafetyPlansWithoutFileHashMetadata //: IEntity
		{
		
			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Description")]
			[Required(ErrorMessage="Description is required")]
			[StringLength(100)]
    		public String Description { get; set; }

		}
	}
	
	[MetadataType(typeof(SiteMetadata))]
	public partial class Site
	{
		public sealed class SiteMetadata //: IEntity
		{
		
			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Site Name")]
			[Required(ErrorMessage="Site Name is required")]
			[StringLength(50)]
    		public String SiteName { get; set; }

			[DisplayName("Site Abbrev")]
			[StringLength(5)]
    		public String SiteAbbrev { get; set; }

			[DisplayName("Site Address")]
			[Required(ErrorMessage="Site Address is required")]
			[StringLength(200)]
    		public String SiteAddress { get; set; }

			[DisplayName("Site Main Contact No")]
			[Required(ErrorMessage="Site Main Contact No is required")]
			[StringLength(50)]
    		public String SiteMainContactNo { get; set; }

			[DisplayName("Loc Code")]
			[StringLength(3)]
    		public String LocCode { get; set; }

			[DisplayName("Iproc Code")]
    		public Int32 IprocCode { get; set; }

			[DisplayName("Is Visible")]
			[Required(ErrorMessage="Is Visible is required")]
    		public Boolean IsVisible { get; set; }

			[DisplayName("Ordinal")]
    		public Int32 Ordinal { get; set; }

			[DisplayName("Modifiedby User")]
			[Required(ErrorMessage="Modifiedby User is required")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Editable")]
			[Required(ErrorMessage="Editable is required")]
    		public Boolean Editable { get; set; }

			[DisplayName("Ehs Consultant")]
    		public Int32 EhsConsultantId { get; set; }

			[DisplayName("Site Name Ebi")]
			[StringLength(50)]
    		public String SiteNameEbi { get; set; }

			[DisplayName("Site Name Ihs")]
			[StringLength(50)]
    		public String SiteNameIhs { get; set; }

			[DisplayName("Ebi All View Name")]
			[StringLength(255)]
    		public String EbiAllViewName { get; set; }

			[DisplayName("Ebi Server Name")]
			[StringLength(50)]
    		public String EbiServerName { get; set; }

			[DisplayName("Ebi Today View Name")]
			[StringLength(255)]
    		public String EbiTodayViewName { get; set; }

			[DisplayName("Site Name Hr")]
			[StringLength(50)]
    		public String SiteNameHr { get; set; }

    		public EntityCollection<AdHoc_Radar_Items> AdHoc_Radar_Items { get; set; }

    		public EntityCollection<AdHoc_Radar_Items2> AdHoc_Radar_Items2 { get; set; }

    		public EntityCollection<CompaniesEhsimsMap> CompaniesEhsimsMaps { get; set; }

    		public EntityCollection<CompaniesHrCrpData> CompaniesHrCrpDatas { get; set; }

    		public EntityCollection<CompanySiteCategoryException> CompanySiteCategoryExceptions { get; set; }

    		public EntityCollection<CompanySiteCategoryException2> CompanySiteCategoryException2 { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards { get; set; }

    		public EntityCollection<ConfigSa812> ConfigSa812 { get; set; }

    		public EntityCollection<ContactsAlcoa> ContactsAlcoas { get; set; }

    		public EntityCollection<ContactsContractor> ContactsContractors { get; set; }

    		public EntityCollection<CSA> CSAs { get; set; }

    		public EntityCollection<EbiMetric> EbiMetrics { get; set; }

    		public EntityCollection<EHSConsultant> EHSConsultant { get; set; }

    		public EntityCollection<EscalationChainProcurement> EscalationChainProcurements { get; set; }

    		public EntityCollection<EscalationChainSafety> EscalationChainSafeties { get; set; }

    		public EntityCollection<Kpi> Kpis { get; set; }

    		public EntityCollection<QuestionnaireInitialLocation> QuestionnaireInitialLocations { get; set; }

    		public EntityCollection<QuestionnairePresentlyWithMetric> QuestionnairePresentlyWithMetrics { get; set; }

    		public EntityCollection<RegionsSite> RegionsSites { get; set; }

    		public EntityCollection<Residential> Residentials { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<SqExemption> SqExemptions { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

    		public EntityCollection<Region> Regions { get; set; }

    		public EntityCollection<CompanySiteCategoryStandardHistory> CompanySiteCategoryStandardHistories { get; set; }

		}
	}
	
	[MetadataType(typeof(SqExemptionMetadata))]
	public partial class SqExemption
	{
		public sealed class SqExemptionMetadata //: IEntity
		{
		
			[DisplayName("Sq Exemption")]
			[Required(ErrorMessage="Sq Exemption is required")]
    		public Int32 SqExemptionId { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Date Applied")]
			[Required(ErrorMessage="Date Applied is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateApplied { get; set; }

			[DisplayName("Valid From")]
			[Required(ErrorMessage="Valid From is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ValidFrom { get; set; }

			[DisplayName("Valid To")]
			[Required(ErrorMessage="Valid To is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ValidTo { get; set; }

			[DisplayName("Company Status2")]
			[Required(ErrorMessage="Company Status2 is required")]
    		public Int32 CompanyStatus2Id { get; set; }

			[DisplayName("Requested By Company")]
			[Required(ErrorMessage="Requested By Company is required")]
    		public Int32 RequestedByCompanyId { get; set; }

			[DisplayName("File Vault")]
			[Required(ErrorMessage="File Vault is required")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Company> Company1 { get; set; }

    		public EntityCollection<CompanyStatus2> CompanyStatus2 { get; set; }

    		public EntityCollection<FileVault> FileVault { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(SqExemptionAuditMetadata))]
	public partial class SqExemptionAudit
	{
		public sealed class SqExemptionAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Sq Exemption")]
    		public Int32 SqExemptionId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Date Applied")]
			[DataType(DataType.DateTime)]
    		public DateTime DateApplied { get; set; }

			[DisplayName("Valid From")]
			[DataType(DataType.DateTime)]
    		public DateTime ValidFrom { get; set; }

			[DisplayName("Valid To")]
			[DataType(DataType.DateTime)]
    		public DateTime ValidTo { get; set; }

			[DisplayName("Company Status2")]
    		public Int32 CompanyStatus2Id { get; set; }

			[DisplayName("Requested By Company")]
    		public Int32 RequestedByCompanyId { get; set; }

			[DisplayName("File Vault")]
    		public Int32 FileVaultId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

		}
	}
	
	[MetadataType(typeof(SystemLogMetadata))]
	public partial class SystemLog
	{
		public sealed class SystemLogMetadata //: IEntity
		{
		
			[DisplayName("System Log")]
			[Required(ErrorMessage="System Log is required")]
    		public Int32 SystemLogId { get; set; }

			[DisplayName("Date Time")]
			[Required(ErrorMessage="Date Time is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateTime { get; set; }

			[DisplayName("Date")]
			[Required(ErrorMessage="Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime Date { get; set; }

			[DisplayName("Activity")]
			[Required(ErrorMessage="Activity is required")]
			[StringLength(100)]
    		public String Activity { get; set; }

			[DisplayName("Success")]
			[Required(ErrorMessage="Success is required")]
    		public Boolean Success { get; set; }

			[DisplayName("Notes")]
    		public String Notes { get; set; }

		}
	}
	
	[MetadataType(typeof(TemplateMetadata))]
	public partial class Template
	{
		public sealed class TemplateMetadata //: IEntity
		{
		
			[DisplayName("Template")]
			[Required(ErrorMessage="Template is required")]
    		public Int32 TemplateId { get; set; }

			[DisplayName("Template Type")]
			[Required(ErrorMessage="Template Type is required")]
    		public Int32 TemplateTypeId { get; set; }

			[DisplayName("Template Content")]
    		public Byte[] TemplateContent { get; set; }

		}
	}
	
	[MetadataType(typeof(TemplateTypeMetadata))]
	public partial class TemplateType
	{
		public sealed class TemplateTypeMetadata //: IEntity
		{
		
			[DisplayName("Template Type")]
			[Required(ErrorMessage="Template Type is required")]
    		public Int32 TemplateTypeId { get; set; }

			[DisplayName("Template Type Name")]
			[Required(ErrorMessage="Template Type Name is required")]
			[StringLength(100)]
    		public String TemplateTypeName { get; set; }

			[DisplayName("Template Type Desc")]
			[Required(ErrorMessage="Template Type Desc is required")]
			[StringLength(255)]
    		public String TemplateTypeDesc { get; set; }

		}
	}
	
	[MetadataType(typeof(Training_Management_ValidityMetadata))]
	public partial class Training_Management_Validity
	{
		public sealed class Training_Management_ValidityMetadata //: IEntity
		{
		
			[DisplayName("FULL_NAME")]
    		public String FULL_NAME { get; set; }

			[DisplayName("EMPL_")]
			[Required(ErrorMessage="EMPL_ is required")]
			[StringLength(30)]
    		public String EMPL_ID { get; set; }

			[DisplayName("COURSE_NAME")]
			[Required(ErrorMessage="COURSE_NAME is required")]
			[StringLength(80)]
    		public String COURSE_NAME { get; set; }

			[DisplayName("COURSE_END_DATE")]
			[Required(ErrorMessage="COURSE_END_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime COURSE_END_DATE { get; set; }

			[DisplayName("EXPIRATION_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime EXPIRATION_DATE { get; set; }

			[DisplayName("VENDOR_NO")]
    		public String VENDOR_NO { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Company Name")]
    		public String CompanyName { get; set; }

			[DisplayName("Site Name")]
    		public String SiteName { get; set; }

			[DisplayName("TRAINING_TITLE")]
			[Required(ErrorMessage="TRAINING_TITLE is required")]
			[StringLength(80)]
    		public String TRAINING_TITLE { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

		}
	}
	
	[MetadataType(typeof(TrainingCompetencyMetadata))]
	public partial class TrainingCompetency
	{
		public sealed class TrainingCompetencyMetadata //: IEntity
		{
		
			[DisplayName("Column")]
			[Required(ErrorMessage="Column is required")]
    		public Int32 ColumnId { get; set; }

			[DisplayName("Package_ Column Name")]
			[StringLength(50)]
    		public String Package_ColumnName { get; set; }

			[DisplayName("Package_ Name")]
			[StringLength(100)]
    		public String Package_Name { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

			[DisplayName("Modified By User")]
			[StringLength(50)]
    		public String ModifiedByUser { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAuditMetadata))]
	public partial class TwentyOnePointAudit
	{
		public sealed class TwentyOnePointAuditMetadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Site")]
			[Required(ErrorMessage="Site is required")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Createdby User")]
			[Required(ErrorMessage="Createdby User is required")]
    		public Int32 CreatedbyUserId { get; set; }

			[DisplayName("Modifiedby User")]
			[Required(ErrorMessage="Modifiedby User is required")]
    		public Int32 ModifiedbyUserId { get; set; }

			[DisplayName("Date Added")]
			[Required(ErrorMessage="Date Added is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateAdded { get; set; }

			[DisplayName("Date Modified")]
			[Required(ErrorMessage="Date Modified is required")]
			[DataType(DataType.DateTime)]
    		public DateTime DateModified { get; set; }

			[DisplayName("Qtr")]
			[Required(ErrorMessage="Qtr is required")]
    		public Int32 QtrId { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int64 Year { get; set; }

			[DisplayName("Total Possible Score")]
    		public Int32 TotalPossibleScore { get; set; }

			[DisplayName("Total Actual Score")]
    		public Int32 TotalActualScore { get; set; }

			[DisplayName("Total Rating")]
    		public Int32 TotalRating { get; set; }

			[DisplayName("Point_01_")]
    		public Int32 Point_01_Id { get; set; }

			[DisplayName("Point_02_")]
    		public Int32 Point_02_Id { get; set; }

			[DisplayName("Point_03_")]
    		public Int32 Point_03_Id { get; set; }

			[DisplayName("Point_04_")]
    		public Int32 Point_04_Id { get; set; }

			[DisplayName("Point_05_")]
    		public Int32 Point_05_Id { get; set; }

			[DisplayName("Point_06_")]
    		public Int32 Point_06_Id { get; set; }

			[DisplayName("Point_07_")]
    		public Int32 Point_07_Id { get; set; }

			[DisplayName("Point_08_")]
    		public Int32 Point_08_Id { get; set; }

			[DisplayName("Point_09_")]
    		public Int32 Point_09_Id { get; set; }

			[DisplayName("Point_10_")]
    		public Int32 Point_10_Id { get; set; }

			[DisplayName("Point_11_")]
    		public Int32 Point_11_Id { get; set; }

			[DisplayName("Point_12_")]
    		public Int32 Point_12_Id { get; set; }

			[DisplayName("Point_13_")]
    		public Int32 Point_13_Id { get; set; }

			[DisplayName("Point_14_")]
    		public Int32 Point_14_Id { get; set; }

			[DisplayName("Point_15_")]
    		public Int32 Point_15_Id { get; set; }

			[DisplayName("Point_16_")]
    		public Int32 Point_16_Id { get; set; }

			[DisplayName("Point_17_")]
    		public Int32 Point_17_Id { get; set; }

			[DisplayName("Point_18_")]
    		public Int32 Point_18_Id { get; set; }

			[DisplayName("Point_19_")]
    		public Int32 Point_19_Id { get; set; }

			[DisplayName("Point_20_")]
    		public Int32 Point_20_Id { get; set; }

			[DisplayName("Point_21_")]
    		public Int32 Point_21_Id { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<Site> Site { get; set; }

    		public EntityCollection<TwentyOnePointAudit_01> TwentyOnePointAudit_01 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_02> TwentyOnePointAudit_02 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_03> TwentyOnePointAudit_03 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_04> TwentyOnePointAudit_04 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_05> TwentyOnePointAudit_05 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_06> TwentyOnePointAudit_06 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_07> TwentyOnePointAudit_07 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_08> TwentyOnePointAudit_08 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_09> TwentyOnePointAudit_09 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_10> TwentyOnePointAudit_10 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_11> TwentyOnePointAudit_11 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_12> TwentyOnePointAudit_12 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_13> TwentyOnePointAudit_13 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_14> TwentyOnePointAudit_14 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_15> TwentyOnePointAudit_15 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_16> TwentyOnePointAudit_16 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_17> TwentyOnePointAudit_17 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_18> TwentyOnePointAudit_18 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_19> TwentyOnePointAudit_19 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_20> TwentyOnePointAudit_20 { get; set; }

    		public EntityCollection<TwentyOnePointAudit_21> TwentyOnePointAudit_21 { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_01Metadata))]
	public partial class TwentyOnePointAudit_01
	{
		public sealed class TwentyOnePointAudit_01Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved1a")]
    		public Int32 Achieved1a { get; set; }

			[DisplayName("Achieved1b")]
    		public Int32 Achieved1b { get; set; }

			[DisplayName("Achieved1c")]
    		public Int32 Achieved1c { get; set; }

			[DisplayName("Achieved1c1")]
    		public Int32 Achieved1c1 { get; set; }

			[DisplayName("Achieved1c2")]
    		public Int32 Achieved1c2 { get; set; }

			[DisplayName("Achieved1c3")]
    		public Int32 Achieved1c3 { get; set; }

			[DisplayName("Achieved1c4")]
    		public Int32 Achieved1c4 { get; set; }

			[DisplayName("Achieved1c5")]
    		public Int32 Achieved1c5 { get; set; }

			[DisplayName("Achieved1c6")]
    		public Int32 Achieved1c6 { get; set; }

			[DisplayName("Achieved1c7")]
    		public Int32 Achieved1c7 { get; set; }

			[DisplayName("Achieved1c8")]
    		public Int32 Achieved1c8 { get; set; }

			[DisplayName("Achieved1c9")]
    		public Int32 Achieved1c9 { get; set; }

			[DisplayName("Achieved1c10")]
    		public Int32 Achieved1c10 { get; set; }

			[DisplayName("Achieved1c11")]
    		public Int32 Achieved1c11 { get; set; }

			[DisplayName("Achieved1d")]
    		public Int32 Achieved1d { get; set; }

			[DisplayName("Achieved1e")]
    		public Int32 Achieved1e { get; set; }

			[DisplayName("Achieved1f")]
    		public Int32 Achieved1f { get; set; }

			[DisplayName("Achieved1g")]
    		public Int32 Achieved1g { get; set; }

			[DisplayName("Achieved1h")]
    		public Int32 Achieved1h { get; set; }

			[DisplayName("Achieved1i")]
    		public Int32 Achieved1i { get; set; }

			[DisplayName("Achieved1j")]
    		public Int32 Achieved1j { get; set; }

			[DisplayName("Achieved1k")]
    		public Int32 Achieved1k { get; set; }

			[DisplayName("Achieved1l")]
    		public Int32 Achieved1l { get; set; }

			[DisplayName("Achieved1m")]
    		public Int32 Achieved1m { get; set; }

			[DisplayName("Achieved1n")]
    		public Int32 Achieved1n { get; set; }

			[DisplayName("Achieved1o")]
    		public Int32 Achieved1o { get; set; }

			[DisplayName("Achieved1p")]
    		public Int32 Achieved1p { get; set; }

			[DisplayName("Achieved1q")]
    		public Int32 Achieved1q { get; set; }

			[DisplayName("Achieved1r")]
    		public Int32 Achieved1r { get; set; }

			[DisplayName("Achieved1s")]
    		public Int32 Achieved1s { get; set; }

			[DisplayName("Achieved1t")]
    		public Int32 Achieved1t { get; set; }

			[DisplayName("Achieved1u")]
    		public Int32 Achieved1u { get; set; }

			[DisplayName("Achieved1v")]
    		public Int32 Achieved1v { get; set; }

			[DisplayName("Achieved1w")]
    		public Int32 Achieved1w { get; set; }

			[DisplayName("Achieved1x")]
    		public Int32 Achieved1x { get; set; }

			[DisplayName("Achieved1y")]
    		public Int32 Achieved1y { get; set; }

			[DisplayName("Achieved1z")]
    		public Int32 Achieved1z { get; set; }

			[DisplayName("Observation1a")]
			[StringLength(100)]
    		public String Observation1a { get; set; }

			[DisplayName("Observation1b")]
			[StringLength(100)]
    		public String Observation1b { get; set; }

			[DisplayName("Observation1c")]
			[StringLength(100)]
    		public String Observation1c { get; set; }

			[DisplayName("Observation1c1")]
			[StringLength(100)]
    		public String Observation1c1 { get; set; }

			[DisplayName("Observation1c2")]
			[StringLength(100)]
    		public String Observation1c2 { get; set; }

			[DisplayName("Observation1c3")]
			[StringLength(100)]
    		public String Observation1c3 { get; set; }

			[DisplayName("Observation1c4")]
			[StringLength(100)]
    		public String Observation1c4 { get; set; }

			[DisplayName("Observation1c5")]
			[StringLength(100)]
    		public String Observation1c5 { get; set; }

			[DisplayName("Observation1c6")]
			[StringLength(100)]
    		public String Observation1c6 { get; set; }

			[DisplayName("Observation1c7")]
			[StringLength(100)]
    		public String Observation1c7 { get; set; }

			[DisplayName("Observation1c8")]
			[StringLength(100)]
    		public String Observation1c8 { get; set; }

			[DisplayName("Observation1c9")]
			[StringLength(100)]
    		public String Observation1c9 { get; set; }

			[DisplayName("Observation1c10")]
			[StringLength(100)]
    		public String Observation1c10 { get; set; }

			[DisplayName("Observation1c11")]
			[StringLength(100)]
    		public String Observation1c11 { get; set; }

			[DisplayName("Observation1d")]
			[StringLength(100)]
    		public String Observation1d { get; set; }

			[DisplayName("Observation1e")]
			[StringLength(100)]
    		public String Observation1e { get; set; }

			[DisplayName("Observation1f")]
			[StringLength(100)]
    		public String Observation1f { get; set; }

			[DisplayName("Observation1g")]
			[StringLength(100)]
    		public String Observation1g { get; set; }

			[DisplayName("Observation1h")]
			[StringLength(100)]
    		public String Observation1h { get; set; }

			[DisplayName("Observation1i")]
			[StringLength(100)]
    		public String Observation1i { get; set; }

			[DisplayName("Observation1j")]
			[StringLength(100)]
    		public String Observation1j { get; set; }

			[DisplayName("Observation1k")]
			[StringLength(100)]
    		public String Observation1k { get; set; }

			[DisplayName("Observation1l")]
			[StringLength(100)]
    		public String Observation1l { get; set; }

			[DisplayName("Observation1m")]
			[StringLength(100)]
    		public String Observation1m { get; set; }

			[DisplayName("Observation1n")]
			[StringLength(100)]
    		public String Observation1n { get; set; }

			[DisplayName("Observation1o")]
			[StringLength(100)]
    		public String Observation1o { get; set; }

			[DisplayName("Observation1p")]
			[StringLength(100)]
    		public String Observation1p { get; set; }

			[DisplayName("Observation1q")]
			[StringLength(100)]
    		public String Observation1q { get; set; }

			[DisplayName("Observation1r")]
			[StringLength(100)]
    		public String Observation1r { get; set; }

			[DisplayName("Observation1s")]
			[StringLength(100)]
    		public String Observation1s { get; set; }

			[DisplayName("Observation1t")]
			[StringLength(100)]
    		public String Observation1t { get; set; }

			[DisplayName("Observation1u")]
			[StringLength(100)]
    		public String Observation1u { get; set; }

			[DisplayName("Observation1v")]
			[StringLength(100)]
    		public String Observation1v { get; set; }

			[DisplayName("Observation1w")]
			[StringLength(100)]
    		public String Observation1w { get; set; }

			[DisplayName("Observation1x")]
			[StringLength(100)]
    		public String Observation1x { get; set; }

			[DisplayName("Observation1y")]
			[StringLength(100)]
    		public String Observation1y { get; set; }

			[DisplayName("Observation1z")]
			[StringLength(100)]
    		public String Observation1z { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_02Metadata))]
	public partial class TwentyOnePointAudit_02
	{
		public sealed class TwentyOnePointAudit_02Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved2a")]
    		public Int32 Achieved2a { get; set; }

			[DisplayName("Achieved2b")]
    		public Int32 Achieved2b { get; set; }

			[DisplayName("Achieved2c")]
    		public Int32 Achieved2c { get; set; }

			[DisplayName("Achieved2d")]
    		public Int32 Achieved2d { get; set; }

			[DisplayName("Achieved2e")]
    		public Int32 Achieved2e { get; set; }

			[DisplayName("Achieved2f")]
    		public Int32 Achieved2f { get; set; }

			[DisplayName("Achieved2g")]
    		public Int32 Achieved2g { get; set; }

			[DisplayName("Achieved2h")]
    		public Int32 Achieved2h { get; set; }

			[DisplayName("Achieved2i")]
    		public Int32 Achieved2i { get; set; }

			[DisplayName("Observation2a")]
			[StringLength(100)]
    		public String Observation2a { get; set; }

			[DisplayName("Observation2b")]
			[StringLength(100)]
    		public String Observation2b { get; set; }

			[DisplayName("Observation2c")]
			[StringLength(100)]
    		public String Observation2c { get; set; }

			[DisplayName("Observation2d")]
			[StringLength(100)]
    		public String Observation2d { get; set; }

			[DisplayName("Observation2e")]
			[StringLength(100)]
    		public String Observation2e { get; set; }

			[DisplayName("Observation2f")]
			[StringLength(100)]
    		public String Observation2f { get; set; }

			[DisplayName("Observation2g")]
			[StringLength(100)]
    		public String Observation2g { get; set; }

			[DisplayName("Observation2h")]
			[StringLength(100)]
    		public String Observation2h { get; set; }

			[DisplayName("Observation2i")]
			[StringLength(100)]
    		public String Observation2i { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_03Metadata))]
	public partial class TwentyOnePointAudit_03
	{
		public sealed class TwentyOnePointAudit_03Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved3a")]
    		public Int32 Achieved3a { get; set; }

			[DisplayName("Achieved3b")]
    		public Int32 Achieved3b { get; set; }

			[DisplayName("Achieved3c")]
    		public Int32 Achieved3c { get; set; }

			[DisplayName("Achieved3d")]
    		public Int32 Achieved3d { get; set; }

			[DisplayName("Achieved3e")]
    		public Int32 Achieved3e { get; set; }

			[DisplayName("Achieved3f")]
    		public Int32 Achieved3f { get; set; }

			[DisplayName("Achieved3g")]
    		public Int32 Achieved3g { get; set; }

			[DisplayName("Achieved3h")]
    		public Int32 Achieved3h { get; set; }

			[DisplayName("Achieved3i")]
    		public Int32 Achieved3i { get; set; }

			[DisplayName("Observation3a")]
			[StringLength(100)]
    		public String Observation3a { get; set; }

			[DisplayName("Observation3b")]
			[StringLength(100)]
    		public String Observation3b { get; set; }

			[DisplayName("Observation3c")]
			[StringLength(100)]
    		public String Observation3c { get; set; }

			[DisplayName("Observation3d")]
			[StringLength(100)]
    		public String Observation3d { get; set; }

			[DisplayName("Observation3e")]
			[StringLength(100)]
    		public String Observation3e { get; set; }

			[DisplayName("Observation3f")]
			[StringLength(100)]
    		public String Observation3f { get; set; }

			[DisplayName("Observation3g")]
			[StringLength(100)]
    		public String Observation3g { get; set; }

			[DisplayName("Observation3h")]
			[StringLength(100)]
    		public String Observation3h { get; set; }

			[DisplayName("Observation3i")]
			[StringLength(100)]
    		public String Observation3i { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_04Metadata))]
	public partial class TwentyOnePointAudit_04
	{
		public sealed class TwentyOnePointAudit_04Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved4a")]
    		public Int32 Achieved4a { get; set; }

			[DisplayName("Achieved4b")]
    		public Int32 Achieved4b { get; set; }

			[DisplayName("Achieved4c")]
    		public Int32 Achieved4c { get; set; }

			[DisplayName("Achieved4d")]
    		public Int32 Achieved4d { get; set; }

			[DisplayName("Achieved4e")]
    		public Int32 Achieved4e { get; set; }

			[DisplayName("Achieved4f")]
    		public Int32 Achieved4f { get; set; }

			[DisplayName("Achieved4g")]
    		public Int32 Achieved4g { get; set; }

			[DisplayName("Achieved4h")]
    		public Int32 Achieved4h { get; set; }

			[DisplayName("Achieved4i")]
    		public Int32 Achieved4i { get; set; }

			[DisplayName("Achieved4j")]
    		public Int32 Achieved4j { get; set; }

			[DisplayName("Achieved4k")]
    		public Int32 Achieved4k { get; set; }

			[DisplayName("Achieved4l")]
    		public Int32 Achieved4l { get; set; }

			[DisplayName("Observation4a")]
			[StringLength(100)]
    		public String Observation4a { get; set; }

			[DisplayName("Observation4b")]
			[StringLength(100)]
    		public String Observation4b { get; set; }

			[DisplayName("Observation4c")]
			[StringLength(100)]
    		public String Observation4c { get; set; }

			[DisplayName("Observation4d")]
			[StringLength(100)]
    		public String Observation4d { get; set; }

			[DisplayName("Observation4e")]
			[StringLength(100)]
    		public String Observation4e { get; set; }

			[DisplayName("Observation4f")]
			[StringLength(100)]
    		public String Observation4f { get; set; }

			[DisplayName("Observation4g")]
			[StringLength(100)]
    		public String Observation4g { get; set; }

			[DisplayName("Observation4h")]
			[StringLength(100)]
    		public String Observation4h { get; set; }

			[DisplayName("Observation4i")]
			[StringLength(100)]
    		public String Observation4i { get; set; }

			[DisplayName("Observation4j")]
			[StringLength(100)]
    		public String Observation4j { get; set; }

			[DisplayName("Observation4k")]
			[StringLength(100)]
    		public String Observation4k { get; set; }

			[DisplayName("Observation4l")]
			[StringLength(100)]
    		public String Observation4l { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_05Metadata))]
	public partial class TwentyOnePointAudit_05
	{
		public sealed class TwentyOnePointAudit_05Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved5a")]
    		public Int32 Achieved5a { get; set; }

			[DisplayName("Achieved5b")]
    		public Int32 Achieved5b { get; set; }

			[DisplayName("Achieved5c")]
    		public Int32 Achieved5c { get; set; }

			[DisplayName("Achieved5d")]
    		public Int32 Achieved5d { get; set; }

			[DisplayName("Achieved5e")]
    		public Int32 Achieved5e { get; set; }

			[DisplayName("Achieved5f")]
    		public Int32 Achieved5f { get; set; }

			[DisplayName("Achieved5g")]
    		public Int32 Achieved5g { get; set; }

			[DisplayName("Achieved5h")]
    		public Int32 Achieved5h { get; set; }

			[DisplayName("Observation5a")]
			[StringLength(100)]
    		public String Observation5a { get; set; }

			[DisplayName("Observation5b")]
			[StringLength(100)]
    		public String Observation5b { get; set; }

			[DisplayName("Observation5c")]
			[StringLength(100)]
    		public String Observation5c { get; set; }

			[DisplayName("Observation5d")]
			[StringLength(100)]
    		public String Observation5d { get; set; }

			[DisplayName("Observation5e")]
			[StringLength(100)]
    		public String Observation5e { get; set; }

			[DisplayName("Observation5f")]
			[StringLength(100)]
    		public String Observation5f { get; set; }

			[DisplayName("Observation5g")]
			[StringLength(100)]
    		public String Observation5g { get; set; }

			[DisplayName("Observation5h")]
			[StringLength(100)]
    		public String Observation5h { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_06Metadata))]
	public partial class TwentyOnePointAudit_06
	{
		public sealed class TwentyOnePointAudit_06Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved6a")]
    		public Int32 Achieved6a { get; set; }

			[DisplayName("Achieved6b")]
    		public Int32 Achieved6b { get; set; }

			[DisplayName("Achieved6c")]
    		public Int32 Achieved6c { get; set; }

			[DisplayName("Achieved6d")]
    		public Int32 Achieved6d { get; set; }

			[DisplayName("Achieved6e")]
    		public Int32 Achieved6e { get; set; }

			[DisplayName("Achieved6f")]
    		public Int32 Achieved6f { get; set; }

			[DisplayName("Achieved6g")]
    		public Int32 Achieved6g { get; set; }

			[DisplayName("Achieved6h")]
    		public Int32 Achieved6h { get; set; }

			[DisplayName("Achieved6i")]
    		public Int32 Achieved6i { get; set; }

			[DisplayName("Achieved6j")]
    		public Int32 Achieved6j { get; set; }

			[DisplayName("Achieved6k")]
    		public Int32 Achieved6k { get; set; }

			[DisplayName("Achieved6l")]
    		public Int32 Achieved6l { get; set; }

			[DisplayName("Observation6a")]
			[StringLength(100)]
    		public String Observation6a { get; set; }

			[DisplayName("Observation6b")]
			[StringLength(100)]
    		public String Observation6b { get; set; }

			[DisplayName("Observation6c")]
			[StringLength(100)]
    		public String Observation6c { get; set; }

			[DisplayName("Observation6d")]
			[StringLength(100)]
    		public String Observation6d { get; set; }

			[DisplayName("Observation6e")]
			[StringLength(100)]
    		public String Observation6e { get; set; }

			[DisplayName("Observation6f")]
			[StringLength(100)]
    		public String Observation6f { get; set; }

			[DisplayName("Observation6g")]
			[StringLength(100)]
    		public String Observation6g { get; set; }

			[DisplayName("Observation6h")]
			[StringLength(100)]
    		public String Observation6h { get; set; }

			[DisplayName("Observation6i")]
			[StringLength(100)]
    		public String Observation6i { get; set; }

			[DisplayName("Observation6j")]
			[StringLength(100)]
    		public String Observation6j { get; set; }

			[DisplayName("Observation6k")]
			[StringLength(100)]
    		public String Observation6k { get; set; }

			[DisplayName("Observation6l")]
			[StringLength(100)]
    		public String Observation6l { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_07Metadata))]
	public partial class TwentyOnePointAudit_07
	{
		public sealed class TwentyOnePointAudit_07Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved7a")]
    		public Int32 Achieved7a { get; set; }

			[DisplayName("Achieved7b")]
    		public Int32 Achieved7b { get; set; }

			[DisplayName("Achieved7c")]
    		public Int32 Achieved7c { get; set; }

			[DisplayName("Achieved7d")]
    		public Int32 Achieved7d { get; set; }

			[DisplayName("Achieved7e")]
    		public Int32 Achieved7e { get; set; }

			[DisplayName("Achieved7f")]
    		public Int32 Achieved7f { get; set; }

			[DisplayName("Observation7a")]
			[StringLength(100)]
    		public String Observation7a { get; set; }

			[DisplayName("Observation7b")]
			[StringLength(100)]
    		public String Observation7b { get; set; }

			[DisplayName("Observation7c")]
			[StringLength(100)]
    		public String Observation7c { get; set; }

			[DisplayName("Observation7d")]
			[StringLength(100)]
    		public String Observation7d { get; set; }

			[DisplayName("Observation7e")]
			[StringLength(100)]
    		public String Observation7e { get; set; }

			[DisplayName("Observation7f")]
			[StringLength(100)]
    		public String Observation7f { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_08Metadata))]
	public partial class TwentyOnePointAudit_08
	{
		public sealed class TwentyOnePointAudit_08Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved8a")]
    		public Int32 Achieved8a { get; set; }

			[DisplayName("Achieved8b")]
    		public Int32 Achieved8b { get; set; }

			[DisplayName("Achieved8c")]
    		public Int32 Achieved8c { get; set; }

			[DisplayName("Achieved8d")]
    		public Int32 Achieved8d { get; set; }

			[DisplayName("Achieved8e")]
    		public Int32 Achieved8e { get; set; }

			[DisplayName("Achieved8f")]
    		public Int32 Achieved8f { get; set; }

			[DisplayName("Achieved8g")]
    		public Int32 Achieved8g { get; set; }

			[DisplayName("Achieved8h")]
    		public Int32 Achieved8h { get; set; }

			[DisplayName("Achieved8i")]
    		public Int32 Achieved8i { get; set; }

			[DisplayName("Observation8a")]
			[StringLength(100)]
    		public String Observation8a { get; set; }

			[DisplayName("Observation8b")]
			[StringLength(100)]
    		public String Observation8b { get; set; }

			[DisplayName("Observation8c")]
			[StringLength(100)]
    		public String Observation8c { get; set; }

			[DisplayName("Observation8d")]
			[StringLength(100)]
    		public String Observation8d { get; set; }

			[DisplayName("Observation8e")]
			[StringLength(100)]
    		public String Observation8e { get; set; }

			[DisplayName("Observation8f")]
			[StringLength(100)]
    		public String Observation8f { get; set; }

			[DisplayName("Observation8g")]
			[StringLength(100)]
    		public String Observation8g { get; set; }

			[DisplayName("Observation8h")]
			[StringLength(100)]
    		public String Observation8h { get; set; }

			[DisplayName("Observation8i")]
			[StringLength(100)]
    		public String Observation8i { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_09Metadata))]
	public partial class TwentyOnePointAudit_09
	{
		public sealed class TwentyOnePointAudit_09Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved9a")]
    		public Int32 Achieved9a { get; set; }

			[DisplayName("Achieved9b")]
    		public Int32 Achieved9b { get; set; }

			[DisplayName("Achieved9c")]
    		public Int32 Achieved9c { get; set; }

			[DisplayName("Achieved9d")]
    		public Int32 Achieved9d { get; set; }

			[DisplayName("Achieved9e")]
    		public Int32 Achieved9e { get; set; }

			[DisplayName("Achieved9f")]
    		public Int32 Achieved9f { get; set; }

			[DisplayName("Achieved9g")]
    		public Int32 Achieved9g { get; set; }

			[DisplayName("Achieved9h")]
    		public Int32 Achieved9h { get; set; }

			[DisplayName("Observation9a")]
			[StringLength(100)]
    		public String Observation9a { get; set; }

			[DisplayName("Observation9b")]
			[StringLength(100)]
    		public String Observation9b { get; set; }

			[DisplayName("Observation9c")]
			[StringLength(100)]
    		public String Observation9c { get; set; }

			[DisplayName("Observation9d")]
			[StringLength(100)]
    		public String Observation9d { get; set; }

			[DisplayName("Observation9e")]
			[StringLength(100)]
    		public String Observation9e { get; set; }

			[DisplayName("Observation9f")]
			[StringLength(100)]
    		public String Observation9f { get; set; }

			[DisplayName("Observation9g")]
			[StringLength(100)]
    		public String Observation9g { get; set; }

			[DisplayName("Observation9h")]
			[StringLength(100)]
    		public String Observation9h { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_10Metadata))]
	public partial class TwentyOnePointAudit_10
	{
		public sealed class TwentyOnePointAudit_10Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved10a")]
    		public Int32 Achieved10a { get; set; }

			[DisplayName("Achieved10b")]
    		public Int32 Achieved10b { get; set; }

			[DisplayName("Achieved10c")]
    		public Int32 Achieved10c { get; set; }

			[DisplayName("Achieved10d")]
    		public Int32 Achieved10d { get; set; }

			[DisplayName("Achieved10e")]
    		public Int32 Achieved10e { get; set; }

			[DisplayName("Achieved10f")]
    		public Int32 Achieved10f { get; set; }

			[DisplayName("Achieved10g")]
    		public Int32 Achieved10g { get; set; }

			[DisplayName("Achieved10h")]
    		public Int32 Achieved10h { get; set; }

			[DisplayName("Observation10a")]
			[StringLength(100)]
    		public String Observation10a { get; set; }

			[DisplayName("Observation10b")]
			[StringLength(100)]
    		public String Observation10b { get; set; }

			[DisplayName("Observation10c")]
			[StringLength(100)]
    		public String Observation10c { get; set; }

			[DisplayName("Observation10d")]
			[StringLength(100)]
    		public String Observation10d { get; set; }

			[DisplayName("Observation10e")]
			[StringLength(100)]
    		public String Observation10e { get; set; }

			[DisplayName("Observation10f")]
			[StringLength(100)]
    		public String Observation10f { get; set; }

			[DisplayName("Observation10g")]
			[StringLength(100)]
    		public String Observation10g { get; set; }

			[DisplayName("Observation10h")]
			[StringLength(100)]
    		public String Observation10h { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_11Metadata))]
	public partial class TwentyOnePointAudit_11
	{
		public sealed class TwentyOnePointAudit_11Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved11a")]
    		public Int32 Achieved11a { get; set; }

			[DisplayName("Achieved11b")]
    		public Int32 Achieved11b { get; set; }

			[DisplayName("Achieved11c")]
    		public Int32 Achieved11c { get; set; }

			[DisplayName("Achieved11d")]
    		public Int32 Achieved11d { get; set; }

			[DisplayName("Achieved11e")]
    		public Int32 Achieved11e { get; set; }

			[DisplayName("Achieved11f")]
    		public Int32 Achieved11f { get; set; }

			[DisplayName("Observation11a")]
			[StringLength(100)]
    		public String Observation11a { get; set; }

			[DisplayName("Observation11b")]
			[StringLength(100)]
    		public String Observation11b { get; set; }

			[DisplayName("Observation11c")]
			[StringLength(100)]
    		public String Observation11c { get; set; }

			[DisplayName("Observation11d")]
			[StringLength(100)]
    		public String Observation11d { get; set; }

			[DisplayName("Observation11e")]
			[StringLength(100)]
    		public String Observation11e { get; set; }

			[DisplayName("Observation11f")]
			[StringLength(100)]
    		public String Observation11f { get; set; }

			[DisplayName("Achieved12a")]
    		public Int32 Achieved12a { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_12Metadata))]
	public partial class TwentyOnePointAudit_12
	{
		public sealed class TwentyOnePointAudit_12Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved12a")]
    		public Int32 Achieved12a { get; set; }

			[DisplayName("Achieved12b")]
    		public Int32 Achieved12b { get; set; }

			[DisplayName("Achieved12c")]
    		public Int32 Achieved12c { get; set; }

			[DisplayName("Achieved12d")]
    		public Int32 Achieved12d { get; set; }

			[DisplayName("Achieved12e")]
    		public Int32 Achieved12e { get; set; }

			[DisplayName("Observation12a")]
			[StringLength(100)]
    		public String Observation12a { get; set; }

			[DisplayName("Observation12b")]
			[StringLength(100)]
    		public String Observation12b { get; set; }

			[DisplayName("Observation12c")]
			[StringLength(100)]
    		public String Observation12c { get; set; }

			[DisplayName("Observation12d")]
			[StringLength(100)]
    		public String Observation12d { get; set; }

			[DisplayName("Observation12e")]
			[StringLength(100)]
    		public String Observation12e { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_13Metadata))]
	public partial class TwentyOnePointAudit_13
	{
		public sealed class TwentyOnePointAudit_13Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved13a")]
    		public Int32 Achieved13a { get; set; }

			[DisplayName("Achieved13b")]
    		public Int32 Achieved13b { get; set; }

			[DisplayName("Achieved13c")]
    		public Int32 Achieved13c { get; set; }

			[DisplayName("Achieved13d")]
    		public Int32 Achieved13d { get; set; }

			[DisplayName("Achieved13e")]
    		public Int32 Achieved13e { get; set; }

			[DisplayName("Achieved13f")]
    		public Int32 Achieved13f { get; set; }

			[DisplayName("Observation13a")]
			[StringLength(100)]
    		public String Observation13a { get; set; }

			[DisplayName("Observation13b")]
			[StringLength(100)]
    		public String Observation13b { get; set; }

			[DisplayName("Observation13c")]
			[StringLength(100)]
    		public String Observation13c { get; set; }

			[DisplayName("Observation13d")]
			[StringLength(100)]
    		public String Observation13d { get; set; }

			[DisplayName("Observation13e")]
			[StringLength(100)]
    		public String Observation13e { get; set; }

			[DisplayName("Observation13f")]
			[StringLength(100)]
    		public String Observation13f { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_14Metadata))]
	public partial class TwentyOnePointAudit_14
	{
		public sealed class TwentyOnePointAudit_14Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved14a")]
    		public Int32 Achieved14a { get; set; }

			[DisplayName("Achieved14b")]
    		public Int32 Achieved14b { get; set; }

			[DisplayName("Achieved14c")]
    		public Int32 Achieved14c { get; set; }

			[DisplayName("Achieved14d")]
    		public Int32 Achieved14d { get; set; }

			[DisplayName("Achieved14e")]
    		public Int32 Achieved14e { get; set; }

			[DisplayName("Achieved14f")]
    		public Int32 Achieved14f { get; set; }

			[DisplayName("Achieved14g")]
    		public Int32 Achieved14g { get; set; }

			[DisplayName("Achieved14h")]
    		public Int32 Achieved14h { get; set; }

			[DisplayName("Observation14a")]
			[StringLength(100)]
    		public String Observation14a { get; set; }

			[DisplayName("Observation14b")]
			[StringLength(100)]
    		public String Observation14b { get; set; }

			[DisplayName("Observation14c")]
			[StringLength(100)]
    		public String Observation14c { get; set; }

			[DisplayName("Observation14d")]
			[StringLength(100)]
    		public String Observation14d { get; set; }

			[DisplayName("Observation14e")]
			[StringLength(100)]
    		public String Observation14e { get; set; }

			[DisplayName("Observation14f")]
			[StringLength(100)]
    		public String Observation14f { get; set; }

			[DisplayName("Observation14g")]
			[StringLength(100)]
    		public String Observation14g { get; set; }

			[DisplayName("Observation14h")]
			[StringLength(100)]
    		public String Observation14h { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_15Metadata))]
	public partial class TwentyOnePointAudit_15
	{
		public sealed class TwentyOnePointAudit_15Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved15a")]
    		public Int32 Achieved15a { get; set; }

			[DisplayName("Achieved15b")]
    		public Int32 Achieved15b { get; set; }

			[DisplayName("Achieved15c")]
    		public Int32 Achieved15c { get; set; }

			[DisplayName("Achieved15d")]
    		public Int32 Achieved15d { get; set; }

			[DisplayName("Achieved15e")]
    		public Int32 Achieved15e { get; set; }

			[DisplayName("Achieved15f")]
    		public Int32 Achieved15f { get; set; }

			[DisplayName("Achieved15g")]
    		public Int32 Achieved15g { get; set; }

			[DisplayName("Achieved15h")]
    		public Int32 Achieved15h { get; set; }

			[DisplayName("Achieved15i")]
    		public Int32 Achieved15i { get; set; }

			[DisplayName("Observation15a")]
			[StringLength(100)]
    		public String Observation15a { get; set; }

			[DisplayName("Observation15b")]
			[StringLength(100)]
    		public String Observation15b { get; set; }

			[DisplayName("Observation15c")]
			[StringLength(100)]
    		public String Observation15c { get; set; }

			[DisplayName("Observation15d")]
			[StringLength(100)]
    		public String Observation15d { get; set; }

			[DisplayName("Observation15e")]
			[StringLength(100)]
    		public String Observation15e { get; set; }

			[DisplayName("Observation15f")]
			[StringLength(100)]
    		public String Observation15f { get; set; }

			[DisplayName("Observation15g")]
			[StringLength(100)]
    		public String Observation15g { get; set; }

			[DisplayName("Observation15h")]
			[StringLength(100)]
    		public String Observation15h { get; set; }

			[DisplayName("Observation15i")]
			[StringLength(100)]
    		public String Observation15i { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_16Metadata))]
	public partial class TwentyOnePointAudit_16
	{
		public sealed class TwentyOnePointAudit_16Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved16a")]
    		public Int32 Achieved16a { get; set; }

			[DisplayName("Achieved16b")]
    		public Int32 Achieved16b { get; set; }

			[DisplayName("Achieved16c")]
    		public Int32 Achieved16c { get; set; }

			[DisplayName("Achieved16d")]
    		public Int32 Achieved16d { get; set; }

			[DisplayName("Achieved16e")]
    		public Int32 Achieved16e { get; set; }

			[DisplayName("Achieved16f")]
    		public Int32 Achieved16f { get; set; }

			[DisplayName("Achieved16g")]
    		public Int32 Achieved16g { get; set; }

			[DisplayName("Achieved16h")]
    		public Int32 Achieved16h { get; set; }

			[DisplayName("Achieved16i")]
    		public Int32 Achieved16i { get; set; }

			[DisplayName("Achieved16j")]
    		public Int32 Achieved16j { get; set; }

			[DisplayName("Observation16a")]
			[StringLength(100)]
    		public String Observation16a { get; set; }

			[DisplayName("Observation16b")]
			[StringLength(100)]
    		public String Observation16b { get; set; }

			[DisplayName("Observation16c")]
			[StringLength(100)]
    		public String Observation16c { get; set; }

			[DisplayName("Observation16d")]
			[StringLength(100)]
    		public String Observation16d { get; set; }

			[DisplayName("Observation16e")]
			[StringLength(100)]
    		public String Observation16e { get; set; }

			[DisplayName("Observation16f")]
			[StringLength(100)]
    		public String Observation16f { get; set; }

			[DisplayName("Observation16g")]
			[StringLength(100)]
    		public String Observation16g { get; set; }

			[DisplayName("Observation16h")]
			[StringLength(100)]
    		public String Observation16h { get; set; }

			[DisplayName("Observation16i")]
			[StringLength(100)]
    		public String Observation16i { get; set; }

			[DisplayName("Observation16j")]
			[StringLength(100)]
    		public String Observation16j { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_17Metadata))]
	public partial class TwentyOnePointAudit_17
	{
		public sealed class TwentyOnePointAudit_17Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved17a")]
    		public Int32 Achieved17a { get; set; }

			[DisplayName("Achieved17b")]
    		public Int32 Achieved17b { get; set; }

			[DisplayName("Achieved17c")]
    		public Int32 Achieved17c { get; set; }

			[DisplayName("Achieved17d")]
    		public Int32 Achieved17d { get; set; }

			[DisplayName("Achieved17e")]
    		public Int32 Achieved17e { get; set; }

			[DisplayName("Achieved17f")]
    		public Int32 Achieved17f { get; set; }

			[DisplayName("Achieved17g")]
    		public Int32 Achieved17g { get; set; }

			[DisplayName("Achieved17h")]
    		public Int32 Achieved17h { get; set; }

			[DisplayName("Achieved17i")]
    		public Int32 Achieved17i { get; set; }

			[DisplayName("Achieved17j")]
    		public Int32 Achieved17j { get; set; }

			[DisplayName("Achieved17k")]
    		public Int32 Achieved17k { get; set; }

			[DisplayName("Achieved17l")]
    		public Int32 Achieved17l { get; set; }

			[DisplayName("Achieved17m")]
    		public Int32 Achieved17m { get; set; }

			[DisplayName("Observation17a")]
			[StringLength(100)]
    		public String Observation17a { get; set; }

			[DisplayName("Observation17b")]
			[StringLength(100)]
    		public String Observation17b { get; set; }

			[DisplayName("Observation17c")]
			[StringLength(100)]
    		public String Observation17c { get; set; }

			[DisplayName("Observation17d")]
			[StringLength(100)]
    		public String Observation17d { get; set; }

			[DisplayName("Observation17e")]
			[StringLength(100)]
    		public String Observation17e { get; set; }

			[DisplayName("Observation17f")]
			[StringLength(100)]
    		public String Observation17f { get; set; }

			[DisplayName("Observation17g")]
			[StringLength(100)]
    		public String Observation17g { get; set; }

			[DisplayName("Observation17h")]
			[StringLength(100)]
    		public String Observation17h { get; set; }

			[DisplayName("Observation17i")]
			[StringLength(100)]
    		public String Observation17i { get; set; }

			[DisplayName("Observation17j")]
			[StringLength(100)]
    		public String Observation17j { get; set; }

			[DisplayName("Observation17k")]
			[StringLength(100)]
    		public String Observation17k { get; set; }

			[DisplayName("Observation17l")]
			[StringLength(100)]
    		public String Observation17l { get; set; }

			[DisplayName("Observation17m")]
			[StringLength(100)]
    		public String Observation17m { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_18Metadata))]
	public partial class TwentyOnePointAudit_18
	{
		public sealed class TwentyOnePointAudit_18Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved18a")]
    		public Int32 Achieved18a { get; set; }

			[DisplayName("Achieved18b")]
    		public Int32 Achieved18b { get; set; }

			[DisplayName("Achieved18c")]
    		public Int32 Achieved18c { get; set; }

			[DisplayName("Achieved18d")]
    		public Int32 Achieved18d { get; set; }

			[DisplayName("Achieved18e")]
    		public Int32 Achieved18e { get; set; }

			[DisplayName("Achieved18f")]
    		public Int32 Achieved18f { get; set; }

			[DisplayName("Achieved18g")]
    		public Int32 Achieved18g { get; set; }

			[DisplayName("Achieved18h")]
    		public Int32 Achieved18h { get; set; }

			[DisplayName("Achieved18i")]
    		public Int32 Achieved18i { get; set; }

			[DisplayName("Achieved18j")]
    		public Int32 Achieved18j { get; set; }

			[DisplayName("Achieved18k")]
    		public Int32 Achieved18k { get; set; }

			[DisplayName("Achieved18l")]
    		public Int32 Achieved18l { get; set; }

			[DisplayName("Achieved18m")]
    		public Int32 Achieved18m { get; set; }

			[DisplayName("Achieved18n")]
    		public Int32 Achieved18n { get; set; }

			[DisplayName("Achieved18o")]
    		public Int32 Achieved18o { get; set; }

			[DisplayName("Achieved18p")]
    		public Int32 Achieved18p { get; set; }

			[DisplayName("Observation18a")]
			[StringLength(100)]
    		public String Observation18a { get; set; }

			[DisplayName("Observation18b")]
			[StringLength(100)]
    		public String Observation18b { get; set; }

			[DisplayName("Observation18c")]
			[StringLength(100)]
    		public String Observation18c { get; set; }

			[DisplayName("Observation18d")]
			[StringLength(100)]
    		public String Observation18d { get; set; }

			[DisplayName("Observation18e")]
			[StringLength(100)]
    		public String Observation18e { get; set; }

			[DisplayName("Observation18f")]
			[StringLength(100)]
    		public String Observation18f { get; set; }

			[DisplayName("Observation18g")]
			[StringLength(100)]
    		public String Observation18g { get; set; }

			[DisplayName("Observation18h")]
			[StringLength(100)]
    		public String Observation18h { get; set; }

			[DisplayName("Observation18i")]
			[StringLength(100)]
    		public String Observation18i { get; set; }

			[DisplayName("Observation18j")]
			[StringLength(100)]
    		public String Observation18j { get; set; }

			[DisplayName("Observation18k")]
			[StringLength(100)]
    		public String Observation18k { get; set; }

			[DisplayName("Observation18l")]
			[StringLength(100)]
    		public String Observation18l { get; set; }

			[DisplayName("Observation18m")]
			[StringLength(100)]
    		public String Observation18m { get; set; }

			[DisplayName("Observation18n")]
			[StringLength(100)]
    		public String Observation18n { get; set; }

			[DisplayName("Observation18o")]
			[StringLength(100)]
    		public String Observation18o { get; set; }

			[DisplayName("Observation18p")]
			[StringLength(100)]
    		public String Observation18p { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_19Metadata))]
	public partial class TwentyOnePointAudit_19
	{
		public sealed class TwentyOnePointAudit_19Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved19a")]
    		public Int32 Achieved19a { get; set; }

			[DisplayName("Achieved19b")]
    		public Int32 Achieved19b { get; set; }

			[DisplayName("Achieved19c")]
    		public Int32 Achieved19c { get; set; }

			[DisplayName("Achieved19d")]
    		public Int32 Achieved19d { get; set; }

			[DisplayName("Achieved19e")]
    		public Int32 Achieved19e { get; set; }

			[DisplayName("Achieved19f")]
    		public Int32 Achieved19f { get; set; }

			[DisplayName("Observation19a")]
			[StringLength(100)]
    		public String Observation19a { get; set; }

			[DisplayName("Observation19b")]
			[StringLength(100)]
    		public String Observation19b { get; set; }

			[DisplayName("Observation19c")]
			[StringLength(100)]
    		public String Observation19c { get; set; }

			[DisplayName("Observation19d")]
			[StringLength(100)]
    		public String Observation19d { get; set; }

			[DisplayName("Observation19e")]
			[StringLength(100)]
    		public String Observation19e { get; set; }

			[DisplayName("Observation19f")]
			[StringLength(100)]
    		public String Observation19f { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_20Metadata))]
	public partial class TwentyOnePointAudit_20
	{
		public sealed class TwentyOnePointAudit_20Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved20a")]
    		public Int32 Achieved20a { get; set; }

			[DisplayName("Achieved20b")]
    		public Int32 Achieved20b { get; set; }

			[DisplayName("Achieved20c")]
    		public Int32 Achieved20c { get; set; }

			[DisplayName("Achieved20d")]
    		public Int32 Achieved20d { get; set; }

			[DisplayName("Achieved20e")]
    		public Int32 Achieved20e { get; set; }

			[DisplayName("Observation20a")]
			[StringLength(100)]
    		public String Observation20a { get; set; }

			[DisplayName("Observation20b")]
			[StringLength(100)]
    		public String Observation20b { get; set; }

			[DisplayName("Observation20c")]
			[StringLength(100)]
    		public String Observation20c { get; set; }

			[DisplayName("Observation20d")]
			[StringLength(100)]
    		public String Observation20d { get; set; }

			[DisplayName("Observation20e")]
			[StringLength(100)]
    		public String Observation20e { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_21Metadata))]
	public partial class TwentyOnePointAudit_21
	{
		public sealed class TwentyOnePointAudit_21Metadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Achieved21a")]
    		public Int32 Achieved21a { get; set; }

			[DisplayName("Achieved21b")]
    		public Int32 Achieved21b { get; set; }

			[DisplayName("Achieved21c")]
    		public Int32 Achieved21c { get; set; }

			[DisplayName("Achieved21d")]
    		public Int32 Achieved21d { get; set; }

			[DisplayName("Achieved21e")]
    		public Int32 Achieved21e { get; set; }

			[DisplayName("Achieved21f")]
    		public Int32 Achieved21f { get; set; }

			[DisplayName("Achieved21g")]
    		public Int32 Achieved21g { get; set; }

			[DisplayName("Achieved21h")]
    		public Int32 Achieved21h { get; set; }

			[DisplayName("Achieved21i")]
    		public Int32 Achieved21i { get; set; }

			[DisplayName("Observation21a")]
			[StringLength(100)]
    		public String Observation21a { get; set; }

			[DisplayName("Observation21b")]
			[StringLength(100)]
    		public String Observation21b { get; set; }

			[DisplayName("Observation21c")]
			[StringLength(100)]
    		public String Observation21c { get; set; }

			[DisplayName("Observation21d")]
			[StringLength(100)]
    		public String Observation21d { get; set; }

			[DisplayName("Observation21e")]
			[StringLength(100)]
    		public String Observation21e { get; set; }

			[DisplayName("Observation21f")]
			[StringLength(100)]
    		public String Observation21f { get; set; }

			[DisplayName("Observation21g")]
			[StringLength(100)]
    		public String Observation21g { get; set; }

			[DisplayName("Observation21h")]
			[StringLength(100)]
    		public String Observation21h { get; set; }

			[DisplayName("Observation21i")]
			[StringLength(100)]
    		public String Observation21i { get; set; }

			[DisplayName("Total Score")]
    		public Int32 TotalScore { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

		}
	}
	
	[MetadataType(typeof(TwentyOnePointAudit_QtrMetadata))]
	public partial class TwentyOnePointAudit_Qtr
	{
		public sealed class TwentyOnePointAudit_QtrMetadata //: IEntity
		{
		
			[DisplayName("qtr")]
			[Required(ErrorMessage="qtr is required")]
    		public Int32 qtrId { get; set; }

			[DisplayName("qtr Name")]
			[Required(ErrorMessage="qtr Name is required")]
			[StringLength(100)]
    		public String qtrName { get; set; }

			[DisplayName("qtr Desc")]
			[Required(ErrorMessage="qtr Desc is required")]
			[StringLength(100)]
    		public String qtrDesc { get; set; }

		}
	}
	
	[MetadataType(typeof(UpdateReasonTypeMetadata))]
	public partial class UpdateReasonType
	{
		public sealed class UpdateReasonTypeMetadata //: IEntity
		{
		
			[DisplayName("Update Reason Type")]
			[Required(ErrorMessage="Update Reason Type is required")]
    		public Int32 UpdateReasonTypeId { get; set; }

			[DisplayName("Update Reason Type Desc")]
			[Required(ErrorMessage="Update Reason Type Desc is required")]
			[StringLength(20)]
    		public String UpdateReasonTypeDesc { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews { get; set; }

		}
	}
	
	[MetadataType(typeof(UsefulLinkMetadata))]
	public partial class UsefulLink
	{
		public sealed class UsefulLinkMetadata //: IEntity
		{
		
			[DisplayName("Useful Link")]
			[Required(ErrorMessage="Useful Link is required")]
    		public Int32 UsefulLinkId { get; set; }

			[DisplayName("Navigate Url")]
			[Required(ErrorMessage="Navigate Url is required")]
			[StringLength(255)]
			[DataType(DataType.Url)]
    		public String NavigateUrl { get; set; }

			[DisplayName("Target")]
			[StringLength(50)]
    		public String Target { get; set; }

			[DisplayName("Text")]
			[Required(ErrorMessage="Text is required")]
			[StringLength(50)]
    		public String Text { get; set; }

			[DisplayName("Is Visible")]
			[Required(ErrorMessage="Is Visible is required")]
    		public Boolean IsVisible { get; set; }

			[DisplayName("Ordinal")]
			[Required(ErrorMessage="Ordinal is required")]
    		public Int32 Ordinal { get; set; }

		}
	}
	
	[MetadataType(typeof(UserMetadata))]
	public partial class User
	{
		public sealed class UserMetadata //: IEntity
		{
		
			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("User Logon")]
			[Required(ErrorMessage="User Logon is required")]
			[StringLength(255)]
    		public String UserLogon { get; set; }

			[DisplayName("Last Name")]
			[Required(ErrorMessage="Last Name is required")]
			[StringLength(100)]
    		public String LastName { get; set; }

			[DisplayName("First Name")]
			[Required(ErrorMessage="First Name is required")]
			[StringLength(100)]
    		public String FirstName { get; set; }

			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Title")]
			[StringLength(100)]
    		public String Title { get; set; }

			[DisplayName("Phone No")]
			[StringLength(100)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[DisplayName("Fax No")]
			[StringLength(100)]
    		public String FaxNo { get; set; }

			[DisplayName("Mob No")]
			[StringLength(100)]
    		public String MobNo { get; set; }

			[DisplayName("Email")]
			[Required(ErrorMessage="Email is required")]
			[StringLength(100)]
			[DataType(DataType.EmailAddress)]
    		public String Email { get; set; }

			[DisplayName("Role")]
			[Required(ErrorMessage="Role is required")]
    		public Int32 RoleId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

    		public EntityCollection<AccessLog> AccessLogs { get; set; }

    		public EntityCollection<AdminAuditScore> AdminAuditScores { get; set; }

    		public EntityCollection<AdminTask> AdminTasks { get; set; }

    		public EntityCollection<Company> Companies { get; set; }

    		public EntityCollection<Company> Company { get; set; }

    		public EntityCollection<CompaniesRelationship> CompaniesRelationships { get; set; }

    		public EntityCollection<CompanyNote> CompanyNotes { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards1 { get; set; }

    		public EntityCollection<CompanySiteCategoryStandard> CompanySiteCategoryStandards2 { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals { get; set; }

    		public EntityCollection<CompanyStatusChangeApproval> CompanyStatusChangeApprovals1 { get; set; }

    		public EntityCollection<ConfigSa812> ConfigSa812 { get; set; }

    		public EntityCollection<ContactsAlcoa> ContactsAlcoas { get; set; }

    		public EntityCollection<ContactsContractor> ContactsContractors { get; set; }

    		public EntityCollection<CSA> CSAs { get; set; }

    		public EntityCollection<CSA> CSAs1 { get; set; }

    		public EntityCollection<CsmsFile> CsmsFiles { get; set; }

    		public EntityCollection<CsmsFile> CsmsFiles1 { get; set; }

    		public EntityCollection<CustomReportingLayout> CustomReportingLayouts { get; set; }

    		public EntityCollection<DocumentsDownloadLog> DocumentsDownloadLogs { get; set; }

    		public EntityCollection<EHSConsultant> EHSConsultants { get; set; }

    		public EntityCollection<EscalationChainProcurement> EscalationChainProcurements { get; set; }

    		public EntityCollection<EscalationChainSafety> EscalationChainSafeties { get; set; }

    		public EntityCollection<FileDb> FileDbs { get; set; }

    		public EntityCollection<FileDb> FileDbs1 { get; set; }

    		public EntityCollection<FileDbMedicalTraining> FileDbMedicalTrainings { get; set; }

    		public EntityCollection<FileVault> FileVaults { get; set; }

    		public EntityCollection<FileVaultTable> FileVaultTables { get; set; }

    		public EntityCollection<Kpi> Kpis { get; set; }

    		public EntityCollection<KpiHelpFile> KpiHelpFiles { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires1 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires2 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires3 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires4 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires5 { get; set; }

    		public EntityCollection<Questionnaire> Questionnaires6 { get; set; }

    		public EntityCollection<QuestionnaireActionLog> QuestionnaireActionLogs { get; set; }

    		public EntityCollection<QuestionnaireComment> QuestionnaireComments { get; set; }

    		public EntityCollection<QuestionnaireComment> QuestionnaireComments1 { get; set; }

    		public EntityCollection<QuestionnaireInitialContact> QuestionnaireInitialContacts { get; set; }

    		public EntityCollection<QuestionnaireInitialContact> QuestionnaireInitialContacts1 { get; set; }

    		public EntityCollection<QuestionnaireInitialContactEmail> QuestionnaireInitialContactEmails { get; set; }

    		public EntityCollection<QuestionnaireInitialLocation> QuestionnaireInitialLocations { get; set; }

    		public EntityCollection<QuestionnaireInitialLocation> QuestionnaireInitialLocations1 { get; set; }

    		public EntityCollection<QuestionnaireInitialLocation> QuestionnaireInitialLocations2 { get; set; }

    		public EntityCollection<QuestionnaireInitialResponse> QuestionnaireInitialResponses { get; set; }

    		public EntityCollection<QuestionnaireMainAssessment> QuestionnaireMainAssessments { get; set; }

    		public EntityCollection<QuestionnaireMainAttachment> QuestionnaireMainAttachments { get; set; }

    		public EntityCollection<QuestionnaireMainResponse> QuestionnaireMainResponses { get; set; }

    		public EntityCollection<QuestionnaireServicesSelected> QuestionnaireServicesSelecteds { get; set; }

    		public EntityCollection<QuestionnaireVerificationAssessment> QuestionnaireVerificationAssessments { get; set; }

    		public EntityCollection<QuestionnaireVerificationAttachment> QuestionnaireVerificationAttachments { get; set; }

    		public EntityCollection<QuestionnaireVerificationResponse> QuestionnaireVerificationResponses { get; set; }

    		public EntityCollection<Role> Role { get; set; }

    		public EntityCollection<Site> Sites { get; set; }

    		public EntityCollection<SqExemption> SqExemptions { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits { get; set; }

    		public EntityCollection<TwentyOnePointAudit> TwentyOnePointAudits1 { get; set; }

    		public EntityCollection<UserPrivilege> UserPrivileges { get; set; }

    		public EntityCollection<UserPrivilege> UserPrivileges1 { get; set; }

    		public EntityCollection<UserPrivilege> UserPrivileges2 { get; set; }

    		public EntityCollection<User> Users1 { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<UsersPrivileged> UsersPrivilegeds { get; set; }

    		public EntityCollection<UsersPrivileged2> UsersPrivileged2 { get; set; }

    		public EntityCollection<UsersPrivileged3> UsersPrivileged3 { get; set; }

    		public EntityCollection<UsersProcurement> UsersProcurements { get; set; }

    		public EntityCollection<EHSConsultant> EHSConsultants1 { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews1 { get; set; }

    		public EntityCollection<CompanyContractorReview> CompanyContractorReviews2 { get; set; }

		}
	}
	
	[MetadataType(typeof(UserCompanyMetadata))]
	public partial class UserCompany
	{
		public sealed class UserCompanyMetadata //: IEntity
		{
		
			[DisplayName("User Company")]
			[Required(ErrorMessage="User Company is required")]
    		public Int32 UserCompanyId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Active")]
    		public Boolean Active { get; set; }

		}
	}
	
	[MetadataType(typeof(UserPrivilegeMetadata))]
	public partial class UserPrivilege
	{
		public sealed class UserPrivilegeMetadata //: IEntity
		{
		
			[DisplayName("User Privilege")]
			[Required(ErrorMessage="User Privilege is required")]
    		public Int32 UserPrivilegeId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("Privilege")]
			[Required(ErrorMessage="Privilege is required")]
    		public Int32 PrivilegeId { get; set; }

			[DisplayName("Active")]
			[Required(ErrorMessage="Active is required")]
    		public Boolean Active { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Created By User")]
			[Required(ErrorMessage="Created By User is required")]
    		public Int32 CreatedByUserId { get; set; }

			[DisplayName("Created Date")]
			[Required(ErrorMessage="Created Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime CreatedDate { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<Privilege> Privilege { get; set; }

    		public EntityCollection<User> User { get; set; }

    		public EntityCollection<User> User1 { get; set; }

    		public EntityCollection<User> User2 { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersAuditMetadata))]
	public partial class UsersAudit
	{
		public sealed class UsersAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("User")]
    		public Int32 UserId { get; set; }

			[DisplayName("User Logon")]
			[StringLength(255)]
    		public String UserLogon { get; set; }

			[DisplayName("Last Name")]
			[StringLength(100)]
    		public String LastName { get; set; }

			[DisplayName("First Name")]
			[StringLength(100)]
    		public String FirstName { get; set; }

			[DisplayName("Company")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Title")]
			[StringLength(100)]
    		public String Title { get; set; }

			[DisplayName("Phone No")]
			[StringLength(100)]
			[DataType(DataType.PhoneNumber)]
    		public String PhoneNo { get; set; }

			[DisplayName("Fax No")]
			[StringLength(100)]
    		public String FaxNo { get; set; }

			[DisplayName("Mob No")]
			[StringLength(100)]
    		public String MobNo { get; set; }

			[DisplayName("Email")]
			[StringLength(100)]
			[DataType(DataType.EmailAddress)]
    		public String Email { get; set; }

			[DisplayName("Role")]
    		public Int32 RoleId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersEbiMetadata))]
	public partial class UsersEbi
	{
		public sealed class UsersEbiMetadata //: IEntity
		{
		
			[DisplayName("Users Ebi")]
			[Required(ErrorMessage="Users Ebi is required")]
    		public Int32 UsersEbiId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersEbiAuditMetadata))]
	public partial class UsersEbiAudit
	{
		public sealed class UsersEbiAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Users Ebi")]
    		public Int32 UsersEbiId { get; set; }

			[DisplayName("User")]
    		public Int32 UserId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersPrivilegedMetadata))]
	public partial class UsersPrivileged
	{
		public sealed class UsersPrivilegedMetadata //: IEntity
		{
		
			[DisplayName("Users Privileged")]
			[Required(ErrorMessage="Users Privileged is required")]
    		public Int32 UsersPrivilegedId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersPrivileged2Metadata))]
	public partial class UsersPrivileged2
	{
		public sealed class UsersPrivileged2Metadata //: IEntity
		{
		
			[DisplayName("Users Privileged")]
			[Required(ErrorMessage="Users Privileged is required")]
    		public Int32 UsersPrivilegedId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersPrivileged3Metadata))]
	public partial class UsersPrivileged3
	{
		public sealed class UsersPrivileged3Metadata //: IEntity
		{
		
			[DisplayName("Users Privileged")]
			[Required(ErrorMessage="Users Privileged is required")]
    		public Int32 UsersPrivilegedId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersProcurementMetadata))]
	public partial class UsersProcurement
	{
		public sealed class UsersProcurementMetadata //: IEntity
		{
		
			[DisplayName("Users Procurement")]
			[Required(ErrorMessage="Users Procurement is required")]
    		public Int32 UsersProcurementId { get; set; }

			[DisplayName("User")]
			[Required(ErrorMessage="User is required")]
    		public Int32 UserId { get; set; }

			[DisplayName("Enabled")]
			[Required(ErrorMessage="Enabled is required")]
    		public Boolean Enabled { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Supervisor User")]
    		public Int32 SupervisorUserId { get; set; }

			[DisplayName("Level3 User")]
    		public Int32 Level3UserId { get; set; }

			[DisplayName("Level4 User")]
    		public Int32 Level4UserId { get; set; }

			[DisplayName("Level5 User")]
    		public Int32 Level5UserId { get; set; }

			[DisplayName("Level6 User")]
    		public Int32 Level6UserId { get; set; }

			[DisplayName("Level7 User")]
    		public Int32 Level7UserId { get; set; }

			[DisplayName("Level8 User")]
    		public Int32 Level8UserId { get; set; }

			[DisplayName("Level9 User")]
    		public Int32 Level9UserId { get; set; }

			[DisplayName("Modified By User")]
			[Required(ErrorMessage="Modified By User is required")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[Required(ErrorMessage="Modified Date is required")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

    		public EntityCollection<User> User { get; set; }

		}
	}
	
	[MetadataType(typeof(UsersProcurementAuditMetadata))]
	public partial class UsersProcurementAudit
	{
		public sealed class UsersProcurementAuditMetadata //: IEntity
		{
		
			[DisplayName("Audit")]
			[Required(ErrorMessage="Audit is required")]
    		public Int32 AuditId { get; set; }

			[DisplayName("Audited On")]
			[Required(ErrorMessage="Audited On is required")]
			[DataType(DataType.DateTime)]
    		public DateTime AuditedOn { get; set; }

			[DisplayName("Audit Event")]
			[Required(ErrorMessage="Audit Event is required")]
			[StringLength(1)]
    		public String AuditEventId { get; set; }

			[DisplayName("Users Procurement")]
    		public Int32 UsersProcurementId { get; set; }

			[DisplayName("User")]
    		public Int32 UserId { get; set; }

			[DisplayName("Enabled")]
    		public Boolean Enabled { get; set; }

			[DisplayName("Site")]
    		public Int32 SiteId { get; set; }

			[DisplayName("Region")]
    		public Int32 RegionId { get; set; }

			[DisplayName("Supervisor User")]
    		public Int32 SupervisorUserId { get; set; }

			[DisplayName("Level3 User")]
    		public Int32 Level3UserId { get; set; }

			[DisplayName("Level4 User")]
    		public Int32 Level4UserId { get; set; }

			[DisplayName("Level5 User")]
    		public Int32 Level5UserId { get; set; }

			[DisplayName("Level6 User")]
    		public Int32 Level6UserId { get; set; }

			[DisplayName("Level7 User")]
    		public Int32 Level7UserId { get; set; }

			[DisplayName("Level8 User")]
    		public Int32 Level8UserId { get; set; }

			[DisplayName("Level9 User")]
    		public Int32 Level9UserId { get; set; }

			[DisplayName("Modified By User")]
    		public Int32 ModifiedByUserId { get; set; }

			[DisplayName("Modified Date")]
			[DataType(DataType.DateTime)]
    		public DateTime ModifiedDate { get; set; }

		}
	}
	
	[MetadataType(typeof(vwCompanyContractorReviewDetailMetadata))]
	public partial class vwCompanyContractorReviewDetail
	{
		public sealed class vwCompanyContractorReviewDetailMetadata //: IEntity
		{
		
			[DisplayName("Company")]
			[Required(ErrorMessage="Company is required")]
    		public Int32 CompanyId { get; set; }

			[DisplayName("Company Name")]
			[Required(ErrorMessage="Company Name is required")]
			[StringLength(200)]
    		public String CompanyName { get; set; }

			[DisplayName("Company Abn")]
			[Required(ErrorMessage="Company Abn is required")]
			[StringLength(11)]
    		public String CompanyAbn { get; set; }

			[DisplayName("CWK Number")]
			[Required(ErrorMessage="CWK Number is required")]
    		public Decimal CWKNumber { get; set; }

			[DisplayName("Full Name")]
    		public String FullName { get; set; }

			[DisplayName("Alcoa Record Active")]
			[Required(ErrorMessage="Alcoa Record Active is required")]
    		public Boolean AlcoaRecordActive { get; set; }

			[DisplayName("Active Induction")]
			[Required(ErrorMessage="Active Induction is required")]
    		public Boolean ActiveInduction { get; set; }

			[DisplayName("Labour Class Type")]
			[Required(ErrorMessage="Labour Class Type is required")]
    		public Int32 LabourClassTypeId { get; set; }

			[DisplayName("Labour Class Type Desc")]
			[Required(ErrorMessage="Labour Class Type Desc is required")]
			[StringLength(30)]
    		public String LabourClassTypeDesc { get; set; }

			[DisplayName("Labour Class Type Is Active")]
			[Required(ErrorMessage="Labour Class Type Is Active is required")]
    		public Boolean LabourClassTypeIsActive { get; set; }

			[DisplayName("Still At Company")]
			[Required(ErrorMessage="Still At Company is required")]
    		public Boolean StillAtCompany { get; set; }

			[DisplayName("Active At Alcoa")]
			[Required(ErrorMessage="Active At Alcoa is required")]
    		public Boolean ActiveAtAlcoa { get; set; }

			[DisplayName("Comments")]
    		public String Comments { get; set; }

			[DisplayName("Changed")]
			[Required(ErrorMessage="Changed is required")]
    		public Boolean Changed { get; set; }

			[DisplayName("Processed")]
			[Required(ErrorMessage="Processed is required")]
    		public Boolean Processed { get; set; }

		}
	}
	
	[MetadataType(typeof(XXHR_ADD_TRNGMetadata))]
	public partial class XXHR_ADD_TRNG
	{
		public sealed class XXHR_ADD_TRNGMetadata //: IEntity
		{
		
			[DisplayName("FULL_NAME")]
			[StringLength(240)]
    		public String FULL_NAME { get; set; }

			[DisplayName("EMPL_")]
			[Required(ErrorMessage="EMPL_ is required")]
			[StringLength(30)]
    		public String EMPL_ID { get; set; }

			[DisplayName("COURSE_NAME")]
			[Required(ErrorMessage="COURSE_NAME is required")]
			[StringLength(80)]
    		public String COURSE_NAME { get; set; }

			[DisplayName("TRAINING_TITLE")]
			[Required(ErrorMessage="TRAINING_TITLE is required")]
			[StringLength(80)]
    		public String TRAINING_TITLE { get; set; }

			[DisplayName("COURSE_END_DATE")]
			[Required(ErrorMessage="COURSE_END_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime COURSE_END_DATE { get; set; }

			[DisplayName("EXPIRATION_DATE")]
			[StringLength(19)]
    		public String EXPIRATION_DATE { get; set; }

			[DisplayName("DURATION")]
			[StringLength(384)]
    		public String DURATION { get; set; }

			[DisplayName("DURATION_UNITS")]
			[StringLength(30)]
    		public String DURATION_UNITS { get; set; }

			[DisplayName("PER_TYPE_")]
			[Required(ErrorMessage="PER_TYPE_ is required")]
			[StringLength(80)]
    		public String PER_TYPE_ID { get; set; }

			[DisplayName("RECORD_TYPE")]
			[StringLength(19)]
    		public String RECORD_TYPE { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

		}
	}
	
	[MetadataType(typeof(XXHR_CWK_HISTORYMetadata))]
	public partial class XXHR_CWK_HISTORY
	{
		public sealed class XXHR_CWK_HISTORYMetadata //: IEntity
		{
		
			[DisplayName("CWK_NBR")]
			[Required(ErrorMessage="CWK_NBR is required")]
			[StringLength(50)]
    		public String CWK_NBR { get; set; }

			[DisplayName("FULL_NAME")]
    		public String FULL_NAME { get; set; }

			[DisplayName("LAST_NAME")]
			[Required(ErrorMessage="LAST_NAME is required")]
    		public String LAST_NAME { get; set; }

			[DisplayName("FIRST_NAME")]
    		public String FIRST_NAME { get; set; }

			[DisplayName("MIDDLE_NAMES")]
    		public String MIDDLE_NAMES { get; set; }

			[DisplayName("AOA_PREF_NAME")]
    		public String AOA_PREF_NAME { get; set; }

			[DisplayName("GENDER")]
			[StringLength(50)]
    		public String GENDER { get; set; }

			[DisplayName("LOGIN")]
    		public String LOGINID { get; set; }

			[DisplayName("PER_TYPE_")]
			[StringLength(50)]
    		public String PER_TYPE_ID { get; set; }

			[DisplayName("SERVICE_DATE")]
			[Required(ErrorMessage="SERVICE_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime SERVICE_DATE { get; set; }

			[DisplayName("ORIGINAL_DATE_OF_HIRE")]
			[DataType(DataType.DateTime)]
    		public DateTime ORIGINAL_DATE_OF_HIRE { get; set; }

			[DisplayName("TERMINATION_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime TERMINATION_DATE { get; set; }

			[DisplayName("POS_DESCR")]
    		public String POS_DESCR { get; set; }

			[DisplayName("SUPERVISOR_")]
			[StringLength(50)]
    		public String SUPERVISOR_ID { get; set; }

			[DisplayName("EFFECTIVE_START_DATE")]
			[Required(ErrorMessage="EFFECTIVE_START_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EFFECTIVE_START_DATE { get; set; }

			[DisplayName("EFFECTIVE_END_DATE")]
			[Required(ErrorMessage="EFFECTIVE_END_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime EFFECTIVE_END_DATE { get; set; }

			[DisplayName("ALCOA_PRIV")]
    		public String ALCOA_PRIV { get; set; }

			[DisplayName("LABOUR_CLASS")]
    		public String LABOUR_CLASS { get; set; }

			[DisplayName("NON_PREFF_SUPP")]
    		public String NON_PREFF_SUPP { get; set; }

			[DisplayName("SPONSOR_")]
    		public String SPONSOR_ID { get; set; }

			[DisplayName("SUPPLY_SUP")]
    		public String SUPPLY_SUP { get; set; }

			[DisplayName("LOCATION_CODE")]
    		public String LOCATION_CODE { get; set; }

			[DisplayName("ORGANIZATION_")]
			[Required(ErrorMessage="ORGANIZATION_ is required")]
    		public Decimal ORGANIZATION_ID { get; set; }

			[DisplayName("ORG_CODE")]
    		public String ORG_CODE { get; set; }

			[DisplayName("DEPT_ORG_CODE")]
			[StringLength(50)]
    		public String DEPT_ORG_CODE { get; set; }

			[DisplayName("OP_CENTRE")]
			[StringLength(50)]
    		public String OP_CENTRE { get; set; }

			[DisplayName("NAME")]
			[Required(ErrorMessage="NAME is required")]
    		public String NAME { get; set; }

			[DisplayName("LBC")]
    		public String LBC { get; set; }

			[DisplayName("DEPT")]
    		public String DEPT { get; set; }

			[DisplayName("ACCOUNT")]
    		public String ACCOUNT { get; set; }

			[DisplayName("CWK_VENDOR_")]
    		public Decimal CWK_VENDOR_ID { get; set; }

			[DisplayName("CWK_VENDOR_NO")]
    		public String CWK_VENDOR_NO { get; set; }

			[DisplayName("CWK_VENDOR_NAME")]
    		public String CWK_VENDOR_NAME { get; set; }

			[DisplayName("CWK_NON_PREFERRED_SUPP_NAME")]
    		public String CWK_NON_PREFERRED_SUPP_NAME { get; set; }

			[DisplayName("CWK_SPONSOR_")]
    		public String CWK_SPONSOR_ID { get; set; }

			[DisplayName("CWK_SUPP_SUPERVISOR_")]
    		public String CWK_SUPP_SUPERVISOR_ID { get; set; }

			[DisplayName("CWK_PRIVILEGE_FLAG")]
    		public String CWK_PRIVILEGE_FLAG { get; set; }

			[DisplayName("LAST_UPDATE_DATE_PER")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE_PER { get; set; }

			[DisplayName("LAST_UPDATE_DATE_ASS")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE_ASS { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

			[DisplayName("TITLE")]
    		public String TITLE { get; set; }

			[DisplayName("DOB")]
			[DataType(DataType.DateTime)]
    		public DateTime DOB { get; set; }

			[DisplayName("PHONE_NO")]
			[DataType(DataType.PhoneNumber)]
    		public String PHONE_NO { get; set; }

			[DisplayName("MOBILE")]
    		public String MOBILE { get; set; }

			[DisplayName("EMAIL")]
			[DataType(DataType.EmailAddress)]
    		public String EMAIL { get; set; }

			[DisplayName("ADDR_TYPE")]
    		public String ADDR_TYPE { get; set; }

			[DisplayName("ADDR_LINE_1")]
    		public String ADDR_LINE_1 { get; set; }

			[DisplayName("ADDR_LINE_2")]
    		public String ADDR_LINE_2 { get; set; }

			[DisplayName("ADDR_LINE_3")]
    		public String ADDR_LINE_3 { get; set; }

			[DisplayName("CITY")]
    		public String CITY { get; set; }

			[DisplayName("STATE")]
    		public String STATE { get; set; }

			[DisplayName("POSTCODE")]
    		public String POSTCODE { get; set; }

			[DisplayName("COUNTRY")]
    		public String COUNTRY { get; set; }

			[DisplayName("PEOPLE_GROUP")]
    		public String PEOPLE_GROUP { get; set; }

			[DisplayName("ASSIGNMENT")]
    		public String ASSIGNMENT { get; set; }

			[DisplayName("PLACEMENT_START_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime PLACEMENT_START_DATE { get; set; }

			[DisplayName("ASSIGNMENT_NO")]
    		public String ASSIGNMENT_NO { get; set; }

			[DisplayName("ASSIGNMENT_STATUS")]
    		public String ASSIGNMENT_STATUS { get; set; }

			[DisplayName("ASSIGNMENT_CAT")]
    		public String ASSIGNMENT_CAT { get; set; }

			[DisplayName("OLD_CWK_NBR")]
    		public String OLD_CWK_NBR { get; set; }

			[DisplayName("PROJ_CONTRACT_END_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime PROJ_CONTRACT_END_DATE { get; set; }

			[DisplayName("SUPPLIER_SUP_ON_SITE")]
    		public String SUPPLIER_SUP_ON_SITE { get; set; }

			[DisplayName("LAST_UPDATED_BY")]
    		public String LAST_UPDATED_BY { get; set; }

			[DisplayName("LABOUR_CLASS_2")]
			[StringLength(50)]
    		public String LABOUR_CLASS_2 { get; set; }

			[DisplayName("LABOUR_CLASS_3")]
			[StringLength(50)]
    		public String LABOUR_CLASS_3 { get; set; }

		}
	}
	
	[MetadataType(typeof(XXHR_ENR_TRNGMetadata))]
	public partial class XXHR_ENR_TRNG
	{
		public sealed class XXHR_ENR_TRNGMetadata //: IEntity
		{
		
			[DisplayName("FULL_NAME")]
    		public String FULL_NAME { get; set; }

			[DisplayName("EMPL_")]
			[Required(ErrorMessage="EMPL_ is required")]
			[StringLength(50)]
    		public String EMPL_ID { get; set; }

			[DisplayName("COURSE_NAME")]
			[Required(ErrorMessage="COURSE_NAME is required")]
			[StringLength(80)]
    		public String COURSE_NAME { get; set; }

			[DisplayName("TRAINING_TITLE")]
			[Required(ErrorMessage="TRAINING_TITLE is required")]
			[StringLength(80)]
    		public String TRAINING_TITLE { get; set; }

			[DisplayName("COURSE_END_DATE")]
			[Required(ErrorMessage="COURSE_END_DATE is required")]
			[DataType(DataType.DateTime)]
    		public DateTime COURSE_END_DATE { get; set; }

			[DisplayName("EXPIRATION_DATE")]
    		public String EXPIRATION_DATE { get; set; }

			[DisplayName("PER_TYPE_")]
			[Required(ErrorMessage="PER_TYPE_ is required")]
			[StringLength(50)]
    		public String PER_TYPE_ID { get; set; }

			[DisplayName("RECORD_TYPE")]
			[StringLength(10)]
    		public String RECORD_TYPE { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

		}
	}
	
	[MetadataType(typeof(XXHR_INTEGRATION_JOBSMetadata))]
	public partial class XXHR_INTEGRATION_JOBS
	{
		public sealed class XXHR_INTEGRATION_JOBSMetadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Decimal ID { get; set; }

			[DisplayName("JOB_NAME")]
			[Required(ErrorMessage="JOB_NAME is required")]
			[StringLength(50)]
    		public String JOB_NAME { get; set; }

			[DisplayName("LAST_RUN_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_RUN_DATE { get; set; }

			[DisplayName("RESULT")]
			[Required(ErrorMessage="RESULT is required")]
			[StringLength(50)]
    		public String RESULT { get; set; }

		}
	}
	
	[MetadataType(typeof(XXHR_PEOPLE_DETAILSMetadata))]
	public partial class XXHR_PEOPLE_DETAILS
	{
		public sealed class XXHR_PEOPLE_DETAILSMetadata //: IEntity
		{
		
			[DisplayName("EMPL")]
			[Required(ErrorMessage="EMPL is required")]
			[StringLength(50)]
    		public String EMPLID { get; set; }

			[DisplayName("PER_TYPE_")]
			[StringLength(50)]
    		public String PER_TYPE_ID { get; set; }

			[DisplayName("FULL_NAME")]
    		public String FULL_NAME { get; set; }

			[DisplayName("SGNAME")]
    		public String SGNAME { get; set; }

			[DisplayName("LAST_NAME_SRCH")]
    		public String LAST_NAME_SRCH { get; set; }

			[DisplayName("FIRST_NAME_SRCH")]
    		public String FIRST_NAME_SRCH { get; set; }

			[DisplayName("SUPERVISOR_")]
			[StringLength(50)]
    		public String SUPERVISOR_ID { get; set; }

			[DisplayName("POS_DESCR")]
    		public String POS_DESCR { get; set; }

			[DisplayName("PAYGROUP")]
			[StringLength(50)]
    		public String PAYGROUP { get; set; }

			[DisplayName("LOCATION")]
    		public String LOCATION { get; set; }

			[DisplayName("COMPANY")]
			[StringLength(50)]
    		public String COMPANY { get; set; }

			[DisplayName("SEX")]
			[StringLength(50)]
    		public String SEX { get; set; }

			[DisplayName("LBC")]
			[StringLength(50)]
    		public String LBC { get; set; }

			[DisplayName("DEPT_CD")]
			[StringLength(50)]
    		public String DEPT_CD { get; set; }

			[DisplayName("ACCOUNT")]
			[StringLength(50)]
    		public String ACCOUNT { get; set; }

			[DisplayName("SERVICE_DT")]
			[Required(ErrorMessage="SERVICE_DT is required")]
			[DataType(DataType.DateTime)]
    		public DateTime SERVICE_DT { get; set; }

			[DisplayName("TERMINATION_DT")]
			[DataType(DataType.DateTime)]
    		public DateTime TERMINATION_DT { get; set; }

			[DisplayName("AOA_PREF_NAME")]
			[StringLength(50)]
    		public String AOA_PREF_NAME { get; set; }

			[DisplayName("LOGIN")]
			[StringLength(50)]
    		public String LOGINID { get; set; }

			[DisplayName("ORG_CODE")]
			[StringLength(50)]
    		public String ORG_CODE { get; set; }

			[DisplayName("DEPT_ORG_CODE")]
			[StringLength(10)]
    		public String DEPT_ORG_CODE { get; set; }

			[DisplayName("OP_CENTRE")]
    		public String OP_CENTRE { get; set; }

			[DisplayName("CWK_VENDOR_NO")]
    		public String CWK_VENDOR_NO { get; set; }

			[DisplayName("CWK_VENDOR_NAME")]
    		public String CWK_VENDOR_NAME { get; set; }

			[DisplayName("CWK_NON_PREFERRED_SUPP_NAME")]
    		public String CWK_NON_PREFERRED_SUPP_NAME { get; set; }

			[DisplayName("CWK_SPONSOR_")]
    		public String CWK_SPONSOR_ID { get; set; }

			[DisplayName("CWK_SUPP_SUPERVISOR_")]
    		public String CWK_SUPP_SUPERVISOR_ID { get; set; }

			[DisplayName("CWL_PRIVILEGE_FLAG")]
    		public String CWL_PRIVILEGE_FLAG { get; set; }

			[DisplayName("LAST_UPDATE_DATE")]
			[DataType(DataType.DateTime)]
    		public DateTime LAST_UPDATE_DATE { get; set; }

		}
	}
	
	[MetadataType(typeof(YearlyTargetMetadata))]
	public partial class YearlyTarget
	{
		public sealed class YearlyTargetMetadata //: IEntity
		{
		
			[DisplayName("ID")]
			[Required(ErrorMessage="ID is required")]
    		public Int32 ID { get; set; }

			[DisplayName("Year")]
			[Required(ErrorMessage="Year is required")]
    		public Int32 Year { get; set; }

			[DisplayName("EHS Audits")]
    		public Int32 EHSAudits { get; set; }

			[DisplayName("Workplace Safety Compliance")]
    		public Int32 WorkplaceSafetyCompliance { get; set; }

			[DisplayName("Health Safety Work Contacts")]
    		public Int32 HealthSafetyWorkContacts { get; set; }

			[DisplayName("Behavioural Safety Program")]
    		public Decimal BehaviouralSafetyProgram { get; set; }

			[DisplayName("Quarterly Audit Score")]
    		public Int32 QuarterlyAuditScore { get; set; }

			[DisplayName("Toolbox Meetings Per Month")]
    		public Int32 ToolboxMeetingsPerMonth { get; set; }

			[DisplayName("Alcoa Weekly Contractors Meeting")]
    		public Int32 AlcoaWeeklyContractorsMeeting { get; set; }

			[DisplayName("Alcoa Monthly Contractors Meeting")]
    		public Int32 AlcoaMonthlyContractorsMeeting { get; set; }

			[DisplayName("Safety Plan")]
    		public Int32 SafetyPlan { get; set; }

			[DisplayName("JSA Score")]
    		public Int32 JSAScore { get; set; }

			[DisplayName("Fatality Prevention")]
    		public Int32 FatalityPrevention { get; set; }

			[DisplayName("TRIFR")]
    		public Decimal TRIFR { get; set; }

			[DisplayName("AIFR")]
    		public Decimal AIFR { get; set; }

			[DisplayName("Training")]
    		public Int32 Training { get; set; }

			[DisplayName("Medical Schedule")]
    		public Int32 MedicalSchedule { get; set; }

			[DisplayName("Training Schedule")]
    		public Int32 TrainingSchedule { get; set; }

		}
	}
	
	
}

