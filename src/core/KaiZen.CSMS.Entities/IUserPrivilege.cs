﻿using System;
using System.ComponentModel;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	///		The data structure representation of the 'UserPrivilege' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IUserPrivilege 
	{
		/// <summary>			
		/// UserPrivilegeId : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "UserPrivilege"</remarks>
		System.Int32 UserPrivilegeId { get; set; }
				
		
		
		/// <summary>
		/// UserId : 
		/// </summary>
		System.Int32  UserId  { get; set; }
		
		/// <summary>
		/// PrivilegeId : 
		/// </summary>
		System.Int32  PrivilegeId  { get; set; }
		
		/// <summary>
		/// Active : 
		/// </summary>
		System.Boolean  Active  { get; set; }
		
		/// <summary>
		/// RegionId : 
		/// </summary>
		System.Int32?  RegionId  { get; set; }
		
		/// <summary>
		/// SiteId : 
		/// </summary>
		System.Int32?  SiteId  { get; set; }
		
		/// <summary>
		/// CreatedByUserId : 
		/// </summary>
		System.Int32  CreatedByUserId  { get; set; }
		
		/// <summary>
		/// CreatedDate : 
		/// </summary>
		System.DateTime  CreatedDate  { get; set; }
		
		/// <summary>
		/// ModifiedByUserId : 
		/// </summary>
		System.Int32  ModifiedByUserId  { get; set; }
		
		/// <summary>
		/// ModifiedDate : 
		/// </summary>
		System.DateTime  ModifiedDate  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


