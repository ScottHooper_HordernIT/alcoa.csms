﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/user.master" AutoEventWireup="true" CodeBehind="Testing.aspx.cs" Inherits="ALCOA.CSMS.Website.Testing" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" Runat="Server">
 
 
    <div>
    
        <dx:ASPxGridView ID="grid" runat="server" AutoGenerateColumns="False" 
            DataSourceID="AccessDataSource1" KeyFieldName="UserId" onhtmlrowcreated="grid_Dash_HtmlRowCreated">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="UserId" ReadOnly="True" Width="300px" 
                    VisibleIndex="0" Name="UserId">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="LastName" VisibleIndex="1" Width="300px" Name="LastName" >
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="FirstName" VisibleIndex="2" Width="300px" Name="FirstName">
                </dx:GridViewDataTextColumn>
            </Columns>
            <Templates>
                <TitlePanel>
                    <table width="100%" border="2" style="border-color:Yellow; background-color:Olive; border-style: inset">
                        <tr width="100%">
                            <td>Personnel</td>
                            <td colspan="2">Person Info</td>
                        </tr>
                        <tr width="100%">
                            <td Width="300px" >UserId</td>
                            <td Width="300px" >LastName</td>
                            <td Width="300px" >FirstName</td>
                        </tr>
                    </table>
                </TitlePanel>
            </Templates>
            <Settings ShowColumnHeaders="false" ShowTitlePanel="true" />
        </dx:ASPxGridView>
        

        <asp:SqlDataSource ID="AccessDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="Select UserId,LastName,FirstName from Users">
</asp:SqlDataSource>

          
    </div>
    


</asp:Content>


