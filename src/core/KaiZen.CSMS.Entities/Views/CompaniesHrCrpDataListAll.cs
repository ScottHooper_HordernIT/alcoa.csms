﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'CompaniesHrCrpData_ListAll' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CompaniesHrCrpDataListAll : CompaniesHrCrpDataListAllBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CompaniesHrCrpDataListAll"/> instance.
		///</summary>
		public CompaniesHrCrpDataListAll():base(){}	
		
		#endregion
	}
}
