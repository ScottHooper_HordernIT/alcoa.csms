﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;

//Added By Bishwajit For Item#14
using KaiZen.CSMS.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
//End Added By Bishwajit For Item#14

public partial class login : System.Web.UI.Page
{
    Configuration configuration = new Configuration();
    Auth auth = new Auth();
    string userEmail;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //Added for Item#14
                bool ifUserValid = false;
                //End Added for Item#14

                if (!auth.isAlcoaDirect)
                {
                    auth.authAlcoaLan();

                    CompaniesService cService = new CompaniesService();
                    Companies c = cService.GetByCompanyId(auth.CompanyId);

                    if (auth.RoleId == (int)RoleList.Disabled || c.Deactivated == true)
                    {
                        AccountDisabled();
                    }
                    else
                    {
                        if (!auth.isValid) { NotAllowed(); }
                        else
                        {
                            //Addedfor Item#14
                            tblAuth.Visible = false;
                            ifUserValid = true;
                            //End Addedfor Item#14

                            //Commented by Bishwajit Sahoo to implement Multiple companies for a user Option #Item14
                            //auth.getUserDetails();
                            //SessionHandler.LoggedIn = "Yes";
                            //SessionHandler.UserLogon = auth.UserLogon;
                            //SessionHandler.UserId = auth.UserId.ToString();
                            //SessionHandler.UserLogon_UserEmail = userEmail;
                            //SessionHandler.CompanyId = auth.CompanyId.ToString();

                            //SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                            //SessionHandler.spVar_UserId = auth.UserId.ToString();

                            ////TODO: Implement 'Site Offline Module'
                            //if (auth.RoleId != (int)RoleList.Administrator)
                            //{
                            //    CheckIfOffline();
                            //}

                            //if (auth.RoleId == (int)RoleList.PreQual)
                            //{
                            //    Response.Redirect("default.aspx");
                            //}
                            //else
                            //{
                            //    if (Request.QueryString["redirectfrom"] != null)
                            //    {

                            //    }
                            //    else
                            //    {
                            //        Response.Redirect("news.aspx");
                            //    }
                            //}
                            //End Commented by Bishwajit Sahoo to implement Multiple companies for a user Option #Item14
                        }
                    }
                }
                else //Alcoa Direct
                {
                    userEmail = Request.QueryString[auth._AlcoaDirectHTTPParameter];
                    userEmail = HttpUtility.UrlDecode(userEmail);
                    userEmail = userEmail.Replace(" ", "+");
                    if (userEmail != null)
                    {
                        //lblStatus.Text = "Logging In...";
                        lblStatus.Text = "";
                        userEmail = auth.Decrypt(userEmail);
                    }
                    auth.authAlcoaDirect(userEmail);


                    if (!auth.isValid) { NotAllowed(); } //AlcoaDirect User Not Valid
                    else
                    {
                        //Addedfor Item#14
                        ifUserValid = true;
                        //End Addedfor Item#14

                        //Commented by Bishwajit Sahoo to implement Multiple companies for a user Option: Item#14---------------

                        //auth.getUserDetails();
                        //CompaniesService cService = new CompaniesService();
                        //Companies c = cService.GetByCompanyId(auth.CompanyId);
                        //if (auth.RoleId == (int)RoleList.Disabled || c.Deactivated == true)
                        //{
                        //    AccountDisabled();
                        //}
                        //else
                        //{
                        //    SessionHandler.LoggedIn = "Yes";
                        //    SessionHandler.UserLogon = auth.UserLogon;
                        //    SessionHandler.UserLogon_UserEmail = auth.Email;
                        //    SessionHandler.UserId = auth.UserId.ToString();
                        //    SessionHandler.CompanyId = auth.CompanyId.ToString();

                        //    SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                        //    SessionHandler.spVar_UserId = auth.UserId.ToString();
                        //    //TODO: Implement 'Site Offline Module'
                        //    if (auth.RoleId != (int)RoleList.Administrator)
                        //    {
                        //        CheckIfOffline();
                        //    }

                        //    if (auth.RoleId == (int)RoleList.PreQual)
                        //    {
                        //        Response.Redirect("default.aspx");
                        //    }
                        //    else
                        //    {
                        //        Response.Redirect("news.aspx");
                        //    }
                        //}
                        //End Commented by Bishwajit Sahoo to implement Multiple companies for a user Option: Item#14--------
                    }
                }
                //Added by Bishwajit Sahoo to implement Multiple companies for a user Option Item#14
                if (ifUserValid)
                {

                    string userLogon = auth.getUserLogon();
                    UsersCompaniesMapService ucSer = new UsersCompaniesMapService();
                    DataSet ds = ucSer.GetByUserLogon(Convert.ToString(userLogon));
                    if (ds != null)
                    {
                        if (ds.Tables[0].Rows.Count > 1)
                        {
                            DataView dv = new DataView(ds.Tables[0]);
                            dv.Sort = "CompanyName ASC";
                            DataTable dt = dv.ToTable();
                            tblAuth.Visible = true;
                            ddlCompanies.DataSource = dt;
                            ddlCompanies.TextField = "CompanyName";
                            ddlCompanies.ValueField = "CompanyId";
                            ddlCompanies.DataBind();
                            ddlCompanies.Items.Insert(0, new DevExpress.Web.ASPxEditors.ListEditItem("--Select a Company--", -1));
                            ddlCompanies.Value = -1;
                        }
                        else
                        {
                            auth.getUserDetails();
                            tblAuth.Visible = false;
                            //UsersService uSer = new UsersService();
                            //Users u = uSer.GetByUserLogon(Convert.ToString(userLogon));

                            //TransactionManager transactionManager1 = null;
                            //transactionManager1 = ConnectionScope.ValidateOrCreateTransaction(true);
                            //Users updateU = new Users();
                            //updateU.UserId = u.UserId;
                            //updateU.UserLogon = u.UserLogon;
                            //updateU.LastName = u.LastName;
                            //updateU.FirstName = u.FirstName;
                            //updateU.CompanyId = Convert.ToInt32(ds.Tables[0].Rows[0]["CompanyId"]);
                            //updateU.Title = u.Title;
                            //updateU.PhoneNo = u.PhoneNo;
                            //updateU.FaxNo = u.FaxNo;
                            //updateU.MobNo = u.MobNo;
                            //updateU.RoleId = u.RoleId;
                            //updateU.Email = u.Email;
                            //if (updateU.EntityState != EntityState.Unchanged)
                            //{
                            //    updateU.ModifiedByUserId = auth.UserId;

                            //    //conser.Save(contNew);
                            //    DataRepository.UsersProvider.Update(transactionManager1, updateU);
                            //    transactionManager1.Commit();
                            //}

                            SessionHandler.LoggedIn = "Yes";
                            SessionHandler.UserLogon = auth.UserLogon;
                            SessionHandler.UserId = auth.UserId.ToString();
                            SessionHandler.UserLogon_UserEmail = userEmail;
                            SessionHandler.CompanyId = auth.CompanyId.ToString();

                            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
                            SessionHandler.spVar_UserId = auth.UserId.ToString();


                            if (auth.RoleId != (int)RoleList.Administrator)
                            {
                                CheckIfOffline();
                            }

                            if (auth.RoleId == (int)RoleList.PreQual)
                            {
                                Response.Redirect("default.aspx");
                            }
                            else
                            {
                                if (Request.QueryString["redirectfrom"] != null)
                                {

                                }
                                else
                                {
                                    Response.Redirect("news.aspx");
                                }
                            }

                        }
                    }

                }
                else
                {
                    NotAllowed();
                }
                //End Added by Bishwajit Sahoo to implement Multiple companies for a user Option Item#14
            }
        }
        catch (Exception)
        {
            //Response.Write(ex.Message.ToString());
            NotAllowed();
        }
    }
    protected void NotAllowed()
    {
        tblAuth.Visible = false;
        lblStatus.Visible = true;
        lblStatus.Text = "You are not recognised by this system.<br />Please try again or contact <a href='mailto:AUAAlcoaContractorServices@alcoa.com.au'>AUAAlcoaContractorServices@alcoa.com.au</a> for general enquiries.";
    }
    protected void AccountDisabled()
    {
        tblAuth.Visible = false;
        lblStatus.Visible = true;
        lblStatus.Text = "Your account or company has been de-activated from logging into this system.<br />Please contact <a href='mailto:AUAAlcoaContractorServices@alcoa.com.au'>AUAAlcoaContractorServices@alcoa.com.au</a> if you believe this is incorrect and/or for general enquiries.";
    }

    protected void CheckIfOffline()
    {
        if (configuration.GetValue(ConfigList.SiteOfflineStatus) == "1") Response.Redirect("~/SiteDown.aspx");
    }
    protected void GetDetails()
    {
        auth.getUserDetails();
        //Session["Auth_currentUserIdentity"] = auth.currentUserIdentity();
        //Session["Auth_AlcoaDirectEmail"] = auth.AlcoaDirectEmail();
        //Session["Auth_UserId"] = auth.UserId();
        //Session["Auth_CompanyId"] = auth.CompanyId();
        //Session["Auth_RoleId"] = auth.RoleId();
        //Session["Auth_UserLogon"] = auth.UserLogon();
        //Session["Auth_LastName"] = auth.LastName();
        //Session["Auth_FirstName"] = auth.FirstName();
        //Session["Auth_Email"] = auth.Email();
        //Session["Auth_Role"] = auth.Role();
        //Session["Auth_CompanyName"] = auth.CompanyName();
        //Session["Auth_CompanyABN"] = auth.CompanyABN();
    }

    protected void btnClick_click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlCompanies.Value) == -1)
        {
            lblMessage.Visible = true;
            lblMessage.Text = "Please Select a Company";
        }
        else
        {

            lblMessage.Visible = false;

            if (auth.isAlcoaDirect)
            {
                userEmail = Request.QueryString[auth._AlcoaDirectHTTPParameter];
                userEmail = HttpUtility.UrlDecode(userEmail);
                userEmail = userEmail.Replace(" ", "+");
                if (userEmail != null)
                {
                    //lblStatus.Text = "Logging In...";
                    lblStatus.Text = "";
                    userEmail = auth.Decrypt(userEmail);
                }
                auth.authAlcoaDirect(userEmail);
            }




            auth.getUserDetails(Convert.ToInt32(ddlCompanies.Value));
            UserCompanyService ucSer = new UserCompanyService();
            TList<UserCompany> ucList = ucSer.GetByUserId(auth.UserId);
            foreach (UserCompany uc in ucList)
            {
                if (uc.Active == true)
                {
                    ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                    string sqldb = conString.ConnectionString;
                    using (SqlConnection cn = new SqlConnection(sqldb))
                    {
                        cn.Open();

                        SqlCommand cmd = new SqlCommand("UserCompany_Update_Grid", cn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserCompanyId", uc.UserCompanyId);
                        cmd.Parameters.AddWithValue("@UserId", uc.UserId);
                        cmd.Parameters.AddWithValue("@CompanyId", uc.CompanyId);
                        cmd.Parameters.AddWithValue("@Active", false);
                        cmd.CommandTimeout = 120;
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }

            TList<UserCompany> ucList1 = ucSer.GetByUserIdCompanyId(auth.UserId, Convert.ToInt32(ddlCompanies.Value), false);
            foreach (UserCompany uc1 in ucList1)
            {
                ConnectionStringSettings conString = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"];
                string sqldb = conString.ConnectionString;
                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    cn.Open();

                    SqlCommand cmd = new SqlCommand("UserCompany_Update_Grid", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserCompanyId", uc1.UserCompanyId);
                    cmd.Parameters.AddWithValue("@UserId", uc1.UserId);
                    cmd.Parameters.AddWithValue("@CompanyId", uc1.CompanyId);
                    cmd.Parameters.AddWithValue("@Active", true);
                    cmd.CommandTimeout = 120;
                    cmd.ExecuteNonQuery();
                    cn.Close();
                }

            }

            //UsersService uSer = new UsersService();
            //Users u = uSer.GetByUserId(auth.UserId);

            //TransactionManager transactionManager1 = null;
            //transactionManager1 = ConnectionScope.ValidateOrCreateTransaction(true);
            //Users updateU = new Users();
            //updateU.UserId = u.UserId;
            //updateU.UserLogon = u.UserLogon;
            //updateU.LastName = u.LastName;
            //updateU.FirstName = u.FirstName;
            //updateU.CompanyId = Convert.ToInt32(ddlCompanies.Value);
            //updateU.Title = u.Title;
            //updateU.PhoneNo = u.PhoneNo;
            //updateU.FaxNo = u.FaxNo;
            //updateU.MobNo = u.MobNo;
            //updateU.RoleId = u.RoleId;
            //updateU.Email = u.Email;
            //if (updateU.EntityState != EntityState.Unchanged)
            //{
            //    updateU.ModifiedByUserId = auth.UserId;

            //    //conser.Save(contNew);
            //    DataRepository.UsersProvider.Update(transactionManager1, updateU);
            //    transactionManager1.Commit();
            //}



            SessionHandler.LoggedIn = "Yes";
            SessionHandler.UserLogon = auth.UserLogon;
            SessionHandler.UserId = auth.UserId.ToString();
            SessionHandler.UserLogon_UserEmail = userEmail;
            SessionHandler.CompanyId = auth.CompanyId.ToString();

            SessionHandler.spVar_CompanyId = auth.CompanyId.ToString();
            SessionHandler.spVar_UserId = auth.UserId.ToString();


            if (auth.RoleId != (int)RoleList.Administrator)
            {
                CheckIfOffline();
            }

            if (auth.RoleId == (int)RoleList.PreQual)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                if (Request.QueryString["redirectfrom"] != null)
                {

                }
                else
                {
                    Response.Redirect("news.aspx");
                }
            }
        }


    }
}
