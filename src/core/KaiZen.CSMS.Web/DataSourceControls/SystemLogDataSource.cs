﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.SystemLogProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(SystemLogDataSourceDesigner))]
	public class SystemLogDataSource : ProviderDataSource<SystemLog, SystemLogKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogDataSource class.
		/// </summary>
		public SystemLogDataSource() : base(new SystemLogService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the SystemLogDataSourceView used by the SystemLogDataSource.
		/// </summary>
		protected SystemLogDataSourceView SystemLogView
		{
			get { return ( View as SystemLogDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the SystemLogDataSource control invokes to retrieve data.
		/// </summary>
		public SystemLogSelectMethod SelectMethod
		{
			get
			{
				SystemLogSelectMethod selectMethod = SystemLogSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (SystemLogSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the SystemLogDataSourceView class that is to be
		/// used by the SystemLogDataSource.
		/// </summary>
		/// <returns>An instance of the SystemLogDataSourceView class.</returns>
		protected override BaseDataSourceView<SystemLog, SystemLogKey> GetNewDataSourceView()
		{
			return new SystemLogDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the SystemLogDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class SystemLogDataSourceView : ProviderDataSourceView<SystemLog, SystemLogKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SystemLogDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the SystemLogDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public SystemLogDataSourceView(SystemLogDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal SystemLogDataSource SystemLogOwner
		{
			get { return Owner as SystemLogDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal SystemLogSelectMethod SelectMethod
		{
			get { return SystemLogOwner.SelectMethod; }
			set { SystemLogOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal SystemLogService SystemLogProvider
		{
			get { return Provider as SystemLogService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<SystemLog> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<SystemLog> results = null;
			SystemLog item;
			count = 0;
			
			System.Int32 _systemLogId;
			System.String _activity;
			System.DateTime _date;

			switch ( SelectMethod )
			{
				case SystemLogSelectMethod.Get:
					SystemLogKey entityKey  = new SystemLogKey();
					entityKey.Load(values);
					item = SystemLogProvider.Get(entityKey);
					results = new TList<SystemLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case SystemLogSelectMethod.GetAll:
                    results = SystemLogProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case SystemLogSelectMethod.GetPaged:
					results = SystemLogProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case SystemLogSelectMethod.Find:
					if ( FilterParameters != null )
						results = SystemLogProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = SystemLogProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case SystemLogSelectMethod.GetBySystemLogId:
					_systemLogId = ( values["SystemLogId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SystemLogId"], typeof(System.Int32)) : (int)0;
					item = SystemLogProvider.GetBySystemLogId(_systemLogId);
					results = new TList<SystemLog>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case SystemLogSelectMethod.GetByActivity:
					_activity = ( values["Activity"] != null ) ? (System.String) EntityUtil.ChangeType(values["Activity"], typeof(System.String)) : string.Empty;
					results = SystemLogProvider.GetByActivity(_activity, this.StartIndex, this.PageSize, out count);
					break;
				case SystemLogSelectMethod.GetByDateActivity:
					_date = ( values["Date"] != null ) ? (System.DateTime) EntityUtil.ChangeType(values["Date"], typeof(System.DateTime)) : DateTime.MinValue;
					_activity = ( values["Activity"] != null ) ? (System.String) EntityUtil.ChangeType(values["Activity"], typeof(System.String)) : string.Empty;
					results = SystemLogProvider.GetByDateActivity(_date, _activity, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == SystemLogSelectMethod.Get || SelectMethod == SystemLogSelectMethod.GetBySystemLogId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				SystemLog entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					SystemLogProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<SystemLog> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			SystemLogProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region SystemLogDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the SystemLogDataSource class.
	/// </summary>
	public class SystemLogDataSourceDesigner : ProviderDataSourceDesigner<SystemLog, SystemLogKey>
	{
		/// <summary>
		/// Initializes a new instance of the SystemLogDataSourceDesigner class.
		/// </summary>
		public SystemLogDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SystemLogSelectMethod SelectMethod
		{
			get { return ((SystemLogDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new SystemLogDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region SystemLogDataSourceActionList

	/// <summary>
	/// Supports the SystemLogDataSourceDesigner class.
	/// </summary>
	internal class SystemLogDataSourceActionList : DesignerActionList
	{
		private SystemLogDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the SystemLogDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public SystemLogDataSourceActionList(SystemLogDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public SystemLogSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion SystemLogDataSourceActionList
	
	#endregion SystemLogDataSourceDesigner
	
	#region SystemLogSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the SystemLogDataSource.SelectMethod property.
	/// </summary>
	public enum SystemLogSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetBySystemLogId method.
		/// </summary>
		GetBySystemLogId,
		/// <summary>
		/// Represents the GetByActivity method.
		/// </summary>
		GetByActivity,
		/// <summary>
		/// Represents the GetByDateActivity method.
		/// </summary>
		GetByDateActivity
	}
	
	#endregion SystemLogSelectMethod

	#region SystemLogFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogFilter : SqlFilter<SystemLogColumn>
	{
	}
	
	#endregion SystemLogFilter

	#region SystemLogExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogExpressionBuilder : SqlExpressionBuilder<SystemLogColumn>
	{
	}
	
	#endregion SystemLogExpressionBuilder	

	#region SystemLogProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;SystemLogChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="SystemLog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SystemLogProperty : ChildEntityProperty<SystemLogChildEntityTypes>
	{
	}
	
	#endregion SystemLogProperty
}

