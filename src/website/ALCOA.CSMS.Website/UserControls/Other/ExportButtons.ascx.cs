using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxCallbackPanel;
using DevExpress.Web.ASPxClasses;
using DevExpress.XtraPrinting;
using System.Reflection;
using System.Linq;
using System.IO;
using DevExpress.Web.ASPxGridView.Export;
using DevExpress.Web.ASPxPopupControl;

/// <summary>
/// Last Updated: 11/09/2009
/// Description: User control with 3 buttons to export an aspxgridview to rtf(doc)/pdf/xls
/// </summary>
public partial class UserControls_Tiny_ExportButtons : System.Web.UI.UserControl
{
    bool forceDownload = true;

    //implemented for Prequal user getting access denied issue
    protected override void OnInit(EventArgs e)
    {
        //ScriptManager sm = (ScriptManager)ScriptManager.GetCurrent(this.Page);
        //MethodInfo m = (
        //from methods in typeof(ScriptManager).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
        //where methods.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel")
        //select methods).First<MethodInfo>();

        ////UpdatePanel UpdatePanel1 = (UpdatePanel)this.Page.FindControl("UpdatePanel1");

        //m.Invoke(sm, new object[] { UpdatePanel1 });
        //base.OnInit(e);
    }

    ////implemented for Prequal user getting access denied issue
    protected void UpdatePanel_Unload(object sender, EventArgs e)
    {
        this.RegisterUpdatePanel(sender as UpdatePanel);
    }

    protected void RegisterUpdatePanel(UpdatePanel panel)
    {
        foreach (System.Reflection.MethodInfo methodInfo in typeof(ScriptManager).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (methodInfo.Name.Equals("System.Web.UI.IScriptManagerInternal.RegisterUpdatePanel"))
            {
                methodInfo.Invoke(ScriptManager.GetCurrent(Page), new object[] { panel });
            }
        }
    }


    protected void imgBtnWord_Click(object sender, ImageClickEventArgs e) //export to word document format 
    {
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {
                Response.Clear();
                RtfExportOptions opt = new RtfExportOptions();
                opt.ExportWatermarks = true;
                gridExporter.WriteRtfToResponse(opt);
                Response.Close();
            } 
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once." +
           "\nThere is currently over 65,000 rows being displayed." +
           "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {
            PopUpErrorMessage(ex.Message);
        }
    }
    protected void imgBtnPdf_Click(object sender, ImageClickEventArgs e) //export to adobe acrobat pdf format 
    {
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {

                Response.Clear();
                PdfExportOptions opt = new PdfExportOptions();
                opt.Compressed = true;
                opt.DocumentOptions.Title = gridExporter.FileName;
                opt.DocumentOptions.Author = "Alcoa Inc.";
                gridExporter.WritePdfToResponse(opt);
                Response.Close();
            }
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once." +
           "\nThere is currently over 65,000 rows being displayed." +
           "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {

            PopUpErrorMessage(ex.Message);
        }
    }
    protected void response_write(MemoryStream iStream, string FileName)
    {
        // Buffer to read 10K bytes in chunk:
        byte[] buffer = new Byte[10000];

        // Length of the file:
        int length;

        // Total bytes to read:
        long dataToRead;

        try
        {
            dataToRead = iStream.Length;
            if (forceDownload)
            {
                Response.ContentType = "application/download";
            }
            else
            {
                Response.ContentType = "application/octet-stream";
            }

            string ContentDisposition = String.Format(@"attachment; filename={0}", Server.UrlEncode(FileName));
            String userAgent = Request.Headers.Get("User-Agent");
            if (userAgent.Contains("MSIE 7.0"))
            {
                ContentDisposition = String.Format(@"attachment; filename={0}", FileName.Replace(" ", "%20"));
            }
            else
            {
                ContentDisposition = String.Format("attachment; filename=\"{0}\"", FileName);
            }
            Response.AppendHeader("Content-Disposition", ContentDisposition);

            // Read the bytes.
            while (dataToRead > 0)
            {
                // Verify that the client is connected.
                if (Response.IsClientConnected)
                {
                    // Read the data in buffer.
                    length = iStream.Read(buffer, 0, 10000);

                    // Write the data to the current output stream.
                    Response.OutputStream.Write(buffer, 0, length);

                    // Flush the data to the HTML output.
                    Response.Flush();

                    buffer = new Byte[10000];
                    dataToRead = dataToRead - length;
                }
                else
                {
                    //prevent infinite loop if user disconnects
                    dataToRead = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Elmah.ErrorSignal.FromContext(Context).Raise(ex);
            Response.Write("Error: " + ex.Message);
        }
        finally
        {
            iStream.Close();
            Response.Close();
        }
    }
    protected void imgBtnExcel_Click(object sender, ImageClickEventArgs e) //export to excel spreadsheet format 
    {
        try
        {
            if (gridExporter.GridView.VisibleRowCount < 65000)
            {
                Response.Clear();
                if (Session["RawDataGrid"] == "gridRawData_ByMonthYear")
                {
                    XlsxExportOptions opt = new XlsxExportOptions();
                    opt.SheetName = gridExporter.FileName;
                    opt.ShowGridLines = true;
                    opt.TextExportMode = TextExportMode.Value;
                    gridExporter.WriteXlsxToResponse(opt);
                    Session["RawDataGrid"] = null;
                }
                else
                {
                    XlsExportOptions opt = new XlsExportOptions();
                    opt.SheetName = gridExporter.FileName;
                    opt.ShowGridLines = true;
                    opt.TextExportMode = TextExportMode.Value;

                    gridExporter.WriteXlsToResponse(opt);
                }

                Response.Close();
            }
            else
            {
                PopUpErrorMessage("The Export feature only supports the displayed grid having a maximum of 65,000 rows/items being displayed at once."+
            "\nThere is currently over 65,000 rows being displayed."+
            "\nPlease try again by reducing the number of rows displayed by using appropriate filters.");
            }
        }
        catch (Exception ex)
        {
            PopUpErrorMessage(ex.Message);
        }
      
    }

    //protected void OnButtonClick(object sender, EventArgs e)
    //{
    //    Response.Clear();
    //    XlsExportOptions opt = new XlsExportOptions();
    //    opt.SheetName = gridExporter.FileName;
    //    opt.ShowGridLines = true;
    //    opt.TextExportMode = TextExportMode.Value;
    //    //opt.UseNativeFormat = true;
    //    gridExporter.WriteXlsToResponse(opt);
    //    Response.Close();
    //}

    protected void PopUpErrorMessage(string errormsg)
    {
        //ASPxGridView1.Visible = false;
        PopupWindow pcWindow = new PopupWindow(errormsg);
        pcWindow.FooterText = "";
        pcWindow.ShowOnPageLoad = true;
        pcWindow.Modal = true;
        //ASPxPopupControl ASPxPopupControl1 = (ASPxPopupControl)Page.Master.FindControl("ASPxPopupControl1");
        ASPxPopupControl1.Windows.Add(pcWindow);
        //WebChartControl1.Visible = false;
    }


    

}
