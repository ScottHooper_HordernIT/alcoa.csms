﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsersProcurementEscalation.ascx.cs" Inherits="ALCOA.CSMS.Website.UserControls.Admin.ProcurementEscalation" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register Src="../Other/ExportButtons.ascx" TagName="ExportButtons" TagPrefix="uc1" %>

<dx:ASPxGridView
    ID="gridProcurement"
    runat="server" AutoGenerateColumns="False"
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css"
    CssPostfix="Office2003Blue"
    DataSourceID="dsUsersProcurement"
    KeyFieldName="UsersProcurementId"
    OnInitNewRow="grid_InitNewRow"
    OnHtmlRowCreated="grid_RowCreated"
    OnCellEditorInitialize="grid_CellEditorInitialize"
    Width="900px"
    >
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px" Width="11px" />
        <ExpandedButton Height="12px" Width="11px" />
        <DetailCollapsedButton Height="12px" Width="11px" />
        <DetailExpandedButton Height="12px" Width="11px" />
        <LoadingPanel Url="~/App_Themes/Office2003Blue/GridView/Loading.gif">
        </LoadingPanel>
    </Images>
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
        <LoadingPanel ImageSpacing="10px" />
    </Styles>
    <Columns>
        <dx:GridViewCommandColumn Caption="Action" VisibleIndex="0" Width="120px" 
            FixedStyle="Left">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="True">
            </ClearFilterButton>
        </dx:GridViewCommandColumn>
        <dx:GridViewDataTextColumn FieldName="UsersProcurementId" ReadOnly="True" Visible="False"
            VisibleIndex="1" Width="0px">
            <EditFormSettings Visible="False" />
        </dx:GridViewDataTextColumn>
        
        <dx:GridViewDataCheckColumn Caption="Active" FieldName="Enabled" VisibleIndex="2" Width="60px">
        </dx:GridViewDataCheckColumn>

        <dx:GridViewDataComboBoxColumn
                    Caption="Name"
                    FieldName="UserId"
                    unboundType="String"
                    Settings-SortMode="DisplayText"
                    VisibleIndex="3" SortIndex="0" SortOrder="Ascending" Width="126px" FixedStyle="Left">
                    <PropertiesComboBox
                        DataSourceID="UsersDataSource"
                        DropDownHeight="150px"
                        TextField="UserDetails"
                        ValueField="UserId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                        <ValidationSettings>
                            <RegularExpression ErrorText="*" ValidationExpression="^[1-9][0-9]*$" />
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </PropertiesComboBox>

            <Settings SortMode="DisplayText"></Settings>
        </dx:GridViewDataComboBoxColumn>

        <dx:GridViewDataComboBoxColumn
                    Caption="Supervisor"
                    FieldName="SupervisorUserId"
                    unboundType="String"
                    Settings-SortMode="DisplayText"
                    VisibleIndex="4" Width="126px">
                    <PropertiesComboBox
                        DataSourceID="UsersDataSource"
                        DropDownHeight="150px"
                        TextField="UserDetails"
                        ValueField="UserId" IncrementalFilteringMode="StartsWith"
                        ValueType="System.Int32">
                        <ValidationSettings>
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </PropertiesComboBox>
                    <Settings SortMode="DisplayText"></Settings>
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Region" FieldName="RegionId"
                unboundType="String" VisibleIndex="5" Width="75px">
                <PropertiesComboBox DropDownHeight="150px" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                        <Items>
                        <dx:ListEditItem Text="ARP" Value="7" />  
                        <dx:ListEditItem Text="VICOPS" Value="4" />  
                        <dx:ListEditItem Text="WAO" Value="1" />
                        </Items>
                        <ValidationSettings>
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                        </PropertiesComboBox>
            <Settings SortMode="DisplayText"/>
        </dx:GridViewDataComboBoxColumn>
        
        <dx:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId"
                unboundType="String" VisibleIndex="6" Width="100px">
                <PropertiesComboBox DataSourceID="dsSitesFilter" DropDownHeight="150px" TextField="SiteName"
                    ValueField="SiteId" ValueType="System.Int32" IncrementalFilteringMode="StartsWith">
                    <ValidationSettings>
                        <RequiredField IsRequired="True" />
                    </ValidationSettings>
                </PropertiesComboBox>
            <Settings SortMode="DisplayText"/>
        </dx:GridViewDataComboBoxColumn>
        
        <dx:GridViewDataTextColumn Caption="# SQ Assigned as Proc Contact" 
            VisibleIndex="7" Width="115px">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:35px;" align="center"><asp:Label ID="noSQProcContact" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="# SQ Assigned as Func Proc Mngr" 
            VisibleIndex="8" Width="115px">
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            <DataItemTemplate>
                     <table cellpadding="0" cellspacing="0"><tr>
                     <td style="width:35px;" align="center"><asp:Label ID="noSQFuncProcMgr" runat="server" Text="" ></asp:Label></td>
                     </tr></table>
                 </DataItemTemplate>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 3" FieldName="Level3UserId" UnboundType="String"
            VisibleIndex="9" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 4" FieldName="Level4UserId" UnboundType="String"
            VisibleIndex="10" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 5" FieldName="Level5UserId" UnboundType="String"
            VisibleIndex="11" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 6" FieldName="Level6UserId" UnboundType="String"
            VisibleIndex="12" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 7" FieldName="Level7UserId" UnboundType="String"
            VisibleIndex="13" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 8" FieldName="Level8UserId" UnboundType="String"
            VisibleIndex="14" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        <dx:GridViewDataComboBoxColumn Caption="Level 9" FieldName="Level9UserId" UnboundType="String"
            VisibleIndex="15" Width="126px">
            <PropertiesComboBox DataSourceID="UsersDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserDetails" ValueField="UserId" ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dx:GridViewDataComboBoxColumn>
        
    </Columns>
    <SettingsPager NumericButtonCount="5" PageSize="5" AlwaysShowPager="True" >
        <AllButton Visible="True">
        </AllButton>
    </SettingsPager>
    <Settings  ShowHorizontalScrollBar="True" ShowFilterRow="true" />
    <SettingsEditing Mode="Inline" />
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dx:ASPxGridView>
<br />
<table border="0" cellpadding="0" cellspacing="0" width="900">
    <tr>
        <td style="height: 10px; text-align: right">
            <uc1:ExportButtons ID="ucExportButtons" runat="server" />
        </td>
    </tr>
</table>
<br />

<asp:SqlDataSource
	ID="UsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT TOP 1 NULL As UserId, '' As UserDetails, '' As LastName UNION
SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ')' As UserDetails, dbo.Users.LastName
FROM dbo.Users WHERE dbo.Users.CompanyId = 0  ORDER BY LastName">
</asp:SqlDataSource>

<asp:SqlDataSource
	ID="NewUsersDataSource"
	runat="server"
	ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="SELECT TOP 1 NULL As UserId, '' As UserDetails, '' As LastName UNION
SELECT dbo.Users.UserId, dbo.Users.LastName + ', ' + dbo.Users.FirstName + ' (' + dbo.Users.UserLogon + ')' As UserDetails, dbo.Users.LastName
FROM dbo.Users WHERE dbo.Users.CompanyId = 0 AND dbo.Users.RoleId in (1, 3) ORDER BY LastName">
</asp:SqlDataSource>

<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True" Sort="SiteName ASC"></data:SitesDataSource>

<asp:ObjectDataSource ID="dsUsersProcurement" runat="server" 
    DataObjectTypeName="Repo.CSMS.DAL.EntityModels.UsersProcurement" 
    DeleteMethod="Delete" InsertMethod="Insert" OnObjectCreating="dsUsersProcurement_ObjectCreating" SelectMethod="GetAllReaderAdmin" 
    TypeName="Repo.CSMS.Service.Database.UsersProcurementService" UpdateMethod="Update">
    <DeleteParameters>
        <asp:Parameter Name="UsersProcurementId" Type="Int32" />
    </DeleteParameters>
</asp:ObjectDataSource>
