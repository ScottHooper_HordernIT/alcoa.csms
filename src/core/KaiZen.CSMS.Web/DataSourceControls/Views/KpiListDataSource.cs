﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.KpiListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(KpiListDataSourceDesigner))]
	public class KpiListDataSource : ReadOnlyDataSource<KpiList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListDataSource class.
		/// </summary>
		public KpiListDataSource() : base(new KpiListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the KpiListDataSourceView used by the KpiListDataSource.
		/// </summary>
		protected KpiListDataSourceView KpiListView
		{
			get { return ( View as KpiListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the KpiListDataSourceView class that is to be
		/// used by the KpiListDataSource.
		/// </summary>
		/// <returns>An instance of the KpiListDataSourceView class.</returns>
		protected override BaseDataSourceView<KpiList, Object> GetNewDataSourceView()
		{
			return new KpiListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the KpiListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class KpiListDataSourceView : ReadOnlyDataSourceView<KpiList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the KpiListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the KpiListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public KpiListDataSourceView(KpiListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal KpiListDataSource KpiListOwner
		{
			get { return Owner as KpiListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal KpiListService KpiListProvider
		{
			get { return Provider as KpiListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region KpiListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the KpiListDataSource class.
	/// </summary>
	public class KpiListDataSourceDesigner : ReadOnlyDataSourceDesigner<KpiList>
	{
	}

	#endregion KpiListDataSourceDesigner

	#region KpiListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListFilter : SqlFilter<KpiListColumn>
	{
	}

	#endregion KpiListFilter

	#region KpiListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="KpiList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class KpiListExpressionBuilder : SqlExpressionBuilder<KpiListColumn>
	{
	}
	
	#endregion KpiListExpressionBuilder		
}

