﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.AdminTaskStatusProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(AdminTaskStatusDataSourceDesigner))]
	public class AdminTaskStatusDataSource : ProviderDataSource<AdminTaskStatus, AdminTaskStatusKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusDataSource class.
		/// </summary>
		public AdminTaskStatusDataSource() : base(new AdminTaskStatusService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the AdminTaskStatusDataSourceView used by the AdminTaskStatusDataSource.
		/// </summary>
		protected AdminTaskStatusDataSourceView AdminTaskStatusView
		{
			get { return ( View as AdminTaskStatusDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the AdminTaskStatusDataSource control invokes to retrieve data.
		/// </summary>
		public AdminTaskStatusSelectMethod SelectMethod
		{
			get
			{
				AdminTaskStatusSelectMethod selectMethod = AdminTaskStatusSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (AdminTaskStatusSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the AdminTaskStatusDataSourceView class that is to be
		/// used by the AdminTaskStatusDataSource.
		/// </summary>
		/// <returns>An instance of the AdminTaskStatusDataSourceView class.</returns>
		protected override BaseDataSourceView<AdminTaskStatus, AdminTaskStatusKey> GetNewDataSourceView()
		{
			return new AdminTaskStatusDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the AdminTaskStatusDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class AdminTaskStatusDataSourceView : ProviderDataSourceView<AdminTaskStatus, AdminTaskStatusKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the AdminTaskStatusDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public AdminTaskStatusDataSourceView(AdminTaskStatusDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal AdminTaskStatusDataSource AdminTaskStatusOwner
		{
			get { return Owner as AdminTaskStatusDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal AdminTaskStatusSelectMethod SelectMethod
		{
			get { return AdminTaskStatusOwner.SelectMethod; }
			set { AdminTaskStatusOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal AdminTaskStatusService AdminTaskStatusProvider
		{
			get { return Provider as AdminTaskStatusService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<AdminTaskStatus> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<AdminTaskStatus> results = null;
			AdminTaskStatus item;
			count = 0;
			
			System.Int32 _adminTaskStatusId;
			System.String _statusName;

			switch ( SelectMethod )
			{
				case AdminTaskStatusSelectMethod.Get:
					AdminTaskStatusKey entityKey  = new AdminTaskStatusKey();
					entityKey.Load(values);
					item = AdminTaskStatusProvider.Get(entityKey);
					results = new TList<AdminTaskStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case AdminTaskStatusSelectMethod.GetAll:
                    results = AdminTaskStatusProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case AdminTaskStatusSelectMethod.GetPaged:
					results = AdminTaskStatusProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case AdminTaskStatusSelectMethod.Find:
					if ( FilterParameters != null )
						results = AdminTaskStatusProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = AdminTaskStatusProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case AdminTaskStatusSelectMethod.GetByAdminTaskStatusId:
					_adminTaskStatusId = ( values["AdminTaskStatusId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AdminTaskStatusId"], typeof(System.Int32)) : (int)0;
					item = AdminTaskStatusProvider.GetByAdminTaskStatusId(_adminTaskStatusId);
					results = new TList<AdminTaskStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case AdminTaskStatusSelectMethod.GetByStatusName:
					_statusName = ( values["StatusName"] != null ) ? (System.String) EntityUtil.ChangeType(values["StatusName"], typeof(System.String)) : string.Empty;
					item = AdminTaskStatusProvider.GetByStatusName(_statusName);
					results = new TList<AdminTaskStatus>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == AdminTaskStatusSelectMethod.Get || SelectMethod == AdminTaskStatusSelectMethod.GetByAdminTaskStatusId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				AdminTaskStatus entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					AdminTaskStatusProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<AdminTaskStatus> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			AdminTaskStatusProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region AdminTaskStatusDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the AdminTaskStatusDataSource class.
	/// </summary>
	public class AdminTaskStatusDataSourceDesigner : ProviderDataSourceDesigner<AdminTaskStatus, AdminTaskStatusKey>
	{
		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusDataSourceDesigner class.
		/// </summary>
		public AdminTaskStatusDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskStatusSelectMethod SelectMethod
		{
			get { return ((AdminTaskStatusDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new AdminTaskStatusDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region AdminTaskStatusDataSourceActionList

	/// <summary>
	/// Supports the AdminTaskStatusDataSourceDesigner class.
	/// </summary>
	internal class AdminTaskStatusDataSourceActionList : DesignerActionList
	{
		private AdminTaskStatusDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the AdminTaskStatusDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public AdminTaskStatusDataSourceActionList(AdminTaskStatusDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public AdminTaskStatusSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion AdminTaskStatusDataSourceActionList
	
	#endregion AdminTaskStatusDataSourceDesigner
	
	#region AdminTaskStatusSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the AdminTaskStatusDataSource.SelectMethod property.
	/// </summary>
	public enum AdminTaskStatusSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAdminTaskStatusId method.
		/// </summary>
		GetByAdminTaskStatusId,
		/// <summary>
		/// Represents the GetByStatusName method.
		/// </summary>
		GetByStatusName
	}
	
	#endregion AdminTaskStatusSelectMethod

	#region AdminTaskStatusFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusFilter : SqlFilter<AdminTaskStatusColumn>
	{
	}
	
	#endregion AdminTaskStatusFilter

	#region AdminTaskStatusExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusExpressionBuilder : SqlExpressionBuilder<AdminTaskStatusColumn>
	{
	}
	
	#endregion AdminTaskStatusExpressionBuilder	

	#region AdminTaskStatusProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;AdminTaskStatusChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="AdminTaskStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AdminTaskStatusProperty : ChildEntityProperty<AdminTaskStatusChildEntityTypes>
	{
	}
	
	#endregion AdminTaskStatusProperty
}

