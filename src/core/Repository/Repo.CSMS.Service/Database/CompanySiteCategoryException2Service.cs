﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ICompanySiteCategoryException2Service
    {
        CompanySiteCategoryException2 Get(Expression<Func<CompanySiteCategoryException2, bool>> where, List<Expression<Func<CompanySiteCategoryException2, object>>> includes);
        List<CompanySiteCategoryException2> GetMany(int? top, Expression<Func<CompanySiteCategoryException2, bool>> where, Func<IQueryable<CompanySiteCategoryException2>, IOrderedQueryable<CompanySiteCategoryException2>> orderBy, List<Expression<Func<CompanySiteCategoryException2, object>>> includes);
        List<CompanySiteCategoryException2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryException2, bool>> where, Func<IQueryable<CompanySiteCategoryException2>, IOrderedQueryable<CompanySiteCategoryException2>> orderBy, List<Expression<Func<CompanySiteCategoryException2, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<CompanySiteCategoryException2, bool>> where, List<Expression<Func<CompanySiteCategoryException2, object>>> includes);
        void Insert(CompanySiteCategoryException2 item, bool saveChanges = true);
        void Update(CompanySiteCategoryException2 item, bool saveChanges = true);
        void Delete(CompanySiteCategoryException2 item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class CompanySiteCategoryException2Service : ICompanySiteCategoryException2Service
    {
        private readonly IRepository<CompanySiteCategoryException2> _repository;

        public CompanySiteCategoryException2Service(IRepository<CompanySiteCategoryException2> repository)
        {
            this._repository = repository;
        }

        public CompanySiteCategoryException2 Get(Expression<Func<CompanySiteCategoryException2, bool>> where, List<Expression<Func<CompanySiteCategoryException2, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<CompanySiteCategoryException2> GetMany(int? top, Expression<Func<CompanySiteCategoryException2, bool>> where, Func<IQueryable<CompanySiteCategoryException2>, IOrderedQueryable<CompanySiteCategoryException2>> orderBy, List<Expression<Func<CompanySiteCategoryException2, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<CompanySiteCategoryException2> GetManyPaged(int pageNumber, int pageSize, Expression<Func<CompanySiteCategoryException2, bool>> where, Func<IQueryable<CompanySiteCategoryException2>, IOrderedQueryable<CompanySiteCategoryException2>> orderBy, List<Expression<Func<CompanySiteCategoryException2, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<CompanySiteCategoryException2, bool>> where, List<Expression<Func<CompanySiteCategoryException2, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(CompanySiteCategoryException2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException2");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(CompanySiteCategoryException2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException2");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(CompanySiteCategoryException2 item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("CompanySiteCategoryException2");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
