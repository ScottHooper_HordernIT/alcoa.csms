﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the 'TwentyOnePointAudit_21' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class TwentyOnePointAudit21 : TwentyOnePointAudit21Base
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="TwentyOnePointAudit21"/> instance.
		///</summary>
		public TwentyOnePointAudit21():base(){}	
		
		#endregion
	}
}
