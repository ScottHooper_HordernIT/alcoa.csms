﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'TemplateType' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table TemplateType</remark>
	[Serializable]
	public enum TemplateTypeList
	{
		/// <summary> 
		/// S8.12 Audit Report
		/// </summary>
		[EnumTextValue(@"S8.12 Audit Report")]
		S812Audit = 1, 

		/// <summary> 
		/// Safety Qualification Escalation Procurement Email
		/// </summary>
		[EnumTextValue(@"Safety Qualification Escalation Procurement Email")]
		BatchJobEmailSqEscalationProcurement = 2, 

		/// <summary> 
		/// Safety Qualification Escalation H&S Assessor Email
		/// </summary>
		[EnumTextValue(@"Safety Qualification Escalation H&S Assessor Email")]
		BatchJobEmailSqEscalationHsAssessor = 3, 

		/// <summary> 
		/// Safety Qualification Escalation Procurement (with Supplier) Email
		/// </summary>
		[EnumTextValue(@"Safety Qualification Escalation Procurement (with Supplier) Email")]
		BatchJobEmailSqEscalationSupplier = 4, 

		/// <summary> 
		/// Safety Qualification Exemption Form
		/// </summary>
		[EnumTextValue(@"Safety Qualification Exemption Form")]
		SqExemptionForm = 5, 

		/// <summary> 
		/// Other Useful Links
		/// </summary>
		[EnumTextValue(@"Other Useful Links")]
		UsefulLinksOther = 6

	}
}
