﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="EbiProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class EbiProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Ebi, KaiZen.CSMS.Entities.EbiKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiKey key)
		{
			return Delete(transactionManager, key.EbiId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_ebiId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _ebiId)
		{
			return Delete(null, _ebiId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _ebiId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Ebi Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.EbiKey key, int start, int pageLength)
		{
			return GetByEbiId(transactionManager, key.EbiId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_Ebi index.
		/// </summary>
		/// <param name="_ebiId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public KaiZen.CSMS.Entities.Ebi GetByEbiId(System.Int32 _ebiId)
		{
			int count = -1;
			return GetByEbiId(null,_ebiId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Ebi index.
		/// </summary>
		/// <param name="_ebiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public KaiZen.CSMS.Entities.Ebi GetByEbiId(System.Int32 _ebiId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiId(null, _ebiId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public KaiZen.CSMS.Entities.Ebi GetByEbiId(TransactionManager transactionManager, System.Int32 _ebiId)
		{
			int count = -1;
			return GetByEbiId(transactionManager, _ebiId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public KaiZen.CSMS.Entities.Ebi GetByEbiId(TransactionManager transactionManager, System.Int32 _ebiId, int start, int pageLength)
		{
			int count = -1;
			return GetByEbiId(transactionManager, _ebiId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Ebi index.
		/// </summary>
		/// <param name="_ebiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public KaiZen.CSMS.Entities.Ebi GetByEbiId(System.Int32 _ebiId, int start, int pageLength, out int count)
		{
			return GetByEbiId(null, _ebiId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_ebiId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Ebi"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Ebi GetByEbiId(TransactionManager transactionManager, System.Int32 _ebiId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi index.
		/// </summary>
		/// <param name="_dataChecked"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByDataChecked(System.Boolean? _dataChecked)
		{
			int count = -1;
			return GetByDataChecked(null,_dataChecked, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi index.
		/// </summary>
		/// <param name="_dataChecked"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByDataChecked(System.Boolean? _dataChecked, int start, int pageLength)
		{
			int count = -1;
			return GetByDataChecked(null, _dataChecked, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dataChecked"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByDataChecked(TransactionManager transactionManager, System.Boolean? _dataChecked)
		{
			int count = -1;
			return GetByDataChecked(transactionManager, _dataChecked, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dataChecked"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByDataChecked(TransactionManager transactionManager, System.Boolean? _dataChecked, int start, int pageLength)
		{
			int count = -1;
			return GetByDataChecked(transactionManager, _dataChecked, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi index.
		/// </summary>
		/// <param name="_dataChecked"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByDataChecked(System.Boolean? _dataChecked, int start, int pageLength, out int count)
		{
			return GetByDataChecked(null, _dataChecked, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_dataChecked"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetByDataChecked(TransactionManager transactionManager, System.Boolean? _dataChecked, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="_accessCardNo"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByAccessCardNo(System.Int32? _accessCardNo)
		{
			int count = -1;
			return GetByAccessCardNo(null,_accessCardNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="_accessCardNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByAccessCardNo(System.Int32? _accessCardNo, int start, int pageLength)
		{
			int count = -1;
			return GetByAccessCardNo(null, _accessCardNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessCardNo"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByAccessCardNo(TransactionManager transactionManager, System.Int32? _accessCardNo)
		{
			int count = -1;
			return GetByAccessCardNo(transactionManager, _accessCardNo, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessCardNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByAccessCardNo(TransactionManager transactionManager, System.Int32? _accessCardNo, int start, int pageLength)
		{
			int count = -1;
			return GetByAccessCardNo(transactionManager, _accessCardNo, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="_accessCardNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByAccessCardNo(System.Int32? _accessCardNo, int start, int pageLength, out int count)
		{
			return GetByAccessCardNo(null, _accessCardNo, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_AccessCardNo index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accessCardNo"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetByAccessCardNo(TransactionManager transactionManager, System.Int32? _accessCardNo, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByCompanyName(System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(null,_companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByCompanyName(System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByCompanyName(TransactionManager transactionManager, System.String _companyName)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyName(transactionManager, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetByCompanyName(System.String _companyName, int start, int pageLength, out int count)
		{
			return GetByCompanyName(null, _companyName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_CompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetByCompanyName(TransactionManager transactionManager, System.String _companyName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_Site index.
		/// </summary>
		/// <param name="_swipeSite"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeSite(System.String _swipeSite)
		{
			int count = -1;
			return GetBySwipeSite(null,_swipeSite, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Site index.
		/// </summary>
		/// <param name="_swipeSite"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeSite(System.String _swipeSite, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeSite(null, _swipeSite, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Site index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeSite"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeSite(TransactionManager transactionManager, System.String _swipeSite)
		{
			int count = -1;
			return GetBySwipeSite(transactionManager, _swipeSite, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Site index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeSite"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeSite(TransactionManager transactionManager, System.String _swipeSite, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeSite(transactionManager, _swipeSite, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Site index.
		/// </summary>
		/// <param name="_swipeSite"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeSite(System.String _swipeSite, int start, int pageLength, out int count)
		{
			return GetBySwipeSite(null, _swipeSite, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Site index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeSite"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetBySwipeSite(TransactionManager transactionManager, System.String _swipeSite, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_Year index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYear(System.Int32 _swipeYear)
		{
			int count = -1;
			return GetBySwipeYear(null,_swipeYear, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Year index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYear(System.Int32 _swipeYear, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYear(null, _swipeYear, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Year index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYear(TransactionManager transactionManager, System.Int32 _swipeYear)
		{
			int count = -1;
			return GetBySwipeYear(transactionManager, _swipeYear, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Year index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYear(TransactionManager transactionManager, System.Int32 _swipeYear, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYear(transactionManager, _swipeYear, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Year index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYear(System.Int32 _swipeYear, int start, int pageLength, out int count)
		{
			return GetBySwipeYear(null, _swipeYear, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_Year index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetBySwipeYear(TransactionManager transactionManager, System.Int32 _swipeYear, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonth(System.Int32 _swipeYear, System.Int32 _swipeMonth)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonth(null,_swipeYear, _swipeMonth, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonth(System.Int32 _swipeYear, System.Int32 _swipeMonth, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonth(null, _swipeYear, _swipeMonth, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonth(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonth(transactionManager, _swipeYear, _swipeMonth, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonth(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonth(transactionManager, _swipeYear, _swipeMonth, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonth(System.Int32 _swipeYear, System.Int32 _swipeMonth, int start, int pageLength, out int count)
		{
			return GetBySwipeYearSwipeMonth(null, _swipeYear, _swipeMonth, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonth index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetBySwipeYearSwipeMonth(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeDay(null,_swipeYear, _swipeMonth, _swipeDay, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeDay(null, _swipeYear, _swipeMonth, _swipeDay, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeDay(transactionManager, _swipeYear, _swipeMonth, _swipeDay, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeDay(transactionManager, _swipeYear, _swipeMonth, _swipeDay, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay, int start, int pageLength, out int count)
		{
			return GetBySwipeYearSwipeMonthSwipeDay(null, _swipeYear, _swipeMonth, _swipeDay, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthDay index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeDay"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetBySwipeYearSwipeMonthSwipeDay(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.Int32 _swipeDay, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeSiteCompanyName(null,_swipeYear, _swipeMonth, _swipeSite, _companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeSiteCompanyName(null, _swipeYear, _swipeMonth, _swipeSite, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeSiteCompanyName(transactionManager, _swipeYear, _swipeMonth, _swipeSite, _companyName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName, int start, int pageLength)
		{
			int count = -1;
			return GetBySwipeYearSwipeMonthSwipeSiteCompanyName(transactionManager, _swipeYear, _swipeMonth, _swipeSite, _companyName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName, int start, int pageLength, out int count)
		{
			return GetBySwipeYearSwipeMonthSwipeSiteCompanyName(null, _swipeYear, _swipeMonth, _swipeSite, _companyName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Ebi_YearMonthCompanyName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_swipeYear"></param>
		/// <param name="_swipeMonth"></param>
		/// <param name="_swipeSite"></param>
		/// <param name="_companyName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Ebi&gt;"/> class.</returns>
		public abstract TList<Ebi> GetBySwipeYearSwipeMonthSwipeSiteCompanyName(TransactionManager transactionManager, System.Int32 _swipeYear, System.Int32 _swipeMonth, System.String _swipeSite, System.String _companyName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Ebi_CompaniesOnSite_ByYearMonthDay 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthDay(System.Int32? year, System.Int32? month, System.Int32? day, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonthDay(null, 0, int.MaxValue , year, month, day, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthDay(int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? day, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonthDay(null, start, pageLength , year, month, day, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthDay(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.Int32? day, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonthDay(transactionManager, 0, int.MaxValue , year, month, day, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompaniesOnSite_ByYearMonthDay(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.Int32? day, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Ebi_YearMonthDayCompanies_ByYearMonthDay 
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYearMonthDay(System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return YearMonthDayCompanies_ByYearMonthDay(null, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYearMonthDay(int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return YearMonthDayCompanies_ByYearMonthDay(null, start, pageLength , year, month, day);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYearMonthDay(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.Int32? day)
		{
			return YearMonthDayCompanies_ByYearMonthDay(transactionManager, 0, int.MaxValue , year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="day"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet YearMonthDayCompanies_ByYearMonthDay(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.Int32? day);
		
		#endregion
		
		#region _Ebi_CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameEbi"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(System.Int32? year, System.Int32? month, System.String companyNameEbi, System.String siteNameEbi)
		{
			return CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(null, 0, int.MaxValue , year, month, companyNameEbi, siteNameEbi);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(int start, int pageLength, System.Int32? year, System.Int32? month, System.String companyNameEbi, System.String siteNameEbi)
		{
			return CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(null, start, pageLength , year, month, companyNameEbi, siteNameEbi);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.String companyNameEbi, System.String siteNameEbi)
		{
			return CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(transactionManager, 0, int.MaxValue , year, month, companyNameEbi, siteNameEbi);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="siteNameEbi"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompaniesOnSite_ByYearMonthCompanyNameEbiSiteNameEbi(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.String companyNameEbi, System.String siteNameEbi);
		
		#endregion
		
		#region _Ebi_CompanyEmployeesOnSite_ByYearMonthDay 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompanyEmployeesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="ebiCompanyName"> A <c>System.String</c> instance.</param>
		/// <param name="ebiSiteName"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanyEmployeesOnSite_ByYearMonthDay(System.String ebiCompanyName, System.String ebiSiteName, System.String year, System.String month, System.String day)
		{
			return CompanyEmployeesOnSite_ByYearMonthDay(null, 0, int.MaxValue , ebiCompanyName, ebiSiteName, year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompanyEmployeesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="ebiCompanyName"> A <c>System.String</c> instance.</param>
		/// <param name="ebiSiteName"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanyEmployeesOnSite_ByYearMonthDay(int start, int pageLength, System.String ebiCompanyName, System.String ebiSiteName, System.String year, System.String month, System.String day)
		{
			return CompanyEmployeesOnSite_ByYearMonthDay(null, start, pageLength , ebiCompanyName, ebiSiteName, year, month, day);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompanyEmployeesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="ebiCompanyName"> A <c>System.String</c> instance.</param>
		/// <param name="ebiSiteName"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompanyEmployeesOnSite_ByYearMonthDay(TransactionManager transactionManager, System.String ebiCompanyName, System.String ebiSiteName, System.String year, System.String month, System.String day)
		{
			return CompanyEmployeesOnSite_ByYearMonthDay(transactionManager, 0, int.MaxValue , ebiCompanyName, ebiSiteName, year, month, day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompanyEmployeesOnSite_ByYearMonthDay' stored procedure. 
		/// </summary>
		/// <param name="ebiCompanyName"> A <c>System.String</c> instance.</param>
		/// <param name="ebiSiteName"> A <c>System.String</c> instance.</param>
		/// <param name="year"> A <c>System.String</c> instance.</param>
		/// <param name="month"> A <c>System.String</c> instance.</param>
		/// <param name="day"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompanyEmployeesOnSite_ByYearMonthDay(TransactionManager transactionManager, int start, int pageLength , System.String ebiCompanyName, System.String ebiSiteName, System.String year, System.String month, System.String day);
		
		#endregion
		
		#region _Ebi_YearMonthDayCompaniesEmployees_ByYear 
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompaniesEmployees_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompaniesEmployees_ByYear(System.Int32? year)
		{
			return YearMonthDayCompaniesEmployees_ByYear(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompaniesEmployees_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompaniesEmployees_ByYear(int start, int pageLength, System.Int32? year)
		{
			return YearMonthDayCompaniesEmployees_ByYear(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompaniesEmployees_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompaniesEmployees_ByYear(TransactionManager transactionManager, System.Int32? year)
		{
			return YearMonthDayCompaniesEmployees_ByYear(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompaniesEmployees_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet YearMonthDayCompaniesEmployees_ByYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#region _Ebi_CompaniesOnSite_ByYearMonthCompanyIdSiteId 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyIdSiteId(System.Int32? year, System.Int32? month, System.Int32? companyId, System.Int32? siteId)
		{
			return CompaniesOnSite_ByYearMonthCompanyIdSiteId(null, 0, int.MaxValue , year, month, companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyIdSiteId(int start, int pageLength, System.Int32? year, System.Int32? month, System.Int32? companyId, System.Int32? siteId)
		{
			return CompaniesOnSite_ByYearMonthCompanyIdSiteId(null, start, pageLength , year, month, companyId, siteId);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonthCompanyIdSiteId(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.Int32? companyId, System.Int32? siteId)
		{
			return CompaniesOnSite_ByYearMonthCompanyIdSiteId(transactionManager, 0, int.MaxValue , year, month, companyId, siteId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonthCompanyIdSiteId' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="siteId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompaniesOnSite_ByYearMonthCompanyIdSiteId(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.Int32? companyId, System.Int32? siteId);
		
		#endregion
		
		#region _Ebi_YearMonthCompaniesCompareKpi_ByYear 
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthCompaniesCompareKpi_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthCompaniesCompareKpi_ByYear(System.Int32? year)
		{
			return YearMonthCompaniesCompareKpi_ByYear(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthCompaniesCompareKpi_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthCompaniesCompareKpi_ByYear(int start, int pageLength, System.Int32? year)
		{
			return YearMonthCompaniesCompareKpi_ByYear(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthCompaniesCompareKpi_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthCompaniesCompareKpi_ByYear(TransactionManager transactionManager, System.Int32? year)
		{
			return YearMonthCompaniesCompareKpi_ByYear(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthCompaniesCompareKpi_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet YearMonthCompaniesCompareKpi_ByYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#region _Ebi_YearMonthDayCompanies_ByDay 
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByDay' stored procedure. 
		/// </summary>
		/// <param name="day"> A <c>System.DateTime?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByDay(System.DateTime? day)
		{
			return YearMonthDayCompanies_ByDay(null, 0, int.MaxValue , day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByDay' stored procedure. 
		/// </summary>
		/// <param name="day"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByDay(int start, int pageLength, System.DateTime? day)
		{
			return YearMonthDayCompanies_ByDay(null, start, pageLength , day);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByDay' stored procedure. 
		/// </summary>
		/// <param name="day"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByDay(TransactionManager transactionManager, System.DateTime? day)
		{
			return YearMonthDayCompanies_ByDay(transactionManager, 0, int.MaxValue , day);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByDay' stored procedure. 
		/// </summary>
		/// <param name="day"> A <c>System.DateTime?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet YearMonthDayCompanies_ByDay(TransactionManager transactionManager, int start, int pageLength , System.DateTime? day);
		
		#endregion
		
		#region _Ebi_CompaniesResidential_OnSite_ByYearMonth 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesResidential_OnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesResidential_OnSite_ByYearMonth(System.Int32? year, System.Int32? month)
		{
			return CompaniesResidential_OnSite_ByYearMonth(null, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesResidential_OnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesResidential_OnSite_ByYearMonth(int start, int pageLength, System.Int32? year, System.Int32? month)
		{
			return CompaniesResidential_OnSite_ByYearMonth(null, start, pageLength , year, month);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesResidential_OnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesResidential_OnSite_ByYearMonth(TransactionManager transactionManager, System.Int32? year, System.Int32? month)
		{
			return CompaniesResidential_OnSite_ByYearMonth(transactionManager, 0, int.MaxValue , year, month);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesResidential_OnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompaniesResidential_OnSite_ByYearMonth(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month);
		
		#endregion
		
		#region _Ebi_CompaniesOnSite_ByYearMonth 
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonth(System.Int32? year, System.Int32? month, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonth(null, 0, int.MaxValue , year, month, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonth(int start, int pageLength, System.Int32? year, System.Int32? month, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonth(null, start, pageLength , year, month, companySiteCategoryId);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet CompaniesOnSite_ByYearMonth(TransactionManager transactionManager, System.Int32? year, System.Int32? month, System.String companySiteCategoryId)
		{
			return CompaniesOnSite_ByYearMonth(transactionManager, 0, int.MaxValue , year, month, companySiteCategoryId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_CompaniesOnSite_ByYearMonth' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="month"> A <c>System.Int32?</c> instance.</param>
		/// <param name="companySiteCategoryId"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet CompaniesOnSite_ByYearMonth(TransactionManager transactionManager, int start, int pageLength , System.Int32? year, System.Int32? month, System.String companySiteCategoryId);
		
		#endregion
		
		#region _Ebi_Companies_NoLink 
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies_NoLink' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies_NoLink()
		{
			return Companies_NoLink(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies_NoLink' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies_NoLink(int start, int pageLength)
		{
			return Companies_NoLink(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_Companies_NoLink' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies_NoLink(TransactionManager transactionManager)
		{
			return Companies_NoLink(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies_NoLink' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Companies_NoLink(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Ebi_LastTimeOnSite_ByCompanyId 
		
		/// <summary>
		///	This method wrap the '_Ebi_LastTimeOnSite_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet LastTimeOnSite_ByCompanyId(System.Int32? companyId)
		{
			return LastTimeOnSite_ByCompanyId(null, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_LastTimeOnSite_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet LastTimeOnSite_ByCompanyId(int start, int pageLength, System.Int32? companyId)
		{
			return LastTimeOnSite_ByCompanyId(null, start, pageLength , companyId);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_LastTimeOnSite_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet LastTimeOnSite_ByCompanyId(TransactionManager transactionManager, System.Int32? companyId)
		{
			return LastTimeOnSite_ByCompanyId(transactionManager, 0, int.MaxValue , companyId);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_LastTimeOnSite_ByCompanyId' stored procedure. 
		/// </summary>
		/// <param name="companyId"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet LastTimeOnSite_ByCompanyId(TransactionManager transactionManager, int start, int pageLength , System.Int32? companyId);
		
		#endregion
		
		#region _Ebi_Companies 
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies' stored procedure. 
		/// </summary>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies()
		{
			return Companies(null, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies(int start, int pageLength)
		{
			return Companies(null, start, pageLength );
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_Companies' stored procedure. 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet Companies(TransactionManager transactionManager)
		{
			return Companies(transactionManager, 0, int.MaxValue );
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_Companies' stored procedure. 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet Companies(TransactionManager transactionManager, int start, int pageLength );
		
		#endregion
		
		#region _Ebi_YearMonthDayCompanies_ByYear 
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYear(System.Int32? year)
		{
			return YearMonthDayCompanies_ByYear(null, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYear(int start, int pageLength, System.Int32? year)
		{
			return YearMonthDayCompanies_ByYear(null, start, pageLength , year);
		}
				
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet YearMonthDayCompanies_ByYear(TransactionManager transactionManager, System.Int32? year)
		{
			return YearMonthDayCompanies_ByYear(transactionManager, 0, int.MaxValue , year);
		}
		
		/// <summary>
		///	This method wrap the '_Ebi_YearMonthDayCompanies_ByYear' stored procedure. 
		/// </summary>
		/// <param name="year"> A <c>System.Int32?</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet YearMonthDayCompanies_ByYear(TransactionManager transactionManager, int start, int pageLength , System.Int32? year);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Ebi&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Ebi&gt;"/></returns>
		public static TList<Ebi> Fill(IDataReader reader, TList<Ebi> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Ebi c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Ebi")
					.Append("|").Append((System.Int32)reader[((int)EbiColumn.EbiId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Ebi>(
					key.ToString(), // EntityTrackingKey
					"Ebi",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Ebi();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.EbiId = (System.Int32)reader[((int)EbiColumn.EbiId - 1)];
					c.CompanyName = (reader.IsDBNull(((int)EbiColumn.CompanyName - 1)))?null:(System.String)reader[((int)EbiColumn.CompanyName - 1)];
					c.ClockId = (reader.IsDBNull(((int)EbiColumn.ClockId - 1)))?null:(System.Int32?)reader[((int)EbiColumn.ClockId - 1)];
					c.FullName = (reader.IsDBNull(((int)EbiColumn.FullName - 1)))?null:(System.String)reader[((int)EbiColumn.FullName - 1)];
					c.AccessCardNo = (reader.IsDBNull(((int)EbiColumn.AccessCardNo - 1)))?null:(System.Int32?)reader[((int)EbiColumn.AccessCardNo - 1)];
					c.SwipeSite = (System.String)reader[((int)EbiColumn.SwipeSite - 1)];
					c.DataChecked = (reader.IsDBNull(((int)EbiColumn.DataChecked - 1)))?null:(System.Boolean?)reader[((int)EbiColumn.DataChecked - 1)];
					c.SwipeDateTime = (System.DateTime)reader[((int)EbiColumn.SwipeDateTime - 1)];
					c.SwipeYear = (System.Int32)reader[((int)EbiColumn.SwipeYear - 1)];
					c.SwipeMonth = (System.Int32)reader[((int)EbiColumn.SwipeMonth - 1)];
					c.SwipeDay = (System.Int32)reader[((int)EbiColumn.SwipeDay - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Ebi"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Ebi"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Ebi entity)
		{
			if (!reader.Read()) return;
			
			entity.EbiId = (System.Int32)reader[((int)EbiColumn.EbiId - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)EbiColumn.CompanyName - 1)))?null:(System.String)reader[((int)EbiColumn.CompanyName - 1)];
			entity.ClockId = (reader.IsDBNull(((int)EbiColumn.ClockId - 1)))?null:(System.Int32?)reader[((int)EbiColumn.ClockId - 1)];
			entity.FullName = (reader.IsDBNull(((int)EbiColumn.FullName - 1)))?null:(System.String)reader[((int)EbiColumn.FullName - 1)];
			entity.AccessCardNo = (reader.IsDBNull(((int)EbiColumn.AccessCardNo - 1)))?null:(System.Int32?)reader[((int)EbiColumn.AccessCardNo - 1)];
			entity.SwipeSite = (System.String)reader[((int)EbiColumn.SwipeSite - 1)];
			entity.DataChecked = (reader.IsDBNull(((int)EbiColumn.DataChecked - 1)))?null:(System.Boolean?)reader[((int)EbiColumn.DataChecked - 1)];
			entity.SwipeDateTime = (System.DateTime)reader[((int)EbiColumn.SwipeDateTime - 1)];
			entity.SwipeYear = (System.Int32)reader[((int)EbiColumn.SwipeYear - 1)];
			entity.SwipeMonth = (System.Int32)reader[((int)EbiColumn.SwipeMonth - 1)];
			entity.SwipeDay = (System.Int32)reader[((int)EbiColumn.SwipeDay - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Ebi"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Ebi"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Ebi entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.EbiId = (System.Int32)dataRow["EbiId"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.ClockId = Convert.IsDBNull(dataRow["ClockId"]) ? null : (System.Int32?)dataRow["ClockId"];
			entity.FullName = Convert.IsDBNull(dataRow["FullName"]) ? null : (System.String)dataRow["FullName"];
			entity.AccessCardNo = Convert.IsDBNull(dataRow["AccessCardNo"]) ? null : (System.Int32?)dataRow["AccessCardNo"];
			entity.SwipeSite = (System.String)dataRow["SwipeSite"];
			entity.DataChecked = Convert.IsDBNull(dataRow["DataChecked"]) ? null : (System.Boolean?)dataRow["DataChecked"];
			entity.SwipeDateTime = (System.DateTime)dataRow["SwipeDateTime"];
			entity.SwipeYear = (System.Int32)dataRow["SwipeYear"];
			entity.SwipeMonth = (System.Int32)dataRow["SwipeMonth"];
			entity.SwipeDay = (System.Int32)dataRow["SwipeDay"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Ebi"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Ebi Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Ebi entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Ebi object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Ebi instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Ebi Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Ebi entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region EbiChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Ebi</c>
	///</summary>
	public enum EbiChildEntityTypes
	{
	}
	
	#endregion EbiChildEntityTypes
	
	#region EbiFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EbiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiFilterBuilder : SqlFilterBuilder<EbiColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiFilterBuilder class.
		/// </summary>
		public EbiFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiFilterBuilder
	
	#region EbiParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EbiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class EbiParameterBuilder : ParameterizedSqlFilterBuilder<EbiColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiParameterBuilder class.
		/// </summary>
		public EbiParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the EbiParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public EbiParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the EbiParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public EbiParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion EbiParameterBuilder
	
	#region EbiSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EbiColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Ebi"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class EbiSortBuilder : SqlSortBuilder<EbiColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the EbiSqlSortBuilder class.
		/// </summary>
		public EbiSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion EbiSortBuilder
	
} // end namespace
