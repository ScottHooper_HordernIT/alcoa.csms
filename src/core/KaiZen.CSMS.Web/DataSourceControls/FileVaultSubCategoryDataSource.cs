﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.FileVaultSubCategoryProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(FileVaultSubCategoryDataSourceDesigner))]
	public class FileVaultSubCategoryDataSource : ProviderDataSource<FileVaultSubCategory, FileVaultSubCategoryKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryDataSource class.
		/// </summary>
		public FileVaultSubCategoryDataSource() : base(new FileVaultSubCategoryService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the FileVaultSubCategoryDataSourceView used by the FileVaultSubCategoryDataSource.
		/// </summary>
		protected FileVaultSubCategoryDataSourceView FileVaultSubCategoryView
		{
			get { return ( View as FileVaultSubCategoryDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the FileVaultSubCategoryDataSource control invokes to retrieve data.
		/// </summary>
		public FileVaultSubCategorySelectMethod SelectMethod
		{
			get
			{
				FileVaultSubCategorySelectMethod selectMethod = FileVaultSubCategorySelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (FileVaultSubCategorySelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the FileVaultSubCategoryDataSourceView class that is to be
		/// used by the FileVaultSubCategoryDataSource.
		/// </summary>
		/// <returns>An instance of the FileVaultSubCategoryDataSourceView class.</returns>
		protected override BaseDataSourceView<FileVaultSubCategory, FileVaultSubCategoryKey> GetNewDataSourceView()
		{
			return new FileVaultSubCategoryDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the FileVaultSubCategoryDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class FileVaultSubCategoryDataSourceView : ProviderDataSourceView<FileVaultSubCategory, FileVaultSubCategoryKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the FileVaultSubCategoryDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public FileVaultSubCategoryDataSourceView(FileVaultSubCategoryDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal FileVaultSubCategoryDataSource FileVaultSubCategoryOwner
		{
			get { return Owner as FileVaultSubCategoryDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal FileVaultSubCategorySelectMethod SelectMethod
		{
			get { return FileVaultSubCategoryOwner.SelectMethod; }
			set { FileVaultSubCategoryOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal FileVaultSubCategoryService FileVaultSubCategoryProvider
		{
			get { return Provider as FileVaultSubCategoryService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<FileVaultSubCategory> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<FileVaultSubCategory> results = null;
			FileVaultSubCategory item;
			count = 0;
			
			System.Int32 _fileVaultSubCategoryId;
			System.String _subCategoryName;

			switch ( SelectMethod )
			{
				case FileVaultSubCategorySelectMethod.Get:
					FileVaultSubCategoryKey entityKey  = new FileVaultSubCategoryKey();
					entityKey.Load(values);
					item = FileVaultSubCategoryProvider.Get(entityKey);
					results = new TList<FileVaultSubCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case FileVaultSubCategorySelectMethod.GetAll:
                    results = FileVaultSubCategoryProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case FileVaultSubCategorySelectMethod.GetPaged:
					results = FileVaultSubCategoryProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case FileVaultSubCategorySelectMethod.Find:
					if ( FilterParameters != null )
						results = FileVaultSubCategoryProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = FileVaultSubCategoryProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case FileVaultSubCategorySelectMethod.GetByFileVaultSubCategoryId:
					_fileVaultSubCategoryId = ( values["FileVaultSubCategoryId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["FileVaultSubCategoryId"], typeof(System.Int32)) : (int)0;
					item = FileVaultSubCategoryProvider.GetByFileVaultSubCategoryId(_fileVaultSubCategoryId);
					results = new TList<FileVaultSubCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case FileVaultSubCategorySelectMethod.GetBySubCategoryName:
					_subCategoryName = ( values["SubCategoryName"] != null ) ? (System.String) EntityUtil.ChangeType(values["SubCategoryName"], typeof(System.String)) : string.Empty;
					item = FileVaultSubCategoryProvider.GetBySubCategoryName(_subCategoryName);
					results = new TList<FileVaultSubCategory>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == FileVaultSubCategorySelectMethod.Get || SelectMethod == FileVaultSubCategorySelectMethod.GetByFileVaultSubCategoryId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				FileVaultSubCategory entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					FileVaultSubCategoryProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<FileVaultSubCategory> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			FileVaultSubCategoryProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region FileVaultSubCategoryDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the FileVaultSubCategoryDataSource class.
	/// </summary>
	public class FileVaultSubCategoryDataSourceDesigner : ProviderDataSourceDesigner<FileVaultSubCategory, FileVaultSubCategoryKey>
	{
		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryDataSourceDesigner class.
		/// </summary>
		public FileVaultSubCategoryDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultSubCategorySelectMethod SelectMethod
		{
			get { return ((FileVaultSubCategoryDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new FileVaultSubCategoryDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region FileVaultSubCategoryDataSourceActionList

	/// <summary>
	/// Supports the FileVaultSubCategoryDataSourceDesigner class.
	/// </summary>
	internal class FileVaultSubCategoryDataSourceActionList : DesignerActionList
	{
		private FileVaultSubCategoryDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the FileVaultSubCategoryDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public FileVaultSubCategoryDataSourceActionList(FileVaultSubCategoryDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public FileVaultSubCategorySelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion FileVaultSubCategoryDataSourceActionList
	
	#endregion FileVaultSubCategoryDataSourceDesigner
	
	#region FileVaultSubCategorySelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the FileVaultSubCategoryDataSource.SelectMethod property.
	/// </summary>
	public enum FileVaultSubCategorySelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByFileVaultSubCategoryId method.
		/// </summary>
		GetByFileVaultSubCategoryId,
		/// <summary>
		/// Represents the GetBySubCategoryName method.
		/// </summary>
		GetBySubCategoryName
	}
	
	#endregion FileVaultSubCategorySelectMethod

	#region FileVaultSubCategoryFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryFilter : SqlFilter<FileVaultSubCategoryColumn>
	{
	}
	
	#endregion FileVaultSubCategoryFilter

	#region FileVaultSubCategoryExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryExpressionBuilder : SqlExpressionBuilder<FileVaultSubCategoryColumn>
	{
	}
	
	#endregion FileVaultSubCategoryExpressionBuilder	

	#region FileVaultSubCategoryProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;FileVaultSubCategoryChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="FileVaultSubCategory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FileVaultSubCategoryProperty : ChildEntityProperty<FileVaultSubCategoryChildEntityTypes>
	{
	}
	
	#endregion FileVaultSubCategoryProperty
}

