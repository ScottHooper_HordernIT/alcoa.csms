﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireTypeProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireType, KaiZen.CSMS.Entities.QuestionnaireTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireTypeKey key)
		{
			return Delete(transactionManager, key.QuestionnaireTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireTypeId)
		{
			return Delete(null, _questionnaireTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireType Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireTypeKey key, int start, int pageLength)
		{
			return GetByQuestionnaireTypeId(transactionManager, key.QuestionnaireTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(null,_questionnaireTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(null, _questionnaireTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(transactionManager, _questionnaireTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireTypeId(transactionManager, _questionnaireTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(System.Int32 _questionnaireTypeId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireTypeId(null, _questionnaireTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeId(TransactionManager transactionManager, System.Int32 _questionnaireTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeName"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(System.String _questionnaireTypeName)
		{
			int count = -1;
			return GetByQuestionnaireTypeName(null,_questionnaireTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(System.String _questionnaireTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireTypeName(null, _questionnaireTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(TransactionManager transactionManager, System.String _questionnaireTypeName)
		{
			int count = -1;
			return GetByQuestionnaireTypeName(transactionManager, _questionnaireTypeName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(TransactionManager transactionManager, System.String _questionnaireTypeName, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireTypeName(transactionManager, _questionnaireTypeName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireType index.
		/// </summary>
		/// <param name="_questionnaireTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(System.String _questionnaireTypeName, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireTypeName(null, _questionnaireTypeName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireTypeName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireType GetByQuestionnaireTypeName(TransactionManager transactionManager, System.String _questionnaireTypeName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireType&gt;"/></returns>
		public static TList<QuestionnaireType> Fill(IDataReader reader, TList<QuestionnaireType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireType")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireType>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireTypeId = (System.Int32)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeId - 1)];
					c.QuestionnaireTypeName = (System.String)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeName - 1)];
					c.QuestionnaireTypeDesc = (System.String)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireType entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireTypeId = (System.Int32)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeId - 1)];
			entity.QuestionnaireTypeName = (System.String)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeName - 1)];
			entity.QuestionnaireTypeDesc = (System.String)reader[((int)QuestionnaireTypeColumn.QuestionnaireTypeDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireTypeId = (System.Int32)dataRow["QuestionnaireTypeId"];
			entity.QuestionnaireTypeName = (System.String)dataRow["QuestionnaireTypeName"];
			entity.QuestionnaireTypeDesc = (System.String)dataRow["QuestionnaireTypeDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireTypeId methods when available
			
			#region QuestionnaireServicesSelectedCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireServicesSelectedCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireServicesSelectedCollection = DataRepository.QuestionnaireServicesSelectedProvider.GetByQuestionnaireTypeId(transactionManager, entity.QuestionnaireTypeId);

				if (deep && entity.QuestionnaireServicesSelectedCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireServicesSelected>) DataRepository.QuestionnaireServicesSelectedProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireServicesSelected>
				if (CanDeepSave(entity.QuestionnaireServicesSelectedCollection, "List<QuestionnaireServicesSelected>|QuestionnaireServicesSelectedCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireServicesSelected child in entity.QuestionnaireServicesSelectedCollection)
					{
						if(child.QuestionnaireTypeIdSource != null)
						{
							child.QuestionnaireTypeId = child.QuestionnaireTypeIdSource.QuestionnaireTypeId;
						}
						else
						{
							child.QuestionnaireTypeId = entity.QuestionnaireTypeId;
						}

					}

					if (entity.QuestionnaireServicesSelectedCollection.Count > 0 || entity.QuestionnaireServicesSelectedCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireServicesSelectedProvider.Save(transactionManager, entity.QuestionnaireServicesSelectedCollection);
						
						deepHandles.Add("QuestionnaireServicesSelectedCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireServicesSelected >) DataRepository.QuestionnaireServicesSelectedProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireServicesSelectedCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireType</c>
	///</summary>
	public enum QuestionnaireTypeChildEntityTypes
	{

		///<summary>
		/// Collection of <c>QuestionnaireType</c> as OneToMany for QuestionnaireServicesSelectedCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireServicesSelected>))]
		QuestionnaireServicesSelectedCollection,
	}
	
	#endregion QuestionnaireTypeChildEntityTypes
	
	#region QuestionnaireTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeFilterBuilder : SqlFilterBuilder<QuestionnaireTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilterBuilder class.
		/// </summary>
		public QuestionnaireTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireTypeFilterBuilder
	
	#region QuestionnaireTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireTypeParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeParameterBuilder class.
		/// </summary>
		public QuestionnaireTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireTypeParameterBuilder
	
	#region QuestionnaireTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireTypeSortBuilder : SqlSortBuilder<QuestionnaireTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireTypeSqlSortBuilder class.
		/// </summary>
		public QuestionnaireTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireTypeSortBuilder
	
} // end namespace
