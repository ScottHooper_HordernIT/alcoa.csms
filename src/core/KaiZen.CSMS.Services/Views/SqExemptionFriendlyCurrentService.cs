﻿
#region Using Directives
using System;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Data;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Entities.Validation;

using KaiZen.CSMS.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;

#endregion

namespace KaiZen.CSMS.Services
{		
	
	///<summary>
	/// An component type implementation of the 'SqExemption_Friendly_Current' table.
	///</summary>
	/// <remarks>
	/// All custom implementations should be done here.
	/// </remarks>
	[CLSCompliant(true)]
	public partial class SqExemptionFriendlyCurrentService : KaiZen.CSMS.Services.SqExemptionFriendlyCurrentServiceBase
	{
		/// <summary>
		/// Initializes a new instance of the SqExemptionFriendlyCurrentService class.
		/// </summary>
		public SqExemptionFriendlyCurrentService() : base()
		{
		}
		
	}//End Class


} // end namespace
