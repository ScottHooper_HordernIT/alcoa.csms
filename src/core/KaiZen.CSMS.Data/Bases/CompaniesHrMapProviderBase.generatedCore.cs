﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CompaniesHrMapProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CompaniesHrMapProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.CompaniesHrMap, KaiZen.CSMS.Entities.CompaniesHrMapKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrMapKey key)
		{
			return Delete(transactionManager, key.CompaniesHrMapId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_companiesHrMapId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _companiesHrMapId)
		{
			return Delete(null, _companiesHrMapId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrMapId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _companiesHrMapId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.CompaniesHrMap Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrMapKey key, int start, int pageLength)
		{
			return GetByCompaniesHrMapId(transactionManager, key.CompaniesHrMapId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CompaniesHr index.
		/// </summary>
		/// <param name="_companiesHrMapId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(System.Int32 _companiesHrMapId)
		{
			int count = -1;
			return GetByCompaniesHrMapId(null,_companiesHrMapId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHr index.
		/// </summary>
		/// <param name="_companiesHrMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(System.Int32 _companiesHrMapId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompaniesHrMapId(null, _companiesHrMapId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrMapId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(TransactionManager transactionManager, System.Int32 _companiesHrMapId)
		{
			int count = -1;
			return GetByCompaniesHrMapId(transactionManager, _companiesHrMapId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(TransactionManager transactionManager, System.Int32 _companiesHrMapId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompaniesHrMapId(transactionManager, _companiesHrMapId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHr index.
		/// </summary>
		/// <param name="_companiesHrMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(System.Int32 _companiesHrMapId, int start, int pageLength, out int count)
		{
			return GetByCompaniesHrMapId(null, _companiesHrMapId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CompaniesHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companiesHrMapId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesHrMap GetByCompaniesHrMapId(TransactionManager transactionManager, System.Int32 _companiesHrMapId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(null,_companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyId(transactionManager, _companyId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(System.Int32 _companyId, int start, int pageLength, out int count)
		{
			return GetByCompanyId(null, _companyId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyId(TransactionManager transactionManager, System.Int32 _companyId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="_companyNameHr"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(System.Int32 _companyNameHr)
		{
			int count = -1;
			return GetByCompanyNameHr(null,_companyNameHr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="_companyNameHr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(System.Int32 _companyNameHr, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNameHr(null, _companyNameHr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameHr"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(TransactionManager transactionManager, System.Int32 _companyNameHr)
		{
			int count = -1;
			return GetByCompanyNameHr(transactionManager, _companyNameHr, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameHr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(TransactionManager transactionManager, System.Int32 _companyNameHr, int start, int pageLength)
		{
			int count = -1;
			return GetByCompanyNameHr(transactionManager, _companyNameHr, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="_companyNameHr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(System.Int32 _companyNameHr, int start, int pageLength, out int count)
		{
			return GetByCompanyNameHr(null, _companyNameHr, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the UK_CompaniesHrMap_CompanyNameHr index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_companyNameHr"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.CompaniesHrMap GetByCompanyNameHr(TransactionManager transactionManager, System.Int32 _companyNameHr, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CompaniesHrMap&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CompaniesHrMap&gt;"/></returns>
		public static TList<CompaniesHrMap> Fill(IDataReader reader, TList<CompaniesHrMap> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.CompaniesHrMap c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CompaniesHrMap")
					.Append("|").Append((System.Int32)reader[((int)CompaniesHrMapColumn.CompaniesHrMapId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CompaniesHrMap>(
					key.ToString(), // EntityTrackingKey
					"CompaniesHrMap",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.CompaniesHrMap();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CompaniesHrMapId = (System.Int32)reader[((int)CompaniesHrMapColumn.CompaniesHrMapId - 1)];
					c.CompanyId = (System.Int32)reader[((int)CompaniesHrMapColumn.CompanyId - 1)];
					c.CompanyNameHr = (System.Int32)reader[((int)CompaniesHrMapColumn.CompanyNameHr - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.CompaniesHrMap entity)
		{
			if (!reader.Read()) return;
			
			entity.CompaniesHrMapId = (System.Int32)reader[((int)CompaniesHrMapColumn.CompaniesHrMapId - 1)];
			entity.CompanyId = (System.Int32)reader[((int)CompaniesHrMapColumn.CompanyId - 1)];
			entity.CompanyNameHr = (System.Int32)reader[((int)CompaniesHrMapColumn.CompanyNameHr - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.CompaniesHrMap entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CompaniesHrMapId = (System.Int32)dataRow["CompaniesHrMapId"];
			entity.CompanyId = (System.Int32)dataRow["CompanyId"];
			entity.CompanyNameHr = (System.Int32)dataRow["CompanyNameHr"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.CompaniesHrMap"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesHrMap Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrMap entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCompaniesHrMapId methods when available
			
			#region CompaniesHrCrpDataCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CompaniesHrCrpData>|CompaniesHrCrpDataCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CompaniesHrCrpDataCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CompaniesHrCrpDataCollection = DataRepository.CompaniesHrCrpDataProvider.GetByCompanyId(transactionManager, entity.CompanyId);

				if (deep && entity.CompaniesHrCrpDataCollection.Count > 0)
				{
					deepHandles.Add("CompaniesHrCrpDataCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CompaniesHrCrpData>) DataRepository.CompaniesHrCrpDataProvider.DeepLoad,
						new object[] { transactionManager, entity.CompaniesHrCrpDataCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.CompaniesHrMap object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.CompaniesHrMap instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.CompaniesHrMap Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.CompaniesHrMap entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CompaniesHrCrpData>
				if (CanDeepSave(entity.CompaniesHrCrpDataCollection, "List<CompaniesHrCrpData>|CompaniesHrCrpDataCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CompaniesHrCrpData child in entity.CompaniesHrCrpDataCollection)
					{
						if(child.CompanyIdSource != null)
						{
							child.CompanyId = child.CompanyIdSource.CompanyId;
						}
						else
						{
							child.CompanyId = entity.CompanyId;
						}

					}

					if (entity.CompaniesHrCrpDataCollection.Count > 0 || entity.CompaniesHrCrpDataCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CompaniesHrCrpDataProvider.Save(transactionManager, entity.CompaniesHrCrpDataCollection);
						
						deepHandles.Add("CompaniesHrCrpDataCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CompaniesHrCrpData >) DataRepository.CompaniesHrCrpDataProvider.DeepSave,
							new object[] { transactionManager, entity.CompaniesHrCrpDataCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CompaniesHrMapChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.CompaniesHrMap</c>
	///</summary>
	public enum CompaniesHrMapChildEntityTypes
	{

		///<summary>
		/// Collection of <c>CompaniesHrMap</c> as OneToMany for CompaniesHrCrpDataCollection
		///</summary>
		[ChildEntityType(typeof(TList<CompaniesHrCrpData>))]
		CompaniesHrCrpDataCollection,
	}
	
	#endregion CompaniesHrMapChildEntityTypes
	
	#region CompaniesHrMapFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CompaniesHrMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapFilterBuilder : SqlFilterBuilder<CompaniesHrMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilterBuilder class.
		/// </summary>
		public CompaniesHrMapFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrMapFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrMapFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrMapFilterBuilder
	
	#region CompaniesHrMapParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CompaniesHrMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CompaniesHrMapParameterBuilder : ParameterizedSqlFilterBuilder<CompaniesHrMapColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapParameterBuilder class.
		/// </summary>
		public CompaniesHrMapParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CompaniesHrMapParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CompaniesHrMapParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CompaniesHrMapParameterBuilder
	
	#region CompaniesHrMapSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CompaniesHrMapColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CompaniesHrMap"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CompaniesHrMapSortBuilder : SqlSortBuilder<CompaniesHrMapColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CompaniesHrMapSqlSortBuilder class.
		/// </summary>
		public CompaniesHrMapSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CompaniesHrMapSortBuilder
	
} // end namespace
