﻿
using System;

namespace KaiZen.CSMS.Entities
{
	/// <summary>
	/// An enum representation of the 'SafetyPlanStatus' table. [No description found in the database]
	/// </summary>
	/// <remark>this enumeration contains the items contained in the table SafetyPlanStatus</remark>
	[Serializable]
	public enum SafetyPlanStatusList
	{
		/// <summary> 
		/// Document submitted. Awaiting assignment of assessor.
		/// </summary>
		[EnumTextValue(@"Document submitted. Awaiting assignment of assessor.")]
		Submitted = 1, 

		/// <summary> 
		/// Document received. Under assessment
		/// </summary>
		[EnumTextValue(@"Document received. Under assessment")]
		Being_Assessed = 2, 

		/// <summary> 
		/// Document approved
		/// </summary>
		[EnumTextValue(@"Document approved")]
		Approved = 3, 

		/// <summary> 
		/// Document Not Approved
		/// </summary>
		[EnumTextValue(@"Document Not Approved")]
		Not_Approved = 4, 

		/// <summary> 
		/// Document assigned. Awaiting assessment.
		/// </summary>
		[EnumTextValue(@"Document assigned. Awaiting assessment.")]
		Assigned = 5

	}
}
