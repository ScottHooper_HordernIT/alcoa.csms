﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Services;
using System.Data;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxGridView;
using System.Drawing;
using DevExpress.Data.Filtering;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using DevExpress.XtraPrinting;

namespace ALCOA.CSMS.Website.UserControls.Main.PeopleManagementTraining
{
    public partial class Validity2 : System.Web.UI.UserControl
    {
        string filteredText = "";
        static string fileName = "People Management – Training – Validity";
        Auth auth = new Auth();

        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Helper._Auth.PageLoad(auth);
                if (auth.RoleId == Convert.ToInt32(RoleList.Contractor))
                {
                    SqlCompanyDataSource.SelectCommand = "select CompanyId,CompanyName from Companies where CompanyId=" + auth.CompanyId;
                    SqlCompanyDataSource.DataBind();
                }

                if (!Page.IsPostBack)
                {
                    Session["ShowAll"] = false;

                    Session["filterText"] = "";
                    Session["CompanyIdVal"] = null;
                    Session["SiteIdVal"] = null;
                    Session["COURSE_NAMEVal"] = null;
                    Session["FULL_NAMEVal"] = null;
                    Session["COURSE_END_DATEVal"] = null;
                    Session["EXPIRATION_DATEVal"] = null;
                    Session["ExpiresInVal"] = null;
                    Session["ExpiredVal"] = null;


                    Helper.ExportGrid.Settings(ucExportButtons2, "People Management – Training – Validity", "grid");
                    //ucExportButtons2.Visible = false;

                    FillCompanyDropdown(cmbCompanies);
                    //FillDateFrom();
                    //lblDateTo.Text = DateTime.Now.ToShortDateString();
                    int startIndex = 0;

                    switch (auth.RoleId)
                    {
                        case ((int)RoleList.Reader):

                            cmbCompanies.Enabled = true;
                            FillSitesCombo("-1", cmbSites);
                            startIndex = cmbSites.Items.IndexOfText("Australia");
                            cmbSites.SelectedIndex = startIndex;
                            break;
                        case ((int)RoleList.Contractor):
                            cmbCompanies.Value = auth.CompanyId;
                            cmbCompanies.Enabled = false;
                            FillSitesCombo(auth.CompanyId.ToString(), cmbSites);
                            Session["CompanyFilter"] = cmbCompanies.Value.ToString();
                            if (cmbSites.Value != null)
                            {
                                Session["SiteFilter"] = cmbSites.Value.ToString();
                            }
                            break;
                        case ((int)RoleList.Administrator):
                            cmbCompanies.Enabled = true;
                            FillSitesCombo("-1", cmbSites);
                            startIndex = cmbSites.Items.IndexOfText("Australia");
                            cmbSites.SelectedIndex = startIndex;
                            break;
                        default:
                            Response.Redirect("AccessDenied.aspx?error=" + Server.UrlEncode("Unrecognised User."));
                            break;
                    }
                }
                Session["CompanyFilter"] = cmbCompanies.Value.ToString();
                if (cmbSites.Value != null)
                {
                    Session["SiteFilter"] = cmbSites.Value.ToString();
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        protected void FillSitesCombo(string companyId, ASPxComboBox CmbSite)
        {
            int _i = 0;
            if (string.IsNullOrEmpty(companyId)) return;

            int _companyId;
            bool companyId_isInt = Int32.TryParse(companyId, out _companyId);
            if (!companyId_isInt) return;



            if (_companyId > 0) //Specific Company
            {

                SitesService sService = new SitesService();
                CompanySiteCategoryStandardService cscsService = new CompanySiteCategoryStandardService();
                TList<CompanySiteCategoryStandard> cscsTlist = cscsService.GetByCompanyId(_companyId);

                CmbSite.Items.Clear();

                if (cscsTlist != null)
                {
                    if (cscsTlist.Count > 0)
                    {
                        SitesFilters sitesFilters = new SitesFilters();
                        sitesFilters.Append(SitesColumn.IsVisible, "1");
                        TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                        foreach (Sites s in sitesList)
                        {
                            foreach (CompanySiteCategoryStandard cscs in cscsTlist)
                            {
                                if (cscs.CompanySiteCategoryId != null)
                                {
                                    if (s.SiteId == cscs.SiteId)
                                    {
                                        CmbSite.Items.Add(s.SiteName, cscs.SiteId);
                                    }
                                }
                            }
                        }

                        cmbSites.Items.Add("----------------", 0);

                        //Add Regions
                        RegionsService rService = new RegionsService();
                        DataSet dsRegion = rService.GetByCompanyId(_companyId);
                        if (dsRegion.Tables[0] != null)
                        {
                            if (dsRegion.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in dsRegion.Tables[0].Rows)
                                {
                                    //if (dr[2].ToString() != "AU") 
                                    CmbSite.Items.Add(dr[0].ToString(), (-1 * Convert.ToInt32(dr[1].ToString())));
                                }
                            }
                        }
                        CmbSite.Items.Add("Australia", -2);
                        CmbSite.Value = -2;
                    }
                }
            }
            else
            {
                if (_companyId == -1) // All Companies
                {
                    CmbSite.Items.Clear();
                    SitesFilters sitesFilters = new SitesFilters();
                    sitesFilters.Append(SitesColumn.IsVisible, "1");
                    TList<Sites> sitesList = DataRepository.SitesProvider.GetPaged(sitesFilters.ToString(), "SiteName ASC", 0, 100, out _i);
                    foreach (Sites s in sitesList)
                    {
                        CmbSite.Items.Add(s.SiteName, s.SiteId);
                    }
                    CmbSite.Items.Add("----------------", 0);

                    RegionsFilters regionsFilters = new RegionsFilters();
                    regionsFilters.Append(RegionsColumn.IsVisible, "1");
                    TList<Regions> regionsList = DataRepository.RegionsProvider.GetPaged(regionsFilters.ToString(), "Ordinal ASC", 0, 100, out _i);
                    foreach (Regions r in regionsList)
                    {
                        CmbSite.Items.Add(r.RegionName, (-1 * r.RegionId));
                    }
                }
                else if (_companyId == -2) //Selected "----" invalid option, so lets hide all sites.
                {
                    CmbSite.Items.Clear();
                    CmbSite.DataSourceID = "";
                    CmbSite.DataBind();
                }
            }

        }


        protected void FillCompanyDropdown(ASPxComboBox CmbCompany)
        {
            CmbCompany.DataSourceID = "SqlCompanyDataSource"; 
            CmbCompany.TextField = "CompanyName";
            CmbCompany.ValueField = "CompanyId";
            CmbCompany.DataBind();
            CmbCompany.Items.Add("-----------------------------", 0);
            CmbCompany.Items.Add("All", -1);
            CmbCompany.Value = -1;
        }


        protected void cmbCompanies_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillSitesCombo(cmbCompanies.SelectedItem.Value.ToString(), cmbSites);

        }

        protected void btnGoFilter_click(object sender, EventArgs e)
        {
            //SqlDataSourceTrainingManagementValidity.SelectParameters["CompanyId"].DefaultValue = cmbCompanies.Value.ToString();
            //if (cmbSites.Value != null)
            //{
            //    SqlDataSourceTrainingManagementValidity.SelectParameters["SiteId"].DefaultValue = cmbSites.Value.ToString();
            //}
            //else
            //{
            //    SqlDataSourceTrainingManagementValidity.SelectParameters["SiteId"].DefaultValue = "0";
            //}
            //grid.DataBind();

            if (auth.RoleId == Convert.ToInt32(RoleList.Contractor))
            {
                SqlCompanyDataSource.SelectCommand = "select CompanyId,CompanyName from Companies where CompanyId=" + auth.CompanyId;
                SqlCompanyDataSource.DataBind();
            }
            
            Session["CompanyFilter"] = cmbCompanies.Value.ToString();
            Session["SiteFilter"] = cmbSites.Value.ToString();
            
            grid.DataBind();

            //if (Session["ValidityCount"] != null)
            //{
            //    if (Convert.ToInt32(Session["ValidityCount"]) > 65000)
            //    {
            //        ucExportButtons2.Visible = false;
            //    }
            //    else
            //    {
            //        ucExportButtons2.Visible = true;
            //    }
            //}
            
        }

        protected void cmbSites_Callback(object source, CallbackEventArgsBase e)
        {
            try
            {
                cmbSites.SelectedIndex = 0;
                FillSitesCombo(e.Parameter, cmbSites);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        protected void grid_RowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {

            try
            {
                if (e.RowType == GridViewRowType.Data)
                {
                    string strExpires = Convert.ToString(grid.GetRowValues(e.VisibleIndex, "ExpiresIn"));
                    ViewState["strExpires"] = strExpires;
                    ASPxLabel lblExpiresIn = grid.FindRowCellTemplateControl(e.VisibleIndex, null, "lblExpiresIn") as ASPxLabel;
                    // lblExpiresIn.Text = "";

                    if (strExpires == "-" || strExpires == "")
                    {
                        lblExpiresIn.Text = strExpires;
                    }
                    else if (Convert.ToInt32(strExpires) <= 0)
                    {
                        lblExpiresIn.ForeColor = Color.Red;
                        lblExpiresIn.Text = "Expired " + Convert.ToString(Convert.ToInt32(strExpires) * -1) + " Days ago";
                    }
                    else if (Convert.ToInt32(strExpires) > 0)
                    {
                        lblExpiresIn.Text = "Expiring in " + strExpires + " Days";
                        if (Convert.ToInt32(strExpires) > 60)
                        {
                            lblExpiresIn.ForeColor = Color.Black;
                        }
                        else
                        {
                            lblExpiresIn.ForeColor = Color.Red;
                        }
                    }
                }
                //if (Session["ValidityCount"] != null)
                //{
                //    if (Convert.ToInt32(Session["ValidityCount"]) > 65000)
                //    {
                //        ucExportButtons2.Visible = false;
                //    }
                //    else
                //    {
                //        ucExportButtons2.Visible = true;
                //    }
                //}
            }
            catch (Exception ex)
            {
                string str=Convert.ToString(ViewState["strExpires"]);
                throw;
            }
        }      

        protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
        {
            try
            {
                if (e.Column.FieldName != "ExpiresIn") return;
                e.EditorProperties = new ComboBoxProperties();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        protected void grid_AutoFilterCellEditorInitialize(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorEventArgs e)
        {
            try
            {
                if (e.Column.FieldName != "ExpiresIn") return;

                ASPxComboBox combo = (ASPxComboBox)e.Editor;
                combo.ValueType = typeof(string);
                combo.Items.Add("Expired", "Expired");
                combo.Items.Add(">60", ">60");
                combo.Items.Add("<=60", "<=60");
                combo.Items.Add("n/a", "n/a");
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        protected void grid_ProcessColumnAutoFilter(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewAutoFilterEventArgs e)
        {
            try
            {
                int filterCount = 0;
                string whereCondition = string.Empty;

                if (e.Column.FieldName == "ExpiresIn")
                {
                    if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                    {
                        //string[] param = e.Value.ToString().Split('$');
                        if (e.Value.ToString() == "<=60")
                        {
                            e.Criteria = new OperandProperty("ExpiresIn") <= 60;
                            Session["ExpiresInVal"] = "<=60";
                            Session["filterText"] = "<=60";

                        }
                        else if (e.Value.ToString() == ">60")
                        {
                            e.Criteria = new OperandProperty("ExpiresIn") > 60;
                            Session["ExpiresInVal"] = ">60";                            
                            Session["filterText"] = ">60";
                        }
                        else if (e.Value.ToString() == "n/a")
                        {
                            e.Criteria = new OperandProperty("ExpiresIn") == 0;
                            Session["ExpiresInVal"] = "n/a";                            
                            Session["filterText"] = "n/a";
                        }
                        else if (e.Value.ToString() == "Expired")
                        {
                            e.Criteria = new OperandProperty("ExpiresIn") == 1;
                            Session["ExpiresInVal"] = "Expired";                            
                            Session["filterText"] = "Expired";
                        }

                        Session["filterCount"] = filterCount;
                        grid.PageIndex = 0;

                    }
                    else
                    {
                        e.Value = Session["filterText"].ToString();
                    }
                }

                if (e.Column.FieldName == "CompanyId")
                {
                    //  if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                    //  {
                    e.Criteria = new OperandProperty("CompanyId") == e.Value.ToString();
                    Session["CompanyIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                    //  }
                }

                if (e.Column.FieldName == "SiteId")
                {
                    // if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria)
                    // {
                    e.Criteria = new OperandProperty("SiteId") == e.Value.ToString();
                    Session["SiteIdVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    grid.PageIndex = 0;
                    //  }
                }

                if (e.Column.FieldName == "COURSE_NAME")
                {
                    if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria || e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.ExtractDisplayText)
                  {
                    e.Criteria = new OperandProperty("COURSE_NAME") == e.Value.ToString();
                    Session["COURSE_NAMEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                   }
                }
                if (e.Column.FieldName == "FULL_NAME")
                {
                if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria || e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.ExtractDisplayText)
                  {
                    e.Criteria = new OperandProperty("FULL_NAME") == e.Value.ToString();
                    Session["FULL_NAMEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                 }
                }
                if (e.Column.FieldName == "COURSE_END_DATE")
                {
                    if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria || e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.ExtractDisplayText)
                  {
                    e.Criteria = new OperandProperty("COURSE_END_DATE") == e.Value.ToString();
                    Session["COURSE_END_DATEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                   }
                }
                if (e.Column.FieldName == "EXPIRATION_DATE")
                {
                   if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria || e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.ExtractDisplayText)
                  {
                    e.Criteria = new OperandProperty("EXPIRATION_DATE") == e.Value.ToString();
                    Session["EXPIRATION_DATEVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                   }
                }
                if (e.Column.FieldName == "Expired")
                {
                   if (e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.CreateCriteria || e.Kind == DevExpress.Web.ASPxGridView.GridViewAutoFilterEventKind.ExtractDisplayText)
                  {
                    e.Criteria = new OperandProperty("Expired") == e.Value.ToString();
                    Session["ExpiredVal"] = e.Value.ToString();
                    filterCount++;
                    Session["filterCount"] = filterCount;
                    //grid.PageIndex = 0;
                   }
                }
                grid.DataBind();

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        protected void grid_CustomFilterExpressionDisplayText(object sender, CustomFilterExpressionDisplayTextEventArgs e)
        {
            if (e.FilterExpression.Contains("[ExpiresIn] = 0") || e.FilterExpression.Contains("[ExpiresIn] = 'n/a'"))
            {
                e.DisplayText=e.DisplayText.Replace("0", "n/a"); 
            }
             if(e.FilterExpression.Contains("[ExpiresIn] = 1") || e.FilterExpression.Contains("[ExpiresIn] = 'Expired'"))
            {
                 e.DisplayText=e.DisplayText.Replace("1", "Expired"); 
            }
        }

        protected void grid_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
        {
            try
            {
              
                if (e.CallbackName == "APPLYCOLUMNFILTER")
                {
                    grid.PageIndex = 0;
                }
                if (e.CallbackName == "APPLYFILTER")
                {
                    Session["CompanyIdVal"] = null;
                    Session["SiteIdVal"] = null;
                    Session["COURSE_NAMEVal"] = null;
                    Session["FULL_NAMEVal"] = null;
                    Session["COURSE_END_DATEVal"] = null;
                    Session["EXPIRATION_DATEVal"] = null;
                    Session["ExpiresInVal"] = null;
                    Session["ExpiredVal"] = null;

                    grid.DataBind();
                }
                //if (Session["ValidityCount"] != null)
                //{
                //    if (Convert.ToInt32(Session["ValidityCount"]) > 65000)
                //    {
                //        ucExportButtons2.Visible = false;
                //    }
                //    else
                //    {
                //        ucExportButtons2.Visible = true;
                //    }
                //}
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public static void UpdateTraining_Management_Validity()
        {
            string errorMsg = string.Empty;
            try
            {
                DataSet ds = new DataSet();
                string sqldb = ConfigurationManager.ConnectionStrings["ALCOA_WAO_CSMWP_devConnectionString"].ConnectionString;

                using (SqlConnection cnDel = new SqlConnection(sqldb))
                {
                    cnDel.Open();
                    Console.WriteLine("..Delteing data from table: Training_Management_Validity" + "...");
                    SqlCommand cmdDel = new SqlCommand("truncate table Training_Management_Validity", cnDel);
                    cmdDel.CommandTimeout = 120;
                    cmdDel.CommandType = CommandType.StoredProcedure;
                    cmdDel.ExecuteNonQuery();


                    cnDel.Close();
                }

                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    Console.WriteLine("..Selecting data from View: XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP" + "...");

                    cn.Open();

                    //Cindi Thornton, change hr.dbo to hr, now in CSMS in HR schema
                    string strQuery = "SELECT DISTINCT A.PERSON_ID, A.FULL_NAME,A.EMPL_ID, COURSE_NAME,  " +
                      " COURSE_END_DATE,EXPIRATION_DATE, B.CWK_VENDOR_NO,D.CompanyId,E.SiteId  " +
                     " FROM HR.[XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP] A " +
                     " LEFT OUTER JOIN HR.XXHR_CWK_HISTORY_V B " + 
                     " ON A.PERSON_ID = B.PERSON_ID " +
                     " Left Outer Join CompanyVendor C  " +
                     " ON B.CWK_VENDOR_NO = C.Vendor_Number  " +
                     " Left Outer Join Companies D " +
                     " On C.Tax_Registration_Number=D.CompanyABN " +
                     " Left Outer Join Sites E   " +
                     " on B.Location_Code=E.SiteNameHr  " +
                     " WHERE A.COURSE_END_DATE = (SELECT MAX(COURSE_END_DATE) " +
                      " FROM HR.[XXHR_HR_TO_CSMS_CWK_ENR_ADD_TRNG_MAP] " +
                      " WHERE A.PERSON_ID = PERSON_ID AND A.COURSE_NAME = COURSE_NAME)  " +
                        //--AND (A.COURSE_END_DATE BETWEEN B.EFFECTIVE_START_DATE AND B.EFFECTIVE_END_DATE)                          
                     " AND C.Vendor_Number Is Not Null " +
                     " AND E.SiteId Is Not Null  " +
                     " AND D.CompanyId Is Not Null   ";

                    SqlCommand cmd = new SqlCommand(strQuery, cn);

                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);

                    sqlda.Fill(ds);

                    cmd.ExecuteNonQuery();

                    cn.Close();
                }

                using (SqlConnection cn = new SqlConnection(sqldb))
                {
                    if (ds != null)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            try
                            {
                                Console.WriteLine("..Inserting data from Table: Training_Management_Validity" + "...");
                                cn.Open();

                                SqlCommand cmd = new SqlCommand("Training_Management_Validity_Insert", cn);

                                //SqlCommand cmd = new SqlCommand("Training_Management_ValidityAll_2", cn);
                                cmd.CommandTimeout = 120;
                                cmd.CommandType = CommandType.StoredProcedure;

                                if (ds.Tables[0].Rows[i]["PERSON_ID"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@PERSON_ID", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@PERSON_ID", Convert.ToInt32(ds.Tables[0].Rows[i]["PERSON_ID"]));
                                }

                                if (ds.Tables[0].Rows[i]["FULL_NAME"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@FULL_NAME", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@FULL_NAME", Convert.ToString(ds.Tables[0].Rows[i]["FULL_NAME"]));
                                }

                                if (ds.Tables[0].Rows[i]["EMPL_ID"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@EMPL_ID", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@EMPL_ID", Convert.ToString(ds.Tables[0].Rows[i]["EMPL_ID"]));
                                }
                                if (ds.Tables[0].Rows[i]["COURSE_NAME"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@COURSE_NAME", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@COURSE_NAME", Convert.ToString(ds.Tables[0].Rows[i]["COURSE_NAME"]));
                                }

                                if (ds.Tables[0].Rows[i]["COURSE_END_DATE"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@COURSE_END_DATE", DBNull.Value);
                                }
                                else
                                {
                                    if ((Convert.ToDateTime(ds.Tables[0].Rows[i]["COURSE_END_DATE"]).Year > 1900))
                                    {
                                        cmd.Parameters.AddWithValue("@COURSE_END_DATE", Convert.ToDateTime(ds.Tables[0].Rows[i]["COURSE_END_DATE"]));
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@COURSE_END_DATE", DBNull.Value);
                                    }
                                }

                                if (ds.Tables[0].Rows[i]["EXPIRATION_DATE"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@EXPIRATION_DATE", DBNull.Value);
                                }
                                else
                                {
                                    if ((Convert.ToDateTime(ds.Tables[0].Rows[i]["EXPIRATION_DATE"]).Year > 1900))
                                    {
                                        cmd.Parameters.AddWithValue("@EXPIRATION_DATE", Convert.ToDateTime(ds.Tables[0].Rows[i]["EXPIRATION_DATE"]));
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@EXPIRATION_DATE", DBNull.Value);
                                    }
                                }

                                if (ds.Tables[0].Rows[i]["CWK_VENDOR_NO"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@VENDOR_NO", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@VENDOR_NO", Convert.ToString(ds.Tables[0].Rows[i]["CWK_VENDOR_NO"]));
                                }

                                if (ds.Tables[0].Rows[i]["CompanyId"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@CompanyId", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@CompanyId", Convert.ToInt32(ds.Tables[0].Rows[i]["CompanyId"]));
                                }

                                if (ds.Tables[0].Rows[i]["SiteId"].ToString() == "")
                                {
                                    cmd.Parameters.AddWithValue("@SiteId", DBNull.Value);
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@SiteId", Convert.ToInt32(ds.Tables[0].Rows[i]["SiteId"]));
                                }


                                cmd.ExecuteNonQuery();

                                cn.Close();
                            }
                            catch (Exception ex)
                            {
                                errorMsg += ex.Message + ";";
                                Console.Write(ex.Message.ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               errorMsg += ex.Message + ";";
               Console.Write(ex.Message.ToString());
            }

        }


        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            if (Session["ShowAll"] != null)
            {
                if (Convert.ToBoolean(Session["ShowAll"]) == false)
                {
                    btnShowAll.Text = "Show Pager";
                    Session["ShowAll"] = true;
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
                                    
                    grid.DataBind();
                }
                else
                {
                    grid.SettingsPager.Mode = GridViewPagerMode.ShowPager;
                    Response.Redirect("validity.aspx", true);                    
                }
            }

        }

    }
}