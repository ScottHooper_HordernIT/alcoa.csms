﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IQuestionnaireVerificationAttachmentAuditService
    {
        QuestionnaireVerificationAttachmentAudit Get(Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes);
        List<QuestionnaireVerificationAttachmentAudit> GetMany(int? top, Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachmentAudit>, IOrderedQueryable<QuestionnaireVerificationAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes);
        List<QuestionnaireVerificationAttachmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachmentAudit>, IOrderedQueryable<QuestionnaireVerificationAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes);
        void Insert(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true);
        void Update(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true);
        void Delete(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class QuestionnaireVerificationAttachmentAuditService : IQuestionnaireVerificationAttachmentAuditService
    {
        private readonly IRepository<QuestionnaireVerificationAttachmentAudit> _repository;

        public QuestionnaireVerificationAttachmentAuditService(IRepository<QuestionnaireVerificationAttachmentAudit> repository)
        {
            this._repository = repository;
        }

        public QuestionnaireVerificationAttachmentAudit Get(Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<QuestionnaireVerificationAttachmentAudit> GetMany(int? top, Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachmentAudit>, IOrderedQueryable<QuestionnaireVerificationAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<QuestionnaireVerificationAttachmentAudit> GetManyPaged(int pageNumber, int pageSize, Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, Func<IQueryable<QuestionnaireVerificationAttachmentAudit>, IOrderedQueryable<QuestionnaireVerificationAttachmentAudit>> orderBy, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<QuestionnaireVerificationAttachmentAudit, bool>> where, List<Expression<Func<QuestionnaireVerificationAttachmentAudit, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachmentAudit");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachmentAudit");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(QuestionnaireVerificationAttachmentAudit item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("QuestionnaireVerificationAttachmentAudit");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
