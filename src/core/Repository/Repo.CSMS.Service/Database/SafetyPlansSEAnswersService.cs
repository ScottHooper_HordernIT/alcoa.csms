﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ISafetyPlansSEAnswersService
    {
        SafetyPlans_SE_Answers Get(Expression<Func<SafetyPlans_SE_Answers, bool>> where, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes);
        List<SafetyPlans_SE_Answers> GetMany(int? top, Expression<Func<SafetyPlans_SE_Answers, bool>> where, Func<IQueryable<SafetyPlans_SE_Answers>, IOrderedQueryable<SafetyPlans_SE_Answers>> orderBy, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes);
        List<SafetyPlans_SE_Answers> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlans_SE_Answers, bool>> where, Func<IQueryable<SafetyPlans_SE_Answers>, IOrderedQueryable<SafetyPlans_SE_Answers>> orderBy, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<SafetyPlans_SE_Answers, bool>> where, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes);
        void Insert(SafetyPlans_SE_Answers item, bool saveChanges = true);
        void Update(SafetyPlans_SE_Answers item, bool saveChanges = true);
        void Delete(SafetyPlans_SE_Answers item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class SafetyPlansSEAnswersService : ISafetyPlansSEAnswersService
    {
        private readonly IRepository<SafetyPlans_SE_Answers> _repository;

        public SafetyPlansSEAnswersService(IRepository<SafetyPlans_SE_Answers> repository)
        {
            this._repository = repository;
        }

        public SafetyPlans_SE_Answers Get(Expression<Func<SafetyPlans_SE_Answers, bool>> where, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<SafetyPlans_SE_Answers> GetMany(int? top, Expression<Func<SafetyPlans_SE_Answers, bool>> where, Func<IQueryable<SafetyPlans_SE_Answers>, IOrderedQueryable<SafetyPlans_SE_Answers>> orderBy, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<SafetyPlans_SE_Answers> GetManyPaged(int pageNumber, int pageSize, Expression<Func<SafetyPlans_SE_Answers, bool>> where, Func<IQueryable<SafetyPlans_SE_Answers>, IOrderedQueryable<SafetyPlans_SE_Answers>> orderBy, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<SafetyPlans_SE_Answers, bool>> where, List<Expression<Func<SafetyPlans_SE_Answers, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(SafetyPlans_SE_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlans_SE_Answers");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(SafetyPlans_SE_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlans_SE_Answers");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(SafetyPlans_SE_Answers item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("SafetyPlans_SE_Answers");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
