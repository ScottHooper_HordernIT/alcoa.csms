﻿#region Using directives

using System;

#endregion

namespace KaiZen.CSMS.Entities
{	
	///<summary>
	/// An object representation of the '_Current_Questionnaire_ProcurementFunctionalManager' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class CurrentQuestionnaireProcurementFunctionalManager : CurrentQuestionnaireProcurementFunctionalManagerBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="CurrentQuestionnaireProcurementFunctionalManager"/> instance.
		///</summary>
		public CurrentQuestionnaireProcurementFunctionalManager():base(){}	
		
		#endregion
	}
}
