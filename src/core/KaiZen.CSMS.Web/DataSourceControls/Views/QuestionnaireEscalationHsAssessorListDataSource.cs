﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireEscalationHsAssessorListProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[CLSCompliant(true)]
	[Designer(typeof(QuestionnaireEscalationHsAssessorListDataSourceDesigner))]
	public class QuestionnaireEscalationHsAssessorListDataSource : ReadOnlyDataSource<QuestionnaireEscalationHsAssessorList>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListDataSource class.
		/// </summary>
		public QuestionnaireEscalationHsAssessorListDataSource() : base(new QuestionnaireEscalationHsAssessorListService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireEscalationHsAssessorListDataSourceView used by the QuestionnaireEscalationHsAssessorListDataSource.
		/// </summary>
		protected QuestionnaireEscalationHsAssessorListDataSourceView QuestionnaireEscalationHsAssessorListView
		{
			get { return ( View as QuestionnaireEscalationHsAssessorListDataSourceView ); }
		}
		
		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireEscalationHsAssessorListDataSourceView class that is to be
		/// used by the QuestionnaireEscalationHsAssessorListDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireEscalationHsAssessorListDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireEscalationHsAssessorList, Object> GetNewDataSourceView()
		{
			return new QuestionnaireEscalationHsAssessorListDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireEscalationHsAssessorListDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireEscalationHsAssessorListDataSourceView : ReadOnlyDataSourceView<QuestionnaireEscalationHsAssessorList>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireEscalationHsAssessorListDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireEscalationHsAssessorListDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireEscalationHsAssessorListDataSourceView(QuestionnaireEscalationHsAssessorListDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireEscalationHsAssessorListDataSource QuestionnaireEscalationHsAssessorListOwner
		{
			get { return Owner as QuestionnaireEscalationHsAssessorListDataSource; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireEscalationHsAssessorListService QuestionnaireEscalationHsAssessorListProvider
		{
			get { return Provider as QuestionnaireEscalationHsAssessorListService; }
		}

		#endregion Properties
		
		#region Methods
		
		#endregion Methods
	}

	#region QuestionnaireEscalationHsAssessorListDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireEscalationHsAssessorListDataSource class.
	/// </summary>
	public class QuestionnaireEscalationHsAssessorListDataSourceDesigner : ReadOnlyDataSourceDesigner<QuestionnaireEscalationHsAssessorList>
	{
	}

	#endregion QuestionnaireEscalationHsAssessorListDataSourceDesigner

	#region QuestionnaireEscalationHsAssessorListFilter

	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListFilter : SqlFilter<QuestionnaireEscalationHsAssessorListColumn>
	{
	}

	#endregion QuestionnaireEscalationHsAssessorListFilter

	#region QuestionnaireEscalationHsAssessorListExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireEscalationHsAssessorList"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireEscalationHsAssessorListExpressionBuilder : SqlExpressionBuilder<QuestionnaireEscalationHsAssessorListColumn>
	{
	}
	
	#endregion QuestionnaireEscalationHsAssessorListExpressionBuilder		
}

