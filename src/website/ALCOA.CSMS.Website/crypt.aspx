<%@ Page Language="C#" AutoEventWireup="true" Inherits="crypt" Codebehind="crypt.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Encyption Test Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager id="ScriptManager1" runat="server">
        </asp:ScriptManager>&nbsp;</div>
        <asp:UpdatePanel id="UpdatePanel1" runat="server">
            <contenttemplate>
        <asp:TextBox ID="tbPassword" runat="server" TextMode="Password"></asp:TextBox>
        <asp:Button ID="Button3" runat="server" Text="Show" OnClick="Button3_Click" />
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server" Height="277px" Visible="False" Width="537px">
        TripleDES Encryption Calculator Test...<br />
        <br />
        Encrypt:
        <asp:TextBox ID="TextBox1" runat="server">yujia.li@alcoa.com.au</asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <asp:Label ID="Label1" runat="server"></asp:Label><br />
        Decrypt:
        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <br />
        <br />
        Using Key:
        <asp:TextBox ID="TextBox3" runat="server" OnTextChanged="TextBox3_TextChanged">AlcoaSecretKey</asp:TextBox><br />
        <br />
        <asp:HyperLink ID="HyperLink1" runat="server">[HyperLink1]</asp:HyperLink>
        </asp:Panel></contenttemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
