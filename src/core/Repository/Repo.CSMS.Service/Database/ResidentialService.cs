﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IResidentialService
    {
        Residential Get(Expression<Func<Residential, bool>> where, List<Expression<Func<Residential, object>>> includes);
        List<Residential> GetMany(int? top, Expression<Func<Residential, bool>> where, Func<IQueryable<Residential>, IOrderedQueryable<Residential>> orderBy, List<Expression<Func<Residential, object>>> includes);
        List<Residential> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Residential, bool>> where, Func<IQueryable<Residential>, IOrderedQueryable<Residential>> orderBy, List<Expression<Func<Residential, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Residential, bool>> where, List<Expression<Func<Residential, object>>> includes);
        void Insert(Residential item, bool saveChanges = true);
        void Update(Residential item, bool saveChanges = true);
        void Delete(Residential item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class ResidentialService : IResidentialService
    {
        private readonly IRepository<Residential> _repository;

        public ResidentialService(IRepository<Residential> repository)
        {
            this._repository = repository;
        }

        public Residential Get(Expression<Func<Residential, bool>> where, List<Expression<Func<Residential, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Residential> GetMany(int? top, Expression<Func<Residential, bool>> where, Func<IQueryable<Residential>, IOrderedQueryable<Residential>> orderBy, List<Expression<Func<Residential, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Residential> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Residential, bool>> where, Func<IQueryable<Residential>, IOrderedQueryable<Residential>> orderBy, List<Expression<Func<Residential, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Residential, bool>> where, List<Expression<Func<Residential, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Residential item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Residential");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Residential item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Residential");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Residential item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Residential");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
