﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireAuditProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireAuditDataSourceDesigner))]
	public class QuestionnaireAuditDataSource : ProviderDataSource<QuestionnaireAudit, QuestionnaireAuditKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditDataSource class.
		/// </summary>
		public QuestionnaireAuditDataSource() : base(new QuestionnaireAuditService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireAuditDataSourceView used by the QuestionnaireAuditDataSource.
		/// </summary>
		protected QuestionnaireAuditDataSourceView QuestionnaireAuditView
		{
			get { return ( View as QuestionnaireAuditDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireAuditDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireAuditSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireAuditSelectMethod selectMethod = QuestionnaireAuditSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireAuditSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireAuditDataSourceView class that is to be
		/// used by the QuestionnaireAuditDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireAuditDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireAudit, QuestionnaireAuditKey> GetNewDataSourceView()
		{
			return new QuestionnaireAuditDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireAuditDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireAuditDataSourceView : ProviderDataSourceView<QuestionnaireAudit, QuestionnaireAuditKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireAuditDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireAuditDataSourceView(QuestionnaireAuditDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireAuditDataSource QuestionnaireAuditOwner
		{
			get { return Owner as QuestionnaireAuditDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireAuditSelectMethod SelectMethod
		{
			get { return QuestionnaireAuditOwner.SelectMethod; }
			set { QuestionnaireAuditOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireAuditService QuestionnaireAuditProvider
		{
			get { return Provider as QuestionnaireAuditService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireAudit> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireAudit> results = null;
			QuestionnaireAudit item;
			count = 0;
			
			System.Int32 _auditId;

			switch ( SelectMethod )
			{
				case QuestionnaireAuditSelectMethod.Get:
					QuestionnaireAuditKey entityKey  = new QuestionnaireAuditKey();
					entityKey.Load(values);
					item = QuestionnaireAuditProvider.Get(entityKey);
					results = new TList<QuestionnaireAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireAuditSelectMethod.GetAll:
                    results = QuestionnaireAuditProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireAuditSelectMethod.GetPaged:
					results = QuestionnaireAuditProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireAuditSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireAuditProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireAuditProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireAuditSelectMethod.GetByAuditId:
					_auditId = ( values["AuditId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["AuditId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireAuditProvider.GetByAuditId(_auditId);
					results = new TList<QuestionnaireAudit>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireAuditSelectMethod.Get || SelectMethod == QuestionnaireAuditSelectMethod.GetByAuditId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireAudit entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireAuditProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireAudit> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireAuditProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireAuditDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireAuditDataSource class.
	/// </summary>
	public class QuestionnaireAuditDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireAudit, QuestionnaireAuditKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditDataSourceDesigner class.
		/// </summary>
		public QuestionnaireAuditDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireAuditSelectMethod SelectMethod
		{
			get { return ((QuestionnaireAuditDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireAuditDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireAuditDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireAuditDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireAuditDataSourceActionList : DesignerActionList
	{
		private QuestionnaireAuditDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireAuditDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireAuditDataSourceActionList(QuestionnaireAuditDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireAuditSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireAuditDataSourceActionList
	
	#endregion QuestionnaireAuditDataSourceDesigner
	
	#region QuestionnaireAuditSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireAuditDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireAuditSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByAuditId method.
		/// </summary>
		GetByAuditId
	}
	
	#endregion QuestionnaireAuditSelectMethod

	#region QuestionnaireAuditFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditFilter : SqlFilter<QuestionnaireAuditColumn>
	{
	}
	
	#endregion QuestionnaireAuditFilter

	#region QuestionnaireAuditExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditExpressionBuilder : SqlExpressionBuilder<QuestionnaireAuditColumn>
	{
	}
	
	#endregion QuestionnaireAuditExpressionBuilder	

	#region QuestionnaireAuditProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireAuditChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireAudit"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireAuditProperty : ChildEntityProperty<QuestionnaireAuditChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireAuditProperty
}

