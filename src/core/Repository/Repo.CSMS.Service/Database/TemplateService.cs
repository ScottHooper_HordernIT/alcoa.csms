﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface ITemplateService
    {
        Template Get(Expression<Func<Template, bool>> where, List<Expression<Func<Template, object>>> includes);
        List<Template> GetMany(int? top, Expression<Func<Template, bool>> where, Func<IQueryable<Template>, IOrderedQueryable<Template>> orderBy, List<Expression<Func<Template, object>>> includes);
        List<Template> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Template, bool>> where, Func<IQueryable<Template>, IOrderedQueryable<Template>> orderBy, List<Expression<Func<Template, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<Template, bool>> where, List<Expression<Func<Template, object>>> includes);
        void Insert(Template item, bool saveChanges = true);
        void Update(Template item, bool saveChanges = true);
        void Delete(Template item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class TemplateService : ITemplateService
    {
        private readonly IRepository<Template> _repository;

        public TemplateService(IRepository<Template> repository)
        {
            this._repository = repository;
        }

        public Template Get(Expression<Func<Template, bool>> where, List<Expression<Func<Template, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<Template> GetMany(int? top, Expression<Func<Template, bool>> where, Func<IQueryable<Template>, IOrderedQueryable<Template>> orderBy, List<Expression<Func<Template, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<Template> GetManyPaged(int pageNumber, int pageSize, Expression<Func<Template, bool>> where, Func<IQueryable<Template>, IOrderedQueryable<Template>> orderBy, List<Expression<Func<Template, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<Template, bool>> where, List<Expression<Func<Template, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(Template item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Template");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(Template item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Template");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(Template item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("Template");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
