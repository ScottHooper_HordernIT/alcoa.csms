﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="QuestionnaireInitialContactProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class QuestionnaireInitialContactProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.QuestionnaireInitialContact, KaiZen.CSMS.Entities.QuestionnaireInitialContactKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactKey key)
		{
			return Delete(transactionManager, key.QuestionnaireInitialContactId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_questionnaireInitialContactId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _questionnaireInitialContactId)
		{
			return Delete(null, _questionnaireInitialContactId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="_contactStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByContactStatusId(System.Int32 _contactStatusId)
		{
			int count = -1;
			return GetByContactStatusId(_contactStatusId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactStatusId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContact> GetByContactStatusId(TransactionManager transactionManager, System.Int32 _contactStatusId)
		{
			int count = -1;
			return GetByContactStatusId(transactionManager, _contactStatusId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByContactStatusId(TransactionManager transactionManager, System.Int32 _contactStatusId, int start, int pageLength)
		{
			int count = -1;
			return GetByContactStatusId(transactionManager, _contactStatusId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		fkQuestionnaireInitialContactQuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contactStatusId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByContactStatusId(System.Int32 _contactStatusId, int start, int pageLength)
		{
			int count =  -1;
			return GetByContactStatusId(null, _contactStatusId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		fkQuestionnaireInitialContactQuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contactStatusId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByContactStatusId(System.Int32 _contactStatusId, int start, int pageLength,out int count)
		{
			return GetByContactStatusId(null, _contactStatusId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus key.
		///		FK_QuestionnaireInitialContact_QuestionnaireInitialContactStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contactStatusId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public abstract TList<QuestionnaireInitialContact> GetByContactStatusId(TransactionManager transactionManager, System.Int32 _contactStatusId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		FK_QuestionnaireInitialContact_Users Description: 
		/// </summary>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByCreatedByUserId(System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(_createdByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		FK_QuestionnaireInitialContact_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContact> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		FK_QuestionnaireInitialContact_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedByUserId(transactionManager, _createdByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		fkQuestionnaireInitialContactUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		fkQuestionnaireInitialContactUsers Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByCreatedByUserId(System.Int32 _createdByUserId, int start, int pageLength,out int count)
		{
			return GetByCreatedByUserId(null, _createdByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users key.
		///		FK_QuestionnaireInitialContact_Users Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public abstract TList<QuestionnaireInitialContact> GetByCreatedByUserId(TransactionManager transactionManager, System.Int32 _createdByUserId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		FK_QuestionnaireInitialContact_Users1 Description: 
		/// </summary>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByModifiedByUserId(System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(_modifiedByUserId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		FK_QuestionnaireInitialContact_Users1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		/// <remarks></remarks>
		public TList<QuestionnaireInitialContact> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		FK_QuestionnaireInitialContact_Users1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count = -1;
			return GetByModifiedByUserId(transactionManager, _modifiedByUserId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		fkQuestionnaireInitialContactUsers1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength)
		{
			int count =  -1;
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		fkQuestionnaireInitialContactUsers1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public TList<QuestionnaireInitialContact> GetByModifiedByUserId(System.Int32 _modifiedByUserId, int start, int pageLength,out int count)
		{
			return GetByModifiedByUserId(null, _modifiedByUserId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_QuestionnaireInitialContact_Users1 key.
		///		FK_QuestionnaireInitialContact_Users1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_modifiedByUserId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.QuestionnaireInitialContact objects.</returns>
		public abstract TList<QuestionnaireInitialContact> GetByModifiedByUserId(TransactionManager transactionManager, System.Int32 _modifiedByUserId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.QuestionnaireInitialContact Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContactKey key, int start, int pageLength)
		{
			return GetByQuestionnaireInitialContactId(transactionManager, key.QuestionnaireInitialContactId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(System.Int32 _questionnaireInitialContactId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactId(null,_questionnaireInitialContactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(System.Int32 _questionnaireInitialContactId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactId(null, _questionnaireInitialContactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactId)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactId(transactionManager, _questionnaireInitialContactId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireInitialContactId(transactionManager, _questionnaireInitialContactId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(System.Int32 _questionnaireInitialContactId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireInitialContactId(null, _questionnaireInitialContactId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireInitialContactId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireInitialContactId(TransactionManager transactionManager, System.Int32 _questionnaireInitialContactId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(null,_questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength)
		{
			int count = -1;
			return GetByQuestionnaireId(transactionManager, _questionnaireId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(System.Int32 _questionnaireId, int start, int pageLength, out int count)
		{
			return GetByQuestionnaireId(null, _questionnaireId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_QuestionnaireInitialContact index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_questionnaireId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.QuestionnaireInitialContact GetByQuestionnaireId(TransactionManager transactionManager, System.Int32 _questionnaireId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;QuestionnaireInitialContact&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;QuestionnaireInitialContact&gt;"/></returns>
		public static TList<QuestionnaireInitialContact> Fill(IDataReader reader, TList<QuestionnaireInitialContact> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.QuestionnaireInitialContact c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("QuestionnaireInitialContact")
					.Append("|").Append((System.Int32)reader[((int)QuestionnaireInitialContactColumn.QuestionnaireInitialContactId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<QuestionnaireInitialContact>(
					key.ToString(), // EntityTrackingKey
					"QuestionnaireInitialContact",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.QuestionnaireInitialContact();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.QuestionnaireInitialContactId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.QuestionnaireInitialContactId - 1)];
					c.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.QuestionnaireId - 1)];
					c.ContactFirstName = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactFirstName - 1)];
					c.ContactLastName = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactLastName - 1)];
					c.ContactEmail = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactEmail - 1)];
					c.ContactTitle = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactTitle - 1)];
					c.ContactPhone = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactPhone - 1)];
					c.ContactStatusId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.ContactStatusId - 1)];
					c.VerifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactColumn.VerifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactColumn.VerifiedDate - 1)];
					c.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireInitialContactColumn.ModifiedDate - 1)];
					c.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.ModifiedByUserId - 1)];
					c.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.CreatedByUserId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.QuestionnaireInitialContact entity)
		{
			if (!reader.Read()) return;
			
			entity.QuestionnaireInitialContactId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.QuestionnaireInitialContactId - 1)];
			entity.QuestionnaireId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.QuestionnaireId - 1)];
			entity.ContactFirstName = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactFirstName - 1)];
			entity.ContactLastName = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactLastName - 1)];
			entity.ContactEmail = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactEmail - 1)];
			entity.ContactTitle = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactTitle - 1)];
			entity.ContactPhone = (System.String)reader[((int)QuestionnaireInitialContactColumn.ContactPhone - 1)];
			entity.ContactStatusId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.ContactStatusId - 1)];
			entity.VerifiedDate = (reader.IsDBNull(((int)QuestionnaireInitialContactColumn.VerifiedDate - 1)))?null:(System.DateTime?)reader[((int)QuestionnaireInitialContactColumn.VerifiedDate - 1)];
			entity.ModifiedDate = (System.DateTime)reader[((int)QuestionnaireInitialContactColumn.ModifiedDate - 1)];
			entity.ModifiedByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.ModifiedByUserId - 1)];
			entity.CreatedByUserId = (System.Int32)reader[((int)QuestionnaireInitialContactColumn.CreatedByUserId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.QuestionnaireInitialContact entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.QuestionnaireInitialContactId = (System.Int32)dataRow["QuestionnaireInitialContactId"];
			entity.QuestionnaireId = (System.Int32)dataRow["QuestionnaireId"];
			entity.ContactFirstName = (System.String)dataRow["ContactFirstName"];
			entity.ContactLastName = (System.String)dataRow["ContactLastName"];
			entity.ContactEmail = (System.String)dataRow["ContactEmail"];
			entity.ContactTitle = (System.String)dataRow["ContactTitle"];
			entity.ContactPhone = (System.String)dataRow["ContactPhone"];
			entity.ContactStatusId = (System.Int32)dataRow["ContactStatusId"];
			entity.VerifiedDate = Convert.IsDBNull(dataRow["VerifiedDate"]) ? null : (System.DateTime?)dataRow["VerifiedDate"];
			entity.ModifiedDate = (System.DateTime)dataRow["ModifiedDate"];
			entity.ModifiedByUserId = (System.Int32)dataRow["ModifiedByUserId"];
			entity.CreatedByUserId = (System.Int32)dataRow["CreatedByUserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.QuestionnaireInitialContact"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContact Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContact entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region QuestionnaireIdSource	
			if (CanDeepLoad(entity, "Questionnaire|QuestionnaireIdSource", deepLoadType, innerList) 
				&& entity.QuestionnaireIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.QuestionnaireId;
				Questionnaire tmpEntity = EntityManager.LocateEntity<Questionnaire>(EntityLocator.ConstructKeyFromPkItems(typeof(Questionnaire), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.QuestionnaireIdSource = tmpEntity;
				else
					entity.QuestionnaireIdSource = DataRepository.QuestionnaireProvider.GetByQuestionnaireId(transactionManager, entity.QuestionnaireId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.QuestionnaireIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireProvider.DeepLoad(transactionManager, entity.QuestionnaireIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion QuestionnaireIdSource

			#region ContactStatusIdSource	
			if (CanDeepLoad(entity, "QuestionnaireInitialContactStatus|ContactStatusIdSource", deepLoadType, innerList) 
				&& entity.ContactStatusIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ContactStatusId;
				QuestionnaireInitialContactStatus tmpEntity = EntityManager.LocateEntity<QuestionnaireInitialContactStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(QuestionnaireInitialContactStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ContactStatusIdSource = tmpEntity;
				else
					entity.ContactStatusIdSource = DataRepository.QuestionnaireInitialContactStatusProvider.GetByQuestionnaireInitialContactStatusId(transactionManager, entity.ContactStatusId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContactStatusIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ContactStatusIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.QuestionnaireInitialContactStatusProvider.DeepLoad(transactionManager, entity.ContactStatusIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ContactStatusIdSource

			#region CreatedByUserIdSource	
			if (CanDeepLoad(entity, "Users|CreatedByUserIdSource", deepLoadType, innerList) 
				&& entity.CreatedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CreatedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CreatedByUserIdSource = tmpEntity;
				else
					entity.CreatedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.CreatedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CreatedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CreatedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.CreatedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CreatedByUserIdSource

			#region ModifiedByUserIdSource	
			if (CanDeepLoad(entity, "Users|ModifiedByUserIdSource", deepLoadType, innerList) 
				&& entity.ModifiedByUserIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ModifiedByUserId;
				Users tmpEntity = EntityManager.LocateEntity<Users>(EntityLocator.ConstructKeyFromPkItems(typeof(Users), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ModifiedByUserIdSource = tmpEntity;
				else
					entity.ModifiedByUserIdSource = DataRepository.UsersProvider.GetByUserId(transactionManager, entity.ModifiedByUserId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ModifiedByUserIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ModifiedByUserIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.UsersProvider.DeepLoad(transactionManager, entity.ModifiedByUserIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ModifiedByUserIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByQuestionnaireInitialContactId methods when available
			
			#region QuestionnaireInitialContactEmailCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'QuestionnaireInitialContactEmailCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.QuestionnaireInitialContactEmailCollection = DataRepository.QuestionnaireInitialContactEmailProvider.GetByContactId(transactionManager, entity.QuestionnaireInitialContactId);

				if (deep && entity.QuestionnaireInitialContactEmailCollection.Count > 0)
				{
					deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<QuestionnaireInitialContactEmail>) DataRepository.QuestionnaireInitialContactEmailProvider.DeepLoad,
						new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.QuestionnaireInitialContact object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.QuestionnaireInitialContact instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.QuestionnaireInitialContact Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.QuestionnaireInitialContact entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region QuestionnaireIdSource
			if (CanDeepSave(entity, "Questionnaire|QuestionnaireIdSource", deepSaveType, innerList) 
				&& entity.QuestionnaireIdSource != null)
			{
				DataRepository.QuestionnaireProvider.Save(transactionManager, entity.QuestionnaireIdSource);
				entity.QuestionnaireId = entity.QuestionnaireIdSource.QuestionnaireId;
			}
			#endregion 
			
			#region ContactStatusIdSource
			if (CanDeepSave(entity, "QuestionnaireInitialContactStatus|ContactStatusIdSource", deepSaveType, innerList) 
				&& entity.ContactStatusIdSource != null)
			{
				DataRepository.QuestionnaireInitialContactStatusProvider.Save(transactionManager, entity.ContactStatusIdSource);
				entity.ContactStatusId = entity.ContactStatusIdSource.QuestionnaireInitialContactStatusId;
			}
			#endregion 
			
			#region CreatedByUserIdSource
			if (CanDeepSave(entity, "Users|CreatedByUserIdSource", deepSaveType, innerList) 
				&& entity.CreatedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.CreatedByUserIdSource);
				entity.CreatedByUserId = entity.CreatedByUserIdSource.UserId;
			}
			#endregion 
			
			#region ModifiedByUserIdSource
			if (CanDeepSave(entity, "Users|ModifiedByUserIdSource", deepSaveType, innerList) 
				&& entity.ModifiedByUserIdSource != null)
			{
				DataRepository.UsersProvider.Save(transactionManager, entity.ModifiedByUserIdSource);
				entity.ModifiedByUserId = entity.ModifiedByUserIdSource.UserId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<QuestionnaireInitialContactEmail>
				if (CanDeepSave(entity.QuestionnaireInitialContactEmailCollection, "List<QuestionnaireInitialContactEmail>|QuestionnaireInitialContactEmailCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(QuestionnaireInitialContactEmail child in entity.QuestionnaireInitialContactEmailCollection)
					{
						if(child.ContactIdSource != null)
						{
							child.ContactId = child.ContactIdSource.QuestionnaireInitialContactId;
						}
						else
						{
							child.ContactId = entity.QuestionnaireInitialContactId;
						}

					}

					if (entity.QuestionnaireInitialContactEmailCollection.Count > 0 || entity.QuestionnaireInitialContactEmailCollection.DeletedItems.Count > 0)
					{
						//DataRepository.QuestionnaireInitialContactEmailProvider.Save(transactionManager, entity.QuestionnaireInitialContactEmailCollection);
						
						deepHandles.Add("QuestionnaireInitialContactEmailCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< QuestionnaireInitialContactEmail >) DataRepository.QuestionnaireInitialContactEmailProvider.DeepSave,
							new object[] { transactionManager, entity.QuestionnaireInitialContactEmailCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region QuestionnaireInitialContactChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.QuestionnaireInitialContact</c>
	///</summary>
	public enum QuestionnaireInitialContactChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Questionnaire</c> at QuestionnaireIdSource
		///</summary>
		[ChildEntityType(typeof(Questionnaire))]
		Questionnaire,
			
		///<summary>
		/// Composite Property for <c>QuestionnaireInitialContactStatus</c> at ContactStatusIdSource
		///</summary>
		[ChildEntityType(typeof(QuestionnaireInitialContactStatus))]
		QuestionnaireInitialContactStatus,
			
		///<summary>
		/// Composite Property for <c>Users</c> at CreatedByUserIdSource
		///</summary>
		[ChildEntityType(typeof(Users))]
		Users,
	
		///<summary>
		/// Collection of <c>QuestionnaireInitialContact</c> as OneToMany for QuestionnaireInitialContactEmailCollection
		///</summary>
		[ChildEntityType(typeof(TList<QuestionnaireInitialContactEmail>))]
		QuestionnaireInitialContactEmailCollection,
	}
	
	#endregion QuestionnaireInitialContactChildEntityTypes
	
	#region QuestionnaireInitialContactFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;QuestionnaireInitialContactColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactFilterBuilder : SqlFilterBuilder<QuestionnaireInitialContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactFilterBuilder
	
	#region QuestionnaireInitialContactParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;QuestionnaireInitialContactColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialContactParameterBuilder : ParameterizedSqlFilterBuilder<QuestionnaireInitialContactColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactParameterBuilder class.
		/// </summary>
		public QuestionnaireInitialContactParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public QuestionnaireInitialContactParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public QuestionnaireInitialContactParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion QuestionnaireInitialContactParameterBuilder
	
	#region QuestionnaireInitialContactSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;QuestionnaireInitialContactColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialContact"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class QuestionnaireInitialContactSortBuilder : SqlSortBuilder<QuestionnaireInitialContactColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialContactSqlSortBuilder class.
		/// </summary>
		public QuestionnaireInitialContactSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion QuestionnaireInitialContactSortBuilder
	
} // end namespace
