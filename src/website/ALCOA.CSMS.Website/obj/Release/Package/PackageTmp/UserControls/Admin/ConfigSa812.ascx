<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Admin_ConfigSa812" Codebehind="ConfigSa812.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v14.1, Version=14.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<dxwgv:aspxgridview id="grid" runat="server" 
    AutoGenerateColumns="False" 
    CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
    CssPostfix="Office2003Blue" DataSourceID="ConfigSa812DataSource"  OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
            OnCellEditorInitialize="grid_CellEditorInitialize" ClientInstanceName="grid"
    KeyFieldName="Sa812Id" Width="900px">
    <ImagesFilterControl>
        <LoadingPanel Url="~/App_Themes/Office2003Blue/Editors/Loading.gif">
        </LoadingPanel>
    </ImagesFilterControl>
    <Styles CssFilePath="~/App_Themes/Office2003Blue/{0}/styles.css" 
        CssPostfix="Office2003Blue">
        <Header ImageSpacing="5px" SortingImageSpacing="5px" HorizontalAlign="Center" Wrap="True">
        </Header>
        <LoadingPanel ImageSpacing="10px">
        </LoadingPanel>
    </Styles>
    <Images SpriteCssFilePath="~/App_Themes/Office2003Blue/{0}/sprite.css">
        <LoadingPanelOnStatusBar Url="~/App_Themes/Office2003Blue/GridView/gvLoadingOnStatusBar.gif">
        </LoadingPanelOnStatusBar>
        <CollapsedButton Height="12px"
            Width="11px" />
        <ExpandedButton Height="12px"
            Width="11px" />
        <DetailCollapsedButton Height="12px"
            Width="11px" />
        <DetailExpandedButton Height="12px"
            Width="11px" />
        <FilterRowButton Height="13px" Width="13px" />
        <LoadingPanel Url="~/App_Themes/Office2003BlueOld/GridView/Loading.gif"><%--Enhancement_023:change by debashis for testing platform upgradation--%>
        </LoadingPanel>
    </Images>
    <Columns>
        <dxwgv:GridViewCommandColumn VisibleIndex="0" Caption=" " Width="115px">
            <EditButton Visible="True">
            </EditButton>
            <NewButton Visible="True">
            </NewButton>
            <DeleteButton Visible="True">
            </DeleteButton>
            <ClearFilterButton Visible="true"></ClearFilterButton>
        </dxwgv:GridViewCommandColumn>
        <dxwgv:GridViewDataTextColumn FieldName="Sa812Id" ReadOnly="True" Visible="False"
            VisibleIndex="0">
        </dxwgv:GridViewDataTextColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="SiteId" VisibleIndex="1" Width="150px">
            <PropertiesComboBox DataSourceID="dsSitesFilter" TextField="SiteName" ValueField="SiteId"
                ValueType="System.Int32">
            </PropertiesComboBox>
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="Supervisor" FieldName="SupervisorUserId"
            VisibleIndex="2">
            <PropertiesComboBox DataSourceID="UsersAlcoanDataSource" DropDownHeight="150px" IncrementalFilteringMode="StartsWith"
                TextField="UserFullName" ValueField="UserId" ValueType="System.Int32" NullDisplayText="(None)">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataDateColumn Caption="Supervisor Date Training Completed" FieldName="DateTrainingCompleted" VisibleIndex="3" Width="120px">
        </dxwgv:GridViewDataDateColumn>
        <dxwgv:GridViewDataComboBoxColumn Caption="H&amp;S Assessor" FieldName="EhsConsultantId"
            VisibleIndex="4" Width="200px">
            <PropertiesComboBox DataSourceID="SqlDataSource1" DropDownHeight="150px"
                IncrementalFilteringMode="StartsWith" TextField="UserFullName" ValueField="EhsConsultantId"  NullDisplayText="(None)"
                ValueType="System.Int32">
            </PropertiesComboBox>
            <Settings SortMode="DisplayText" />
        </dxwgv:GridViewDataComboBoxColumn>
        <dxwgv:GridViewDataDateColumn Caption="H&amp;S Assessor Date Training Completed" FieldName="DateTrainingCompleted2" VisibleIndex="5" Width="120px">
        </dxwgv:GridViewDataDateColumn>
    </Columns>
    <Settings ShowFilterRow="true" />
    <StylesEditors>
        <ProgressBar Height="25px">
        </ProgressBar>
    </StylesEditors>
    <SettingsBehavior ColumnResizeMode="NextColumn" ConfirmDelete="True"></SettingsBehavior>
</dxwgv:aspxgridview>


    <br />
    <br />
    <strong>Template</strong>
    <br />
    Legend: <font color="green" style="FONT-WEIGHT: bold">{date_today} {site} {contract_supervisor} {css_33051_trained_at} {ehs_consultant} {ehsc_33051_trained_at} {number_embedded} {number_interviewed} {number_active}</font>
    <dx:ASPxHtmlEditor ID="aheTemplate" runat="server" Height="600px" 
        Width="900px">
    </dx:ASPxHtmlEditor><br /><br />
<dx:ASPxLabel ID="lblSave" runat="server" Text="" ForeColor="Red">
</dx:ASPxLabel><br />
    <dx:ASPxButton ID="btnSave" runat="server" onclick="btnSave_Click" 
        Text="Save">
    </dx:ASPxButton>
<data:ConfigSa812DataSource
    ID="ConfigSa812DataSource"
    runat="server">
</data:ConfigSa812DataSource>
                    
<data:SitesDataSource ID="dsSitesFilter" runat="server" Filter="IsVisible = True AND Editable = True" Sort="SiteName ASC"></data:SitesDataSource> 

<asp:SqlDataSource ID="UsersEhsConsultantsGetActiveAllDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>" SelectCommand="_UsersEhsConsultants_GetAll_Active" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

<data:EntityDataSource ID="UsersAlcoanDataSource" runat="server"
    ProviderName="UsersAlcoanProvider"
    EntityTypeName="KaiZen.CSMS.UsersAlcoan, KaiZen.CSMS"
    EntityKeyTypeName="System.Int32"
    SelectMethod="GetAll"
    Sort="UserFullName ASC"
/> 
<asp:SqlDataSource ID="SupervisorDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.SupervisorUserId,B.UserFullName from 
dbo.ConfigSa812 A LEFT OUTER JOIN dbo.UsersFullName B ON
A.SupervisorUserId=B.UserId where A.SupervisorUserId Is Not Null Order By UserFullName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="HSAssessorDS" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.EhsConsultantId,B.UserFullName from 
dbo.ConfigSa812 A LEFT OUTER JOIN dbo.UsersEhsConsultants B ON
A.EhsConsultantId=B.EhsConsultantId Order By UserFullName">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ALCOA_WAO_CSMWP_devConnectionString %>"
    SelectCommand="select Distinct A.EhsConsultantId,A.UserFullName from 
 dbo.UsersEhsConsultants A ">
</asp:SqlDataSource>


