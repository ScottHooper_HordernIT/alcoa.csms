﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

#endregion

namespace KaiZen.CSMS.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="DocumentsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class DocumentsProviderBaseCore : EntityProviderBase<KaiZen.CSMS.Entities.Documents, KaiZen.CSMS.Entities.DocumentsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsKey key)
		{
			return Delete(transactionManager, key.DocumentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_documentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _documentId)
		{
			return Delete(null, _documentId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _documentId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		FK_Documents_Regions Description: 
		/// </summary>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		public TList<Documents> GetByRegionId(System.Int32? _regionId)
		{
			int count = -1;
			return GetByRegionId(_regionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		FK_Documents_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		/// <remarks></remarks>
		public TList<Documents> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		FK_Documents_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		public TList<Documents> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId, int start, int pageLength)
		{
			int count = -1;
			return GetByRegionId(transactionManager, _regionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		fkDocumentsRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		public TList<Documents> GetByRegionId(System.Int32? _regionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRegionId(null, _regionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		fkDocumentsRegions Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_regionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		public TList<Documents> GetByRegionId(System.Int32? _regionId, int start, int pageLength,out int count)
		{
			return GetByRegionId(null, _regionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_Documents_Regions key.
		///		FK_Documents_Regions Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_regionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of KaiZen.CSMS.Entities.Documents objects.</returns>
		public abstract TList<Documents> GetByRegionId(TransactionManager transactionManager, System.Int32? _regionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override KaiZen.CSMS.Entities.Documents Get(TransactionManager transactionManager, KaiZen.CSMS.Entities.DocumentsKey key, int start, int pageLength)
		{
			return GetByDocumentId(transactionManager, key.DocumentId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_APSS_MFU index.
		/// </summary>
		/// <param name="_documentId"></param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public KaiZen.CSMS.Entities.Documents GetByDocumentId(System.Int32 _documentId)
		{
			int count = -1;
			return GetByDocumentId(null,_documentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSS_MFU index.
		/// </summary>
		/// <param name="_documentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public KaiZen.CSMS.Entities.Documents GetByDocumentId(System.Int32 _documentId, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentId(null, _documentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSS_MFU index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public KaiZen.CSMS.Entities.Documents GetByDocumentId(TransactionManager transactionManager, System.Int32 _documentId)
		{
			int count = -1;
			return GetByDocumentId(transactionManager, _documentId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSS_MFU index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public KaiZen.CSMS.Entities.Documents GetByDocumentId(TransactionManager transactionManager, System.Int32 _documentId, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentId(transactionManager, _documentId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSS_MFU index.
		/// </summary>
		/// <param name="_documentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public KaiZen.CSMS.Entities.Documents GetByDocumentId(System.Int32 _documentId, int start, int pageLength, out int count)
		{
			return GetByDocumentId(null, _documentId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_APSS_MFU index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="KaiZen.CSMS.Entities.Documents"/> class.</returns>
		public abstract KaiZen.CSMS.Entities.Documents GetByDocumentId(TransactionManager transactionManager, System.Int32 _documentId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="_documentFileName"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentFileName(System.String _documentFileName)
		{
			int count = -1;
			return GetByDocumentFileName(null,_documentFileName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="_documentFileName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentFileName(System.String _documentFileName, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentFileName(null, _documentFileName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentFileName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentFileName(TransactionManager transactionManager, System.String _documentFileName)
		{
			int count = -1;
			return GetByDocumentFileName(transactionManager, _documentFileName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentFileName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentFileName(TransactionManager transactionManager, System.String _documentFileName, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentFileName(transactionManager, _documentFileName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="_documentFileName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentFileName(System.String _documentFileName, int start, int pageLength, out int count)
		{
			return GetByDocumentFileName(null, _documentFileName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentFileName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentFileName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public abstract TList<Documents> GetByDocumentFileName(TransactionManager transactionManager, System.String _documentFileName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="_documentType"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentType(System.String _documentType)
		{
			int count = -1;
			return GetByDocumentType(null,_documentType, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="_documentType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentType(System.String _documentType, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentType(null, _documentType, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentType"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentType(TransactionManager transactionManager, System.String _documentType)
		{
			int count = -1;
			return GetByDocumentType(transactionManager, _documentType, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentType(TransactionManager transactionManager, System.String _documentType, int start, int pageLength)
		{
			int count = -1;
			return GetByDocumentType(transactionManager, _documentType, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="_documentType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public TList<Documents> GetByDocumentType(System.String _documentType, int start, int pageLength, out int count)
		{
			return GetByDocumentType(null, _documentType, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Documents_DocumentType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_documentType"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Documents&gt;"/> class.</returns>
		public abstract TList<Documents> GetByDocumentType(TransactionManager transactionManager, System.String _documentType, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#region _Documents_GetByDocumentFileName_Custom 
		
		/// <summary>
		///	This method wrap the '_Documents_GetByDocumentFileName_Custom' stored procedure. 
		/// </summary>
		/// <param name="documentFileName"> A <c>System.String</c> instance.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByDocumentFileName_Custom(System.String documentFileName)
		{
			return GetByDocumentFileName_Custom(null, 0, int.MaxValue , documentFileName);
		}
		
		/// <summary>
		///	This method wrap the '_Documents_GetByDocumentFileName_Custom' stored procedure. 
		/// </summary>
		/// <param name="documentFileName"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByDocumentFileName_Custom(int start, int pageLength, System.String documentFileName)
		{
			return GetByDocumentFileName_Custom(null, start, pageLength , documentFileName);
		}
				
		/// <summary>
		///	This method wrap the '_Documents_GetByDocumentFileName_Custom' stored procedure. 
		/// </summary>
		/// <param name="documentFileName"> A <c>System.String</c> instance.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public DataSet GetByDocumentFileName_Custom(TransactionManager transactionManager, System.String documentFileName)
		{
			return GetByDocumentFileName_Custom(transactionManager, 0, int.MaxValue , documentFileName);
		}
		
		/// <summary>
		///	This method wrap the '_Documents_GetByDocumentFileName_Custom' stored procedure. 
		/// </summary>
		/// <param name="documentFileName"> A <c>System.String</c> instance.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <remark>This method is generate from a stored procedure.</remark>
		/// <returns>A <see cref="DataSet"/> instance.</returns>
		public abstract DataSet GetByDocumentFileName_Custom(TransactionManager transactionManager, int start, int pageLength , System.String documentFileName);
		
		#endregion
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Documents&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Documents&gt;"/></returns>
		public static TList<Documents> Fill(IDataReader reader, TList<Documents> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				KaiZen.CSMS.Entities.Documents c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Documents")
					.Append("|").Append((System.Int32)reader[((int)DocumentsColumn.DocumentId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Documents>(
					key.ToString(), // EntityTrackingKey
					"Documents",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new KaiZen.CSMS.Entities.Documents();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.DocumentId = (System.Int32)reader[((int)DocumentsColumn.DocumentId - 1)];
					c.RegionId = (reader.IsDBNull(((int)DocumentsColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)DocumentsColumn.RegionId - 1)];
					c.DocumentName = (reader.IsDBNull(((int)DocumentsColumn.DocumentName - 1)))?null:(System.String)reader[((int)DocumentsColumn.DocumentName - 1)];
					c.DocumentFileName = (System.String)reader[((int)DocumentsColumn.DocumentFileName - 1)];
					c.DocumentFileDescription = (reader.IsDBNull(((int)DocumentsColumn.DocumentFileDescription - 1)))?null:(System.String)reader[((int)DocumentsColumn.DocumentFileDescription - 1)];
					c.DocumentType = (System.String)reader[((int)DocumentsColumn.DocumentType - 1)];
					c.RootNode = (reader.IsDBNull(((int)DocumentsColumn.RootNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.RootNode - 1)];
					c.ParentNode = (reader.IsDBNull(((int)DocumentsColumn.ParentNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.ParentNode - 1)];
					c.ChildNode = (reader.IsDBNull(((int)DocumentsColumn.ChildNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.ChildNode - 1)];
					c.UploadedFile = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile - 1)];
					c.UploadedFile2 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile2 - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile2 - 1)];
					c.UploadedFile3 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile3 - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile3 - 1)];
					c.UploadedFileName = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName - 1)];
					c.UploadedFileName2 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName2 - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName2 - 1)];
					c.UploadedFileName3 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName3 - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName3 - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Documents"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Documents"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, KaiZen.CSMS.Entities.Documents entity)
		{
			if (!reader.Read()) return;
			
			entity.DocumentId = (System.Int32)reader[((int)DocumentsColumn.DocumentId - 1)];
			entity.RegionId = (reader.IsDBNull(((int)DocumentsColumn.RegionId - 1)))?null:(System.Int32?)reader[((int)DocumentsColumn.RegionId - 1)];
			entity.DocumentName = (reader.IsDBNull(((int)DocumentsColumn.DocumentName - 1)))?null:(System.String)reader[((int)DocumentsColumn.DocumentName - 1)];
			entity.DocumentFileName = (System.String)reader[((int)DocumentsColumn.DocumentFileName - 1)];
			entity.DocumentFileDescription = (reader.IsDBNull(((int)DocumentsColumn.DocumentFileDescription - 1)))?null:(System.String)reader[((int)DocumentsColumn.DocumentFileDescription - 1)];
			entity.DocumentType = (System.String)reader[((int)DocumentsColumn.DocumentType - 1)];
			entity.RootNode = (reader.IsDBNull(((int)DocumentsColumn.RootNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.RootNode - 1)];
			entity.ParentNode = (reader.IsDBNull(((int)DocumentsColumn.ParentNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.ParentNode - 1)];
			entity.ChildNode = (reader.IsDBNull(((int)DocumentsColumn.ChildNode - 1)))?null:(System.String)reader[((int)DocumentsColumn.ChildNode - 1)];
			entity.UploadedFile = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile - 1)];
			entity.UploadedFile2 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile2 - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile2 - 1)];
			entity.UploadedFile3 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFile3 - 1)))?null:(System.Byte[])reader[((int)DocumentsColumn.UploadedFile3 - 1)];
			entity.UploadedFileName = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName - 1)];
			entity.UploadedFileName2 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName2 - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName2 - 1)];
			entity.UploadedFileName3 = (reader.IsDBNull(((int)DocumentsColumn.UploadedFileName3 - 1)))?null:(System.String)reader[((int)DocumentsColumn.UploadedFileName3 - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="KaiZen.CSMS.Entities.Documents"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Documents"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, KaiZen.CSMS.Entities.Documents entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.DocumentId = (System.Int32)dataRow["DocumentId"];
			entity.RegionId = Convert.IsDBNull(dataRow["RegionId"]) ? null : (System.Int32?)dataRow["RegionId"];
			entity.DocumentName = Convert.IsDBNull(dataRow["DocumentName"]) ? null : (System.String)dataRow["DocumentName"];
			entity.DocumentFileName = (System.String)dataRow["DocumentFileName"];
			entity.DocumentFileDescription = Convert.IsDBNull(dataRow["DocumentFileDescription"]) ? null : (System.String)dataRow["DocumentFileDescription"];
			entity.DocumentType = (System.String)dataRow["DocumentType"];
			entity.RootNode = Convert.IsDBNull(dataRow["RootNode"]) ? null : (System.String)dataRow["RootNode"];
			entity.ParentNode = Convert.IsDBNull(dataRow["ParentNode"]) ? null : (System.String)dataRow["ParentNode"];
			entity.ChildNode = Convert.IsDBNull(dataRow["ChildNode"]) ? null : (System.String)dataRow["ChildNode"];
			entity.UploadedFile = Convert.IsDBNull(dataRow["UploadedFile"]) ? null : (System.Byte[])dataRow["UploadedFile"];
			entity.UploadedFile2 = Convert.IsDBNull(dataRow["UploadedFile2"]) ? null : (System.Byte[])dataRow["UploadedFile2"];
			entity.UploadedFile3 = Convert.IsDBNull(dataRow["UploadedFile3"]) ? null : (System.Byte[])dataRow["UploadedFile3"];
			entity.UploadedFileName = Convert.IsDBNull(dataRow["UploadedFileName"]) ? null : (System.String)dataRow["UploadedFileName"];
			entity.UploadedFileName2 = Convert.IsDBNull(dataRow["UploadedFileName2"]) ? null : (System.String)dataRow["UploadedFileName2"];
			entity.UploadedFileName3 = Convert.IsDBNull(dataRow["UploadedFileName3"]) ? null : (System.String)dataRow["UploadedFileName3"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="KaiZen.CSMS.Entities.Documents"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Documents Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, KaiZen.CSMS.Entities.Documents entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region RegionIdSource	
			if (CanDeepLoad(entity, "Regions|RegionIdSource", deepLoadType, innerList) 
				&& entity.RegionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.RegionId ?? (int)0);
				Regions tmpEntity = EntityManager.LocateEntity<Regions>(EntityLocator.ConstructKeyFromPkItems(typeof(Regions), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RegionIdSource = tmpEntity;
				else
					entity.RegionIdSource = DataRepository.RegionsProvider.GetByRegionId(transactionManager, (entity.RegionId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RegionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RegionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.RegionsProvider.DeepLoad(transactionManager, entity.RegionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RegionIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the KaiZen.CSMS.Entities.Documents object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">KaiZen.CSMS.Entities.Documents instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">KaiZen.CSMS.Entities.Documents Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, KaiZen.CSMS.Entities.Documents entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region RegionIdSource
			if (CanDeepSave(entity, "Regions|RegionIdSource", deepSaveType, innerList) 
				&& entity.RegionIdSource != null)
			{
				DataRepository.RegionsProvider.Save(transactionManager, entity.RegionIdSource);
				entity.RegionId = entity.RegionIdSource.RegionId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region DocumentsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>KaiZen.CSMS.Entities.Documents</c>
	///</summary>
	public enum DocumentsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Regions</c> at RegionIdSource
		///</summary>
		[ChildEntityType(typeof(Regions))]
		Regions,
		}
	
	#endregion DocumentsChildEntityTypes
	
	#region DocumentsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;DocumentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsFilterBuilder : SqlFilterBuilder<DocumentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsFilterBuilder class.
		/// </summary>
		public DocumentsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsFilterBuilder
	
	#region DocumentsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;DocumentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class DocumentsParameterBuilder : ParameterizedSqlFilterBuilder<DocumentsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsParameterBuilder class.
		/// </summary>
		public DocumentsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the DocumentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public DocumentsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the DocumentsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public DocumentsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion DocumentsParameterBuilder
	
	#region DocumentsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;DocumentsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Documents"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class DocumentsSortBuilder : SqlSortBuilder<DocumentsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DocumentsSqlSortBuilder class.
		/// </summary>
		public DocumentsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion DocumentsSortBuilder
	
} // end namespace
