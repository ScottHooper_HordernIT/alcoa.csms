﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationRationaleProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationRationaleDataSourceDesigner))]
	public class QuestionnaireVerificationRationaleDataSource : ProviderDataSource<QuestionnaireVerificationRationale, QuestionnaireVerificationRationaleKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleDataSource class.
		/// </summary>
		public QuestionnaireVerificationRationaleDataSource() : base(new QuestionnaireVerificationRationaleService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationRationaleDataSourceView used by the QuestionnaireVerificationRationaleDataSource.
		/// </summary>
		protected QuestionnaireVerificationRationaleDataSourceView QuestionnaireVerificationRationaleView
		{
			get { return ( View as QuestionnaireVerificationRationaleDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationRationaleDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationRationaleSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationRationaleSelectMethod selectMethod = QuestionnaireVerificationRationaleSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationRationaleSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationRationaleDataSourceView class that is to be
		/// used by the QuestionnaireVerificationRationaleDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationRationaleDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationRationale, QuestionnaireVerificationRationaleKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationRationaleDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationRationaleDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationRationaleDataSourceView : ProviderDataSourceView<QuestionnaireVerificationRationale, QuestionnaireVerificationRationaleKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationRationaleDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationRationaleDataSourceView(QuestionnaireVerificationRationaleDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationRationaleDataSource QuestionnaireVerificationRationaleOwner
		{
			get { return Owner as QuestionnaireVerificationRationaleDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationRationaleSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationRationaleOwner.SelectMethod; }
			set { QuestionnaireVerificationRationaleOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationRationaleService QuestionnaireVerificationRationaleProvider
		{
			get { return Provider as QuestionnaireVerificationRationaleService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationRationale> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationRationale> results = null;
			QuestionnaireVerificationRationale item;
			count = 0;
			
			System.Int32 _questionnaireVerificationRationale;
			System.Int32 _sectionId;
			System.String _questionId;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationRationaleSelectMethod.Get:
					QuestionnaireVerificationRationaleKey entityKey  = new QuestionnaireVerificationRationaleKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationRationaleProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationRationaleSelectMethod.GetAll:
                    results = QuestionnaireVerificationRationaleProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationRationaleSelectMethod.GetPaged:
					results = QuestionnaireVerificationRationaleProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationRationaleSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationRationaleProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationRationaleProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationRationaleSelectMethod.GetByQuestionnaireVerificationRationale:
					_questionnaireVerificationRationale = ( values["QuestionnaireVerificationRationale"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireVerificationRationale"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationRationaleProvider.GetByQuestionnaireVerificationRationale(_questionnaireVerificationRationale);
					results = new TList<QuestionnaireVerificationRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationRationaleSelectMethod.GetBySectionIdQuestionId:
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					_questionId = ( values["QuestionId"] != null ) ? (System.String) EntityUtil.ChangeType(values["QuestionId"], typeof(System.String)) : string.Empty;
					item = QuestionnaireVerificationRationaleProvider.GetBySectionIdQuestionId(_sectionId, _questionId);
					results = new TList<QuestionnaireVerificationRationale>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationRationaleSelectMethod.Get || SelectMethod == QuestionnaireVerificationRationaleSelectMethod.GetByQuestionnaireVerificationRationale )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationRationale entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationRationaleProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationRationale> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationRationaleProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationRationaleDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationRationaleDataSource class.
	/// </summary>
	public class QuestionnaireVerificationRationaleDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationRationale, QuestionnaireVerificationRationaleKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationRationaleDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationRationaleSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationRationaleDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationRationaleDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationRationaleDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationRationaleDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationRationaleDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationRationaleDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationRationaleDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationRationaleDataSourceActionList(QuestionnaireVerificationRationaleDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationRationaleSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationRationaleDataSourceActionList
	
	#endregion QuestionnaireVerificationRationaleDataSourceDesigner
	
	#region QuestionnaireVerificationRationaleSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationRationaleDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationRationaleSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireVerificationRationale method.
		/// </summary>
		GetByQuestionnaireVerificationRationale,
		/// <summary>
		/// Represents the GetBySectionIdQuestionId method.
		/// </summary>
		GetBySectionIdQuestionId
	}
	
	#endregion QuestionnaireVerificationRationaleSelectMethod

	#region QuestionnaireVerificationRationaleFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleFilter : SqlFilter<QuestionnaireVerificationRationaleColumn>
	{
	}
	
	#endregion QuestionnaireVerificationRationaleFilter

	#region QuestionnaireVerificationRationaleExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationRationaleColumn>
	{
	}
	
	#endregion QuestionnaireVerificationRationaleExpressionBuilder	

	#region QuestionnaireVerificationRationaleProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationRationaleChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationRationale"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationRationaleProperty : ChildEntityProperty<QuestionnaireVerificationRationaleChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationRationaleProperty
}

