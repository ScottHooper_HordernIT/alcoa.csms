using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using repo = Repo.CSMS.Service.Database;
using model = Repo.CSMS.DAL.EntityModels;
using System.Security.Principal;
using KaiZen.CSMS.Entities;
using System.Collections.Generic;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using KaiZen.Library;
using DevExpress.Web.ASPxGridView;
//Written by Ashley Goldstraw for Devtrack item 294.  7/10/2015
//Manages files for S812 audits.  Admin can upload the files.  Other users can view and download them.
public partial class UserControls_AuditS812EvidenceFolder : System.Web.UI.UserControl
{
    
    Auth auth = new Auth();
    public static WindowsIdentity ident;
    repo.IS812EvidenceFileService s812EvidenceFileService = ALCOA.CSMS.Website.Global.GetInstance<repo.IS812EvidenceFileService>();
    repo.IEHSConsultantService ehsConsultantService = ALCOA.CSMS.Website.Global.GetInstance<repo.IEHSConsultantService>();   
    protected void Page_Load(object sender, EventArgs e) { Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {

        if (!postBack)
        {   
            //Todo: add in section to allow EHS Assessors to load docs.
            //check if user is EHS consultant
            model.EHSConsultant ehsConsultant = ehsConsultantService.Get(i => i.UserId == auth.UserId && i.Enabled == true, null);
            if (auth.RoleId == (int)RoleList.Administrator || ehsConsultant != null)
            {
                //show the upload control and teh delete button if the user is administrator
                uplNewFile.Visible = true;
                lblNewFile.Visible = true;
                gridFiles.Columns["CommandCol"].Visible = true;

            }
            else
            {
                //hide the upload control and teh delete button if the user is not administrator
                uplNewFile.Visible = false;
                lblNewFile.Visible = false;
                gridFiles.Columns["CommandCol"].Visible = false;


            }
            
        }
        else
            gridFiles.DataBind();
    }
    protected void ods_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        //used for the datasource to inject the context
        e.ObjectInstance = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IS812EvidenceFileService>();
    }


    protected void ods_UsersCreating(object sender, ObjectDataSourceEventArgs e)
    {
        //used for the datasource to inject the context
        e.ObjectInstance = ALCOA.CSMS.Website.Global.GetInstance<Repo.CSMS.Service.Database.IUserService>();
    }

    protected void uplNewFile_FileUploadComplete(object sender, DevExpress.Web.ASPxUploadControl.FileUploadCompleteEventArgs e)
    {
        //Runs when the file is uploaded to the server and this writes it to the database.        
        SaveAttachment(uplNewFile);
       
    }

    
    protected bool SaveAttachment(ASPxUploadControl fu)
    {
        
        Configuration configuration = new Configuration();
        string tempPath = @configuration.GetValue(ConfigList.TempDir).ToString();
        string fullPath = tempPath + fu.UploadedFiles[0].FileName;
        fu.UploadedFiles[0].SaveAs(fullPath);
        FileStream fs = new FileStream(fullPath, FileMode.Open,
                                                 FileAccess.Read,
                                                 FileShare.ReadWrite);


        model.S812EvidenceFile evFile = new model.S812EvidenceFile();
        evFile.FileName = fu.UploadedFiles[0].FileName;
        evFile.Content = FileUtilities.Read.ReadFully(fs, 51200);

        evFile.FileSize = Convert.ToInt32(fs.Length);
        evFile.UploadedById = auth.UserId;
        evFile.UploadDate = DateTime.Now;

        bool fail = false;
        try { s812EvidenceFileService.Insert(evFile); }
        catch (Exception ex) { Elmah.ErrorSignal.FromContext(Context).Raise(ex); fail = true; };

        fs.Close();
        fu.Dispose();
        File.Delete(fullPath);

        if (fail != true) { return true; } else { return false; };

    }

    protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {
       
    }

    protected void gridFiles_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        //File upload is ajax, so custom callback is performed to refresh the grid
        gridFiles.DataBind();
    }
}
