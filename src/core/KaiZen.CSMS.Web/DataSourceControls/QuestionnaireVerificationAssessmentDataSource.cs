﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireVerificationAssessmentProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireVerificationAssessmentDataSourceDesigner))]
	public class QuestionnaireVerificationAssessmentDataSource : ProviderDataSource<QuestionnaireVerificationAssessment, QuestionnaireVerificationAssessmentKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentDataSource class.
		/// </summary>
		public QuestionnaireVerificationAssessmentDataSource() : base(new QuestionnaireVerificationAssessmentService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireVerificationAssessmentDataSourceView used by the QuestionnaireVerificationAssessmentDataSource.
		/// </summary>
		protected QuestionnaireVerificationAssessmentDataSourceView QuestionnaireVerificationAssessmentView
		{
			get { return ( View as QuestionnaireVerificationAssessmentDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireVerificationAssessmentDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireVerificationAssessmentSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireVerificationAssessmentSelectMethod selectMethod = QuestionnaireVerificationAssessmentSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireVerificationAssessmentSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireVerificationAssessmentDataSourceView class that is to be
		/// used by the QuestionnaireVerificationAssessmentDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireVerificationAssessmentDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireVerificationAssessment, QuestionnaireVerificationAssessmentKey> GetNewDataSourceView()
		{
			return new QuestionnaireVerificationAssessmentDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireVerificationAssessmentDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireVerificationAssessmentDataSourceView : ProviderDataSourceView<QuestionnaireVerificationAssessment, QuestionnaireVerificationAssessmentKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireVerificationAssessmentDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireVerificationAssessmentDataSourceView(QuestionnaireVerificationAssessmentDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireVerificationAssessmentDataSource QuestionnaireVerificationAssessmentOwner
		{
			get { return Owner as QuestionnaireVerificationAssessmentDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireVerificationAssessmentSelectMethod SelectMethod
		{
			get { return QuestionnaireVerificationAssessmentOwner.SelectMethod; }
			set { QuestionnaireVerificationAssessmentOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireVerificationAssessmentService QuestionnaireVerificationAssessmentProvider
		{
			get { return Provider as QuestionnaireVerificationAssessmentService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireVerificationAssessment> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireVerificationAssessment> results = null;
			QuestionnaireVerificationAssessment item;
			count = 0;
			
			System.Int32 _responseId;
			System.Int32 _questionnaireId;
			System.Int32 _sectionId;
			System.Int32 _questionId;
			System.Int32 _modifiedByUserId;
			System.Boolean? _assessorApproval_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireVerificationAssessmentSelectMethod.Get:
					QuestionnaireVerificationAssessmentKey entityKey  = new QuestionnaireVerificationAssessmentKey();
					entityKey.Load(values);
					item = QuestionnaireVerificationAssessmentProvider.Get(entityKey);
					results = new TList<QuestionnaireVerificationAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetAll:
                    results = QuestionnaireVerificationAssessmentProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetPaged:
					results = QuestionnaireVerificationAssessmentProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireVerificationAssessmentProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireVerificationAssessmentProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireVerificationAssessmentSelectMethod.GetByResponseId:
					_responseId = ( values["ResponseId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ResponseId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationAssessmentProvider.GetByResponseId(_responseId);
					results = new TList<QuestionnaireVerificationAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireVerificationAssessmentSelectMethod.GetByQuestionnaireIdSectionIdQuestionId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					_questionId = ( values["QuestionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireIdSectionIdQuestionId(_questionnaireId, _sectionId, _questionId);
					results = new TList<QuestionnaireVerificationAssessment>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationAssessmentProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetByQuestionnaireIdAssessorApproval:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_assessorApproval_nullable = (System.Boolean?) EntityUtil.ChangeType(values["AssessorApproval"], typeof(System.Boolean?));
					results = QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireIdAssessorApproval(_questionnaireId, _assessorApproval_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetByQuestionnaireIdSectionIdAssessorApproval:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					_assessorApproval_nullable = (System.Boolean?) EntityUtil.ChangeType(values["AssessorApproval"], typeof(System.Boolean?));
					results = QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireIdSectionIdAssessorApproval(_questionnaireId, _sectionId, _assessorApproval_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireVerificationAssessmentSelectMethod.GetByQuestionnaireIdSectionId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_sectionId = ( values["SectionId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SectionId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireIdSectionId(_questionnaireId, _sectionId, this.StartIndex, this.PageSize, out count);
					break;
				// FK
				case QuestionnaireVerificationAssessmentSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireVerificationAssessmentProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireVerificationAssessmentSelectMethod.Get || SelectMethod == QuestionnaireVerificationAssessmentSelectMethod.GetByResponseId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireVerificationAssessment entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireVerificationAssessmentProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireVerificationAssessment> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireVerificationAssessmentProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireVerificationAssessmentDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireVerificationAssessmentDataSource class.
	/// </summary>
	public class QuestionnaireVerificationAssessmentDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireVerificationAssessment, QuestionnaireVerificationAssessmentKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentDataSourceDesigner class.
		/// </summary>
		public QuestionnaireVerificationAssessmentDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAssessmentSelectMethod SelectMethod
		{
			get { return ((QuestionnaireVerificationAssessmentDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireVerificationAssessmentDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireVerificationAssessmentDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireVerificationAssessmentDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireVerificationAssessmentDataSourceActionList : DesignerActionList
	{
		private QuestionnaireVerificationAssessmentDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireVerificationAssessmentDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireVerificationAssessmentDataSourceActionList(QuestionnaireVerificationAssessmentDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireVerificationAssessmentSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireVerificationAssessmentDataSourceActionList
	
	#endregion QuestionnaireVerificationAssessmentDataSourceDesigner
	
	#region QuestionnaireVerificationAssessmentSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireVerificationAssessmentDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireVerificationAssessmentSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByResponseId method.
		/// </summary>
		GetByResponseId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionIdQuestionId method.
		/// </summary>
		GetByQuestionnaireIdSectionIdQuestionId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdAssessorApproval method.
		/// </summary>
		GetByQuestionnaireIdAssessorApproval,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionIdAssessorApproval method.
		/// </summary>
		GetByQuestionnaireIdSectionIdAssessorApproval,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSectionId method.
		/// </summary>
		GetByQuestionnaireIdSectionId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId
	}
	
	#endregion QuestionnaireVerificationAssessmentSelectMethod

	#region QuestionnaireVerificationAssessmentFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentFilter : SqlFilter<QuestionnaireVerificationAssessmentColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentFilter

	#region QuestionnaireVerificationAssessmentExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentExpressionBuilder : SqlExpressionBuilder<QuestionnaireVerificationAssessmentColumn>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentExpressionBuilder	

	#region QuestionnaireVerificationAssessmentProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireVerificationAssessmentChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireVerificationAssessment"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireVerificationAssessmentProperty : ChildEntityProperty<QuestionnaireVerificationAssessmentChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireVerificationAssessmentProperty
}

