﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using Repo.CSMS.DAL.EntityModels;
using Repo.CSMS.DAL.Infrastructure;
using Repo.CSMS.Common;
using System.Web;
using Repo.CSMS.Common.Helpers;
using System.Data.Entity.Core.Objects;

namespace Repo.CSMS.Service.Database
{
    #region Interface

    public interface IEnumQuestionnaireRiskRatingService
    {
        EnumQuestionnaireRiskRating Get(Expression<Func<EnumQuestionnaireRiskRating, bool>> where, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes);
        List<EnumQuestionnaireRiskRating> GetMany(int? top, Expression<Func<EnumQuestionnaireRiskRating, bool>> where, Func<IQueryable<EnumQuestionnaireRiskRating>, IOrderedQueryable<EnumQuestionnaireRiskRating>> orderBy, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes);
        List<EnumQuestionnaireRiskRating> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EnumQuestionnaireRiskRating, bool>> where, Func<IQueryable<EnumQuestionnaireRiskRating>, IOrderedQueryable<EnumQuestionnaireRiskRating>> orderBy, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes, out int pageCount, out int totalRecords);
        int? GetCount(Expression<Func<EnumQuestionnaireRiskRating, bool>> where, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes);
        void Insert(EnumQuestionnaireRiskRating item, bool saveChanges = true);
        void Update(EnumQuestionnaireRiskRating item, bool saveChanges = true);
        void Delete(EnumQuestionnaireRiskRating item, bool saveChanges = true);
        void SaveChanges();
    }
    #endregion

    #region Interface Implementation

    public class EnumQuestionnaireRiskRatingService : IEnumQuestionnaireRiskRatingService
    {
        private readonly IRepository<EnumQuestionnaireRiskRating> _repository;

        public EnumQuestionnaireRiskRatingService(IRepository<EnumQuestionnaireRiskRating> repository)
        {
            this._repository = repository;
        }

        public EnumQuestionnaireRiskRating Get(Expression<Func<EnumQuestionnaireRiskRating, bool>> where, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes)
        {
            return _repository.Get(where, includes);
        }

        public List<EnumQuestionnaireRiskRating> GetMany(int? top, Expression<Func<EnumQuestionnaireRiskRating, bool>> where, Func<IQueryable<EnumQuestionnaireRiskRating>, IOrderedQueryable<EnumQuestionnaireRiskRating>> orderBy, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes)
        {
            if (where != null)
                return _repository.GetMany(where, top, orderBy, includes).ToList();
            else
                return _repository.GetAll(top, orderBy, includes).ToList();
        }

        public List<EnumQuestionnaireRiskRating> GetManyPaged(int pageNumber, int pageSize, Expression<Func<EnumQuestionnaireRiskRating, bool>> where, Func<IQueryable<EnumQuestionnaireRiskRating>, IOrderedQueryable<EnumQuestionnaireRiskRating>> orderBy, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes, out int pageCount, out int totalRecords)
        {
            return _repository.GetManyPaged(pageNumber, pageSize, where, orderBy, includes, out pageCount, out totalRecords).ToList();
        }

        public int? GetCount(Expression<Func<EnumQuestionnaireRiskRating, bool>> where, List<Expression<Func<EnumQuestionnaireRiskRating, object>>> includes)
        {
            return _repository.GetCount(where, includes);
        }

        public void Insert(EnumQuestionnaireRiskRating item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumQuestionnaireRiskRating");

            _repository.Insert(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Update(EnumQuestionnaireRiskRating item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumQuestionnaireRiskRating");

            _repository.Update(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void Delete(EnumQuestionnaireRiskRating item, bool saveChanges = true)
        {
            if (item == null)
                throw new ArgumentNullException("EnumQuestionnaireRiskRating");

            _repository.Delete(item);

            if (saveChanges)
                _repository.SaveChanges();
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }

    #endregion
}
