using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.Data;
using DevExpress.Web.ASPxGridView;
using KaiZen.CSMS.Services;
using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;

public partial class Adminv2_UserControls_KPI : System.Web.UI.UserControl
{

    Auth auth = new Auth();

    protected void Page_Load(object sender, EventArgs e) {  Helper._Auth.PageLoad(auth); moduleInit(Page.IsPostBack); }

    private void moduleInit(bool postBack)
    {
        if (!postBack)
        { //first time load
            //DropDownList1.SelectedValue = "All";
            //ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            

        }
    }

    protected void grid_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) //Default Values 
    {
        //if (e.NewValues["PostedOn"] == null) { e.NewValues["PostedOn"] = DateTime.Today.Date; }
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (DropDownList1.SelectedValue.ToString() != "All")
        //{
        //    ASPxGridView1.SettingsPager.PageSize = Convert.ToInt32(DropDownList1.SelectedValue.ToString());
        //}
        //else
        //{
        //    ASPxGridView1.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
        //}
        //ASPxGridView1.DataBind();
    }
}
