﻿#region Using Directives
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Web.UI;
using System.Web.UI.Design;

using KaiZen.CSMS.Entities;
using KaiZen.CSMS.Data;
using KaiZen.CSMS.Data.Bases;
using KaiZen.CSMS.Services;
#endregion

namespace KaiZen.CSMS.Web.Data
{
	/// <summary>
	/// Represents the DataRepository.QuestionnaireInitialLocationProvider object that provides
	/// data to data-bound controls in multi-tier Web application architectures.
	/// </summary>
	[Designer(typeof(QuestionnaireInitialLocationDataSourceDesigner))]
	public class QuestionnaireInitialLocationDataSource : ProviderDataSource<QuestionnaireInitialLocation, QuestionnaireInitialLocationKey>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationDataSource class.
		/// </summary>
		public QuestionnaireInitialLocationDataSource() : base(new QuestionnaireInitialLocationService())
		{
		}

		#endregion Constructors
		
		#region Properties
		
		/// <summary>
		/// Gets a reference to the QuestionnaireInitialLocationDataSourceView used by the QuestionnaireInitialLocationDataSource.
		/// </summary>
		protected QuestionnaireInitialLocationDataSourceView QuestionnaireInitialLocationView
		{
			get { return ( View as QuestionnaireInitialLocationDataSourceView ); }
		}
		
		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the QuestionnaireInitialLocationDataSource control invokes to retrieve data.
		/// </summary>
		public QuestionnaireInitialLocationSelectMethod SelectMethod
		{
			get
			{
				QuestionnaireInitialLocationSelectMethod selectMethod = QuestionnaireInitialLocationSelectMethod.GetAll;
				Object method = ViewState["SelectMethod"];
				if ( method != null )
				{
					selectMethod = (QuestionnaireInitialLocationSelectMethod) method;
				}
				return selectMethod;
			}
			set { ViewState["SelectMethod"] = value; }
		}

		#endregion Properties
		
		#region Methods

		/// <summary>
		/// Creates a new instance of the QuestionnaireInitialLocationDataSourceView class that is to be
		/// used by the QuestionnaireInitialLocationDataSource.
		/// </summary>
		/// <returns>An instance of the QuestionnaireInitialLocationDataSourceView class.</returns>
		protected override BaseDataSourceView<QuestionnaireInitialLocation, QuestionnaireInitialLocationKey> GetNewDataSourceView()
		{
			return new QuestionnaireInitialLocationDataSourceView(this, DefaultViewName);
		}
		
		/// <summary>
        /// Creates a cache hashing key based on the startIndex, pageSize and the SelectMethod being used.
        /// </summary>
        /// <param name="startIndex">The current start row index.</param>
        /// <param name="pageSize">The current page size.</param>
        /// <returns>A string that can be used as a key for caching purposes.</returns>
		protected override string CacheHashKey(int startIndex, int pageSize)
        {
			return String.Format("{0}:{1}:{2}", SelectMethod, startIndex, pageSize);
        }
		
		#endregion Methods
	}
	
	/// <summary>
	/// Supports the QuestionnaireInitialLocationDataSource control and provides an interface for
	/// data-bound controls to perform data operations with business and data objects.
	/// </summary>
	public class QuestionnaireInitialLocationDataSourceView : ProviderDataSourceView<QuestionnaireInitialLocation, QuestionnaireInitialLocationKey>
	{
		#region Declarations

		#endregion Declarations
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationDataSourceView class.
		/// </summary>
		/// <param name="owner">A reference to the QuestionnaireInitialLocationDataSource which created this instance.</param>
		/// <param name="viewName">The name of the view.</param>
		public QuestionnaireInitialLocationDataSourceView(QuestionnaireInitialLocationDataSource owner, String viewName)
			: base(owner, viewName)
		{
		}
		
		#endregion Constructors
		
		#region Properties

		/// <summary>
		/// Gets a strongly-typed reference to the Owner property.
		/// </summary>
		internal QuestionnaireInitialLocationDataSource QuestionnaireInitialLocationOwner
		{
			get { return Owner as QuestionnaireInitialLocationDataSource; }
		}

		/// <summary>
		/// Gets or sets the name of the method or function that
		/// the DataSource control invokes to retrieve data.
		/// </summary>
		internal QuestionnaireInitialLocationSelectMethod SelectMethod
		{
			get { return QuestionnaireInitialLocationOwner.SelectMethod; }
			set { QuestionnaireInitialLocationOwner.SelectMethod = value; }
		}

		/// <summary>
		/// Gets a strongly typed reference to the Provider property.
		/// </summary>
		internal QuestionnaireInitialLocationService QuestionnaireInitialLocationProvider
		{
			get { return Provider as QuestionnaireInitialLocationService; }
		}

		#endregion Properties
		
		#region Methods
		 
		/// <summary>
		/// Gets a collection of Entity objects based on the value of the SelectMethod property.
		/// </summary>
		/// <param name="count">The total number of rows in the DataSource.</param>
	    /// <param name="values"></param>
		/// <returns>A collection of Entity objects.</returns>
		protected override IList<QuestionnaireInitialLocation> GetSelectData(IDictionary values, out int count)
		{
            if (values == null || values.Count == 0) values = CollectionsUtil.CreateCaseInsensitiveHashtable(GetParameterValues());
            
			Hashtable customOutput = CollectionsUtil.CreateCaseInsensitiveHashtable();
			IList<QuestionnaireInitialLocation> results = null;
			QuestionnaireInitialLocation item;
			count = 0;
			
			System.Int32 _questionnaireLocationId;
			System.Int32 _questionnaireId;
			System.Int32 _siteId;
			System.Int32? _approvedByUserId_nullable;
			System.Int32 _modifiedByUserId;
			System.Int32? _spa_nullable;

			switch ( SelectMethod )
			{
				case QuestionnaireInitialLocationSelectMethod.Get:
					QuestionnaireInitialLocationKey entityKey  = new QuestionnaireInitialLocationKey();
					entityKey.Load(values);
					item = QuestionnaireInitialLocationProvider.Get(entityKey);
					results = new TList<QuestionnaireInitialLocation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				case QuestionnaireInitialLocationSelectMethod.GetAll:
                    results = QuestionnaireInitialLocationProvider.GetAll(StartIndex, PageSize, out count);
                    break;
				case QuestionnaireInitialLocationSelectMethod.GetPaged:
					results = QuestionnaireInitialLocationProvider.GetPaged(WhereClause, OrderBy, PageIndex, PageSize, out count);
					break;
				case QuestionnaireInitialLocationSelectMethod.Find:
					if ( FilterParameters != null )
						results = QuestionnaireInitialLocationProvider.Find(FilterParameters, OrderBy, StartIndex, PageSize, out count);
					else
						results = QuestionnaireInitialLocationProvider.Find(WhereClause, StartIndex, PageSize, out count);
                    break;
				// PK
				case QuestionnaireInitialLocationSelectMethod.GetByQuestionnaireLocationId:
					_questionnaireLocationId = ( values["QuestionnaireLocationId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireLocationId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialLocationProvider.GetByQuestionnaireLocationId(_questionnaireLocationId);
					results = new TList<QuestionnaireInitialLocation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// IX
				case QuestionnaireInitialLocationSelectMethod.GetByQuestionnaireId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialLocationProvider.GetByQuestionnaireId(_questionnaireId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialLocationSelectMethod.GetByQuestionnaireIdSiteId:
					_questionnaireId = ( values["QuestionnaireId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["QuestionnaireId"], typeof(System.Int32)) : (int)0;
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					item = QuestionnaireInitialLocationProvider.GetByQuestionnaireIdSiteId(_questionnaireId, _siteId);
					results = new TList<QuestionnaireInitialLocation>();
					if ( item != null ) results.Add(item);
					count = results.Count;
					break;
				// FK
				case QuestionnaireInitialLocationSelectMethod.GetBySiteId:
					_siteId = ( values["SiteId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["SiteId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialLocationProvider.GetBySiteId(_siteId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialLocationSelectMethod.GetByApprovedByUserId:
					_approvedByUserId_nullable = (System.Int32?) EntityUtil.ChangeType(values["ApprovedByUserId"], typeof(System.Int32?));
					results = QuestionnaireInitialLocationProvider.GetByApprovedByUserId(_approvedByUserId_nullable, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialLocationSelectMethod.GetByModifiedByUserId:
					_modifiedByUserId = ( values["ModifiedByUserId"] != null ) ? (System.Int32) EntityUtil.ChangeType(values["ModifiedByUserId"], typeof(System.Int32)) : (int)0;
					results = QuestionnaireInitialLocationProvider.GetByModifiedByUserId(_modifiedByUserId, this.StartIndex, this.PageSize, out count);
					break;
				case QuestionnaireInitialLocationSelectMethod.GetBySpa:
					_spa_nullable = (System.Int32?) EntityUtil.ChangeType(values["Spa"], typeof(System.Int32?));
					results = QuestionnaireInitialLocationProvider.GetBySpa(_spa_nullable, this.StartIndex, this.PageSize, out count);
					break;
				// M:M
				// Custom
				default:
					break;
			}

			if ( results != null && count < 1 )
			{
				count = results.Count;

				if ( !String.IsNullOrEmpty(CustomMethodRecordCountParamName) )
				{
					object objCustomCount = EntityUtil.ChangeType(customOutput[CustomMethodRecordCountParamName], typeof(Int32));
					
					if ( objCustomCount != null )
					{
						count = (int) objCustomCount;
					}
				}
			}
			
			return results;
		}
		
		/// <summary>
		/// Gets the values of any supplied parameters for internal caching.
		/// </summary>
		/// <param name="values">An IDictionary object of name/value pairs.</param>
		protected override void GetSelectParameters(IDictionary values)
		{
			if ( SelectMethod == QuestionnaireInitialLocationSelectMethod.Get || SelectMethod == QuestionnaireInitialLocationSelectMethod.GetByQuestionnaireLocationId )
			{
				EntityId = GetEntityKey(values);
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation for the current entity if it has
		/// not already been performed.
		/// </summary>
		internal override void DeepLoad()
		{
			if ( !IsDeepLoaded )
			{
				QuestionnaireInitialLocation entity = GetCurrentEntity();
				
				if ( entity != null )
				{
					// init transaction manager
					GetTransactionManager();
					// execute deep load method
					QuestionnaireInitialLocationProvider.DeepLoad(GetCurrentEntity(), EnableRecursiveDeepLoad);
					// set loaded flag
					IsDeepLoaded = true;
				}
			}
		}

		/// <summary>
		/// Performs a DeepLoad operation on the specified entity collection.
		/// </summary>
		/// <param name="entityList"></param>
		/// <param name="properties"></param>
		internal override void DeepLoad(TList<QuestionnaireInitialLocation> entityList, ProviderDataSourceDeepLoadList properties)
		{
			// init transaction manager
			GetTransactionManager();
			// execute deep load method
			QuestionnaireInitialLocationProvider.DeepLoad(entityList, properties.Recursive, properties.Method, properties.GetTypes());
		}

		#endregion Select Methods
	}
	
	#region QuestionnaireInitialLocationDataSourceDesigner

	/// <summary>
	/// Provides design-time support in a design host for the QuestionnaireInitialLocationDataSource class.
	/// </summary>
	public class QuestionnaireInitialLocationDataSourceDesigner : ProviderDataSourceDesigner<QuestionnaireInitialLocation, QuestionnaireInitialLocationKey>
	{
		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationDataSourceDesigner class.
		/// </summary>
		public QuestionnaireInitialLocationDataSourceDesigner()
		{
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialLocationSelectMethod SelectMethod
		{
			get { return ((QuestionnaireInitialLocationDataSource) DataSource).SelectMethod; }
			set { SetPropertyValue("SelectMethod", value); }
		}

		/// <summary>Gets the designer action list collection for this designer.</summary>
		/// <returns>The <see cref="T:System.ComponentModel.Design.DesignerActionListCollection"/>
		/// associated with this designer.</returns>
		public override DesignerActionListCollection ActionLists
		{
			get
			{
				DesignerActionListCollection actions = new DesignerActionListCollection();
				actions.Add(new QuestionnaireInitialLocationDataSourceActionList(this));
				actions.AddRange(base.ActionLists);
				return actions;
			}
		}
	}

	#region QuestionnaireInitialLocationDataSourceActionList

	/// <summary>
	/// Supports the QuestionnaireInitialLocationDataSourceDesigner class.
	/// </summary>
	internal class QuestionnaireInitialLocationDataSourceActionList : DesignerActionList
	{
		private QuestionnaireInitialLocationDataSourceDesigner _designer;

		/// <summary>
		/// Initializes a new instance of the QuestionnaireInitialLocationDataSourceActionList class.
		/// </summary>
		/// <param name="designer"></param>
		public QuestionnaireInitialLocationDataSourceActionList(QuestionnaireInitialLocationDataSourceDesigner designer) : base(designer.Component)
		{
			_designer = designer;
		}

		/// <summary>
		/// Gets or sets the SelectMethod property.
		/// </summary>
		public QuestionnaireInitialLocationSelectMethod SelectMethod
		{
			get { return _designer.SelectMethod; }
			set { _designer.SelectMethod = value; }
		}

		/// <summary>
		/// Returns the collection of <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// objects contained in the list.
		/// </summary>
		/// <returns>A <see cref="T:System.ComponentModel.Design.DesignerActionItem"/>
		/// array that contains the items in this list.</returns>
		public override DesignerActionItemCollection GetSortedActionItems()
		{
			DesignerActionItemCollection items = new DesignerActionItemCollection();
			items.Add(new DesignerActionPropertyItem("SelectMethod", "Select Method", "Methods"));
			return items;
		}
	}

	#endregion QuestionnaireInitialLocationDataSourceActionList
	
	#endregion QuestionnaireInitialLocationDataSourceDesigner
	
	#region QuestionnaireInitialLocationSelectMethod
	
	/// <summary>
	/// Enumeration of method names available for the QuestionnaireInitialLocationDataSource.SelectMethod property.
	/// </summary>
	public enum QuestionnaireInitialLocationSelectMethod
	{
		/// <summary>
		/// Represents the Get method.
		/// </summary>
		Get,
		/// <summary>
		/// Represents the GetAll method.
		/// </summary>
		GetAll,
		/// <summary>
		/// Represents the GetPaged method.
		/// </summary>
		GetPaged,
		/// <summary>
		/// Represents the Find method.
		/// </summary>
		Find,
		/// <summary>
		/// Represents the GetByQuestionnaireLocationId method.
		/// </summary>
		GetByQuestionnaireLocationId,
		/// <summary>
		/// Represents the GetByQuestionnaireId method.
		/// </summary>
		GetByQuestionnaireId,
		/// <summary>
		/// Represents the GetByQuestionnaireIdSiteId method.
		/// </summary>
		GetByQuestionnaireIdSiteId,
		/// <summary>
		/// Represents the GetBySiteId method.
		/// </summary>
		GetBySiteId,
		/// <summary>
		/// Represents the GetByApprovedByUserId method.
		/// </summary>
		GetByApprovedByUserId,
		/// <summary>
		/// Represents the GetByModifiedByUserId method.
		/// </summary>
		GetByModifiedByUserId,
		/// <summary>
		/// Represents the GetBySpa method.
		/// </summary>
		GetBySpa
	}
	
	#endregion QuestionnaireInitialLocationSelectMethod

	#region QuestionnaireInitialLocationFilter
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilter&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationFilter : SqlFilter<QuestionnaireInitialLocationColumn>
	{
	}
	
	#endregion QuestionnaireInitialLocationFilter

	#region QuestionnaireInitialLocationExpressionBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlExpressionBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationExpressionBuilder : SqlExpressionBuilder<QuestionnaireInitialLocationColumn>
	{
	}
	
	#endregion QuestionnaireInitialLocationExpressionBuilder	

	#region QuestionnaireInitialLocationProperty
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ChildEntityProperty&lt;QuestionnaireInitialLocationChildEntityTypes&gt;"/> class
	/// that is used exclusively with a <see cref="QuestionnaireInitialLocation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class QuestionnaireInitialLocationProperty : ChildEntityProperty<QuestionnaireInitialLocationChildEntityTypes>
	{
	}
	
	#endregion QuestionnaireInitialLocationProperty
}

